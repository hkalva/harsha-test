package com.lyonscg.cisavs.experian.avs.converter.request;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.lyonscg.cisavs.experian.avs.utils.CisAvsTestUtil;
import com.qas.ondemand_2011_03.QAGetAddress;


/**
 * Unit Tests for {@link ExperianQAGetAddressConverter}.
 */
public class ExperianQAGetAddressConverterTest
{
    private ExperianQAGetAddressConverter testClass;

    /**
     * Setup test data.
     */
    @Before
    public void setUp() throws Exception
    {
        testClass = new ExperianQAGetAddressConverter();
    }

    /**
     * Unit test for {@link ExperianQAGetAddressConverter#convert(com.hybris.cis.api.model.com.hybris.cis.api.model)} with com.hybris.cis.api.model
     * as null.
     */
    @Test
    public void convertWithNullSource() throws Exception
    {
        Assert.assertNull(testClass.convert(null));
    }

    /**
     * Positive Unit test for {@link ExperianQAGetAddressConverter#convert(com.hybris.cis.api.model.com.hybris.cis.api.model)}.
     */
    @Test
    public void convertWithSampleSource() throws Exception
    {
    	QAGetAddress qaGetAddress = testClass.convert(CisAvsTestUtil.sampleCisAddress());
        Assert.assertNotNull(qaGetAddress);
        Assert.assertEquals(CisAvsTestUtil.SampleAddress.MONIKER, qaGetAddress.getMoniker());   
        Assert.assertEquals("Magic Leap", qaGetAddress.getLayout());
        }

}