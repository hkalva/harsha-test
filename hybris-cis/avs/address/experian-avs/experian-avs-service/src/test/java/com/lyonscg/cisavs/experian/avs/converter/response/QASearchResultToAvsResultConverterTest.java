package com.lyonscg.cisavs.experian.avs.converter.response;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.convert.converter.Converter;

import com.hybris.cis.api.avs.model.AvsResult;
import com.hybris.cis.api.model.CisAddress;
import com.lyonscg.cisavs.experian.avs.utils.CisAvsTestUtil;
import com.lyonscg.cisavs.experian.avs.utils.ExperianAvsTestUtil;
import com.qas.ondemand_2011_03.PicklistEntryType;
import com.qas.ondemand_2011_03.VerifyLevelType;


/**
 * Unit Tests for {@link QASearchResultToAvsResultConverter}.
 * 
 * @author lyonscg
 */
@RunWith(MockitoJUnitRunner.class)
public class QASearchResultToAvsResultConverterTest
{
	 @InjectMocks
    private QASearchResultToAvsResultConverter qaSearchResultToAvsResultConverter;
    @Mock
    private Converter<PicklistEntryType, CisAddress> picklistEntryTypeToCisAddressConverter=Mockito.mock(PicklistEntryTypeToCisAddressConverter.class);
    

    /**
     * Setup test data.
     */
    @Before
    public void setUp() throws Exception
    {    	
    	qaSearchResultToAvsResultConverter = new QASearchResultToAvsResultConverter();
    	qaSearchResultToAvsResultConverter.setPicklistEntryTypeToCisAddressConverter(picklistEntryTypeToCisAddressConverter);
    	Mockito.when(picklistEntryTypeToCisAddressConverter.convert(Mockito.any(PicklistEntryType.class))).thenReturn(CisAvsTestUtil.sampleCisAddress());    	
    }

    /**	
     * Unit test for
     * {@link QASearchResultToAvsResultConverter#convert(com.qas.ondemand_2011_03.QASearchResult)} with
     * QASearchOk as null.
     */
    @Test
    public void convertWithNullSource() throws Exception
    {
        Assert.assertNull(qaSearchResultToAvsResultConverter.convert(null));
    }

    /**
     * Unit test for success
     * {@link QASearchResultToAvsResultConverter#convert(com.qas.ondemand_2011_03.QASearchResult)}.
     */
    @Test
    public void convertWithSampleSource() throws Exception
    {
        // Avs Result
        final AvsResult avsResult = qaSearchResultToAvsResultConverter.convert(ExperianAvsTestUtil.sampleQASearchResult());

        Assert.assertNotNull(avsResult);
        Assert.assertEquals("AVS", avsResult.getVendorId());
        
       
        List<CisAddress> suggestedAddresses = avsResult.getSuggestedAddresses();
        Assert.assertNotNull(suggestedAddresses);
        Assert.assertEquals(1, suggestedAddresses.size());
        
        CisAddress cisaddress = suggestedAddresses.get(0);
        Assert.assertNotNull(cisaddress);     
        
        Assert.assertEquals(ExperianAvsTestUtil.PicklistEntryTypeData.PICK_LIST, cisaddress.getAddressLine1());
        Assert.assertEquals(ExperianAvsTestUtil.PicklistEntryTypeData.POSTCODE, cisaddress.getZipCode());
        
        Map<String,String> vendorResponsesMap = avsResult.getVendorResponses().getMap();
        Assert.assertNotNull(vendorResponsesMap);
        
        Assert.assertEquals(ExperianAvsTestUtil.QAPicklistTypeData.MONIKER, vendorResponsesMap.get("fullPicklistMoniker"));
        Assert.assertEquals(ExperianAvsTestUtil.QAPicklistTypeData.PROMPT, vendorResponsesMap.get("prompt"));
        Assert.assertEquals(VerifyLevelType.VERIFIED.toString(), vendorResponsesMap.get("verifyLevel"));
        Assert.assertEquals(ExperianAvsTestUtil.QAPicklistTypeData.TIME_OUT, Boolean.valueOf(vendorResponsesMap.get("timeout")));      
        
    }    
    
}
