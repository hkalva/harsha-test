package com.lyonscg.cisavs.experian.avs.converter.response;

import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.hybris.cis.api.avs.model.AvsResult;
import com.hybris.cis.api.model.CisAddress;
import com.hybris.cis.api.model.CisDecision;
import com.lyonscg.cisavs.experian.avs.utils.ExperianAvsTestUtil;
import com.qas.ondemand_2011_03.DPVStatusType;


/**
 * Unit Tests for {@link PicklistEntryTypeToCisAddressConverter}.
 * 
 * @author lyonscg
 */
public class PicklistEntryTypeToCisAddressConverterTest
{
    private PicklistEntryTypeToCisAddressConverter picklistEntryTypeToCisAddressConverter;

    /**
     * Setup test data.
     */
    @Before
    public void setUp() throws Exception
    {
    	picklistEntryTypeToCisAddressConverter = new PicklistEntryTypeToCisAddressConverter();
    }

    /**
     * Unit test for
     * {@link PicklistEntryTypeToCisAddressConverter#convert(com.qas.ondemand_2011_03.PicklistEntryType)} with
     * QASearchOk as null.
     */
    @Test
    public void convertWithNullSource() throws Exception
    {
        Assert.assertNull(picklistEntryTypeToCisAddressConverter.convert(null));
    }

    /**
     * Unit test for success
     * {@link PicklistEntryTypeToCisAddressConverter#convert(com.qas.ondemand_2011_03.PicklistEntryType)}.
     */
    @Test
    public void convertWithSampleSource() throws Exception
    {
        // Avs Result
        final CisAddress cisAddress = picklistEntryTypeToCisAddressConverter.convert(ExperianAvsTestUtil.samplePicklistEntryType());
        Assert.assertNotNull(cisAddress);
        Assert.assertEquals(ExperianAvsTestUtil.PicklistEntryTypeData.PICK_LIST, cisAddress.getAddressLine1());
        Assert.assertEquals(ExperianAvsTestUtil.PicklistEntryTypeData.POSTCODE, cisAddress.getZipCode()); 
        Map<String,String> vendorParametersMap = cisAddress.getVendorParameters().getMap();
        Assert.assertNotNull(vendorParametersMap);        
        Assert.assertEquals(ExperianAvsTestUtil.PicklistEntryTypeData.MONIKER, vendorParametersMap.get("moniker"));        
    }    
    
}
