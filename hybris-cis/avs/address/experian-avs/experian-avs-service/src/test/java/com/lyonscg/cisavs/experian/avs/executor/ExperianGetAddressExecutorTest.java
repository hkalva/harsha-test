package com.lyonscg.cisavs.experian.avs.executor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.hybris.cis.api.avs.model.AvsResult;
import com.hybris.cis.api.model.CisAddress;
import com.lyonscg.cisavs.experian.api.logger.ExperianXMLLogger;
import com.lyonscg.cisavs.experian.avs.utils.CisAvsTestUtil;
import com.qas.ondemand_2011_03.Address;
import com.qas.ondemand_2011_03.QAAddressType;
import com.qas.ondemand_2011_03.QAGetAddress;
import com.qas.ondemand_2011_03.QAPortType;


/**
 * Unit test for {@link ExperianGetAddressExecutor}.
 */
@RunWith(MockitoJUnitRunner.class)
public class ExperianGetAddressExecutorTest
{
    @InjectMocks
    private ExperianGetAddressExecutor testClass;
    @Mock
	private Converter<CisAddress, QAGetAddress> experianQAGetAddressConverter;
    @Mock
	private Converter<QAAddressType, AvsResult> qaAddressTypeToAvsResultConverter;
    @Mock
    private QAPortType experianServiceClient ;    
    @Mock
    private QAGetAddress qaGetAddress;
    @Mock
    private AvsResult avsResult;
    @Mock
    private Address address;   
    @Mock
    private ExperianXMLLogger experianXMLLogger;

    private CisAddress cisAddress = CisAvsTestUtil.sampleCisAddress();
    private final Logger LOG = LoggerFactory.getLogger(ExperianCanSearchExecutor.class);

    /**
     * Delegation test for {@link ExperianGetAddressExecutor#convertRequest(CisAddress)}.
     */
    @Test
    public void convertRequestTest()
    {
       testClass.convertRequest(cisAddress);
        Mockito.verify(experianQAGetAddressConverter).convert(cisAddress);
    }

    /**
     * Delegation test for
     * {@link ExperianGetAddressExecutor#convertResponse(CisAddress, QAGetAddress, Address)}.
     */
    @Test
    public void convertResponseTest()
    {
        testClass.convertResponse(cisAddress, qaGetAddress, address);
        Mockito.verify(qaAddressTypeToAvsResultConverter).convert(address.getQAAddress());
    }

    /**
     * Delegation test for {@link ExperianGetAddressExecutor#process(GetAddress)}.
     */
    @Test
    public void processTest()
    {
        testClass.process(qaGetAddress);
        Mockito.verify(experianServiceClient).doGetAddress(qaGetAddress);
    }

}