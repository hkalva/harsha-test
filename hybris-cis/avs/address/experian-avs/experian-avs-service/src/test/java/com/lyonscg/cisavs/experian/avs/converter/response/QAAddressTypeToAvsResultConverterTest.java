package com.lyonscg.cisavs.experian.avs.converter.response;

import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.hybris.cis.api.avs.model.AvsResult;
import com.hybris.cis.api.model.CisAddress;
import com.hybris.cis.api.model.CisDecision;
import com.lyonscg.cisavs.experian.avs.utils.ExperianAvsTestUtil;
import com.qas.ondemand_2011_03.DPVStatusType;


/**
 * Unit Tests for {@link QAAddressTypeToAvsResultConverter}.
 * 
 * @author lyonscg
 */
public class QAAddressTypeToAvsResultConverterTest
{
    private QAAddressTypeToAvsResultConverter qaAddressTypeToAvsResultConverter;

    /**
     * Setup test data.
     */
    @Before
    public void setUp() throws Exception
    {
    	qaAddressTypeToAvsResultConverter = new QAAddressTypeToAvsResultConverter();
    }

    /**
     * Unit test for
     * {@link QAAddressTypeToAvsResultConverter#convert(com.qas.ondemand_2011_03.QAAddressType)} with
     * QASearchOk as null.
     */
    @Test
    public void convertWithNullSource() throws Exception
    {
        Assert.assertNull(qaAddressTypeToAvsResultConverter.convert(null));
    }

    /**
     * Unit test for success
     * {@link QAAddressTypeToAvsResultConverter#convert(com.qas.ondemand_2011_03.QAAddressType)}.
     */
    @Test
    public void convertWithSampleSource() throws Exception
    {
        // Avs Result
        final AvsResult avsResult = qaAddressTypeToAvsResultConverter.convert(ExperianAvsTestUtil.sampleQAAddressType());

        Assert.assertNotNull(avsResult);
        Assert.assertEquals(CisDecision.ACCEPT, avsResult.getDecision());
        Assert.assertEquals("AVS", avsResult.getVendorId());
        
       
        List<CisAddress> suggestedAddresses = avsResult.getSuggestedAddresses();
        Assert.assertNotNull(suggestedAddresses);
        Assert.assertEquals(1, suggestedAddresses.size());
        
        CisAddress cisaddress = suggestedAddresses.get(0);
        Assert.assertNotNull(cisaddress);     
        
        Assert.assertEquals(ExperianAvsTestUtil.AddressLineTypeAddressLine1.LINE, cisaddress.getAddressLine1());
        Assert.assertEquals(ExperianAvsTestUtil.AddressLineTypeTown.LINE, cisaddress.getCity());
        Assert.assertEquals(ExperianAvsTestUtil.AddressLineTypePostcode.LINE, cisaddress.getZipCode());
        Assert.assertEquals(ExperianAvsTestUtil.AddressLineTypeState.LINE, cisaddress.getState());
        Assert.assertEquals(ExperianAvsTestUtil.AddressLineTypeCountry.LINE, cisaddress.getCountry());
        
        Map<String,String> vendorParametersMap = cisaddress.getVendorParameters().getMap();
        Assert.assertNotNull(vendorParametersMap);
        
        Assert.assertEquals(false, Boolean.valueOf(vendorParametersMap.get("overflow")));
        Assert.assertEquals(false, Boolean.valueOf(vendorParametersMap.get("truncated")));
        Assert.assertEquals(DPVStatusType.DPV_CONFIRMED.toString(), vendorParametersMap.get("dpvStatus"));
        Assert.assertEquals(false, Boolean.valueOf(vendorParametersMap.get("missingSubPremise")));
      
        
    }    
    
}
