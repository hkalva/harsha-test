package com.lyonscg.cisavs.experian.avs.executor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.convert.converter.Converter;

import com.hybris.cis.api.executor.ServiceMethodRequest;
import com.hybris.cis.api.model.CisAddress;
import com.hybris.cis.api.model.CisResult;
import com.lyonscg.cisavs.experian.api.logger.ExperianXMLLogger;
import com.lyonscg.cisavs.experian.avs.utils.CisAvsTestUtil;
import com.qas.ondemand_2011_03.QACanSearch;
import com.qas.ondemand_2011_03.QAPortType;
import com.qas.ondemand_2011_03.QASearchOk;


/**
 * Unit test for {@link ExperianCanSearchExecutor}.
 */
@RunWith(MockitoJUnitRunner.class)
public class ExperianCanSearchExecutorTest
{
    @InjectMocks
    private ExperianCanSearchExecutor testClass;
    @Mock
    private Converter<CisAddress, QACanSearch> experianQACanSearchConverter;
    @Mock
	private Converter<QASearchOk, CisResult> qaSearchOkToCisResultConverter;
    @Mock
    private QAPortType experianServiceClient ;    
    @Mock
    private QACanSearch qaCanSearch;
    @Mock
    private CisResult cisResult;
    @Mock
    private QASearchOk qaSearchOk;   
    @Mock
    private ExperianXMLLogger experianXMLLogger;

    private CisAddress cisAddress = CisAvsTestUtil.sampleCisAddress();

    /**
     * Delegation test for {@link ExperianCanSearchExecutor#convertRequest(CisAddress)}.
     */
    @Test
    public void convertRequestTest()
    {
       testClass.convertRequest(cisAddress);
        Mockito.verify(experianQACanSearchConverter).convert(cisAddress);
    }

    /**
     * Delegation test for
     * {@link ExperianCanSearchExecutor#convertResponse(CisAddress, QACanSearch, QASearchOk)}.
     */
    @Test
    public void convertResponseTest()
    {
        testClass.convertResponse(cisAddress, qaCanSearch, qaSearchOk);
        Mockito.verify(qaSearchOkToCisResultConverter).convert(qaSearchOk);
    }

    /**
     * Delegation test for {@link ExperianCanSearchExecutor#process(QACanSearch)}.
     */
    @Test
    public void processTest()
    {
        testClass.process(qaCanSearch);
        Mockito.verify(experianServiceClient).doCanSearch(qaCanSearch);
    }

}