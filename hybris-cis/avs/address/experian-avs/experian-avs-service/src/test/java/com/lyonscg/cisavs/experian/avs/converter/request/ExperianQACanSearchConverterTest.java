package com.lyonscg.cisavs.experian.avs.converter.request;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.lyonscg.cisavs.experian.avs.utils.CisAvsTestUtil;
import com.qas.ondemand_2011_03.QACanSearch;


/**
 * Unit Tests for {@link ExperianQACanSearchConverter}.
 */
public class ExperianQACanSearchConverterTest
{
    private ExperianQACanSearchConverter testClass;

    /**
     * Setup test data.
     */
    @Before
    public void setUp() throws Exception
    {
        testClass = new ExperianQACanSearchConverter();
    }

    /**
     * Unit test for {@link ExperianQACanSearchConverter#convert(com.hybris.cis.api.model.com.hybris.cis.api.model)} with com.hybris.cis.api.model
     * as null.
     */
    @Test
    public void convertWithNullSource() throws Exception
    {
        Assert.assertNull(testClass.convert(null));
    }

    /**
     * Positive Unit test for {@link ExperianQACanSearchConverter#convert(com.hybris.cis.api.model.com.hybris.cis.api.model)}.
     */
    @Test
    public void convertWithSampleSource() throws Exception
    {
    	QACanSearch qaCanSearch = testClass.convert(CisAvsTestUtil.sampleCisAddress());

        Assert.assertNotNull(qaCanSearch);
        Assert.assertEquals(CisAvsTestUtil.SampleAddress.COUNTRY, qaCanSearch.getCountry());
        Assert.assertEquals(CisAvsTestUtil.SampleEngineType.FLATTEN, qaCanSearch.getEngine().isFlatten());
        Assert.assertEquals(CisAvsTestUtil.SampleEngineType.INTENSITY, qaCanSearch.getEngine().getIntensity());
        Assert.assertEquals(CisAvsTestUtil.SampleEngineType.ENGINE_ENUM, qaCanSearch.getEngine().getValue());
        Assert.assertEquals("Magic Leap", qaCanSearch.getLayout());
    }

}