package com.lyonscg.cisavs.experian.avs.utils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.qas.ondemand_2011_03.AddressLineType;
import com.qas.ondemand_2011_03.DPVStatusType;
import com.qas.ondemand_2011_03.LineContentType;
import com.qas.ondemand_2011_03.PicklistEntryType;
import com.qas.ondemand_2011_03.QAAddressType;
import com.qas.ondemand_2011_03.QAPicklistType;
import com.qas.ondemand_2011_03.QASearchOk;
import com.qas.ondemand_2011_03.QASearchResult;
import com.qas.ondemand_2011_03.VerificationFlagsType;
import com.qas.ondemand_2011_03.VerifyLevelType;


/**
 * Test Utility for Experian AVS Service DTO objects.
 *
 * @author lyonscg
 */
public abstract class ExperianAvsTestUtil
{
    /*** Can Search Response - QASearchOk ***/
   
	public static QASearchOk sampleSuccessQASearchOk()
    {
		QASearchOk qaSearchOk = new QASearchOk();
		qaSearchOk.setIsOk(true);
        return qaSearchOk;
    }
	
	public static QASearchOk sampleFailQASearchOk()
    {
		QASearchOk qaSearchOk = new QASearchOk();
		qaSearchOk.setIsOk(false);
		qaSearchOk.setErrorCode("503");
		qaSearchOk.setErrorMessage("The search combination is not correct");
		qaSearchOk.getErrorDetail().add("The search combination is not correct");
        return qaSearchOk;
    }
  
	public interface QASearchOkSuccess
    {
        Boolean ISOK = true;
        String ERROR_CODE = null;
        String ERROR_MESSAGE = null;
        List<String> ERROR_DETAIL = null;
    }
	
	public interface QASearchOkFail
    {
        Boolean ISOK = false;
        String ERROR_CODE = "503";
        String ERROR_MESSAGE = "The search combination is not correct";
        List<String> ERROR_DETAIL = new ArrayList<String>(
        	    Arrays.asList("The search combination is not correct"));
    }
	
	/*** Search Response - QASearchResult ***/
	
	public interface PicklistEntryTypeData
    {
		 String PICK_LIST = "123 Main Street";
		 String POSTCODE = "12345";
		 String MONIKER = "testmoniker";
		 BigInteger SCORE = new BigInteger("100");
    }
	
	public static PicklistEntryType samplePicklistEntryType()
    {
		PicklistEntryType picklistEntryType = new PicklistEntryType();
		picklistEntryType.setPicklist(PicklistEntryTypeData.PICK_LIST);
		picklistEntryType.setPostcode(PicklistEntryTypeData.POSTCODE);
		picklistEntryType.setMoniker(PicklistEntryTypeData.MONIKER);
		picklistEntryType.setScore(PicklistEntryTypeData.SCORE);
        return picklistEntryType;
    }
	
	public interface QAPicklistTypeData
    {
		 String PROMPT = "test";
		 String MONIKER = "testmoniker";
		 Boolean TIME_OUT = false;
		 BigInteger TOTAL = new BigInteger("5");
    }
	
	public static QAPicklistType sampleQAPicklistType()
    {
		QAPicklistType qaPicklistType = new QAPicklistType();
		qaPicklistType.setFullPicklistMoniker(QAPicklistTypeData.MONIKER);
		qaPicklistType.setPrompt(QAPicklistTypeData.PROMPT);
		qaPicklistType.setTimeout(QAPicklistTypeData.TIME_OUT);
		qaPicklistType.setTotal(QAPicklistTypeData.TOTAL);
		qaPicklistType.getPicklistEntry().add(samplePicklistEntryType());
        return qaPicklistType;
    }
	
	public static QASearchResult sampleQASearchResult()
    {
		QASearchResult qaSearchResult = new QASearchResult();
		qaSearchResult.setQAPicklist(sampleQAPicklistType());
		qaSearchResult.setVerifyLevel(VerifyLevelType.VERIFIED);
        return qaSearchResult;
    }
   
	/*** Get address Response - QAAddressType ***/
	
	public static QAAddressType sampleQAAddressType()
    {
		QAAddressType qaAddressType = new QAAddressType();
		
		AddressLineType addressLineTypeTown = createAddressLineType(AddressLineTypeTown.LABEL, AddressLineTypeTown.LINE, AddressLineTypeTown.LINE_CONTENT_TYPE);
		AddressLineType addressLineTypePostcode = createAddressLineType(AddressLineTypePostcode.LABEL, AddressLineTypePostcode.LINE, AddressLineTypePostcode.LINE_CONTENT_TYPE);
		AddressLineType addressLineTypeAddressLine1 = createAddressLineType(AddressLineTypeAddressLine1.LABEL, AddressLineTypeAddressLine1.LINE, AddressLineTypeAddressLine1.LINE_CONTENT_TYPE);
		AddressLineType addressLineTypeState = createAddressLineType(AddressLineTypeState.LABEL, AddressLineTypeState.LINE, AddressLineTypeState.LINE_CONTENT_TYPE);
		AddressLineType addressLineTypeCountry = createAddressLineType(AddressLineTypeCountry.LABEL, AddressLineTypeCountry.LINE, AddressLineTypeCountry.LINE_CONTENT_TYPE);

		qaAddressType.getAddressLine().add(addressLineTypeTown);
		qaAddressType.getAddressLine().add(addressLineTypePostcode);
		qaAddressType.getAddressLine().add(addressLineTypeAddressLine1);
		qaAddressType.getAddressLine().add(addressLineTypeState);
		qaAddressType.getAddressLine().add(addressLineTypeCountry);
		
		qaAddressType.setDPVStatus(DPVStatusType.DPV_CONFIRMED);
		qaAddressType.setMissingSubPremise(false);
		qaAddressType.setOverflow(false);
		qaAddressType.setTruncated(false);
        return qaAddressType;
    }
	
	public interface AddressLineTypeTown
    {
		 String LABEL = "Town";
		 String LINE = "New York";
		 LineContentType LINE_CONTENT_TYPE = LineContentType.ADDRESS;
    }
	public interface AddressLineTypePostcode
    {
		 String LABEL = "PostalCode";
		 String LINE = "12345";
		 LineContentType LINE_CONTENT_TYPE = LineContentType.ADDRESS;
    }
	public interface AddressLineTypeCountry
    {
		 String LABEL = "CountryIso";
		 String LINE = "US";
		 LineContentType LINE_CONTENT_TYPE = LineContentType.ADDRESS;
    }
	public interface AddressLineTypeState
    {
		 String LABEL = "RegionIso";
		 String LINE = "NY";
		 LineContentType LINE_CONTENT_TYPE = LineContentType.ADDRESS;
    }
	public interface AddressLineTypeAddressLine1
    {
		 String LABEL = "StreetName";
		 String LINE = "123 Main Street";
		 LineContentType LINE_CONTENT_TYPE = LineContentType.ADDRESS;
    }
	
	 public static AddressLineType createAddressLineType(final String label, final String line, final LineContentType lineContent)
	    {
		 AddressLineType addressLineType = new AddressLineType();		 
		 addressLineType.setLabel(label);
		 addressLineType.setLine(line);
		 addressLineType.setLineContent(lineContent);
	     return addressLineType;
	    }
}
