package com.lyonscg.cisavs.experian.avs.executor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.convert.converter.Converter;

import com.hybris.cis.api.avs.model.AvsResult;
import com.hybris.cis.api.model.CisAddress;
import com.lyonscg.cisavs.experian.api.logger.ExperianXMLLogger;
import com.lyonscg.cisavs.experian.avs.utils.CisAvsTestUtil;
import com.qas.ondemand_2011_03.QAPortType;
import com.qas.ondemand_2011_03.QASearch;
import com.qas.ondemand_2011_03.QASearchResult;


/**
 * Unit test for {@link ExperianSearchExecutor}.
 */
@RunWith(MockitoJUnitRunner.class)
public class ExperianSearchExecutorTest
{
    @InjectMocks
    private ExperianSearchExecutor testClass;
    @Mock
    private Converter<CisAddress, QASearch> experianQASearchConverter;	
    @Mock
    private Converter<QASearchResult, AvsResult> qaSearchResultToAvsResultConverter;
    @Mock
    private QAPortType experianServiceClient ;    
    @Mock
    private QASearch qaSearch;
    @Mock
    private AvsResult avsResult;
    @Mock
    private QASearchResult qaSearchResult;   
    @Mock
    private ExperianXMLLogger experianXMLLogger;

    private CisAddress cisAddress = CisAvsTestUtil.sampleCisAddress();

    /**
     * Delegation test for {@link ExperianSearchExecutor#convertRequest(CisAddress)}.
     */
    @Test
    public void convertRequestTest()
    {
       testClass.convertRequest(cisAddress);
        Mockito.verify(experianQASearchConverter).convert(cisAddress);
    }

    /**
     * Delegation test for
     * {@link ExperianSearchExecutor#convertResponse(CisAddress, QASearch, QASearchResult)}.
     */
    @Test
    public void convertResponseTest()
    {
        testClass.convertResponse(cisAddress, qaSearch, qaSearchResult);
        Mockito.verify(qaSearchResultToAvsResultConverter).convert(qaSearchResult);
    }

    /**
     * Delegation test for {@link ExperianSearchExecutor#process(QASearch)}.
     */
    @Test
    public void processTest()
    {
        testClass.process(qaSearch);
        Mockito.verify(experianServiceClient).doSearch(qaSearch);
    }

}