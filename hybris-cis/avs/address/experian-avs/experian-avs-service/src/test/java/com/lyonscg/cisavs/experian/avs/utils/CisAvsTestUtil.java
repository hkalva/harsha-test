package com.lyonscg.cisavs.experian.avs.utils;

import java.util.HashMap;
import java.util.Map;

import com.hybris.cis.api.model.AnnotationHashMap;
import com.hybris.cis.api.model.CisAddress;
import com.qas.ondemand_2011_03.EngineEnumType;
import com.qas.ondemand_2011_03.EngineIntensityType;


/**
 * Test Utility for CisAvs objects.
 *
 * @author lyonscg
 */
public abstract class CisAvsTestUtil
{

	/*** Sample Engine Type ***/

    public interface SampleEngineType
    {
    	Boolean FLATTEN = false;
    	EngineEnumType ENGINE_ENUM = EngineEnumType.VERIFICATION;
    	EngineIntensityType INTENSITY = EngineIntensityType.CLOSE;        
    }

	
    /*** CIS ADDRESS: Sample Address ***/

    public interface SampleAddress
    {
        String LINE1 = "123 Main Street";
        String LINE2 = "Suite 100";
        String LINE3 = "Unit 1";
        String LINE4 = "Apt 2";
        String CITY = "New York";
        String STATE = "New York";
        String ZIP_CODE = "12345";
        String COUNTRY = "USA";
        String MONIKER = "testmoniker";
    }

    public static CisAddress sampleCisAddress()
    {
        CisAddress sampleAddress = createCisAddress(SampleAddress.LINE1, SampleAddress.LINE2, SampleAddress.LINE3,
        		SampleAddress.LINE4, SampleAddress.CITY, SampleAddress.STATE, SampleAddress.ZIP_CODE, SampleAddress.COUNTRY, SampleAddress.MONIKER);
        
        return sampleAddress;
    }

    
    /*** CIS ADDRESS ***/

    public static CisAddress createCisAddress(final String addressLine1, final String addressLine2, final String addressLine3,
            final String addressLine4, final String city, final String state, final String zipCode, final String country, String moniker)
    {
        CisAddress cisAddress = new CisAddress();
        cisAddress.setAddressLine1(addressLine1);
        cisAddress.setAddressLine2(addressLine2);
        cisAddress.setAddressLine3(addressLine3);
        cisAddress.setAddressLine4(addressLine4);
        cisAddress.setCity(city);
        cisAddress.setState(state);
        cisAddress.setZipCode(zipCode);
        cisAddress.setCountry(country);        
        Map<String, String> vendorParametersMap = new HashMap<String, String>();
    	vendorParametersMap.put("moniker", moniker);
        cisAddress.setVendorParameters(new AnnotationHashMap(vendorParametersMap));
        return cisAddress;
    }
}
