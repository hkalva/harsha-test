package com.lyonscg.cisavs.experian.avs.converter.request;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.lyonscg.cisavs.experian.avs.utils.CisAvsTestUtil;
import com.qas.ondemand_2011_03.QASearch;


/**
 * Unit Tests for {@link ExperianQASearchConverter}.
 */
public class ExperianQASearchConverterTest
{
    private ExperianQASearchConverter testClass;

    /**
     * Setup test data.
     */
    @Before
    public void setUp() throws Exception
    {
        testClass = new ExperianQASearchConverter();
    }

    /**
     * Unit test for {@link ExperianQASearchConverter#convert(com.hybris.cis.api.model.com.hybris.cis.api.model)} with com.hybris.cis.api.model
     * as null.
     */
    @Test
    public void convertWithNullSource() throws Exception
    {
        Assert.assertNull(testClass.convert(null));
    }

    /**
     * Positive Unit test for {@link ExperianQASearchConverter#convert(com.hybris.cis.api.model.com.hybris.cis.api.model)}.
     */
    @Test
    public void convertWithSampleSource() throws Exception
    {
    	QASearch qaSearch = testClass.convert(CisAvsTestUtil.sampleCisAddress());

        Assert.assertNotNull(qaSearch);
        Assert.assertEquals(CisAvsTestUtil.SampleAddress.COUNTRY, qaSearch.getCountry());
        Assert.assertEquals(CisAvsTestUtil.SampleEngineType.FLATTEN, qaSearch.getEngine().isFlatten());
        Assert.assertEquals(CisAvsTestUtil.SampleEngineType.INTENSITY, qaSearch.getEngine().getIntensity());
        Assert.assertEquals(CisAvsTestUtil.SampleEngineType.ENGINE_ENUM, qaSearch.getEngine().getValue());
        Assert.assertEquals("Magic Leap", qaSearch.getLayout());
        Assert.assertEquals(CisAvsTestUtil.SampleAddress.LINE1 + ", " + CisAvsTestUtil.SampleAddress.LINE2 + ", " + CisAvsTestUtil.SampleAddress.CITY + ", " + CisAvsTestUtil.SampleAddress.STATE + ", " + CisAvsTestUtil.SampleAddress.ZIP_CODE, qaSearch.getSearch());
    }

}