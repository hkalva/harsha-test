package com.lyonscg.cisavs.experian.avs.converter.response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.hybris.cis.api.model.CisDecision;
import com.hybris.cis.api.model.CisResult;
import com.lyonscg.cisavs.experian.avs.utils.ExperianAvsTestUtil;


/**
 * Unit Tests for {@link QASearchOkToCisResultConverter}.
 * 
 * @author lyonscg
 */
public class QASearchOkToCisResultConverterTest
{
    private QASearchOkToCisResultConverter qaSearchOkToCisResultConverter;

    /**
     * Setup test data.
     */
    @Before
    public void setUp() throws Exception
    {
    	qaSearchOkToCisResultConverter = new QASearchOkToCisResultConverter();
    }

    /**
     * Unit test for
     * {@link QASearchOkToCisResultConverter#convert(com.qas.ondemand_2011_03.QASearchOk)} with
     * QASearchOk as null.
     */
    @Test
    public void convertWithNullSource() throws Exception
    {
        Assert.assertNull(qaSearchOkToCisResultConverter.convert(null));
    }

    /**
     * Unit test for success
     * {@link QASearchOkToCisResultConverter#convert(com.qas.ondemand_2011_03.QASearchOk)}.
     */
    @Test
    public void convertWithSampleSuccessSource() throws Exception
    {
        // Cis Result
        final CisResult cisResult = qaSearchOkToCisResultConverter.convert(ExperianAvsTestUtil.sampleSuccessQASearchOk());

        Assert.assertNotNull(cisResult);
        Assert.assertEquals(CisDecision.ACCEPT, cisResult.getDecision());
        Assert.assertEquals("AVS", cisResult.getVendorId());
        Assert.assertEquals(ExperianAvsTestUtil.QASearchOkSuccess.ERROR_MESSAGE, cisResult.getVendorReasonCode());
        Assert.assertNull(cisResult.getVendorStatusCode());
        Assert.assertNull(cisResult.getVendorResponses());
      
    }
    
    /**
     * Unit test for fail
     * {@link QASearchOkToCisResultConverter#convert(com.qas.ondemand_2011_03.QASearchOk)}.
     */
    @Test
    public void convertWithSampleFailSource() throws Exception
    {
        // Cis Result
        final CisResult cisResult = qaSearchOkToCisResultConverter.convert(ExperianAvsTestUtil.sampleFailQASearchOk());

        Assert.assertNotNull(cisResult);
        Assert.assertEquals(CisDecision.REJECT, cisResult.getDecision());
        Assert.assertEquals("AVS", cisResult.getVendorId());
        Assert.assertEquals(ExperianAvsTestUtil.QASearchOkFail.ERROR_MESSAGE, cisResult.getVendorReasonCode());
        Assert.assertEquals(ExperianAvsTestUtil.QASearchOkFail.ERROR_CODE, cisResult.getVendorStatusCode());
        Assert.assertNotNull(cisResult.getVendorResponses().getEntries());
        Assert.assertEquals(1, cisResult.getVendorResponses().getEntries().size());

    }
}
