/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.lyonscg.cisavs.experian.avs;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hybris.cis.api.avs.model.AvsResult;
import com.hybris.cis.api.avs.service.AddressVerificationService;
import com.hybris.cis.api.model.CisAddress;
import com.hybris.cis.api.model.CisDecision;
import com.lyonscg.cisavs.experian.avs.utils.CisAvsTestUtil;


/**
 * Validates the avs service implementation and integrates with executor.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations =
{ "/experian-avs-service-test-spring.xml" })
public class ExperianAvsServiceImplTest
{

    @Autowired
    private AddressVerificationService avsService;

    private CisAddress cisaddress;

    /**
     * Setup test data.
     */
    @Before
    public void before() throws Exception
    {
        this.cisaddress = CisAvsTestUtil.sampleCisAddress();
    }

    /**
     * Positive Unit test for {@link AddressVerificationService#verifyAddress(CisAddress)}.
     */
    @Test
    public void verifyAddressViaExecutor()
    {
    	AvsResult avsResult = avsService.verifyAddress(this.cisaddress);
    	if(null != avsResult){
    		Assert.assertEquals(CisDecision.ACCEPT, avsResult.getDecision());
            Assert.assertNotNull(avsResult.getVendorId());
    	}
        
    }
    
}
