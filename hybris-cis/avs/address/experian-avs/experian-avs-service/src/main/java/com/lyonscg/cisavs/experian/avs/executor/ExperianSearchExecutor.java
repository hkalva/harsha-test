/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.lyonscg.cisavs.experian.avs.executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.hybris.cis.api.avs.model.AvsResult;
import com.hybris.cis.api.executor.AbstractSimpleServiceMethodExecutor;
import com.hybris.cis.api.model.CisAddress;
import com.lyonscg.cisavs.experian.api.logger.ExperianXMLLogger;
import com.qas.ondemand_2011_03.QAPortType;
import com.qas.ondemand_2011_03.QASearch;
import com.qas.ondemand_2011_03.QASearchResult;


/**
 * AVS Search Executor.
 */
public class ExperianSearchExecutor extends AbstractSimpleServiceMethodExecutor<CisAddress, AvsResult, QASearch, QASearchResult>
{
	
	private Converter<CisAddress, QASearch> experianQASearchConverter;
	private Converter<QASearchResult, AvsResult> qaSearchResultToAvsResultConverter;
	
	private QAPortType experianServiceClient ;
	private ExperianXMLLogger experianXMLLogger;
	private final Logger LOG = LoggerFactory.getLogger(ExperianCanSearchExecutor.class);
	@Override
	protected QASearch convertRequest(final CisAddress cisAddress)
	{      
		return getExperianQASearchConverter().convert(cisAddress);
	}

	@Override
	protected AvsResult convertResponse(final CisAddress cisAddress, final QASearch qaSearch,
			final QASearchResult qaSearchResult)
	{
		
		return getQaSearchResultToAvsResultConverter().convert(qaSearchResult);
	}

	@Override
	protected QASearchResult process(final QASearch qaSearch)
	{
		
		QASearchResult qaSearchResult = null;	
		try{
		getExperianXMLLogger().logXMLRequest(qaSearch);
		qaSearchResult = getExperianServiceClient().doSearch(qaSearch)	;
		getExperianXMLLogger().logXMLResponse(qaSearchResult);
	}
	catch (Exception e){
		 LOG.error(e.getMessage());
	}
        return qaSearchResult;
	}

	/**
	 * @return the experianQASearchConverter
	 */
	public Converter<CisAddress, QASearch> getExperianQASearchConverter() {
		return experianQASearchConverter;
	}

	/**
	 * @param experianQASearchConverter the experianQASearchConverter to set
	 */
	public void setExperianQASearchConverter(Converter<CisAddress, QASearch> experianQASearchConverter) {
		this.experianQASearchConverter = experianQASearchConverter;
	}

	/**
	 * @return the qaSearchResultToAvsResultConverter
	 */
	public Converter<QASearchResult, AvsResult> getQaSearchResultToAvsResultConverter() {
		return qaSearchResultToAvsResultConverter;
	}

	/**
	 * @param qaSearchResultToAvsResultConverter the qaSearchResultToAvsResultConverter to set
	 */
	public void setQaSearchResultToAvsResultConverter(
			Converter<QASearchResult, AvsResult> qaSearchResultToAvsResultConverter) {
		this.qaSearchResultToAvsResultConverter = qaSearchResultToAvsResultConverter;
	}

	/**
	 * @return the experianServiceClient
	 */
	public QAPortType getExperianServiceClient() {
		return experianServiceClient;
	}

	/**
	 * @param experianServiceClient the experianServiceClient to set
	 */
	public void setExperianServiceClient(QAPortType experianServiceClient) {
		this.experianServiceClient = experianServiceClient;
	}

	/**
	 * @return the experianXMLLogger
	 */
	public ExperianXMLLogger getExperianXMLLogger() {
		return experianXMLLogger;
	}

	/**
	 * @param experianXMLLogger the experianXMLLogger to set
	 */
	public void setExperianXMLLogger(ExperianXMLLogger experianXMLLogger) {
		this.experianXMLLogger = experianXMLLogger;
	}

   
}
