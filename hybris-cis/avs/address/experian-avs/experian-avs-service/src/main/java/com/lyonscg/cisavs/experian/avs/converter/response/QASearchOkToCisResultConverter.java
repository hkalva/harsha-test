
package com.lyonscg.cisavs.experian.avs.converter.response;

import java.util.HashMap;
import java.util.Map;

import org.springframework.core.convert.converter.Converter;
import org.springframework.util.CollectionUtils;

import com.hybris.cis.api.model.AnnotationHashMap;
import com.hybris.cis.api.model.CisDecision;
import com.hybris.cis.api.model.CisResult;
import com.qas.ondemand_2011_03.QASearchOk;

/**
 * Response converter for QACanSearch
 * @author lyonscg
 *
 */
public class QASearchOkToCisResultConverter implements Converter<QASearchOk, CisResult>
{
    /**
     * Populates CisResult (Hybris Commerce) from QASearchOk (Experian AVS)
     *
     * @param qaSearchOk
     * @return Converted {@link CisResult}
     */
    @Override
    public CisResult convert(QASearchOk qaSearchOk)
    {
    	CisResult cisResult = null;

        if (qaSearchOk != null)
        {
        	cisResult = new CisResult();
        	// If the search can be performed based on the request parameter passed then set ACCEPT else REJECT
        	cisResult.setDecision(qaSearchOk.isIsOk() ? CisDecision.ACCEPT : CisDecision.REJECT);
        	cisResult.setVendorId("AVS");
        	if(!qaSearchOk.isIsOk()){
        		cisResult.setVendorReasonCode(null != qaSearchOk.getErrorMessage() ? qaSearchOk.getErrorMessage() : "");
            	cisResult.setVendorStatusCode(qaSearchOk.getErrorCode());
            	if(!CollectionUtils.isEmpty(qaSearchOk.getErrorDetail())){
            		Map<String, String> vendorResponsesMap = new HashMap<String, String>();
            		for (int i = 0; i < qaSearchOk.getErrorDetail().size() ;){
            			vendorResponsesMap.put(String.valueOf(i), qaSearchOk.getErrorDetail().get(i));   
            			i++;
					}
                	cisResult.setVendorResponses(new AnnotationHashMap(vendorResponsesMap));
            	}            	
        	}  
        }
        return cisResult;
    }
}