/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.lyonscg.cisavs.experian.avs;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hybris.cis.api.avs.model.AvsResult;
import com.hybris.cis.api.avs.service.AddressVerificationService;
import com.hybris.cis.api.exception.AbstractCisServiceException;
import com.hybris.cis.api.exception.ServiceNotImplementedException;
import com.hybris.cis.api.executor.ServiceMethodRequest;
import com.hybris.cis.api.model.CisAddress;
import com.hybris.cis.api.model.CisDecision;
import com.hybris.cis.api.model.CisResult;
import com.hybris.cis.api.service.CisServiceType;
import com.lyonscg.cisavs.experian.avs.executor.ExperianCanSearchExecutor;
import com.lyonscg.cisavs.experian.avs.executor.ExperianGetAddressExecutor;
import com.lyonscg.cisavs.experian.avs.executor.ExperianSearchExecutor;


/**
 * Sample implementation of {@link AddressVerificationService} for demonstration purposes.
 */
public class ExperianAvsServiceImpl implements AddressVerificationService
{
	 private final Logger LOG = LoggerFactory.getLogger(ExperianAvsServiceImpl.class);
    private ExperianCanSearchExecutor experianCanSearchExecutor;
    private ExperianSearchExecutor experianSearchExecutor;
    private ExperianGetAddressExecutor experianGetAddressExecutor;

	@Override
	public AvsResult verifyAddress(final CisAddress address) throws AbstractCisServiceException
	{
		AvsResult avsResult = null;
		CisResult cisResult = null;
		String moniker = "";

		if (null != address.getVendorParameters() && null != address.getVendorParameters()) {
			moniker = address.getVendorParameters().getMap().get("moniker");
		}
		try{
			cisResult = getExperianCanSearchExecutor().execute(ServiceMethodRequest.create(address));
			if(null != cisResult && CisDecision.ACCEPT.equals(cisResult.getDecision()) && StringUtils.isEmpty(moniker) ){
				avsResult = getExperianSearchExecutor().execute(ServiceMethodRequest.create(address));
			} else if(StringUtils.isNotEmpty(moniker)){
				avsResult = getExperianGetAddressExecutor().execute(ServiceMethodRequest.create(address));
			}
			else {
				LOG.info("The search combinations are not correct. Search cannot be performed for the QACanSearch Request");
			}
		}
		catch(Exception e){
			LOG.info("Error while processing the search request");
		}
		
        return avsResult;
	}

	@Override
	public CisResult testConnection()
	{
		throw new ServiceNotImplementedException( "testConnection" );
	}

	@Override
	public CisServiceType getType()
	{
		return CisServiceType.AVS;
	}

	@Override
	public String getId()
	{
		return "experian-avs";
	}

	/**
	 * @return the experianCanSearchExecutor
	 */
	public ExperianCanSearchExecutor getExperianCanSearchExecutor() {
		return experianCanSearchExecutor;
	}

	/**
	 * @param experianCanSearchExecutor the experianCanSearchExecutor to set
	 */
	public void setExperianCanSearchExecutor(ExperianCanSearchExecutor experianCanSearchExecutor) {
		this.experianCanSearchExecutor = experianCanSearchExecutor;
	}

	/**
	 * @return the experianSearchExecutor
	 */
	public ExperianSearchExecutor getExperianSearchExecutor() {
		return experianSearchExecutor;
	}

	/**
	 * @param experianSearchExecutor the experianSearchExecutor to set
	 */
	public void setExperianSearchExecutor(ExperianSearchExecutor experianSearchExecutor) {
		this.experianSearchExecutor = experianSearchExecutor;
	}

	/**
	 * @return the experianGetAddressExecutor
	 */
	public ExperianGetAddressExecutor getExperianGetAddressExecutor() {
		return experianGetAddressExecutor;
	}

	/**
	 * @param experianGetAddressExecutor the experianGetAddressExecutor to set
	 */
	public void setExperianGetAddressExecutor(ExperianGetAddressExecutor experianGetAddressExecutor) {
		this.experianGetAddressExecutor = experianGetAddressExecutor;
	}

   
}
