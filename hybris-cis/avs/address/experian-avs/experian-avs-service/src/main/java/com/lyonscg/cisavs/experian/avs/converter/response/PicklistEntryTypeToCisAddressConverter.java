
package com.lyonscg.cisavs.experian.avs.converter.response;

import java.util.HashMap;
import java.util.Map;

import org.springframework.core.convert.converter.Converter;

import com.hybris.cis.api.model.AnnotationHashMap;
import com.hybris.cis.api.model.CisAddress;
import com.qas.ondemand_2011_03.PicklistEntryType;

/**
 * Populates CisAddress (Hybris Commerce) from PicklistEntryType (Experian AVS).
 * @author lyonscg
 *
 */
public class PicklistEntryTypeToCisAddressConverter implements Converter<PicklistEntryType, CisAddress>
{
    /**
     * Populates CisAddress (Hybris Commerce) from PicklistEntryType (Experian AVS)
     *
     * @param picklistEntryType
     * @return Converted {@link CisAddress}
     */
    @Override
    public CisAddress convert(PicklistEntryType picklistEntryType)
    {
    	CisAddress cisAddress = null;

        if (picklistEntryType != null)
        {
        	cisAddress = new CisAddress();
        	//  <Picklist>1 Alpine Ave, Staten Island NY</Picklist>
        	cisAddress.setAddressLine1(picklistEntryType.getPicklist());
        	cisAddress.setZipCode(picklistEntryType.getPostcode());
        	if (picklistEntryType.getMoniker() !=null) {
        	    String country = (picklistEntryType.getMoniker().split("[\\\\|\\\\s]+"))[0];
        	    cisAddress.setCountry((country.equalsIgnoreCase("USA") || country.equals(""))? "US" : country);
        	}
        	// setting values for moniker and score in the vendor parameters
        	Map<String, String> vendorParametersMap = new HashMap<String, String>();
        	vendorParametersMap.put("moniker", picklistEntryType.getMoniker());
        	vendorParametersMap.put("score", null != picklistEntryType.getScore() ? picklistEntryType.getScore().toString() : "");
        	
        	cisAddress.setVendorParameters(new AnnotationHashMap(vendorParametersMap));
        	
        }
        return cisAddress;
    }
}