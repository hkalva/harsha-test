
package com.lyonscg.cisavs.experian.avs.converter.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.convert.converter.Converter;

import com.hybris.cis.api.avs.model.AvsResult;
import com.hybris.cis.api.model.AnnotationHashMap;
import com.hybris.cis.api.model.CisAddress;
import com.hybris.cis.api.model.CisDecision;
import com.qas.ondemand_2011_03.AddressLineType;
import com.qas.ondemand_2011_03.LineContentType;
import com.qas.ondemand_2011_03.QAAddressType;

/**
 * Populates AvsResult (Hybris Commerce) from PicklistEntryType (Experian AVS).
 * @author lyonscg
 *
 */
public class QAAddressTypeToAvsResultConverter implements Converter<QAAddressType, AvsResult>
{
    /**
     * Populates AvsResult (Hybris Commerce) from PicklistEntryType (Experian AVS)
     *
     * @param qaAddressType
     * @return Converted {@link AvsResult}
     */
    @Override
    public AvsResult convert(QAAddressType qaAddressType)
    {
    	CisAddress cisAddress = null;
    	AvsResult avsResult = null;
        if (qaAddressType != null)
        {
        	cisAddress = new CisAddress();
        	StringBuilder addressLine1 = new StringBuilder();
        	Boolean flag = false;
        	if(null != qaAddressType.getAddressLine()){        		
        		for(AddressLineType addressLine : qaAddressType.getAddressLine()){
        			if(LineContentType.ADDRESS.equals(addressLine.getLineContent())){
        				switch (addressLine.getLabel())
        				{
							case "BuildingName":
								if(!addressLine.getLine().isEmpty()){
									addressLine1.append(addressLine.getLine());
									addressLine1.append(",");
                                    addressLine1.append(" ");
								}
								break;
							case "StreetName":
								if(!addressLine.getLine().isEmpty()) {
									addressLine1.append(addressLine.getLine());
									cisAddress.setAddressLine1(addressLine1.toString());
								}
								break;
							case "Secondary number":
								if(!addressLine.getLine().isEmpty()) {
									cisAddress.setAddressLine2(addressLine.getLine());
								}
								break;
        					case "Town":
        						cisAddress.setCity(addressLine.getLine());
        						break;
        					case "PostalCode":
        						cisAddress.setZipCode(addressLine.getLine());
        						break;
							case "RegionIso":
								cisAddress.setState(addressLine.getLine());
								break;
        					case "CountryIso":
        					    cisAddress.setCountry(addressLine.getLine());
        						break;
                        }
        			}        		
        		}            		
        	}        	
        	// setting values for vendor parameters 
        	Map<String, String> vendorParametersMap = new HashMap<String, String>();        	
        	vendorParametersMap.put("overflow", String.valueOf(qaAddressType.isOverflow()));
        	vendorParametersMap.put("truncated", String.valueOf(qaAddressType.isTruncated()));
        	vendorParametersMap.put("dpvStatus", qaAddressType.getDPVStatus().toString());
        	vendorParametersMap.put("missingSubPremise", String.valueOf(qaAddressType.isMissingSubPremise())); 
        	cisAddress.setVendorParameters(new AnnotationHashMap(vendorParametersMap)); 
        	
        	List<CisAddress> suggestedaddresses = new ArrayList<CisAddress>();
        	suggestedaddresses.add(cisAddress);
        	avsResult = new AvsResult();
        	avsResult.setDecision(CisDecision.ACCEPT);
        	avsResult.setSuggestedAddresses(suggestedaddresses);
        	avsResult.setVendorId("AVS");
        	// TODO:        	
        	//avsResult.setClientRefId(clientRefId);       	

        }
        return avsResult;
    }
}