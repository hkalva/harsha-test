/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.lyonscg.cisavs.experian.avs.executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.hybris.cis.api.executor.AbstractSimpleServiceMethodExecutor;
import com.hybris.cis.api.model.CisAddress;
import com.hybris.cis.api.model.CisResult;
import com.lyonscg.cisavs.experian.api.logger.ExperianXMLLogger;
import com.qas.ondemand_2011_03.QACanSearch;
import com.qas.ondemand_2011_03.QAPortType;
import com.qas.ondemand_2011_03.QASearchOk;


/**
 * Sample AVS Executor.
 */
public class ExperianCanSearchExecutor extends AbstractSimpleServiceMethodExecutor<CisAddress, CisResult, QACanSearch, QASearchOk>
{
	
	private Converter<CisAddress, QACanSearch> experianQACanSearchConverter;
	private Converter<QASearchOk, CisResult> qaSearchOkToCisResultConverter;
	
	private QAPortType experianServiceClient ;
	private ExperianXMLLogger experianXMLLogger;
	private final Logger LOG = LoggerFactory.getLogger(ExperianCanSearchExecutor.class);
	
	@Override
	protected QACanSearch convertRequest(final CisAddress cisAddress)
	{      
		return getExperianQACanSearchConverter().convert(cisAddress);
	}

	@Override
	protected CisResult convertResponse(final CisAddress cisAddress, final QACanSearch qaCanSearch,
			final QASearchOk qaSearchOk)
	{
		return getQaSearchOkToCisResultConverter().convert(qaSearchOk);
	}

	@Override
	protected QASearchOk process(final QACanSearch qaCanSearch)
	{
		QASearchOk qaSearchOk = null;	
		try{
		getExperianXMLLogger().logXMLRequest(qaCanSearch);		
		qaSearchOk = getExperianServiceClient().doCanSearch(qaCanSearch);	
		}
		catch (Exception e){
			 LOG.error(e.getMessage());
		}
		getExperianXMLLogger().logXMLResponse(qaSearchOk);
        return qaSearchOk;
	}

	/**
	 * @return the experianQACanSearchConverter
	 */
	public Converter<CisAddress, QACanSearch> getExperianQACanSearchConverter() {
		return experianQACanSearchConverter;
	}

	/**
	 * @param experianQACanSearchConverter the experianQACanSearchConverter to set
	 */
	public void setExperianQACanSearchConverter(Converter<CisAddress, QACanSearch> experianQACanSearchConverter) {
		this.experianQACanSearchConverter = experianQACanSearchConverter;
	}

	/**
	 * @return the qaSearchOkToCisResultConverter
	 */
	public Converter<QASearchOk, CisResult> getQaSearchOkToCisResultConverter() {
		return qaSearchOkToCisResultConverter;
	}

	/**
	 * @param qaSearchOkToCisResultConverter the qaSearchOkToCisResultConverter to set
	 */
	public void setQaSearchOkToCisResultConverter(Converter<QASearchOk, CisResult> qaSearchOkToCisResultConverter) {
		this.qaSearchOkToCisResultConverter = qaSearchOkToCisResultConverter;
	}

	/**
	 * @return the experianServiceClient
	 */
	public QAPortType getExperianServiceClient() {
		return experianServiceClient;
	}

	/**
	 * @param experianServiceClient the experianServiceClient to set
	 */
	public void setExperianServiceClient(QAPortType experianServiceClient) {
		this.experianServiceClient = experianServiceClient;
	}

	/**
	 * @return the experianXMLLogger
	 */
	public ExperianXMLLogger getExperianXMLLogger() {
		return experianXMLLogger;
	}

	/**
	 * @param experianXMLLogger the experianXMLLogger to set
	 */
	public void setExperianXMLLogger(ExperianXMLLogger experianXMLLogger) {
		this.experianXMLLogger = experianXMLLogger;
	}
 
   
}
