
package com.lyonscg.cisavs.experian.avs.converter.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.lyonscg.cisavs.experian.avs.executor.ExperianGetAddressExecutor;
import com.qas.ondemand_2011_03.*;
import org.springframework.core.convert.converter.Converter;

import com.hybris.cis.api.avs.model.AvsResult;
import com.hybris.cis.api.model.AnnotationHashMap;
import com.hybris.cis.api.model.CisAddress;
import com.hybris.cis.api.model.CisDecision;

/**
 * Populates AvsResult (Hybris Commerce) from QASearchResult (Experian AVS).
 * @author lyonscg
 *
 */
public class QASearchResultToAvsResultConverter implements Converter<QASearchResult, AvsResult>
{
    /**
     * Populates AvsResult (Hybris Commerce) from QASearchResult (Experian AVS)
     *
     * @param qaSearchResult
     * @return Converted {@link AvsResult}
     */
	private Converter<PicklistEntryType, CisAddress> picklistEntryTypeToCisAddressConverter;

	private Converter<QAAddressType, AvsResult> qAAddressTypeToAvsResultConverter;

	@Override
    public AvsResult convert(QASearchResult qaSearchResult)
    {
    	AvsResult avsResult = null;
    	List<CisAddress> suggestedaddresses = null;

		if (qaSearchResult != null && (null != qaSearchResult.getQAPicklist() || null != qaSearchResult.getQAAddress()))
		{
			avsResult = new AvsResult();
			// get the picklist entries from qaSearchResult
			if (null != qaSearchResult.getQAPicklist()) {
				List<PicklistEntryType> picklistEntries =  qaSearchResult.getQAPicklist().getPicklistEntry();
				if(!org.springframework.util.CollectionUtils.isEmpty(picklistEntries)) {
					suggestedaddresses = new ArrayList<CisAddress>();
					// convert each picklist entry to CisAddress
					for(PicklistEntryType picklistEntry : picklistEntries) {
						suggestedaddresses.add(getPicklistEntryTypeToCisAddressConverter().convert(picklistEntry));
					}
					avsResult.setSuggestedAddresses(suggestedaddresses);
				}
			} else if(null != qaSearchResult.getQAAddress()){
				return qAAddressTypeToAvsResultConverter.convert(qaSearchResult.getQAAddress());
			}
			// set the suggested address in the avsResult to show the suggested addresses on the storefront
			avsResult.setSuggestedAddresses(suggestedaddresses);
			avsResult.setVendorId("AVS");


			Map<String, String> vendorResponsesMap = new HashMap<String, String>();
			vendorResponsesMap.put("fullPicklistMoniker", qaSearchResult.getQAPicklist().getFullPicklistMoniker());
			vendorResponsesMap.put("prompt", qaSearchResult.getQAPicklist().getPrompt());
			vendorResponsesMap.put("total", null != qaSearchResult.getQAPicklist().getTotal() ? qaSearchResult.getQAPicklist().getTotal().toString() : "");
			vendorResponsesMap.put("verifyLevel", qaSearchResult.getVerifyLevel().toString());
			vendorResponsesMap.put("timeout", String.valueOf(qaSearchResult.getQAPicklist().isTimeout()));
			avsResult.setVendorResponses(new AnnotationHashMap(vendorResponsesMap));

			// TODO:
			//Need to manage the errors. This can be set based on the qaSearchResult.qaPicklist.verificationFlags
			//avsResult.setFieldErrors(fieldErrors);
			//avsResult.setClientRefId(clientRefId);
			// Need to manage accept, reject etc. This can be set based on the qaSearchResult.qaPicklist.verifylevel
			avsResult.setDecision(convertToCisDecision(qaSearchResult.getVerifyLevel()));
			//avsResult.setId(id);

		}
		return avsResult;
	}

    /**
     * Convert the vendor specific decision into a CisDecision
     * @param vendorVerifyLevel
	 * 			gives the verification level from the vendor response
     * @return enum valu of corresponding CIS decision
     */
    private CisDecision convertToCisDecision(VerifyLevelType vendorVerifyLevel) {
    	// TODO: Need to confirm these conditions
        if (VerifyLevelType.VERIFIED.equals(vendorVerifyLevel)  || VerifyLevelType.VERIFIED_PLACE.equals(vendorVerifyLevel) || VerifyLevelType.VERIFIED_STREET.equals(vendorVerifyLevel) || VerifyLevelType.STREET_PARTIAL.equals(vendorVerifyLevel))
        {
            return CisDecision.ACCEPT;
        }
        else if (VerifyLevelType.INTERACTION_REQUIRED.equals(vendorVerifyLevel) || VerifyLevelType.PREMISES_PARTIAL.equals(vendorVerifyLevel)  || VerifyLevelType.MULTIPLE.equals(vendorVerifyLevel) )
        {
            return CisDecision.REVIEW;
        }
        else if (VerifyLevelType.NONE.equals(vendorVerifyLevel))
        {
            return CisDecision.REJECT;
        }
        else return CisDecision.ERROR;
    }

	/**
	 * @return the picklistEntryTypeToCisAddressConverter
	 */
	public Converter<PicklistEntryType, CisAddress> getPicklistEntryTypeToCisAddressConverter() {
		return picklistEntryTypeToCisAddressConverter;
	}

	/**
	 * @param picklistEntryTypeToCisAddressConverter the picklistEntryTypeToCisAddressConverter to set
	 */
	public void setPicklistEntryTypeToCisAddressConverter(
			Converter<PicklistEntryType, CisAddress> picklistEntryTypeToCisAddressConverter) {
		this.picklistEntryTypeToCisAddressConverter = picklistEntryTypeToCisAddressConverter;
	}

	public Converter<QAAddressType, AvsResult> getqAAddressTypeToAvsResultConverter() {
		return qAAddressTypeToAvsResultConverter;
	}

	public void setqAAddressTypeToAvsResultConverter(Converter<QAAddressType, AvsResult> qAAddressTypeToAvsResultConverter) {
		this.qAAddressTypeToAvsResultConverter = qAAddressTypeToAvsResultConverter;
	}

}