/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.lyonscg.cisavs.experian.avs.executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import com.hybris.cis.api.avs.model.AvsResult;
import com.hybris.cis.api.executor.AbstractSimpleServiceMethodExecutor;
import com.hybris.cis.api.model.CisAddress;
import com.lyonscg.cisavs.experian.api.logger.ExperianXMLLogger;
import com.qas.ondemand_2011_03.Address;
import com.qas.ondemand_2011_03.QAAddressType;
import com.qas.ondemand_2011_03.QAGetAddress;
import com.qas.ondemand_2011_03.QAPortType;


/**
 * Sample AVS Executor.
 */
public class ExperianGetAddressExecutor extends AbstractSimpleServiceMethodExecutor<CisAddress, AvsResult, QAGetAddress, Address>
{
	
	private Converter<CisAddress, QAGetAddress> experianQAGetAddressConverter;
	private Converter<QAAddressType, AvsResult> qaAddressTypeToAvsResultConverter;
	
	private QAPortType experianServiceClient ;
	
	private ExperianXMLLogger experianXMLLogger;
	private final Logger LOG = LoggerFactory.getLogger(ExperianCanSearchExecutor.class);
	@Override
	protected QAGetAddress convertRequest(final CisAddress cisAddress)
	{      
		return getExperianQAGetAddressConverter().convert(cisAddress);
	}

	@Override
	protected AvsResult convertResponse(final CisAddress cisAddress, final QAGetAddress qaGetAddress,
			final Address address)
	{
		return getQaAddressTypeToAvsResultConverter().convert(address.getQAAddress());
	}

	@Override
	protected Address process(final QAGetAddress qaGetAddress)
	{
		
		Address address = null;		
		try{
		getExperianXMLLogger().logXMLRequest(qaGetAddress);
		address = getExperianServiceClient().doGetAddress(qaGetAddress)	;
		getExperianXMLLogger().logXMLResponse(address);
		}
		catch (Exception e){
			 LOG.error(e.getMessage());
		}
        return address;
	}

	/**
	 * @return the experianQAGetAddressConverter
	 */
	public Converter<CisAddress, QAGetAddress> getExperianQAGetAddressConverter() {
		return experianQAGetAddressConverter;
	}

	/**
	 * @param experianQAGetAddressConverter the experianQAGetAddressConverter to set
	 */
	public void setExperianQAGetAddressConverter(Converter<CisAddress, QAGetAddress> experianQAGetAddressConverter) {
		this.experianQAGetAddressConverter = experianQAGetAddressConverter;
	}

	/**
	 * @return the qaAddressTypeToAvsResultConverter
	 */
	public Converter<QAAddressType, AvsResult> getQaAddressTypeToAvsResultConverter() {
		return qaAddressTypeToAvsResultConverter;
	}

	/**
	 * @param qaAddressTypeToAvsResultConverter the qaAddressTypeToAvsResultConverter to set
	 */
	public void setQaAddressTypeToAvsResultConverter(
			Converter<QAAddressType, AvsResult> qaAddressTypeToAvsResultConverter) {
		this.qaAddressTypeToAvsResultConverter = qaAddressTypeToAvsResultConverter;
	}

	/**
	 * @return the experianServiceClient
	 */
	public QAPortType getExperianServiceClient() {
		return experianServiceClient;
	}

	/**
	 * @param experianServiceClient the experianServiceClient to set
	 */
	public void setExperianServiceClient(QAPortType experianServiceClient) {
		this.experianServiceClient = experianServiceClient;
	}

	/**
	 * @return the experianXMLLogger
	 */
	public ExperianXMLLogger getExperianXMLLogger() {
		return experianXMLLogger;
	}

	/**
	 * @param experianXMLLogger the experianXMLLogger to set
	 */
	public void setExperianXMLLogger(ExperianXMLLogger experianXMLLogger) {
		this.experianXMLLogger = experianXMLLogger;
	}

	

   
}
