package com.lyonscg.cisavs.experian.avs.converter.request;

import org.springframework.core.convert.converter.Converter;

import com.hybris.cis.api.model.CisAddress;
import com.qas.ondemand_2011_03.EngineEnumType;
import com.qas.ondemand_2011_03.EngineIntensityType;
import com.qas.ondemand_2011_03.EngineType;
import com.qas.ondemand_2011_03.PromptSetType;
import com.qas.ondemand_2011_03.QACanSearch;



/**
 * Populates QACanSearch (Experian AVS) from CisAddress (Hybris Commerce).
 *
 * @author lyonscg
 */
public class ExperianQACanSearchConverter implements Converter<CisAddress, QACanSearch>
{
    /**
     * Populates QACanSearch (Experian AVS) from CisAddress (Hybris Commerce)
     *
     * @param cisAddress
     * @return Converted {@link QACanSearch}
     */
    @Override
    public QACanSearch convert(CisAddress cisAddress)
    {
    	QACanSearch qaCanSearch = null;

        if (cisAddress != null)
        {
        	qaCanSearch = new QACanSearch();

            // Line ID
        	qaCanSearch.setCountry(cisAddress.getCountry());
        	EngineType engineType = new EngineType();
        	engineType.setFlatten(false);
        	engineType.setIntensity(EngineIntensityType.CLOSE);        	
        	//engineType.setThreshold(value );
        	// default is 10000 ms
        	engineType.setTimeout(100000);
        	engineType.setValue(EngineEnumType.VERIFICATION);
        	// should be used only with singleline and partner dataset
        	//engineType.setPromptSet(PromptSetType.DEFAULT);
        	qaCanSearch.setEngine(engineType);
        	qaCanSearch.setLayout("Magic Leap");
			if (cisAddress.getCountry().equalsIgnoreCase("US")) {
				qaCanSearch.setCountry("USA");
			} else {
				qaCanSearch.setCountry(cisAddress.getCountry());
			}
        }

        return qaCanSearch;
    }
}
