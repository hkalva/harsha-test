package com.lyonscg.cisavs.experian.avs.converter.request;

import org.springframework.core.convert.converter.Converter;

import com.hybris.cis.api.model.CisAddress;
import com.qas.ondemand_2011_03.QAGetAddress;



/**
 * Populates QAGetAddress (Experian AVS) from CisAddress (Hybris Commerce).
 *
 * @author lyonscg
 */
public class ExperianQAGetAddressConverter implements Converter<CisAddress, QAGetAddress>
{
    /**
     * Populates QAGetAddress (Experian AVS) from CisAddress (Hybris Commerce)
     *
     * @param cisAddress
     * @return Converted {@link QAGetAddress}
     */
    @Override
    public QAGetAddress convert(CisAddress cisAddress)
    {
    	QAGetAddress qaGetAddress = null;
        if (cisAddress != null)
        {
        	qaGetAddress = new QAGetAddress();           
        	qaGetAddress.setLayout("Magic Leap");
        	
        	qaGetAddress.setMoniker(cisAddress.getVendorParameters().getMap().get("moniker"));
        }
        return qaGetAddress;
    }
}
