package com.lyonscg.cisavs.experian.avs.converter.request;

import org.apache.commons.lang.StringUtils;
import org.springframework.core.convert.converter.Converter;

import com.hybris.cis.api.model.CisAddress;
import com.qas.ondemand_2011_03.EngineEnumType;
import com.qas.ondemand_2011_03.EngineIntensityType;
import com.qas.ondemand_2011_03.EngineType;
import com.qas.ondemand_2011_03.PromptSetType;
import com.qas.ondemand_2011_03.QACanSearch;
import com.qas.ondemand_2011_03.QASearch;



/**
 * Populates QASearch (Experian AVS) from CisAddress (Hybris Commerce).
 *
 * @author lyonscg
 */
public class ExperianQASearchConverter implements Converter<CisAddress, QASearch>
{
    /**
     * Populates QASearch (Experian AVS) from CisAddress (Hybris Commerce)
     *
     * @param cisAddress
     * @return Converted {@link QASearch}
     */
    @Override
    public QASearch convert(CisAddress cisAddress)
    {
    	QASearch qaSearch = null;

        if (cisAddress != null)
        {
        	qaSearch = new QASearch();

			if (cisAddress.getCountry().equalsIgnoreCase("US")) {
				qaSearch.setCountry("USA");
			} else {
				qaSearch.setCountry(cisAddress.getCountry());
			}
        	EngineType engineType = new EngineType();
        	engineType.setFlatten(false);
        	engineType.setIntensity(EngineIntensityType.CLOSE);
        	//engineType.setThreshold(value);
        	//engineType.setTimeout(value);
        	engineType.setValue(EngineEnumType.VERIFICATION);
        	//engineType.setPromptSet(PromptSetType.DEFAULT);
        	qaSearch.setEngine(engineType);
        	qaSearch.setLayout("Magic Leap");
        	qaSearch.setFormattedAddressInPicklist(true);
        	// search is the address to be searched, create a comma separated string from address
        	//125 Summer St, Boston, MA, 02110
        	StringBuilder search = new StringBuilder();
        	if(!StringUtils.isEmpty(cisAddress.getAddressLine1())){
        		search.append(cisAddress.getAddressLine1()).append(", ");
        	}
        	if(!StringUtils.isEmpty(cisAddress.getAddressLine2())){
        		search.append(cisAddress.getAddressLine2()).append(", ");
        	}
        	if(!StringUtils.isEmpty(cisAddress.getCity())){
        		search.append(cisAddress.getCity()).append(", ");
        	}
        	if(!StringUtils.isEmpty(cisAddress.getState())){
        		search.append(cisAddress.getState()).append(", ");
        	}
        	if(!StringUtils.isEmpty(cisAddress.getZipCode())){
        		search.append(cisAddress.getZipCode()).append(", ");
        	}
        	if(search.length() > 0){
        		search.setLength(search.length() - 2);     		
        	}
        	qaSearch.setSearch(search.toString());
        }

        return qaSearch;
    }
}
