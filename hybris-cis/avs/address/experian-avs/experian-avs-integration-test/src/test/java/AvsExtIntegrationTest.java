import com.hybris.cis.api.avs.model.AvsResult;
import com.hybris.cis.api.model.CisAddress;
import com.hybris.cis.api.model.CisDecision;
import com.hybris.cis.client.rest.avs.impl.AvsClientImpl;
import com.hybris.commons.client.RestResponse;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/experian-avs-integration-test-spring.xml"})
public class AvsExtIntegrationTest
{
	private static final String CLIENT_REF = "test";

	@Autowired
	private AvsClientImpl avsClient;

	@Value("${hybris.gateway.uri}")
	private String gatewayValue;

	@Test
	public void shouldAcceptAddress()
	{
		final CisAddress address = new CisAddress("1700 Broadway  Fl 26", "10019", "New York", "NY", "US");
		final RestResponse<AvsResult> response = this.avsClient.verifyAddress(CLIENT_REF, address);
		Assert.assertEquals(CisDecision.ACCEPT, response.getResult().getDecision());
	}
}
