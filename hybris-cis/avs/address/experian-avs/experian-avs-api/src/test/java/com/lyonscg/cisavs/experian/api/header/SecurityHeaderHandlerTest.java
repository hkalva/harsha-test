package com.lyonscg.cisavs.experian.api.header;

import java.util.List;
import java.util.Map;

import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.hybris.commons.configuration.ConfigurationProvider;
import com.hybris.commons.configuration.PropertyNotFoundException;


/**
 * Unit Tests for {@link SecurityHeaderHandler}.
 */
public class SecurityHeaderHandlerTest
{
    @InjectMocks
    private SecurityHeaderHandler testClass;
    private String authtokenKey = "authtokenKey";
    private String authtoken = "authtoken";    
    
    @Mock
    private ConfigurationProvider applicationConfiguration;
    @Mock
    private SOAPMessageContext context;
    @Mock
    private SOAPMessage message;
    @Mock
    private SOAPPart part;
    @Mock
    private SOAPEnvelope envelope;
    @Mock
    private SOAPHeader header;
    @Mock
    private  Map<String, List<String>> requestHeaders;   

    /**
     * Setup test data.
     * 
     * @throws SOAPException
     * @throws PropertyNotFoundException
     */
    @Before
    public void setup() throws SOAPException, PropertyNotFoundException
    {
        testClass = new SecurityHeaderHandler();
        MockitoAnnotations.initMocks(this);
        testClass.setAuthtoken(authtokenKey);       
        Mockito.doReturn(authtoken).when(applicationConfiguration).getString(authtokenKey);
        Mockito.doReturn(true).when(context).get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        Mockito.doReturn(message).when(context).getMessage();
        Mockito.doReturn(part).when(message).getSOAPPart();
        Mockito.doReturn(envelope).when(part).getEnvelope();
        Mockito.doReturn(header).when(envelope).getHeader();        
        Mockito.doReturn(requestHeaders).when(context).get(SOAPMessageContext.HTTP_REQUEST_HEADERS);      
    }

    /**
     * Positive Unit test for {@link SecurityHeaderHandler#handleMessage(javax.xml.ws.handler.soap.SOAPMessageContext)}.
     * 
     * @throws SOAPException
     */
    @Test
    public void handleMessageSuccessTest() throws SOAPException
    {
        Assert.assertTrue(testClass.handleMessage(context));
    }

    /**
     * Negative Unit test for {@link SecurityHeaderHandler#handleMessage(javax.xml.ws.handler.soap.SOAPMessageContext)}.
     * 
     * @throws SOAPException
     */
    @Test
    public void handleMessageFailureTest() throws SOAPException
    {
        Mockito.doReturn(false).when(context).get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        Assert.assertFalse(testClass.handleMessage(context));
    }

    /**
     * Unit test for {@link SecurityHeaderHandler#handleMessage(javax.xml.ws.handler.soap.SOAPMessageContext)} on
     * exception.
     * 
     * @throws SOAPException
     */
    @Test
    public void handleMessageExceptionTest() throws SOAPException
    {
        Mockito.doReturn(false).when(context).get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
    }
}
