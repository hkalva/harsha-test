package com.lyonscg.cisavs.experian.api.header;

import java.util.List;

import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.PortInfo;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Unit Tests for {@link HeaderHandlerResolver}.
 */
public class HeaderHandlerResolverTest
{
    @InjectMocks
    private HeaderHandlerResolver testClass;
    @Mock
    private SOAPHandler<SOAPMessageContext> securityHeaderHandler;
    @Mock
    private PortInfo portInfo;

    /**
     * Setup test data.
     */
    @Before
    public void setup()
    {
        testClass = new HeaderHandlerResolver();
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Positive Unit test for {@link HeaderHandlerResolver#getHandlerChain(javax.xml.ws.handler.PortInfo)}.
     */
    @Test
    @SuppressWarnings("rawtypes")
    public void getHandlerChainSuccessTest()
    {
        List<Handler> handlers = testClass.getHandlerChain(portInfo);
        Assert.assertNotNull(handlers);
        Assert.assertFalse(handlers.isEmpty());
        Assert.assertTrue(handlers.size() == 1);
        Assert.assertEquals(securityHeaderHandler, handlers.iterator().next());
    }
}
