package com.lyonscg.cisavs.experian.api.logger.impl;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.hybris.commons.configuration.ConfigurationProvider;
import com.hybris.commons.configuration.ValueWrongTypeException;
import com.lyonscg.cisavs.experian.api.logger.ExperianXMLLogger;
import com.qas.ondemand_2011_03.Address;
import com.qas.ondemand_2011_03.ObjectFactory;
import com.qas.ondemand_2011_03.QAAuthentication;
import com.qas.ondemand_2011_03.QACanSearch;
import com.qas.ondemand_2011_03.QAGetAddress;
import com.qas.ondemand_2011_03.QAQueryHeader;
import com.qas.ondemand_2011_03.QASearch;
import com.qas.ondemand_2011_03.QASearchOk;
import com.qas.ondemand_2011_03.QASearchResult;


/**
 * Default Experian SOAP xml logger implementation.
 * 
 * @author lyonscg
 *
 */
public class ExperianXMLLoggerImpl implements ExperianXMLLogger
{
    private final Logger LOG = LoggerFactory.getLogger(ExperianXMLLoggerImpl.class);
    private static final String JAXB_LOG_PACKAGE = "com.qas.ondemand_2011_03";
    private ConfigurationProvider applicationConfiguration;
    private String logSoapXmlKey, jaxbPackageForLogKey;

    
     
    @Override
	public void logXMLRequest(QACanSearch qaCanSearch) {
    	writeXMLTransport(qaCanSearch, null);
		
	}

	@Override
	public void logXMLRequest(QASearch qaSearch) {
		writeXMLTransport(qaSearch, null);
		
	}

	@Override
	public void logXMLRequest(QAGetAddress qaGetAddress) {
		writeXMLTransport(qaGetAddress, null);
		
	}

	@Override
	public void logXMLResponse(QASearchOk qaSearchOk) {
		 writeXMLTransport(null, qaSearchOk);
		
	}

	@Override
	public void logXMLResponse(QASearchResult qaSearchResult) {
		 writeXMLTransport(null, qaSearchResult);
		
	}

	@Override
	public void logXMLResponse(Address address) {
		 writeXMLTransport(null, address);
		
	}


    private void writeXMLTransport(QACanSearch qaCanSearch, QASearchOk qaSearchOk)
    {
        boolean logXml = isXmlLoggingEnabled(getLogSoapXmlKey());
        if (!logXml)
        {
            return;
        }
        try
        {
            JAXBElement<?> transport = null;
            if (qaCanSearch == null)
            {
               // transport = new ObjectFactory().createQASearchOk(qaSearchOk);   
            	transport = new ObjectFactory().createQAAuthentication(new QAAuthentication());
                
            }
            else
            {
               //transport = new ObjectFactory().createQACanSearch(qaCanSearch);  
            	transport = new ObjectFactory().createQAAuthentication(new QAAuthentication());
            }
            final StringWriter transportWriter = new StringWriter();
            Marshaller marshaller = JAXBContext
                    .newInstance(getApplicationConfiguration().getString(getJaxbPackageForLogKey(), JAXB_LOG_PACKAGE))
                    .createMarshaller();
            
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(transport, transportWriter);
            if (qaCanSearch == null)
            {
               // LOG.info("Experian response");
            }
            else
            {
              // LOG.info("Experian Request");
            }
            LOG.info(transportWriter.toString());
            transportWriter.close();
        }
        catch (Exception e)
        {
            LOG.error("Error writing request to log", e);
        }
    }
    
    private void writeXMLTransport(QASearch qaSearch, QASearchResult qaSearchResult)
    {
        boolean logXml = isXmlLoggingEnabled(getLogSoapXmlKey());
        if (!logXml)
        {
            return;
        }
        try
        {
            JAXBElement<?> transport = null;
            if (qaSearch == null)
            {
                //transport = new ObjectFactory().createQASearchResult(qaSearchResult);    
            	transport = new ObjectFactory().createQAAuthentication(new QAAuthentication());
            }
            else
            {
               // transport = new ObjectFactory().createQASearch(qaSearch);
            	transport = new ObjectFactory().createQAAuthentication(new QAAuthentication());
            }
            final StringWriter transportWriter = new StringWriter();
            Marshaller marshaller = JAXBContext
                    .newInstance(getApplicationConfiguration().getString(getJaxbPackageForLogKey(), JAXB_LOG_PACKAGE))
                    .createMarshaller();
            
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(transport, transportWriter);
            if (qaSearch == null)
            {
                LOG.info("Experian response");
            }
            else
            {
                LOG.info("Experian Request");
            }
            LOG.info(transportWriter.toString());
            transportWriter.close();
        }
        catch (Exception e)
        {
            LOG.error("Error writing request to log", e);
        }
    }
    
    private void writeXMLTransport(QAGetAddress qaGetAddress, Address address)
    {
        boolean logXml = isXmlLoggingEnabled(getLogSoapXmlKey());
        if (!logXml)
        {
            return;
        }
        try
        {
            JAXBElement<?> transport = null;
            if (qaGetAddress == null)
            {
               // transport = new ObjectFactory().createAddress(address);
            	transport = new ObjectFactory().createQAAuthentication(new QAAuthentication());
            }
            else
            {
             //  transport = new ObjectFactory().createQAGetAddress(qaGetAddress);
            	transport = new ObjectFactory().createQAAuthentication(new QAAuthentication());
            }
            final StringWriter transportWriter = new StringWriter();
            Marshaller marshaller = JAXBContext
                    .newInstance(getApplicationConfiguration().getString(getJaxbPackageForLogKey(), JAXB_LOG_PACKAGE))
                    .createMarshaller();
            
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(transport, transportWriter);
            if (qaGetAddress == null)
            {
                LOG.info("Experian response");
            }
            else
            {
                LOG.info("Experian Request");
            }
            LOG.info(transportWriter.toString());
            transportWriter.close();
        }
        catch (Exception e)
        {
            LOG.error("Error writing request to log", e);
        }
    }


    private boolean isXmlLoggingEnabled(String logSoapXml)
    {
        try
        {
            return getApplicationConfiguration().getBoolean(logSoapXml, false);
        }
        catch (ValueWrongTypeException e)
        {
            LOG.error("Error getting xml logging property", e);
        }
        return false;
    }

    public ConfigurationProvider getApplicationConfiguration()
    {
        return applicationConfiguration;
    }

    @Required
    public void setApplicationConfiguration(ConfigurationProvider applicationConfiguration)
    {
        this.applicationConfiguration = applicationConfiguration;
    }

    public String getLogSoapXmlKey()
    {
        return logSoapXmlKey;
    }

    @Required
    public void setLogSoapXmlKey(String logSoapXmlKey)
    {
        this.logSoapXmlKey = logSoapXmlKey;
    }

    public String getJaxbPackageForLogKey()
    {
        return jaxbPackageForLogKey;
    }

    @Required
    public void setJaxbPackageForLogKey(String jaxbPackageForLogKey)
    {
        this.jaxbPackageForLogKey = jaxbPackageForLogKey;
    }

	
}