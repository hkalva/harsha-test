package com.lyonscg.cisavs.experian.api.header;

import java.util.Collections;
import java.util.List;

import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.springframework.beans.factory.annotation.Required;


/**
 * This class is used to add header messages to the SOAP request.
 * 
 * @author lyonscg
 */
public class HeaderHandlerResolver implements HandlerResolver
{

    private SOAPHandler<SOAPMessageContext> securityHeaderHandler;

    /**
     * Returns a list of configured handlers.
     * 
     * @param portInfo
     *            Port information of the service
     * @return {@link List} containing the {@link SOAPHandler}
     */
    @SuppressWarnings("rawtypes")
    public List<Handler> getHandlerChain(PortInfo portInfo)
    {
        return Collections.singletonList(getSecurityHeaderHandler());
    }

    protected SOAPHandler<SOAPMessageContext> getSecurityHeaderHandler()
    {
        return securityHeaderHandler;
    }

    @Required
    public void setSecurityHeaderHandler(SOAPHandler<SOAPMessageContext> securityHeaderHandler)
    {
        this.securityHeaderHandler = securityHeaderHandler;
    }

}
