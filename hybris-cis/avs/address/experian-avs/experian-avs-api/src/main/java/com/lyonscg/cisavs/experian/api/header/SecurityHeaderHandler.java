package com.lyonscg.cisavs.experian.api.header;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.w3c.dom.DOMException;

import com.hybris.commons.configuration.ConfigurationProvider;
import com.hybris.commons.configuration.PropertyNotFoundException;


/**
 * Implementation of {@link SOAPHandler} to add user credentials for Experian SOAP service requests.
 */
public class SecurityHeaderHandler implements SOAPHandler<SOAPMessageContext>
{
    private String authtoken;
    private ConfigurationProvider applicationConfiguration;
    private static final String AUTH_TOKEN = "Auth-Token";
    private static final Logger LOG = LoggerFactory.getLogger(SecurityHeaderHandler.class);

    /**
     * Adds authtoken to the request header to the SOAP request.
     * 
     * @param context
     *            - SOAP message context.
     * @return - returns true if successful.
     */
    @Override
    public boolean handleMessage(SOAPMessageContext context)
    {    	
        boolean outMessageIndicator = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);        
        if (outMessageIndicator)
        {
            try
            {            	
                SOAPEnvelope envelope = context.getMessage().getSOAPPart().getEnvelope();
                SOAPHeader header = envelope.getHeader();
                if (header == null)
                {
                    header = envelope.addHeader();
                }       
                // Adding Auth-Token in the request header
                @SuppressWarnings("unchecked")
				Map<String, List<String>> requestHeaders = (Map<String, List<String>>) context.get(SOAPMessageContext.HTTP_REQUEST_HEADERS);
                if (requestHeaders == null) {
                    requestHeaders = new HashMap<String, List<String>>();
                    context.put(SOAPMessageContext.HTTP_REQUEST_HEADERS, requestHeaders);
                }
                requestHeaders.put(AUTH_TOKEN, Arrays.asList(getApplicationConfiguration().getString(getAuthtoken())));                
                context.getMessage().saveChanges();   
                
                try {
                	LOG.info("Experian request");
					context.getMessage().writeTo(System.out);					
				} catch (IOException e) {
					LOG.error("Error while writing the SOAP request message", e);
					e.printStackTrace();
				}
            }
            catch (DOMException | SOAPException | PropertyNotFoundException  e)
            {
                LOG.error("Error adding security header to request", e);
                return false;
            }
            return true;
        }
        try {
        	LOG.info("Experian response");
			context.getMessage().writeTo(System.out);
		} catch (SOAPException | IOException e) {
			LOG.error("Error while writing the SOAP response message", e);
			e.printStackTrace();
		} 
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean handleFault(SOAPMessageContext context)
    {
    	LOG.info("Fault" + context);
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close(MessageContext context)
    {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<QName> getHeaders()
    {
        return null;
    }
   

    /**
	 * @return the authtoken
	 */
	public String getAuthtoken() {
		return authtoken;
	}

	/**
	 * @param authtoken the authtoken to set
	 */
	public void setAuthtoken(String authtoken) {
		this.authtoken = authtoken;
	}

	protected ConfigurationProvider getApplicationConfiguration()
    {
        return applicationConfiguration;
    }

    @Required
    public void setApplicationConfiguration(ConfigurationProvider applicationConfiguration)
    {
        this.applicationConfiguration = applicationConfiguration;
    }

}