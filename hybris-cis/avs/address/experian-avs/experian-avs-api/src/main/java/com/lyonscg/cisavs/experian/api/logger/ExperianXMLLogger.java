package com.lyonscg.cisavs.experian.api.logger;

import com.qas.ondemand_2011_03.Address;
import com.qas.ondemand_2011_03.QACanSearch;
import com.qas.ondemand_2011_03.QAGetAddress;
import com.qas.ondemand_2011_03.QASearch;
import com.qas.ondemand_2011_03.QASearchOk;
import com.qas.ondemand_2011_03.QASearchResult;


/**
 * Experian SOAP xml logger.
 * 
 * @author lyonscg
 *
 */
public interface ExperianXMLLogger
{
    /**
     * Log XML request.
     * 
     * @param -
     *            {@link QACanSearch}
     */
    void logXMLRequest(QACanSearch qaCanSearch);
    
    /**
     * Log XML request.
     * 
     * @param -
     *            {@link QASearch}
     */
    void logXMLRequest(QASearch qaSearch);
    
    /**
     * Log XML request.
     * 
     * @param -
     *            {@link QACanSearch}
     */
    void logXMLRequest(QAGetAddress qaGetAddress);
    /**
     * Log XML response.
     * 
     * @param -
     *            {@link QASearchOk}
     */
    void logXMLResponse(QASearchOk qaSearchOk);
    
    /**
     * Log XML response.
     * 
     * @param -
     *            {@link QASearchOk}
     */
    void logXMLResponse(QASearchResult qaSearchResult);
    
   
    /**
     * Log XML response.
     * 
     * @param -
     *            {@link QASearchOk}
     */
    void logXMLResponse(Address address);
    
    
}