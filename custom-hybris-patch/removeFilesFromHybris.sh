#!/bin/bash
###############################################
# file: removeFilesFromHybris.sh
#
# This script will read in a data file and delete the files specified in
# the provided data file.  It is integrated with our maven build framework
# and is called during the clean phase.
#
# Usage: ./removeFilesFromHybris.sh filesToRemove.txt
#
# WARNING! This is a dangerous script. Proceed with caution. 
###############################################

#Check that a data file was presented.  This file should be a list of file paths
if [ $# -eq 0 ]; then
    echo "No argument provided. This script requires a data text file."
    exit 1
fi

#Save basedir of config file.
DIR=$(dirname "$1")

while IFS='' read -r file || [[ -n "$file" ]]; do
    if [[ ! "$file" == \#* ]]; then #ignore comments
        if [ ! -z $file ] ; then #ignore whitespace/empty lines
            if [ -f $file ] || [ -d $file ] ; then
                echo "[DELETE] Deleting $file"
		
                cp --parents -r $file $DIR/.deleted #copy file to .deleted folder
                rm -rf $file #remove original file

            else
	       echo "[WARN] Could not remove file $file - invalid path or filename"
            fi
        fi
    fi

done < "$1"

