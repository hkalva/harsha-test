package org.rlpbackoffice.backoffice.labels;

import com.hybris.backoffice.proxy.LabelServiceProxy;

import java.util.Locale;

/**
* Implementation of LabelServiceProxy.
*
*/
public class RlpLabelServiceProxy implements LabelServiceProxy
{
    @Override
    public String getObjectLabel(Object o, Locale locale)
    {
        return "pk";
    }

    @Override
    public String getObjectDescription(Object o)
    {
        return "{empty}";
    }
}
