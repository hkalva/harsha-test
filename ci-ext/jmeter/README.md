Please download the jmeter binary from the below link  
https://jmeter.apache.org/download_jmeter.cgi


 execute the below script and see the results inside of results folder 
``` 
./jmeter.sh -l Test.jtl -Jjmeter.save.saveservice.output_format=csv -n -t /opt/lyonscg/sapcc-rlp/ci-ext/jmeter/BasicJMeterTest/BasicLoadTest-2.0.jmx -e -o /opt/lyonscg/sapcc-rlp/ci-ext/jmeter/results -JSERVER_NAME="rlp.local" -JSERVER_PORT="9002" -JLOOP_COUNT="1" -JTHREADS="2" -JRAMPUP="2" -JSITE_BASE_URL="/rlp/en/USD" -JUSERS=users2.txt -JDURATION=60
```