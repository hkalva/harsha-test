#!/bin/sh
# -----------------------------------------------------------------------------
# Production Jmeter Test runner
#
rm Test.jtl
mkdir -p jmeter/results
rm -rf jmeter/results/*
/home/djoly/jmeter-5.0/bin/jmeter.sh -l Test.jtl -Jjmeter.save.saveservice.output_format=csv -n -t BasicLoadTest-2.0.jmx -e -o /home/djoly/BasicJMeterTest/jmeter/results -JSERVER_NAME="prod.bell-giro-dealersupport.com" -JSERVER_PORT="443" -JTHREADS="50" -JRAMPUP="10" -JSITE_BASE_URL="/dealersupport/actionsports/en/USD" -JUSERS=produsers.txt -JDURATION=14400


