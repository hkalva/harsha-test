#!/bin/bash

##replace CLIENT_NAME, SHORT_CLIENT_NAME and CLIENT_HYBRIS_PROJECT_ID

CUSTOMER_ID=${CUSTOMER_ID:=capgemini}
SHORT_CUSTOMER_ID=${SHORT_CUSTOMER_ID:=cap}
PROJECT_ID=${PROJECT_ID:=sapcc-rlp}
ENVIRONMENT=${ENVIRONMENT:=dev}
HYBRIS_HOME=${HYBRIS_HOME:=/opt/hybris}
ARTIFACTS_DIR=${ARTIFACTS_DIR:=/NFS_DATA/deployment}
BASE_CONFIG_DIR=${BASE_CONFIG_DIR:=/NFS_DATA/config/hybris-config}
SKIP_ANT_CLEAN=${SKIP_ANT_CLEAN:=false}
FILE_PREFIX=${CUSTOMER_ID}-${PROJECT_ID}
 
 
cd $ARTIFACTS_DIR
zip_file_name=$(ls -rt ${FILE_PREFIX}*.zip | tail -n 1)
package_name="${zip_file_name%.*}"
 
unzip $package_name.zip -d .
 
cd /opt/$SHORT_CUSTOMER_ID
mkdir hybris_$package_name
chmod 755 hybris_$package_name
CURRENT_VERSION_DIR=/opt/$SHORT_CUSTOMER_ID/hybris_$package_name
 
 
cd $CURRENT_VERSION_DIR
cp -r /opt/hybris/data .
ln -s /var/log/hybris log
 
mkdir config
cd config
cp -r $BASE_CONFIG_DIR/* .
cp -r -n $ARTIFACTS_DIR/$package_name/hybris/config/$ENVIRONMENT/* .
cat `hostname`.properties hybris.properties customer.properties > local.properties
 
 
cd /opt/tomcat/bin/
./catalina.sh stop
sleep 15s
 
cd /opt/$SHORT_CUSTOMER_ID
rm ${SHORT_CUSTOMER_ID}hybris
ln -s hybris_$package_name ${SHORT_CUSTOMER_ID}hybris
 
cd /opt
rm -rf $HYBRIS_HOME/bin/*
unzip -o $ARTIFACTS_DIR/$package_name/hybris/bin/hybrisServer-Platform.zip
unzip -o $ARTIFACTS_DIR/$package_name/hybris/bin/hybrisServer-AllExtensions.zip
 
sed -i -e 's/#RUN_AS_USER=/RUN_AS_USER=hybris/g' $HYBRIS_HOME/bin/platform/tomcat/bin/wrapper.sh
 
cd $HYBRIS_HOME/bin/platform
source setantenv.sh
if [ $SKIP_ANT_CLEAN == true ]
then
    echo "Executing ant"
    ant
else
    echo "Executing ant clean all"
    ant clean all
fi
 
cd /opt/tomcat/bin/
./catalina.sh start
 
#Cleanup
cd /NFS_DATA
rm -rf deployment/$package_name
