#!/bin/bash

ENVIRONMENT=${ENVIRONMENT:=dev}
ARTIFACTS_DIR=/opt/hybris-deploy/artifacts
BASE_CONFIG_DIR=/opt/hybris-deploy/config/datahub-config
DATAHUB_BASE=/opt/hybris-datahub


##get the latest artifact
cd $ARTIFACTS_DIR
zip_file_name=$(ls -rt ${FILE_PREFIX}*.zip | tail -n 1)
package_name="${zip_file_name%.*}"


##uncompress the latest artifact
unzip $package_name.zip -d .


##stop datahub service
sudo systemctl stop tomcat7


##deploy the datahub web app
cp $ARTIFACTS_DIR/$package_name/datahub/bin/datahub-webapp.war $DATAHUB_BASE/


##copy the encryption key 
cp $ARTIFACTS_DIR/$package_name/datahub/config/${ENVIRONMENT}hosted/dhub-encrypt-key $DATAHUB_BASE/security-dir/


##create the local.properties file
cat $BASE_CONFIG_DIR/`hostname`.properties $BASE_CONFIG_DIR/hybris.properties $ARTIFACTS_DIR/$package_name/datahub/config/${ENVIRONMENT}hosted/customer.properties > $DATAHUB_BASE/config/local.properties


##start datahub service
sudo systemctl stop tomcat7


##cleanup
rm -rf $ARTIFACTS_DIR/$package_name
