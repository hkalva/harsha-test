#!/bin/bash

##replace CLIENT_HYBRIS_PROJECT_ID

PROJECT_ID=${PROJECT_ID:=sapcc-rlp}
ENVIRONMENT=${ENVIRONMENT:=dev}
SKIP_ANT_CLEAN=${SKIP_ANT_CLEAN:=false}
RUN_HYBRIS_UPDATE=${RUN_HYBRIS_UPDATE:=false}
DEPLOY_SCRIPTS_DIR=/opt/hybris-deploy
ARTIFACTS_DIR=$DEPLOY_SCRIPTS_DIR/artifacts
BASE_CONFIG_DIR=/opt/hybris-deploy/config/hybris-config
HYBRIS_BASE=/opt/hybris
HYBRIS_HOME=$HYBRIS_BASE/hybris
BACKUP_BASE=/opt/hybris-backup
BACKUP_DIR=$BACKUP_BASE/$(date +%Y%m%d)_bkup
GLUSTERFS_BASE=/mnt/glusterfs/media


##always restart the service when the script exits
function restartService {
	##start hybris
	sudo systemctl start hybris
}

trap restartService EXIT

##stop hybris service
sudo systemctl stop hybris


##create a backup directory
mkdir -p $BACKUP_DIR


##remove backups older than 10 days
find $BACKUP_BASE/* -type d -ctime +10 | xargs rm -rf

##remove artifacts older than 10 days
find $ARTIFACTS_DIR/* -type f -ctime +10 | xargs rm -rf

##backup current extensions and configurations
cd $HYBRIS_HOME/bin
tar -cf $BACKUP_DIR/extensions.tar custom/
tar -tvf $BACKUP_DIR/extensions.tar
cd $HYBRIS_HOME
tar -cf $BACKUP_DIR/config.tar config/
tar -tvf $BACKUP_DIR/config.tar


##clean out custom extensions
rm -rf $HYBRIS_HOME/bin/custom



##fix for patched extensions.  src is deployed via jar files
rm -rf $HYBRIS_HOME/bin/ext-addon
rm -rf $HYBRIS_HOME/bin/ext-accelerator
rm -rf $HYBRIS_HOME/bin/ext-content
rm -rf $HYBRIS_HOME/bin/ext-cockpit


##get the latest artifact
cd $ARTIFACTS_DIR
zip_file_name=$(ls -rt ${FILE_PREFIX}*.zip | tail -n 1)
package_name="${zip_file_name%.*}"


##uncompress the latest artifact
unzip $package_name.zip -d .


##deploy archive
cd $HYBRIS_BASE
unzip -o $ARTIFACTS_DIR/$package_name/hybris/bin/hybrisServer-AllExtensions.zip
chown -R hybris:hybris $HYBRIS_HOME


##copy the localextensions.xml
cp $ARTIFACTS_DIR/$package_name/hybris/config/${ENVIRONMENT}hosted/localextensions.xml hybris/config/


##copy the local_tenant_junit.properties
cp $ARTIFACTS_DIR/$package_name/hybris/config/${ENVIRONMENT}hosted/local_tenant_junit.properties hybris/config/


##create the local.properties file
cat $BASE_CONFIG_DIR/`hostname`.properties $BASE_CONFIG_DIR/hybris.properties $ARTIFACTS_DIR/$package_name/hybris/config/${ENVIRONMENT}hosted/customer.properties > $HYBRIS_BASE/hybris/config/local.properties


##compile
cd $HYBRIS_HOME/bin/platform
source setantenv.sh
if [ $SKIP_ANT_CLEAN == true ]
then
    echo "Executing ant"
    ant
else
    echo "Executing ant clean all"
    ant clean all
fi


##ant seems to change some perms
chown -R hybris:hybris $HYBRIS_HOME
chown -R hybris:hybris $HYBRIS_HOME/bin/platform/tomcat/work


##copy _ui to fs
if [ -d "$GLUSTERFS_BASE" ]
then
	echo "copying _ui to Gluster FS mount"
	cp -r $HYBRIS_HOME/bin/custom/${PROJECT_ID}storefront/web/webroot/_ui $GLUSTERFS_BASE/
fi


##cleanup
rm -rf $ARTIFACTS_DIR/$package_name


##if clustered environment, we only need to run hybris update in one server
##will rely on Jenkins script to set this flag to true on the server that will run the update
if [ "$RUN_HYBRIS_UPDATE" == true ]
then
	findLatestLogFile() {
		##append update log file to latest hybris console log file
		pathToFile="${HYBRIS_BASE}/hybris/log/tomcat"
		logFilePattern='console*'
		cd ${pathToFile}
		consoleFile=`ls -lrt ${logFilePattern}* | tail -1 | awk -F" " '{print $NF}'`
		if [ ! -z "$consoleFile" ]
		then
			consoleFile="$pathToFile/$consoleFile"
			echo "Latest Hybris log file $consoleFile"
		fi
	}

	
	addToTomcatLog() {
		if [ -f ${consoleFile} ]
		then
			echo "Appending update logs(${updateLogFile}) to ${consoleFile}" >> ${consoleFile}
			cat ${updateLogFile} >> ${consoleFile}
			echo "Update logs added to hybris log file ${consoleFile}"
			rm -f ${updateLogFile}
			rm -f ${tempFile}
		else
			echo "Hybris console file not found. Logs will not be added to hybris log file."
			rm -f ${tempFile}
		fi
	}

	
	updateFailedMessage(){
		echo "There is a problem with the update."
		echo "The build will be marked unstable."
		exit 0
	}

	
	TIME_PATTERN=$(date +%Y%m%d%H%M)
	updateLogFile=${DEPLOY_SCRIPTS_DIR}/update${TIME_PATTERN}.log
	findLatestLogFile;
	cd ${HYBRIS_BASE}/hybris/bin/platform
	source setantenv.sh
	echo "Updating hybris suite....."
	ant updatesystem -Dtenant=master -DconfigFile=${DEPLOY_SCRIPTS_DIR}/updateOptions.json >> ${updateLogFile}
	tempFile=${DEPLOY_SCRIPTS_DIR}/tempFile${TIME_PATTERN}.txt
	startMatchPattern="\[Initialization\] Updating system \.\.\."
	successConditionFile="${DEPLOY_SCRIPTS_DIR}/successPattern.txt"

	
	if [ ! -f ${updateLogFile} ]
	then
		echo "Ant update log file ${updateLogFile} does not exist."
		updateFailedMessage;
	fi
	

	cat ${updateLogFile} |  grep  -n "${startMatchPattern}"
	if [ "$?" != "0" ]
	then
		echo "Start pattern(${startMatchPattern}) not found in ant update log file"
		addToTomcatLog;
		updateFailedMessage;
	fi

	
	##get the line number of the file that matches the pattern
	startLineNumber=`cat ${updateLogFile} |  grep  -n "${startMatchPattern}"  | cut -d":" -f1`

	
	##copy the contents of the file from this line number until the end in a temp file
	tail -n +${startLineNumber} ${updateLogFile} > ${tempFile}
	

	errorLine=`grep -n 'ERROR' ${updateLogFile}`
	if [ "$?" == "0" ]
	then
		echo "**********************************************************************"
			echo "Errors encountered in the update log file - ${updateLogFile}"
		if [ ! -z "${errorLine}" ]
		then
			echo "Error in line number ${errorLine}"
		fi
		echo "Update has errors. Please check the log file for details on the error"
		echo "**********************************************************************"
		addToTomcatLog;
		updateFailedMessage;
	fi
	

	##make newlines the only separator
	IFS=$'\n'
	set -f
	for line in `cat ${successConditionFile}`
	do
		if [ "$line" == "END" ]
		then
			echo "+++++++++++++++++-----------------------------+++++++++++++++++"
			echo "All success conditions are matched in the log file"
			echo "Hybris suite successfully updated"
			echo "+++++++++++++++++-----------------------------+++++++++++++++++"
			addToTomcatLog;
			exit 0
		fi
		echo "******* Looking for $line in the log file *******"		
		grep "${line}" ${tempFile} >/dev/null
		if [ "$?" != "0" ]
		then
			echo "Success pattern(${line}) not found in ant update log file"
			addToTomcatLog;
			updateFailedMessage;
		else
			echo "Pattern found. Finding next success pattern"
		fi

		lineNumber=`grep -n "${line}" ${tempFile}| cut -d":" -f1 | head -1`
		if [ ! -z "${lineNumber}" ]
		then
			tail -n +${lineNumber} ${tempFile} > "temp2.txt"
			mv "temp2.txt" ${tempFile}
			echo "*************************************************"
		else
			echo "EOF"
			addToTomcatLog;
			updateFailedMessage;
		fi
	done
fi

