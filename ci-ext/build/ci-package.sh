#!/bin/bash

##replace CLIENT_NAME and CLIENT_HYBRIS_PROJECT_ID

##get the directory of where the script is located
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

##environment variables. can be overriden in Jenkins job
CUSTOMER_ID=${CUSTOMER_ID:=capgemini}
PROJECT_ID=${PROJECT_ID:=sapcc-rlp}
VERSION_NUMBER=${VERSION_NUMBER:=03.00.00}
WORKSPACE=${SCRIPTPATH}/../..
METADATA_FILENAME=metadata.properties
METADATA_CONTENT=${METADATA_CONTENT:=package_version = 2.3}
CLOUD_ENVIRONMENTS=${CLOUD_ENVIRONMENTS:=dev stag prod}
HOSTED_ENVIRONMENTS=${HOSTED_ENVIRONMENTS:=dev stage prod}
IMPEX_EXT=${IMPEX_EXT:=*.impex}
DATAHUB_INFRA=${DATAHUB_INFRA:=false}
CLOUD_INFRA=${CLOUD_INFRA:=false}
HOSTED_INFRA=${HOSTED_INFRA:=false}


pathtohybris=${WORKSPACE}/hybris-suite/hybris
dirName=${CUSTOMER_ID}-${PROJECT_ID}_v${VERSION_NUMBER}
packagePath=${pathtohybris}/temp/hybris/hybrisServer/${dirName}/${dirName}
datahubPath=${pathtohybris}/temp/hybris/hybrisServer/${dirName}/datahub
mkdir -p ${packagePath}
touch ${packagePath}/${METADATA_FILENAME}
echo ${METADATA_CONTENT} > ${packagePath}/${METADATA_FILENAME}
 

##package hybris
mkdir -p ${packagePath}/hybris/bin
cp ${pathtohybris}/temp/hybris/hybrisServer/hybrisServer-Platform.zip ${packagePath}/hybris/bin/hybrisServer-Platform.zip
cp ${pathtohybris}/temp/hybris/hybrisServer/hybrisServer-AllExtensions.zip ${packagePath}/hybris/bin/hybrisServer-AllExtensions.zip


if [ "${CLOUD_INFRA}" == true ]
then 
	envs=( ${CLOUD_ENVIRONMENTS} )
	for env in "${envs[@]}"
	do
		mkdir -p ${packagePath}/hybris/config/${env}
		cp ${WORKSPACE}/hybris-config/${env}cloud/hybris/customer.properties ${packagePath}/hybris/config/${env}/customer.properties
		cp ${WORKSPACE}/hybris-config/${env}cloud/hybris/customer.adm.properties ${packagePath}/hybris/config/${env}/customer.adm.properties
		cp ${WORKSPACE}/hybris-config/${env}cloud/hybris/customer.app.properties ${packagePath}/hybris/config/${env}/customer.app.properties
		cp ${WORKSPACE}/hybris-config/${env}cloud/hybris/localextensions.xml ${packagePath}/hybris/config/${env}/localextensions.xml
		cp ${WORKSPACE}/hybris-config/${env}cloud/hybris/localextensions.adm.xml ${packagePath}/hybris/config/${env}/localextensions.adm.xml
		cp ${WORKSPACE}/hybris-config/${env}cloud/hybris/localextensions.app.xml ${packagePath}/hybris/config/${env}/localextensions.app.xml
		cp ${WORKSPACE}/hybris-config/${env}cloud/hybris/local_tenant_junit.properties ${packagePath}/hybris/config/${env}/local_tenant_junit.properties
		mkdir -p ${packagePath}/hybris/misc/${env}
   
   
		##include any impexes
		impex_files=(`find ${WORKSPACE}/hybris-config/${env}cloud/impex/ -maxdepth 1 -name "${IMPEX_EXT}"`)
		if [ ${#impex_files[@]} -gt 0 ];
		then
			cp ${WORKSPACE}/hybris-config/${env}cloud/impex/${IMPEX_EXT} ${packagePath}/hybris/misc/${env}/
		fi
	done
fi


if [ "${HOSTED_INFRA}" == true ]
then
	envs=( ${HOSTED_ENVIRONMENTS} )
	for env in "${envs[@]}"
	do
		mkdir -p ${packagePath}/hybris/config/${env}hosted
		cp ${WORKSPACE}/hybris-config/${env}/hybris/customer.properties ${packagePath}/hybris/config/${env}hosted/customer.properties
		cp ${WORKSPACE}/hybris-config/${env}/hybris/localextensions.xml ${packagePath}/hybris/config/${env}hosted/localextensions.xml
		cp ${WORKSPACE}/hybris-config/${env}/hybris/local_tenant_junit.properties ${packagePath}/hybris/config/${env}hosted/local_tenant_junit.properties
		mkdir -p ${packagePath}/hybris/misc/${env}hosted

		
		##include any impexes
		impex_files=(`find ${WORKSPACE}/hybris-config/${env}/impex/ -maxdepth 1 -name "${IMPEX_EXT}"`)
		if [ ${#impex_files[@]} -gt 0 ]; then
			cp ${WORKSPACE}/hybris-config/${env}/impex/${IMPEX_EXT} ${packagePath}/hybris/misc/${env}hosted/
		fi
	done
fi


##package datahub
if [ "${DATAHUB_INFRA}" == true ]
then
	echo "datahub_infra = true" >> ${packagePath}/${METADATA_FILENAME}
	
	##web app should be self-contained ready to work with no external dependencies
	##copy all datahub extensions, custom and ootb to the web app's library
	mkdir -p ${datahubPath}/WEB-INF/lib
	cp ${WORKSPACE}/hybris-datahub/datahub-webapp.war ${datahubPath}/
	cp ${WORKSPACE}/hybris-datahub/extensions/*.jar ${datahubPath}/WEB-INF/lib/
	cd ${datahubPath}
	jar uf datahub-webapp.war WEB-INF/lib
	mkdir -p ${packagePath}/datahub/bin
	cp ${datahubPath}/datahub-webapp.war ${packagePath}/datahub/bin/
  
  
	if [ "${CLOUD_INFRA}" == true ]
	then
		envs=( ${CLOUD_ENVIRONMENTS} )
		for env in "${envs[@]}"
		do
			mkdir -p ${packagePath}/datahub/config/${env}
			cp ${WORKSPACE}/hybris-config/${env}cloud/datahub/customer.properties ${packagePath}/datahub/config/${env}/customer.properties
			cp ${WORKSPACE}/hybris-config/${env}cloud/datahub/dhub-encrypt-key ${packagePath}/datahub/config/${env}/dhub-encrypt-key
		done
	fi
	
  
	if [ "${HOSTED_INFRA}" == true ]
	then
		envs=( ${HOSTED_ENVIRONMENTS} )
		for env in "${envs[@]}"
		do
			mkdir -p ${packagePath}/datahub/config/${env}hosted
			cp ${WORKSPACE}/hybris-config/${env}/datahub/customer.properties ${packagePath}/datahub/config/${env}hosted/customer.properties
			cp ${WORKSPACE}/hybris-config/${env}/datahub/dhub-encrypt-key ${packagePath}/datahub/config/${env}hosted/dhub-encrypt-key
		done
	fi
fi
 
 
##create the SAP package
cd ${packagePath}/../
zip -r ${dirName}.zip ${dirName}
md5sum ${dirName}.zip > ${dirName}.md5


##move the files to the artifacts directory
mkdir -p ${WORKSPACE}/artifacts
mv ${packagePath}/../${dirName}.zip ${WORKSPACE}/artifacts/${dirName}.zip
mv ${packagePath}/../${dirName}.md5 ${WORKSPACE}/artifacts/${dirName}.md5

