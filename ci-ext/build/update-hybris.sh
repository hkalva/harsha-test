HYBRIS_BASE=/opt/hybris
DEPLOY_SCRIPTS_DIR=/opt/hybris-deploy

findLatestLogFile() {
	#Append update log file to latest hybris console log file
	pathToFile="${HYBRIS_BASE}/hybris/log/tomcat"
	logFilePattern='console*'
	cd ${pathToFile}
	consoleFile=`ls -lrt ${logFilePattern}* | tail -1 | awk -F" " '{print $NF}'`
	if [ ! -z "$consoleFile" ]
	then
		consoleFile="$pathToFile/$consoleFile"
		echo "Latest Hybris log file $consoleFile"
	fi
}

addToTomcatLog() {
	if [ -f ${consoleFile} ]
	then
		echo "Appending update logs(${updateLogFile}) to ${consoleFile}" >> ${consoleFile}
		cat ${updateLogFile} >> ${consoleFile}
		echo "Update logs added to hybris log file ${consoleFile}"
		rm -f ${updateLogFile}
		rm -f ${tempFile}
	else
		echo "Hybris console file not found. Logs will not be added to hybris log file."
		rm -f ${tempFile}
	fi
}

updateFailedMessage(){
	echo "There is a problem with the update."
	echo "The build will be marked unstable."
	exit 0
}

TIME_PATTERN=$(date +%Y%m%d%H%M)
updateLogFile=${DEPLOY_SCRIPTS_DIR}/update${TIME_PATTERN}.log
findLatestLogFile;
cd ${HYBRIS_BASE}/hybris/bin/platform
source setantenv.sh
echo "Updating hybris suite....."
ant updatesystem -Dtenant=master -DconfigFile=${DEPLOY_SCRIPTS_DIR}/updateOptions.json >> ${updateLogFile}
############################################################################################################################################
#Below script validates if hybris update & sync is successful.
############################################################################################################################################
tempFile=${DEPLOY_SCRIPTS_DIR}/tempFile${TIME_PATTERN}.txt
startMatchPattern="\[Initialization\] Updating system \.\.\."
successConditionFile="${DEPLOY_SCRIPTS_DIR}/successPattern.txt"

if [ ! -f ${updateLogFile} ]
then
	echo "Ant update log file ${updateLogFile} does not exist."
	updateFailedMessage;
fi

cat ${updateLogFile} |  grep  -n "${startMatchPattern}"
if [ "$?" != "0" ]
then
	echo "Start pattern(${startMatchPattern}) not found in ant update log file"
	addToTomcatLog;
	updateFailedMessage;
fi

#Get the line number of the file that matches the pattern
startLineNumber=`cat ${updateLogFile} |  grep  -n "${startMatchPattern}"  | cut -d":" -f1`

#Copy the contents of the file from this line number until the end in a temp file
tail -n +${startLineNumber} ${updateLogFile} > ${tempFile}

errorLine=`grep -n 'ERROR' ${updateLogFile}`
if [ "$?" == "0" ]
then
	echo "**********************************************************************"
        echo "Errors encountered in the update log file - ${updateLogFile}"
	if [ ! -z "${errorLine}" ]
	then
		echo "Error in line number ${errorLine}"
	fi
	echo "Update has errors. Please check the log file for details on the error"
	echo "**********************************************************************"
	addToTomcatLog;
	updateFailedMessage;
fi

IFS=$'\n'       # make newlines the only separator
set -f
for line in `cat ${successConditionFile}`
do
	if [ "$line" == "END" ]
	then
		echo "+++++++++++++++++-----------------------------+++++++++++++++++"
		echo "All success conditions are matched in the log file"
		echo "Hybris suite successfully updated"
		echo "+++++++++++++++++-----------------------------+++++++++++++++++"
		addToTomcatLog;
		exit 0
	fi
	echo "******* Looking for $line in the log file *******"		
	grep "${line}" ${tempFile} >/dev/null
	if [ "$?" != "0" ]
	then
		echo "Success pattern(${line}) not found in ant update log file"
		addToTomcatLog;
		updateFailedMessage;
	else
		echo "Pattern found. Finding next success pattern"
	fi

	lineNumber=`grep -n "${line}" ${tempFile}| cut -d":" -f1 | head -1`
	if [ ! -z "${lineNumber}" ]
	then
		tail -n +${lineNumber} ${tempFile} > "temp2.txt"
		mv "temp2.txt" ${tempFile}
		echo "*************************************************"
	else
		echo "EOF"
		addToTomcatLog;
		updateFailedMessage;
	fi
done
