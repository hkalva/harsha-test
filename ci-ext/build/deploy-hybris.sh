#!/bin/bash

HYBRIS_BASE=/opt/hybris
HYBRIS_HOME=$HYBRIS_BASE/hybris
BACKUP_DIR=/opt/hybris-backup/$(date +%Y%m%d)_bkup
DEPLOY_SCRIPTS_DIR=/opt/hybris-deploy
BUILD_DIR=${DEPLOY_SCRIPTS_DIR}/hybris-lea-57
NOTIFY_EMAIL="hybrislea@lyonscg.com"


mkdir -p $BACKUP_DIR

#stop server
sudo service hybris stop

#backup current server
cd $HYBRIS_HOME/bin

echo "*** Creating extensions backup archive ***"
# tar -cvf $BACKUP_DIR/extensions.tar --exclude='platform' *
tar -cf $BACKUP_DIR/extensions.tar custom/
tar -tvf $BACKUP_DIR/extensions.tar

echo "*** Creating config backup archive ***"
cd $HYBRIS_HOME
tar -cf $BACKUP_DIR/config.tar config/
tar -tvf $BACKUP_DIR/config.tar

#clean out custom extensions
rm -rf $HYBRIS_HOME/bin/custom

#fix for patched extensions.  src is deployed via jar files
rm -rf $HYBRIS_HOME/bin/ext-addon
rm -rf $HYBRIS_HOME/bin/ext-accelerator
rm -rf $HYBRIS_HOME/bin/ext-content
rm -rf $HYBRIS_HOME/bin/ext-cockpit

#deploy archive
cd $HYBRIS_BASE
unzip -o $BUILD_DIR/hybrisServer-AllExtensions.zip
chown -R hybris:hybris $HYBRIS_HOME

#copy config
unzip -o $BUILD_DIR/hybrisServer-Config.zip hybris/config/local\* hybris/config/tomcat/conf/server.xml
#clustered environments will also have a configuration file for concatenation.
cat $BUILD_DIR/`hostname`.properties hybris/config/local.properties > $HYBRIS_BASE/hybris/config/local.properties.complete
mv -f $HYBRIS_BASE/hybris/config/local.properties.complete $HYBRIS_BASE/hybris/config/local.properties
#Copy the tomcat server.xml file, as it is customized in some environments
mv -f hybris/config/tomcat/conf/server.xml $HYBRIS_BASE/hybris/config/tomcat/conf/server.xml

# compile
cd $HYBRIS_HOME/bin/platform
source setantenv.sh
ant clean all

#ant seems to change some perms
chown -R hybris:hybris $HYBRIS_HOME
chown -R hybris:hybris $HYBRIS_HOME/bin/platform/tomcat/work
