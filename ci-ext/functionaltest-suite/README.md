# RLP Functional Test Suite

The RLP Functional Test Suite is built using the Serenity-BDD Cucumber 4 test framework. The test suite is uses Selenium WebDriver to test website features as a real user would.

### A little about Cucumber
Cucumber is a well-known Behavior-Driven-Development test framework. Cucumber will compile _feature_ files into test step definitions. Step definitions include _Given_, _When_, and _Then_. Unlike junit tests, which are bound to a single test method having an _@Test_ annotation, Cucumber tests execute multiple test step methods:
- Given: Test Preconditions are set up in _@Given_ annotated step methods. Think of these methods like _@Before_ in junit.
- When: User interaction specific to the test takes place in _@When_ annotated step methods.
- Then: Assertions are made in _@Then_ annotated step methods.

Cucumber feature files use what is called the Gherkin syntax to describe a feature's requirements. For more on Cucumber and Gherkin syntax, see:
- https://cucumber.io/docs/cucumber/
- https://cucumber.io/docs/gherkin/

### The test suite directory structure

This is the directory structure of the suite. Note the organization of test directories, such addons, b2b, common, etc.

```
src
  + main
  + test
    + java                               Test runners and supporting code
      + features                         Test Runners and StepDefinitions
        + addons
        + b2b
        + common
          + cart
            + CartFeatureTestSuite.java
            + CartSetpDefinition.java
          + search
      + pages                            Serenity Page Objects
      + responsive                       Responsive Util and Test Runner
      + steps                            Organized Step files
      + util                             Utility classes, such as Preconditions, Assertions, TearDown, and HacClient.
    + resources
      + features                         Feature files
        + addons                         Addon Feature files
          + assistedservicesstorefront
            + asm.feature                ASM feature file
        + b2b                            B2B Feature files
          + portal.feature 
        + common
          + cart.feature                 Common Cart Features
          + search.feature               Common Search features
      + webdriver                        Bundled webdriver binaries
        + linux
        + mac
        + windows
          chromedriver.exe               OS-specific Webdriver binaries
          geckodriver.exe

```


### Adding New Features

Cucumber Gherkin feature files can be found in the ./src/test/resources/features directory. Add your new feature file here. By way of example, here is a search feature marked with the @search tag:

```gherkin
@search
Feature: Search

  Scenario Outline: User types search term into search field
    Given William has logged in using email '<email>' and password '<password>'
    When he types '<term>' in search field
    Then autosuggest results are displayed
  Examples:
  | email                        | password | term  |
  | william.hunter@rustic-hw.com | 12341234 | drill |
```

The @search tag will allow you to run the test suite with your new feature without running existing feature tests. You can run your feature using the tag, like so:

```
mvn clean verify -Dcucumber.options="--tags @search"
```

No tests will actually run if your feature is new. Instead, Cucumber will output a skeleton of given/when/then methods you can copy from the console and into your new *StepDefinition class.

### Addon Feature Tests

Addons tests may live in their own directory. Tag the feature with "@addon" and the name of the addon (e.g. "asm"). For example:


```Gherkin
@asm @addon
Feature: Assisted Services

  Scenario Outline: AS Agent logs in on portal login page using ASM widget
    Given AS agent at login page
    And navigates to ASM URL
    When agent logs in using username '<username>' and password '<password>'
    Then agent is authenticated
    And agent is on homepage
  Examples:
  | username       | password |
  | asagentmanager | 123456   |

```

This will allow the addon test to be excluded if not included on a project, like so:

```
mvn clean compile -Dcucumber.options="--tags ~@asm"
```

Or if excluding multiple addons:

```
mvn clean compile -Dcucumber.options="--tags ~@asm --tags ~@smartedit"
```
### Feature Test Organization

Each .feature file should be associated with a single Test Runner class. For example, the common/cart.feature file is associated with the CartTestSuite test runner:

```java
package features.common.search;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty"}, features="src/test/resources/features/common/search.feature")
public class SearchFeatureTestSuite {
    
}

```

Your StepDefinition file will live along side the test runner class. Your test runner and step definition classes should be in a package matching your feature definition file:

```
./src/test/resources/features/common/cart.feature
./src/test/java/features/common/search/SearchFeatureTestSuite.java
./src/test/java/features/common/search/SearchStepDefinition.java
 ```

The one-to-one mapping between test runners and feature files is not a hard rule, as long as only one test runner can run your feature tests. The one-to-one mapping makes test development faster, because you focus on a single feature file at a time. Also, as a warning, having more than one test runner class configured to execute a feature will result in test-stablity problems and reporting issues. Only one runner should ever execute a particular feature.

### Responsive Tests

This test suite provides a custom ResponsiveRunner Junit runner class. This is a drop in replacement for the CucumberWithSerenity runner that adds support for responsive test modes. The test suite can be run in any one of the configured test modes.

The responsive modes are defined in serenity.conf. The default mode is desktop. This can be changed in the conf file. 

```java
responsive {
    mode = mobile

    desktop {
        width = 1200
        height = 700
    }
    tablet {
        width = 768
        height = 1024
    }
    mobile {
        width = 400
        height = 700
    }
}
```

To run the test suite in a specific responsive mode, include the responsive.mode property:

```
mvn clean verify -Dcucumber.options="--tags @asm" -Dresponsive.mode=mobile
```

Depending on how your responsive UI has been configured, it may necessary to interact with different elements on the page based on the current responsve mode. For this, the following functions can be used to determine the current mode:

```java
ResponsiveUtils.isDesktop();
ResponsiveUtils.isTablet();
ResponsiveUtils.isMobile();

```

### The HAC Client

Included in this test suite is an HAC client. This is an interface to the hybris admin console. The client makes it possible to enforce certain test preconditions, cleanup certain test data you don't want to leave in the database, and perform custom assertions against the database.

#### Running an impex
If you need to setup some data for a particular test, an impex is a flexible means to do it. Put impex files in the resources/setup/impex directory. Impexes should have limited hard-coded data. For example, a customer uid should be provided via the Cucumber feature file. The impex will contain template variables for data found in the impex. For more details, see the function ScriptsUtil#getScript.

#### Running a groovy script
Sometimes, a groovy script may be more appropriate or easier to use than impex. Groovy scripts are templates, that are placed in the resources/setup/groovy directory. As an example, consider a script that removes a customer's carts:

```groovy
//resources/setup/groovy/clearUnsavedCustomerCarts.groovy.tpl

import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.core.model.order.CartModel;

def query = """"
select {pk} from {Cart} where {user} in (
  {{ select {pk} from {Customer} where {uid} = ?customer }}
) and {savetime} is null
"""

SearchResult<CartModel> result = flexibleSearchService.search(query, ['customer' : '${customerUid}'])
result.getResult().each { modelService.remove it }
``` 

Note the template variable, _${customerUid}_. This will be replaced by the template variable value you specify when loading the template.

In the test suite Constants class, add the name of your template. 

```java
public class Constants {

    //...

    /**
     * Templates
     */
    public static class Template {
        
        //...
        
        public final static String CLEAR_UNSAVED_CUSTOMER_CARTS = "clearUnsavedCustomerCarts";
        
        //...
    }
    //...
}
```
The groovy script is now ready to use in a precondition or tear down script. In this example, it will be used in a precondition script.

 ```java

public class Preconditions {

    //...
    
    /**
     * Removes customer's existing unsaved carts.
     *
     * @param customerUid
     * @throws IOException
     */
    public static void ensureCustomerCartIsEmpty(final String customerUid) {
        final Map<String, String> params = new HashMap<>();
        params.put("customerUid", customerUid);
        executeGroovyScript(Constants.Template.CLEAR_UNSAVED_CUSTOMER_CARTS, params, true);
    }
    
    //...
}

```

In this precondition, we want to make sure the test customer has no items in the cart. To accomplish this, we are simply removing any unsaved carts associated with this user.

We execute the script by calling Scripts#executeGroovyScript. This function takes three arguments:

- The name of the groovy script template.
- A HashMap of template variable key/value pairs.
- A commit flag. The commit flag tells hybris to persist any changes made during script execution. 

This function wraps your groovy script with log statements indicating when your script is starting and stopping.

#### Updating Table Data
The HAC Client can be used to run both native SQL queries and Flexible Search queries. Native SQL Queries enable us to update data without the need for impexes or groovy scripts. For simple data changes, this is the preferred method. However, because native SQL updates bypass the hybris service layer, it is imperative that native queries are used cautiously, as hybris service-layer validation will be of no help. Also, you must clear the hybris cache after making your updates.

When updating table data, the DataUtil#updateTable function may be used. This function accepts three arguments:

- The table name
- A HashMap of field/value pairs
- A where filter value

If you need to update a table, it is advised that you add a generic function to update the table. This can then be re-used elsewhere. For a live usage example, see DataUtil#updateUser:

```java
public class DataUtil {
    
    //...
    
    public static void updateUser(final String uid, final Map<String, Object> values) {
        updateTable("users", values, "p_uid = '" + uid + "'");
        HacClient.getInstance().clearCache();
    }
        
    //...
    
}
```

Pay careful attention to the clearCache call. This is necessary when updating data via SQL queries.

#### Flexible Search Queries
If you don't need to make an update to the database, Flexible Search queries are preferred over native queries.

Running FlexibleSearch queries are simple enough. Here's an example:

```java
    final FlexSearchResponse flexQueryResponse = client.executeFlexQuery("select * from {Customer} where {uid} = 'william.hunter@rustic-hw.com'");
    final List<Map<String, String>> flexQueryResults = flexQueryResponse.getResults();
    assert "william.hunter@rustic-hw.com".equals(flexQueryResults.get(0).get("p_email"));
    assert "william.hunter@rustic-hw.com".equals(flexQueryResults.get(0).get("p_uid"));
```

### Custom Assertions

Custom data assertions should be added to the util.Assertions class. These assertions will generally use Flexible Search queries to assert against table data. For example:

```java
public class Assertions {

    //...

    public static void assertCustomerAccountIsLocked(final String uid) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_CUSTOMER_LOGIN_DISABLED_STATUS, tplParams);
        Assert.assertTrue(Boolean.valueOf(res.getResults().get(0).get("p_logindisabled")));
    }
    
    //...

}
```

## Executing the tests
To execute the tests, use mvn verify.

For all tests:

```
$ mvn clean verify
```

For all tests in responsive mode:

```
$ mvn clean verify -Dresponsive.mode=mobile
```

For a single feature tag in mobile responsive mode:

```
$ mvn clean verify -Dcucumber.options="--tags @asm" -Dresponsive.mode=mobile
```

Run all tests but those tagged with @asm:

```
$ mvn clean verify -Dcucumber.options="--tags ~@asm"
```

The test results will be recorded in the `target/site/serenity` directory.

## Serenity Configuration

This test suite uses the `serenity.conf` file in the `src/test/resources` directory to configure test execution options.  

### Webdriver configuration

The WebDriver configuration is managed entirely from this file, as illustrated below:
```java
webdriver {
    driver = chrome
}
headless.mode = true

chrome.switches="""--start-maximized;--test-type;--no-sandbox;--ignore-certificate-errors;
                   --disable-popup-blocking;--disable-default-apps;--disable-extensions-file-access-check;
                   --incognito;--disable-infobars,--disable-gpu"""

```

The project also bundles some of the WebDriver binaries that you need to run Selenium tests in the `src/test/resources/webdriver` directories. These binaries are configured in the `drivers` section of the `serenity.conf` config file:
```json
drivers {
  windows {
    webdriver.chrome.driver = "src/test/resources/webdriver/windows/chromedriver.exe"
    webdriver.gecko.driver = "src/test/resources/webdriver/windows/geckodriver.exe"
  }
  mac {
    webdriver.chrome.driver = "src/test/resources/webdriver/mac/chromedriver"
    webdriver.gecko.driver = "src/test/resources/webdriver/mac/geckodriver"
  }
  linux {
    webdriver.chrome.driver = "src/test/resources/webdriver/linux/chromedriver"
    webdriver.gecko.driver = "src/test/resources/webdriver/linux/geckodriver"
  }
}
```
This configuration means that development machines and build servers do not need to have a particular version of the WebDriver drivers installed for the tests to run correctly.

### Environment-specific configurations
We can also configure environment-specific properties and options, so that the tests can be run in different environments. Here, we configure three environments, __dev__, _staging_ and _prod_, with different starting URLs for each:
```json
environments {
  default {
    webdriver.base.url = "https://duckduckgo.com"
  }
  dev {
    webdriver.base.url = "https://duckduckgo.com/dev"
  }
  staging {
    webdriver.base.url = "https://duckduckgo.com/staging"
  }
  prod {
    webdriver.base.url = "https://duckduckgo.com/prod"
  }
}
```
  
You use the `environment` system property to determine which environment to run against. For example to run the tests in the staging environment, you could run:
```json
$ mvn clean verify -Denvironment=staging
```

See [**this article**](https://johnfergusonsmart.com/environment-specific-configuration-in-serenity-bdd/) for more details about this feature.

## Want to learn more?
For more information about Serenity BDD, you can read the [**Serenity BDD Book**](https://serenity-bdd.github.io/theserenitybook/latest/index.html), the official online Serenity documentation source. Other sources include:
* **[Byte-sized Serenity BDD](https://www.youtube.com/channel/UCav6-dPEUiLbnu-rgpy7_bw/featured)** - tips and tricks about Serenity BDD
* [**Serenity BDD Blog**](https://johnfergusonsmart.com/category/serenity-bdd/) - regular articles about Serenity BDD
* [**The Serenity BDD Dojo**](https://serenitydojo.teachable.com) - Online training on Serenity BDD and on test automation and BDD in general. 
