package features.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class SaveCartPopup extends Modal {
    @FindBy(css = "#saveCartName")
    WebElementFacade saveCartName;

    @FindBy(css = "#saveCartDescription")
    WebElementFacade saveCartDescription;

    @FindBy(css = "#saveCartButton")
    WebElementFacade saveCartButton;

    public void saveCart(String name, String description) {
        saveCartName.typeAndTab(name);
        saveCartDescription.type(description);
        saveCartButton.click();
    }
}
