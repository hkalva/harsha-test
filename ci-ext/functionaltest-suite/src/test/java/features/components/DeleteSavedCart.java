package features.components;

import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import pages.CurrentPage;
import util.Elements;

import java.util.List;
import java.util.Optional;

public class DeleteSavedCart extends Modal {
    CurrentPage currentPage;

    public void confirmDelete() {
        List<WebElementFacade> elementFacadeList = findAll(Elements.DeleteSavedCart.DELETE);
        Optional<WebElementFacade> deleteButton = elementFacadeList.stream()
                .filter(webElementFacade -> webElementFacade.isEnabled() && webElementFacade.isVisible())
                .findFirst();
        if (deleteButton.isPresent()) {
            currentPage.scrollToElement(deleteButton.get());
            deleteButton.get().click();
        } else {
            Assert.assertTrue(deleteButton.isPresent());
        }
    }
}
