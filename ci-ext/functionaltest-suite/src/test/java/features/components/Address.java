package features.components;

import components.Form;
import net.serenitybdd.core.pages.WebElementFacade;
import pages.SAPccPageObject;
import util.Elements;

import java.util.Map;

public class Address extends SAPccPageObject {

    static public final String TITLE = "TITLE";
    static public final String FIRST_NAME = "FIRST_NAME";
    static public final String LAST_NAME = "LAST_NAME";
    static public final String LINE_ONE = "LINE_ONE";
    static public final String LINE_TWO = "LINE_TWO";
    static public final String TOWN = "TOWN";
    static public final String POSTL_CODE = "POSTL_CODE";
    static public final String STATE = "STATE";
    static public final String COUNTRY = "COUNTRY";
    static public final String PHONE_NUMBER = "PHONE_NUMBER";

    public void populateAddress(Map<String, String> address) {
        WebElementFacade form = find(Elements.Address.ADDRESS_FORM);
        Form addressForm = new Form(form);
        addressForm.select(Elements.Address.ADDRESS_COUNTRY, address.get(COUNTRY));
        addressForm.select(Elements.Address.TITLE, address.get(TITLE));
        addressForm.typeAndTab(Elements.Address.FIRST_NAME, address.get(FIRST_NAME));
        addressForm.typeAndTab(Elements.Address.LAST_NAME, address.get(LAST_NAME));
        addressForm.typeAndTab(Elements.Address.LINE1, address.get(LINE_ONE));
        addressForm.typeAndTab(Elements.Address.LINE2, address.get(LINE_TWO));
        addressForm.typeAndTab(Elements.Address.CITY, address.get(TOWN));
        addressForm.select(Elements.Address.REGION, address.get(STATE));
        addressForm.typeAndTab(Elements.Address.POSTAL_CODE, address.get(POSTL_CODE));
        addressForm.typeAndTab(Elements.Address.PHONE, address.get(PHONE_NUMBER));
    }

    public void populateBillingAddress(Map<String, String> address) {
        WebElementFacade form = find(Elements.BillingAddress.ADDRESS_FORM);
        Form addressForm = new Form(form);
        addressForm.select(Elements.BillingAddress.ADDRESS_COUNTRY, address.get(COUNTRY));
        addressForm.select(Elements.BillingAddress.TITLE, address.get(TITLE));
        addressForm.typeAndTab(Elements.BillingAddress.FIRST_NAME, address.get(FIRST_NAME));
        addressForm.typeAndTab(Elements.BillingAddress.LAST_NAME, address.get(LAST_NAME));
        addressForm.typeAndTab(Elements.BillingAddress.LINE1, address.get(LINE_ONE));
        addressForm.typeAndTab(Elements.BillingAddress.LINE2, address.get(LINE_TWO));
        addressForm.typeAndTab(Elements.BillingAddress.CITY, address.get(TOWN));
        addressForm.select(Elements.BillingAddress.REGION, address.get(STATE));
        addressForm.typeAndTab(Elements.BillingAddress.POSTAL_CODE, address.get(POSTL_CODE));
        addressForm.typeAndTab(Elements.BillingAddress.PHONE, address.get(PHONE_NUMBER));
    }

}
