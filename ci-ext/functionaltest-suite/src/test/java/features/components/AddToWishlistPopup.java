package features.components;

import components.Form;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import util.Elements;

public class AddToWishlistPopup extends Modal {

    public boolean isDisplayed(){
       return find(Elements.Wishlist.ADD_WISHLIST_FORM).getRect().height > 0;
    }


    public void saveToNewWishlist(String name, String description) {
        WebElementFacade form = find(Elements.Wishlist.ADD_WISHLIST_FORM);
        Form addToWishlistForm = new Form(form);
        addToWishlistForm.typeAndTab(Elements.Wishlist.NAME, name);
        addToWishlistForm.typeAndTab(Elements.Wishlist.DESCRIPTION, description);
        form.findElement(By.tagName("button")).click();
    }

    public void saveToExistingWishlist(String name) {
        WebElementFacade form = find(Elements.Wishlist.ADD_WISHLIST_FORM);
        Form addToWishlistForm = new Form(form);
        addToWishlistForm.selectByVisbleText(Elements.Wishlist.CODE, name);
        form.findElement(By.tagName("button")).click();
    }
}
