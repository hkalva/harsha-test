package features.components;

import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

import java.util.Optional;

public class DeleteAddressPopup extends Modal {

    public void confirmDelete() {
        Optional<WebElementFacade> link = find(By.cssSelector(".modal-actions")).thenFindAll(By.tagName("a")).stream()
                .filter(element -> element.getAttribute("href").contains("/my-account/remove-address") && element.isClickable())
                .findFirst();
        link.get().click();
    }
}
