package features.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

import java.util.List;

public class DeleteCreditCardPopup extends Modal {
    public void clickDelete() {
        findAll(By.cssSelector(".paymentsDeleteBtn")).stream()
                .filter( button -> ((WebElementFacade) button).isVisible())
                .findFirst().get().click();
    }
}
