package features.components;

import net.serenitybdd.core.pages.WebElementFacade;
import util.Elements;

import java.util.Optional;

public class RestoreSavedCartPopup extends Modal {
    public void selectKeepSavedCart(boolean saveCart) {
        Optional<WebElementFacade> keepSavedCart = getWebElementFacade(Elements.SavedCarts.KEEP_SAVED_CART);
        if (keepSavedCart.isPresent()) {
            WebElementFacade saveCartCheckbox = keepSavedCart.get();
            if ((saveCart && !saveCartCheckbox.isSelected()) || (!saveCart && saveCartCheckbox.isSelected()))
                saveCartCheckbox.click();
        }
    }

    public void restoreCart() {
        Optional<WebElementFacade> restoreCartButton = getWebElementFacade(Elements.SavedCarts.RESTORE_SAVED_CART_BUTTON);
        if (restoreCartButton.isPresent())
            restoreCartButton.get().click();
    }

}
