package features.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import util.Elements;

public class AddToCartPopup extends Modal {

    final String ADDED_TO_CART = "Added to Your Shopping Cart";

    @FindBy(css = ".add-to-cart-item")
    WebElementFacade addToCartItem;

    @FindBy(css = "#addToCartLayer .add-to-cart-button")
    WebElementFacade checkoutButton;

    @FindBy(css = ".headline-text")
    WebElementFacade headlineText;
    
    @FindBy(css = ".js-cartItemDetailGroup")
    WebElementFacade openItemRemoveButton;

    @FindBy(css = ".js-execute-entry-action-button")
    WebElementFacade itemRemoveButton;

    @FindBy(css = ".js-update-entry-quantity-input")
    WebElementFacade itemQtyTextField;



    public WebElementFacade getAddToCartItem() {
        return addToCartItem;
    }

    public void clickCheckoutButton() {
        checkoutButton.click();
    }

    public boolean isDispalyed() {
      return find(Elements.Cart.ADD_TO_CART_LAYER) != null;
    }
    
    public void openItemRemoveButton() {
        openItemRemoveButton.click();
    }
    
    public void clickItemRemoveButton() {
        itemRemoveButton.click();;
    }
    
    public String getItemQuantity() {
        return itemQtyTextField.getTextValue();
    }
    
    public void changeItemQuantity(int qtantity) {
        itemQtyTextField.typeAndEnter(String.valueOf(qtantity));
    }
}
