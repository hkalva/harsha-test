package features.components;

import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.Constants;
import util.Elements;

import java.util.Optional;

public class QuickViewPopup extends Modal {
    public boolean isDisplayed() {
        WebElement element = (new WebDriverWait(getDriver(), 10))
                .until(ExpectedConditions.elementToBeClickable(Elements.QuickView.SELECTOR));
        waitFor(element);
        return find(Elements.QuickView.SELECTOR).isVisible();
    }

    public boolean isProductAddedToCart() {
        WebElement cartHeadLine = (new WebDriverWait(getDriver(), 10))
                .until(ExpectedConditions.elementToBeClickable(Elements.QuickView.ADDED_TO_CART_HEADLINE));
        waitFor(cartHeadLine);
        Optional<WebElementFacade> headlineElement = findAll(Elements.QuickView.ADDED_TO_CART_HEADLINE)
                .stream()
                .filter(element -> element.isVisible())
                .findFirst();
        return headlineElement.isPresent() && headlineElement.get().getTextValue().indexOf(Constants.QuickView.ADDED_TO_CART) > -1;
    }
}
