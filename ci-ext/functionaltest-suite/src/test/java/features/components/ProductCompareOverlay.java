package features.components;

import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import pages.SAPccPageObject;
import util.Elements;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

public class ProductCompareOverlay extends SAPccPageObject {

    public Map<String, String> addToCart() {
        Optional<WebElementFacade> optional = findAll(Elements.ProductCompare.OVERLAY_PRODUCT).stream().findFirst();
        if (optional.isPresent()) {
            WebElementFacade product = optional.get();
            WebElementFacade addToCart = product.then(Elements.ProductCompare.ADD_TO_CART);
            WebElementFacade form = product.find(By.tagName(Elements.FORM));
            String productcode = form.find(By.name(Elements.ProductCompare.PRODUCT_CODE)).getAttribute(Elements.VALUE);
            String productQty = form.find(By.id(Elements.ProductCompare.QUANTITY)).getAttribute(Elements.VALUE);
            if (addToCart != null) {
                addToCart.click();
            }
            return Collections.singletonMap(productcode, productQty);
        }
        return Collections.EMPTY_MAP;
    }
}
