package features.components;

import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import pages.SAPccPageObject;

import java.util.Optional;

public class Header extends SAPccPageObject {

    public int getCurrentCartCount() {
        Optional<WebElementFacade> countElement = findAll(By.className("js-mini-cart-count")).stream().filter(WebElementFacade::isVisible).findFirst();
        if(!countElement.isPresent()) {
            return 0;
        }
        return countElement.get().isVisible() ? Integer.parseInt(countElement.get().getText().split(" ")[0]) : 0;
    }
}
