package features.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import pages.CurrentPage;

import java.util.List;
import java.util.Optional;

public class Modal extends PageObject {

    @FindBy(id = "colorbox")
    WebElementFacade modal;

    @FindBy(id = "cboxLoadedContent")
    WebElementFacade modalContent;

    @FindBy(css = ".modal-actions")
    WebElementFacade modalActions;

    CurrentPage currentPage;

    public WebElementFacade getModalContent() {
        return modalContent;
    }

    public Optional<WebElementFacade> getWebElementFacade(By by) {

        List<WebElementFacade> webElementFacades = findAll(by);
        Optional<WebElementFacade> webElementFacade = webElementFacades.stream()
                .filter(element -> element.isEnabled() && element.isVisible())
                .findFirst();
        if (webElementFacade.isPresent()) {
            currentPage.scrollToElement(webElementFacade.get());
        }
        return webElementFacade;
    }


}
