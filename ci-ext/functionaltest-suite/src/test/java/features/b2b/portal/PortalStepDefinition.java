package features.b2b.portal;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import pages.HomePage;
import pages.LoginPage;
import steps.LoginSteps;
import util.Assertions;
import util.TearDown;

import static util.TestUtil.pause;

public class PortalStepDefinition {

    @Steps
    LoginSteps loginSteps;
    LoginPage loginPage;
    HomePage homePage;

    @Given("John is not authenticated")
    public void john_is_not_authenticated() {
        //Do nothing
    }

    @When("he opens the home page")
    public void he_opens_the_home_page() {
        loginSteps.openHomePage();
    }

    @Then("the login page is shown")
    public void the_login_page_is_shown() {
        Assert.assertTrue(loginPage.onPage());
    }

    @Given("William is not authenticated")
    public void william_is_not_authenticated() {
        loginSteps.openHomePage();
    }

    @Given("is on the login page")
    public void is_on_the_login_page() {
        Assert.assertTrue(loginPage.onPage());
    }

    @When("he submits incorrect email {string} or password {string}")
    public void he_submits_incorrect_credentials_or(String email, String password) {
        loginSteps.submitLoginForm(email, password);
    }

    @Then("he remains on login page")
    public void he_remains_on_login_page() {
        Assert.assertTrue(loginPage.onPage());
    }

    @Then("generic error message {string} is shown")
    public void and_generic_error_message_is_shown(String message) {
        Assert.assertTrue(loginPage.getAlertElement().isVisible());
        Assert.assertTrue(loginPage.getAlertElement().getText().contains(message));
    }

    @When("he submits valid email {string} and password {string}")
    public void he_submits_valid_email_and_password(String email, String password) {
        loginSteps.submitLoginForm(email, password);
    }

    @Then("the home page is shown")
    public void the_home_page_is_shown() {
        Assert.assertTrue(homePage.onPage());
    }

    @When("he submits valid email {string} and invalid {string} {int} times")
    public void he_submits_valid_email_and_invalid_times(String email, String password, Integer failedLogins) {

        for(int i = 0; i < failedLogins.intValue(); i++) {
            loginSteps.submitLoginForm(email, password);
            pause(1);
        }
    }

    @Then("customer {string} account is locked")
    public void then_customer_account_is_locked(String email) {
        Assertions.assertCustomerAccountIsLocked(email);
        TearDown.unlockUserAccount(email);
    }

}
