package features.b2b.portal;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/PortalFeatureTestResults.html"}, features="src/test/resources/features/b2b/portal.feature")
public class PortalFeatureTestSuite {
}
