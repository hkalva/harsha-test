package features.b2b.mycompany.permissionmanagement;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import pages.mycompany.ViewPermissionPage;
import steps.LoginSteps;
import steps.NavigationSteps;
import steps.mycompany.PermissionManagementSteps;
import util.Assertions;
import util.Preconditions;

public class PermissionManagementStepDefinition {

    @Steps
    LoginSteps loginSteps;

    @Steps
    NavigationSteps navigationSteps;

    @Steps
    PermissionManagementSteps permissionManagementSteps;

    ViewPermissionPage viewPermissionPage;

    @Given("commerce org {string} exists")
    public void commerce_org_exists(String commerceOrgUid) {
        Preconditions.ensureCommerceOrgExists(commerceOrgUid);
    }

    @Given("Customer is authenticated using email {string} and password {string}")
    public void customer_is_authenticated_using_email_and_password(String email, String password) {
        loginSteps.openHomePage();
        loginSteps.submitLoginForm(email, password);
    }


    @Given("permission {string} does not exist")
    public void permission_does_not_exist(String permission) {
        Preconditions.b2bPermissionDoesNotExist(permission);
    }

    @Given("permission {string} has unit {string}")
    public void permission_has_unit(String permission, String unit) {
        Preconditions.b2bPermissionHasUnit(permission, unit);
    }

    @Given("permission {string} is enabled")
    public void permission_is_enabled(String permission) {
        Preconditions.b2bPermissionIsEnabled(permission);
    }

    @Given("permission {string} is disabled")
    public void permission_is_disabled(String permission) {
        Preconditions.b2bPermissionIsDisabled(permission);
    }

    @When("customer navigates to manage permissions page")
    public void customer_navigates_to_manage_permissions_page() {
        navigationSteps.navigateToLinkTitle("Permissions");
    }

    @When("navigates to permission {string} view page")
    public void navigates_to_permission_view_page(String permission) {
        permissionManagementSteps.navigateToViewPermissionPage(permission);
    }

    @When("clicks edit permission button")
    public void clicks_edit_permission_button() {
        permissionManagementSteps.clickOnEditPermissionButton();
    }

    @When("clicks add new permission button")
    public void clicks_add_new_permission_button() {
        permissionManagementSteps.clickAddNewPermissionButton();
    }

    @When("submits permission form having type {string}, timespan {string}, currency {string}, value {string}, parent unit {string} and code {string}")
    public void submits_permission_form_having_type_timespan_currency_value_parent_unit_and_code(String type,
             String timespan, String currency, String value, String parentUnit, String code) {
        permissionManagementSteps.selectPermissionType(type);
        permissionManagementSteps.populateNewTimespanPermissionForm(value, parentUnit, timespan, code, currency);
        permissionManagementSteps.submitNewPermissionForm();
    }

    @When("changes unit to {string}")
    public void changes_unit(String unit) {
        permissionManagementSteps.selectsUnit(unit);
    }

    @When("submits permission form")
    public void submits_permission_form() {
        permissionManagementSteps.submitEditPermissionForm();
    }

    @When("clicks disable permission button")
    public void clicks_disable_permission_button() {
        permissionManagementSteps.clickOnDisablePermissionButton();
    }

    @When("clicks enable permission button")
    public void clicks_enable_permission_button() {
        permissionManagementSteps.clickOnEnablePermissionButton();
    }

    @When("clicks on confirm action button")
    public void clicks_on_confirm_action_button() {
        permissionManagementSteps.clickOnConfirmActionButton();
    }

    @Then("permission having code {string} is created")
    public void permission_having_code_is_created(String code) {
        Assertions.assertB2BPermissionExists(code);
    }

    @Then("customer is on view permission page")
    public void customer_is_on_view_permission_page() {
        Assert.assertEquals("Permission Details", viewPermissionPage.getBackLinkLabel());
    }

    @Then("the message {string} is displayed")
    public void the_message_is_displayed(String message) {
        Assert.assertTrue(viewPermissionPage.getAlertElement().isVisible());
        Assert.assertTrue(viewPermissionPage.getAlertElement().getText().contains(message));
    }

    @Then("permission {string} unit is {string}")
    public void permission_unit_is(String permission, String unit) {
        Assertions.assertB2BPermissionUnit(permission, unit);
    }

    @Then("permission {string} is now disabled")
    public void permission_is_now_disabled(String permission) {
        Assertions.assertB2BPermissionDisabled(permission);
    }

    @Then("permission {string} is now enabled")
    public void permission_is_now_enabled(String permission) {
        Assertions.assertB2BPermissionEnabled(permission);
    }

}
