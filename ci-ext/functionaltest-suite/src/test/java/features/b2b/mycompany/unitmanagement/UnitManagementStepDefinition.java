package features.b2b.mycompany.unitmanagement;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import pages.mycompany.unit.UnitDetailPage;
import steps.LoginSteps;
import steps.NavigationSteps;
import steps.mycompany.UnitManagementSteps;
import util.Assertions;
import util.Preconditions;

public class UnitManagementStepDefinition {

    @Steps
    LoginSteps loginSteps;

    @Steps
    NavigationSteps navigationSteps;

    @Steps
    UnitManagementSteps unitManagementSteps;

    UnitDetailPage unitDetailPage;


    @Given("commerce org {string} exists")
    public void commerce_org_exists(String commerceOrgUid) {
        Preconditions.ensureCommerceOrgExists(commerceOrgUid);
    }

    @Given("unit {string} does not exist")
    public void unit_does_not_exist(String unit) {
        Preconditions.b2bUnitDoesNotExist(unit);
    }


    @Given("Customer is authenticated using email {string} and password {string}")
    public void customer_is_authenticated_using_email_and_password(String email, String password) {
        loginSteps.openHomePage();
        loginSteps.submitLoginForm(email, password);
    }

    @Given("unit having id {string}, name {string}, parent unit {string}, and approval process {string}")
    public void unit_having_id_name_parent_unit_and_approval_process(String unit, String name, String parentUnit, String approvalProcess) {
        Preconditions.b2bUnit(unit, name, parentUnit, approvalProcess);
    }

    @Given("address having owner {string}, street name {string}, street number {string}, and postal code {string} does not exist")
    public void address_having_owner_street_name_street_number_and_postal_code_does_not_exist(String owner, String streetName, String streetNumber, String postalCode) {
        Preconditions.addressDoesNotExist(owner, streetName, streetNumber, postalCode);
    }

    @When("customer navigates to units page")
    public void customer_navigates_to_units_page() {
        navigationSteps.navigateToLinkTitle("Units");
    }

    @When("clicks add new unit button")
    public void clicks_add_new_unit_button() {
        unitManagementSteps.clickAddNewUnitButton();
    }

    @When("clicks add new address button")
    public void clicks_add_new_address_button() {
        unitManagementSteps.clickAddNewAddressButton();
    }

    @When("navigates to unit {string} details page")
    public void navigates_to_unit_details_page(String unit) {
        unitManagementSteps.navigateToUnitDetailPage(unit);
    }

    @When("submits user form having id {string}, name {string}, parent unit {string}, and approval process {string}")
    public void submits_user_form_having_id_name_parent_unit_and_approval_process(String unit, String name, String parentUnit, String approvalProcess) {
        unitManagementSteps.populateUnitForm(unit, name, parentUnit, approvalProcess);
        unitManagementSteps.submitUnitForm();
    }

    @When("submits address form having country {string}, title {string} first name {string}, last name {string}, street name {string}, street number {string}, city {string} and postal code {string}")
    public void submits_address_form(String country, String title, String firstName, String lastName, String streetName,
                                     String streetNumber, String city, String postalCode) {
        unitManagementSteps.populateNewAddressForm(country, title, firstName, lastName, streetName, streetNumber, city, postalCode);
        unitManagementSteps.submitNewAddressForm();
    }

    @Then("clicks add edit button")
    public void clicks_edit_button() {
        unitManagementSteps.clickEditButton();
    }

    @Then("unit {string} now exists")
    public void unit_now_exists(String unit) {
        Assertions.assertB2BUnitExists(unit);
    }

    @Then("customer is on manage unit details page")
    public void customer_is_on_manage_unit_details_page() {
        Assert.assertEquals("Unit Details", unitDetailPage.getBackLinkLabel());
    }

    @Then("the message {string} is displayed")
    public void the_message_is_displayed(String message) {
        Assert.assertTrue(unitDetailPage.getAlertElement().isVisible());
        Assert.assertTrue(unitDetailPage.getAlertElement().getText().contains(message));
    }

    @Then("unit {string} now has name {string}, parent unit {string}, and approval process {string}")
    public void unit_now_has_name_parent_unit_and_approval_process(String unit, String name, String parentUnit, String approvalProcess) {
        Assertions.assertB2BUnit(unit, name, parentUnit, approvalProcess);
    }

    @Then("unit {string} has address with country {string}, title {string} first name {string}, last name {string}, street name {string}, street number {string}, city {string} and postal code {string}")
    public void address_having_owner_streetname_streetnumber_city_postalcode_exists(String unit, String country, String title, String firstName, String lastName, String streetName,
                                                                                    String streetNumber, String city, String postalCode) {
        Assertions.assertB2BUnitAddress(unit, country, title, firstName, lastName, streetName, streetNumber, city, postalCode);
    }

}
