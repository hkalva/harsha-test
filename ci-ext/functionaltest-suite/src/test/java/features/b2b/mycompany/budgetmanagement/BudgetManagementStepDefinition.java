package features.b2b.mycompany.budgetmanagement;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import pages.mycompany.budget.ViewBudgetPage;
import steps.LoginSteps;
import steps.NavigationSteps;
import steps.mycompany.BudgetManagementSteps;
import util.Assertions;
import util.Preconditions;

public class BudgetManagementStepDefinition {

    @Steps
    LoginSteps loginSteps;

    @Steps
    NavigationSteps navigationSteps;

    @Steps
    BudgetManagementSteps budgetManagementSteps;

    ViewBudgetPage viewBudgetPage;

    @Given("commerce org {string} exists")
    public void commerce_org_exists(String commerceOrgUid) {
        Preconditions.ensureCommerceOrgExists(commerceOrgUid);
    }

    @Given("Customer is authenticated using email {string} and password {string}")
    public void customer_is_authenticated_using_email_and_password(String email, String password) {
        loginSteps.openHomePage();
        loginSteps.submitLoginForm(email, password);
    }

    @Given("budget {string} is enabled")
    public void budget_is_enabled(String budget) {
        Preconditions.b2bBudgetIsEnabled(budget);
    }

    @Given("budget {string} is disabled")
    public void budget_is_disabled(String budget) {
        Preconditions.b2bBudgetIsDisabled(budget);
    }

    @Given("budget {string} has name {string}")
    public void budget_has_name(String budget, String name) {
        Preconditions.b2bBudgetHasName(budget, name);
    }

    @Given("budget {string} does not exist")
    public void budget_does_not_exist(String budget) {
        Preconditions.b2bBudgetDoesNotExist(budget);
    }

    @When("customer navigates to manage budgets page")
    public void customer_navigates_to_manage_budgets_page() {
        navigationSteps.navigateToLinkTitle("Budgets");
    }

    @When("clicks add new budget button")
    public void clicks_add_new_budget_button() {
        budgetManagementSteps.clickAddNewBudgetButton();
    }

    @When("submits budget form having code {string}, unit {string}, name {string}, currency {string}, amount {string}, start date {string}, and end date {string}")
    public void submits_budget_form_having_code_unit_name_currency_amount_start_date_and_end_date(String code, String unit,
              String name, String currency, String amount, String startDate, String endDate) {
        budgetManagementSteps.populateBudgetForm(code, name, unit, amount, currency, startDate, endDate);
        budgetManagementSteps.submitBudgetForm();
    }

    @When("clicks on budget edit button")
    public void clicks_on_budget_edit_button() {
        budgetManagementSteps.clickOnEditBudgetButton();
    }

    @When("navigates to budget {string} view page")
    public void navigates_to_budget_view_page(String budget) {
        budgetManagementSteps.navigateToViewBudgetPage(budget);
    }

    @When("clicks disable budget button")
    public void clicks_disable_budget_button() {
        budgetManagementSteps.clickOnDisableBudgetButton();
    }

    @When("clicks on confirm action button")
    public void clicks_on_confirm_action_button() {
        budgetManagementSteps.clickOnConfirmActionButton();
    }

    @When("clicks enable budget button")
    public void clicks_enable_budget_button() {
        budgetManagementSteps.clickOnEnableBudgetButton();
    }

    @Then("budget having code {string} is created")
    public void budget_having_code_is_created(String budget) {
        Assertions.assertB2BBudgetExists(budget);
    }

    @Then("customer is on view budget page")
    public void customer_is_on_view_budget_page() {
        Assert.assertEquals("Budget Details", viewBudgetPage.getBackLinkLabel());
    }

    @Then("the message {string} is displayed")
    public void the_message_is_displayed(String message) {
        Assert.assertTrue(viewBudgetPage.getAlertElement().isVisible());
        Assert.assertTrue(viewBudgetPage.getAlertElement().getText().contains(message));
    }

    @Then("budget {string} is now disabled")
    public void budget_is_now_disabled(String budget) {
        Assertions.assertB2BBudgetDisabled(budget);
    }

    @Then("budget {string} is now enabled")
    public void budget_is_now_enabled(String budget) {
        Assertions.assertB2BBudgetEnabled(budget);
    }

    @Then("budget {string} now has name {string}")
    public void budget_now_has_name(String budget, String name) {
        Assertions.assertB2BBudgetName(budget, name);
    }

}
