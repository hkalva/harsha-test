package features.b2b.mycompany.unitmanagement;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/UnitManagementTestResults.html"}, features="src/test/resources/features/b2b/mycompany/unitmanagement.feature")
public class UnitManagementTestSuite {
}
