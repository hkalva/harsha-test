package features.b2b.mycompany.groupmanagement;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import pages.mycompany.group.UserGroupDetailPage;
import steps.LoginSteps;
import steps.NavigationSteps;
import steps.mycompany.GroupManagementSteps;
import util.Assertions;
import util.Preconditions;

import java.util.Arrays;

public class GroupManagementStepDefinition {

    @Steps
    LoginSteps loginSteps;

    @Steps
    NavigationSteps navigationSteps;

    @Steps
    GroupManagementSteps groupManagementSteps;

    UserGroupDetailPage userGroupDetailPage;

    @Given("commerce org {string} exists")
    public void commerce_org_exists(String commerceOrgUid) {
        Preconditions.ensureCommerceOrgExists(commerceOrgUid);
    }

    @Given("Customer is authenticated using email {string} and password {string}")
    public void customer_is_authenticated_using_email_and_password(String email, String password) {
        loginSteps.openHomePage();
        loginSteps.submitLoginForm(email, password);
    }

    @Given("group {string} does not exist")
    public void group_does_not_exist(String uid) {
        Preconditions.b2bUserGroupDoesNotExist(uid);
    }

    @Given("group {string} has members {string}")
    public void group_has_members(String group, String members) {
        Preconditions.b2bUserGroupHasMembers(group, members);
    }

    @Given("group {string} has permissions {string}")
    public void group_has_permissions(String group, String permissions) {
        Preconditions.b2bUserGroupHasPermissions(group, permissions);
    }

    @Given("group having uid {string}, name {string}, and unit {string}")
    public void group_having_uid_name_and(String uid, String name, String unit) {
        Preconditions.b2bUserGroup(uid, name, unit);
    }

    @When("customer navigates to manage groups page")
    public void customer_navigates_to_manage_groups_page() {
        navigationSteps.navigateToLinkTitle("User Groups");
    }

    @When("navigates to group {string} view page")
    public void navigates_to_group_view_page(String group) {
        groupManagementSteps.navigateToViewGroupPage(group);
    }

    @When("clicks add new group button")
    public void clicks_add_new_group_button() {
        groupManagementSteps.clickAddNewGroupButton();
    }

    @When("submits group form having code {string}, unit {string}, and name {string}")
    public void submits_group_form_having_code_unit_and_name(String uid, String unit, String name) {
        groupManagementSteps.populateGroupForm(uid, name, unit);
        groupManagementSteps.submitGroupForm();
    }

    @When("clicks disable group button")
    public void clicks_disable_group_button() {
        groupManagementSteps.clickOnDisableGroupButton();
    }

    @When("clicks on confirm action button")
    public void clicks_on_confirm_action_button() {
        groupManagementSteps.clickOnConfirmActionButton();
    }

    @When("clicks on group edit button")
    public void clicks_on_group_edit_button() {
        groupManagementSteps.clickOnEditGroupButton();
    }

    @When("clicks on done button")
    public void clicks_on_done_button() {
        groupManagementSteps.clickOnDoneButton();
    }

    @When("clicks on add existing user link")
    public void clicks_on_add_existing_user_link() {
        groupManagementSteps.clickOnAddExistingUserLink();
    }

    @When("clicks on add existing permission link")
    public void clicks_on_add_existing_permission_link() {
        groupManagementSteps.clickOnAddExistingPermissionLink();
    }

    @When("toggles members {string}")
    public void toggles_members(String members) {
        groupManagementSteps.toggleUsers(Arrays.asList(members.split(",")));
    }

    @When("toggles permissions {string}")
    public void toggles_permissions(String permissions) {
        groupManagementSteps.togglePermissions(Arrays.asList(permissions.split(",")));
    }

    @When("clicks on group delete button")
    public void clicks_on_group_delete_button() {
        groupManagementSteps.clicksOnGroupDeleteButton();
    }

    @Then("group {string} has name {string}, and unit {string}")
    public void group_has_name_and_unit(String uid, String name, String unit) {
        Assertions.assertB2BUserGroup(uid, name, unit);
    }

    @Then("customer is on group detail page")
    public void customer_is_on_group_detail_page() {
        Assert.assertEquals("User Group Details", userGroupDetailPage.getBackLinkLabel());
    }

    @Then("the message {string} is displayed")
    public void the_message_is_displayed(String message) {
        Assert.assertTrue(userGroupDetailPage.getAlertElement().isVisible());
        Assert.assertTrue(userGroupDetailPage.getAlertElement().getText().contains(message));
    }

    @Then("group {string} is now disabled")
    public void group_is_now_disabled(String group) {
        Assertions.assertB2BUserGroupIsEmpty(group);
    }

    @Then("group {string} now has members {string}")
    public void group_now_has_members(String group, String members) {
        Assertions.assertB2BUserGroupHasUsers(group, Arrays.asList(members.split(",")));
    }

    @Then("group {string} now has permissions {string}")
    public void group_now_has_permissions(String group, String permissions) {
        Assertions.assertB2BUserGroupHasPermissions(group, Arrays.asList(permissions.split(",")));
    }

    @Then("group {string} no longer exists")
    public void group_no_longer_exists(String group) {
        Assertions.assertB2BUserGroupDoesNotExist(group);
    }
}
