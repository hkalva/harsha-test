package features.b2b.mycompany.permissionmanagement;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/PermissionManagementTestResults.html"}, features="src/test/resources/features/b2b/mycompany/permissionmanagement.feature")
public class PermissionManagementTestSuite {
}
