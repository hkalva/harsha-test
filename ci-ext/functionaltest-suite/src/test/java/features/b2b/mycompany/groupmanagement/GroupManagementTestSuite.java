package features.b2b.mycompany.groupmanagement;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/GroupManagementTestResults.html"}, features="src/test/resources/features/b2b/mycompany/groupmanagement.feature")
public class GroupManagementTestSuite {
}
