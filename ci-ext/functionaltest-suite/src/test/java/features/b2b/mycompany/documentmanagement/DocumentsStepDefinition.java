package features.b2b.mycompany.documentmanagement;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import pages.mycompany.AcccountSummaryPage;
import pages.mycompany.costcenter.ViewCostCenterPage;
import steps.LoginSteps;
import steps.NavigationSteps;
import steps.mycompany.CostCenterManagmentSteps;
import util.Assertions;
import util.Preconditions;

import java.util.Arrays;

public class DocumentsStepDefinition {

    @Steps
    NavigationSteps navigationSteps;

    AcccountSummaryPage acccountSummaryPage;

    @Given("Documents exists for {string}")
    public void documents_exists_for( String commerceOrgUid) {
       // Preconditions.ensureB2BDocumentsForCommerceOrgExists(commerceOrgUid);
    }

    @When("customer navigates to cost center {string}")
    public void customer_navigates_to_cost_center_and_then(String costCenter) {
        acccountSummaryPage.selectCostCenter(costCenter);
    }

    @When("customer navigates to account summary page")
    public void customer_navigates_to_account_summary_page() {
        navigationSteps.navigateToLinkTitle("Account Summary");
    }

    @When("selects document status {string} and clicks on search")
    public void selects_document_stauts_and_clicks_on_search(String status) {
       acccountSummaryPage.searchForDocumentsWithStatus(status);
    }

    @Then("All listed documents are in {string} status")
    public void only_documents_in_selected_status_are_shown(String status) {
       Assert.assertFalse("Found docuemnts that are not in " + status , acccountSummaryPage.allDisplayedDocumentsAreHaveStatus(status));
    }

    @When("filter by  document number {string}")
    public void filter_by_document_number(String documentNumber) {
        acccountSummaryPage.filterByDocumentId(documentNumber);
    }

    @Then("listed documents is in {string} status with {string} document number")
    public void listed_documents_is_in_status_with_document_number(String status, String documentNumber) {
        only_documents_in_selected_status_are_shown(status);
        Assert.assertTrue("Docuemnts is no  " + documentNumber , acccountSummaryPage.isDocumentNumber(documentNumber));
    }
}
