package features.b2b.mycompany.costcentermanagement;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/CostCenterManagementTestResults.html"}, features="src/test/resources/features/b2b/mycompany/costcentermanagement.feature")
public class CostCenterManagementTestSuite {
}
