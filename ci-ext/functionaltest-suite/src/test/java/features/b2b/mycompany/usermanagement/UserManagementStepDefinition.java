package features.b2b.mycompany.usermanagement;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import pages.mycompany.ManageUserPage;
import steps.LoginSteps;
import steps.NavigationSteps;
import steps.mycompany.UserManagementSteps;
import util.Assertions;
import util.Preconditions;

import java.util.Arrays;

public class UserManagementStepDefinition {

    @Steps
    LoginSteps loginSteps;

    @Steps
    NavigationSteps navigationSteps;

    @Steps
    UserManagementSteps userManagementSteps;

    ManageUserPage manageUserPage;

    @Given("commerce org {string} exists")
    public void commerce_org_exists(String commerceOrgUid) {
        Preconditions.ensureCommerceOrgExists(commerceOrgUid);
    }

    @Given("customer with {string} does not exist")
    public void customer_with_uid_does_not_exist(String email) {
        Preconditions.b2bCustomerDoesNotExist(email);
    }

    @Given("Customer is authenticated using email {string} and password {string}")
    public void customer_is_authenticated_using_email_and_password(String email, String password) {
        loginSteps.openHomePage();
        loginSteps.submitLoginForm(email, password);
    }

    @When("customer navigates to manage users page")
    public void customer_navigates_to_manage_users_page() {
        navigationSteps.navigateToLinkTitle("Users");
    }

    @When("clicks add new user button")
    public void clicks_add_new_user_button() {
        userManagementSteps.clickAddNewUserButton();
    }

    @When("submits user form having title {string}, first name {string} last name {string}, email {string}, unit {string}, and roles {string}")
    public void submits_new_user_form_having_title_first_name_last_name_email_unit_and_roles(String title, String firstName, String lastName, String email, String unit, String roles) {
        userManagementSteps.populateNewUserForm(title, firstName, lastName, email, unit, roles);
        userManagementSteps.submitNewUserForm();
    }

    @Then("user having {string} is created")
    public void user_having_uid_is_created(String uid) {
        Assertions.assertB2BCustomerExists(uid);
    }

    @Then("customer is on manage user details page")
    public void customer_is_on_manage_user_details_page() {
        Assert.assertEquals("User Details", manageUserPage.getBackLinkLabel());
    }

    @Then("the message {string} is displayed")
    public void the_message_is_displayed(String message) {
        Assert.assertTrue(manageUserPage.getAlertElement().isVisible());
        Assert.assertTrue(manageUserPage.getAlertElement().getText().contains(message));
    }

    @Given("the user {string} is enabled")
    public void the_user_is_enabled(String uid) {
        Preconditions.userIsActive(uid);
    }

    @Given("the user {string} is disabled")
    public void the_user_is_disabled(String uid) {
        Preconditions.userIsInactive(uid);
    }

    @Given("the user {string} has groups {string}")
    public void the_user_has_groups(String uid, String groups) {
        Preconditions.userHasGroups(uid, groups);
    }

    @Given("the user {string} has permissions {string}")
    public void the_user_has_permissions(String uid, String permissions) {
        Preconditions.b2bCustomerHasPermissions(uid, permissions);
    }

    @Given("the user {string} has approvers {string}")
    public void the_user_has_approvers(String uid, String approvers) {
        Preconditions.b2bCustomerHasApprovers(uid, approvers);
    }

    @When("customer navigates to user {string} detail page")
    public void customer_navigates_to_user_edit_page(String uid) {
        userManagementSteps.navigateToUserDetailPage(uid);
    }

    @When("clicks on disable user button")
    public void clicks_on_disable_user_button() {
        userManagementSteps.clickDisableUserButton();
    }

    @When("clicks on enable user button")
    public void clicks_on_enable_user_button() {
        userManagementSteps.clickOnEnableUserButton();
    }

    @When("clicks on disable button on disable confirm button")
    public void clicks_on_disable_button_on_disable_confirm_button() {
        userManagementSteps.clickOnDisableUserConfirmButton();
    }

    @When("clicks on confirm action button")
    public void clicks_on_confirm_action_button() {
        userManagementSteps.clickOnConfirmActionButton();
    }

    @When("clicks on group {string} card's remove button")
    public void clicks_on_group_cards_remove_button(String group) {
        userManagementSteps.clickOnGroupCardRemoveButton(group);
    }

    @When("clicks on permission {string} card's remove button")
    public void clicks_on_permission_cards_remove_button(String permission) {
        userManagementSteps.clickOnPermissionCardRemoveButton(permission);
    }

    @When("clicks on approver {string} card's remove button")
    public void clicks_on_approver_cards_remove_button(String uid) {
        userManagementSteps.clickOnApproverCardRemoveButton(uid);
    }

    @When("clicks user edit button")
    public void clicks_user_edit_button() {
        userManagementSteps.clickOnUserEditButton();
    }

    @When("clicks add existing group button")
    public void clicks_add_existing_group_button() {
        userManagementSteps.clickOnAddExistingGroupButton();
    }

    @When("clicks add existing approver button")
    public void clicks_add_existing_approver_button() {
        userManagementSteps.clickOnAddExistingApproverButton();
    }

    @When("clicks add existing permission button")
    public void clicks_add_existing_permission_button() {
        userManagementSteps.clickOnAddExistingPermissionButton();
    }

    @When("toggles approvers {string}")
    public void toggles_approvers(String approvers) {
        userManagementSteps.toggleApprovers(Arrays.asList(approvers.split(",")));
    }

    @When("toggles permissions {string}")
    public void toggles_permissions(String permissions) {
        userManagementSteps.togglePermissions(Arrays.asList(permissions.split(",")));
    }

    @When("clicks on done button")
    public void clicks_on_done_button() {
        userManagementSteps.clickOnDoneButton();
    }

    @When("clicks on group {string}")
    public void clicks_on_group(String group) {
        userManagementSteps.clickOnGroup(group);
    }

    @Then("user {string} is disabled")
    public void managed_user_is_disabled(String uid) {
        Assertions.assertB2BCustomerInactive(uid);
    }

    @Then("user {string} is enabled")
    public void managed_user_is_enabled(String uid) {
        Assertions.assertB2BCustomerActive(uid);
    }

    @Then("user {string} has groups {string}")
    public void user_has_groups(String uid, String groups) {
        Assertions.assertUserHasGroups(uid, groups);
    }

    @Then("user {string} has permissions {string}")
    public void user_has_permissions(String uid, String permissions) {
        Assertions.assertB2BCustomerHasPermissions(uid, permissions);
    }

    @Then("user {string} has approvers {string}")
    public void user_has_approvers(String uid, String approvers) {
        Assertions.assertB2BCustomerHasApprovers(uid, approvers);
    }

    @Then("group {string} is visible on user details page")
    public void group_is_visible_on_user_details_page(String uid) {
        Assert.assertTrue(manageUserPage.getRemoveGroupCardLink(uid).isPresent());
    }

    @Then("group {string} is not visible on user details page")
    public void group_is_not_visible_on_user_details_page(String uid) {
        Assert.assertFalse(manageUserPage.getRemoveGroupCardLink(uid).isPresent());
    }

    @Then("permission {string} is not visible on user details page")
    public void permission_is_not_visible_on_user_details_page(String permission) {
        Assert.assertFalse(manageUserPage.getRemovePermissionCardLink(permission).isPresent());
    }

    @Then("approver {string} is not visible on user details page")
    public void approver_is_not_visible_on_user_details_page(String uid) {
        Assert.assertFalse(manageUserPage.getRemoveApproverCardLink(uid).isPresent());
    }

}
