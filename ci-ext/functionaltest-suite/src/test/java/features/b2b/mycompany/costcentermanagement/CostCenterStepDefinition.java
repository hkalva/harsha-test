package features.b2b.mycompany.costcentermanagement;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import pages.mycompany.costcenter.ViewCostCenterPage;
import steps.LoginSteps;
import steps.NavigationSteps;
import steps.mycompany.CostCenterManagmentSteps;
import util.Assertions;
import util.Preconditions;

import java.util.Arrays;

public class CostCenterStepDefinition {

    @Steps
    LoginSteps loginSteps;

    @Steps
    NavigationSteps navigationSteps;

    @Steps
    CostCenterManagmentSteps costCenterManagmentSteps;

    ViewCostCenterPage viewCostCenterPage;

    @Given("commerce org {string} exists")
    public void commerce_org_exists(String commerceOrgUid) {
        Preconditions.ensureCommerceOrgExists(commerceOrgUid);
    }

    @Given("Customer is authenticated using email {string} and password {string}")
    public void customer_is_authenticated_using_email_and_password(String email, String password) {
        loginSteps.openHomePage();
        loginSteps.submitLoginForm(email, password);
    }

    @Given("cost center {string} has budgets {string}")
    public void cost_center_has_budgets(String costCenter, String budgets) {
        Preconditions.b2bCostCenterHasBudgets(costCenter, budgets);
    }

    @Given("cost center {string} does not exist")
    public void cost_center_does_not_exist(String costCenter) {
        Preconditions.b2bCostCenterDoesNotExist(costCenter);
    }

    @Given("cost center {string} is enabled")
    public void cost_center_is_enabled(String costCenter) {
        Preconditions.b2bCostCenterIsEnabled(costCenter);
    }

    @Given("cost center {string} is disabled")
    public void cost_center_is_disabled(String costCenter) {
        Preconditions.b2bCostCenterIsDisabled(costCenter);
    }

    @Given("cost center {string} has name {string}")
    public void cost_center_has_name(String costCenter, String name) {
        Preconditions.b2bCostCenterHasName(costCenter, name);
    }

    @When("customer navigates to manage cost centers page")
    public void customer_navigates_to_manage_cost_centers_page() {
        navigationSteps.navigateToLinkTitle("Cost Centers");
    }

    @When("clicks add new cost center button")
    public void clicks_add_new_cost_center_button() {
        costCenterManagmentSteps.clickAddNewCostCenterButton();
    }

    @When("navigates to cost center {string} view page")
    public void navigates_to_cost_center_page(String costCenter) {
        costCenterManagmentSteps.navigateToViewCostCenterPage(costCenter);
    }

    @When("clicks on cost center edit button")
    public void clicks_on_cost_center_edit_button() {
        costCenterManagmentSteps.clickOnEditCostCenterButton();
    }

    @When("clicks disable cost center button")
    public void clicks_disable_cost_center_button() {
        costCenterManagmentSteps.clickOnDisableCostCenterButton();
    }

    @When("clicks enable cost center button")
    public void clicks_enable_cost_center_button() {
        costCenterManagmentSteps.clickOnEnableCostCenterButton();
    }

    @When("clicks on confirm action button")
    public void clicks_on_confirm_action_button() {
        costCenterManagmentSteps.clickOnConfirmActionButton();
    }

    @When("submits cost center form having code {string}, unit {string}, name {string}, and currency {string}")
    public void submits_cost_center_form_having(String code, String unit, String name, String currency) {
        costCenterManagmentSteps.populateCostCenterForm(code, name, unit, currency);
        costCenterManagmentSteps.submitCostCenterForm();
    }

    @When("clicks add existing budget button")
    public void clicks_add_existing_cost_center_button() {
        costCenterManagmentSteps.clickOnAddExistingBudgetButton();
    }

    @When("toggles budgets {string}")
    public void toggles_budgets(String budgets) {
        costCenterManagmentSteps.toggleBudgets(Arrays.asList(budgets.split(",")));
    }

    @When("clicks on done button")
    public void clicks_on_done_button() {
        costCenterManagmentSteps.clickOnDoneButton();
    }

    @Then("cost center having code {string} is created")
    public void cost_center_having_code_is_created(String costCenter) {
        Assertions.assertB2BCostCenterExists(costCenter);
    }

    @Then("customer is on view cost center page")
    public void customer_is_on_view_cost_center_page() {
        Assert.assertEquals("Cost Center Details", viewCostCenterPage.getBackLinkLabel());
    }

    @Then("the message {string} is displayed")
    public void the_message_is_displayed(String message) {
        Assert.assertTrue(viewCostCenterPage.getAlertElement().isVisible());
        Assert.assertTrue(viewCostCenterPage.getAlertElement().getText().contains(message));
    }

    @Then("cost center {string} is now disabled")
    public void cost_center_is_now_disabled(String costCenter) {
        Assertions.assertB2BCostCenterDisabled(costCenter);
    }

    @Then("cost center {string} is now enabled")
    public void cost_center_is_now_enabled(String costCenter) {
        Assertions.assertB2BCostCenterEnabled(costCenter);
    }

    @Then("cost center {string} now has budgets {string}")
    public void cost_center_now_has_budgets(String costCenter, String budgets) {
        Assertions.assertB2BCostCenterHasBudgets(costCenter, Arrays.asList(budgets.split(",")));
    }

    @Then("cost center {string} now has name {string}")
    public void cost_center_now_has_name(String costCenter, String name) {
        Assertions.assertB2BCostCenterName(costCenter, name);
    }

}
