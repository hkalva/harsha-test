package features.b2b.mycompany.budgetmanagement;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/BudgetManagementTestResults.html"}, features="src/test/resources/features/b2b/mycompany/budgetmanagement.feature")
public class BudgetManagementTestSuite {
}
