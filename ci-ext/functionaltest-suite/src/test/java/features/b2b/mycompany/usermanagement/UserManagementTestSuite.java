package features.b2b.mycompany.usermanagement;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/UserManagementTestResults.html"}, features="src/test/resources/features/b2b/mycompany/usermanagement.feature")
public class UserManagementTestSuite {
}
