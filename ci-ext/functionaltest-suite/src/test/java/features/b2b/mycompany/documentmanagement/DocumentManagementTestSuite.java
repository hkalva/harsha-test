package features.b2b.mycompany.documentmanagement;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/DocumentManagementTestResults.html"},
        features="src/test/resources/features/b2b/mycompany/documentmanagement.feature",
        glue = {"features.common.authentication", "features.b2b.mycompany.documentmanagement"})
public class DocumentManagementTestSuite {
}
