package features.common.productcompare;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import features.components.AddToCartPopup;
import features.components.ProductCompareOverlay;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import pages.CartPage;
import pages.CurrentPage;
import pages.SearchResultsPage;
import responsive.ResponsiveUtils;
import util.Preconditions;

import java.util.List;
import java.util.Map;

public class ProductCompareDefinition {

    AddToCartPopup addToCartPopup;

    CartPage cartPage;

    SearchResultsPage searchResultsPage;

    List<String> selectedProducts;

    ProductCompareOverlay productCompareOverlay;

    CurrentPage currentPage;

    Map<String, String> cart;

    @Given("Customer {string} has an empty cart")
    public void customer_has_an_empty_cart(String email) {
        Preconditions.ensureCustomerCartIsEmpty(email);
        Preconditions.ensureCustomerHasNoSavedCarts(email);
    }


    @When("selects product to compare")
    public void selects_product_to_compare() {
//        if (!ResponsiveUtils.isMobile())
//            currentPage.getDriver().manage().window().maximize();
        selectedProducts = Lists.newArrayList();
        selectedProducts.add(searchResultsPage.selectProductForCompare(0, currentPage.getDriver()));
        selectedProducts.add(searchResultsPage.selectProductForCompare(1, currentPage.getDriver()));
        selectedProducts.add(searchResultsPage.selectProductForCompare(2, currentPage.getDriver()));
        selectedProducts.add(searchResultsPage.selectProductForCompare(3, currentPage.getDriver()));
    }

    @Then("selected products are displayed in the product compare tray")
    public void selected_products_are_displayed_in_the_product_compare_section() {
        Assert.assertTrue("Selected Products are missing from compare tray ", searchResultsPage.selectedProductAreInCompareTray(selectedProducts));
    }

    @When("customer clicks on compare")
    public void customer_clicks_on_compare() {
        searchResultsPage.compareProducts();
    }

    @Then("product compare modal is displayed")
    public void product_compare_modal_is_displayed() {
        Assert.assertTrue("Product Compare overylay is not visible", searchResultsPage.isProductCompareOverlayVisible());
    }

    @Then("selected products are included in the product compare overlay")
    public void selected_product_are_included_in_the_product_compare_overlay() {
        Assert.assertTrue("Product Compare Overlay does not match selected Products", searchResultsPage.selectedProductAreInCompareOverlay(selectedProducts));
    }

    @When("customer click on clear all")
    public void customer_click_on_clear_all() {
        if(ResponsiveUtils.isDesktop())
            searchResultsPage.clearAllComparedProducts();
    }

    @Then("All product are removed from product compare tray")
    public void all_product_are_removed_from_product_compare_tray() {
        if(ResponsiveUtils.isDesktop())
         Assert.assertTrue("Product compare tray has products ", searchResultsPage.productsAreRemovedFromCompareTray());
    }

    @When("customer clears compare selection checkbox")
    public void customer_clears_compare_selection_checkbox() {
        selectedProducts.remove(searchResultsPage.selectProductForCompare(0, currentPage.getDriver()));
        selectedProducts.remove(searchResultsPage.selectProductForCompare(1, currentPage.getDriver()));
    }

    @When("customer removes products form compare tray")
    public void customer_clicks_remove_product() {
        selectedProducts.remove(searchResultsPage.removeProductFromCompareTray(0));
        selectedProducts.remove(searchResultsPage.removeProductFromCompareTray(0));
    }

    @When("customer removes products form compare overlay")
    public void customer_removes_products_form_compare_overlay() {
        selectedProducts.remove(searchResultsPage.removeProductFromCompareOverlay(0));
        selectedProducts.remove(searchResultsPage.removeProductFromCompareOverlay(0));
    }

    @When("customer clicks on add to cart")
    public void customer_clicks_on_add_to_cart() {
        cart = productCompareOverlay.addToCart();
    }

    @Then("add to cart modal is diplayed with the product")
    public void add_to_cart_modal_is_diplayed_with_the_product() {
        addToCartPopup.isDispalyed();
    }

    @When("customer click checkout button")
    public void customer_click_checkout_button() {
        addToCartPopup.clickCheckoutButton();
    }

    @Then("cart page is displayed")
    public void cart_page_is_displayed() {
        Assert.assertTrue("Customer is on cart page", cartPage.onPage());
        cartPage.hasProducts(cart);
    }
}
