package features.common.productcompare;


import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;
//, "features.common.product", "features.common.cart"
@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/ProductCompareTestResults.html"}, features = "src/test/resources/features/common/productCompare.feature"
        ,glue={"features.common.authentication", "features.common.search", "features.common.productcompare"}
        )
public class ProductCompareTestSuite {

}
