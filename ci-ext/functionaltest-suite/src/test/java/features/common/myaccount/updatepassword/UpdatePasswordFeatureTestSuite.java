package features.common.myaccount.updatepassword;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/UpdatePasswordFeatureTestResults.html"}, features = "src/test/resources/features/common/myaccount/updatePassword.feature",
        glue = {"features.common.authentication", "features.common.myaccount.updatepassword" })
public class UpdatePasswordFeatureTestSuite {
}
