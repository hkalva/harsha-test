package features.common.myaccount.paymentdetials;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import features.components.DeleteCreditCardPopup;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import pages.myaccount.PaymentDetailsPage;
import steps.NavigationSteps;
import util.Constants;
import util.Preconditions;

public class PaymentDetailsStepDefinition {

    @Steps
    NavigationSteps navigationSteps;

    PaymentDetailsPage paymentDetailsPage;
    DeleteCreditCardPopup deleteCreditCardPopup;


    @Given("customer {string} has saved payment info")
    public void customer_has_saved_payment_info(String email) {
        Preconditions.createCreditCard(email,
                "4111111111111111",
                "12",
                "2025",
                "visa",
                true,
                false,
                "William Hunter",
                "Street Name",
                "Street Number",
                "Town",
                "75024",
                "US-TX",
                "US",
                "billingAddress");
    }

    @When("customer clicks on payment info delete button")
    public void customer_clicks_on_payment_info_delete_button() {
        navigationSteps.clickOnMyAccount();
        navigationSteps.clickLink(Constants.Account.PAYMENT_DETAILS);
    }

    @When("clicks delete button on delete payment modal")
    public void clicks_delete_button_on_delete_payment_modal() {
        paymentDetailsPage.deletePaymentInfo();
        deleteCreditCardPopup.clickDelete();
    }

    @Then("customer remains on payment details page")
    public void customer_remains_on_payment_details_page() {
        Assert.assertTrue(paymentDetailsPage.activeBreadcrumIs(Constants.Account.Breadcrumb.PAYMENT_DETAILS));
    }

    @Then("payment detail is removed")
    public void payment_detail_is_removed() {
        Assert.assertTrue(paymentDetailsPage.creditCardIsDeleted());
    }
}
