package features.common.myaccount.updateemail;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/UpdateEmailFeatureTestResults.html"}, features = "src/test/resources/features/common/myaccount/updateEmail.feature",
        glue = {"features.common.authentication", "features.common.myaccount.updateemail" })
public class UpdateEmailFeatureTestSuite {
}
