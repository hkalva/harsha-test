package features.common.myaccount.returnrequests;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/ReturnRequestsFeatureTestResults.html"}, features = "src/test/resources/features/common/myaccount/returnRequests.feature",
        glue = {"features.common.authentication", "features.common.myaccount.returnrequests" })
public class ReturnRequestsFeatureTestSuite {
}
