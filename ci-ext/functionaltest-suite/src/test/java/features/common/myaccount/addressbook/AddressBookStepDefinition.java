package features.common.myaccount.addressbook;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import features.components.DeleteAddressPopup;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import pages.myaccount.AddressBookPage;
import steps.LoginSteps;
import steps.NavigationSteps;
import util.Constants;

public class AddressBookStepDefinition {

    @Steps
    LoginSteps loginSteps;

    @Steps
    NavigationSteps navigationSteps;

    AddressBookPage addressBookPage;

    DeleteAddressPopup deleteAddressPopup;

    @When("customer navigates to address book")
    public void customer_navigates_to_address_book() {
        navigationSteps.clickOnMyAccount();
        navigationSteps.clickLink(Constants.Account.ADDRESS_BOOK);
    }

    @When("clicks on add address button")
    public void clicks_on_add_address_button() {
        addressBookPage.clickOnAddAddress();
    }

    @When("submits new address")
    public void submits_new_address() {
        addressBookPage.submitAddress(addressBookPage.getNewAddress());
    }

    @Then("customer is on edit address page")
    public void customer_is_on_edit_address_page() {
        Assert.assertTrue(addressBookPage.activeBreadcrumIs(Constants.Account.Breadcrumb.EDIT_ADDRESS_BOOK));
    }

    @Then("success message {string} is shown")
    public void success_message_is_shown(String string) {
        addressBookPage.globalMessageIs(string);
    }

    @Given("customer has existing address")
    public void customer_has_existing_address() {
        addressBookPage.hasAddress();
    }

    @When("clicks on edit address button")
    public void clicks_on_edit_address_button() {
        addressBookPage.clickOnEditAddress();
    }

    @When("submits address change")
    public void submits_address_change() {
        addressBookPage.submitAddress(addressBookPage.getAddressForUpdate());
    }

    @When("clicks on delete address button")
    public void clicks_on_delete_address_button() {
        addressBookPage.clickOnDeleteAddress();
    }

    @When("clicks delete button on delete address modal")
    public void clicks_delete_button_on_delete_address_modal() {
        deleteAddressPopup.confirmDelete();
    }

    @Then("customer is on address book page")
    public void customer_is_on_address_book_page() {
        Assert.assertTrue(addressBookPage.activeBreadcrumIs(Constants.Account.Breadcrumb.ADDRESS_BOOK));
    }
}
