package features.common.myaccount.orderdetails;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/OrderDetailsFeatureTestResults.html"}, features = "src/test/resources/features/common/myaccount/orderDetails.feature",
        glue = {"features.common.authentication", "features.common.myaccount.orderdetails"} )
public class OrderDetailsFeatureTestSuite {
}
