package features.common.myaccount.returnrequests;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import pages.myaccount.ReturnDetailsPage;
import pages.myaccount.ReturnsHistoryPage;
import steps.NavigationSteps;
import util.Constants;
import util.Preconditions;

public class ReturnRequestStepDefinitions {

    @Steps
    NavigationSteps navigationSteps;

    ReturnsHistoryPage returnsHistoryPage;
    ReturnDetailsPage returnDetailsPage;

    final String RETURN_ORDER = "RETURN-ORDER";

    @Given("customer {string} has return request in {string} status")
    public void customer_has_return_request_in_status(String email, String status) {
        Preconditions.createReturnRequest(email, status, RETURN_ORDER);
    }

    @When("customer navigates to return history page")
    public void customer_navigates_to_return_history_page() {
        navigationSteps.clickOnMyAccount();
        navigationSteps.clickLink(Constants.Account.RETURN_HISTORY);
    }

    @When("customer clicks on return request")
    public void customer_clicks_on_return_request() {
        returnsHistoryPage.goToRetunDetails(RETURN_ORDER);
    }

    @Then("customer is on return request detail page")
    public void customer_is_on_return_request_detail_page() {
        Assert.assertTrue("Customer is not on Returns Detail Page ", returnDetailsPage.onPage());
    }

    @When("customer clicks on cancel return button")
    public void customer_clicks_on_cancel_return_button() {
        returnDetailsPage.cancelReturnRequest();
    }

    @When("clicks submit request")
    public void clicks_submit_request() {
        returnDetailsPage.submitReturnRequest();
    }

    @Then("return is canceled")
    public void return_is_canceled() {
        Assert.assertTrue("Return Order is not Cancelled", returnsHistoryPage.isReturnCancelled(RETURN_ORDER));
    }
}
