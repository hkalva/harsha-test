package features.common.myaccount.updatepassword;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import pages.myaccount.UpdatePasswordPage;
import steps.NavigationSteps;
import util.Assertions;
import util.Constants;
import util.Elements;
import util.Preconditions;

public class UpdatePasswordStepDefinition {

    @Steps
    NavigationSteps navigationSteps;

    UpdatePasswordPage updatePasswordPage;

    @When("customer navigates to update password page")
    public void customer_navigates_to_update_password_page() {
        navigationSteps.clickOnMyAccount();
        navigationSteps.clickLink(Constants.Account.PASSWORD);
    }

    @When("submits password form with current password {string} and new password {string} and confirm password {string}")
    public void submits_password_form_with_current_password_and_new_password_and_confirm_password(String currentPassword,
                                                                                                  String newPassowrd, String confirmNewPassword) {
        updatePasswordPage.updatePassword(currentPassword, newPassowrd, confirmNewPassword);
    }

    @Then("customer is still on update password page")
    public void customer_is_still_on_update_password_page() {
        Assert.assertTrue(updatePasswordPage.activeBreadcrumIs(Constants.Account.Breadcrumb.UPDATE_PASSWORD));
    }

    @Then("success message {string} is shown")
    public void success_message_is_shown(String string) {
        Assert.assertTrue(updatePasswordPage.globalMessageIs(string));
    }

    @Then("customer {string} password has changed from {string}")
    public void customer_password_has_changed(String email, String password) {
        Assertions.assertPasswordChanged(email, password);
    }

    @Then("current password validation error message {string} is shown")
    public void validation_error_message_is_shown1(String message) {
        Assert.assertTrue(updatePasswordPage.validationError(Elements.Account.UpdatePassword.PASSWORD_ERRORS, message));
    }

    @Then("password do not match validation error message {string} is shown")
    public void validation_error_message_is_shown2(String message) {
        Assert.assertTrue(updatePasswordPage.validationError(Elements.Account.UpdatePassword.CHECK_NEW_PASSWORD_ERRORS, message));
    }

    @Then("password requirement validation error message {string} is shown")
    public void validation_error_message_is_shown(String message) {
        Assert.assertTrue(updatePasswordPage.validationError(Elements.Account.UpdatePassword.NEW_PASSWORD_ERRORS, message));
        Assert.assertTrue(updatePasswordPage.validationError(Elements.Account.UpdatePassword.CHECK_NEW_PASSWORD_ERRORS, message));
    }

    @And("reset customer {string} password to {string}")
    public void resetCustomerEmailPasswordToPassword(String uid, String password) {
        Preconditions.setUserPassword(uid, password);
    }
}
