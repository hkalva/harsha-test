package features.common.myaccount.personaldetails;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import org.openqa.selenium.By;
import pages.CurrentPage;
import pages.myaccount.PersonalDetailsPage;
import steps.LoginSteps;
import steps.NavigationSteps;
import util.Constants;
import util.Elements;


public class PersonalDetailsStepDefinition {

    @Steps
    NavigationSteps navigationSteps;

    CurrentPage currentPage;

    PersonalDetailsPage personalDetailsPage;

    @When("customer navigates to user details page")
    public void customer_navigates_to_user_details_page() {
        navigationSteps.clickOnMyAccount();
        navigationSteps.clickLink(Constants.Account.PERSONL_DETAILS);
    }

    @When("customer submits profile form with first name {string} and last name {string}")
    public void customer_submits_profile_form_with_first_name_and_last_name(String firstName, String lastName) {
        personalDetailsPage.updatePersonalDetails(firstName, lastName);
    }

    @Then("customer is still on user-details page")
    public void customer_is_still_on_user_details_page() {
        Assert.assertTrue(currentPage.activeBreadcrumIs(Constants.Account.Breadcrumb.PROFILE));
    }

    @Then("customer has a first name of {string} and last name of {string}")
    public void customer_has_a_first_name_and_last_name(String firstName, String lastName) {
        Assert.assertTrue(firstName.equals(currentPage.find(By.name(Elements.Account.Profile.FIRST_NAME)).getTextValue()));
        Assert.assertTrue(lastName.equals(currentPage.find(By.name(Elements.Account.Profile.LAST_NAME)).getTextValue()));
    }
}
