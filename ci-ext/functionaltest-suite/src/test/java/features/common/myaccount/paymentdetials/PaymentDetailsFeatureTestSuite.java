package features.common.myaccount.paymentdetials;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/CheckoutFeatureTestResults.html"}, features = "src/test/resources/features/common/myaccount/paymentDetails.feature",
        glue = {"features.common.authentication", "features.common.myaccount.paymentdetials"}
)
public class PaymentDetailsFeatureTestSuite {
}
