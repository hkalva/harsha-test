package features.common.myaccount.addressbook;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/AddressBookFeatureTestResults.html"}, features = "src/test/resources/features/common/myaccount/addressBook.feature",
    glue = {"features.common.authentication", "features.common.myaccount.addressbook"} )
public class AddressBookFeatureTestSuite {
}
