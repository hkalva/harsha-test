package features.common.myaccount.personaldetails;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/PersonalDetailsFeatureTestResults.html"}, features = "src/test/resources/features/common/personalDetails.feature",
        glue = {"features.common.authentication", "features.common.myaccount.personaldetails" })
public class PersonalDetailsFeatureTestSuite {
}
