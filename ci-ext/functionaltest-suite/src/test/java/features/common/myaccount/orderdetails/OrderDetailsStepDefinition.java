package features.common.myaccount.orderdetails;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import features.components.AddToCartPopup;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import pages.CartPage;
import pages.CheckoutPage;
import pages.CurrentPage;
import pages.SearchResultsPage;
import pages.myaccount.OrderDetailsPage;
import pages.myaccount.OrderHistoryPage;
import pages.myaccount.ReturnOrderPage;
import steps.LoginSteps;
import steps.NavigationSteps;
import steps.SearchSteps;
import util.Assertions;
import util.Constants;
import util.Elements;
import util.Preconditions;

import java.util.List;

public class OrderDetailsStepDefinition {

    @Steps
    SearchSteps searchSteps;

    @Steps
    NavigationSteps navigationSteps;

    AddToCartPopup addToCartPopup;

    SearchResultsPage searchResultsPage;
    CartPage cartPage;
    CheckoutPage checkoutPage;
    CurrentPage currentPage;
    OrderHistoryPage orderHistoryPage;
    OrderDetailsPage orderDetailsPage;
    ReturnOrderPage returnOrderPage;

    String orderNumber;

    final String COMPLETED_ORDER_NUMBER = "99999998";
    List<String> orderProducts;

    @When("customer places order for {string}")
    public void customerPlacesOrderForTerm(String term) {
        searchSteps.searchForTerm(term);
        searchResultsPage.addItemToCart();
        addToCartPopup.clickCheckoutButton();
        cartPage.checkout();
        checkoutPage.selectPaymentMethod();
        checkoutPage.populateShippingAddress();
        checkoutPage.selectDeliveryMethod();
        checkoutPage.populateBillingInformation();
        checkoutPage.submitOrder();
        orderNumber = checkoutPage.getOrderNumber();
    }

    @When("customer navigates to order-history page")
    public void customer_navigates_to_order_history_page() {
        navigationSteps.clickOnMyAccount();
        navigationSteps.clickLink(Constants.Account.ORDER_HISTORY);
    }

    @When("customer clicks on the order link")
    public void customer_clicks_on_the_order_link() {
        orderHistoryPage.clickOnOrder(orderNumber);
    }

    @Then("customer is on order detail page")
    public void customer_is_on_order_detail_page() {
        StringBuilder activeText = new StringBuilder(Constants.Account.Breadcrumb.ORDER_DETAILS);
        Assert.assertTrue(currentPage.activeBreadcrumIs(activeText.append(orderNumber).toString()));
    }

    @Given("customer {string} has a completed order")
    public void customer_has_a_completed_order(String email) {
        Preconditions.createOrder(email, COMPLETED_ORDER_NUMBER);
        navigationSteps.clickOnMyAccount();
        navigationSteps.clickLink(Constants.Account.ORDER_HISTORY);
        orderHistoryPage.clickOnOrder(COMPLETED_ORDER_NUMBER);
        orderNumber = COMPLETED_ORDER_NUMBER;
        customer_is_on_order_detail_page();
    }

    @When("clicks reorder")
    public void clicks_reorder() {
        orderProducts = orderDetailsPage.getProducts();
        currentPage.find(Elements.OrderDetails.REORDER_BUTTON).click();
    }

    @Then("new cart is created using order items")
    public void new_cart_is_created_using_order_items() throws Exception {
        navigationSteps.navigateToCartPage();
        Assert.assertTrue(cartPage.hasAllEntriesFromOrder(orderProducts));
    }

    @Then("customer is on payment method checkout step")
    public void customer_is_on_payment_method_checkout_step() {
        Assert.assertTrue(checkoutPage.customerIsOnPaymentInfoStep());
    }

    @When("clicks return order")
    public void clicks_return_order() {
        currentPage.find(Elements.OrderDetails.RETURN_ORDER_BUTTON).click();
    }

    @When("clicks on return complete order button")
    public void clicks_on_return_complete_order_button() {
        returnOrderPage.returnCompleteOrder();
    }

    @When("clicks on confirm return order button")
    public void clicks_on_confirm_return_order_button() {
        returnOrderPage.confirmReturnOrder();
    }

    @When("clicks on submit request button")
    public void clicks_on_submit_request_button() {
        returnOrderPage.submitReturnOrder();
    }

    @Then("customer is on order history page")
    public void customer_is_on_order_history_page() {
        currentPage.activeBreadcrumIs(Constants.Account.Breadcrumb.ORDER_HISTORY);
    }

    @Then("success message {string} is shown")
    public void success_message_is_shown(String string) {
        Assert.assertTrue(currentPage.globalMessageIs(Constants.Account.RETURN_REQUEST_SUCCESS));
    }

    @Then("return request is created for order")
    public void return_request_is_created_for_order() {
        Assertions.assertReturnRequestIsCreatedForOrder(COMPLETED_ORDER_NUMBER);
    }
}
