package features.common.myaccount.updateemail;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import pages.CurrentPage;
import pages.myaccount.UpdateEmailPage;
import steps.LoginSteps;
import steps.NavigationSteps;
import util.Constants;
import util.Elements;
import util.Preconditions;

public class UpdateEmailStepDefinition {
    @Steps
    NavigationSteps navigationSteps;

    UpdateEmailPage updateEmailPage;

    CurrentPage currentPage;

    @When("customer navigates to update email page")
    public void customer_navigates_to_update_email_page() {
        navigationSteps.clickOnMyAccount();
        navigationSteps.clickLink(Constants.Account.EMAIL_ADDRESS);
    }

    @When("customer submits update email form with email {string} and password {string}")
    public void customer_submits_update_email_form_with_email(String email, String password) {
        updateEmailPage.updateEmail(email, password);
    }

    @Then("customer is still on email-update page")
    public void customer_is_still_on_email_update_page() {
        Assert.assertTrue(currentPage.activeBreadcrumIs(Constants.Account.Breadcrumb.PROFILE));
        Assert.assertTrue(updateEmailPage.isCurrentPage());
    }

    @Then("customer email is {string}")
    public void customer_email_is(String string) {
        Assert.assertTrue(updateEmailPage.customerEmailIs(string));
    }

    @Then("and the error message {string} is displayed")
    public void and_the_error_message_is_displayed(String errorMessage) {
        Assert.assertTrue(updateEmailPage.validationErrorIs(Elements.UpdateEmail.EMAIL_ERROR, errorMessage));
    }

    @Then("reset customer email from {string} to {string}")
    public void reset_customer_email(String fromUserId, String toUserId) {
        Preconditions.updateUserId(fromUserId, toUserId);
    }

}
