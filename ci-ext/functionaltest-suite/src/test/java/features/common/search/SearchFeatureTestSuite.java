package features.common.search;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/SearchFeatureTestResults.html"}, features="src/test/resources/features/common/search.feature")
public class SearchFeatureTestSuite {
}
