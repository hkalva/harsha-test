package features.common.search;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import pages.HomePage;
import pages.SearchResultsPage;
import responsive.ResponsiveUtils;
import steps.LoginSteps;

import org.assertj.core.api.Assertions;
import steps.SearchSteps;

public class SearchStepDefinition {

    @Steps
    LoginSteps loginSteps;

    @Steps
    SearchSteps searchSteps;

    HomePage homePage;
    SearchResultsPage searchResultsPage;

    @Given("William has logged in using email {string} and password {string}")
    public void william_has_logged_in_using_email_and_password(String email, String password) {
        loginSteps.openHomePage();
        loginSteps.submitLoginForm(email, password);
    }

    @When("he types {string} in search field")
    public void he_types_in_search_field(String term) {
        if(ResponsiveUtils.isMobile()) {
            searchSteps.toggleMobileSearch();
        }
        searchSteps.typeSearchTerm(term);
    }

    @When("he submits search term {string} field")
    public void submits_search_term(String term) {
        searchSteps.searchForTerm(term);
    }

    @Then("autosuggest results are displayed")
    public void then_autosuggest_results_are_displayed() {
        Assertions.assertThat(homePage.findAutoSuggestResults().size()).isPositive();
    }

    @Then("search results are displayed")
    public void search_results_are_displayed() {
        Assert.assertTrue(searchResultsPage.onPage());
        Assertions.assertThat(searchResultsPage.findSearchResults().size()).isPositive();
    }

}
