package features.common.authentication;

import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;
import steps.LoginSteps;

public class AuthenticationStepDefinition {
    @Steps
    LoginSteps loginSteps;

    @Given("Customer is authenticated using email {string} and password {string}")
    public void customer_is_authenticated(String email, String password) {
        loginSteps.openHomePage();
        loginSteps.submitLoginForm(email, password);
    }
}
