package features.common.quickview;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/QuickViewTestResults.html"}, features = "src/test/resources/features/common/quickView.feature")
public class QuickViewTestSuite {


}
