package features.common.quickview;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import features.components.QuickViewPopup;
import net.thucydides.core.annotations.Steps;
import org.springframework.util.Assert;
import pages.ProductDetailsPage;
import pages.SearchResultsPage;
import steps.LoginSteps;
import steps.SearchSteps;
import util.Preconditions;


public class QuickViewStepDefinitions {
    @Steps
    LoginSteps loginSteps;

    @Steps
    SearchSteps searchSteps;

    SearchResultsPage searchResultsPage;

    QuickViewPopup quickViewPopup;

    ProductDetailsPage productDetailsPage;


    @Given("Customer {string} has an empty cart")
    public void customer_has_an_empty_cart(String email) {
        Preconditions.ensureCustomerCartIsEmpty(email);
    }

    @When("he hovers over a product")
    public void he_hovers_over_a_product() {
        searchResultsPage.hoverOverProduct();
    }

    @Then("quick view button is displayed")
    public void quick_view_button_is_displayed() {
        Assert.isTrue(searchResultsPage.quickViewButtonIsDisplayed(), "quick view button is not displayed");
    }

    @When("he click on quick view")
    public void he_click_on_quick_view() {
        searchResultsPage.clickOnQuickView();
    }

    @Then("quick view modal is displayed")
    public void quick_view_modal_is_displayed() {
        Assert.isTrue(quickViewPopup.isDisplayed(), "Quick View Modal is not displayed");
    }

    @When("he clicks on Add To Cart")
    public void he_clicks_on_Add_To_Cart() {
        productDetailsPage.addToCart();
        productDetailsPage.waitForLoad();
    }

    @Then("product is added to the shopping cart")
    public void product_is_added_to_the_shopping_cart() {
        Assert.isTrue(quickViewPopup.isProductAddedToCart(), "Product is not added to cart");
    }
}
