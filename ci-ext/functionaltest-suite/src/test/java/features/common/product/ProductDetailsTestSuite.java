package features.common.product;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/ProductDetailsTestResults.html"}, features = "src/test/resources/features/common/productDetails.feature",
 glue={"features.common.authentication", "features.common.product", "features.common.cart"})
public class ProductDetailsTestSuite {
}
