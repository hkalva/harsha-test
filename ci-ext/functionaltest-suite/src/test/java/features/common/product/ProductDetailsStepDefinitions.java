package features.common.product;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import features.components.Header;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import pages.CartPage;
import pages.CurrentPage;
import pages.ProductDetailsPage;
import steps.NavigationSteps;
import util.Elements;
import util.TextUtil;

import java.util.List;

public class ProductDetailsStepDefinitions {


    @Steps
    NavigationSteps navigationSteps;

    CartPage cartPage;
    ProductDetailsPage productDetailsPage;
    CurrentPage currentPage;
    Header header;


    @And("Customer navigated to product page")
    public void customerNavigatedToProductPage() {
        navigationSteps.navigateToProductDetailsPage();
    }

    @When("Customer clicks on add to cart button")
    public void customerClicksOnAddToCartButton() {
        productDetailsPage.addToCart();
    }

    @Then("Mini cart count is {string}")
    public void mini_cart_count_is_updated(String count) {
        Assert.assertEquals(count, header.getCurrentCartCount() + "");
    }


    @When("customer updates product count to {string}")
    public void customerUpdatesProductQuantityInCartPage(String quantity) {
        //scroll to the cart items, to make it visible
        currentPage.scrollToElement(Elements.Cart.CART_ITEM_LIST);
        List<WebElementFacade> quantityFields = cartPage.findAll(Elements.Cart.QUANTITY_INPUT);
        WebElementFacade quantityInput = quantityFields.stream().filter(webElementFacade -> webElementFacade.isVisible()).findFirst().get();
        TextUtil.clearText(quantityInput);
        quantityInput.typeAndTab(quantity);
    }

    @Then("cart item count is {string}")
    public void cartItemCountIsNewcount(String quantity) {
        Assert.assertEquals(quantity, header.getCurrentCartCount() + "");
    }

    @When("Customer removes product from the cart")
    public void customerRemovesProductFromTheCart() {
        cartPage.find(Elements.Cart.CART_ELEMENT_DETAILS_BUTTON).click();
        cartPage.find(Elements.Cart.CART_ELEMENT_REMOVE).then("a").click();
    }

    @Then("product is not in the cart")
    public void productIsNotInTheCart() {
        Assertions.assertThat(0 == header.getCurrentCartCount());
    }

}
