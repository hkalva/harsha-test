package features.common.cart.save;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/SaveCartFeatureTestResults.html"}, features="src/test/resources/features/common/saveCart.feature",
 glue={"features.common.authentication", "features.common.cart"} )
public class SaveCartFeatureTestSuite {
}
