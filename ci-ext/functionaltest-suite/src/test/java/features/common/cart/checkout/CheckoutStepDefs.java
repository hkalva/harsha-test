package features.common.cart.checkout;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import features.components.AddToCartPopup;
import features.components.Header;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import org.openqa.selenium.By;
import pages.CartPage;
import pages.CheckoutPage;
import pages.ProductDetailsPage;
import pages.SearchResultsPage;
import steps.LoginSteps;
import steps.SearchSteps;
import util.Elements;
import util.TextUtil;

import java.util.Optional;

public class CheckoutStepDefs {

    AddToCartPopup addToCartPopup;
    Header header;
    CartPage cartPage;
    CheckoutPage checkoutPage;
    ProductDetailsPage productDetailsPage;
    SearchResultsPage searchResultsPage;

    @Steps
    SearchSteps searchSteps;

    @Given("has added {string} of a product {string} to the cart")
    public void has_added_of_a_product_to_the_cart(String count, String searchTerm) {
        searchSteps.searchForTerm(searchTerm);
        Optional<WebElementFacade> webElementFacadeOption = searchResultsPage.findInStockItem();
        if (webElementFacadeOption.isPresent()) {
            WebElementFacade details = webElementFacadeOption.get();
            //searchResultsPage.scrollToElement(details);
            details.find(By.tagName("a")).click();
            WebElementFacade addToCartInput = productDetailsPage.find(Elements.ProductDetails.ADD_TO_CART_INPUT);
            TextUtil.clearText(addToCartInput);
            addToCartInput.type(count);
            productDetailsPage.find(Elements.ProductDetails.ADD_TO_CART).click();
        }
    }

    @When("customer click checkout button")
    public void customer_click_checkout_button() {
        cartPage.checkout();
    }

    @When("adds shipping address")
    public void adds_shipping_address() {
        checkoutPage.populateShippingAddress();
    }

    @When("selects shipping method")
    public void selects_shipping_method() {
        checkoutPage.selectDeliveryMethod();
    }

    @When("adds billing information")
    public void adds_billing_information() {
        checkoutPage.populateBillingInformation();
        ;
    }

    @When("places order")
    public void places_order() {
        checkoutPage.submitOrder();
    }

    @Then("order confirmation page is created")
    public void order_confirmation_page_is_created() {
        Assert.assertTrue(checkoutPage.confirmOrder());
    }

    @Then("order business process is started")
    public void order_business_process_is_started() {
        checkoutPage.confirmOrderBusinessProcess();
    }

    @When("adds payment information")
    public void adds_payment_information() {
        checkoutPage.selectPaymentMethod();

    }
}
