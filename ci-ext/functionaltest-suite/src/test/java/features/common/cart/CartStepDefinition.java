package features.common.cart;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import features.components.AddToCartPopup;
import features.components.Header;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import pages.CartPage;
import steps.LoginSteps;
import steps.SearchSteps;
import util.Preconditions;


public class CartStepDefinition
{
    AddToCartPopup addToCartPopup;
    Header header;
    CartPage cartPage;

    @Steps
    LoginSteps loginSteps;

    @Steps
    SearchSteps searchSteps;

    @Given("navigated to the cart page")
    public void navigated_to_the_cart_page()
    {
        addToCartPopup.clickCheckoutButton();
    }

    @Given("Customer {string} has an empty cart")
    public void customerCartIsEmpty(String email)
    {
        Preconditions.ensureCustomerCartIsEmpty(email);
        Preconditions.ensureCustomerHasNoSavedCarts(email);
    }

    @Given("Customer searched for term {string}")
    public void customer_searched_for_term(String term)
    {
        searchSteps.searchForTerm(term);
    }

    @When("Customer adds item to cart")
    public void customer_adds_item_to_cart()
    {
        searchSteps.addItemToCart();
    }

    @Then("Cart modal opens showing item added to cart")
    public void cart_modal_opens_showing_item_added_to_cart()
    {
        Assert.assertTrue(addToCartPopup.getAddToCartItem().isVisible());
    }

    @Then("Mini cart count is {int}")
    public void mini_cart_count_is_updated(int count)
    {
        Assert.assertEquals(count, header.getCurrentCartCount());
    }

    @Given("Customer has added item to cart")
    public void customer_has_added_item_to_cart()
    {
        searchSteps.addItemToCart();
    }

    @When("Customer clicks modal checkout button")
    public void customer_clicks_modal_checkout_button()
    {
        addToCartPopup.clickCheckoutButton();
    }

    @Then("cart page is displayed")
    public void cart_page_is_displayed()
    {
        Assert.assertTrue(cartPage.onPage());
    }

    @When("Customer clicks on  Remove button")
    public void customer_clicks_remove_button()
    {
        addToCartPopup.openItemRemoveButton();
        addToCartPopup.clickItemRemoveButton();
    }

    @Then("Cart page is loaded with empty product")
    public void cartpage_reloaded_with_empty_product()
    {
        Assert.assertTrue(cartPage.getNoOfItemsInCart() == 0);
    }

    @When("Customer change the qty of product {int}")
    public void customer_change_qty(int qtantity)
    {
        addToCartPopup.changeItemQuantity(qtantity);
    }

    @Then("Cart page is loaded with new qty of product {int}")
    public void cartpage_reloaded_with_new_qty(int qtantity)
    {
        Assert.assertEquals(String.valueOf(qtantity), addToCartPopup.getItemQuantity());
    }
}
