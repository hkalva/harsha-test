package features.common.cart;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/CartFeatureTestResults.html"}, features="src/test/resources/features/common/cart.feature",
        glue={"features.common.authentication", "features.common.cart" })
public class CartFeatureTestSuite {
}
