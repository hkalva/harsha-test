package features.common.cart.checkout;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/CheckoutFeatureTestResults.html"}, features = {"src/test/resources/features/common/checkout.feature"}, glue={"features.common.authentication",
        "features.common.cart"})
public class CheckoutFeatureTestSuite {
}
