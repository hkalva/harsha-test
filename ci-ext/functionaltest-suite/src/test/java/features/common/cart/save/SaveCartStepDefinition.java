package features.common.cart.save;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import features.components.AddToCartPopup;
import features.components.DeleteSavedCart;
import features.components.RestoreSavedCartPopup;
import features.components.SaveCartPopup;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import pages.*;
import steps.LoginSteps;
import steps.NavigationSteps;
import steps.SearchSteps;
import util.TestUtil;

import java.net.URI;
import java.net.URISyntaxException;

public class SaveCartStepDefinition {

    SearchResultsPage searchResultsPage;
    CartPage cartPage;
    SaveCartPopup saveCartPopup;
    SavedCartsPage savedCartsPage;
    SavedCartDetailsPage savedCartDetailsPage;
    CurrentPage currentPage;
    DeleteSavedCart deleteSavedCart;
    RestoreSavedCartPopup restoreSavedCartPopup;

    @When("Customer submits save cart form with {string} and {string}")
    public void customerSubmitsSaveCartFormWithNameAndDescription(String name, String description) {
        cartPage.newCart();
        saveCartPopup.saveCart(name, description);
        TestUtil.pause(2);
        currentPage.getDriver().navigate().refresh();
    }

    @When("adds {int} of a product to the cart")
    public void hasAddedCountOfAProductToTheCart(Integer quantity) {
        searchResultsPage.addItemToCart();
    }

    @Then("empty cart page is loaded")
    public void emptyCartPageIsLoaded() {
        Assert.assertTrue(cartPage.getNoOfItemsInCart() == 0);
    }


    @When("saved cart is added with {string} and {string}")
    public void savedCartIsAddedWithNameAndDescription(String name, String description) {
        cartPage.goToSavedCarts();
        Assert.assertTrue(savedCartsPage.findSavedCart(name, description).isPresent());
    }

    @Then("has {int} saved carts")
    public void hasCountSavedCarts(Integer noOfCarts) {
        Assert.assertTrue(savedCartsPage.verifyNoOfSavedCarts(noOfCarts));
    }

    @When("customer navigates to the saved carts page")
    public void customerNavigatesToTheSavedCartsPage() throws URISyntaxException {
        WebDriver driver = currentPage.getDriver();
        driver.get(new URI(driver.getCurrentUrl()).resolve("/cart").toString());
        cartPage.goToSavedCarts();
    }

    @When("navigates to saved cart details  page")
    public void navigatesToSavedCartDetailsPage() {
        savedCartsPage.gotoSavedCartDetails();
    }

    @Then("customer is on saved cart details page")
    public void customerIsOnSavedCartDetailsPage() {
        Assert.assertTrue(savedCartDetailsPage.confirmUserIsOnSavedCartDetailsPage());
    }

    @When("clicks saved cart restore button")
    public void clicksSavedCartRestoreButton() {
        savedCartsPage.restoreSavedCart();
    }

    @When("chooses to keep saved cart")
    public void choosesToKeepSavedCart() {
        restoreSavedCartPopup.selectKeepSavedCart(true);
    }

    @When("submits saved cart restore form")
    public void submitsSavedCartRestoreForm() {
        restoreSavedCartPopup.restoreCart();
    }

    @Then("cart page is loaded with restored cart")
    public void cartPageIsLoadedWithRestoredCart() {
        Assert.assertTrue(cartPage.getNoOfItemsInCart() > 0);
    }

    @Then("copy of saved cart is kept")
    public void copyOfSavedCartIsKept() {
        cartPage.goToSavedCarts();
        Assert.assertTrue(savedCartsPage.verifyNoOfSavedCarts(1));
    }

    @When("chooses not to keep saved cart")
    public void choosesNotToKeepSavedCart() {
        restoreSavedCartPopup.selectKeepSavedCart(false);
    }

    @Then("no copy of saved cart is made")
    public void noCopyOfSavedCartIsMade() {
        cartPage.goToSavedCarts();
        Assert.assertTrue(savedCartsPage.verifyNoOfSavedCarts(0));
    }

    @When("clicks saved cart remove button")
    public void clicksSavedCartRemoveButton() {
        savedCartsPage.removeSavedCart();
    }

    @When("clicks delete button on cart removal modal")
    public void clicksDeleteButtonOnCartRemovalModal() {
        deleteSavedCart.confirmDelete();
    }

    @Then("saved cart is removed")
    public void savedCartIsRemoved() {
        Assert.assertTrue(savedCartsPage.verifyNoOfSavedCarts(0));
    }

    @When("submits saved cart restore form from saved cart detail")
    public void submitsSavedCartRestoreFormFromSavedCartDetail() {
        savedCartDetailsPage.restoreSavedCartFromDetails();
    }

    @When("clicks saved cart remove button on saved cart details")
    public void clicksSavedCartRemoveButtonOnSavedCartDetails() {
        savedCartDetailsPage.removeSavedCartFromDetails();
    }
}
