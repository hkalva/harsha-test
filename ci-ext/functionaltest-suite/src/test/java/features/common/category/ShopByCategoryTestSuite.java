package features.common.category;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/ShopByCategoryTestResults.html"}, features = "src/test/resources/features/common/shopByCategory.feature",
        glue = {"features.common.authentication", "features.common.category"})
public class ShopByCategoryTestSuite {
}
