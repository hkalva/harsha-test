package features.common.category;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Lists;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CurrentPage;
import pages.SearchResultsPage;
import steps.LoginSteps;
import steps.NavigationSteps;
import steps.SearchSteps;
import util.Elements;

import java.util.Collections;
import java.util.List;

public class ShopByCategoryDefinition {

    @Steps
    LoginSteps loginSteps;

    @Steps
    NavigationSteps navigationSteps;

    @Steps
    SearchSteps searchSteps;

    CurrentPage currentPage;

    SearchResultsPage searchResultsPage;
    String noOfProductsFound;

    @When("Customer clicks on Category {string}")
    public void customer_clicks_on_category(String category) {
        navigationSteps.navigateToCategoryPage(category);
    }

    @Then("Category Page is displayed")
    public void then_category_results_are_displayed() {
        Assertions.assertThat(searchResultsPage.getNoOfProductsOnPage() > 0);
    }

    @Then("Customer scrolls to see more products")
    public void more_products_are_dispayed_() {
        currentPage.waitForLoad();
        final long productCount = searchResultsPage.getNoOfProductsOnPage();
        currentPage.scrollToElement(Elements.Page.FOOTER);
        WebDriverWait webDriverWait = new WebDriverWait(currentPage.getDriver(), 10);
        webDriverWait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return searchResultsPage.getNoOfProductsOnPage() > productCount;
            }
        });
        Assertions.assertThat(searchResultsPage.getNoOfProductsOnPage() > productCount);
    }

    @Then("Customer filters the results by facet {string} with value {string}")
    public void filter_by_facet(String facetName, String facetValue) {
        String noOfProudctsForFacet = searchSteps.getNoOfValuesForFacet(facetName, facetValue);
        noOfProductsFound = searchSteps.getNoOfProductsFound();
        searchSteps.addFacetFilters(facetName, facetValue);
        String noOfProductsWithFilters = searchSteps.getNoOfProductsFound();
        Assertions.assertThat(noOfProductsWithFilters.startsWith(noOfProudctsForFacet));
    }

    @Then("Customer clears the facter filter {string}")
    public void remove_applied_facet(String facetValue) {
        searchSteps.removeFacetFilter(facetValue);
        Assertions.assertThat(noOfProductsFound.equals(searchSteps.getNoOfProductsFound()));
    }

    @Then("Customer sorts the results by {string}")
    public void sortResults(String sortBy) {
        searchSteps.sortResultsBy(sortBy);
        List<String> productNames = searchSteps.getProductNames();
        List<String> names = Lists.newArrayList();
        names.addAll(productNames);
        Collections.sort(names, String.CASE_INSENSITIVE_ORDER);
        if (SearchSteps.SOR_BY_NAME_DESCENDING.equals(sortBy))
            Collections.reverse(names);
        Assertions.assertThat(productNames.equals(names));
    }
}
