package features.common.wishlist;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/WishlistFeatureTestResults.html"},
        glue = {"features.common.authentication", "features.common.wishlist"},
        features = "src/test/resources/features/common/wishlist.feature")
public class WishlistFeatureTestSuite {
}
