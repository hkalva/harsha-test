package features.common.wishlist;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import features.components.AddToCartPopup;
import features.components.AddToWishlistPopup;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import pages.CartPage;
import pages.CurrentPage;
import pages.SearchResultsPage;
import pages.WishlistsPage;
import responsive.ResponsiveUtils;
import steps.LoginSteps;
import steps.NavigationSteps;
import steps.SearchSteps;
import util.Constants;
import util.Elements;
import util.Preconditions;

import java.net.URISyntaxException;
import java.util.Map;
import java.util.Optional;

public class WishlistDefinition {

    @Steps
    LoginSteps loginSteps;

    @Steps
    NavigationSteps navigationSteps;

    @Steps
    SearchSteps searchSteps;

    CurrentPage currentPage;

    WishlistsPage wishlistsPage;
    CartPage cartPage;

    SearchResultsPage searchResultsPage;
    String noOfProductsFound;

    AddToWishlistPopup addToWishlistPopup;
    AddToCartPopup addToCartPopup;
    WebElement wishlist;
    String productCode;
    Map<String, String> cartProducts;
    @Given("Customer {string} has no wishlists")
    public void customer_has_no_wishlists(String email) {
        Preconditions.ensureCustomerHasNoSavedWishlists(email);
    }

    @Given("Customer {string} has an empty cart")
    public void customer_has_an_empty_cart(String email) {
        Preconditions.ensureCustomerCartIsEmpty(email);
        Preconditions.ensureCustomerHasNoSavedCarts(email);
    }

    @Given("customer has logged in using email {string} and password {string}")
    public void customer_has_logged_in_using_email_and_password(String email, String password) {
        loginSteps.openHomePage();
        loginSteps.submitLoginForm(email, password);
    }

    @When("types {string} in search field")
    public void types_in_search_field(String term) {
        if (ResponsiveUtils.isMobile()) {
            searchSteps.toggleMobileSearch();
        }
        searchSteps.typeAndEnterSearchTerm(term);
    }

    @When("clicks on add to wishlist button for product {int}")
    public void clicks_on_add_to_wishlist_button_for_product(Integer product) {
        productCode = searchResultsPage.clickOnAddToWishlist(product);
    }

    @Then("add to wish list modal is displayed")
    public void add_to_wish_list_modal_is_displayed() {
        addToWishlistPopup.isDisplayed();
    }

    @When("customer enter wish list {string}, {string} and clicks on save")
    public void customer_enter_wish_list_and_clicks_on_save(String name, String description) {
        addToWishlistPopup.saveToNewWishlist(name, description);
    }

    @Then("wish list with {string} is created")
    public void wish_list_with_is_created(String wishlistName) throws  URISyntaxException {
        customer_navigates_to_wishlist_page();
        wishlistsPage.waitForLoad();
        Optional<WebElementFacade> webElementOptional = wishlistsPage.getWishlist(wishlistName);
        Assert.assertTrue("Account does not have wishlist : " + wishlistName, webElementOptional.isPresent());
        wishlist = webElementOptional.get();
    }

    @Then("wish list has product")
    public void wish_list_has_product() {
        customer_navigates_to_wishlist_details_page();
        wishlistsPage.hasProduct(productCode);

    }

    @Given("customer navigates to wishlist page")
    public void customer_navigates_to_wishlist_page() throws URISyntaxException {
        navigationSteps.navigateToWishlists();
    }

    @Then("wishlist {string} is displayed")
    public void wishlist_is_displayed(String wishlistName) {
        Optional<WebElementFacade> webElementOptional = wishlistsPage.getWishlist(wishlistName);
        Assert.assertTrue("Account does not have wishlist : " + wishlistName, webElementOptional.isPresent());
        wishlist = webElementOptional.get();
    }

    @When("customer navigates to wishlist details page")
    public void customer_navigates_to_wishlist_details_page() {
        wishlist.click();
    }

    @Then("wishlist products are listed")
    public void wishlist_products_are_listed() {
        Assert.assertTrue("Wishlist doees not have products : ", wishlistsPage.hasProducts());
    }

    @When("customer clicks on add to cart")
    public void customer_clicks_on_add_to_cart() {
        cartProducts = wishlistsPage.addToCart();
    }

    @Then("add to cart modal is diplayed with the product")
    public void add_to_cart_modal_is_diplayed_with_the_product() {
        addToCartPopup.isDispalyed();
    }

    @When("customer click checkout button")
    public void customer_click_checkout_button() {
        addToCartPopup.clickCheckoutButton();
    }

    @Then("cart page is displayed")
    public void cart_page_is_displayed() {
        Assert.assertTrue("Customer is on cart page", cartPage.onPage());
    }

    @Then("product from the wishlist is in the cart")
    public void product_from_the_wishlist_is_in_the_cart() {
        cartPage.hasProducts(cartProducts);
    }

    @When("customer clicks on add all to cart")
    public void customer_clicks_on_add_all_to_cart() {
        cartProducts = wishlistsPage.addAllToCart();
    }

    @Then("All products from the wishlist are in the cart")
    public void all_products_from_the_wishlist_are_in_the_cart() {
        Assert.assertTrue("Product from wishlist are not added to cart ", cartPage.hasProducts(cartProducts));
    }

    @When("customer selectes wishlist {string} clicks on save")
    public void customer_selectes_wishlist_clicks_on_save(String name) {
        addToWishlistPopup.saveToExistingWishlist(name);
    }

    @Then("product is saved to wish list {string}")
    public void product_is_saved_wish_list(String name) throws URISyntaxException {
        customer_navigates_to_wishlist_page();
        wishlist_is_displayed(name);
        wishlist.click();
    }

    @When("customer clicks on remove product")
    public void customer_clicks_on_remove_product() {
        productCode = wishlistsPage.removeProduct();
    }


    @When("customer clicks on delete wishlist")
    public void customer_clicks_on_delete() {
        wishlistsPage.deleteWishlist();;
    }

    @Then("product is removed from the wishlist {string}")
    public void product_is_removed_from_the_wishlist(String string) {
       Assert.assertFalse("Product is not removed from wishlist",  wishlistsPage.hasProduct(productCode));
    }

    @Then("wishlist {string} is deleted")
    public void wishlist_is_removed(String wishlistName) throws URISyntaxException {
        customer_navigates_to_wishlist_page();
        wishlistsPage.waitForLoad();
        Optional<WebElementFacade> webElementOptional = wishlistsPage.getWishlist(wishlistName);
        Assert.assertFalse("Wishlist is not deleted  : " + wishlistName, webElementOptional.isPresent());
    }

    @When("clicks on Add New wishlist")
    public void clicks_on_Add_New_wishlist() {
        wishlistsPage.clickOnAddNew();
    }

    @When("enter wish list {string}, {string} and clicks on save")
    public void enter_wish_list_and_clicks_on_save(String name, String description) {
        wishlistsPage.createWishlist(name, description);
    }
}
