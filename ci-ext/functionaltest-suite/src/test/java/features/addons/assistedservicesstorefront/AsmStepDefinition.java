package features.addons.assistedservicesstorefront;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pages.CurrentPage;
import pages.HomePage;
import pages.LoginPage;

public class AsmStepDefinition {

    LoginPage loginPage;
    HomePage homePage;
    AsmComponent asmComponent;
    CurrentPage currentPage;

    @Given("AS agent at login page")
    public void as_agent_at_login_page() {
        homePage.open();
        homePage.waitForLoad();
        //Assert.assertTrue("Page is : " + homePage.getDriver().getCurrentUrl(), false);
       // Assert.assertTrue("Page is : " + currentPage.getDriver().getCurrentUrl(), loginPage.onPage());
    }

    @Given("navigates to ASM URL")
    public void navigates_to_ASM_URL() {
        loginPage.openUrl(loginPage.getDriver().getCurrentUrl() + "?asm=true");
    }

    @When("agent logs in using username {string} and password {string}")
    public void agent_logs_in_using_username_and_password(String username, String password) {
        currentPage.waitForLoad();
        asmComponent.submitAsmLogin(username, password);
    }

    @Then("agent is authenticated")
    public void agent_is_authenticated() {
        Assert.assertTrue(asmComponent.isAuthenticated());
    }

    @Then("agent is on homepage")
    public void agent_is_homepage() {
        homePage.waitForLoad();
        Assert.assertTrue(homePage.onPage());
    }

}
