package features.addons.assistedservicesstorefront;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import responsive.ResponsiveRunner;

@RunWith(ResponsiveRunner.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports/AsmFeatureTestResults.html"}, features="src/test/resources/features/addons/assistedservicesstorefront", tags = "@asm")
public class AsmFeatureTestSuite {
}
