package features.addons.assistedservicesstorefront;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.steps.UIInteractionSteps;
import org.openqa.selenium.By;

public class AsmComponent extends UIInteractionSteps {

    public static By USERNAME = By.name("username");
    public static By PASSWORD = By.name("password");
    public static By LOGIN_BUTTON = By.className("ASM-btn-login");
    public static By LOGIN_STATUS = By.className("ASM_loggedin");

    @FindBy(name = "username")
    WebElementFacade usernameField;

    @FindBy(name = "password")
    WebElementFacade passwordField;

    @FindBy(className = "ASM-btn-login")
    WebElementFacade asmLoginButton;

    public void submitAsmLogin(String username, String password) {
        usernameField.type(username);
        passwordField.type(password);
        asmLoginButton.click();
    }

    public boolean isAuthenticated() {
        return find(LOGIN_STATUS).isPresent();
    }
}
