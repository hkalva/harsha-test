package responsive;

import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;

public class ResponsiveUtils {

    public static final String DESKTOP = "desktop";
    public static final String TABLET = "tablet";
    public static final String MOBILE = "mobile";


    public static void initResponsive() {
        EnvironmentVariables envVars = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();
        String mode = getMode();
        String width = envVars.getProperty("responsive." + mode + ".width", "1600");
        String height = envVars.getProperty("responsive." + mode + ".height", "700");
        envVars.setProperty("serenity.browser.width", width);
        envVars.setProperty("serenity.browser.height", height);
        System.setProperty("serenity.browser.width", width);
        System.setProperty("serenity.browser.height", height);
    }

    public static String getMode() {
        return Injectors.getInjector().getProvider(EnvironmentVariables.class).get().getProperty("responsive.mode", DESKTOP);
    }

    public static boolean isDesktop() {
        return DESKTOP.equals(getMode());
    }

    public static boolean isTablet() {
        return TABLET.equals(getMode());
    }

    public static boolean isMobile() {
        return MOBILE.equals(getMode());
    }

}
