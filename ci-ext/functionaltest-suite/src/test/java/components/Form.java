package components;

import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.commons.lang3.ArrayUtils;
import org.openqa.selenium.By;

/**
 * Form Abstraction object makes filling out forms easier.
 */
public class Form {

    private WebElementFacade form;

    public Form(final WebElementFacade form) {
        this.form = form;
    }

    /**
     * Type a value into an input field and tab.
     * @param field
     * @param value
     * @return
     */
    public Form typeAndTab(final String field, final String value) {
        form.waitUntilPresent().findBy(By.name(field)).typeAndTab(value);
        return this;
    }

    /**
     * Select a single option from a select dropdown.
     *
     * @param field
     * @param value
     * @return
     */
    public Form select(final String field, final String value) {
        form.waitUntilPresent().findBy(By.name(field)).selectByValue(value);
        return this;
    }

    /**
     * Select a single option from a select dropdown.
     *
     * @param field
     * @param value
     * @return
     */
    public Form selectByVisbleText(final String field, final String value) {
        form.waitUntilPresent().findBy(By.name(field)).selectByVisibleText(value);
        return this;
    }

    /**
     * Select one or more checkboxes having a name matching the specified field and a value contained in the provided
     * values. If the checkbox value is not found in the provided values, it is unchecked.
     *
     * @param field
     * @param values
     * @return
     */
    public Form check(final String field, final String... values) {
        form.waitUntilPresent().findElements(By.name(field)).stream().forEach(element -> {
            final boolean shouldBeChecked = ArrayUtils.contains(values, element.getAttribute("value"));
            if(element.isSelected() && !shouldBeChecked) {
                element.click();
            } else if(!element.isSelected() && shouldBeChecked) {
                element.click();
            }
        });
        return this;
    }

    public Form submit() {
        form.waitUntilPresent().submit();
        return this;
    }

}
