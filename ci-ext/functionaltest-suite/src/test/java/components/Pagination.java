package components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.steps.UIInteractionSteps;

/**
 * Pagination functionality. Use in Steps class as alternative to using actual pages.
 */
public class Pagination extends UIInteractionSteps {

    @FindBy(css = ".pagination-bar.top .pagination-prev")
    WebElementFacade topBarPreviousButton;

    @FindBy(css = ".pagination-bar.bottom .pagination-prev")
    WebElementFacade bottomBarPreviousButton;

    @FindBy(css = ".pagination-bar.top .pagination-next")
    WebElementFacade topBarNextButton;

    @FindBy(css = ".pagination-bar.bottom .pagination-next")
    WebElementFacade bottomBarNextButton;

    public WebElementFacade getTopBarPreviousButton() {
        return topBarPreviousButton;
    }

    public WebElementFacade getBottomBarPreviousButton() {
        return bottomBarPreviousButton;
    }

    public WebElementFacade getTopBarNextButton() {
        scrollToTop();
        return topBarNextButton;
    }

    public WebElementFacade getBottomBarNextButton() {
        return bottomBarNextButton;
    }

    public boolean isDisabled(final WebElementFacade element) {
        return element.getAttribute("class").contains("disabled");
    }

    public boolean hasNext() {
        return !isDisabled(topBarNextButton) && topBarNextButton.isVisible();
    }

    public boolean hasPrevious() {
        return !isDisabled(topBarPreviousButton);
    }

    public void scrollToTop() {
        getJavascriptExecutorFacade().executeScript("window.scrollTo(0,0)");
    }
}
