package util;

import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.commons.lang3.StringUtils;

public class TextUtil {

    public static void clearText(WebElementFacade webElementFacade) {
        while (StringUtils.isNotBlank(webElementFacade.getTextValue())) {
            webElementFacade.sendKeys("\u0008");
            webElementFacade.clear();
        }
    }
}
