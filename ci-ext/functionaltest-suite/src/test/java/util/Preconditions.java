package util;

import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import util.hac.HacClient;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static util.ScriptUtil.executeGroovyScript;
import static util.ScriptUtil.getScript;


/**
 * This utility class contains functions that are intended to streamline test data setup.
 */
public class Preconditions {

    /**
     * Removes customer's existing unsaved carts.
     *
     * @param customerUid
     * @throws IOException
     */
    public static void ensureCustomerCartIsEmpty(final String customerUid) {
        final Map<String, String> params = new HashMap<>();
        params.put("customerUid", customerUid);
        executeGroovyScript(Constants.Template.CLEAR_UNSAVED_CUSTOMER_CARTS, params, true);
    }

    public static void ensureCustomerHasNoSavedCarts(final String customerUid) {
        final Map<String, String> params = new HashMap<>();
        params.put("customerUid", customerUid);
        executeGroovyScript(Constants.Template.CLEAR_SAVED_CARTS, params, true);
    }


    public static void ensureCustomerHasNoSavedWishlists(final String customerUid) {
        final Map<String, String> params = new HashMap<>();
        params.put("customerUid", customerUid);
        executeGroovyScript(Constants.Template.CLEAR_SAVED_WISHLISTS, params, true);
    }

    public static void ensureCustomerHasNoSavedQuotes(final String customerUid) {
        final Map<String, String> params = new HashMap<>();
        params.put("customerUid", customerUid);
        executeGroovyScript(Constants.Template.CLEAR_SAVED_QUOTES, params, true);
    }

    /**
     * Checks if the specified commerce org uid exists. If not, the commerce organization is imported.
     * <p>
     * Each B2BCustomer is created using an email derived from the commerceOrgUid. Please keep the commerceOrgUid
     * descriptive, yet short.
     *
     * @param commerceOrgUid
     */
    public static void ensureCommerceOrgExists(final String commerceOrgUid) {
        if (!DataUtil.hasB2BUnit(commerceOrgUid) /*|| ConfigUtil.isForcePreconditions() */) {
            final Map<String, String> tplParams = new HashMap<>();
            tplParams.put("emailHost", "sapcc-test.capgemini.com"); //TODO: Externalize email host into config file.
            tplParams.put("emailSuffix", commerceOrgUid.toLowerCase().replaceAll("\\s+", StringUtils.EMPTY));
            tplParams.put("orgName", commerceOrgUid);
            HacClient.getInstance().postImpexScript(getScript(Constants.Template.CREATE_COMMERCE_ORG, tplParams));
        }
    }

    /**
     * Checks if the specified commerce org uid exists. If not, the commerce organization is imported.
     * <p>
     * Each B2BCustomer is created using an email derived from the commerceOrgUid. Please keep the commerceOrgUid
     * descriptive, yet short.
     *
     * @param commerceOrgUid
     */
    public static void ensureB2BDocumentsForCommerceOrgExists(final String commerceOrgUid) {
        ensureCommerceOrgExists(commerceOrgUid);
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("orgName", commerceOrgUid);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.CREATE_B2BDOCUMENTS, tplParams));
    }

    /**
     * Set the specified user to a the specified user groups. The groups param is a comma-delimited list.
     *
     * @param uid
     * @param groups
     */
    public static void userHasGroups(final String uid, final String groups) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        tplParams.put("groups", groups);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.SET_USERS_GROUPS, tplParams));
    }

    /**
     * Set the approvers for the specified B2BCustomer. The approvers param is a comma-delimited list of
     * B2BCustomer uids.
     *
     * @param uid
     * @param approvers
     */
    public static void b2bCustomerHasApprovers(final String uid, final String approvers) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        tplParams.put("approvers", approvers);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.SET_B2BCUSTOMER_APPROVERS, tplParams));
    }

    /**
     * Set the B2BPermissions for the specified customer. The permissions param is a comma-delimited list of
     * B2BPermission codes.
     *
     * @param uid
     * @param permissions
     */
    public static void b2bCustomerHasPermissions(final String uid, final String permissions) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        tplParams.put("permissions", permissions);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.SET_B2BCUSTOMER_PERMISSIONS, tplParams));
    }

    /**
     * Removes the B2B customer having the specified UID.
     *
     * @param customerUid
     */
    public static void b2bCustomerDoesNotExist(final String customerUid) {
        final Map<String, String> params = new HashMap<>();
        params.put("customerUid", customerUid);
        executeGroovyScript(Constants.Template.REMOVE_B2BCUSTOMER, params, true);
    }

    public static void b2bPermissionDoesNotExist(final String code) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("permission", code);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.REMOVE_B2BPERMISSION, tplParams));
    }

    public static void b2bCostCenterDoesNotExist(final String code) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("costcenter", code);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.REMOVE_COSTCENTER, tplParams));
    }

    /**
     * Sets the specified B2BPermission's unit attribute to the specified B2BUnit uid.
     *
     * @param code
     * @param uid
     */
    public static void b2bPermissionHasUnit(final String code, final String uid) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("permission", code);
        tplParams.put("unit", uid);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.SET_PERMISSION_UNIT, tplParams));
    }

    public static void userIsInactive(final String uid) {
        final Map<String, Object> fieldValues = new HashMap<>();
        fieldValues.put("p_active", false);
        DataUtil.updateUser(uid, fieldValues);
    }

    public static void userIsActive(final String uid) {
        final Map<String, Object> fieldValues = new HashMap<>();
        fieldValues.put("p_active", true);
        DataUtil.updateUser(uid, fieldValues);
    }

    public static void b2bPermissionIsEnabled(final String code) {
        final Map<String, Object> values = new HashMap<>();
        values.put("p_active", true);
        DataUtil.updateTable("b2bpermissions", values, " p_code = '" + code + "'");
        HacClient.getInstance().clearCache();
    }

    public static void b2bPermissionIsDisabled(final String code) {
        final Map<String, Object> values = new HashMap<>();
        values.put("p_active", false);
        DataUtil.updateTable("b2bpermissions", values, " p_code = '" + code + "'");
        HacClient.getInstance().clearCache();
    }

    public static void b2bCostCenterIsEnabled(final String code) {
        final Map<String, Object> values = new HashMap<>();
        values.put("p_active", true);
        DataUtil.updateTable("b2bcostcenters", values, " p_code = '" + code + "'");
        HacClient.getInstance().clearCache();
    }

    public static void b2bCostCenterIsDisabled(final String code) {
        final Map<String, Object> values = new HashMap<>();
        values.put("p_active", false);
        DataUtil.updateTable("b2bcostcenters", values, " p_code = '" + code + "'");
        HacClient.getInstance().clearCache();
    }

    public static void b2bCostCenterHasBudgets(final String code, final String budgets) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("costCenter", code);
        tplParams.put("budgets", budgets);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.SET_B2BCOSTCENTER_BUDGETS, tplParams));
    }

    public static void b2bCostCenterHasName(final String code, final String name) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("costCenter", code);
        tplParams.put("name", name);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.SET_B2BCOSTCENTER_NAME, tplParams));
    }

    public static void b2bBudgetDoesNotExist(final String code) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("budget", code);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.REMOVE_B2BBUDGET, tplParams));
    }

    public static void b2bBudgetHasName(final String code, final String name) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("budget", code);
        tplParams.put("name", name);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.SET_B2BBUDGET_NAME, tplParams));
    }

    public static void b2bBudgetIsEnabled(final String code) {
        final Map<String, Object> values = new HashMap<>();
        values.put("p_active", true);
        DataUtil.updateTable("b2bbudgets", values, " p_code = '" + code + "'");
        HacClient.getInstance().clearCache();
    }

    public static void b2bBudgetIsDisabled(final String code) {
        final Map<String, Object> values = new HashMap<>();
        values.put("p_active", false);
        DataUtil.updateTable("b2bbudgets", values, " p_code = '" + code + "'");
        HacClient.getInstance().clearCache();
    }

    public static void b2bUserGroupDoesNotExist(final String uid) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.REMOVE_B2BUSERGROUP, tplParams));
    }

    public static void b2bUserGroupHasPermissions(final String uid, final String permissions) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        tplParams.put("permissions", permissions);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.SET_B2BUSERGROUP_PERMISSIONS, tplParams));
    }

    public static void b2bUserGroupHasMembers(final String uid, final String members) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        tplParams.put("members", members);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.SET_B2BUSERGROUP_MEMBERS, tplParams));
    }

    public static void b2bUserGroup(final String uid, final String name, final String unit) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        tplParams.put("name", name);
        tplParams.put("unit", unit);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.CREATE_B2BUSERGROUP, tplParams));
    }

    public static void b2bUnitDoesNotExist(final String uid) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.REMOVE_B2BUNIT, tplParams));
    }

    public static void b2bUnit(final String uid, final String name, final String parentUnit, final String approvalProcess) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        tplParams.put("name", name);
        tplParams.put("parentUnit", parentUnit);
        tplParams.put("approvalProcess", approvalProcess);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.CREATE_B2BUNIT, tplParams));
        executeGroovyScript(Constants.Template.SET_B2BUNIT_ORG_PATH, tplParams, true);
    }

    public static void addressDoesNotExist(final String owner, final String streetname, final String streetnumber,
                                           final String postcode) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("owner", owner);
        tplParams.put("streetname", streetname);
        tplParams.put("streetnumber", streetnumber);
        tplParams.put("postalcode", postcode);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.REMOVE_ADDRESS, tplParams));
    }

    public static void createAddress(String owner, String streetname, String streetnumber, String town, String postcode, String region, String country, String addressId) {

        HacClient.getInstance().postImpexScript(getCreateAddressImpex(owner, streetname, streetnumber, town, postcode, region, country, addressId));
    }

    private static String getCreateAddressImpex(String owner, String streetname, String streetnumber, String town, String postcode, String region, String country, String addressId) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("owner", owner);
        tplParams.put("streetname", streetname);
        tplParams.put("streetnumber", streetnumber);
        tplParams.put("town", town);
        tplParams.put("postalcode", postcode);
        tplParams.put("region", region);
        tplParams.put("country", country);
        tplParams.put("addressId", addressId);

        return getScript(Constants.Template.CREATE_ADDRESS, tplParams);
    }

    public static void createCreditCard(String user,
                                        String number, String validToMonth, String validToYear, String type,
                                        boolean saved, boolean duplicate, String name,
                                        String streetname, String streetnumber, String town,
                                        String postcode, String region, String country, String addressId) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("user", user);
        tplParams.put("addressId", addressId);
        tplParams.put("number", number);
        tplParams.put("validToMonth", validToMonth);
        tplParams.put("validToYear", validToYear);
        tplParams.put("saved", String.valueOf(saved));
        tplParams.put("duplicate", String.valueOf(duplicate));
        tplParams.put("streetname", streetname);
        tplParams.put("streetnumber", streetnumber);
        tplParams.put("town", town);
        tplParams.put("postalcode", postcode);
        tplParams.put("region", region);
        tplParams.put("country", country);
        tplParams.put("addressId", addressId);
        tplParams.put("name", name);
        tplParams.put("type", type);

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(getCreateAddressImpex(user, streetname, streetnumber, town, postcode, region, country, addressId));

        stringBuilder.append(System.lineSeparator());

        stringBuilder.append(getScript(Constants.Template.CREATE_CREDIT_CARD, tplParams));

        HacClient.getInstance().postImpexScript(stringBuilder.toString());

    }

    public static void setUserPassword(final String uid, final String password) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        tplParams.put("password", password);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.UPDATE_PASSWORD, tplParams));
    }

    public static void updateUserId(final String fromUserId, final String toUserId) {
        final Map<String, String> tplParams = new HashMap<>();
        tplParams.put("fromUserId", fromUserId);
        tplParams.put("toUserId", toUserId);
        HacClient.getInstance().postImpexScript(getScript(Constants.Template.UPDATE_UID, tplParams));
    }


    /**
     * Create Test order.
     *
     * @param email
     */
    public static void createOrder(String email, String completedOrderNumber) {

        Map orderParameters = Maps.newHashMap();
        String quantity = "1";
        //Delivery Address
        orderParameters.put(Constants.Address.ADDRESS_ID, "deliveryAddress");
        orderParameters.put(Constants.Address.OWNER, email);
        orderParameters.put(Constants.Address.STREET_NAME, "Street Name");
        orderParameters.put(Constants.Address.STREET_NUMBER, "1234");
        orderParameters.put(Constants.Address.POSTAL_CODE, "22334");
        orderParameters.put(Constants.Address.REGION, "IL");
        orderParameters.put(Constants.Address.COUNTRY, "US");
        orderParameters.put(Constants.Address.TOWN, "MyTown");

        //Order
        String date = LocalDateTime.now().format(TestUtil.DATE_TIME_FORMATTER);
        orderParameters.put(Constants.Order.ID, completedOrderNumber);
        orderParameters.put(Constants.Order.ORDER_UNIT, "Custom Retail");
        orderParameters.put(Constants.Order.CALCULATED, "TRUE");
        orderParameters.put(Constants.Order.CODE, completedOrderNumber);
        orderParameters.put(Constants.Order.CURRENCY, "USD");
        orderParameters.put(Constants.Order.DATE, date);
        orderParameters.put(Constants.Order.DELIVERY_ADDRESS, "deliveryAddress");
        orderParameters.put(Constants.Order.DELIVERY_COST, "9.99");
        orderParameters.put(Constants.Order.DELIVERY_MODE, "standard-net");
        orderParameters.put(Constants.Order.DELIVERY_STATUS, "SHIPPED");
        orderParameters.put(Constants.Order.DISCOUNT_INCLUDE_DELIVERY_COST, "FALSE");
        orderParameters.put(Constants.Order.DISCOUNT_INCLUDE_PAYMENT_COST, "FALSE");
        orderParameters.put(Constants.Order.EXPIRATION_TIME, date);
        orderParameters.put(Constants.Order.FRAUDULENT, "FALSE");
        orderParameters.put(Constants.Order.LANGUAGE, "en");
        orderParameters.put(Constants.Order.NET, "TRUE");
        orderParameters.put(Constants.Order.PAYMENT_TYPE, "CARD");
        //orderParameters.put(Constants.Order.PAYMENT_TYPE, "ACCOUNT");

        orderParameters.put(Constants.Order.POTENTIALLY_FRAUDULENT, "FALSE");
        orderParameters.put(Constants.Order.SALES_APPLICATION, "Web");
        orderParameters.put(Constants.Order.SITE, "rlp");
        orderParameters.put(Constants.Order.STATUS, "COMPLETED");
        orderParameters.put(Constants.Order.STORE, "rlp");
        orderParameters.put(Constants.Order.SUB_TOTAL, "194");
        orderParameters.put(Constants.Order.TOTAL_DISCOUNTS, "0");
        orderParameters.put(Constants.Order.TOTAL_PRICE, "203.99");
        orderParameters.put(Constants.Order.TOTAL_TAX, "0");
        orderParameters.put(Constants.Order.USER, email);

        //Order Entry
        orderParameters.put(Constants.OrderEntry.ID, "0");
        orderParameters.put(Constants.OrderEntry.BASE_PRICE, "194");
        orderParameters.put(Constants.OrderEntry.BUNDLE_NO, "0");
        orderParameters.put(Constants.OrderEntry.COST_CENTER, "Services West");
        orderParameters.put(Constants.OrderEntry.ENTRY_NUMBER, "0");
        orderParameters.put(Constants.OrderEntry.GIVE_AWAY, "FALSE");
        orderParameters.put(Constants.OrderEntry.ORDER_CODE, completedOrderNumber);
        orderParameters.put(Constants.OrderEntry.PRODUCT_CODE, "3778927");
        orderParameters.put(Constants.OrderEntry.PRODUCT_CATALOG, "rlpProductCatalog");
        orderParameters.put(Constants.OrderEntry.CATALOG_VERSION, "Online");
        orderParameters.put(Constants.OrderEntry.QUANTITY, quantity);
        orderParameters.put(Constants.OrderEntry.REJECTED, "FALSE");
        orderParameters.put(Constants.OrderEntry.TOTAL_PRICE, "194");
        orderParameters.put(Constants.OrderEntry.UNIT, "pieces");
        orderParameters.put(Constants.OrderEntry.STATUS, "SHIPPED");

        orderParameters.put(Constants.Consignment.ID, "consignment");
        orderParameters.put(Constants.Consignment.CODE, "A" + completedOrderNumber);
        orderParameters.put(Constants.Consignment.ORDER_CODE, completedOrderNumber);
        orderParameters.put(Constants.Consignment.DELIVERY_MODE, "standard-net");
        orderParameters.put(Constants.Consignment.SHIPPING_ADDRESS, "deliveryAddress");
        orderParameters.put(Constants.Consignment.SHIPPING_DATE, date);
        orderParameters.put(Constants.Consignment.STATUS, "SHIPPED");

        orderParameters.put(Constants.ConsignmentEntry.ID, "consignmentEntry");
        orderParameters.put(Constants.ConsignmentEntry.CODE, "A" + completedOrderNumber);
        orderParameters.put(Constants.ConsignmentEntry.ORDER_CODE, completedOrderNumber);
        orderParameters.put(Constants.ConsignmentEntry.ORDER_ENTRY, "0");
        orderParameters.put(Constants.ConsignmentEntry.QUQNTITY, quantity);
        orderParameters.put(Constants.ConsignmentEntry.SHIPPED_QUANTITY, quantity);


        StringBuilder impexBuilder = new StringBuilder();
        impexBuilder.append(getScript(Constants.Template.CREATE_ORDER, orderParameters));
        ;
        HacClient.getInstance().postImpexScript(impexBuilder.toString());
    }

    /**
     * Create a return request
     * @param email
     * @param status return request status
     * @param code
     */
    public static void createReturnRequest(String email, String status, String code) {

        Map orderParameters = Maps.newHashMap();
        String date = LocalDateTime.now().format(TestUtil.DATE_TIME_FORMATTER);
        orderParameters.put(Constants.Order.CODE, code);
        orderParameters.put(Constants.Order.DATE, date);
        orderParameters.put(Constants.Order.USER, email);
        orderParameters.put(Constants.Order.STATUS, status);


        StringBuilder impexBuilder = new StringBuilder();
        impexBuilder.append(getScript(Constants.Template.RETURN_REEQUEST, orderParameters));

        HacClient.getInstance().postImpexScript(impexBuilder.toString());
    }

    /**
     * Create Test order.
     *
     * @param email
     */
    public static void createCartForQuote(String email, String productCode, String quantity, String cartId) {

        Map orderParameters = Maps.newHashMap();

        //Cart
        String date = LocalDateTime.now().format(TestUtil.DATE_TIME_FORMATTER);
        orderParameters.put(Constants.Order.ID, cartId);
        orderParameters.put(Constants.Order.ORDER_UNIT, "Custom Retail");
        orderParameters.put(Constants.Order.CALCULATED, "false");
        orderParameters.put(Constants.Order.CODE, cartId);
        orderParameters.put(Constants.Order.CURRENCY, "USD");
        orderParameters.put(Constants.Order.DATE, date);
        orderParameters.put(Constants.Order.SITE, "rlp");
        orderParameters.put(Constants.Order.STORE, "rlp");
        orderParameters.put(Constants.Order.USER, email);

        //Order Entry
        orderParameters.put(Constants.OrderEntry.ID, "0");
        //orderParameters.put(Constants.OrderEntry.BASE_PRICE, "194");
        orderParameters.put(Constants.OrderEntry.ENTRY_NUMBER, "0");
        orderParameters.put(Constants.OrderEntry.ORDER_CODE, cartId);
        orderParameters.put(Constants.OrderEntry.PRODUCT_CODE, productCode);
        orderParameters.put(Constants.OrderEntry.QUANTITY, quantity);


        StringBuilder impexBuilder = new StringBuilder();
        impexBuilder.append(getScript(Constants.Template.CREATE_CART_FOR_QUOTE, orderParameters));

        HacClient.getInstance().postImpexScript(impexBuilder.toString());
    }
}
