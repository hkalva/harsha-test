package util;

import java.nio.charset.Charset;

/**
 * Common Constants File.
 */
public class Constants {

    public final static Charset UTF8_CHARSET = Charset.forName("UTF-8");
    public final static String NEWLINE = "\n";

    /**
     * Config Properties.
     */
    public static class Properties {
        public final static String PRECONDITIONS_FORCE = "preconditions.force";
    }

    /**
     * Templates
     */
    public static class Template {
        public final static String REMOVE_ADDRESS = "setup/impex/removeAddress.impex";
        public final static String CREATE_ADDRESS = "setup/impex/createAddress.impex";
        public final static String UPDATE_PASSWORD = "setup/impex/updateUserPassword.impex";
        public final static String UPDATE_UID = "setup/impex/updateUserUId.impex";
        public final static String CREATE_CREDIT_CARD = "setup/impex/createCreditCard.impex";
        public final static String CREATE_B2BUNIT = "setup/impex/b2b/createB2BUnit.impex";
        public final static String REMOVE_B2BUNIT = "setup/impex/b2b/removeB2BUnit.impex";
        public final static String SET_B2BUSERGROUP_MEMBERS = "setup/impex/b2b/setB2BUserGroupMembers.impex";
        public final static String SET_B2BUSERGROUP_PERMISSIONS = "setup/impex/b2b/setB2BUserGroupPermissions.impex";
        public final static String CREATE_B2BUSERGROUP = "setup/impex/b2b/createB2BUserGroup.impex";
        public final static String REMOVE_B2BUSERGROUP = "setup/impex/b2b/removeB2BUserGroup.impex";
        public final static String SET_B2BBUDGET_NAME = "setup/impex/b2b/setB2BBudgetName.impex";
        public final static String REMOVE_B2BBUDGET = "setup/impex/b2b/removeB2BBudget.impex";
        public final static String REMOVE_COSTCENTER = "setup/impex/b2b/removeCostCenter.impex";
        public final static String SET_B2BCOSTCENTER_NAME = "setup/impex/b2b/setB2BCostCenterName.impex";
        public final static String SET_B2BCOSTCENTER_BUDGETS = "setup/impex/b2b/setB2BCostCenterBudgets.impex";
        public final static String SET_PERMISSION_UNIT = "setup/impex/b2b/setPermissionUnit.impex";
        public final static String REMOVE_B2BPERMISSION = "setup/impex/b2b/removePermission.impex";
        public final static String SET_USERS_GROUPS = "setup/impex/setUsersGroups.impex";
        public final static String SET_B2BCUSTOMER_APPROVERS = "setup/impex/b2b/setB2BCustomerApprovers.impex";
        public final static String SET_B2BCUSTOMER_PERMISSIONS = "setup/impex/b2b/setB2BCustomerPermissions.impex";
        public final static String CREATE_COMMERCE_ORG = "setup/impex/b2b/createCommerceOrg.impex";
        public final static String CREATE_B2BDOCUMENTS = "setup/impex/b2b/createB2BDocuments.impex";
        public final static String CLEAR_UNSAVED_CUSTOMER_CARTS = "clearUnsavedCustomerCarts";
        public final static String CLEAR_SAVED_CARTS = "clearSavedCustomerCarts";
        public final static String CLEAR_SAVED_WISHLISTS = "clearSavedCustomerWishlists";
        public final static String CLEAR_SAVED_QUOTES = "clearCustomerQuotes";
        public final static String REMOVE_B2BCUSTOMER = "removeB2BCustomer";
        public final static String SET_B2BUNIT_ORG_PATH = "setB2BUnitOrgPath";
        public final static String CREATE_ORDER = "setup/impex/createOrder.impex";
        public final static String CREATE_CART_FOR_QUOTE = "setup/impex/createCartForQuote.impex";
        public final static String RETURN_REEQUEST = "setup/impex/returnRequest.impex";
        public final static String CREATE_ORDER_ENTRY = "setup/impex/createOrderEntry.impex";
        public final static String CREATE_CONSIGNMENT = "setup/impex/createConsignment.impex";
        public final static String CREATE_CONSIGNMENT_ENTRY = "setup/impex/createConsignmentEntry.impex";
        public final static String REMOVE_ORDER = "setup/impex/removeOrder.impex";
        public final static String REMOVE_CONSIGNMENT = "setup/impex/removeConsignment.impex";
    }

    public static class Search {
        public final static String APPLIED_FACETS = "Applied Facets";
    }

    public static class SaveCart {
        public final static String BREADCRUMB = "Saved Cart";
        public final static String SAVED_CART_PAGE_TILE_PREFIX = "Saved Cart Details";
    }

    public static class Account {
        public final static String PERSONL_DETAILS = "Personal Details";
        public final static String PAYMENT_DETAILS = "Payment Details";
        public final static String CREDIT_CARD_DELETED = "Payment Card removed successfully";
        public final static String PASSWORD_UPDATED = "Your password has been changed";
        public final static String PASSWORD = "Password";
        public final static String ADDRESS_BOOK = "Address Book";
        public final static String ADD_ADDRESS = "Add Address";
        public final static String RETURN_HISTORY = "Returns History";
        public final static String ORDER_HISTORY = "Order History";
        public final static String RETURN_REQUEST_SUCCESS = "Request for Return submitted successfully.";
        public final static String EMAIL_ADDRESS = "Email Address";
        public final static String WISHLISTS = "Wishlists";
        public final static String QUOTES = "Quotes";

        public static class Breadcrumb {
            public final static String PROFILE = "Profile";
            public final static String PAYMENT_DETAILS = "Payment Details";
            public final static String UPDATE_PASSWORD = "Update Password";
            public final static String EDIT_ADDRESS_BOOK = "Add/Edit Address";
            public final static String ADDRESS_BOOK = "Address Book";
            public final static String ORDER_DETAILS = "Order ";
            public final static String ORDER_HISTORY = "Order History";
        }
    }

    /**
     * Flexible Search Queries
     */
    public static class FlexQueries {

        /**
         * Flex query that will return the value of the p_logindisabled field (true or false).
         */
        public final static String GET_CUSTOMER_LOGIN_DISABLED_STATUS =
                "select {loginDisabled} from {Customer} where {uid} = ?uid";

        /**
         * Find all principal group ids associated with a specific user uid.
         */
        public final static String GET_USERS_GROUPS =
                "select {g.uid} from {PrincipalGroupRelation as pgr " +
                        "join User as u on {u.pk} = {pgr.source} and {u.uid} = ?uid " +
                        "join PrincipalGroup as g on {g.pk} = {pgr.target}}";

        public final static String GET_B2BCUSTOMER_PERMISSIONS =
                "select {p.code} from {B2BPermission as p " +
                        "join B2BCustomer2B2BPermissions as c2p on {c2p.target} = {p.pk} " +
                        "join B2BCustomer as c on {c.pk} = {c2p.source} and {c.uid} = ?uid}";

        public final static String GET_B2BCUSTOMER_APPROVERS =
                "select {a.uid} from {B2BCustomer as a " +
                        "join B2BCustomers2Approvers as c2a on {c2a.target} = {a.pk} " +
                        "join B2BCustomer as c on {c.pk} = {c2a.source} and {c.uid} = ?uid}";

        public final static String GET_B2BPERMISSION_UNIT =
                "select {u.uid} from {B2BUnit as u join B2BPermission as p on {p.unit} = {u.pk} and {p.code} = ?code}";

        public final static String GET_B2BPERMISSION_ACTIVE_VAL = "select {active} from {B2BPermission} where {code} = ?code";
        public final static String GET_B2BCOSTCENTER_ACTIVE_VAL = "select {active} from {B2BCostCenter} where {code} = ?code";
        public final static String GET_B2BBUDGET_ACTIVE_VAL = "select {active} from {B2BBudget} where {code} = ?code";

        public final static String GET_B2BCOSTCENTER_BUDGETS =
                "select {b.code} from {B2BBudgets2CostCenters as b2c " +
                        "join B2BBudget as b on {b.pk} = {b2c.source} " +
                        "join B2BCostCenter as c on {c.pk} = {b2c.target} and {c.code} = ?code}";

        public final static String GET_B2BCOSTCENTER_NAME = "select {name[en]} from {B2BCostCenter} where {code} = ?code";
        public final static String GET_B2BBUDGET_NAME = "select {name[en]} from {B2BBudget} where {code} = ?code";

        public final static String GET_B2BUSERGROUP_PERMISSIONS =
                "select {p.code} from {B2BUserGroups2B2BPermissions as g2p " +
                        "join B2BPermission as p on {p.pk} = {g2p.target} " +
                        "join B2BUserGroup as g on {g.pk} = {g2p.source} and {g.uid} = ?uid}";

        public final static String GET_B2BUSERGROUP_USERS =
                "select {u.uid} from {PrincipalGroupRelation as pgr " +
                        "join User as u on {u.pk} = {pgr.source} " +
                        "join B2BUserGroup as g on {g.pk} = {pgr.target} and {g.uid} = ?uid }";

        public final static String GET_B2BUSERGROUP_DATA =
                "select {g.uid}, {g.name}, {u.uid} as unit from {B2BUserGroup as g " +
                        "join B2BUnit as u on {u.pk} = {g.unit} and {g.uid} = ?uid}";

        public final static String GET_B2BUNIT_DATA =
                "select {u.uid} as uid, {u.name} as name, {p.uid} as parentUnit, {u.approvalProcessCode} as " +
                        "approvalProcessCode from {B2BUnit as u " +
                        "left join PrincipalGroupRelation as pgr on {pgr.source} = {u.pk} " +
                        "join B2BUnit as p on {p.pk} = {pgr.target}} " +
                        "where {u.uid} = ?uid";

        public final static String GET_B2BUNIT_ADDRESSES =
                "select {a.pk},{a.streetname}, {a.streetnumber}, {a.postalcode}, {a.town}, {t.code} as title,{a.firstName}, " +
                        "{a.lastName}, {c.isocode} as countryIso from {Address as a join B2BUnit as u on {u.pk} = {a.owner} " +
                        "and {u.uid} = ?uid join Country as c on {c.pk} = {a.country} join Title as t on {t.pk} = {a.title}}";

        public final static String ORDER_PROCESSES = "select {op:pk}  from {OrderProcess as op " +
                "join Order as o on {op:order} = {o:pk}} " +
                "where {o:code} = ?code ";

        public final static String GET_RETURN_ORDER = "select {rr:code} from {ReturnRequest as rr " +
                "join Order as o on {o:pk} = {rr:order} " +
                "} where {o:code} = ?code and {o:versionID} is null";
    }

    /**
     * Native SQL Queries.
     */
    public static class SqlQueries {

        /**
         * SQL Query that sets the user's login disabled flag to false.
         */
        public final static String UNLOCK_USER_ACCOUNT =
                "update users set p_logindisabled = false where p_uid = ?uid";
    }


    public static class Categories {
        public static final String HAND_TOOLS = "Hand Tools";
    }

    public static class QuickView {
        public static final String ADDED_TO_CART = "Added to Your Shopping Cart";
    }

    public static class Order {
        public static final String ID = "orderId";
        public static final String ORDER_UNIT = "orderUnit";
        public static final String CALCULATED = "calculated";
        public static final String CODE = "orderCode";
        public static final String CURRENCY = "currency";
        public static final String DATE = "date";
        public static final String DELIVERY_ADDRESS = "deliveryAddress";
        public static final String DELIVERY_COST = "deliveryCost";
        public static final String DELIVERY_MODE = "deliveryMode";
        public static final String DELIVERY_STATUS = "deliveryStatus";
        public static final String DISCOUNT_INCLUDE_DELIVERY_COST = "discountsIncludeDeliveryCost";
        public static final String DISCOUNT_INCLUDE_PAYMENT_COST = "discountsIncludePaymentCost";
        public static final String LANGUAGE = "language";
        public static final String NET = "net";
        public static final String PAYMENT_TYPE = "paymentType";
        public static final String POTENTIALLY_FRAUDULENT = "potentiallyFraudulent";
        public static final String SALES_APPLICATION = "salesApplication";
        public static final String SITE = "site";
        public static final String STATUS = "orderStatus";
        public static final String STORE = "store";
        public static final String SUB_TOTAL = "subtotal";
        public static final String TOTAL_DISCOUNTS = "totalDiscounts";
        public static final String TOTAL_PRICE = "totalPrice";
        public static final String TOTAL_TAX = "totalTax";
        public static final String USER = "user";
        public static final String EXPIRATION_TIME = "expirationTime";
        public static final String FRAUDULENT = "fraudulent";
    }

    public static class OrderEntry {

        // Order Entry
        public static final String ID = "orderEntryId";
        public static final String BASE_PRICE = "basePrice";
        public static final String BUNDLE_NO = "bundleNo";
        public static final String COST_CENTER = "costCenter";
        public static final String ENTRY_NUMBER = "entryNumber";
        public static final String GIVE_AWAY = "giveAway";
        public static final String ORDER_CODE = "orderCode";
        public static final String PRODUCT_CATALOG = "productCatalog";
        public static final String CATALOG_VERSION = "catalogVersion";
        public static final String PRODUCT_CODE = "productCode";
        public static final String QUANTITY = "quantity";
        public static final String REJECTED = "rejected";
        public static final String UNIT = "unit";
        public static final String TOTAL_PRICE = "totalPrice";
        public static final String STATUS = "orderEntryStatus";

    }

    public static class Address {
        public static final String OWNER = "owner";
        public static final String ADDRESS_ID = "addressId";
        public static final String STREET_NAME = "streetname";
        public static final String STREET_NUMBER = "streetnumber";
        public static final String TOWN = "town";
        public static final String POSTAL_CODE = "postalcode";
        public static final String REGION = "region";
        public static final String COUNTRY = "country";
    }

    public static class Consignment {
        public static final String ID = "consignmentId";
        public static final String CODE = "consignmentCode";
        public static final String DELIVERY_MODE = "deliveryMode";
        public static final String ORDER_CODE = "orderCode";
        public static final String SHIPPING_ADDRESS = "shippingAddress";
        public static final String SHIPPING_DATE = "shippingDate";
        public static final String STATUS = "status";
    }

    public static class ConsignmentEntry {
        public static final String ID = "consignmentEntryId";
        public static final String CODE = "consignmentCode";
        public static final String ORDER_CODE = "orderCode";
        public static final String ORDER_ENTRY = "orderEntry";
        public static final String QUQNTITY = "quantity";
        public static final String SHIPPED_QUANTITY = "shippedQuantity";
    }

    public static class Quote {
        public static final String DRAFT = "DRAFT";
        public static final String CANCELLED = "CANCELLED";
        public static final String SUBMITTED = "SUBMITTED";
        public static final String BUYER_DRAFT = "BUYER_DRAFT";
    }
}
