package util;

import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/**
 * Test Utility Functions.
 */
public class TestUtil {

    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.mm.yyyy hh:mm:ss");

    /**
     * Wait for a specified number of seconds.
     *
     * @param seconds
     */
    public static void pause(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Cleans codes and uids that to match codes and uids in hybris element classes and ids.
     *
     * @param value
     * @return
     */
    public static String clean(final String value) {
        return value.replaceAll("-|@|\\.|\\s", "_");
    }
}
