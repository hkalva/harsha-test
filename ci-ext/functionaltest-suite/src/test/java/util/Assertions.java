package util;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import util.hac.HacClient;
import util.hac.dto.FlexSearchResponse;

import java.util.*;

/**
 * Custom functional test suite assertions.
 */
public class Assertions {


    public static void assertCustomerAccountIsLocked(final String uid) {
        final Map<String, Object> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_CUSTOMER_LOGIN_DISABLED_STATUS, tplParams);
        Assert.assertTrue(Boolean.valueOf(res.getResults().get(0).get("p_logindisabled")));
    }

    public static void assertB2BCustomerExists(final String uid) {
        Assert.assertTrue(DataUtil.hasB2BCustomer(uid));
    }

    public static void assertB2BCustomerInactive(final String uid) {
        final Optional<Map<String, String>> customerData = DataUtil.getB2BCustomerData(uid, "active");
        Assert.assertTrue("B2BCustomer should exist, but was not found", customerData.isPresent());
        Assert.assertEquals("false", customerData.get().get("p_active"));
    }

    public static void assertB2BCustomerActive(final String uid) {
        final Optional<Map<String, String>> customerData = DataUtil.getB2BCustomerData(uid, "active");
        Assert.assertTrue("B2BCustomer should exist, but was not found", customerData.isPresent());
        Assert.assertEquals("true", customerData.get().get("p_active"));
    }


    /**
     * Assert the user's principal groups contain exactly the specified groups. The expected groups are specified as a
     * comma-delimited list.
     *
     * @param uid
     * @param expectedGroups
     */
    public static void assertUserHasGroups(final String uid, final String expectedGroups) {
        assertUserHasGroups(uid, Arrays.asList(StringUtils.split(expectedGroups, ",")));
    }

    /**
     * Assert the user's principal groups contain exactly the specified groups.
     *
     * @param uid
     * @param expectedGroups
     */
    public static void assertUserHasGroups(final String uid, final List<String> expectedGroups) {
        final List<String> actualGroups = DataUtil.getUsersGroups(uid);
        Assert.assertEquals(actualGroups.size(), expectedGroups.size());
        Assert.assertTrue(actualGroups.containsAll(expectedGroups));
    }

    /**
     * Assert the B2BCustomer has exactly the specified B2BPermissions.
     *
     * @param uid
     * @param expectedPermissions
     */
    public static void assertB2BCustomerHasPermissions(final String uid, final String expectedPermissions) {
        assertB2BCustomerHasPermissions(uid, Arrays.asList(StringUtils.split(expectedPermissions, ",")));
    }

    /**
     * Assert the B2BCustomer has exactly the specified B2BPermissions.
     *
     * @param uid
     * @param expectedPermissions
     */
    public static void assertB2BCustomerHasPermissions(final String uid, final List<String> expectedPermissions) {
        final List<String> actualPermissions = DataUtil.getB2BCustomerPermissions(uid);
        Assert.assertEquals(actualPermissions.size(), expectedPermissions.size());
        Assert.assertTrue(actualPermissions.containsAll(expectedPermissions));
    }

    /**
     * Assert the B2BCustomer has exactly the specified Approvers.
     *
     * @param uid
     * @param expectedApprovers
     */
    public static void assertB2BCustomerHasApprovers(final String uid, final String expectedApprovers) {
        assertB2BCustomerHasApprovers(uid, Arrays.asList(StringUtils.split(expectedApprovers, ",")));
    }

    /**
     * Assert the B2BCustomer has exactly the specified Approvers.
     *
     * @param uid
     * @param expectedApprovers
     */
    public static void assertB2BCustomerHasApprovers(final String uid, final List<String> expectedApprovers) {
        final List<String> actualApprovers = DataUtil.getB2BCustomerApprovers(uid);
        Assert.assertEquals(actualApprovers.size(), expectedApprovers.size());
        Assert.assertTrue(actualApprovers.containsAll(expectedApprovers));
    }

    public static void assertB2BPermissionExists(final String code) {
        Assert.assertTrue(String.format("No B2BPermission with code %s", code), DataUtil.hasB2BPermission(code));
    }

    public static void assertB2BPermissionUnit(final String permissionCode, final String expectedUnitUid) {
        final Map<String, Object> tplParams = new HashMap<>();
        tplParams.put("code", permissionCode);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_B2BPERMISSION_UNIT, tplParams);
        Assert.assertEquals(expectedUnitUid, res.getResults().get(0).get("p_uid"));
    }

    public static void assertB2BPermissionEnabled(final String permissionCode) {
        final Map<String, Object> tplParams = new HashMap<>();
        tplParams.put("code", permissionCode);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_B2BPERMISSION_ACTIVE_VAL, tplParams);
        Assert.assertEquals("true", res.getResults().get(0).get("p_active"));
    }

    public static void assertB2BPermissionDisabled(final String permissionCode) {
        final Map<String, Object> tplParams = new HashMap<>();
        tplParams.put("code", permissionCode);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_B2BPERMISSION_ACTIVE_VAL, tplParams);
        Assert.assertEquals("false", res.getResults().get(0).get("p_active"));
    }

    public static void assertB2BCostCenterExists(final String code) {
        Assert.assertTrue(String.format("No B2BCostCenter with code %s", code), DataUtil.hasB2BCostCenter(code));
    }

    public static void assertB2BCostCenterEnabled(final String costCenterCode) {
        final Map<String, Object> tplParams = new HashMap<>();
        tplParams.put("code", costCenterCode);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_B2BCOSTCENTER_ACTIVE_VAL, tplParams);
        Assert.assertEquals("true", res.getResults().get(0).get("p_active"));
    }

    public static void assertB2BCostCenterDisabled(final String costCenterCode) {
        final Map<String, Object> tplParams = new HashMap<>();
        tplParams.put("code", costCenterCode);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_B2BCOSTCENTER_ACTIVE_VAL, tplParams);
        Assert.assertEquals("false", res.getResults().get(0).get("p_active"));
    }

    public static void assertB2BCostCenterHasBudgets(final String code, final List<String> expectedBudgets) {
        final List<String> actualBudgets = DataUtil.getB2BCostCenterBudgets(code);
        Assert.assertEquals(expectedBudgets.size(), actualBudgets.size());
        Assert.assertTrue(actualBudgets.containsAll(expectedBudgets));
    }

    public static void assertB2BCostCenterName(final String costCenterCode, final String expectedName) {
        final Map<String, Object> tplParams = new HashMap<>();
        tplParams.put("code", costCenterCode);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_B2BCOSTCENTER_NAME, tplParams);
        Assert.assertEquals(expectedName, res.getResults().get(0).get("p_name"));
    }

    public static void assertB2BBudgetName(final String code, final String expectedName) {
        final Map<String, Object> tplParams = new HashMap<>();
        tplParams.put("code", code);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_B2BBUDGET_NAME, tplParams);
        Assert.assertEquals(expectedName, res.getResults().get(0).get("p_name"));
    }

    public static void assertB2BBudgetExists(final String code) {
        Assert.assertTrue(String.format("No B2BBudget with code %s", code), DataUtil.hasB2BBudget(code));
    }

    public static void assertB2BBudgetEnabled(final String code) {
        final Map<String, Object> tplParams = new HashMap<>();
        tplParams.put("code", code);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_B2BBUDGET_ACTIVE_VAL, tplParams);
        Assert.assertEquals("true", res.getResults().get(0).get("p_active"));
    }

    public static void assertB2BBudgetDisabled(final String code) {
        final Map<String, Object> tplParams = new HashMap<>();
        tplParams.put("code", code);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_B2BBUDGET_ACTIVE_VAL, tplParams);
        Assert.assertEquals("false", res.getResults().get(0).get("p_active"));
    }

    public static void assertB2BUserGroupHasPermissions(final String uid, final List<String> expectedPermissions) {
        final List<String> actual = DataUtil.getB2BUserGroupPermissions(uid);
        Assert.assertEquals(expectedPermissions.size(), actual.size());
        Assert.assertTrue(actual.containsAll(expectedPermissions));
    }

    public static void assertB2BUserGroupHasUsers(final String uid, final List<String> expectedUsers) {
        final List<String> actual = DataUtil.getB2BUserGroupMembers(uid);
        Assert.assertEquals(expectedUsers.size(), actual.size());
        Assert.assertTrue(actual.containsAll(expectedUsers));
    }

    /**
     * A user group is considered "disabled" when it has no users.
     *
     * @param uid
     */
    public static void assertB2BUserGroupIsEmpty(final String uid) {
        Assert.assertTrue(DataUtil.getB2BUserGroupMembers(uid).isEmpty());
    }

    /**
     * Asserts the existence of a B2BUserGroup matching the specified name and unit.
     *
     * @param uid
     * @param name
     * @param unit
     */
    public static void assertB2BUserGroup(final String uid, final String name, final String unit) {
        final Map<String, Object> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_B2BUSERGROUP_DATA, tplParams);
        Assert.assertFalse(res.getResultList().isEmpty());
        Assert.assertEquals(name, res.getResults().get(0).get("p_name"));
        Assert.assertEquals(unit, res.getResults().get(0).get("unit"));
    }

    public static void assertB2BUserGroupDoesNotExist(final String uid) {
        Assert.assertFalse(DataUtil.hasB2BUserGroup(uid));
    }

    public static void assertB2BUnitExists(final String uid) {
        Assert.assertTrue(DataUtil.hasB2BUnit(uid));
    }

    /**
     * Assert that a B2BUnit exists having the specified uid, name, parent unit and approvalProcessCode.
     *
     * @param uid
     * @param name
     * @param parentUnit
     * @param approvalProcessCode
     */
    public static void assertB2BUnit(final String uid, final String name, final String parentUnit, final String approvalProcessCode) {
        final Map<String, Object> params = new HashMap<>();
        params.put("uid", uid);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_B2BUNIT_DATA, params);
        Assert.assertFalse(res.getResultList().isEmpty());
        Assert.assertEquals(name, res.getResults().get(0).get("name"));
        Assert.assertEquals(uid, res.getResults().get(0).get("uid"));
        Assert.assertEquals(parentUnit, res.getResults().get(0).get("parentUnit"));
        Assert.assertEquals(approvalProcessCode, res.getResults().get(0).get("approvalProcessCode"));
    }

    public static void assertB2BUnitAddress(final String uid, final String countryIso, final String titleCode, final String firstName, final String lastName, final String streetname, final String streetnumber, final String city, final String postalcode) {
        final Map<String, Object> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_B2BUNIT_ADDRESSES, tplParams);
        Assert.assertTrue(res.getResults().stream().anyMatch(a ->
                streetname.equals(a.get("p_streetname")) &&
                        streetnumber.equals(a.get("p_streetnumber")) &&
                        city.equals(a.get("p_town")) &&
                        countryIso.equals(a.get("countryIso")) &&
                        firstName.equals(a.get("p_firstname")) &&
                        lastName.equals(a.get("p_lastname")) &&
                        titleCode.equals(a.get("title"))
        ));
    }

    public static void assertOrderProcessStarted(final String code) {
        final Map<String, Object> params = new HashMap<>();
        params.put("code", code);
        final FlexSearchResponse response = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.ORDER_PROCESSES, params);
        Assert.assertTrue(!response.getResults().isEmpty());
    }

    public static void assertPasswordChanged(String uid, String oldPassword) {
        Assert.assertFalse(oldPassword.equals(getCustomerPassword(uid)));
    }

    public static void assertPasswordNotChanged(String uid, String oldPassword) {
        Assert.assertTrue(oldPassword.equals(getCustomerPassword(uid)));
    }

    public static void assertReturnRequestIsCreatedForOrder(String orderNumber) {
        final Map<String, Object> params = new HashMap<>();
        params.put("code", orderNumber);
        final FlexSearchResponse response = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_RETURN_ORDER, params);
        Assert.assertTrue(!response.getResults().isEmpty());
    }

    public static String getCustomerPassword(String uid) {
        final String ENCODED_PASSWORD = "encodedPassword";
        final String PASSWD = "passwd";
        Optional<Map<String, String>> customerData = DataUtil.getB2BCustomerData(uid, new String[]{ENCODED_PASSWORD});
        if (customerData.isPresent()) {
            return customerData.get().get(PASSWD);
        } else {
            return null;
        }
    }
}
