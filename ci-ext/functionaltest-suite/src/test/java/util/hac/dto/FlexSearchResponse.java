package util.hac.dto;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents a deserialized JSON response returned when executing a flexsearch.
 */
public class FlexSearchResponse {

    private String query;
    private FlexSearchException exception;
    private List<List<String>> resultList;
    private List<String> headers;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public FlexSearchException getException() {
        return exception;
    }

    public void setException(FlexSearchException exception) {
        this.exception = exception;
    }

    public List<List<String>> getResultList() {
        return resultList;
    }

    public void setResultList(List<List<String>> resultList) {
        this.resultList = resultList;
    }

    public List<String> getHeaders() {
        return headers;
    }

    public void setHeaders(List<String> headers) {
        this.headers = headers;
    }

    public boolean hasError() {
        return exception != null && StringUtils.isNotBlank(exception.getMessage());
    }

    /**
     * Maps the results to the result headers, and returns the results as a list of hash maps for easy consumption.
     *
     * @return
     */
    public List<Map<String, String>> getResults() {
        final List<Map<String, String>> results = new ArrayList<>();

        for(List<String> row : getResultList()) {
            final Map<String, String> mappedRow = new HashMap<>();
            int size = row.size();
            for(int i = 0; i < size; i++) {
                mappedRow.put(getHeaders().get(i), row.get(i));
            }
            results.add(mappedRow);
        }
        return results;
    }




}
