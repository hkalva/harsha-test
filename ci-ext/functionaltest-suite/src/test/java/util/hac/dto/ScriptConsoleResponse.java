package util.hac.dto;

import org.apache.commons.lang3.StringUtils;

/**
 * Represents a deserialized JSON response returned when executing a groovy script.
 */
public class ScriptConsoleResponse
{
    private String stacktraceText;
    private String executionResult;
    private String outputText;

    public String getStacktraceText() {
        return stacktraceText;
    }

    public void setStacktraceText(String stacktraceText) {
        this.stacktraceText = stacktraceText;
    }

    public String getExecutionResult() {
        return executionResult;
    }

    public void setExecutionResult(String executionResult) {
        this.executionResult = executionResult;
    }

    public String getOutputText() {
        return outputText;
    }

    public void setOutputText(String outputText) {
        this.outputText = outputText;
    }

    public boolean hasError() {
        return StringUtils.isNotBlank(stacktraceText);
    }
}
