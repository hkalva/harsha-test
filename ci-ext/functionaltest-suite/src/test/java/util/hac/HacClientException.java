package util.hac;


/**
 * Exception thrown by HacClient.
 */
public class HacClientException extends RuntimeException {

    public HacClientException(final String message) {
        super(message);
    }

    public HacClientException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
