package util.hac;

import com.google.gson.Gson;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.LaxRedirectStrategy;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;

import org.apache.http.Header;
import util.DataUtil;
import util.hac.dto.FlexSearchResponse;
import util.hac.dto.ScriptConsoleResponse;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Http Client used to communicate with the hybris admin console.
 */
public class HacClient {

    /*
     * Header and Header Values.
     */
    private static String ACCEPT = "Accept";
    private static String ACCEPT_VAL = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
    private static String ACCEPT_JSON = "application/json";
    private static String USER_AGENT = "User-Agent";
    private static String ACCEPT_LANGUAGE = "Accept-Language";
    private static String ACCEPT_ENCODING = "Accept-Encoding";
    private static String USER_AGENT_VAL = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0";
    private static String ACCEPT_LANGUAGE_VAL = "en-US,en;q=0.5";
    private static String ACCEPT_ENCODING_VAL = "gzip, deflate, br";
    private static String X_CSRF_TOKEN = "X-CSRF-TOKEN";
    private static String X_REQUESTED_WITH = "X-Requested-With";
    private static String XML_HTTP_REQUEST = "XMLHttpRequest";

    /*
     * Urls
     */
    private static String LOG_IN_HANDLER = "/j_spring_security_check";
    private static String LOG_IN_PAGE_URL = "/login.jsp";
    private static String IMPEX_IMPORT_CONSOLE_URL = "/console/impex/import";
    private static String CONSOLE_ENCRYPT_URL = "/console/flexsearch/encrypt";
    private static String FLEXSEARCH_EXECUTE_URL = "/console/flexsearch/execute";
    private static String FLEXSEARCH_URL = "/console/flexsearch";
    private static String SCRIPT_CONSOLE_URL = "/console/scripting";
    private static String SCRIPT_EXECUTE_URL = "/console/scripting/execute";
    private static String CACHE_PAGE_URL = "/monitoring/cache";
    private static String CLEAR_CACHE_URL = "/monitoring/cache/regionCache/clear";

    /*
     * Parameters/Variables.
     */
    private static String J_USERNAME = "j_username";
    private static String J_PASSWORD = "j_password";
    private static String CSRF = "_csrf";
    private static String TOKEN = "token";

    private static Pattern CSRF_TOKEN_REGEX = Pattern.compile("input type=\"hidden\"\\s+name=\"_csrf\"\\s*value=\"(?<token>[a-z0-9-]*)\"");
    private static Charset UTF8 = Charset.forName("UTF-8");
    private static Gson GSON = new Gson();

    /**
     * Default HacClient Configuration Settings.
     */
    public static String DEFAULT_HAC_USER = "admin";
    public static String DEFAULT_HAC_PASSWORD = "nimda";
    public static String DEFAULT_HAC_BASEURL = "https://localhost:9002/hac";

    private static HacClient instance;
    private String hacUsername;
    private String hacPassword;
    private String hacBaseUrl;
    private CloseableHttpClient httpclient;
    private BasicCookieStore cookieStore;

    private HacClient(final String hacUsername, final String hacPassword, final String hacBaseUrl) throws NoSuchAlgorithmException,
            KeyStoreException, KeyManagementException {
        this.hacUsername = hacUsername;
        this.hacPassword = hacPassword;
        this.hacBaseUrl = hacBaseUrl;

        cookieStore = new BasicCookieStore();
        SSLContextBuilder builder = new SSLContextBuilder();
        builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());
        httpclient = HttpClients.custom()
                .setSSLSocketFactory(sslsf)
                .setRedirectStrategy(new LaxRedirectStrategy())
                .setDefaultCookieStore(cookieStore)
                .setDefaultHeaders(Arrays.asList(
                    new BasicHeader(USER_AGENT, USER_AGENT_VAL),
                    new BasicHeader(ACCEPT_ENCODING, ACCEPT_ENCODING_VAL),
                    new BasicHeader(ACCEPT, ACCEPT_VAL),
                    new BasicHeader(ACCEPT_LANGUAGE, ACCEPT_LANGUAGE_VAL)
                ))
                .build();
    }

    /**
     *
     * @param hacUsername
     * @param hacPassword
     * @param hacBaseUrl
     * @return
     * @throws NoSuchAlgorithmException
     * @throws KeyStoreException
     * @throws KeyManagementException
     */
    public static HacClient create(final String hacUsername, final String hacPassword, final String hacBaseUrl) throws NoSuchAlgorithmException,
            KeyStoreException, KeyManagementException {
        instance = new HacClient(hacUsername, hacPassword, hacBaseUrl);
        return instance;
    }

    /**
     * Get Singleton instance of HacClient.
     *
     * @return HacClient instance
     * @throws HacClientException
     */
    public static HacClient getInstance() throws HacClientException {

        try {
            final EnvironmentVariables envVars = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();
            if (instance == null) {
                instance =  create(
                        envVars.getProperty("hac.username", DEFAULT_HAC_USER),
                        envVars.getProperty("hac.password", DEFAULT_HAC_PASSWORD),
                        envVars.getProperty("hac.baseurl", DEFAULT_HAC_BASEURL)
                );
            }
        } catch (Exception e) {
            throw new HacClientException(String.format("Error initializing HacClient. Error was:  %s", e.getMessage()), e);
        }
        return instance;
    }

    /**
     * Authenticates the HAC user.
     *
     * @throws HacClientException
     */
    public void hacLogin() {
        HacResponse loginForm = doGet(LOG_IN_PAGE_URL);
        final Map<String, String> params = new HashMap<>();
        params.put(J_USERNAME, hacUsername);
        params.put(J_PASSWORD, hacPassword);
        params.put(CSRF, loginForm.getCsrfToken());
        HacResponse loginRes = doPost(LOG_IN_HANDLER, params);

        if(loginRes.getContent().contains("loginErrors")) {
            throw new HacClientException("Unable to login to hybris admin console");
        }

    }

    /**
     * Clear hybris cache.
     *
     * @return
     */
    public HacResponse clearCache() {
        final HacResponse cachePage = doSecureGet(CACHE_PAGE_URL);
        final Map<String, String> headers = new HashMap<>();
        headers.put(ACCEPT, ACCEPT_JSON);
        headers.put(X_CSRF_TOKEN, cachePage.getCsrfToken());
        headers.put(X_REQUESTED_WITH, XML_HTTP_REQUEST);
        return doPost(CLEAR_CACHE_URL, new HashMap<>(), headers);
    }

    /**
     * Imports a raw impex script string.
     *
     * @param script
     * @throws IOException
     */
    public HacResponse postImpexScript(final String script) {
        final HacResponse impexImportPage = doSecureGet(IMPEX_IMPORT_CONSOLE_URL);
        final Map<String, String> params = new HashMap<>();
        params.put("scriptContent", script);
        params.put("validationEnum", "IMPORT_RELAXED");
        params.put("maxThreads", "8");
        params.put("encoding", "UTF-8");
        params.put("_legacyMode", "on");
        params.put("enableCodeExecution", "true");
        params.put("_enableCodeExecution", "on");
        params.put("_distributedMode", "on");
        params.put("_sldEnabled", "on");
        params.put(CSRF, impexImportPage.getCsrfToken());
        final HacResponse postResponse = doPost(IMPEX_IMPORT_CONSOLE_URL, params);
        if(postResponse.getContent().contains("Import has encountered problems.")) {
            final String errors = StringUtils.trim(StringUtils.substringBetween(postResponse.getContent(),"<pre>","</pre>"));
            throw new HacClientException(String.format("Impex Import Errors!\n%s", errors));
        }
        return postResponse;
    }

    /**
     * Post a Groovy Script to the groovy console. The Response is the Raw HacResponse.
     *
     * @param script
     * @param commit
     * @return
     */
    public HacResponse postGroovyScript(final String script, final boolean commit) {
        final HacResponse scriptConsole = doSecureGet(SCRIPT_CONSOLE_URL);
        final Map<String, String> headers = new HashMap<>();
        headers.put(ACCEPT, ACCEPT_JSON);
        headers.put(X_CSRF_TOKEN, scriptConsole.getCsrfToken());
        headers.put(X_REQUESTED_WITH, XML_HTTP_REQUEST);

        final Map<String, String> params = new HashMap<>();
        params.put("script", script);
        params.put("scriptType", "groovy");
        params.put("commit", Boolean.toString(commit));

        return doPost(SCRIPT_EXECUTE_URL, params, headers);
    }

    /**
     * Post and execute a groovy script. If the script returns a result, the text of the result can be retrieved via {@link ScriptConsoleResponse#getOutputText()}.
     *
     * @param script
     * @param commit
     * @return
     */
    public ScriptConsoleResponse executeGroovyScript(final String script, final boolean commit) {

        final HacResponse hacResponse = postGroovyScript(script, commit);
        final ScriptConsoleResponse response = GSON.fromJson(hacResponse.getContent(), ScriptConsoleResponse.class);

        if(response.hasError()) {
            throw new HacClientException(String.format("Script execution error!\nError message was: '%s' \n Script content was: \n%s",response.getStacktraceText(), script));
        }
        return response;
    }

    /**
     * Post and execute a Flexible Search Query. The Response is the Raw HacResponse.
     *
     * @param flexQuery
     * @return
     */
    public HacResponse postFlexQuery(final String flexQuery) {
        return postFlexOrSqlQuery(flexQuery, StringUtils.EMPTY, false);
    }

    /**
     * Execute a Flexible Search Query with bound parameter replacement.
     *
     * Positional query parameters are not supported. Used named parameters only.
     *
     * <pre>
     *     Map<String, Object> queryParams = new HashMap<>();
     *     queryParams.put("uid", "customer-uid@hybris.com");
     *     FlexSearchResponse res = executeFlexQuery("select * from {Customer} where {uid} = ?uid", queryParams);
     * </pre>
     *
     * @param flexQuery
     * @param queryParams
     * @return
     */
    public FlexSearchResponse executeFlexQuery(final String flexQuery, final Map<String, Object> queryParams) {
        return executeFlexQuery(DataUtil.bindQueryParams(flexQuery, queryParams));
    }

    /**
     * Execute a Flexible Search Query. The FlexibleSearchResponse DTO is returned.
     *
     * @param flexQuery
     * @return
     */
    public FlexSearchResponse executeFlexQuery(final String flexQuery) {
        final HacResponse hacResponse = postFlexQuery(flexQuery);
        final FlexSearchResponse response = GSON.fromJson(hacResponse.getContent(), FlexSearchResponse.class);

        if(response.hasError()) {
            throw new HacClientException(String.format("Flexible Search Error: %s\n\t Query was: %s",
                    response.getException().getMessage(), flexQuery));
        }

        return response;
    }

    /**
     * Execute a Native SQL query with named parameters.
     *
     * @param sqlQuery
     * @param queryParams
     * @param commit
     * @return
     */
    public FlexSearchResponse executeSqlQuery(final String sqlQuery, final Map<String, Object> queryParams, boolean commit) {
        return executeSqlQuery(DataUtil.bindQueryParams(sqlQuery, queryParams), commit);
    }

    /**
     * Execute a SQL Query. The FlexibleSearchResponse DTO is returned.
     *
     * @param sqlQuery
     * @param commit
     * @return
     */
    public FlexSearchResponse executeSqlQuery(final String sqlQuery, boolean commit) {
        final HacResponse hacResponse = postSqlQuery(sqlQuery, commit);
        final FlexSearchResponse response = GSON.fromJson(hacResponse.getContent(), FlexSearchResponse.class);

        if(response.hasError()) {
            throw new HacClientException(String.format("Flexible Search Error: %s\n\t Query was: %s",
                    response.getException().getMessage(), sqlQuery));
        }

        return response;
    }

    /**
     * Post a native SQL Query. The Response is the Raw HacResponse.
     *
     * @param sqlQuery
     * @param commit
     * @return
     */
    public HacResponse postSqlQuery(final String sqlQuery, boolean commit) {
        return postFlexOrSqlQuery(StringUtils.EMPTY, sqlQuery, commit);
    }

    private HacResponse postFlexOrSqlQuery(final String flexQuery, final String sqlQuery, final boolean commit) {

        final HacResponse flexsearchConsole = doSecureGet(FLEXSEARCH_URL);
        final Map<String, String> headers = new HashMap<>();
        headers.put(ACCEPT, ACCEPT_JSON);
        headers.put(X_CSRF_TOKEN, flexsearchConsole.getCsrfToken());
        headers.put(X_REQUESTED_WITH, XML_HTTP_REQUEST);

        final Map<String, String> params = new HashMap<>();
        params.put("flexibleSearchQuery", flexQuery);
        params.put("sqlQuery", sqlQuery);
        params.put("maxCount", "200");
        params.put("locale", "en");
        params.put("user", hacUsername);
        params.put("commit", Boolean.toString(commit));

        return doPost(FLEXSEARCH_EXECUTE_URL, params, headers);

    }

    //TODO: Are the encrypt calls even needed? We might be removing this.
    private HacResponse postEncrypt(final Map<String, String> encryptParams, final String csrfToken) {
        encryptParams.put("additionalDescription", "");
        final Map<String, String> params = new HashMap<>();
        final Map<String, String> headers = new HashMap<>();
        params.put("cookie", GSON.toJson(encryptParams));
        headers.put(X_CSRF_TOKEN, csrfToken);
        return doPost(CONSOLE_ENCRYPT_URL, params, headers);
    }

    /**
     * Loads a secure page. If a session has not been established, this method will automatically log in.
     *
     * @param url
     *         - the path of the URL relative to the base URL.
     * @return
     * @throws IOException
     */
    public HacResponse doSecureGet(final String url) {

        HacResponse response = doGet(url);
        if(response.getContent().contains(J_PASSWORD)) {
            hacLogin();
            response = doGet(url);
        }
        return response;
    }

    /**
     * Send a GET Request to the specified URL. Uses Default Headers.
     *
     * @param url
     * @return
     */
    public HacResponse doGet(final String url) {
        return doGet(url, new HashMap<>());
    }

    /**
     * Send a GET Request to the specified URL. The default headers may be overridden using the headers param.
     *
     * @param url
     * @param headers
     * @return
     */
    public HacResponse doGet(final String url, final Map<String, String> headers) {
        return doRequest(new HttpGet(hacBaseUrl + url), headers);
    }

    /**
     * Post a request to the specified URL using the specified parameters. Default headers will be used.
     *
     * @param url
     * @param params
     * @return
     */
    public HacResponse doPost(final String url, final Map<String, String> params) {
        return doPost(url, params, new HashMap<>());
    }

    /**
     * Post a request to the specified URL using the specified parameters. The default headers can be overridden using the headers param.
     *
     * @param url
     * @param params
     * @param headers
     * @return
     */
    public HacResponse doPost(final String url, final Map<String, String> params, final Map<String, String> headers) {
        final HttpPost httpPost = new HttpPost(hacBaseUrl + url);
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params.entrySet().stream().map(e ->
                    new BasicNameValuePair(e.getKey(), e.getValue())).collect(Collectors.toList())));
        } catch(UnsupportedEncodingException e) {
            throw new HacClientException(e.getMessage(), e);
        }
        return doRequest(httpPost, headers);
    }

    private HacResponse doRequest(final HttpUriRequest request, final Map<String, String> headers) {
        CloseableHttpResponse response = null;

        try {

            headers.forEach((name, value) -> {
                request.setHeader(name, value);
            });

            response = httpclient.execute(request);
            return new HacResponse(response);
        } catch (IOException e) {
            throw new HacClientException(String.format("Error sending request. request=%s errorMessage=%s",
                    request.toString(), e.getMessage()), e);
        } finally {
            if(response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public class HacResponse {

        private String content;
        private int status;
        private HttpResponse response;

        public HacResponse(final HttpResponse response) throws IOException {
            this.response = response;
            this.status = response.getStatusLine().getStatusCode();
            this.content = IOUtils.toString(response.getEntity().getContent(), UTF8);
        }

        public int getStatus() {
            return status;
        }

        public String getContent() {
            return content;
        }

        public Optional<Header> getHeader(final String name) {
            return Optional.ofNullable(response.getFirstHeader(name));
        }

        public String getHeaderValue(final String name) {
            final Optional<Header> header = getHeader(name);
            return header.isPresent() ? header.get().getValue() : StringUtils.EMPTY;
        }

        public String getCsrfToken() {
            final Matcher matcher = CSRF_TOKEN_REGEX.matcher(content);
            matcher.find();
            return matcher.group(TOKEN);
        }

    }

    /**
     * Stand-alone script to test the HacClient. Enable assert statements by passing the -ea option to the VM.
     *
     * @param args
     */
    public static void main(String[] args) {

        try {
            final HacClient client = HacClient.create(DEFAULT_HAC_USER, DEFAULT_HAC_PASSWORD, DEFAULT_HAC_BASEURL);

            //Test postImpexScript
            try {
                //NOTE: this test will fail when william has a saved cart and a current cart.
                client.postImpexScript(
                        "REMOVE Cart;user(Customer.uid)[unique=true]\n" +
                                ";william.hunter@rustic-hw.com");

            } catch(HacClientException e) {
                assert ("Impex Import Errors!\n" +
                        "REMOVE Cart;user(Customer.uid)[unique=true]\n" +
                        ",,,,More than one item of type Cart found for unique qualifiers {user1=william.hunter@rustic-hw.com[8796094201860]};william.hunter@rustic-hw.com").equals(e.getMessage());
            }

            //Test valid flex query
            final FlexSearchResponse flexQueryResponse = client.executeFlexQuery("select * from {Customer} where {uid} = 'william.hunter@rustic-hw.com'");
            final List<Map<String, String>> flexQueryResults = flexQueryResponse.getResults();
            assert "william.hunter@rustic-hw.com".equals(flexQueryResults.get(0).get("p_email"));
            assert "william.hunter@rustic-hw.com".equals(flexQueryResults.get(0).get("p_uid"));

            final Map<String, Object> params = new HashMap<>();
            params.put("uid","william.hunter@rustic-hw.com");
            final FlexSearchResponse flexQueryResponse2 = client.executeFlexQuery("select * from {Customer} where {uid} = ?uid", params);
            final List<Map<String, String>> flexQueryResults2 = flexQueryResponse2.getResults();
            assert "william.hunter@rustic-hw.com".equals(flexQueryResults2.get(0).get("p_email"));
            assert "william.hunter@rustic-hw.com".equals(flexQueryResults2.get(0).get("p_uid"));

            //Test valid sql query
            final FlexSearchResponse sqlQueryResponse = client.executeSqlQuery("select p_uid, p_email from users where p_uid = 'william.hunter@rustic-hw.com'", false);
            final List<Map<String, String>> sqlQueryResults = sqlQueryResponse.getResults();
            assert "william.hunter@rustic-hw.com".equals(sqlQueryResults.get(0).get("p_email"));
            assert "william.hunter@rustic-hw.com".equals(sqlQueryResults.get(0).get("p_uid"));

            final FlexSearchResponse sqlQueryResponse2 = client.executeSqlQuery("select p_uid, p_email from users where p_uid = ?uid", params, false);
            final List<Map<String, String>> sqlQueryResults2 = sqlQueryResponse2.getResults();
            assert "william.hunter@rustic-hw.com".equals(sqlQueryResults2.get(0).get("p_email"));
            assert "william.hunter@rustic-hw.com".equals(sqlQueryResults2.get(0).get("p_uid"));

            //Test invalid flex query
            final String invalidFlexQuery = "select * from {Customers} where {uid} = 'william.hunter@rustic-hw.com'";
            try {
                client.executeFlexQuery(invalidFlexQuery);
                assert false; //An exception should have been thrown.
            } catch(HacClientException e) {
                assert String.format("Flexible Search Error: %s\n\t Query was: %s", "type code 'Customers' invalid", invalidFlexQuery).equals(e.getMessage());
            }

            //Test invalid sql query
            final String invalidSqlQuery = "select * from idontexist";
            try {
                client.executeSqlQuery(invalidSqlQuery, false);
                assert false; //An exception should have been thrown.
            } catch(HacClientException e) {
                assert String.format("Flexible Search Error: %s\n\t Query was: %s", "Table 'hybrisrlp.idontexist' doesn't exist", invalidSqlQuery).equals(e.getMessage());
            }

            //Test valid groovy script
            final ScriptConsoleResponse validScriptResponse = client.executeGroovyScript("return 'Groovy Rocks!'", false);
            assert "Groovy Rocks!".equals(validScriptResponse.getExecutionResult());

            final HacResponse clearCacheResponse = client.clearCache();
            assert clearCacheResponse.getContent().contains("totalRegionCount");

            //Test invalid groovy script
            final String invalidGroovyScript = "springs.stuff";
            try {
                client.executeGroovyScript(invalidGroovyScript, false);
            } catch (HacClientException e) {
                assert String.format("Script execution error!\nError message was: '%s' \n Script content was: \n%s","Script execution has failed [reason: groovy.lang.MissingPropertyException: No such property: springs for class: Script9]", invalidGroovyScript).equals(e.getMessage());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
