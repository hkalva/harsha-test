package util;

import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;

/**
 * Provides convenience functions to lookup configuration property settings.
 */
public class ConfigUtil {

    public static boolean getBoolean(final String property, boolean defaultVal) {
        final EnvironmentVariables envVars = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();
        return envVars.getPropertyAsBoolean(property, defaultVal).booleanValue();
    }

    /**
     * Certain precondtions only run conditionally. Conditional preconditions may be forced to run by setting
     * the preconditions.force property to true.
     *
     * @return
     */
    public static boolean isForcePreconditions() {
        return getBoolean(Constants.Properties.PRECONDITIONS_FORCE, false);
    }

}
