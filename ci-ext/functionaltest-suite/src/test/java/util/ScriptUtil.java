package util;

import org.apache.commons.io.IOUtils;
import org.apache.commons.text.StringSubstitutor;
import util.hac.HacClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Script utility functions to load and execute local impex and groovy scripts.
 */
public class ScriptUtil {

    /**
     * Execute a Groovy Script. Use this for scripts containing no template parameters.
     *
     * @param template
     * @param commit
     */
    public static void executeGroovyScript(final String template, final boolean commit) {
        executeGroovyScript(template, new HashMap<>(), commit);
    }

    /**
     * Execute a Groovy Script. Use this for scripts containing template parameters.
     *
     * @param template
     * @param tplParams
     * @param commit
     */
    public static void executeGroovyScript(final String template, final Map<String, String> tplParams, final boolean commit) {

        final StringBuilder script = new StringBuilder();
        script.append("import org.apache.log4j.Logger")
                .append(Constants.NEWLINE)
                .append("Logger logger = Logger.getLogger('Functional Test Suite Script ").append(template).append("')")
                .append(Constants.NEWLINE)
                .append("logger.info('").append(template).append(" starting...").append("')")
                .append(Constants.NEWLINE)
                .append(getScript("setup/groovy/" + template + ".groovy.tpl", tplParams))
                .append(Constants.NEWLINE)
                .append("logger.info('").append(template).append(" finished.").append("')")
                .append(Constants.NEWLINE);

        HacClient.getInstance().executeGroovyScript(script.toString(), commit);

    }

    /**
     * Loads a template file and replaces template variables with the specified params.
     *
     * @param tpl
     *         - the path to the template resource (e.g. setup/impex/clearcustomercarts.impex).
     * @param tplParams
     *         - a hash map of template variable key-value pairs.
     * @return String
     *         - the template content with the interpolated variables.
     */
    public static String getScript(final String tpl, final Map<String, String> tplParams) {

        try {
            final String template = IOUtils.toString(Preconditions.class.getClassLoader().getResource(tpl),"UTF-8");
            return StringSubstitutor.replace(template, tplParams);
        } catch(IOException e) {
            //TODO: Is there a better exception to throw here?
            throw new RuntimeException(e);
        } catch(NullPointerException e) {
            throw new RuntimeException(String.format("Could not load script template %s", tpl));
        }
    }
}
