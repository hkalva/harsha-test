package util;

import org.apache.commons.lang3.StringUtils;
import util.hac.HacClient;
import util.hac.dto.FlexSearchResponse;

import java.util.*;
import java.util.stream.Collectors;

import static util.hac.HacClient.DEFAULT_HAC_BASEURL;
import static util.hac.HacClient.DEFAULT_HAC_PASSWORD;
import static util.hac.HacClient.DEFAULT_HAC_USER;

/**
 * Data Utility class. Simplifies direct updates to SQL tables.
 *
 * TODO: Expand API as needed.
 */
public class DataUtil {

    /**
     * Update fields in the users table for the specified uid.
     *
     * @param uid
     * @param values
     */
    public static void updateUser(final String uid, final Map<String, Object> values) {
        updateTable("users", values, "p_uid = '" + uid + "'");
        HacClient.getInstance().clearCache();
    }

    /**
     * Queries hybris to check for the existence of a B2BUnit having the specified uid.
     *
     * @param uid
     * @return boolean
     */
    public static boolean hasB2BUnit(final String uid) {
        final Map<String, Object> params = new HashMap<>();
        params.put("uid", uid);
        return HacClient.getInstance().executeFlexQuery("select {pk} from {B2BUnit} where {uid} = ?uid",
                params).getResults().size() > 0;
    }

    /**
     * Get the specified attributes of the Customer, if the customer exists.
     *
     * @param uid
     * @param attributes
     * @return
     */
    public static Optional<Map<String, String>> getB2BCustomerData(final String uid, final String... attributes) {
        final Map<String, Object> params = new HashMap<>();
        params.put("uid", uid);
        final String query = String.format("select %s from {B2BCustomer} where {uid} = ?uid",
                Arrays.stream(attributes).map(a -> "{" + a + "}").collect(Collectors.joining(",")));

        final List<Map<String, String>> results = HacClient.getInstance().executeFlexQuery(query, params).getResults();
        return results.size() > 0 ? Optional.of(results.get(0)) : Optional.empty();
    }

    /**
     * Returns a list of permission codes associated with the specified B2BCustomer uid.
     *
     * @param uid
     * @return
     */
    public static List<String> getB2BCustomerPermissions(final String uid) {
        final Map<String, Object> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_B2BCUSTOMER_PERMISSIONS, tplParams);
        return res.getResultList().stream().map(row -> row.get(0)).collect(Collectors.toList());
    }

    /**
     * Returns a list of approver uids associated with the specified B2BCustomer uid.
     *
     * @param uid
     * @return
     */
    public static List<String> getB2BCustomerApprovers(final String uid) {
        final Map<String, Object> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_B2BCUSTOMER_APPROVERS, tplParams);
        return res.getResultList().stream().map(row -> row.get(0)).collect(Collectors.toList());
    }

    /**
     * Get the B2BBudget codes associated with the specified B2BCostCenter.
     *
     * @param code
     * @return
     */
    public static List<String> getB2BCostCenterBudgets(final String code) {
        final Map<String, Object> params = new HashMap<>();
        params.put("code", code);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_B2BCOSTCENTER_BUDGETS, params);
        return res.getResultList().stream().map(row -> row.get(0)).collect(Collectors.toList());
    }

    /**
     * Returns a list of user group uids for the specified user.
     *
     * @param uid
     * @return
     */
    public static List<String> getUsersGroups(final String uid) {
        final Map<String, Object> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_USERS_GROUPS, tplParams);
        return res.getResultList().stream().map(row -> row.get(0)).collect(Collectors.toList());
    }

    /**
     * Returns a list of B2BUserGroup member uids for the specified group.
     *
     * @param uid
     * @return
     */
    public static List<String> getB2BUserGroupMembers(final String uid) {
        final Map<String, Object> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_B2BUSERGROUP_USERS, tplParams);
        return res.getResultList().stream().map(row -> row.get(0)).collect(Collectors.toList());
    }

    /**
     * Returns a list of B2BUserGroup permission codes for the specified group.
     *
     * @param uid
     * @return
     */
    public static List<String> getB2BUserGroupPermissions(final String uid) {
        final Map<String, Object> tplParams = new HashMap<>();
        tplParams.put("uid", uid);
        final FlexSearchResponse res = HacClient.getInstance().executeFlexQuery(Constants.FlexQueries.GET_B2BUSERGROUP_PERMISSIONS, tplParams);
        return res.getResultList().stream().map(row -> row.get(0)).collect(Collectors.toList());
    }

    /**
     * Queries hybris to check for existence of a B2BCustomer having the specified uid.
     *
     * @param uid
     * @return
     */
    public static boolean hasB2BCustomer(final String uid) {
        final Map<String, Object> params = new HashMap<>();
        params.put("uid", uid);
        return HacClient.getInstance().executeFlexQuery("select {pk} from {B2BCustomer} where {uid} = ?uid",
                params).getResults().size() > 0;
    }

    /**
     * Checks for the existence of a B2BPermission having the specified code.
     *
     * @param code
     * @return
     */
    public static boolean hasB2BPermission(final String code) {
        final Map<String, Object> params = new HashMap<>();
        params.put("code", code);
        return HacClient.getInstance().executeFlexQuery("select {pk} from {B2BPermission} where {code} = ?code",
                params).getResults().size() > 0;
    }

    /**
     * Checks for the existence of a B2BCostCenter having the specified code.
     *
     * @param code
     * @return
     */
    public static boolean hasB2BCostCenter(final String code) {
        final Map<String, Object> params = new HashMap<>();
        params.put("code", code);
        return HacClient.getInstance().executeFlexQuery("select {pk} from {B2BCostCenter} where {code} = ?code",
                params).getResults().size() > 0;
    }

    /**
     * Checks for the existence of a B2BBudget having the specified code.
     *
     * @param code
     * @return
     */
    public static boolean hasB2BBudget(final String code) {
        final Map<String, Object> params = new HashMap<>();
        params.put("code", code);
        return HacClient.getInstance().executeFlexQuery("select {pk} from {B2BBudget} where {code} = ?code",
                params).getResults().size() > 0;
    }

    /**
     * Checks for the existence of a B2BUserGroup having the specified uid.
     *
     * @param uid
     * @return
     */
    public static boolean hasB2BUserGroup(final String uid) {
        final Map<String, Object> params = new HashMap<>();
        params.put("uid", uid);
        return HacClient.getInstance().executeFlexQuery("select {pk} from {B2BUserGroup} where {uid} = ?uid",
                params).getResults().size() > 0;
    }

    /**
     * Update a hybris table. Be careful! This will make changes to the database, and bypass hybris' service-layer
     * validation.
     *
     * Make sure you clear the hybris cache after making changes to the database using SQL queries.
     *
     * @param table
     * @param values
     * @param whereFilter
     */
    protected static void updateTable(final String table, final Map<String, Object> values, final String whereFilter) {
        final StringBuilder sql = new StringBuilder();
        sql.append("update ").append(table).append(" set ");

        final Iterator<Map.Entry<String, Object>> it = values.entrySet().iterator();

        while(it.hasNext()) {
            final Map.Entry<String, Object> entry = it.next();
            sql.append(entry.getKey()).append("=").append(resolveFieldValue(entry.getValue()));

            if(it.hasNext()) {
                sql.append(", ");
            }
        }

        if(StringUtils.isNotBlank(whereFilter)) {
            sql.append(" where ").append(whereFilter);
        }

        HacClient.getInstance().executeSqlQuery(sql.toString(), true);
    }

    /**
     * Converts Java Object to SQL. String values are escaped and quoted. Integers, Booleans, etc, are converted to strings.
     *
     * @param value
     * @return
     */
    public static String resolveFieldValue(final Object value) {
        return value instanceof String ? "'" + escape((String)value) + "'" : value.toString();
    }

    /**
     * Escape database field value. Foo's become Foo\'s.
     *
     * @param value
     * @return
     */
    public static String escape(final String value) {
        return StringUtils.replace(value, "'", "\'");
    }

    /**
     * Binds the provided parameters to the provided query or query fragment.
     *
     * <pre>
     *     Map<String, Object params = new HashMap<>();
     *     params.put("baz", "bat"(;
     *     String replacedQuery = bindQueryParams("select * from foo where bar = ?baz", params);
     *     assert "select * from foo where bar = 'bat'".equals(replacedQuery);
     * </pre>
     *
     *
     * @param query
     * @param params
     * @return
     */
    public static String bindQueryParams(final String query, final Map<String, Object> params) {
        String replacedQuery = query;
        for (Map.Entry<String, Object> entry: params.entrySet()) {
            replacedQuery = replacedQuery.replaceAll("\\?" + entry.getKey(), resolveFieldValue(entry.getValue()));
        }
        return replacedQuery;
    }

    /**
     * Stand-alone script to test the DataUtils. Enable assert statements by passing the -ea option to the VM.
     *
     * @param args
     */
    public static void main(String[] args) {

        try {
            final HacClient client = HacClient.create(DEFAULT_HAC_USER, DEFAULT_HAC_PASSWORD, DEFAULT_HAC_BASEURL);

            final Map<String, Object> data = new HashMap<>();
            data.put("p_logindisabled", true);
            DataUtil.updateUser("william.hunter@rustic-hw.com", data);

            FlexSearchResponse sqlQueryResponse = client.executeSqlQuery("select p_logindisabled from users where p_uid = 'william.hunter@rustic-hw.com'", false);
            List<Map<String, String>> sqlQueryResults = sqlQueryResponse.getResults();
            assert "true".equals(sqlQueryResults.get(0).get("p_logindisabled"));

            data.put("p_logindisabled", false);
            DataUtil.updateUser("william.hunter@rustic-hw.com", data);
            sqlQueryResponse = client.executeSqlQuery("select p_logindisabled from users where p_uid = 'william.hunter@rustic-hw.com'", false);
            sqlQueryResults = sqlQueryResponse.getResults();
            assert "false".equals(sqlQueryResults.get(0).get("p_logindisabled"));


            assert !DataUtil.hasB2BUnit("idontexist");
            assert DataUtil.hasB2BUnit("Rustic");

            final List<String> expectedUserGroups = Arrays.asList("Custom Retail","b2bcustomergroup","limitedPermissions","standardPermissions");
            final List<String> actualUserGroups = getUsersGroups("william.hunter@rustic-hw.com");
            assert actualUserGroups.size() == 4;
            assert actualUserGroups.containsAll(expectedUserGroups);

            final Optional<Map<String, String>> b2bCustomerData = getB2BCustomerData("william.hunter@rustic-hw.com", "email", "uid");
            assert b2bCustomerData.isPresent();
            assert "william.hunter@rustic-hw.com".equals(b2bCustomerData.get().get("p_email"));
            assert "william.hunter@rustic-hw.com".equals(b2bCustomerData.get().get("p_uid"));

        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
