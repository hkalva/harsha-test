package util;

import org.openqa.selenium.By;

public class Elements {

    public static String BUTTON = "button";
    public static String FORM = "form";
    public static String VALUE = "value";
    public static String ORDER_NUMBER = "[0-9]{8}";

    static public class Cart {
        static public final By QUANTITY_INPUT = By.cssSelector(".js-update-entry-quantity-input"); //form-control js-update-entry-quantity-input
        static public final By CHECKOUT_BUTTON = By.cssSelector(".js-continue-checkout-button");
        static public final By QUANTITY_DIV = By.cssSelector("div[class='qty']");
        static public final By CART_ELEMENT_DETAILS_BUTTON = By.cssSelector(".js-cartItemDetailBtn");
        static public final By CART_ELEMENT_REMOVE = By.cssSelector("li[data-entry-action='REMOVE']");
        static public final By CART_ITEM_LIST = By.cssSelector(".item__list--item");
        static public final By ITEM_CODE = By.cssSelector(".item__code");
        static public final By NEW_CART = By.cssSelector(".js-save-cart-link");
        static public final By SAVED_CARTS = By.cssSelector("a[class='save__cart--link cart__head--link']");
        static public final By ADD_TO_CART_LAYER = By.cssSelector(".addToCartLayer");
        static public final By CREATE_QUOTE = By.cssSelector(".btn-create-quote");
    }

    static public class Address {
        static public By ADDRESS_FORM = By.id("addressForm");

        static public final String ADDRESS_COUNTRY = "countryIso";
        static public final String TITLE = "titleCode";
        static public final String FIRST_NAME = "firstName";
        static public final String LAST_NAME = "lastName";
        static public final String LINE1 = "line1";
        static public final String LINE2 = "line2";
        static public final String CITY = "townCity";
        static public final String REGION = "regionIso";
        static public final String POSTAL_CODE = "postcode";
        static public final String PHONE = "phone";
        static public final String SAVE_IN_ADDRESS_BOOK = "saveInAddressBook";
    }

    static public class BillingAddress {
        static public By ADDRESS_FORM = By.id("silentOrderPostForm");

        static public final String ADDRESS_COUNTRY = "billTo_country";
        static public final String TITLE = "billTo_titleCode";
        static public final String FIRST_NAME = "billTo_firstName";
        static public final String LAST_NAME = "billTo_lastName";
        static public final String LINE1 = "billTo_street1";
        static public final String LINE2 = "billTo_street2";
        static public final String CITY = "billTo_city";
        static public final String REGION = "billTo_state";
        static public final String POSTAL_CODE = "billTo_postalCode";
        static public final String PHONE = "billTo_phoneNumber";
    }

    static public class Wishlist {
        static public By ADD_WISHLIST_FORM = By.id("addToWishlistForm");
        static public By CREATE_WISHIST_FORM = By.id("wishlistForm");
        static public final String NAME = "name";
        static public final String DESCRIPTION = "description";
        static public final String CODE ="wishlistCode";
        static public final String PRODUCT = ".wishlist_product";
        static public final String DATA_PRODUCT_ID = "data-product-id";
        static public final String DATA_PRODUCT_QTY = "data-product-qty";
        static public final String DELETE = "DELETE";
        static public final String REMOVE = "REMOVE";
        static public final String ADD_NEW = "Add New";
        static public final By ACCOUNT_SECTION_HEADER = By.cssSelector(".account-section-header");
        static public final By CONFIRM = By.cssSelector(".confirm");
        static public final By REMOVE_PRODUCT_FORM = By.cssSelector(".wishlist_product_remove");
        static public final By PRODUCT_INFO = By.cssSelector(".wishlist_product_info");
        static public final By ADD_TO_CART = By.cssSelector(".js-wishlist-addToCart");
        static public final By ADD_ALL_TO_CART = By.cssSelector(".js-wishlist-addToCart-all");
        static public final By RESPONSIVE_TABLE_ITEM =  By.cssSelector(".responsive-table-item");
        static public final By ACCOUNT_ACTIONS = By.cssSelector(".accountActions-link");
        static public final By DELETE_CONFIRMATION_MODAL = By.id("js-action-confirmation-modal-content-delete");
    }

    static public class Account {

        static public final By ACCOUNT = By.cssSelector(".js-myAccount-toggle");

        static public final By PROFILE_FORM = By.id("updateProfileForm");

        static public class EditAddress {
            static public final By SAVE = By.cssSelector(".change_address_button");
        }

        static public class Address {
            static public final By ADD_ADDRESS = By.cssSelector("div[class='account-section-header-add pull-right']");
        }

        static public class AddressBook {
            static public final By ADDRESS_LIST = By.cssSelector(".account-addressbook");
            static public final By ACTION_LINKS = By.cssSelector(".action-links");
            static public final By ADD_ADDRESS = By.cssSelector(".account-section-header-add");
        }

        static public class Profile {
            static public final String FIRST_NAME = "firstName";
            static public final String LAST_NAME = "lastName";

            static public final By UPDATE = By.cssSelector(".btn-primary");

        }

        static public class PaymentDetails {
            static public final By REMOVE_PAYMENT_DETAILS = By.cssSelector(".removePaymentDetailsButton");
            static public final By GLOBAL_ALERTS = By.cssSelector(".global-alerts");
            static public final By CARD_SELECT = By.cssSelector(".card-select");
            static public final By ACCOUNT_LIST = By.cssSelector(".account-list");
        }

        static public class UpdatePassword {
            static public final By FORM = By.id("updatePasswordForm");

            static public final String CURRENT_PASSWORD = "currentPassword";
            static public final String NEW_PASWORD = "newPassword";
            static public final String CHECK_NEW_PASSWORD = "checkNewPassword";

            static public final By UPDATE = By.cssSelector(".btn-primary");
            static public final By PASSWORD_ERRORS = By.id("currentPassword.errors");
            static public final By NEW_PASSWORD_ERRORS = By.id("newPassword.errors");
            static public final By CHECK_NEW_PASSWORD_ERRORS = By.id("checkNewPassword.errors");
        }

    }

    static public class UpdateEmail {
        static public final By ACCOUNT_SECTION_HEADER = By.cssSelector(".account-section-header");

        static public final By FORM = By.id("updateEmailForm");
        static public final By EMAIL_ERROR = By.id("email.errors");

        static public final String PROFILE_EMAIL = "email";
        static public final String PROFILE_CHECK_EMAIL = "chkEmail";
        static public final String PROFILE_PASSWORD = "password";
    }

    static public class Checkout {
        static public class PaymentType {
            static public final By NEXT = By.id("choosePaymentType_continue_button");
            static public final By PAYMENT_TYPE_CARD = By.id("PaymentTypeSelection_CARD");
            static public final By PAYMENT_TYPE_ACCOUNT = By.id("PaymentTypeSelection_ACCOUNT");
            static public final By PURCHASE_ORDER_NO = By.id("PurchaseOrderNumber");
            static public final By COST_CENTER = By.id("costCenterSelect");
        }

        static public class ShippingAddress {
            static public final By SAVE_SHIPPING_ADDRESS = By.id("saveAddressInMyAddressBook");
            static public final By NEXT = By.id("addressSubmit");
        }

        static public class DeliveryMethod {
            static public final By DELIVERY_METHOD_FORM = By.id("selectDeliveryMethodForm");

            static public final String DELIVERY_METHOD = "delivery_method";

            static public final By NEXT = By.id("deliveryMethodSubmit");
        }

        static public class PaymentInfo {
            static public final By FORM = By.id("silentOrderPostForm");
            static public final String CARD_TYPE = "card_cardType";
            static public final String NAME_ON_CARD = "card_nameOnCard";
            static public final String CARD_NUMBER = "card_accountNumber";
            static public final String EXPIRY_MONTH = "card_expirationMonth";
            static public final String EXPIRY_YEAR = "card_expirationYear";
            static public final String CVV_NUMBER = "card_cvNumber";
            static public final String SAVE_PAYMENT_INFO = "savePaymentInfo";
            static public final String USE_DELIVERY_ADDRESS = "useDeliveryAddress";

            static public By NEXT = By.cssSelector(".submit_silentOrderPostForm");
        }

        static public class FinalReview {
            static public By FORM = By.id("placeOrderForm1");
            static public By PLACE_ORDER = By.id("placeOrder");
            static public By PLACE_ORDER_DIV = By.cssSelector(".subtotal");
            static public By STEP_BODY = By.cssSelector(".checkout-steps");

            static public final String TERMS_CHECK = "termsCheck";
        }
    }

    static public class SavedCarts {
        static public By SAVED_CART_NAME = By.cssSelector(".js-saved-cart-name");
        static public By SAVED_CART_DESCRIPTION = By.cssSelector(".saved-cart-description");
        static public By DELETE_SAVED_CART = By.cssSelector(".js-delete-saved-cart");
        static public By RESTORE_SAVED_CART = By.cssSelector(".js-restore-saved-cart");
        static public By RESTORE_SAVED_CART_BUTTON = By.cssSelector(".js-save-cart-restore-btn");
        static public By KEEP_SAVED_CART = By.cssSelector(".js-keep-restored-cart");
    }

    static public class Search {
        static public By ADD_TO_LIST = By.cssSelector(".js-addToList");
        static public By AUTO_SUGGEST_RESULTS = By.cssSelector(".ui-autocomplete .ui-menu-item");
        static public By PRODUCT_LIST_RESULTS = By.className("product-item");
        static public By PRODUCT_LIST_RESULTS_ADD_TO_CART_BTN = By.className("js-enable-btn");
        static public By FACET_NAME = By.cssSelector("div[class='facet__name js-facet-name']");
        static public By FACET_COUNT = By.cssSelector("span[class='facet__value__count']");
        static public By PAGINATION_RESULT = By.cssSelector(".pagination-bar-results");
        static public By SORT_REFINE_BAR = By.cssSelector(".sort-refine-bar");
        static public By PAGINATION_BAR_RESULTS = By.cssSelector(".pagination-bar-results");
        static public By PRODUCT_LISTING_GRID = By.cssSelector(".product__listing");
        static public By PRODUCT_ITEM = By.cssSelector("div[class='product-item']");
        static public By PRODUCT_NAME = By.cssSelector("a[class='name']");
        static public By SORT_OPTIONS = By.id("sortOptions1");
        static public By SAVE_CART_NAME = By.id("saveCartName");
        static public By SAVE_CART_DESCRIPTION = By.id("saveCartDescription");
        static public By SAVE_CART_BUTTON = By.id("saveCartButton");
    }

    static public class ProductDetails {
        static public By ADD_TO_CART = By.id("addToCartButton");
        static public By ADD_TO_CART_INPUT = By.id("pdpAddtoCartInput");
    }

    static public class Page {
        static public By NAVIGATION_BAR = By.cssSelector("ul[class='nav__links nav__links--products js-offcanvas-links']");
        static public By FOOTER = By.cssSelector("footer");
        static public By MINI_CART = By.cssSelector(".nav-cart");
        static public By MINI_CART_LINK = By.cssSelector(".mini-cart-link");
        static public By SEARCH = By.cssSelector(".js-site-search-input");
    }

    static public class DeleteSavedCart {
        static public By DELETE = By.cssSelector(".js-savedcart_delete_confirm");
    }

    static public class OrderConfirmation {
        static public final By CHECKOUT_SUCCESS_HEADLINE = By.cssSelector(".checkout-success__body__headline");
        static public final By CHECKOUT_SUCCESS = By.cssSelector(".checkout-success__body");
    }

    static public class OrderDetails {
        static public final By REORDER_BUTTON = By.id("reorderButton");
        static public final By RETURN_ORDER_BUTTON = By.id("returnOrderButton");
    }

    static public class OrderHistory {
        static public final By ORDER_NUMBER = By.cssSelector(".responsive-table-link");
    }

    static public class ReturnOrder {
        static public final By RETURN_COMPLETE_ORDER = By.cssSelector(".js-return-complete-order-link");
        static public final By CONFIRM_RETURN_ORDER = By.id("returnOrderButtonConfirmation");
        static public final By SUBMIT_RETURN = By.id("submitreturnorderformvbutton");
    }

    static public class QuickView {
        static public final By SELECTOR = By.cssSelector(".quick-view-popup");
        static public final By ADDED_TO_CART_HEADLINE = By.cssSelector(".headline-text");
    }

    static public class ProductListing {
        static public final By COMPARE_CHECK_BOX = By.cssSelector(".product-compare-checkbox");
    }

    static public class ProductCompare {
        static public final String PRODUCT_CODE = "productCodePost";
        static public final String QUANTITY = "qty";

        static public final By GRID = By.cssSelector(".product-compare-grid");
        static public final By TRAY = By.cssSelector(".product-compare-tray");
        static public final By TRAY_PRODUCT = By.cssSelector(".product-compare-tray__product");
        static public final By COMPARE = By.cssSelector(".product-compare-tray__compare");
        static public final By TRAY_CLEAR = By.cssSelector(".product-compare-tray__clear");
        static public final By OVERLAY = By.cssSelector(".product-compare-overlay__grid");
        static public final By OVERLAY_PRODUCT = By.cssSelector(".product-compare-overlay__product");
        static public final By OVERLAY_REMOVE = By.cssSelector(".product-compare-overlay__product-remove");
        static public final By ADD_TO_CART = By.cssSelector(".product-compare-overlay__add-to-cart");
    }
    static public class Quote {
        static public final String EDIT_QUOTE = "Edit Quote";
        static public final String DRAFT = "Draft";

        static public final By CART_ID = By.cssSelector(".cart__id");
        static public final By QUOTE_STATUS = By.cssSelector(".label__value");
        static public final By QUOTE_HEAD = By.cssSelector(".quote__head");

        static public final By ORDER_HISTORY_TABLE = By.cssSelector(".orderhistory-list-table");
        static public final By QUOTES_ITEMS = By.cssSelector(".responsive-table-item");
        static public final By STATUS = By.cssSelector(".status");
        static public final By QUOTE_LINK = By.cssSelector(".responsive-table-link");

        static public final By SUBMIT_QUOTE = By.cssSelector(".js-quote-submit-btn");
        static public final By CANCEL_QUOTE = By.cssSelector(".js-quote-cancel-btn");
        static public final By ALERT = By.cssSelector(".getAccAlert");
        static public final By SUBMIT_YES = By.id("submitYesButton");
        static public final By CANCEL_YES = By.id("cancelYesButton");
    }

    static public class ReturnHistory {
        static public final String CANCELLING = "Cancelling";

        static public final By STATUS = By.cssSelector(".status");
        static public final By ORDER_HISTORY = By.cssSelector(".orderhistory-list-table");
        static public final By ORDER_ITEM = By.cssSelector(".responsive-table-item");
        static public final By RETURN_DETAILS_LINK = By.cssSelector(".responsive-table-link");
    }

    static public class ReturnDetail {
        static public final By CANCEL = By.id("cancelReturnButton");
        static public final By SUBMIT = By.id("submitcancelreturnformbutton");
    }

    static public class AccountSummary {
        static public final String STATUS = "documentStatus";
        static public final String FILTER_BY_KEY = "filterByKey";
        static public final String FILTER_BY_VALUE = "filterByValue";
        static public final String DOCUMENT_NUMBER = "documentNumber";
        static public final By ACCOUNT_SECTION = By.cssSelector(".account-section");
        static public final By DOCUMENT_FORM = By.id("filterByCriteriaForm");
        static public final By ACCOUNT_SUMMARY_TABLE = By.cssSelector(".account-summary-table");
    }
}
