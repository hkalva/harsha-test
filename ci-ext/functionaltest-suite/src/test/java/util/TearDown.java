package util;

import java.util.HashMap;
import java.util.Map;

/**
 * Test Tear Down and Clean Up.
 */
public class TearDown {

    /**
     * Unlock the user account.
     *
     * @param uid
     */
    public static void unlockUserAccount(final String uid) {
        final Map<String, Object> fieldValues = new HashMap<>();
        fieldValues.put("p_logindisabled", false);
        DataUtil.updateUser(uid, fieldValues);
    }

}
