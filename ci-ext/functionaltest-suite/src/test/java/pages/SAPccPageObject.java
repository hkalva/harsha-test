package pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.Elements;

import java.util.List;
import java.util.Optional;

public abstract class SAPccPageObject extends PageObject {


    @FindBy(id = "js-site-search-input")
    WebElementFacade searchField;

    @FindBy(className = "js-myCompany-toggle")
    WebElementFacade toggleMyCompanyNav;

    @FindBy(id = "signedInCompanyToggle")
    WebElementFacade mobileMyCompanyToggle;

    @FindBy(css = "js-myCompany-root")
    WebElementFacade mobileMyCompanyRoot;

    @FindBy(className = "js-myCompanyLinksContainer")
    WebElementFacade mobileMyCompanyLinksContainer;

    @FindBy(className = "js-toggle-sm-navigation")
    WebElementFacade mobileMenuToggle;

    @FindBy(className = "js-toggle-xs-search")
    WebElementFacade mobileSearchToggle;

    @FindBy(className = "js_search_button")
    WebElementFacade searchButton;

    @FindBy(css = ".global-alerts .alert")
    WebElementFacade alertElement;

    @FindBy(css = ".back-link .label")
    WebElementFacade backLinkLabel;

    @FindBy(css = "body")
    protected WebElementFacade body;

    @FindBy(id = "colorbox")
    WebElementFacade modal;

    @FindBy(className = "js-show-facets")
    WebElementFacade refineButton;

    @FindBy(css = ".breadcrumb")
    WebElementFacade breadCrumb;

    @FindBy(css = ".global-alerts")
    WebElementFacade globalMessage;

    public void waitForLoad() {
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        WebDriverWait wait = new WebDriverWait(getDriver(), 30);
        wait.until(pageLoadCondition);
    }

    public List<WebElementFacade> findAutoSuggestResults() {
        return findAll(Elements.Search.AUTO_SUGGEST_RESULTS);
    }

    public WebElementFacade getProductGrid() {
        return find(Elements.Search.PRODUCT_LISTING_GRID);
    }

    public WebElementFacade getNavigationBar() {
        return find(Elements.Page.NAVIGATION_BAR);
    }

    public WebElementFacade getSearchField() {
        return searchField;
    }

    public WebElementFacade getMobileSearchToggle() {
        return mobileSearchToggle;
    }

    public WebElementFacade getSearchButton() {
        return searchButton;
    }

    public WebElementFacade getToggleMyCompanyNav() {
        return toggleMyCompanyNav;
    }

    public WebElementFacade getMobileMyCompanyToggle() {
        return mobileMyCompanyToggle;
    }

    public WebElementFacade getMobileMyCompanyRoot() {
        return mobileMyCompanyRoot;
    }

    public WebElementFacade getMobileMyCompanyLinksContainer() {
        return mobileMyCompanyLinksContainer;
    }

    public WebElementFacade getMobileMenuToggle() {
        return mobileMenuToggle;
    }

    public WebElementFacade getBody() {
        return body;
    }

    public WebElementFacade getModal() {
        return modal;
    }

    public WebElementFacade getAlertElement() {
        return alertElement;
    }

    public WebElementFacade getRefineButton() {
        return refineButton;
    }

    public WebElementFacade getMyAccount() {
        return findAll(Elements.Account.ACCOUNT).stream()
                .filter(element -> element.isVisible())
                .findFirst()
                .get();
    }

    public String getBackLinkLabel() {
        return backLinkLabel.isVisible() ? StringUtils.trim(backLinkLabel.getText()) : StringUtils.EMPTY;
    }

    public void scrollToElement(By by) {
        WebElementFacade webElementFacade = find(by);
        scrollToElement(webElementFacade);
    }

    public void scrollToElement(WebElementFacade element) {
        ((JavascriptExecutor) getDriver()).executeScript(
                "arguments[0].scrollIntoView();", element);
    }

    public void scrollToTop() {
        getJavascriptExecutorFacade().executeScript("window.scrollTo(0,0)");
    }

    public void scrollToBottom() {
        getJavascriptExecutorFacade().executeScript("window.scrollTo(0,document.body.scrollHeight)");
    }

    public boolean activeBreadcrumIs(String breadCrumbText) {
        Optional<WebElementFacade> webElementFacade = breadCrumb.thenFindAll(By.cssSelector(".active"))
                .stream().filter(element -> element.getTextValue().equalsIgnoreCase(breadCrumbText) ||
                        element.getTextValue().startsWith(breadCrumbText.toUpperCase()))
                .findFirst();
        return webElementFacade.isPresent();
    }

    public boolean globalMessageIs(String message) {
        return globalMessage.getTextValue().indexOf(message) > -1;
    }

    public boolean bodyHasClass(String cssClass) {
        return body.hasClass(cssClass);
    }

    public  void clickOn(By by) {
        clickOn((WebElementFacade) find(by));;
    }
}
