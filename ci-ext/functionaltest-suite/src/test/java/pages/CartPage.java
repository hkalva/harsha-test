package pages;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.junit.Assert;
import util.Elements;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/cart")
public class CartPage extends SAPccPageObject {

    public boolean onPage() {
        return body.hasClass("page-cartPage");
    }

    public void newCart() {
        find(Elements.Cart.NEW_CART).click();
    }

    public int getNoOfItemsInCart() {
        return findAll(Elements.Cart.CART_ITEM_LIST).size();
    }

    public void goToSavedCarts() {
        List<WebElementFacade> webElementFacadeList = findAll(Elements.Cart.SAVED_CARTS);
        if (!webElementFacadeList.isEmpty())
            webElementFacadeList.get(0).click();
    }

    public boolean hasAllEntriesFromOrder(List<String> orderProducts) {
        List<WebElementFacade> webElementFacades = findAll(Elements.Cart.CART_ITEM_LIST);
        List<String> products = webElementFacades.stream()
                .map(element -> element.then(Elements.Cart.ITEM_CODE).getTextValue())
                .collect(Collectors.toList());
        return orderProducts.size() == products.size() && products.containsAll(orderProducts);

    }

    public void checkout() {
        find(Elements.Cart.CHECKOUT_BUTTON).click();
    }

    public boolean hasProducts(Map<String, String> cartProducts) {
        List<WebElementFacade> webElementFacades = findAll(Elements.Cart.CART_ITEM_LIST);
        Assert.assertTrue("No of products is cart does not match ", cartProducts.size() == webElementFacades.size());
        return webElementFacades.stream()
                .map(element -> validateProductAndQuantity(element, cartProducts))
                        .allMatch(Boolean::booleanValue);
    }

    private boolean validateProductAndQuantity(WebElementFacade element, Map<String, String> cartProducts) {
        scrollToElement(element);
        String productCode = element.then(Elements.Cart.ITEM_CODE).getTextValue();
        WebElementFacade quantityField = element.thenFindAll(Elements.Cart.QUANTITY_INPUT).stream()
                .filter(quantityElement -> quantityElement.isVisible()).findFirst().get();
        String quantity =  quantityField.getValue();
        String addedQuantity = cartProducts.get(productCode.trim());
        boolean productQuantityMatched = quantity != null && addedQuantity != null && addedQuantity.equalsIgnoreCase(quantity);
        Assert.assertTrue("Cart Product and quantity do not match for : " +  productCode, productQuantityMatched);
        return productQuantityMatched;
    }
}