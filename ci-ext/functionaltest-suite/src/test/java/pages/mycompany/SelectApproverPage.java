package pages.mycompany;

import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import pages.PaginatedPage;
import static util.TestUtil.clean;

public class SelectApproverPage extends PaginatedPage {

    public WebElementFacade getApproverToggleLink(final String uid) {
        return find(By.cssSelector("#selection-" + clean(uid) + " a"));
    }
}
