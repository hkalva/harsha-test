package pages.mycompany;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import pages.PaginatedPage;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-users/usergroups")
public class SelectGroupPage extends PaginatedPage {


    public WebElementFacade getGroupToggleLink(final String uid) {
        return find(By.cssSelector("#selection-" + uid + " a"));
    }
}
