package pages.mycompany.unit;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import pages.SAPccPageObject;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-units/details")
public class UnitDetailPage extends SAPccPageObject {

    @FindBy(css = ".item-action .edit")
    WebElementFacade editButton;

    public WebElementFacade getEditButton() {
        return editButton;
    }

    public WebElementFacade getAddNewAddressButton() {
        return findAll(By.cssSelector("a.edit")).get(1);
    }

    public WebElementFacade getAddNewCostCenterButton() {
        return findAll(By.cssSelector("a.edit")).get(2);
    }
}
