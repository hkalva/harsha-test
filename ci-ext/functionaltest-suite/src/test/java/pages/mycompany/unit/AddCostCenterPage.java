package pages.mycompany.unit;

import components.Form;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import pages.SAPccPageObject;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-units/addcostcenter")
public class AddCostCenterPage extends SAPccPageObject {

    @FindBy(id = "b2BCostCenterform")
    WebElementFacade form;

    @FindBy(css = ".accountActions-bottom button[type=submit]")
    WebElementFacade formSaveButton;


    public Form getForm() {
        return new Form(form);
    }

    public WebElementFacade getFormSaveButton() {
        return formSaveButton;
    }
}
