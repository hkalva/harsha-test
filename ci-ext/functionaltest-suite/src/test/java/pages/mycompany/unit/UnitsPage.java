package pages.mycompany.unit;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import pages.SAPccPageObject;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-units")
public class UnitsPage extends SAPccPageObject {

    @FindBy(css = ".button.add")
    WebElementFacade addNewButton;

    public WebElementFacade getAddNewButton() {
        return addNewButton;
    }
}
