package pages.mycompany.group;

import net.thucydides.core.annotations.DefaultUrl;
import pages.SAPccPageObject;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-usergroups/edit")
public class EditGroupPage extends SAPccPageObject {
}
