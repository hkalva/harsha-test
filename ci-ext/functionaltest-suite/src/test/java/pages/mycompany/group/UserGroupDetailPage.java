package pages.mycompany.group;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import pages.SAPccPageObject;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-usergroups/details")
public class UserGroupDetailPage extends SAPccPageObject {

    @FindBy(css = ".item-action a.button.edit")
    WebElementFacade editButton;

    @FindBy(css = "a[data-action-confirmation-modal-id=disable]")
    WebElementFacade disableButton;

    @FindBy(css = "a[data-action-confirmation-modal-id=delete]")
    WebElementFacade deleteButton;

    @FindBy(className = "enable-link")
    WebElementFacade enableButton;

    @FindBy(css = "#cboxLoadedContent #command button[type=submit]")
    WebElementFacade confirmActionButton;

    public WebElementFacade getConfirmActionButton() {
        return confirmActionButton;
    }

    public WebElementFacade getEditButton() {
        return editButton;
    }

    public WebElementFacade getDisableButton() {
        return disableButton;
    }

    public WebElementFacade getEnableButton() {
        return enableButton;
    }

    /**
     * Returns a reference to the "Add Existing" link for adding a permission.
     *
     * @return
     */
    public WebElementFacade getAddPermissionButton() {
        return findAll(By.cssSelector("a.edit")).get(1);
    }

    /**
     * Returns a reference to the "Add Existing" link for adding a user.
     *
     * @return
     */
    public WebElementFacade getAddUserButton() {
        return findAll(By.cssSelector("a.edit")).get(2);
    }

    public WebElementFacade getDeleteButton() {
        return deleteButton;
    }
}
