package pages.mycompany.group;

import components.Form;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import pages.SAPccPageObject;

@DefaultUrl("")
public class AddGroupPage extends SAPccPageObject {

    @FindBy(id = "b2BUserGroupForm")
    WebElementFacade costCenterForm;

    @FindBy(css = ".accountActions-bottom button[type=submit]")
    WebElementFacade costCenterFormSaveButton;


    public Form getForm() {
        return new Form(costCenterForm);
    }

    public WebElementFacade getFormSaveButton() {
        return costCenterFormSaveButton;
    }
}
