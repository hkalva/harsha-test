package pages.mycompany;

import components.Form;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import pages.SAPccPageObject;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-users/create")
public class AddNewUserPage extends SAPccPageObject {

    @FindBy(id = "b2BCustomerForm")
    WebElementFacade b2bCustomerForm;

    public WebElementFacade getB2bCustomerForm() {
        return b2bCustomerForm;
    }

    public Form getForm() {
        return new Form(b2bCustomerForm);
    }
}
