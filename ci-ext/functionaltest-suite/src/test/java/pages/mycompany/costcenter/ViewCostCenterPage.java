package pages.mycompany.costcenter;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import pages.SAPccPageObject;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-costcenters/view")
public class ViewCostCenterPage extends SAPccPageObject {

    @FindBy(css = ".item-action a.button.edit")
    WebElementFacade editButton;

    @FindBy(css = ".disable-link a")
    WebElementFacade disableButton;

    @FindBy(className = "enable-link")
    WebElementFacade enableButton;

    @FindBy(css = "#cboxLoadedContent #command button[type=submit]")
    WebElementFacade confirmActionButton;

    @FindBy(css = ".account-list-header a.edit")
    WebElementFacade addBudgetLink;

    public WebElementFacade getAddBudgetLink() {
        return addBudgetLink;
    }

    public WebElementFacade getConfirmActionButton() {
        return confirmActionButton;
    }

    public WebElementFacade getEditButton() {
        return editButton;
    }

    public WebElementFacade getDisableButton() {
        return disableButton;
    }

    public WebElementFacade getEnableButton() {
        return enableButton;
    }
}
