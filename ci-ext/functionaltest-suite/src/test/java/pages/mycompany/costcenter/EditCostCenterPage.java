package pages.mycompany.costcenter;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import pages.SAPccPageObject;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-costcenters/edit")
public class EditCostCenterPage extends SAPccPageObject {

    @FindBy(id = "b2BCostCenterform")
    WebElementFacade costCenterForm;

    @FindBy(css = ".accountActions-bottom button[type=submit]")
    WebElementFacade costCenterFormSaveButton;


    public WebElementFacade getCostCenterForm() {
        return costCenterForm;
    }

    public WebElementFacade getCostCenterFormSaveButton() {
        return costCenterFormSaveButton;
    }
}
