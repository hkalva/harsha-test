package pages.mycompany.costcenter;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import pages.PaginatedPage;

import static util.TestUtil.clean;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-costcenters/selectBudget")
public class SelectBudgetPage extends PaginatedPage {

    public WebElementFacade getToggleLink(final String uid) {
        return find(By.cssSelector("#span-" + clean(uid) + " a"));
    }
}
