package pages.mycompany.costcenter;

import net.thucydides.core.annotations.DefaultUrl;
import pages.ResponsiveTablePage;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-costcenters/")
public class ManageCostCentersPage extends ResponsiveTablePage {
}
