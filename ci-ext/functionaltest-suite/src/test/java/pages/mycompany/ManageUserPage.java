package pages.mycompany;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import pages.SAPccPageObject;
import static util.TestUtil.clean;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-users/details")
public class ManageUserPage extends SAPccPageObject {

    @FindBy(className = "disable-link")
    WebElementFacade disableLink;

    @FindBy(css = ".enable-link button[type=submit]")
    WebElementFacade enableLink;

    @FindBy(css = "#js-action-confirmation-modal-content-disable button[type=submit]")
    WebElementFacade confirmDisableButton;

    @FindBy(css = ".item-action a.button.edit")
    WebElementFacade editUserButton;

    @FindBy(css = "#cboxLoadedContent #command button[type=submit]")
    WebElementFacade confirmActionButton;

    public WebElementFacade getConfirmActionButton() {
        return confirmActionButton;
    }

    public WebElementFacade getDisableLink() {
        return disableLink;
    }

    public WebElementFacade getEnableLink() {
        return enableLink;
    }

    public WebElementFacade getConfirmDisableButton() {
        return confirmDisableButton;
    }



    public WebElementFacade getEditUserButton() {
        return editUserButton;
    }

    /**
     * Returns a reference to the "Add Existing" link for adding a group.
     *
     * @return
     */
    public WebElementFacade getAddGroupButton() {
        return findAll(By.cssSelector("a.edit")).get(3);
    }

    /**
     * Returns a reference to the "Add Existing" link for adding a permission.
     *
     * @return
     */
    public WebElementFacade getAddPermissionButton() {
        return findAll(By.cssSelector("a.edit")).get(2);
    }

    /**
     * Returns a reference to the "Add Existing" link for adding a permission.
     *
     * @return
     */
    public WebElementFacade getAddApproverButton() {
        return findAll(By.cssSelector("a.edit")).get(1);
    }

    public WebElementFacade getRemoveGroupCardLink(final String uid) {
        return find(By.cssSelector("a[data-action-confirmation-modal-id=\"removeUserGroup-" + uid + "\"]"));
    }

    public WebElementFacade getRemovePermissionCardLink(final String permission) {
        return find(By.cssSelector("a[data-action-confirmation-modal-id=\"removePermission-" + clean(permission) + "\"]"));
    }

    public WebElementFacade getRemoveApproverCardLink(final String uid) {
        return find(By.cssSelector("a[data-action-confirmation-modal-id=\"removeApprover-" + clean(uid) + "\"]"));
    }

}
