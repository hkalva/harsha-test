package pages.mycompany;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import pages.PaginatedPage;
import static util.TestUtil.clean;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-users/permissions")
public class SelectPermissionPage extends PaginatedPage {

    public WebElementFacade getPermissionToggleLink(final String permission) {
        return find(By.cssSelector("#span-" + clean(permission) + " a"));
    }
}
