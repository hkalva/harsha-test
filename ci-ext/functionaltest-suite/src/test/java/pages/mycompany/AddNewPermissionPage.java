package pages.mycompany;

import components.Form;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import pages.SAPccPageObject;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-permissions/add")
public class AddNewPermissionPage extends SAPccPageObject {

    @FindBy(id = "b2BPermissionForm")
    WebElementFacade permissionForm;

    @FindBy(id = "selectNewPermissionType")
    WebElementFacade permissionTypeMenu;

    @FindBy(css = ".accountActions-bottom button[type=submit]")
    WebElementFacade permissionFormSaveButton;

    public Form getPermissionForm() {
        return new Form(permissionForm);
    }

    public WebElementFacade getPermissionTypeMenu() {
        return permissionTypeMenu;
    }

    public WebElementFacade getPermissionFormSaveButton() {
        return permissionFormSaveButton;
    }
}
