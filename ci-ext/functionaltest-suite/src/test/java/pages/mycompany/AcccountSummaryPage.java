package pages.mycompany;

import components.Form;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.commons.lang3.BooleanUtils;
import org.openqa.selenium.By;
import pages.SAPccPageObject;
import util.Elements;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class AcccountSummaryPage  extends SAPccPageObject  {

    public void selectCostCenter(String costCenter) {
        List<WebElementFacade> toggles =  findAll(By.cssSelector(".accordion-toggle"));
        while(!toggles.isEmpty()) {
            toggles = toggles.stream()
                    .filter(e ->  {
                        if(e.isVisible() && e.getRect().height > 0 )
                        {
                            if(e.hasClass("collapsed"))
                                scrollToElement(e);
                                 e.click();
                            return false;
                        } else {
                            return true;
                        }
                    })
                    .collect(Collectors.toList());
        }

        List<WebElementFacade> links =  findAll(By.cssSelector(".accordion-lnk"));
        Optional<WebElementFacade>  costCenterLink = links.stream()
                .filter(e -> e.getText().equalsIgnoreCase(costCenter))
                .findFirst();
        if(costCenterLink.isPresent())
            costCenterLink.get().click();
    }


    public void searchForDocumentsWithStatus(String status) {
        WebElementFacade form = find(Elements.AccountSummary.DOCUMENT_FORM);
        Form addressForm = new Form(form);
        addressForm.select(Elements.AccountSummary.STATUS, status);
        addressForm.submit();
    }

    public boolean allDisplayedDocumentsAreHaveStatus(String status) {
        WebElementFacade accountSummaryTable = find(Elements.AccountSummary.ACCOUNT_SUMMARY_TABLE);
        List<WebElementFacade> items =  accountSummaryTable.thenFindAll( By.cssSelector(".responsive-table-item"));
        Optional<WebElementFacade> inCorrectStatus = items.stream()
                .map(item -> (WebElementFacade) item.find(By.xpath("//td[14]")))
                .filter(docStatus -> !status.equalsIgnoreCase(docStatus.getTextValue()))
                .findFirst();
        return inCorrectStatus.isPresent(); // there should not be any elements with status not other @Param status
    }

    public void filterByDocumentId(String documentNumber) {
        WebElementFacade form = find(Elements.AccountSummary.DOCUMENT_FORM);
        Form addressForm = new Form(form);
        addressForm.select(Elements.AccountSummary.FILTER_BY_KEY, Elements.AccountSummary.DOCUMENT_NUMBER);
        addressForm.typeAndTab(Elements.AccountSummary.FILTER_BY_VALUE, documentNumber);
        addressForm.submit();
    }

    public boolean isDocumentNumber(String documentNumber) {
        WebElementFacade accountSummaryTable = find(Elements.AccountSummary.ACCOUNT_SUMMARY_TABLE);
        List<WebElementFacade> items =  accountSummaryTable.thenFindAll( By.cssSelector(".responsive-table-item"));
        Optional<WebElementFacade> inCorrectStatus = items.stream()
                .map(item -> (WebElementFacade) item.find(By.xpath("//td[6]")))
                .filter(docStatus -> !documentNumber.equalsIgnoreCase(docStatus.getTextValue()))
                .findFirst();
        return inCorrectStatus.isPresent();
    }
}
