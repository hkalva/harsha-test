package pages.mycompany;

import net.thucydides.core.annotations.DefaultUrl;
import pages.ResponsiveTablePage;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-users")
public class ManageUsersPage extends ResponsiveTablePage {
}
