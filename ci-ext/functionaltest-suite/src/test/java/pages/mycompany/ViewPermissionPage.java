package pages.mycompany;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import pages.SAPccPageObject;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-permissions/view")
public class ViewPermissionPage extends SAPccPageObject {

    @FindBy(css = ".item-action a.button.edit")
    WebElementFacade editPermissionButton;

    @FindBy(css = ".disable-link a")
    WebElementFacade disableButton;

    @FindBy(className = "enable-link")
    WebElementFacade enableButton;

    @FindBy(css = "#cboxLoadedContent #command button[type=submit]")
    WebElementFacade confirmActionButton;

    public WebElementFacade getConfirmActionButton() {
        return confirmActionButton;
    }

    public WebElementFacade getEditPermissionButton() {
        return editPermissionButton;
    }

    public WebElementFacade getDisableButton() {
        return disableButton;
    }

    public WebElementFacade getEnableButton() {
        return enableButton;
    }
}
