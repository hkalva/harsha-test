package pages.mycompany.budget;

import net.thucydides.core.annotations.DefaultUrl;
import pages.ResponsiveTablePage;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-budgets")
public class ManageBudgetsPage extends ResponsiveTablePage {
}
