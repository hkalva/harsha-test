package pages.mycompany.budget;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import pages.SAPccPageObject;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-budgets/view")
public class ViewBudgetPage extends SAPccPageObject {

    @FindBy(css = ".item-action a.button.edit")
    WebElementFacade editButton;

    @FindBy(css = ".disable-link a")
    WebElementFacade disableButton;

    @FindBy(className = "enable-link")
    WebElementFacade enableButton;

    @FindBy(css = "#cboxLoadedContent #command button[type=submit]")
    WebElementFacade confirmActionButton;

    public WebElementFacade getEditButton() {
        return editButton;
    }

    public WebElementFacade getDisableButton() {
        return disableButton;
    }

    public WebElementFacade getEnableButton() {
        return enableButton;
    }

    public WebElementFacade getConfirmActionButton() {
        return confirmActionButton;
    }
}
