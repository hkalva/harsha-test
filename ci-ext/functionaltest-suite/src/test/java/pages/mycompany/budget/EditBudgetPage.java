package pages.mycompany.budget;

import net.thucydides.core.annotations.DefaultUrl;
import pages.SAPccPageObject;

@DefaultUrl("https://rlp.local:9002/rlp/en/USD/my-company/organization-management/manage-budgets/edit")
public class EditBudgetPage extends SAPccPageObject {
}
