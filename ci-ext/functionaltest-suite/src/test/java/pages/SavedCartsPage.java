package pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import util.Elements;

import java.util.List;
import java.util.Optional;

public class SavedCartsPage extends SAPccPageObject {

    public Optional<WebElementFacade> findSavedCart(String name, String description) {
        List<WebElementFacade> savedCarts = findAll(Elements.SavedCarts.SAVED_CART_NAME);
        Optional<WebElementFacade> savedCart = savedCarts.stream()
                .filter(webElementFacade -> name.equals(webElementFacade.getTextValue()))
                .filter(webElementFacade -> {
                    WebElementFacade descriptionElement = webElementFacade.then(By.xpath("..")).then(By.xpath("..")).then(Elements.SavedCarts.SAVED_CART_DESCRIPTION);
                    return description.equals(descriptionElement.getTextValue());
                })
                .findFirst();
        return savedCart;
    }

    public boolean verifyNoOfSavedCarts(int noOfCarts) {
        this.waitForLoad();
        List<WebElementFacade> savedCarts = findAll(Elements.SavedCarts.SAVED_CART_NAME);
        return noOfCarts == savedCarts.size();
    }

    public void removeSavedCart() {
        List<WebElementFacade> savedCarts = findAll(Elements.SavedCarts.SAVED_CART_NAME);
        WebElementFacade deleteButton = savedCarts.get(0).then(By.xpath("..")).then(By.xpath("..")).then(Elements.SavedCarts.DELETE_SAVED_CART);
        deleteButton.click();
    }

    public void gotoSavedCartDetails() {
        List<WebElementFacade> savedCarts = findAll(Elements.SavedCarts.SAVED_CART_NAME);
        Optional<WebElementFacade> savedCart = savedCarts.stream()
                .filter(webElementFacade -> webElementFacade.isVisible() && webElementFacade.isEnabled())
                .findFirst();
        if (savedCart.isPresent())
            savedCart.get().click();
        else {
            Assert.assertTrue(savedCart.isPresent());
        }
    }


    public void restoreSavedCart() {
        List<WebElementFacade> savedCarts = findAll(Elements.SavedCarts.RESTORE_SAVED_CART);
        Optional<WebElementFacade> savedCart = savedCarts.stream()
                .filter(webElementFacade -> webElementFacade.isEnabled() && webElementFacade.isVisible())
                .findFirst();
        if (savedCart.isPresent()) {
            savedCart.get().click();
        } else {
            Assert.assertTrue(savedCart.isPresent());
        }
    }

}
