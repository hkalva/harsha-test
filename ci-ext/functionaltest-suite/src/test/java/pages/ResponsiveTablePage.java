package pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public abstract class ResponsiveTablePage extends PaginatedPage {

    @FindBy(className = "responsive-table")
    WebElementFacade responsiveTable;

    @FindBy(css = ".button.add")
    WebElementFacade addNewButton;

    public WebElementFacade getResponsiveTable() {
        return responsiveTable;
    }

    public WebElementFacade getAddNewButton() {
        return addNewButton;
    }
}
