package pages;

import components.Form;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import util.Elements;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class WishlistsPage extends SAPccPageObject {

    public Optional<WebElementFacade> getWishlist(String name) {
        List<WebElementFacade> webElementFacadeList = findAll(Elements.Wishlist.RESPONSIVE_TABLE_ITEM);
        return webElementFacadeList.stream()
                .map(element -> ((WebElementFacade) element.find(By.tagName("a"))))
                .collect(Collectors.toList()).stream()
                .filter(element -> element.getText().equalsIgnoreCase(name))
                .findFirst();
    }

    public boolean hasProduct(String productCode) {
        return findAll(Elements.Wishlist.PRODUCT_INFO).stream()
                .filter(element -> element.getText().trim().equalsIgnoreCase(productCode))
                .findFirst().isPresent();
    }

    public boolean hasProducts() {
        return !findAll(Elements.Wishlist.PRODUCT_INFO).isEmpty();
    }

    public Map addToCart() {

        Optional<WebElementFacade> optional = findAll(Elements.Wishlist.PRODUCT).stream().findFirst();
        if (optional.isPresent()) {
            WebElementFacade product = optional.get();
            WebElementFacade addToCart = product.then(Elements.Wishlist.ADD_TO_CART);
            if (addToCart != null) {
                addToCart.click();
            }
            return Collections.singletonMap(product.getAttribute(Elements.Wishlist.DATA_PRODUCT_ID), product.getAttribute(Elements.Wishlist.DATA_PRODUCT_QTY));
        }
        return Collections.EMPTY_MAP;
    }

    public String removeProduct() {
        String productCode = null;
        Optional<WebElementFacade> optional = findAll(Elements.Wishlist.PRODUCT).stream().findFirst();
        if (optional.isPresent()) {
            WebElementFacade product = optional.get();
            productCode = product.getAttribute(Elements.Wishlist.DATA_PRODUCT_ID);
            product.then(Elements.Wishlist.REMOVE_PRODUCT_FORM).then(By.tagName(Elements.BUTTON)).click();
        }
        return productCode;
    }

    public void deleteWishlist() {
        Optional<WebElementFacade> optional = findAll(Elements.Wishlist.ACCOUNT_ACTIONS).stream().findFirst();
        if (optional.isPresent()) {
            optional.get().then(By.linkText(Elements.Wishlist.DELETE)).click();
            find(Elements.Wishlist.DELETE_CONFIRMATION_MODAL).then(By.tagName(Elements.BUTTON)).click();
        }
    }

    public Map<String, String> addAllToCart() {
        Map<String, String> productsMap = findAll(Elements.Wishlist.PRODUCT).stream()
                .collect(
                        Collectors.toMap(element -> element.getAttribute(Elements.Wishlist.DATA_PRODUCT_ID), element -> element.getAttribute(Elements.Wishlist.DATA_PRODUCT_QTY)));

        if (productsMap.size() > 0) {
            WebElementFacade addToCart = find(Elements.Wishlist.ADD_ALL_TO_CART);
            if (addToCart != null) {
                addToCart.click();
            }
        }
        return productsMap;
    }

    public void clickOnAddNew() {
        find(Elements.Wishlist.ACCOUNT_SECTION_HEADER).then(By.linkText(Elements.Wishlist.ADD_NEW)).click();
        waitForLoad();
    }

    public void createWishlist(String name, String description) {
        WebElementFacade form = find(Elements.Wishlist.CREATE_WISHIST_FORM);
        Form addToWishlistForm = new Form(form);
        addToWishlistForm.typeAndTab(Elements.Wishlist.NAME, name);
        addToWishlistForm.typeAndTab(Elements.Wishlist.DESCRIPTION, description);
        form.findElement(Elements.Wishlist.CONFIRM).click();
    }
}
