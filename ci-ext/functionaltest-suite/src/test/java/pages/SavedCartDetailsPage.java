package pages;

import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import util.Constants;
import util.Elements;

import java.util.List;
import java.util.Optional;

public class SavedCartDetailsPage extends SAPccPageObject {

    CurrentPage currentPage;

    public void removeSavedCartFromDetails() {
        List<WebElementFacade> webElementFacadeList = findAll(Elements.SavedCarts.DELETE_SAVED_CART);
        Optional<WebElementFacade> deleteSavedCart = webElementFacadeList.stream()
                .filter(deleteButton -> deleteButton.isVisible() && deleteButton.isEnabled())
                .findFirst();
        if (deleteSavedCart.isPresent()) {
            deleteSavedCart.get().click();
        } else {
            Assert.assertTrue(deleteSavedCart.isPresent());
        }
    }

    public boolean confirmUserIsOnSavedCartDetailsPage() {
        return currentPage.getTitle().startsWith(Constants.SaveCart.SAVED_CART_PAGE_TILE_PREFIX)
                && currentPage.activeBreadcrumIs(Constants.SaveCart.BREADCRUMB);
    }

    public void restoreSavedCartFromDetails() {
        List<WebElementFacade> savedCarts = findAll(Elements.SavedCarts.RESTORE_SAVED_CART_BUTTON);
        Optional<WebElementFacade> savedCart = savedCarts.stream()
                .filter(webElementFacade -> webElementFacade.isEnabled() && webElementFacade.isVisible())
                .findFirst();
        if (savedCart.isPresent()) {
            savedCart.get().click();
        } else {
            Assert.assertTrue(savedCart.isPresent());
        }
    }

}
