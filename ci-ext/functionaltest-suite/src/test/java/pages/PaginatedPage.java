package pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public abstract class PaginatedPage extends SAPccPageObject {

    @FindBy(css = ".pagination-bar.top .pagination-prev")
    WebElementFacade topBarPreviousButton;

    @FindBy(css = ".pagination-bar.bottom .pagination-prev")
    WebElementFacade bottomBarPreviousButton;

    @FindBy(css = ".pagination-bar.top .pagination-next")
    WebElementFacade topBarNextButton;

    @FindBy(css = ".pagination-bar.bottom .pagination-next")
    WebElementFacade bottomBarNextButton;

    @FindBy(css = ".account-header-done-btn a")
    WebElementFacade doneButton;

    public WebElementFacade getDoneButton() {
        scrollToTop();
        return doneButton;
    }

    public WebElementFacade getTopBarPreviousButton() {
        return topBarPreviousButton;
    }

    public WebElementFacade getBottomBarPreviousButton() {
        return bottomBarPreviousButton;
    }

    public WebElementFacade getTopBarNextButton() {
        return topBarNextButton;
    }

    public WebElementFacade getBottomBarNextButton() {
        return bottomBarNextButton;
    }
}
