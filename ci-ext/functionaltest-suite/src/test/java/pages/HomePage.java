package pages;

import net.thucydides.core.annotations.DefaultUrl;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import responsive.ResponsiveRunner;

@DefaultUrl("/rlp")
public class HomePage extends SAPccPageObject {
    private static final Logger LOGGER = LoggerFactory.getLogger(HomePage.class);


    public boolean onPage() {
        waitForLoad();
        Assert.assertTrue("HOME PAGE URL IS " + getDriver().getCurrentUrl() , false);
        return body.hasClass("page-dashboard");
    }
}
