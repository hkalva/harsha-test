package pages;

import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import util.Elements;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

abstract class ProductListingPage extends SAPccPageObject {

    public List<WebElementFacade> findSearchResults() {
        return findAll(Elements.Search.PRODUCT_LIST_RESULTS);
    }

    public WebElementFacade getProductGrid() {
        return find(Elements.Search.PRODUCT_LISTING_GRID);
    }

    public Optional<WebElementFacade> findInStockItem() {
        waitForLoad();
        return findSearchResults().stream()
                //.filter(item -> item.containsElements(Elements.Search.PRODUCT_LIST_RESULTS_ADD_TO_CART_BTN))
                .findFirst();
    }

    public void addItemToCart() {
        final Optional<WebElementFacade> item = findAll(Elements.Search.PRODUCT_LIST_RESULTS_ADD_TO_CART_BTN).stream().findAny();
        if (item.isPresent()) {
            item.get().click();
            modal.waitUntilVisible();
        } else {
            Assert.fail("Could not add item to cart. No in-stock items found");
        }
    }

    public String clickOnAddToWishlist(int index) {
        String productCode = null;
        this.waitForLoad();

        WebElementFacade productElement = findSearchResults().get(index);
        WebElementFacade addToList = productElement.then(Elements.Search.ADD_TO_LIST);
        productCode = addToList.getAttribute("data-productcode");
        addToList.click();
        return productCode;
    }

    /**
     * select for or remove a from comparison
     * @param index
     * @return
     */
    public String selectProductForCompare(int index, WebDriver driver) {
        WebElementFacade productCompareTray = find(Elements.ProductCompare.TRAY);
        if(productCompareTray.isVisible()) {
            // hide product compare tray, to access the checkboxes for selecting the products
            ((JavascriptExecutor) driver).executeScript("arguments[0].style.visibility='hidden'", productCompareTray);
        }
        String productCode = null;
        this.waitForLoad();
        WebElementFacade productElement = findSearchResults().get(index);
        WebElementFacade compareCheckbox = productElement.then(Elements.ProductListing.COMPARE_CHECK_BOX);
        scrollToElement(productElement);
        productCode =  compareCheckbox.getAttribute("class");
        productCode = productCode.substring(productCode.lastIndexOf('-')+1);
        compareCheckbox.click();
        return productCode;
    }

    /**
     * Remove product from the product compare tray
     * @param index
     * @return product code
     */
    public String removeProductFromCompareTray(int index) {
        List<WebElementFacade> productElements = find(Elements.ProductCompare.TRAY).thenFindAll(Elements.ProductCompare.TRAY_PRODUCT);
        WebElementFacade productElement = productElements.get(index);
        String productCode =  productElement.getAttribute("class");
        productCode = productCode.substring(productCode.lastIndexOf('-') + 1);
        productElement.find(By.tagName("button")).click();
        return productCode;
    }

    /**
     * Remove product from the product compare overlay
     * @param index
     * @return product code
     */
    public String removeProductFromCompareOverlay(int index) {
        List<WebElementFacade> productElements = find(Elements.ProductCompare.OVERLAY).thenFindAll(Elements.ProductCompare.OVERLAY_PRODUCT);
        WebElementFacade productElement = productElements.get(index);
        String productCode =  productElement.getAttribute("class");
        productCode = productCode.substring(productCode.lastIndexOf('-') + 1);
        productElement.find(By.tagName("button")).click();
        return productCode;
    }

    public WebElementFacade getFacetLinkElement(String facetName, String facetValue) {

        WebElementFacade facetDivElement = getFacetDivElement(facetName);
        if (facetDivElement != null) {
            List<WebElementFacade> facetLinks = facetDivElement.then(By.xpath("..")).thenFindAll(By.tagName("a"));
            for (WebElementFacade link : facetLinks) {
                if (facetValue.equals(link.getTextContent().trim())) {
                    return link;
                }
            }
        }
        return null;
    }

    public WebElementFacade getRemoveFacetLinkElement(String facetName, String facetValue) {

        WebElementFacade facetDivElement = getFacetDivElement(facetName);
        if (facetDivElement != null) {
            List<WebElementFacade> facetLinks = facetDivElement.then(By.xpath("..")).thenFindAll(By.tagName("li"));
            for (WebElementFacade link : facetLinks) {
                ;
                if (facetValue.equals(link.getText().trim())) {
                    return link.find(By.cssSelector("a"));
                }
            }
        }
        return null;
    }

    public WebElementFacade getFacetDivElement(String facetName) {
        List<WebElementFacade> webElementFacades = findAll(Elements.Search.FACET_NAME);
        for (WebElementFacade facetNameDiv : webElementFacades) {
            if (facetName.equals(facetNameDiv.getTextContent().trim())) {
                return facetNameDiv;
            }
        }
        return null;
    }

    public int getNoOfProductsOnPage() {
        return getProductGrid().thenFindAll("div").size();
    }

    public void hoverOverProduct() {
        WebElementFacade productImage = findSearchResults().get(0).then(By.tagName("img"));
        Actions actions = new Actions(getDriver());
 //       scrollToElement((WebElementFacade) find(Elements.Search.SORT_REFINE_BAR));
        actions.moveToElement(productImage).perform();
    }

    public boolean quickViewButtonIsDisplayed() {
        WebElementFacade quickViewButton = findSearchResults().get(0).then(By.cssSelector(".quickview-button"));
        quickViewButton.waitUntilVisible();
        return findSearchResults().get(0).then(By.cssSelector(".quickview-button")).isVisible();
    }


    public void clickOnQuickView() {
        findSearchResults().get(0).then(By.cssSelector(".quickview-button")).click();
        waitFor(6000);
    }

    /**
     * check if all the product in the {@Link List<String>} are present in product compare tray
     * @param products
     * @return boolean
     */
    public boolean selectedProductAreInCompareTray(List<String> products) {
       return selectedProductAreInCompare(Elements.ProductCompare.GRID, Elements.ProductCompare.TRAY_PRODUCT, products);
    }

    /**
     * check if all the product in the {@Link List<String>} are present in product compare overlay
     * @param products
     * @return
     */
    public boolean selectedProductAreInCompareOverlay(List<String> products) {
       return selectedProductAreInCompare(Elements.ProductCompare.OVERLAY, Elements.ProductCompare.OVERLAY_PRODUCT, products);
    }

    /**
     * check if all the product in the {@Link List<String>} are present in product compare container
     * @param productContainerElement
     * @param productElement
     * @param products
     * @return
     */
    private boolean selectedProductAreInCompare(By productContainerElement, By productElement, List<String> products) {
        List<WebElementFacade> productElements = find(productContainerElement).thenFindAll(productElement);
        List<String> productsFromTray = productElements.stream()
                .map(element -> { String classAttribute =  element.getAttribute("class");
                    return classAttribute.substring(classAttribute.lastIndexOf("-") + 1);
                })
                .collect(Collectors.toList());
        return products.equals(productsFromTray);
    }

    public boolean productsAreRemovedFromCompareTray() {
        try {
            find(Elements.ProductCompare.GRID).thenFindAll(Elements.ProductCompare.TRAY_PRODUCT).isEmpty();
            return false;
        } catch (NoSuchElementException e) {
            return true;
        }
    }

    public void compareProducts() {
        Optional<WebElementFacade> compareButton = findAll(Elements.ProductCompare.COMPARE).stream()
                .filter(element -> element.isVisible())
                .findFirst();
        compareButton.get().click();
    }

    public void clearAllComparedProducts() {
        Optional<WebElementFacade> compareButton = findAll(Elements.ProductCompare.TRAY_CLEAR).stream()
                .filter(element -> element.isVisible())
                .findFirst();
        compareButton.get().click();
    }

    /**
     * check if  product compare overlay is visible
     * @return boolean
     */
    public boolean isProductCompareOverlayVisible() {
        WebElementFacade productCompareOverlay = find(Elements.ProductCompare.OVERLAY);
        return productCompareOverlay.isEnabled() && productCompareOverlay.isVisible();
    }

}
