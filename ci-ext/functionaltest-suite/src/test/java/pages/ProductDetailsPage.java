package pages;

import util.Elements;

public class ProductDetailsPage extends SAPccPageObject {

    public void addToCart() {
      find(Elements.ProductDetails.ADD_TO_CART).click();
    }
}
