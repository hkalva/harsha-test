package pages;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.junit.Assert;
import util.Elements;

import java.util.Optional;

@DefaultUrl("https://rlp.local:9002/rlp/search")
public class SearchResultsPage extends ProductListingPage {

    public boolean onPage() {
        return body.hasClass("page-searchGrid");
    }

}
