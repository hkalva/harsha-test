package pages.myaccount;

import pages.SAPccPageObject;
import util.Elements;

public class OrderHistoryPage extends SAPccPageObject {
    public void clickOnOrder(String orderNumber) {
        findAll(Elements.OrderHistory.ORDER_NUMBER).stream()
                .filter(element -> element.getTextValue().equals(orderNumber))
                .findFirst().get().click();
    }
}
