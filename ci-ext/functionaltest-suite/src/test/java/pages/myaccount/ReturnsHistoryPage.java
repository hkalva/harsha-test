package pages.myaccount;

import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import pages.SAPccPageObject;
import util.Elements;

import java.util.List;
import java.util.Optional;

public class ReturnsHistoryPage extends SAPccPageObject {

    /**
     * return a link to return details page for the return.
     * @param returnOrderNumber
     */
    public void goToRetunDetails(String returnOrderNumber) {
        WebElementFacade returnOrder = getReturnOrder(returnOrderNumber, false);
        returnOrder.click();
    }

    /**
     * Check if order is in cancelling status.
     * @param returnOrderNumber
     * @return
     */
    public boolean isReturnCancelled(String returnOrderNumber) {
        WebElementFacade returnOrder = getReturnOrder(returnOrderNumber, true);
        return StringUtils.equalsIgnoreCase(Elements.ReturnHistory.CANCELLING, returnOrder.getText());
    }

    /**
     * Find the order in the list and return a link to return details or the status field
     * @param returnOrderNumber
     * @param statusField
     * @return {@Link WebElementFacade}
     */
    private WebElementFacade getReturnOrder(String returnOrderNumber, boolean statusField) {
        WebElementFacade orderHistory = find(Elements.ReturnHistory.ORDER_HISTORY);
        List<WebElementFacade> returnOrders = orderHistory.thenFindAll(Elements.ReturnHistory.ORDER_ITEM);
        Optional<WebElementFacade> returnOrder = returnOrders.stream()
                .filter(element -> returnOrderNumber.equals(element.find(Elements.ReturnHistory.RETURN_DETAILS_LINK).getText()))
                .findFirst();
        Assert.assertTrue("Return Order is missing ", returnOrder.isPresent());
        return  returnOrder.get().find( statusField ? Elements.ReturnHistory.STATUS : Elements.ReturnHistory.RETURN_DETAILS_LINK);
    }

}
