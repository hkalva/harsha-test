package pages.myaccount;

import net.serenitybdd.core.pages.WebElementFacade;
import pages.SAPccPageObject;
import util.Elements;

import java.util.List;
import java.util.stream.Collectors;

public class OrderDetailsPage extends SAPccPageObject {
    public List<String> getProducts() {
        List<WebElementFacade> webElementFacades = findAll(Elements.Cart.CART_ITEM_LIST);
        return webElementFacades.stream()
                .map(element -> element.then(Elements.Cart.ITEM_CODE).getTextValue())
                .collect(Collectors.toList());
    }
}
