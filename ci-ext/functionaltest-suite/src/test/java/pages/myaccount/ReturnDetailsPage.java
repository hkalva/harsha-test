package pages.myaccount;

import pages.SAPccPageObject;
import util.Elements;

public class ReturnDetailsPage extends SAPccPageObject {

    public boolean onPage() {
        return bodyHasClass("page-return-request-details");
    }

    public void cancelReturnRequest() {
        find(Elements.ReturnDetail.CANCEL).click();
    }

    public void submitReturnRequest() {
        find(Elements.ReturnDetail.SUBMIT).click();
    }
}
