package pages.myaccount;

import features.components.Address;
import net.serenitybdd.core.pages.WebElementFacade;
import pages.SAPccPageObject;
import util.Elements;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class AddressBookPage extends SAPccPageObject {
    Address address;

    public boolean hasAddress() {
        WebElementFacade webElementFacade = find(Elements.Account.AddressBook.ADDRESS_LIST);
        return webElementFacade.isPresent();
    }

    public void clickOnAddAddress() {
        //account-section-header-add pull-right
        Optional<WebElementFacade> editLink = findAll(Elements.Account.AddressBook.ADD_ADDRESS).stream()
                .filter(element -> element.isVisible())
                .findFirst();
        editLink.get().click();
    }

    public void clickOnEditAddress() {
        Optional<WebElementFacade> editLink = findAll(Elements.Account.AddressBook.ACTION_LINKS).stream()
                .filter(element -> element.getAttribute("href").contains("/my-account/edit-address"))
                .findFirst();
        editLink.get().click();
    }

    public void clickOnDeleteAddress() {
        Optional<WebElementFacade> editLink = findAll(Elements.Account.AddressBook.ACTION_LINKS).stream()
                .filter(element -> "Delete Address".equals(element.getAttribute("data-popup-title")))
                .findFirst();
        editLink.get().click();
    }


    public void submitAddress(Map<String, String> addressMap) {
        address.populateAddress(addressMap);
        find(Elements.Account.EditAddress.SAVE).click();
    }

    public Map getNewAddress() {
        Map<String, String> addressMap = new HashMap<>();
        addressMap.put(Address.TITLE, "mr");
        addressMap.put(Address.FIRST_NAME, "First");
        addressMap.put(Address.LAST_NAME, "Last");
        addressMap.put(Address.LINE_ONE, "Line One");
        addressMap.put(Address.LINE_TWO, "Line Two");
        addressMap.put(Address.TOWN, "Chicago");
        addressMap.put(Address.POSTL_CODE, "12345");
        addressMap.put(Address.STATE, "US-IL");
        addressMap.put(Address.COUNTRY, "US");
        addressMap.put(Address.PHONE_NUMBER, "111-111-1111");
        return addressMap;
    }

    public Map getAddressForUpdate() {
        Map<String, String> addressMap = new HashMap<>();
        addressMap.put(Address.TITLE, "mr");
        addressMap.put(Address.FIRST_NAME, "Updated");
        addressMap.put(Address.LAST_NAME, "Last");
        addressMap.put(Address.LINE_ONE, "Updated Line One");
        addressMap.put(Address.LINE_TWO, "Updated Line Two");
        addressMap.put(Address.TOWN, "Dallas");
        addressMap.put(Address.POSTL_CODE, "54321");
        addressMap.put(Address.STATE, "US-TX");
        addressMap.put(Address.COUNTRY, "US");
        addressMap.put(Address.PHONE_NUMBER, "222-222-2222");
        return addressMap;
    }
}
