package pages.myaccount;

import components.Form;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import pages.SAPccPageObject;
import util.Constants;
import util.Elements;

public class UpdateEmailPage extends SAPccPageObject {

    public void updateEmail(String email, String password) {
        WebElementFacade form = find(Elements.UpdateEmail.FORM);
        Form profileForm = new Form(form);
        profileForm.typeAndTab(Elements.UpdateEmail.PROFILE_EMAIL, email);
        profileForm.typeAndTab(Elements.UpdateEmail.PROFILE_CHECK_EMAIL, email);
        profileForm.typeAndTab(Elements.UpdateEmail.PROFILE_PASSWORD, password);
        form.then(Elements.Account.Profile.UPDATE).click();
    }

    public boolean isCurrentPage() {
        return findAll(Elements.UpdateEmail.ACCOUNT_SECTION_HEADER).stream()
                .anyMatch(element -> element.isEnabled() && element.getTextValue().equals(Constants.Account.EMAIL_ADDRESS));
    }

    public boolean customerEmailIs(String email) {
        return findAll(By.name(Elements.UpdateEmail.PROFILE_EMAIL)).stream()
                .anyMatch(element -> element.isEnabled() && element.getTextValue().equals(email));
    }

    public boolean validationErrorIs(By by, String message) {
        return find(by).getTextValue().equals(message);
    }
}
