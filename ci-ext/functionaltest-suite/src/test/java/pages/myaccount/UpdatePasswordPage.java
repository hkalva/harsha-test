package pages.myaccount;

import components.Form;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import pages.SAPccPageObject;
import util.Elements;

public class UpdatePasswordPage extends SAPccPageObject {

    public void updatePassword(String currentPassword, String newPassword, String confirmNewPassword) {
        WebElementFacade form = find(Elements.Account.UpdatePassword.FORM);
        Form profileForm = new Form(form);
        profileForm.typeAndTab(Elements.Account.UpdatePassword.CURRENT_PASSWORD, currentPassword);
        profileForm.typeAndTab(Elements.Account.UpdatePassword.NEW_PASWORD, newPassword);
        profileForm.typeAndTab(Elements.Account.UpdatePassword.CHECK_NEW_PASSWORD, confirmNewPassword);
        form.then(Elements.Account.Profile.UPDATE).click();
    }

    public boolean validationError(By by, String message) {
        return find(by).getTextValue().equals(message);
    }


}
