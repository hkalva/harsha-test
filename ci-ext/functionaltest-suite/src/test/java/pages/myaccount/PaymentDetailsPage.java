package pages.myaccount;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import pages.SAPccPageObject;
import util.Elements;

public class PaymentDetailsPage extends SAPccPageObject {

    @FindBy(css = ".account-list")
    WebElementFacade accoutList;
    int noOfPaymentInfo;
    public void deletePaymentInfo() {
        noOfPaymentInfo = accoutList.thenFindAll(By.tagName("form")).size();
        find(Elements.Account.PaymentDetails.REMOVE_PAYMENT_DETAILS).click();
    }

    public boolean creditCardIsDeleted() {
        return !accoutList.isPresent() || noOfPaymentInfo > accoutList.thenFindAll(By.tagName("form")).size();
    }
}
