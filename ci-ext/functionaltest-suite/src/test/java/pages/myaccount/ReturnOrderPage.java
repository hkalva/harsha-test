package pages.myaccount;

import pages.SAPccPageObject;
import util.Elements;
import util.TestUtil;

public class ReturnOrderPage extends SAPccPageObject {

    public void returnCompleteOrder() {
        find(Elements.ReturnOrder.RETURN_COMPLETE_ORDER).click();
        TestUtil.pause(1);
    }

    public void confirmReturnOrder() {
        find(Elements.ReturnOrder.CONFIRM_RETURN_ORDER).click();
    }

    public void submitReturnOrder() {
        find(Elements.ReturnOrder.SUBMIT_RETURN).click();
    }
}
