package pages.myaccount;

import components.Form;
import net.serenitybdd.core.pages.WebElementFacade;
import pages.SAPccPageObject;
import util.Elements;

public class PersonalDetailsPage extends SAPccPageObject {

    public void updatePersonalDetails(String firstName, String lastName) {
        WebElementFacade form = find(Elements.Account.PROFILE_FORM);
        Form profileForm = new Form(form);
        profileForm.typeAndTab(Elements.Account.Profile.FIRST_NAME, firstName);
        profileForm.typeAndTab(Elements.Account.Profile.LAST_NAME, lastName);
        form.then(Elements.Account.Profile.UPDATE).click();
    }

}
