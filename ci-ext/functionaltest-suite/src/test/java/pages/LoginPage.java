package pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@DefaultUrl("/rlp/login")
public class LoginPage extends SAPccPageObject {

    @FindBy(id = "j_username")
    WebElementFacade usernameField;

    @FindBy(id = "j_password")
    WebElementFacade passwordField;

    @FindBy(css = "#loginForm button[type=submit]")
    WebElementFacade loginSubmitButton;

    public void submitLoginForm(String email, String password) {
        usernameField.typeAndTab(email);
        passwordField.typeAndTab(password);
        loginSubmitButton.click();
    }

    public boolean onPage() {
        WebDriverWait wait = new WebDriverWait(getDriver(), 30); // seconds
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("j_username")));
       return bodyHasClass("page-login");
    }

}
