package pages;

import components.Form;
import features.components.Address;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.WebElementFacade;
import util.Assertions;
import util.Elements;
import util.TestUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class CheckoutPage extends SAPccPageObject {

    static final String ORDER_CONFIRMATION = "THANK YOU FOR YOUR ORDER!";

    Address address;

    static public class DeliveryMethod {
        static public final String STANDARD_NET = "standard-net";
    }

    public void selectPaymentMethod() {
        find(Elements.Checkout.PaymentType.NEXT).click();
    }

    static public class Payment {
        static public final String CARD_TYPE = "001";
        static public final String NAME_ON_CARD = "First Last";
        static public final String ACCOUNT_NUMBER = "4111111111111111";
        static public final String EXPIRY_MONTH = "11";
        static public final String EXPIRY_YEAR = "2025";
        static public final String CVV = "345";
    }

    public void populateShippingAddress() {
        populateAddress(false);
        find(Elements.Checkout.ShippingAddress.NEXT).click();
    }

    public void populateAddress( boolean isBillingAddress) {
        Map<String, String> addressMap = new HashMap<String, String>();
        addressMap.put(Address.TITLE, "mr");
        addressMap.put(Address.FIRST_NAME, "First");
        addressMap.put(Address.LAST_NAME, "Last");
        addressMap.put(Address.LINE_ONE, "Line One");
        addressMap.put(Address.LINE_TWO, "Line Two");
        addressMap.put(Address.TOWN, "Chicago");
        addressMap.put(Address.POSTL_CODE, "12345");
        addressMap.put(Address.STATE, isBillingAddress ? "IL" :  "US-IL");
        addressMap.put(Address.COUNTRY, "US");
        addressMap.put(Address.PHONE_NUMBER, "111-111-1111");
        if(isBillingAddress)
            address.populateBillingAddress(addressMap);
        else
            address.populateAddress(addressMap);
    }


    public void selectDeliveryMethod() {
        WebElementFacade form = find(Elements.Checkout.DeliveryMethod.DELIVERY_METHOD_FORM);
        Form addressForm = new Form(form);
        addressForm.select(Elements.Checkout.DeliveryMethod.DELIVERY_METHOD, DeliveryMethod.STANDARD_NET);
        find(Elements.Checkout.DeliveryMethod.NEXT).click();
    }

    public void populateBillingInformation() {
        WebElementFacade billingInfoForm = find(Elements.Checkout.PaymentInfo.FORM);
        Form form = new Form(billingInfoForm);
        form.select(Elements.Checkout.PaymentInfo.CARD_TYPE, Payment.CARD_TYPE);
        form.typeAndTab(Elements.Checkout.PaymentInfo.NAME_ON_CARD, Payment.NAME_ON_CARD);
        form.typeAndTab(Elements.Checkout.PaymentInfo.CARD_NUMBER, Payment.ACCOUNT_NUMBER);
        form.select(Elements.Checkout.PaymentInfo.EXPIRY_MONTH, Payment.EXPIRY_MONTH);
        form.select(Elements.Checkout.PaymentInfo.EXPIRY_YEAR, Payment.EXPIRY_YEAR);
        form.typeAndTab(Elements.Checkout.PaymentInfo.CVV_NUMBER, Payment.CVV);
        form.check(Elements.Checkout.PaymentInfo.USE_DELIVERY_ADDRESS, "false");
        populateAddress(true);
        find(Elements.Checkout.PaymentInfo.NEXT).click();
    }

    public void submitOrder() {
        List<WebElementFacade> submitOrderForms = findAll(Elements.Checkout.FinalReview.FORM);
        Optional<WebElementFacade> submitOrderForm = submitOrderForms.stream()
                .filter(webElementFacade -> webElementFacade.getRect().getHeight() > 0)
                .findFirst();
        Form form = new Form(submitOrderForm.get());
        scrollToElement((WebElementFacade)find(Elements.Checkout.FinalReview.STEP_BODY).then(Elements.Checkout.FinalReview.PLACE_ORDER_DIV));
        form.check(Elements.Checkout.FinalReview.TERMS_CHECK, "true");
        TestUtil.pause(2);
        submitOrderForm.get().then(Elements.Checkout.FinalReview.PLACE_ORDER).click();
    }

    public boolean customerIsOnPaymentInfoStep() {
        return findAll(Elements.Checkout.PaymentInfo.FORM).stream()
                .filter(element -> element.getRect().height > 0)
                .findFirst().isPresent();
    }

    public boolean confirmOrder() {
        WebElementFacade confirmationMessage = find(Elements.OrderConfirmation.CHECKOUT_SUCCESS_HEADLINE);
        return ORDER_CONFIRMATION.equalsIgnoreCase(confirmationMessage.getTextValue());
    }

    public void confirmOrderBusinessProcess() {
        Assertions.assertOrderProcessStarted(getOrderNumber());
    }

    public String getOrderNumber() {
        WebElementFacade orderNumberElement = find(Elements.OrderConfirmation.CHECKOUT_SUCCESS).then(By.tagName("b"));
        return orderNumberElement.getTextValue();
    }
}
