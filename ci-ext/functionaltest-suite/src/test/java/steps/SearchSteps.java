package steps;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import pages.CurrentPage;
import pages.HomePage;
import pages.SearchResultsPage;
import responsive.ResponsiveUtils;
import util.Elements;

import java.util.List;

public class SearchSteps extends ScenarioSteps {

    CurrentPage currentPage;
    HomePage homePage;
    SearchResultsPage searchResultsPage;

    public static final String APPLIED_FACETS = "Applied Facets";
    public static final String SOR_BY_NAME_DESCENDING = "Name (descending)";

    @Step
    public void toggleMobileSearch() {
        homePage.getMobileSearchToggle().click();
    }

    @Step
    public void typeSearchTerm(String term) {
        currentPage.find(Elements.Page.SEARCH).waitUntilClickable().type(term);
    }

    @Step
    public void typeAndEnterSearchTerm(String term) {
        homePage.getSearchField().typeAndEnter(term);
    }

    @Step
    public void clickSearchButton() {
        homePage.getSearchButton().click();
    }

    public void searchForTerm(String term) {
        if (ResponsiveUtils.isMobile()) {
            toggleMobileSearch();
            typeSearchTerm(term);
            clickSearchButton();
        } else {
            typeAndEnterSearchTerm(term);
        }
    }

    @Step
    public void addItemToCart() {
        searchResultsPage.addItemToCart();
    }

    @Step
    public void addFacetFilters(String facetName, String facetValue) {
        searchResultsPage.scrollToTop();
        if (ResponsiveUtils.isMobile()) {
            searchResultsPage.getRefineButton().click();
            searchResultsPage.getFacetDivElement(facetName).click();
        }
        WebElementFacade facetLink = searchResultsPage.getFacetLinkElement(facetName, facetValue);
        facetLink.click();
    }

    @Step
    public void removeFacetFilter(String facetValue) {
        searchResultsPage.scrollToTop();
        if (ResponsiveUtils.isMobile()) {
            searchResultsPage.getRefineButton().click();
            searchResultsPage.getFacetDivElement(APPLIED_FACETS).click();
        }
        WebElementFacade removeFacetLinkElement = searchResultsPage.getRemoveFacetLinkElement(APPLIED_FACETS, facetValue);
        removeFacetLinkElement.click();
    }

    @Step
    public void sortResultsBy(String sortBy) {
        Select sortByDropDown = new Select(currentPage.find(Elements.Search.SORT_OPTIONS));
        sortByDropDown.selectByVisibleText(sortBy);
        currentPage.waitForLoad();
    }


    public String getNoOfValuesForFacet(String facetName, String facetValue) {
        WebElementFacade facetLink = searchResultsPage.getFacetLinkElement(facetName, facetValue);
        WebElementFacade facetCountWebElement = facetLink.then(By.xpath("..")).then(Elements.Search.FACET_COUNT);
        String facetCount = StringUtils.replace(facetCountWebElement.getTextContent(), "(", "");
        return StringUtils.replace(facetCount, ")", "");
    }

    public String getNoOfProductsFound() {
        WebElementFacade paginationElement = currentPage.find(Elements.Search.PAGINATION_RESULT);
        return paginationElement.getTextContent().trim();
    }

    public List<String> getProductNames() {
        WebElementFacade productGrid = currentPage.find(Elements.Search.PRODUCT_LISTING_GRID);
        List<String> productNames = Lists.newArrayList();
        List<WebElementFacade> webElementFacades = productGrid.thenFindAll("div");
        for (WebElementFacade webElementFacade : webElementFacades) {
            String name = webElementFacade.findElement(By.cssSelector("a[class='name']")).getText().trim();
            productNames.add(name);
        }
        return productNames;
    }
}
