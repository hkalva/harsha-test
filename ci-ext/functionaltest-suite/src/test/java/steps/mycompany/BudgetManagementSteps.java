package steps.mycompany;

import components.Pagination;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import pages.mycompany.budget.AddBudgetPage;
import pages.mycompany.budget.EditBudgetPage;
import pages.mycompany.budget.ManageBudgetsPage;
import pages.mycompany.budget.ViewBudgetPage;

import java.util.Optional;

public class BudgetManagementSteps {
    
    Pagination pagination;
    ViewBudgetPage viewBudgetPage;
    ManageBudgetsPage manageBudgetsPage;
    AddBudgetPage addBudgetPage;
    EditBudgetPage editBudgetPage;

    @Step
    public void clickAddNewBudgetButton() {
        manageBudgetsPage.getAddNewButton().click();
    }

    @Step
    public void populateBudgetForm(final String code, final String name, final String parentUnit, final String budget, final String currency, final String startDate, final String endDate) {
        addBudgetPage.getForm()
                .typeAndTab("code", code)
                .typeAndTab("name", name)
                .select("parentB2BUnit", parentUnit)
                .typeAndTab("budget", budget)
                .select("currency", currency)
                .typeAndTab("startDate", startDate)
                .typeAndTab("endDate", endDate);
    }

    @Step
    public void submitBudgetForm() {
        addBudgetPage.getFormSaveButton().click();
    }

    public void navigateToViewBudgetPage(final String code) {
        final Optional<WebElement> link = manageBudgetsPage.getResponsiveTable().findElements(By.cssSelector(".responsive-table-cell a")).stream()
                .filter(e -> e.getAttribute("href").endsWith(code.replaceAll("\\s","%20"))).findFirst();

        if(link.isPresent()) {
            clickViewBudgetPageLink(link.get());
        } else if(pagination.hasNext()) {
            clickNextButton();
            navigateToViewBudgetPage(code);
        } else {
            throw new WebDriverException("Could not find link for budget " + code);
        }
    }

    @Step
    public void clickViewBudgetPageLink(final WebElement link) {
        link.click();
    }

    @Step
    public void clickNextButton() {
        pagination.getTopBarNextButton().click();
    }

    @Step
    public void clickOnDisableBudgetButton() {
        viewBudgetPage.getDisableButton().click();
    }

    @Step
    public void clickOnEnableBudgetButton() {
        viewBudgetPage.getEnableButton().click();
    }

    @Step
    public void clickOnConfirmActionButton() {
        viewBudgetPage.getConfirmActionButton().waitUntilClickable().click();
    }

    @Step
    public void clickOnEditBudgetButton() {
        viewBudgetPage.getEditButton().click();
    }
}
