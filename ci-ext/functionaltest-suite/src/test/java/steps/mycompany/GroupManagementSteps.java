package steps.mycompany;

import components.Pagination;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import pages.mycompany.group.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GroupManagementSteps {
    
    AddGroupPage addGroupPage;
    EditGroupPage editGroupPage;
    Pagination pagination;
    ManageGroupsPage manageGroupsPage;
    UserGroupDetailPage userGroupDetailPage;
    SelectUserPage selectUserPage;
    SelectPermissionsPage selectPermissionsPage;

    @Step
    public void clickAddNewGroupButton() {
        manageGroupsPage.getAddNewButton().click();
    }

    @Step
    public void populateGroupForm(final String uid, final String name, final String parentUnit) {
        addGroupPage.getForm()
                .typeAndTab("uid", uid)
                .typeAndTab("name", name)
                .select("parentUnit", parentUnit);
    }

    @Step
    public void submitGroupForm() {
        addGroupPage.getFormSaveButton().click();
    }

    public void navigateToViewGroupPage(final String code) {
        final Optional<WebElement> link = manageGroupsPage.getResponsiveTable().findElements(By.cssSelector(".responsive-table-cell a")).stream()
                .filter(e -> e.getAttribute("href").endsWith(code.replaceAll("\\s","%20"))).findFirst();

        if(link.isPresent()) {
            clickViewGroupPageLink(link.get());
        } else if(pagination.hasNext()) {
            clickNextButton();
            navigateToViewGroupPage(code);
        } else {
            throw new WebDriverException("Could not find link for user group " + code);
        }
    }

    @Step
    public void clickViewGroupPageLink(final WebElement link) {
        link.click();
    }

    @Step
    public void clickNextButton() {
        pagination.getTopBarNextButton().click();
    }

    @Step
    public void clickOnDisableGroupButton() {
        userGroupDetailPage.getDisableButton().click();
    }

    @Step
    public void clickOnConfirmActionButton() {
        userGroupDetailPage.getConfirmActionButton().waitUntilClickable().click();
    }

    @Step
    public void clickOnEditGroupButton() {
        userGroupDetailPage.getEditButton().click();
    }

    @Step
    public void clickOnAddExistingUserLink() {
        userGroupDetailPage.getAddUserButton().click();
    }

    @Step
    public void clickOnAddExistingPermissionLink() {
        userGroupDetailPage.getAddPermissionButton().click();
    }

    /**
     * Pages through users, clicking on them as they are found. Assumes all users will be found.
     *
     * @param users
     */
    public void toggleUsers(final List<String> users) {
        final List<String> remaining = new ArrayList<>();
        users.forEach(code -> {
            final WebElementFacade link = selectUserPage.getToggleLink(code);
            if(link.isPresent()) {
                toggleUser(link);
            } else {
                remaining.add(code);
            }
        });

        if(remaining.size() > 0) {
            if(pagination.hasNext()) {
                clickNextButton();
                toggleUsers(remaining);
            } else {
                throw new WebDriverException(String.format("Could not find users %s to toggle.",remaining));
            }
        }
    }

    @Step
    public void toggleUser(final WebElement link) {
        link.click();
    }


    /**
     * Pages through permissions, clicking on them as they are found. Assumes all permissions will be found.
     *
     * @param permissions
     */
    public void togglePermissions(final List<String> permissions) {
        final List<String> remaining = new ArrayList<>();
        permissions.forEach(code -> {
            final WebElementFacade link = selectPermissionsPage.getToggleLink(code);
            if(link.isPresent()) {
                togglePermission(link);
            } else {
                remaining.add(code);
            }
        });

        if(remaining.size() > 0) {
            if(pagination.hasNext()) {
                clickNextButton();
                togglePermissions(remaining);
            } else {
                throw new WebDriverException(String.format("Could not find permissions %s to toggle.",remaining));
            }
        }
    }

    @Step
    public void togglePermission(final WebElement link) {
        link.click();
    }

    @Step
    public void clickOnDoneButton() {
        selectUserPage.getDoneButton().click();
    }

    @Step
    public void clicksOnGroupDeleteButton() {
        userGroupDetailPage.getDeleteButton().click();
    }
}
