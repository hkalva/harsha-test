package steps.mycompany;

import components.Pagination;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import pages.mycompany.costcenter.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CostCenterManagmentSteps {

    EditCostCenterPage editCostCenterPage;
    AddCostCenterPage addCostCenterPage;
    ViewCostCenterPage viewCostCenterPage;
    ManageCostCentersPage manageCostCentersPage;
    SelectBudgetPage selectBudgetPage;
    Pagination pagination;


    @Step
    public void clickAddNewCostCenterButton() {
        manageCostCentersPage.getAddNewButton().click();
    }

    @Step
    public void populateCostCenterForm(final String code, final String name, final String parentUnit, final String currency) {
        addCostCenterPage.getCostCenterForm()
                .select("parentB2BUnit", parentUnit)
                .typeAndTab("code", code)
                .typeAndTab("name", name)
                .select("currency", currency);
    }

    @Step
    public void submitCostCenterForm() {
        addCostCenterPage.getCostCenterFormSaveButton().click();
    }

    public void navigateToViewCostCenterPage(final String code) {
        final Optional<WebElement> link = manageCostCentersPage.getResponsiveTable().findElements(By.cssSelector(".responsive-table-cell a")).stream()
                .filter(e -> e.getAttribute("href").endsWith(code.replaceAll("\\s","%20"))).findFirst();

        if(link.isPresent()) {
            clickViewCostCenterPageLink(link.get());
        } else if(pagination.hasNext()) {
            clickNextButton();
            navigateToViewCostCenterPage(code);
        } else {
            throw new WebDriverException("Could not find link for cost center " + code);
        }
    }

    @Step
    public void clickViewCostCenterPageLink(final WebElement link) {
        link.click();
    }

    @Step
    public void clickNextButton() {
        pagination.getTopBarNextButton().click();
    }

    @Step
    public void clickOnDisableCostCenterButton() {
        viewCostCenterPage.getDisableButton().click();
    }

    @Step
    public void clickOnEnableCostCenterButton() {
        viewCostCenterPage.getEnableButton().click();
    }

    @Step
    public void clickOnConfirmActionButton() {
        viewCostCenterPage.getConfirmActionButton().waitUntilClickable().click();
    }

    @Step
    public void clickOnAddExistingBudgetButton() {
        viewCostCenterPage.getAddBudgetLink().click();
    }

    @Step
    public void clickOnDoneButton() {
        selectBudgetPage.getDoneButton().click();
    }

    @Step
    public void clickOnEditCostCenterButton() {
        viewCostCenterPage.getEditButton().click();
    }

    /**
     * Pages through budgets, clicking on them as they are found. Assumes all budgets will be found.
     *
     * @param budgets
     */
    public void toggleBudgets(final List<String> budgets) {
        final List<String> remaining = new ArrayList<>();
        budgets.forEach(code -> {
            final WebElementFacade link = selectBudgetPage.getToggleLink(code);
            if(link.isPresent()) {
                toggleBudget(link);
            } else {
                remaining.add(code);
            }
        });

        if(remaining.size() > 0) {
            clickNextButton();
            toggleBudgets(remaining);
        }
    }

    @Step
    public void toggleBudget(final WebElement link) {
        link.click();
    }
}
