package steps.mycompany;

import components.Pagination;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import pages.mycompany.AddNewPermissionPage;
import pages.mycompany.EditPermissionPage;
import pages.mycompany.ManagePermissionsPage;
import pages.mycompany.ViewPermissionPage;

import java.util.Optional;

public class PermissionManagementSteps {

    ManagePermissionsPage managePermissionsPage;
    AddNewPermissionPage addNewPermissionPage;
    ViewPermissionPage viewPermissionPage;
    EditPermissionPage editPermissionPage;
    Pagination pagination;

    @Step
    public void clickAddNewPermissionButton() {
        managePermissionsPage.getAddNewButton().click();
    }

    @Step
    public void selectPermissionType(final String type) {
        addNewPermissionPage.getPermissionTypeMenu().selectByValue(type);
    }

    @Step
    public void populateNewTimespanPermissionForm(final String value, final String parentUnit, final String timespan,
                                                  final String code, final String currency) {
        addNewPermissionPage.getPermissionForm()
                .select("parentUnitName", parentUnit)
                .select("currency", currency)
                .typeAndTab("value", value)
                .select("timeSpan", timespan)
                .typeAndTab("code", code);
    }

    @Step
    public void submitNewPermissionForm() {
        addNewPermissionPage.getPermissionFormSaveButton().click();
    }

    @Step
    public void submitEditPermissionForm() {
        editPermissionPage.getPermissionFormSaveButton().click();
    }

    public void navigateToViewPermissionPage(final String code) {
        final Optional<WebElement> link = managePermissionsPage.getResponsiveTable().findElements(By.cssSelector(".responsive-table-cell a")).stream()
                .filter(e -> e.getAttribute("href").endsWith(code.replaceAll("\\s","%20"))).findFirst();

        if(link.isPresent()) {
            clickViewPermissionPageLink(link.get());
        } else if(pagination.hasNext()) {
            clickNextButton();
            navigateToViewPermissionPage(code);
        } else {
            throw new WebDriverException("Could not find link for permission " + code);
        }
    }

    @Step
    public void clickOnEditPermissionButton() {
        viewPermissionPage.getEditPermissionButton().click();
    }

    @Step
    public void selectsUnit(final String unit) {
        editPermissionPage.getPermissionForm().select("parentUnitName", unit);
    }

    @Step
    public void clickViewPermissionPageLink(final WebElement link) {
        link.click();
    }

    @Step
    public void clickNextButton() {
        pagination.getTopBarNextButton().click();
    }

    @Step
    public void clickOnDisablePermissionButton() {
        viewPermissionPage.getDisableButton().click();
    }

    @Step
    public void clickOnEnablePermissionButton() {
        viewPermissionPage.getEnableButton().click();
    }

    @Step
    public void clickOnConfirmActionButton() {
        viewPermissionPage.getConfirmActionButton().waitUntilClickable().click();
    }

}
