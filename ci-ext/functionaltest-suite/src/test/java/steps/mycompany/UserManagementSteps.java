package steps.mycompany;

import components.Pagination;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import pages.mycompany.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Steps associated with user management activities.
 */
public class UserManagementSteps {

    ManageUsersPage manageUsersPage;
    ManageUserPage manageUserPage;
    AddNewUserPage addNewUserPage;
    SelectGroupPage selectGroupPage;
    SelectPermissionPage selectPermissionPage;
    SelectApproverPage selectApproverPage;
    Pagination pagination;

    @Step
    public void clickAddNewUserButton() {
        manageUsersPage.getAddNewButton().click();
    }

    @Step
    public void populateNewUserForm(final String title, final String firstName, final String lastName,
                                    final String email, final String unit, final String roles) {
        addNewUserPage.getForm()
                .select("titleCode", title)
                .typeAndTab("firstName", firstName)
                .typeAndTab("lastName", lastName)
                .typeAndTab("email", email)
                .select("parentB2BUnit", unit)
                .check("roles", roles.split(","));
    }

    @Step
    public void submitNewUserForm() {
        addNewUserPage.getForm().submit();
    }

    public void navigateToUserDetailPage(final String uid) {
        final Optional<WebElement> link = manageUsersPage.getResponsiveTable().findElements(By.cssSelector("a.responsive-table-link")).stream()
                .filter(e -> e.getAttribute("href").endsWith(uid)).findFirst();

        if(link.isPresent()) {
            clickUserDetailPageLink(link.get());
        } else if(pagination.hasNext()) {
            clickNextButton();
            navigateToUserDetailPage(uid);
        } else {
            throw new WebDriverException("Could not find link for user" + uid);
        }
    }

    @Step
    public void clickUserDetailPageLink(final WebElement link) {
        link.click();
    }

    @Step
    public void clickNextButton() {
        pagination.getTopBarNextButton().click();
    }

    @Step
    public void clickDisableUserButton() {
        manageUserPage.getDisableLink().click();
    }

    @Step
    public void clickOnDisableUserConfirmButton() {
        manageUserPage.getConfirmDisableButton().waitUntilClickable().click();
    }

    @Step
    public void clickOnEnableUserButton() {
        manageUserPage.getEnableLink().click();
    }

    @Step
    public void clickOnUserEditButton() {
        manageUserPage.getEditUserButton().click();
    }

    @Step
    public void clickOnAddExistingGroupButton() {
        manageUserPage.getAddGroupButton().click();
    }

    @Step
    public void clickOnAddExistingApproverButton() {
        manageUserPage.getAddApproverButton().click();
    }

    @Step
    public void clickOnAddExistingPermissionButton() {
        manageUserPage.getAddPermissionButton().click();
    }

    @Step
    public void clickOnGroup(final String uid) {
        selectGroupPage.getGroupToggleLink(uid).click();
    }

    @Step
    public void clickOnDoneButton() {
        selectGroupPage.getDoneButton().click();
    }

    @Step
    public void clickOnGroupCardRemoveButton(final String uid) {
        manageUserPage.getRemoveGroupCardLink(uid).click();
    }

    @Step
    public void clickOnApproverCardRemoveButton(final String uid) {
        manageUserPage.getRemoveApproverCardLink(uid).click();
    }

    @Step
    public void clickOnPermissionCardRemoveButton(final String permission) {
        manageUserPage.getRemovePermissionCardLink(permission).click();
    }

    @Step
    public void clickOnConfirmActionButton() {
        manageUserPage.getConfirmActionButton().waitUntilClickable().click();
    }

    /**
     * Pages through approvers, clicking on them as they are found. Assumes all approvers will be found.
     *
     * @param approvers
     */
    public void toggleApprovers(final List<String> approvers) {
        final List<String> remaining = new ArrayList<>();
        approvers.forEach(uid -> {
            final WebElementFacade link = selectApproverPage.getApproverToggleLink(uid);
            if(link.isPresent()) {
                toggleApprover(link);
            } else {
                remaining.add(uid);
            }
        });

        if(remaining.size() > 0) {
            clickNextButton();
            toggleApprovers(remaining);
        }
    }

    /**
     * Pages through permissions, clicking on permissions as they are found. Assumes all permissions will be found.
     *
     * @param permissions
     */
    public void togglePermissions(final List<String> permissions) {
        final List<String> remaining = new ArrayList<>();
        permissions.forEach(perm -> {
            final WebElementFacade link = selectPermissionPage.getPermissionToggleLink(perm);
            if(link.isPresent()) {
                togglePermission(link);
            } else {
                remaining.add(perm);
            }
        });

        if(remaining.size() > 0) {
            clickNextButton();
            togglePermissions(remaining);
        }
    }

    @Step
    public void togglePermission(final WebElementFacade link) {
        link.click();
    }

    @Step
    public void toggleApprover(final WebElementFacade link) {
        link.click();
    }

}
