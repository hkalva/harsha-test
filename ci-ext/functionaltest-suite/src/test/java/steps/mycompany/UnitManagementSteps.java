package steps.mycompany;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import pages.mycompany.unit.*;

import java.util.Optional;

public class UnitManagementSteps {

    UnitsPage unitsPage;
    AddUnitPage addUnitPage;
    AddAddressPage addAddressPage;
    AddCostCenterPage addCostCenterPage;
    UnitDetailPage unitDetailPage;

    @Step
    public void navigateToUnitDetailPage(final String unit) {
        Optional<WebElementFacade> link = unitsPage.findAll(By.className("accordion-lnk")).stream().filter(e ->
                e.getAttribute("href").endsWith(unit.replaceAll("\\s+","%20"))).findFirst();
        if(!link.isPresent()) {
            throw new WebDriverException(String.format("Could not find link for B2BUnit '%s'", unit));
        }

        if(!link.get().isClickable()) {
            throw new WebDriverException(String.format("Link for B2BUnit '%s' is not clickable. Adjust webdriver logic.", unit));
        }

        link.get().click();
    }

    @Step
    public void clickEditButton() {
        unitDetailPage.getEditButton().click();
    }

    @Step
    public void populateUnitForm(final String unit, final String name, final String parentUnit, final String approvalProcess) {
        addUnitPage.getForm().typeAndTab("uid", unit)
                .typeAndTab("name", name)
                .select("parentUnit", parentUnit)
                .select("approvalProcessCode", approvalProcess);
    }

    @Step
    public void populateNewAddressForm(final String countryIso, final String titleCode, final String firstName,
                                       final String lastName, final String line1, final String line2,
                                       final String townCity, final String postcode) {
        addAddressPage.getForm()
                .select("countryIso", countryIso)
                .select("titleCode", titleCode)
                .typeAndTab("firstName", firstName)
                .typeAndTab("lastName", lastName)
                .typeAndTab("line1", line1)
                .typeAndTab("line2", line2)
                .typeAndTab("townCity", townCity)
                .typeAndTab("postcode", postcode);
    }

    @Step
    public void submitNewAddressForm() {
        addAddressPage.getFormSaveButton().click();
    }

    @Step
    public void populateNewCostCenterForm(final String code, final String name, final String parentB2BUnit, final String currency) {
        addCostCenterPage.getForm()
                .typeAndTab("code", code)
                .typeAndTab("name", name)
                .select("parentB2BUnit", parentB2BUnit)
                .select("currency", currency);
    }

    @Step
    public void submitNewCostCenterForm() {
        addCostCenterPage.getFormSaveButton().click();
    }

    @Step
    public void submitUnitForm() {
        addUnitPage.getFormSaveButton().click();
    }

    @Step
    public void clickAddNewUnitButton() {
        unitsPage.getAddNewButton().click();
    }

    @Step
    public void clickAddNewAddressButton() {
        unitDetailPage.getAddNewAddressButton().click();
    }

    @Step
    public void clickAddNewCostCenterButton() {
        unitDetailPage.getAddNewCostCenterButton().click();
    }
}
