package steps;

import features.components.AddToCartPopup;
import features.components.Header;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.CurrentPage;
import responsive.ResponsiveUtils;
import util.Elements;
import util.TestUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static util.TestUtil.pause;

/**
 * Steps for desktop and mobile menu navigation.
 */
public class NavigationSteps {

    Header header;

    CurrentPage currentPage;

    AddToCartPopup addToCartPopup;

    public void navigateToLinkTitle(String title) {
        if (ResponsiveUtils.isMobile()) {
            toggleMobileMenu();
            toggleMyCompanyNav();
            pause(2);
            clickLink(title);
        } else {
            toggleMyCompanyNav();
            clickLink(title);
        }

    }

    @Step
    public void toggleMobileMenu() {
        header.getMobileMenuToggle().click();
    }

    @Step
    public void toggleMyCompanyNav() {
        if (ResponsiveUtils.isMobile()) {
            header.getMobileMyCompanyToggle().waitUntilClickable().click();
        } else {
            header.getToggleMyCompanyNav().waitUntilClickable().click();
        }
    }

    @Step
    public void clickLink(String title) {
        TestUtil.pause(1);
        final Optional<WebElementFacade> link = header.findAll(By.cssSelector("a[title='" + title + "']")).stream()
                .filter(e -> e.isVisible() && e.getRect().getHeight() > 0).findFirst();
        Assert.assertTrue(link.isPresent());
        if (link.isPresent()) {
            WebElementFacade webElementFacade = link.get();
            if (ResponsiveUtils.isMobile()) {
                currentPage.scrollToElement(webElementFacade);
            }
            webElementFacade.waitUntilClickable().click();
        }
    }

    @Step
    public void navigateToCategoryPage(String category) {
        if (ResponsiveUtils.isMobile()) {
            toggleMobileMenu();
        }
        List<WebElementFacade> webElementFacades = currentPage.getNavigationBar().thenFindAll(By.cssSelector("a"));
        for (WebElementFacade webElementFacade : webElementFacades) {
            if (webElementFacade.isCurrentlyVisible() && category.equalsIgnoreCase(webElementFacade.getText())) {
                webElementFacade.click();
            }
        }
    }

    @Step
    public void navigateToProductDetailsPage() {
        List<WebElementFacade> products = currentPage.findAll(Elements.Search.PRODUCT_ITEM);
        Optional<WebElementFacade> productItem = products.stream().findFirst();
        if (productItem.isPresent()) {
            productItem.get().then(Elements.Search.PRODUCT_NAME).click();
        } else {
            Assert.assertTrue(productItem.isPresent());
        }
    }

    @Step
    public void clickOnMyAccount() {
        if (ResponsiveUtils.isMobile()) {
            toggleMobileMenu();
        }
        WebElementFacade myAccount = currentPage.getMyAccount();
        myAccount.waitUntilClickable();
        myAccount.click();
    }

    @Step
    public void navigateToCartPage() throws URISyntaxException {
        WebDriver driver = currentPage.getDriver();
        driver.get(new URI(driver.getCurrentUrl()).resolve("/cart").toString());
    }

    @Step
    public void navigateToWishlists() throws  URISyntaxException {
        ///my-account/manage-wishlists
        WebDriver driver = currentPage.getDriver();
        driver.get(new URI(driver.getCurrentUrl()).resolve("/my-account/manage-wishlists").toString());
    }


}
