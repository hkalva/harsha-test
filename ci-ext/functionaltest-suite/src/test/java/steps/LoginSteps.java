package steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import pages.HomePage;
import pages.LoginPage;

public class LoginSteps extends ScenarioSteps {

    LoginPage loginPage;
    HomePage homePage;

    @Step
    public void openHomePage() {
        homePage.open();
    }

    @Step
    public void submitLoginForm(String email, String password) {
        loginPage.submitLoginForm(email, password);
    }
}
