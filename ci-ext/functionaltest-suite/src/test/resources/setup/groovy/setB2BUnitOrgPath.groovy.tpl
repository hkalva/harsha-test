import de.hybris.platform.commerceservices.constants.CommerceServicesConstants

String uid = '${uid}'

final boolean isPathGenerationEnabled = configurationService.getConfiguration()
		.getBoolean(CommerceServicesConstants.ORG_UNIT_PATH_GENERATION_ENABLED, true);

if(isPathGenerationEnabled) {
logger.info('Setting org hierarchy path for B2BUnit ' + uid)
	orgUnitHierarchyService.saveChangesAndUpdateUnitPath(b2bUnitService.getUnitForUid(uid))
}