import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;

def query = """
select {pk} from {Cart} where {user} in (
  {{ select {pk} from {Customer} where {uid} = ?customer }}
) and {savetime} is null
"""

SearchResult<CartModel> result = flexibleSearchService.search(query, ['customer' : '${customerUid}'])
result.getResult().each {
    if(!result.getResult().isEmpty()) {
      modelService.remove it
    }
}

