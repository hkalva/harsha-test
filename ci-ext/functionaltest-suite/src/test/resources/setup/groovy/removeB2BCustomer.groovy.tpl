import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.b2b.model.B2BCustomerModel;

def query = 'select {pk} from {B2BCustomer} where {uid} = ?uid'

SearchResult<B2BCustomerModel> result = flexibleSearchService.search(query, ['uid' : '${customerUid}'])
result.getResult().each { modelService.remove it }