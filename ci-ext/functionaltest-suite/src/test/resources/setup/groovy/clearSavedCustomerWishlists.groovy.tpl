import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

def query = """
select {pk} from {Wishlist2} where {user} in (
  {{ select {pk} from {Customer} where {uid} = ?customer }}
)
"""

SearchResult<Wishlist2Model> result = flexibleSearchService.search(query, ['customer' : '${customerUid}'])
if(!result.getResult().isEmpty()) {
    result.getResult().each { modelService.remove it }
}
