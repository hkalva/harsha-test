import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.core.model.order.CartModel;

def query = """
select {pk} from {Cart} where {user} in (
  {{ select {pk} from {Customer} where {uid} = ?customer }}
) and {savetime} is not null
"""

SearchResult<CartModel> result = flexibleSearchService.search(query, ['customer' : '${customerUid}'])
if(!result.getResult().isEmpty()) {
    result.getResult().each { modelService.remove it }
}
