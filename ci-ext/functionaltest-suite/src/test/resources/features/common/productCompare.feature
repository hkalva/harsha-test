@productcompare
Feature: ProductCompare

  Scenario Outline:  Customer can compare products
    Given Customer is authenticated using email '<email>' and password '<password>'
    When  he submits search term '<term>' field
    And selects product to compare
    Then selected products are displayed in the product compare tray
    When customer clicks on compare
    Then product compare modal is displayed
    And  selected products are included in the product compare overlay
    Examples:
      | email                        | password | term  |
      | william.hunter@rustic-hw.com | 12341234 | drill |

  Scenario Outline:  Customer can clear all products selected for compare
    Given Customer is authenticated using email '<email>' and password '<password>'
    When he submits search term '<term>' field
    And selects product to compare
    Then selected products are displayed in the product compare tray
    When customer click on clear all
    Then All product are removed from product compare tray
    Examples:
      | email                        | password | term  |
      | william.hunter@rustic-hw.com | 12341234 | drill |

  Scenario Outline:  Customer removes products selected for compare by deselecting the checkbox
    Given Customer is authenticated using email '<email>' and password '<password>'
    When  he submits search term '<term>' field
    And selects product to compare
    Then selected products are displayed in the product compare tray
    When customer clears compare selection checkbox
    Then selected products are displayed in the product compare tray
    Examples:
      | email                        | password | term  |
      | william.hunter@rustic-hw.com | 12341234 | drill |

  Scenario Outline:  Customer removes products from product compare tray
    Given Customer is authenticated using email '<email>' and password '<password>'
    When  he submits search term '<term>' field
    And selects product to compare
    Then selected products are displayed in the product compare tray
    When customer removes products form compare tray
    Then selected products are displayed in the product compare tray
    Examples:
      | email                        | password | term  |
      | william.hunter@rustic-hw.com | 12341234 | drill |

  Scenario Outline:  Customer remove products from compare overlay
    Given Customer is authenticated using email '<email>' and password '<password>'
    When  he submits search term '<term>' field
    And selects product to compare
    Then selected products are displayed in the product compare tray
    When customer clicks on compare
    Then product compare modal is displayed
    And  selected products are included in the product compare overlay
    When customer removes products form compare overlay
    Then selected products are displayed in the product compare tray
    Examples:
      | email                        | password | term  |
      | william.hunter@rustic-hw.com | 12341234 | drill |

  Scenario Outline: Customer Can add products to cart from Product Compare modal
    Given Customer is authenticated using email '<email>' and password '<password>'
    And Customer '<email>' has an empty cart
    When  he submits search term '<term>' field
    And selects product to compare
    And customer clicks on compare
    Then product compare modal is displayed
    When customer clicks on add to cart
    Then add to cart modal is diplayed with the product
    When customer click checkout button
    Then cart page is displayed
    Examples:
      | email                        | password | term  |
      | william.hunter@rustic-hw.com | 12341234 | drill |