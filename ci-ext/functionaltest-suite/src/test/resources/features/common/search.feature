@search
Feature: Search

  Scenario Outline: User types search term into search field
    Given William has logged in using email '<email>' and password '<password>'
    When he types '<term>' in search field
    Then autosuggest results are displayed
  Examples:
  | email                        | password | term  |
  | william.hunter@rustic-hw.com | 12341234 | drill |


  Scenario Outline: User submits search term
    Given William has logged in using email '<email>' and password '<password>'
    When he submits search term '<term>' field
    Then search results are displayed
  Examples:
    | email                        | password | term  |
    | william.hunter@rustic-hw.com | 12341234 | drill |