@save-cart
Feature: Save Cart

  Scenario Outline: Customer Can Save Existing Cart
    Given Customer '<email>' has an empty cart
    And Customer is authenticated using email '<email>' and password '<password>'
    When Customer searched for term '<term>'
    And adds <count> of a product to the cart
    And navigated to the cart page
    And Customer submits save cart form with '<name>' and '<description>'
    Then empty cart page is loaded
    And saved cart is added with '<name>' and '<description>'
    Examples:
      | email                        | password | term  | count | name         | description         |
      | william.hunter@rustic-hw.com | 12341234 | drill | 1     | My test cart | This is a test cart |


  Scenario Outline: Customer Can Navigate Saved Carts
    Given Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to the saved carts page
    Then has <count> saved carts
    When navigates to saved cart details  page
    Then customer is on saved cart details page
    Examples:
      | email                        | password | count |
      | william.hunter@rustic-hw.com | 12341234 | 1     |


  Scenario Outline: Customer can copy saved cart to current cart from saved carts page
    Given Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to the saved carts page
    Then has <count> saved carts
    When clicks saved cart restore button
    And chooses to keep saved cart
    And submits saved cart restore form
    Then cart page is loaded with restored cart
    And copy of saved cart is kept
    Examples:
      | email                        | password | count |
      | william.hunter@rustic-hw.com | 12341234 | 1     |

  Scenario Outline: Customer can restore saved cart to current cart from saved carts page
    Given Customer '<email>' has an empty cart
    And Customer is authenticated using email '<email>' and password '<password>'
    When Customer searched for term '<term>'
    And adds <count> of a product to the cart
    And navigated to the cart page
    And Customer submits save cart form with '<name>' and '<description>'
    And customer navigates to the saved carts page
    Then has <count> saved carts
    When clicks saved cart restore button
    And chooses not to keep saved cart
    And submits saved cart restore form
    Then cart page is loaded with restored cart
    And no copy of saved cart is made
    Examples:
      | email                        | password | term  | count | name         | description         |
      | william.hunter@rustic-hw.com | 12341234 | drill | 1     | My test cart | This is a test cart |


  Scenario Outline: Customer can remove saved cart from saved carts page
    Given Customer is authenticated using email '<email>' and password '<password>'
    When Customer searched for term '<term>'
    And adds <count> of a product to the cart
    And navigated to the cart page
    And Customer submits save cart form with '<name>' and '<description>'
    And customer navigates to the saved carts page
    Then has <count> saved carts
    When clicks saved cart remove button
    And clicks delete button on cart removal modal
    Then saved cart is removed
    Examples:
      | email                        | password | count | name         | description         | term  |
      | william.hunter@rustic-hw.com | 12341234 | 1     | My test cart | This is a test cart | drill |

  Scenario Outline: Customer can copy saved cart from saved cart detail page
    Given Customer is authenticated using email '<email>' and password '<password>'
    When Customer searched for term '<term>'
    And adds <count> of a product to the cart
    And navigated to the cart page
    And Customer submits save cart form with '<name>' and '<description>'
    And customer navigates to the saved carts page
    Then has <count> saved carts
    When navigates to saved cart details  page
    And clicks saved cart restore button
    And submits saved cart restore form from saved cart detail
    Then cart page is loaded with restored cart
    And copy of saved cart is kept
    Examples:
      | email                        | password | count | name         | description         | term  |
      | william.hunter@rustic-hw.com | 12341234 | 1     | My test cart | This is a test cart | drill |


  Scenario Outline: Customer can restore saved cart from saved cart detail page
    Given Customer '<email>' has an empty cart
    And Customer is authenticated using email '<email>' and password '<password>'
    When Customer searched for term '<term>'
    And adds <count> of a product to the cart
    And navigated to the cart page
    And Customer submits save cart form with '<name>' and '<description>'
    And customer navigates to the saved carts page
    Then has <count> saved carts
    When customer navigates to the saved carts page
    And navigates to saved cart details  page
    And clicks saved cart restore button
    And chooses not to keep saved cart
    And submits saved cart restore form from saved cart detail
    Then cart page is loaded with restored cart
    And no copy of saved cart is made
    Examples:
      | email                        | password | count | name         | description         | term  |
      | william.hunter@rustic-hw.com | 12341234 | 1     | My test cart | This is a test cart | drill |


  Scenario Outline: Customer can remove saved cart from saved cart detail page
    Given Customer is authenticated using email '<email>' and password '<password>'
    When Customer searched for term '<term>'
    And adds <count> of a product to the cart
    And navigated to the cart page
    And Customer submits save cart form with '<name>' and '<description>'
    And customer navigates to the saved carts page
    Then has <count> saved carts
    When navigates to saved cart details  page
    And clicks saved cart remove button on saved cart details
    And clicks delete button on cart removal modal
    Then saved cart is removed
    Examples:
      | email                        | password | count | name         | description         | term  |
      | william.hunter@rustic-hw.com | 12341234 | 1     | My test cart | This is a test cart | drill |
