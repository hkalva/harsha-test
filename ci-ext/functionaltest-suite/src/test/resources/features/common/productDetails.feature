@productdetails
Feature: Add to cart

  Scenario Outline: Customer can add product to cart from Product Details Page
    Given Customer '<email>' has an empty cart
    And Customer is authenticated using email '<email>' and password '<password>'
    And Customer searched for term '<term>'
    And Customer navigated to product page
    When Customer clicks on add to cart button
    Then Cart modal opens showing item added to cart
    And Mini cart count is '<count>'
    Examples:
      | email                        | password | count | term    |
      | william.hunter@rustic-hw.com | 12341234 | 1     | drill   |


  Scenario Outline: Customer can change line item count from cart page
    Given Customer '<email>' has an empty cart
    And Customer is authenticated using email '<email>' and password '<password>'
    And has added '<count>' of a product '<term>' to the cart
    And navigated to the cart page
    When customer updates product count to '<newcount>'
    Then cart item count is '<newcount>'
    Examples:
      | email                        | password | term  | count | newcount |
      | william.hunter@rustic-hw.com | 12341234 | drill | 1     | 3        |


  Scenario Outline: Customer can remove product from cart page
    Given Customer '<email>' has an empty cart
    And Customer is authenticated using email '<email>' and password '<password>'
    And has added '<count>' of a product '<term>' to the cart
    And navigated to the cart page
    When Customer removes product from the cart
    Then product is not in the cart
    Examples:
      | email                        | password | term  | count |
      | william.hunter@rustic-hw.com | 12341234 | drill | 1     |

