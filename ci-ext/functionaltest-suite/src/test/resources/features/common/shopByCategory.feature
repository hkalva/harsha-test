@shop
@category
Feature: Shop By Categorry

  Scenario Outline: Customer can navigate to a Category Page
    Given Customer is authenticated using email '<email>' and password '<password>'
    When Customer clicks on Category '<Category Name>'
    Then Category Page is displayed
    And Customer scrolls to see more products
    And Customer filters the results by facet '<facet-name>' with value '<facet-value>'
    And Customer clears the facter filter '<facet-value>'
    Examples:
      | email                        | password | Category Name  | facet-name    | facet-value |
      | william.hunter@rustic-hw.com | 12341234 | HAND TOOLS     | Shop by Brand | Bosch       |


  Scenario Outline: Customer can navigate to a Category Page and sort results
    Given Customer is authenticated using email '<email>' and password '<password>'
    When Customer clicks on Category '<Category Name>'
    Then Category Page is displayed
    And Customer sorts the results by '<sort-by>'
    Examples:
      | email                        | password | Category Name  | sort-by           |
      | william.hunter@rustic-hw.com | 12341234 | HAND TOOLS     | Name (ascending)  |
      | william.hunter@rustic-hw.com | 12341234 | HAND TOOLS     | Name (descending) |


