@my-account @update-profile
Feature: My Account Update Profile

  Scenario Outline: Customer can change their name
    Given Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to user details page
    And customer submits profile form with first name '<first>' and last name '<last>'
    Then customer is still on user-details page
    And customer has a first name of '<first>' and last name of '<last>'
    Examples:
      | email                        | password | first | last   |
      | william.hunter@rustic-hw.com | 12341234 | Bill  | Hunter |
