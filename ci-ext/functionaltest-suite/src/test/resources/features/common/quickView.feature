@quick-view
Feature: Quick View

  Scenario Outline: User submits search term
    Given Customer '<email>' has an empty cart
    And William has logged in using email '<email>' and password '<password>'
    When he submits search term '<term>' field
    And he hovers over a product
    Then quick view button is displayed
    When he click on quick view
    Then quick view modal is displayed
    When he clicks on Add To Cart
    Then product is added to the shopping cart

    Examples:
      | email                        | password | term  |
      | william.hunter@rustic-hw.com | 12341234 | drill |
