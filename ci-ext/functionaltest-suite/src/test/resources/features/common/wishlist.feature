@wishlist
Feature: Wishlist

  Scenario Outline: User adds a product to a new wishlist
    Given Customer '<email>' has an empty cart
    Given Customer '<email>' has no wishlists
    And customer has logged in using email '<email>' and password '<password>'
    When  types '<term>' in search field
    And clicks on add to wishlist button for product <product>
    Then add to wish list modal is displayed
    When customer enter wish list '<name>', '<description>' and clicks on save
    Then wish list with '<name>' is created
    And wish list has product
    Examples:
      | email                        | password | term  | name       | description | product |
      | william.hunter@rustic-hw.com | 12341234 | drill | wishlist-1 | wishlist-1  | 0       |

  Scenario Outline: Add a new wishlist by using Add New on wishlist page
    Given customer has logged in using email '<email>' and password '<password>'
    When  customer navigates to wishlist page
    And clicks on Add New wishlist
    And enter wish list '<name>', '<description>' and clicks on save
    Then wish list with '<name>' is created
    Examples:
      | email                        | password | term  | name       | description |
      | william.hunter@rustic-hw.com | 12341234 | drill | wishlist-2 | wishlist-2  |

  Scenario Outline:  User add a product to an existing wishlist
    Given customer has logged in using email '<email>' and password '<password>'
    When  types '<term>' in search field
    And clicks on add to wishlist button for product <product>
    Then add to wish list modal is displayed
    When customer selectes wishlist '<name>' clicks on save
    Then product is saved to wish list '<name>'
    Examples:
      | email                        | password | term  | product | name       |
      | william.hunter@rustic-hw.com | 12341234 | drill | 1       | wishlist-1 |

  Scenario Outline: Add single product from  wishlist to the cart
    Given Customer '<email>' has an empty cart
    And customer has logged in using email '<email>' and password '<password>'
    And customer navigates to wishlist page
    Then wishlist '<name>' is displayed
    When customer navigates to wishlist details page
    Then wishlist products are listed
    When customer clicks on add to cart
    Then add to cart modal is diplayed with the product
    When customer click checkout button
    Then cart page is displayed
    And product from the wishlist is in the cart
    Examples:
      | email                        | password | name       |
      | william.hunter@rustic-hw.com | 12341234 | wishlist-1 |

  Scenario Outline: Add all products from  wishlist to the cart
    Given Customer '<email>' has an empty cart
    And customer has logged in using email '<email>' and password '<password>'
    And customer navigates to wishlist page
    Then wishlist '<name>' is displayed
    When customer navigates to wishlist details page
    Then wishlist products are listed
    When customer clicks on add all to cart
    Then add to cart modal is diplayed with the product
    When customer click checkout button
    Then cart page is displayed
    And All products from the wishlist are in the cart
    Examples:
      | email                        | password | name       |
      | william.hunter@rustic-hw.com | 12341234 | wishlist-1 |


  Scenario Outline: Remove a product from  wishlist
    Given Customer '<email>' has an empty cart
    And customer has logged in using email '<email>' and password '<password>'
    And customer navigates to wishlist page
    Then wishlist '<name>' is displayed
    When customer navigates to wishlist details page
    Then wishlist products are listed
    When customer clicks on remove product
    Then product is removed from the wishlist '<name>'
    Examples:
      | email                        | password | name       |
      | william.hunter@rustic-hw.com | 12341234 | wishlist-1 |

  Scenario Outline: Delete a wishlist
    Given customer has logged in using email '<email>' and password '<password>'
    And customer navigates to wishlist page
    Then wishlist '<name>' is displayed
    When customer navigates to wishlist details page
    And customer clicks on delete wishlist
    Then wishlist '<name>' is deleted
    Examples:
      | email                        | password | name       |
      | william.hunter@rustic-hw.com | 12341234 | wishlist-1 |