@my-account @address-book
Feature: Address Book

  Scenario Outline: Customer adds address
    Given Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to address book
    And clicks on add address button
    And submits new address
    Then customer is on edit address page
    And success message '<message>' is shown
    Examples:
      | email                        | password | message                   |
      | william.hunter@rustic-hw.com | 12341234 | Your address was created. |

  Scenario Outline: Customer updates existing address
    Given Customer is authenticated using email '<email>' and password '<password>'
    And customer has existing address
    When customer navigates to address book
    And clicks on edit address button
    And submits address change
    Then customer is on edit address page
    And success message '<message>' is shown
    Examples:
      | email                        | password | message                   |
      | william.hunter@rustic-hw.com | 12341234 | Your address was updated. |

  Scenario Outline: Customer removes existing address
    Given Customer is authenticated using email '<email>' and password '<password>'
    And customer has existing address
    When customer navigates to address book
    And clicks on delete address button
    And clicks delete button on delete address modal
    Then customer is on address book page
    And success message '<message>' is shown
    Examples:
      | email                        | password | message                   |
      | william.hunter@rustic-hw.com | 12341234 | Your address was removed. |
