@my-account @update-email
Feature: My Account Update Email

  Scenario Outline: Customer can change their email
    Given Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to update email page
    And customer submits update email form with email '<newemail>' and password '<password>'
    Then customer is still on email-update page
    And customer email is '<newemail>'
    Then  reset customer email from '<newemail>' to '<email>'
    Examples:
      | email                        | password | newemail                      |
      | william.hunter@rustic-hw.com | 12341234 | william.hunter2@rustic-hw.com |

  Scenario Outline: Customer tries to change their email to an existing email
    Given Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to update email page
    And customer submits update email form with email '<newemail>' and password '<password>'
    Then customer is still on email-update page
    And customer email is '<newemail>'
    And and the error message '<error>' is displayed
    Examples:
      | email                        | password | newemail                       | error                                  |
      | william.hunter@rustic-hw.com | 12341234 | anthony.lombardi@rustic-hw.com | The email you entered is not available |


