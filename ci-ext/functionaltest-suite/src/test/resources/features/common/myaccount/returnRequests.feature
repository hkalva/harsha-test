
@my-account @return-details
Feature: Return Details

  Scenario Outline: Customer can navigate to return request detail page
    Given Customer is authenticated using email '<email>' and password '<password>'
    And customer '<email>' has return request in '<status>' status
    When customer navigates to return history page
    And customer clicks on return request
    Then customer is on return request detail page
    Examples:
      | email                        | password | status    |
      | returns.tester@rustic-hw.com | 12341234 | APPROVAL_PENDING |


  Scenario Outline: Customer can cancel return request
    Given Customer is authenticated using email '<email>' and password '<password>'
   # And customer has return request in '<status>' status
    When customer navigates to return history page
    And customer clicks on return request
    And customer clicks on cancel return button
    And clicks submit request
    Then return is canceled

    Examples:
      | email                        | password | status    |
      | returns.tester@rustic-hw.com | 12341234 | APPROVAL_PENDING |