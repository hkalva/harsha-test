@my-account @payment-details
Feature: My Account Payment Details

  Scenario Outline: Customer can remove an existing payment detail
    Given Customer is authenticated using email '<email>' and password '<password>'
    And customer '<email>' has saved payment info
    When customer clicks on payment info delete button
    And clicks delete button on delete payment modal
    Then customer remains on payment details page
    And payment detail is removed
    Examples:
      | email                        | password |
      | william.hunter@rustic-hw.com | 12341234 |
