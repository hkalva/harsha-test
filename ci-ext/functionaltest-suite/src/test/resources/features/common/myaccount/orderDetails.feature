@my-account @order-details
Feature: Order Details

  Scenario Outline: Customer can navigate to new order from order history page
    Given Customer is authenticated using email '<email>' and password '<password>'
    When customer places order for '<term>'
    And customer navigates to order-history page
    And customer clicks on the order link
    Then customer is on order detail page

    Examples:
      | email                        | password | term  |
      | william.hunter@rustic-hw.com | 12341234 | drill |


  @reorders
  Scenario Outline: Customer reorders existing order
    Given Customer is authenticated using email '<email>' and password '<password>'
    And customer '<email>' has a completed order
    When customer navigates to order-history page
    And customer clicks on the order link
    And clicks reorder
    Then customer is on payment method checkout step
    And new cart is created using order items

    Examples:
      | email                        | password |
      | william.hunter@rustic-hw.com | 12341234 |

  @returns
  Scenario Outline: Customer returns whole order
    Given Customer is authenticated using email '<email>' and password '<password>'
    And customer '<email>' has a completed order
    When customer navigates to order-history page
    And customer clicks on the order link
    And clicks return order
    And clicks on return complete order button
    And clicks on confirm return order button
    And clicks on submit request button
    Then customer is on order history page
    And success message '<message>' is shown
    And return request is created for order
    Examples:
      | email                        | password | message                                    |
      | william.hunter@rustic-hw.com | 12341234 | Request for Return submitted successfully. |
