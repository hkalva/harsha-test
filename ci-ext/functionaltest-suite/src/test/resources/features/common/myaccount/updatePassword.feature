@my-account @update-password
Feature: My Account Update Password

  Scenario Outline: Customer can change password
    Given Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to update password page
    And submits password form with current password '<password>' and new password '<newpassword>' and confirm password '<confirmpassword>'
    Then customer is still on update password page
    And success message '<message>' is shown
    And customer '<email>' password has changed from '<password>'
    And reset customer '<email>' password to '<password>'
    Examples:
      | email                        | password | newpassword | confirmpassword | message                        |
      | william.hunter@rustic-hw.com | 12341234 | Aa!2341234  | Aa!2341234      | Your password has been changed |

  Scenario Outline: Customer types wrong current password
    Given Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to update password page
    And submits password form with current password '<newpassword>' and new password '<newpassword>' and confirm password '<newpassword>'
    Then customer is still on update password page
    And current password validation error message '<message>' is shown
    Examples:
      | email                        | password | newpassword | message                            |
      | william.hunter@rustic-hw.com | 12341234 | Aa!2341234  | Please enter your current password |

  Scenario Outline: Customer types wrong confirm password
    Given Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to update password page
    And submits password form with current password '<password>' and new password '<newpassword>' and confirm password '<confirmpassword>'
    Then customer is still on update password page
    And password do not match validation error message '<message>' is shown
    Examples:
      | email                        | password | newpassword | confirmpassword | message                                         |
      | william.hunter@rustic-hw.com | 12341234 | Aa!2341234  | 123456          | Password and password confirmation do not match |

  Scenario Outline: Customer types new password not meeting strength requirements
    Given Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to update password page
    And submits password form with current password '<password>' and new password '<newpassword>' and confirm password '<confirmpassword>'
    Then customer is still on update password page
    And password requirement validation error message '<message>' is shown
    Examples:
      | email                        | password | newpassword | confirmpassword | message                                      |
      | william.hunter@rustic-hw.com | 12341234 | 1234        | 1234            | Password does not meet minimum requirements. |
