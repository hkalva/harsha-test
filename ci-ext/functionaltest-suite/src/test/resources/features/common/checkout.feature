@checkout
Feature: Checkout

  Scenario Outline: Customer can checkout
    Given Customer is authenticated using email '<email>' and password '<password>'
    And has added '<count>' of a product '<term>' to the cart
    And navigated to the cart page
    When customer click checkout button
    And adds payment information
    And adds shipping address
    And selects shipping method
    And adds billing information
    And places order
    Then order confirmation page is created
    And order business process is started
    Examples:
      | email                        | password | count | term  |
      | william.hunter@rustic-hw.com | 12341234 | 1     | drill |
