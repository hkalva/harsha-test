@cart
Feature: Add to cart

  Scenario Outline: Customer can add to cart from search page
    Given Customer '<email>' has an empty cart
    And Customer is authenticated using email '<email>' and password '<password>'
    And Customer searched for term '<term>'
    When Customer adds item to cart
    Then Cart modal opens showing item added to cart
    And Mini cart count is <count>
  Examples:
    | email                        | password | term  | count |
    | william.hunter@rustic-hw.com | 12341234 | drill | 1     |


  Scenario Outline: Customer can access cart page using add cart popup modal
    Given Customer '<email>' has an empty cart
    And Customer is authenticated using email '<email>' and password '<password>'
    And Customer searched for term '<term>'
    And Customer has added item to cart
    When Customer clicks modal checkout button
    Then cart page is displayed
  Examples:
    | email                        | password | term  |
    | william.hunter@rustic-hw.com | 12341234 | drill |
    
    
  Scenario Outline: Customer can change line item count from cart page
    Given Customer '<email>' has an empty cart
    And Customer is authenticated using email '<email>' and password '<password>'
    And Customer searched for term '<term>'
    And adds <count> of a product to the cart
    And navigated to the cart page
    When Customer change the qty of product <qtantity>
    Then Cart page is loaded with new qty of product <qtantity>
    Examples:
      | email                        | password | term  | count | qtantity |
      | william.hunter@rustic-hw.com | 12341234 | drill | 1     | 2	  		 |
      
  Scenario Outline: Customer can remove product from cart page
    Given Customer '<email>' has an empty cart
    And Customer is authenticated using email '<email>' and password '<password>'
    And Customer searched for term '<term>'
    And adds <count> of a product to the cart
    And navigated to the cart page
    When Customer clicks on  Remove button
    Then Cart page is loaded with empty product
    Examples:
      | email                        | password | term  | count |
      | william.hunter@rustic-hw.com | 12341234 | drill | 1     |