@b2b @portal
Feature: B2B Secure Portal

  Scenario: Unauthenticated user lands on home page
    Given William is not authenticated
    When he opens the home page
    Then the login page is shown

  Scenario Outline: Failed authentication attempt
    Given William is not authenticated
    And is on the login page
    When he submits incorrect email '<email>' or password '<password>'
    Then he remains on login page
    And generic error message '<message>' is shown
  Examples:
    | email                        | password       | message                                  |
    | william.hunter@rustic-hw.com | wrong-password | Your username or password was incorrect. |
    | idontexist@rustic-hw.com     | wrong-password | Your username or password was incorrect. |

  Scenario Outline: Successful authentication
    Given William is not authenticated
    And is on the login page
    When he submits valid email '<email>' and password '<password>'
    Then the home page is shown
  Examples:
  | email                        | password |
  | william.hunter@rustic-hw.com | 12341234 |

  Scenario Outline: Repeated authentication failures result in locked account.
    Given William is not authenticated
    And is on the login page
    When he submits valid email '<email>' and invalid '<password>' <number> times
    Then customer '<email>' account is locked
  Examples:
    | email                        | password       | number |
    | william.hunter@rustic-hw.com | wrong-password | 6      |