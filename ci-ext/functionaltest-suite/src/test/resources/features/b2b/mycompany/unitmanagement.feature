@b2b @my-company @unit-management
Feature: B2B Unit Management

  Scenario Outline: B2B Admin adds new unit
    Given commerce org '<commerceorg>' exists
    And unit '<unit>' does not exist
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to units page
    And clicks add new unit button
    And submits user form having id '<unit>', name '<unit>', parent unit '<parent_unit>', and approval process '<approval_process>'
    Then unit '<unit>' now exists
    And customer is on manage unit details page
    And the message 'Business unit saved successfully' is displayed
  Examples:
  | commerceorg         | email                                                   | password | unit                      | parent_unit         | approval_process |
  | TestAutoUnitMgmtOrg | linda.wolf.testautounitmgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoUnitMgmtOrg North | TestAutoUnitMgmtOrg | accApproval      |

  Scenario Outline: B2B Admin edits existing unit
    Given commerce org '<commerceorg>' exists
    And unit having id '<unit>', name '<unit>', parent unit '<parent_unit>', and approval process '<approval_process>'
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to units page
    And navigates to unit '<unit>' details page
    And clicks add edit button
    And submits user form having id '<unit>', name '<unit_name>', parent unit '<parent_unit>', and approval process '<approval_process>'
    Then unit '<unit>' now has name '<unit_name>', parent unit '<parent_unit>', and approval process '<approval_process>'
    And customer is on manage unit details page
    And the message 'Business unit saved successfully' is displayed
  Examples:
  | commerceorg         | email                                                   | password | unit                      | unit_name                     | parent_unit         | approval_process |
  | TestAutoUnitMgmtOrg | linda.wolf.testautounitmgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoUnitMgmtOrg South | TestAutoUnitMgmtOrg South USA | TestAutoUnitMgmtOrg | accApproval      |

  Scenario Outline: B2B Admin adds address to unit
    Given commerce org '<commerceorg>' exists
    And address having owner '<unit>', street name '<streetname>', street number '<streetnumber>', and postal code '<postalcode>' does not exist
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to units page
    And navigates to unit '<unit>' details page
    And clicks add new address button
    And submits address form having country '<country>', title '<title>' first name '<firstname>', last name '<lastname>', street name '<streetname>', street number '<streetnumber>', city '<city>' and postal code '<postalcode>'
    Then unit '<unit>' has address with country '<country>', title '<title>' first name '<firstname>', last name '<lastname>', street name '<streetname>', street number '<streetnumber>', city '<city>' and postal code '<postalcode>'
    And customer is on manage unit details page
    And the message 'Your address was created' is displayed
  Examples:
  | commerceorg         | email                                                   | password | unit                      | streetname | streetnumber | city    | postalcode | country | title | firstname | lastname |
  | TestAutoUnitMgmtOrg | linda.wolf.testautounitmgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoUnitMgmtOrg South | Wacker Dr. | 30           | Chicago | 60607      | US      | mr    | Linda     | Wolf     |
