@b2b @my-company @permission-management
Feature: B2B Permissions Management

  Scenario Outline: B2B Admin adds new timespan permission
    Given commerce org '<commerceorg>' exists
    And permission '<permission>' does not exist
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage permissions page
    And clicks add new permission button
    And submits permission form having type '<type>', timespan '<timespan>', currency '<currency>', value '<amount>', parent unit '<parent_unit>' and code '<permission>'
    Then permission having code '<permission>' is created
    And customer is on view permission page
    And the message 'Permission created successfully' is displayed
  Examples:
    | commerceorg         | email                                                   | password | type                                | timespan | currency | amount | permission                               | parent_unit         |
    | TestAutoPermMgmtOrg | linda.wolf.testautopermmgmtorg@sapcc-test.capgemini.com | 12341234 | B2BOrderThresholdTimespanPermission | MONTH    | USD      | 100000 | TestAutoPermMgmtOrg USD 100000 PER MONTH | TestAutoPermMgmtOrg |


  Scenario Outline: B2B Admin changes existing permission's parent unit
    Given commerce org '<commerceorg>' exists
    And permission '<permission>' has unit '<original_unit>'
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage permissions page
    And navigates to permission '<permission>' view page
    And clicks edit permission button
    And changes unit to '<new_unit>'
    And submits permission form
    Then permission '<permission>' unit is '<new_unit>'
    And customer is on view permission page
    And the message 'Permission updated successfully' is displayed
  Examples:
    | commerceorg         | email                                                   | password | permission                        | original_unit       | new_unit                          |
    | TestAutoPermMgmtOrg | linda.wolf.testautopermmgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoPermMgmtOrg 15K USD MONTH | TestAutoPermMgmtOrg | TestAutoPermMgmtOrg Custom Retail |

  Scenario Outline: B2B Admin disables permission
    Given commerce org '<commerceorg>' exists
    And permission '<permission>' is enabled
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage permissions page
    And navigates to permission '<permission>' view page
    And clicks disable permission button
    And clicks on confirm action button
    Then permission '<permission>' is now disabled
    And customer is on view permission page
  Examples:
    | commerceorg         | email                                                   | password | permission                       |
    | TestAutoPermMgmtOrg | linda.wolf.testautopermmgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoPermMgmtOrg 4K USD ORDER |

  Scenario Outline: B2B Admin enables permission
    Given commerce org '<commerceorg>' exists
    And permission '<permission>' is disabled
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage permissions page
    And navigates to permission '<permission>' view page
    And clicks enable permission button
    Then permission '<permission>' is now enabled
    And customer is on view permission page
  Examples:
    | commerceorg         | email                                                   | password | permission                       |
    | TestAutoPermMgmtOrg | linda.wolf.testautopermmgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoPermMgmtOrg 4K USD ORDER |
