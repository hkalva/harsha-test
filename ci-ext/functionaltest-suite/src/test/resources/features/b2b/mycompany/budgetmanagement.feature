@b2b @my-company @budget-management
Feature: B2B Budget Management

  Scenario Outline: B2B Admin adds new budget
    Given commerce org '<commerceorg>' exists
    And budget '<budget>' does not exist
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage budgets page
    And clicks add new budget button
    And submits budget form having code '<budget>', unit '<unit>', name '<budget_name>', currency '<currency>', amount '<amount>', start date '<start_date>', and end date '<end_date>'
    Then budget having code '<budget>' is created
    And customer is on view budget page
    And the message 'Budget created successfully' is displayed
  Examples:
  | commerceorg           | email                                                     | password | budget                                | budget_name     | unit                                | currency | amount   | start_date | end_date   |
  | TestAutoBudgetMgmtOrg | linda.wolf.testautobudgetmgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoBudgetMgmtOrg Monthly 30K USD | Monthly 30K USD | TestAutoBudgetMgmtOrg Services West | USD      | 30000.00 | 01/01/2020 | 01/01/2024 |

  Scenario Outline: B2B Admin disables budget
    Given commerce org '<commerceorg>' exists
    And budget '<budget>' is enabled
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage budgets page
    And navigates to budget '<budget>' view page
    And clicks disable budget button
    And clicks on confirm action button
    Then budget '<budget>' is now disabled
    And customer is on view budget page
  Examples:
  | commerceorg           | email                                                     | password | budget                                 |
  | TestAutoBudgetMgmtOrg | linda.wolf.testautobudgetmgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoBudgetMgmtOrg Monthly 2.5K USD |

  Scenario Outline: B2B Admin enables budget
    Given commerce org '<commerceorg>' exists
    And budget '<budget>' is disabled
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage budgets page
    And navigates to budget '<budget>' view page
    And clicks enable budget button
    Then budget '<budget>' is now enabled
    And customer is on view budget page
  Examples:
  | commerceorg           | email                                                     | password | budget                               |
  | TestAutoBudgetMgmtOrg | linda.wolf.testautobudgetmgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoBudgetMgmtOrg Monthly 4K USD |

  Scenario Outline: B2B Admin updates budget
    Given commerce org '<commerceorg>' exists
    And budget '<budget>' has name '<budget_name>'
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage budgets page
    And navigates to budget '<budget>' view page
    And clicks on budget edit button
    And submits budget form having code '<budget>', unit '<unit>', name '<budget>', currency '<currency>', amount '<amount>', start date '<start_date>', and end date '<end_date>'
    Then budget '<budget>' now has name '<budget>'
    And customer is on view budget page
    And the message 'Budget updated successfully' is displayed
  Examples:
  | commerceorg           | email                                                     | password | budget                                | budget_name     | unit                  | currency | amount   | start_date | end_date   |
  | TestAutoBudgetMgmtOrg | linda.wolf.testautobudgetmgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoBudgetMgmtOrg Monthly 50K USD | Monthly 50K USD | TestAutoBudgetMgmtOrg | USD      | 50000.00 | 01/01/2010 | 07/12/2034 |

