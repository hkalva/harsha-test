@b2b @my-company @costcenter-management
Feature: B2B Cost Center Management

  Scenario Outline: B2B Admin adds new cost center
    Given commerce org '<commerceorg>' exists
    And cost center '<costcenter>' does not exist
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage cost centers page
    And clicks add new cost center button
    And submits cost center form having code '<costcenter>', unit '<unit>', name '<costcenter>', and currency '<currency>'
    Then cost center having code '<costcenter>' is created
    And customer is on view cost center page
    And the message 'Cost Center created successfully' is displayed
  Examples:
  | commerceorg               | email                                                         | password | costcenter                              | unit                                    | currency |
  | TestAutoCostCenterMgmtOrg | linda.wolf.testautocostcentermgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoCostCenterMgmtOrg Services West | TestAutoCostCenterMgmtOrg Services West | USD      |

  Scenario Outline: B2B Admin disables cost center
    Given commerce org '<commerceorg>' exists
    And cost center '<costcenter>' is enabled
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage cost centers page
    And navigates to cost center '<costcenter>' view page
    And clicks disable cost center button
    And clicks on confirm action button
    Then cost center '<costcenter>' is now disabled
    And customer is on view cost center page
  Examples:
  | commerceorg               | email                                                         | password | costcenter                              |
  | TestAutoCostCenterMgmtOrg | linda.wolf.testautocostcentermgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoCostCenterMgmtOrg Services East |

  Scenario Outline: B2B Admin enables cost center
    Given commerce org '<commerceorg>' exists
    And cost center '<costcenter>' is disabled
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage cost centers page
    And navigates to cost center '<costcenter>' view page
    And clicks enable cost center button
    Then cost center '<costcenter>' is now enabled
    And customer is on view cost center page
  Examples:
  | commerceorg               | email                                                         | password | costcenter                              |
  | TestAutoCostCenterMgmtOrg | linda.wolf.testautocostcentermgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoCostCenterMgmtOrg Services East |

  Scenario Outline: B2B Admin updates cost center budgets
    Given commerce org '<commerceorg>' exists
    And cost center '<costcenter>' has budgets '<old_budgets>'
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage cost centers page
    And navigates to cost center '<costcenter>' view page
    And clicks add existing budget button
    And toggles budgets '<toggled_budgets>'
    And clicks on done button
    Then cost center '<costcenter>' now has budgets '<expected_budgets>'
    And customer is on view cost center page
  Examples:
  | commerceorg               | email                                                         | password | costcenter                              | old_budgets                               | toggled_budgets                                                                     | expected_budgets                          |
  | TestAutoCostCenterMgmtOrg | linda.wolf.testautocostcentermgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoCostCenterMgmtOrg Services East | TestAutoCostCenterMgmtOrg Weekly 2.5K USD | TestAutoCostCenterMgmtOrg Weekly 2.5K USD,TestAutoCostCenterMgmtOrg Monthly 50K USD | TestAutoCostCenterMgmtOrg Monthly 50K USD |


  Scenario Outline: B2B Admin updates cost center
    Given commerce org '<commerceorg>' exists
    And cost center '<costcenter>' has name '<costcenter_name>'
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage cost centers page
    And navigates to cost center '<costcenter>' view page
    And clicks on cost center edit button
    And submits cost center form having code '<costcenter>', unit '<unit>', name '<costcenter>', and currency '<currency>'
    Then cost center '<costcenter>' now has name '<costcenter>'
    And customer is on view cost center page
    And the message 'Cost Center updated successfully' is displayed
  Examples:
  | commerceorg               | email                                                         | password | costcenter                       | costcenter_name            | unit                             | currency |
  | TestAutoCostCenterMgmtOrg | linda.wolf.testautocostcentermgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoCostCenterMgmtOrg Retail | This is a test cost center | TestAutoCostCenterMgmtOrg Retail | USD      |

