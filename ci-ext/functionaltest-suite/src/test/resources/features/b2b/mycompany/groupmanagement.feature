@b2b @my-company @group-management
Feature: B2B Group Management

  Scenario Outline: B2B Admin adds new group
    Given commerce org '<commerceorg>' exists
    And group '<group>' does not exist
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage groups page
    And clicks add new group button
    And submits group form having code '<group>', unit '<unit>', and name '<group_name>'
    Then group '<group>' has name '<group>', and unit '<unit>'
    And customer is on group detail page
    And the message 'User group updated successfully' is displayed
  Examples:
  | commerceorg          | email                                                    | password | group                                     | group_name                                | unit                               |
  | TestAutoGroupMgmtOrg | linda.wolf.testautogroupmgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoGroupMgmtOrgRestrictedPermissions | TestAutoGroupMgmtOrgRestrictedPermissions | TestAutoGroupMgmtOrg Services West |

  Scenario Outline: B2B Admin disables group
    Given commerce org '<commerceorg>' exists
    And group '<group>' has members '<members>'
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage groups page
    And navigates to group '<group>' view page
    And clicks disable group button
    And clicks on confirm action button
    Then group '<group>' is now disabled
    And customer is on group detail page
  Examples:
  | commerceorg          | email                                                    | password | group                                   | members                                                                                                         |
  | TestAutoGroupMgmtOrg | linda.wolf.testautogroupmgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoGroupMgmtOrgStandardPermissions | marie.dubois.testautogroupmgmtorg@sapcc-test.capgemini.com,gi.sun.testautogroupmgmtorg@sapcc-test.capgemini.com |

  Scenario Outline: B2B Admin updates group
    Given commerce org '<commerceorg>' exists
    And group having uid '<group>', name '<group_name>', and unit '<unit>'
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage groups page
    And navigates to group '<group>' view page
    And clicks on group edit button
    And submits group form having code '<group>', unit '<unit>', and name '<group>'
    Then group '<group>' has name '<group>', and unit '<unit>'
    And customer is on group detail page
    And the message 'User group updated successfully' is displayed
  Examples:
  | commerceorg          | email                                                    | password | group                                  | group_name          | unit                 |
  | TestAutoGroupMgmtOrg | linda.wolf.testautogroupmgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoGroupMgmtOrgLimitedPermissions | Limited Permissions | TestAutoGroupMgmtOrg |


  Scenario Outline: B2B Admin updates group members
    Given commerce org '<commerceorg>' exists
    And group '<group>' has members '<original_members>'
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage groups page
    And navigates to group '<group>' view page
    And clicks on add existing user link
    And toggles members '<toggled_members>'
    And clicks on done button
    Then group '<group>' now has members '<expected_members>'
    And customer is on group detail page
  Examples:
  | commerceorg          | email                                                    | password | group                                   | original_members                                                                                                | toggled_members                                                                                                | expected_members                                                                                                    |
  | TestAutoGroupMgmtOrg | linda.wolf.testautogroupmgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoGroupMgmtOrgStandardPermissions | marie.dubois.testautogroupmgmtorg@sapcc-test.capgemini.com,gi.sun.testautogroupmgmtorg@sapcc-test.capgemini.com | gi.sun.testautogroupmgmtorg@sapcc-test.capgemini.com,mark.rivers.testautogroupmgmtorg@sapcc-test.capgemini.com | mark.rivers.testautogroupmgmtorg@sapcc-test.capgemini.com,marie.dubois.testautogroupmgmtorg@sapcc-test.capgemini.com|

  Scenario Outline: B2B Admin updates group permissions
    Given commerce org '<commerceorg>' exists
    And group '<group>' has permissions '<original_permissions>'
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage groups page
    And navigates to group '<group>' view page
    And clicks on add existing permission link
    And toggles permissions '<toggled_permissions>'
    And clicks on done button
    Then group '<group>' now has permissions '<expected_permissions>'
    And customer is on group detail page
  Examples:
  | commerceorg          | email                                                    | password | group                                   | original_permissions                                                 | toggled_permissions                                                                                    | expected_permissions                                                                                    |
  | TestAutoGroupMgmtOrg | linda.wolf.testautogroupmgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoGroupMgmtOrgStandardPermissions | TestAutoGroupMgmtOrg 5K USD ORDER,TestAutoGroupMgmtOrg 15K USD MONTH | TestAutoGroupMgmtOrg 5K USD ORDER,TestAutoGroupMgmtOrg 25K USD MONTH,TestAutoGroupMgmtOrg 7K USD ORDER | TestAutoGroupMgmtOrg 15K USD MONTH,TestAutoGroupMgmtOrg 25K USD MONTH,TestAutoGroupMgmtOrg 7K USD ORDER |

  Scenario Outline: B2B Admin deletes group
    Given commerce org '<commerceorg>' exists
    And group having uid '<group>', name '<group_name>', and unit '<unit>'
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage groups page
    And navigates to group '<group>' view page
    And clicks on group delete button
    And clicks on confirm action button
    Then group '<group>' no longer exists
    And the message 'User group successfully removed' is displayed
  Examples:
  | commerceorg          | email                                                    | password | group                                      | group_name                | unit                 |
  | TestAutoGroupMgmtOrg | linda.wolf.testautogroupmgmtorg@sapcc-test.capgemini.com | 12341234 | TestAutoGroupMgmtOrgToBeDeletedPermissions | To Be Deleted Permissions | TestAutoGroupMgmtOrg |
