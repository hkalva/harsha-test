@b2b @my-company @user-management
Feature: B2B User Management

  Scenario Outline: B2B Admin adds new user
    Given commerce org '<commerceorg>' exists
    And customer with '<user_email>' does not exist
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage users page
    And clicks add new user button
    And submits user form having title '<user_title>', first name '<user_firstname>' last name '<user_lastname>', email '<user_email>', unit '<user_unit>', and roles '<user_roles>'
    Then user having '<user_email>' is created
    And customer is on manage user details page
    And the message 'Customer successfully created' is displayed
  Examples:
  | commerceorg         | email                                                   | password | user_title | user_firstname | user_lastname | user_email                                               | user_unit           | user_roles       |
  | TestAutoUserMgmtOrg | linda.wolf.testautousermgmtorg@sapcc-test.capgemini.com | 12341234 | mr         | John           | Jacobs        | john.jacobs.testautousermgmtorg@sapcc-test.capgemini.com | TestAutoUserMgmtOrg | b2bcustomergroup |


  Scenario Outline: B2B Admin disables existing enabled user
    Given commerce org '<commerceorg>' exists
    And Customer is authenticated using email '<email>' and password '<password>'
    And the user '<user>' is enabled
    When customer navigates to manage users page
    And customer navigates to user '<user>' detail page
    And clicks on disable user button
    And clicks on disable button on disable confirm button
    Then user '<user>' is disabled
    And customer is on manage user details page
    And the message 'The user has been disabled' is displayed
  Examples:
  | commerceorg         | email                                                   | password | user                                                        |
  | TestAutoUserMgmtOrg | linda.wolf.testautousermgmtorg@sapcc-test.capgemini.com | 12341234 | william.hunter.testautousermgmtorg@sapcc-test.capgemini.com |


  Scenario Outline: B2B Admin enables existing disabled user
    Given commerce org '<commerceorg>' exists
    And Customer is authenticated using email '<email>' and password '<password>'
    And the user '<user>' is disabled
    When customer navigates to manage users page
    And customer navigates to user '<user>' detail page
    And clicks on enable user button
    Then user '<user>' is enabled
    And customer is on manage user details page
    And the message 'The user has been enabled' is displayed
  Examples:
  | commerceorg         | email                                                   | password | user                                                        |
  | TestAutoUserMgmtOrg | linda.wolf.testautousermgmtorg@sapcc-test.capgemini.com | 12341234 | william.hunter.testautousermgmtorg@sapcc-test.capgemini.com |

  Scenario Outline: B2B Admin edits existing user
    Given commerce org '<commerceorg>' exists
    And Customer is authenticated using email '<email>' and password '<password>'
    And the user '<user_email>' has groups '<original_groups>'
    When customer navigates to manage users page
    And customer navigates to user '<user_email>' detail page
    And clicks user edit button
    And submits user form having title '<user_title>', first name '<user_firstname>' last name '<user_lastname>', email '<user_email>', unit '<user_unit>', and roles '<user_roles>'
    Then user '<user_email>' has groups '<expected_groups>'
    And customer is on manage user details page
    And the message 'Customer successfully updated' is displayed
  Examples:
    | commerceorg         | email                                                   | password | user_title | user_firstname | user_lastname | user_email                                                | user_unit                  | user_roles                       | original_groups                                                                           | expected_groups                                                                                                                      |
    | TestAutoUserMgmtOrg | linda.wolf.testautousermgmtorg@sapcc-test.capgemini.com | 12341234 | ms         | Marie          | Dubois        | marie.dubois.testautousermgmtorg@sapcc-test.capgemini.com | TestAutoUserMgmtOrg Retail | b2bmanagergroup,b2bcustomergroup | TestAutoUserMgmtOrg Custom Retail,b2bcustomergroup,TestAutoUserMgmtOrgStandardPermissions | TestAutoUserMgmtOrg Retail,TestAutoUserMgmtOrg Custom Retail,b2bcustomergroup,b2bmanagergroup,TestAutoUserMgmtOrgStandardPermissions |

  Scenario Outline: B2B Admin updates user's groups
    Given commerce org '<commerceorg>' exists
    And the user '<user_email>' has groups '<original_groups>'
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage users page
    And customer navigates to user '<user_email>' detail page
    And clicks add existing group button
    And clicks on group '<new_group>'
    And clicks on group '<existing_group>'
    And clicks on done button
    Then user '<user_email>' has groups '<expected_groups>'
    And customer is on manage user details page
    And group '<new_group>' is visible on user details page
    Examples:
    | commerceorg         | email                                                   | password | user_email                                                     | new_group                             | existing_group                         | original_groups                                                                           | expected_groups                                                                          |
    | TestAutoUserMgmtOrg | linda.wolf.testautousermgmtorg@sapcc-test.capgemini.com | 12341234 | alejandro.navarro.testautousermgmtorg@sapcc-test.capgemini.com | TestAutoUserMgmtOrgLimitedPermissions | TestAutoUserMgmtOrgStandardPermissions | b2bcustomergroup,TestAutoUserMgmtOrg Services East,TestAutoUserMgmtOrgStandardPermissions | b2bcustomergroup,TestAutoUserMgmtOrg Services East,TestAutoUserMgmtOrgLimitedPermissions |


  Scenario Outline: B2B Admin removes group from user
    Given commerce org '<commerceorg>' exists
    And the user '<user_email>' has groups '<original_groups>'
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage users page
    And customer navigates to user '<user_email>' detail page
    And clicks on group '<existing_group>' card's remove button
    And clicks on confirm action button
    Then user '<user_email>' has groups '<expected_groups>'
    And customer is on manage user details page
    And group '<existing_group>' is not visible on user details page
  Examples:
  | commerceorg         | email                                                   | password | user_email                                                     | existing_group                         | original_groups                                                                           | expected_groups                                    |
  | TestAutoUserMgmtOrg | linda.wolf.testautousermgmtorg@sapcc-test.capgemini.com | 12341234 | alejandro.navarro.testautousermgmtorg@sapcc-test.capgemini.com | TestAutoUserMgmtOrgStandardPermissions | b2bcustomergroup,TestAutoUserMgmtOrg Services East,TestAutoUserMgmtOrgStandardPermissions | b2bcustomergroup,TestAutoUserMgmtOrg Services East |

  Scenario Outline: B2B Admin removes permission from user
    Given commerce org '<commerceorg>' exists
    And the user '<user_email>' has permissions '<original_permissions>'
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage users page
    And customer navigates to user '<user_email>' detail page
    And clicks on permission '<permission>' card's remove button
    And clicks on confirm action button
    Then user '<user_email>' has permissions '<expected_permissions>'
    And customer is on manage user details page
    And permission '<permission>' is not visible on user details page
  Examples:
  | commerceorg         | email                                                   | password | user_email                                                     | permission                        | original_permissions                                                                                 | expected_permissions                                               |
  | TestAutoUserMgmtOrg | linda.wolf.testautousermgmtorg@sapcc-test.capgemini.com | 12341234 | alejandro.navarro.testautousermgmtorg@sapcc-test.capgemini.com | TestAutoUserMgmtOrg 15K USD ORDER | TestAutoUserMgmtOrg 1K USD ORDER,TestAutoUserMgmtOrg 15K USD ORDER,TestAutoUserMgmtOrg 20K USD MONTH | TestAutoUserMgmtOrg 1K USD ORDER,TestAutoUserMgmtOrg 20K USD MONTH |

  Scenario Outline: B2B Admin removes approver from user
    Given commerce org '<commerceorg>' exists
    And the user '<user_email>' has approvers '<original_approvers>'
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage users page
    And customer navigates to user '<user_email>' detail page
    And clicks on approver '<approver>' card's remove button
    And clicks on confirm action button
    Then user '<user_email>' has approvers '<expected_approvers>'
    And customer is on manage user details page
    And approver '<approver>' is not visible on user details page
  Examples:
    | commerceorg         | email                                                   | password | user_email                                                     | approver                                                  | original_approvers                                                                                                | expected_approvers                                      |
    | TestAutoUserMgmtOrg | linda.wolf.testautousermgmtorg@sapcc-test.capgemini.com | 12341234 | alejandro.navarro.testautousermgmtorg@sapcc-test.capgemini.com | carla.torres.testautousermgmtorg@sapcc-test.capgemini.com | carla.torres.testautousermgmtorg@sapcc-test.capgemini.com,james.bell.testautousermgmtorg@sapcc-test.capgemini.com | james.bell.testautousermgmtorg@sapcc-test.capgemini.com |

  Scenario Outline: B2B Admin updates user's approvers
    Given commerce org '<commerceorg>' exists
    And the user '<user_email>' has approvers '<original_approvers>'
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage users page
    And customer navigates to user '<user_email>' detail page
    And clicks add existing approver button
    And toggles approvers '<toggled_approvers>'
    And clicks on done button
    Then user '<user_email>' has approvers '<expected_approvers>'
    And customer is on manage user details page
  Examples:
    | commerceorg         | email                                                   | password | user_email                                                     | toggled_approvers                                                                                                   | original_approvers                                                                                                | expected_approvers                                                                                                |
    | TestAutoUserMgmtOrg | linda.wolf.testautousermgmtorg@sapcc-test.capgemini.com | 12341234 | alejandro.navarro.testautousermgmtorg@sapcc-test.capgemini.com | carla.torres.testautousermgmtorg@sapcc-test.capgemini.com,matheu.silva.testautousermgmtorg@sapcc-test.capgemini.com | carla.torres.testautousermgmtorg@sapcc-test.capgemini.com,james.bell.testautousermgmtorg@sapcc-test.capgemini.com | james.bell.testautousermgmtorg@sapcc-test.capgemini.com,matheu.silva.testautousermgmtorg@sapcc-test.capgemini.com |

  Scenario Outline: B2B Admin updates user's permissions
    Given commerce org '<commerceorg>' exists
    And the user '<user_email>' has permissions '<original_permissions>'
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to manage users page
    And customer navigates to user '<user_email>' detail page
    And clicks add existing permission button
    And toggles permissions '<toggled_permissions>'
    And clicks on done button
    Then user '<user_email>' has permissions '<expected_permissions>'
    And customer is on manage user details page
  Examples:
    | commerceorg         | email                                                   | password | user_email                                                     | toggled_permissions                                                                                       | original_permissions                                                                                 | expected_permissions                                                                                                                       |
    | TestAutoUserMgmtOrg | linda.wolf.testautousermgmtorg@sapcc-test.capgemini.com | 12341234 | alejandro.navarro.testautousermgmtorg@sapcc-test.capgemini.com | TestAutoUserMgmtOrg 15K USD ORDER,TestAutoUserMgmtOrg 3K USD MONTH,TestAutoUserMgmtOrg Unlimited Timespan | TestAutoUserMgmtOrg 1K USD ORDER,TestAutoUserMgmtOrg 15K USD ORDER,TestAutoUserMgmtOrg 20K USD MONTH | TestAutoUserMgmtOrg 1K USD ORDER,TestAutoUserMgmtOrg 20K USD MONTH,TestAutoUserMgmtOrg 3K USD MONTH,TestAutoUserMgmtOrg Unlimited Timespan |
