@b2b @my-company @document-management
Feature: Document Management

  Scenario Outline: B2B Admin Filters documents by Status
    Given Documents exists for '<commerceorg>'
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to account summary page
    And customer navigates to cost center '<costcenter>'
    And selects document status '<status>' and clicks on search
    Then All listed documents are in '<status>' status
    Examples:
      | commerceorg               | email                                                     | password | status | costcenter                          |
      | TestAutoCostCenterMgmtOrg | linda.wolf.testautobudgetmgmtorg@sapcc-test.capgemini.com | 12341234 | OPEN   | TestAutoBudgetMgmtOrg Services West |


  Scenario Outline: B2B Admin Filters documents by using filters
    And Customer is authenticated using email '<email>' and password '<password>'
    When customer navigates to account summary page
    And customer navigates to cost center '<costcenter>'
    And selects document status '<status>' and clicks on search
    And filter by  document number '<document>'
    Then listed documents is in '<status>' status with '<document>' document number
    Examples:
      | commerceorg               | email                                                     | password | status | costcenter                          | document                                        |
      | TestAutoCostCenterMgmtOrg | linda.wolf.testautobudgetmgmtorg@sapcc-test.capgemini.com | 12341234 | CLOSED | TestAutoBudgetMgmtOrg Services West | TestAutoBudgetMgmtOrg Services WestPOCR-0000006 |
