@asm @addon
Feature: Assisted Services

  Scenario Outline: AS Agent logs in on portal login page using ASM widget
    Given AS agent at login page
    And navigates to ASM URL
    When agent logs in using username '<username>' and password '<password>'
    Then agent is authenticated
    And agent is on homepage
  Examples:
  | username       | password |
  | asagentmanager | 123456   |
