This archetype is designed for LYONSCG use and provides a maven wrapper around the hybris collection of application.  The folders include:


* custom-ext - folder to contain all custom hybris extensions, this folder is symlinked to hybris/bin/custom     
* ci-ext - extensions and supporting files for the CI build system
* custom-assets - resources and supporting files for the hybris client implementation
* hybris-cis - cis custom extensions and web applications
* hybris-config - hybris configuration files for each environment
* hybris-datahub - custom data hub extensions
* hybris-oms-ext - custom oms extensions
* hybris-suite - hybris commerce suite installation
