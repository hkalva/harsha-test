/**
 * [y] hybris Platform
 * Copyright (c) 2019 Capgemini. All rights reserved.
 *
 * This software is the confidential and proprietary information of Capgemini
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Capgemini.
 */
package com.capgemini.wordpressaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.capgemini.wordpressaddon.model.WordPressPageModel;
import com.capgemini.wordpressservices.data.WordPressPage;
import com.capgemini.wordpressservices.data.WordPressPostsRequest;
import com.capgemini.wordpressservices.exception.WordPressException;
import com.capgemini.wordpressservices.service.WordPressService;


/**
 *
 */
@Controller
public class WordPressHomePageController extends AbstractPageController
{

    /**
     * the logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(WordPressHomePageController.class);

    /**
     * constant for logout.
     */
    private static final String LOGOUT = "logout";

    /**
     * constant for signout confirmation property.
     */
    private static final String ACCOUNT_CONFIRMATION_SIGNOUT_TITLE = "account.confirmation.signout.title";

    /**
     * constant for close property.
     */
    private static final String ACCOUNT_CONFIRMATION_CLOSE_TITLE = "account.confirmation.close.title";

    /**
     * constant for wordpress content attribute.
     */
    private static final String WORD_PRESS_CONTENT = "wordPressContent";

    /**
     * the wordpress service.
     */
    @Resource(name = "wordPressService")
    private WordPressService wordPressService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(@RequestParam(value = WebConstants.CLOSE_ACCOUNT, defaultValue = "false")
    final boolean closeAcc, @RequestParam(value = LOGOUT, defaultValue = "false")
    final boolean logout, final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
    {
        if (logout)
        {
            String message = ACCOUNT_CONFIRMATION_SIGNOUT_TITLE;

            if (closeAcc)
            {
                message = ACCOUNT_CONFIRMATION_CLOSE_TITLE;
            }

            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, message);
            return REDIRECT_PREFIX + ROOT;
        }

        final ContentPageModel contentPage = getContentPageForLabelOrId(null);
        storeCmsPageInModel(model, contentPage);
        setUpMetaDataForContentPage(model, contentPage);
        updatePageTitle(model, contentPage);

        if (contentPage instanceof WordPressPageModel)
        {
            storeWordPressContentInModel((WordPressPageModel) contentPage, model);
        }

        return getViewForPage(model);
    }

    protected void updatePageTitle(final Model model, final AbstractPageModel cmsPage)
    {
        storeContentPageTitleInModel(model, getPageTitleResolver().resolveHomePageTitle(cmsPage.getTitle()));
    }

    protected void storeWordPressContentInModel(final WordPressPageModel page, final Model model)
    {
        //set the slug to the default slug of the page
        final String slug = page.getDefaultSlug();

        try
        {
            final WordPressPostsRequest wordPressPostsRequest = new WordPressPostsRequest();
            wordPressPostsRequest.setSlug(slug);
            final WordPressPage wpPage = wordPressService.getPage(wordPressPostsRequest, true, false);
            final String wordPressContent = wpPage.getContent();
            model.addAttribute(WORD_PRESS_CONTENT, StringUtils.trimToEmpty(wordPressContent));

            if (StringUtils.isNotBlank(wpPage.getTitle()))
            {
                storeContentPageTitleInModel(model, getPageTitleResolver().resolveHomePageTitle(wpPage.getTitle()));
            }
        }
        catch (final WordPressException e)
        {
            LOG.error("Error retrieving Word Press content", e);
        }
    }

}
