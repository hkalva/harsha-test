/**
 * [y] hybris Platform
 * Copyright (c) 2019 Capgemini. All rights reserved.
 *
 * This software is the confidential and proprietary information of Capgemini
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Capgemini.
 */
package com.capgemini.wordpressaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UrlPathHelper;

import com.capgemini.wordpressaddon.model.WordPressPageModel;
import com.capgemini.wordpressservices.data.WordPressPage;
import com.capgemini.wordpressservices.data.WordPressPostsRequest;
import com.capgemini.wordpressservices.exception.WordPressException;
import com.capgemini.wordpressservices.service.WordPressService;


/**
 * @author lyonscg
 *
 *         Overrides the storefront's DefaultPageController. It will handle both Word Press pages and regular content
 *         pages
 */
@Controller
public class WordPressDefaultPageController extends AbstractPageController
{

    /**
     * the logger.
     */
    private static final Logger LOG = Logger.getLogger(WordPressDefaultPageController.class);

    /**
     * constant for not found cms page uid.
     */
    private static final String ERROR_CMS_PAGE = "notFound";

    /**
     * constant for wordpress content attribute.
     */
    private static final String WORD_PRESS_CONTENT = "wordPressContent";

    /**
     * constant for path separator.
     */
    private static final String PATH_SEPARATOR = "/";

    /**
     * the url path helper.
     */
    private final UrlPathHelper urlPathHelper = new UrlPathHelper();

    /**
     * the breadcrumb builder.
     */
    @Resource(name = "simpleBreadcrumbBuilder")
    private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

    /**
     * the content page breadcrumb builder.
     */
    @Resource(name = "contentPageBreadcrumbBuilder")
    private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

    /**
     * the wordpress service.
     */
    @Resource(name = "wordPressService")
    private WordPressService wordPressService;

    @RequestMapping(method = RequestMethod.GET)
    public String getContentPage(final Model model, final HttpServletRequest request, final HttpServletResponse response)
            throws CMSItemNotFoundException
    {
        // Check for CMS Page where label or id is like /page
        final ContentPageModel pageForRequest = getContentPageForRequest(request);

        if (pageForRequest != null)
        {
            storeCmsPageInModel(model, pageForRequest);
            setUpMetaDataForContentPage(model, pageForRequest);
            model.addAttribute(WebConstants.BREADCRUMBS_KEY, contentPageBreadcrumbBuilder.getBreadcrumbs(pageForRequest));

            if (pageForRequest instanceof WordPressPageModel)
            {
                try
                {
                    storeWordPressContentInModel((WordPressPageModel) pageForRequest, model, request);
                }
                catch (final WordPressException e)
                {
                    LOG.error("Error retrieving Word Press content", e);
                    request.setAttribute("message", e.getMessage());
                    return FORWARD_PREFIX + "/404";
                }
            }

            return getViewForPage(pageForRequest);
        }

        final ContentPageModel notFoundPage = getContentPageForLabelOrId(ERROR_CMS_PAGE);
        // No page found - display the notFound page with error from controller
        storeCmsPageInModel(model, notFoundPage);
        setUpMetaDataForContentPage(model, notFoundPage);

        model.addAttribute(WebConstants.MODEL_KEY_ADDITIONAL_BREADCRUMB,
                resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.not.found"));
        GlobalMessages.addErrorMessage(model, "system.error.page.not.found");

        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        return getViewForPage(notFoundPage);
    }

    /**
     * Lookup the CMS Content Page for this request.
     *
     * @param request
     *            The request
     * @return the CMS content page
     */
    protected ContentPageModel getContentPageForRequest(final HttpServletRequest request)
    {
        // Get the path for this request.
        // Note that the path begins with a '/'
        String lookupPathForRequest = urlPathHelper.getLookupPathForRequest(request);

        try
        {
            // Lookup the CMS Content Page by label. Note that the label value must begin with a '/'.
            return getCmsPageService().getPageForLabel(lookupPathForRequest);
        }
        catch (final CMSItemNotFoundException ignore)
        {
            //if page is not found and the request has more than 1 path level,
            //grab the first level path and use that to get the content page
            final String path = urlPathHelper.getPathWithinApplication(request);
            final String[] paths = StringUtils.split(path, PATH_SEPARATOR);

            if ((paths != null) && paths.length > 0)
            {
                lookupPathForRequest = PATH_SEPARATOR + paths[0];

                try
                {
                    return getCmsPageService().getPageForLabel(lookupPathForRequest);
                }
                catch (final CMSItemNotFoundException e)
                {
                    //ignore
                }
            }
        }

        return null;
    }

    protected void storeWordPressContentInModel(final WordPressPageModel page, final Model model,
            final HttpServletRequest request)
    {
        //set the slug to the default slug of the page
        String slug = page.getDefaultSlug();
        final String path = urlPathHelper.getPathWithinApplication(request);
        final String[] paths = StringUtils.split(path, PATH_SEPARATOR);

        //if the request has more than 1 path level, use the last level as the slug
        if ((paths != null) && paths.length > 1)
        {
            slug = paths[paths.length - 1];
        }

        final WordPressPostsRequest wordPressPostsRequest = new WordPressPostsRequest();
        wordPressPostsRequest.setSlug(slug);
        final WordPressPage wpPage = wordPressService.getPage(wordPressPostsRequest, true, true);
        final String wordPressContent = wpPage.getContent();
        model.addAttribute(WORD_PRESS_CONTENT, StringUtils.trimToEmpty(wordPressContent));

        if (StringUtils.isNotBlank(wpPage.getTitle()))
        {
            storeContentPageTitleInModel(model, getPageTitleResolver().resolveContentPageTitle(wpPage.getTitle()));
        }

    }

}
