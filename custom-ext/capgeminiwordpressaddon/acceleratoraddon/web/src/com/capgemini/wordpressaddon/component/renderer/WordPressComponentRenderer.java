/**
 * [y] hybris Platform
 * Copyright (c) 2019 Capgemini. All rights reserved.
 *
 * This software is the confidential and proprietary information of Capgemini
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Capgemini.
 */
package com.capgemini.wordpressaddon.component.renderer;

import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.capgemini.wordpressaddon.model.WordPressComponentModel;
import com.capgemini.wordpressservices.data.WordPressPostsRequest;
import com.capgemini.wordpressservices.exception.WordPressException;
import com.capgemini.wordpressservices.service.WordPressService;


/**
 * @author lyonscg
 *
 */
public class WordPressComponentRenderer<C extends WordPressComponentModel> extends DefaultAddOnCMSComponentRenderer<C>
{

    private static final Logger LOG = Logger.getLogger(WordPressComponentRenderer.class);

    private static final String WORD_PRESS_CONTENT = "wordPressContent";

    private WordPressService wordPressService;

    @Override
    public void renderComponent(final PageContext pageContext, final C component) throws ServletException, IOException
    {
        final WordPressPostsRequest wordPressPostsRequest = new WordPressPostsRequest();
        wordPressPostsRequest.setSlug(component.getSlug());
        String wordPressContent = null;

        try
        {
            wordPressContent = wordPressService.getContent(wordPressPostsRequest, true);
        }
        catch (final WordPressException e)
        {
            LOG.error(String.format("Error getting Word Press content for slug '%s'", component.getSlug()), e);
        }

        final StringBuilder moduleView = new StringBuilder();
        moduleView.append("/WEB-INF/views/addons/");
        moduleView.append(getAddonUiExtensionName(component)).append("/");
        moduleView.append(getUIExperienceFolder());
        moduleView.append("/cms/wordpresscomponent.jsp");
        final Map<String, Object> exposedVariables = exposeVariables(pageContext, component);
        exposeContent(pageContext, exposedVariables, StringUtils.trimToEmpty(wordPressContent));
        pageContext.include(moduleView.toString());
        unExposeVariables(pageContext, component, exposedVariables);
    }

    @Required
    public void setWordPressService(final WordPressService wordPressService)
    {
        this.wordPressService = wordPressService;
    }

    protected WordPressService getWordPressService()
    {
        return wordPressService;
    }

    protected void exposeContent(final PageContext pageContext, final Map<String, Object> exposedVariables, final String content)
    {
        exposedVariables.put(WORD_PRESS_CONTENT, StringUtils.trimToEmpty(content));
        pageContext.setAttribute(WORD_PRESS_CONTENT, StringUtils.trimToEmpty(content),
                getScopeForVariableName(WORD_PRESS_CONTENT));
    }
}
