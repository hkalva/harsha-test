/**
 * [y] hybris Platform
 * Copyright (c) 2019 Capgemini. All rights reserved.
 *
 * This software is the confidential and proprietary information of Capgemini
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Capgemini.
 */
package com.capgemini.wordpressaddon.interceptors.beforeview;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.ModelAndView;


/**
 *
 */
public class WordPressBeforeViewHandler implements BeforeViewHandler
{

    /**
     * the cms site service.
     */
    private CMSSiteService cmsSiteService;

    /*
     * (non-Javadoc)
     *
     * @see
     * de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler#beforeView(javax.servlet.http.
     * HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.web.servlet.ModelAndView)
     */
    @Override
    public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView)
            throws Exception
    {
        final CMSSiteModel site = getCmsSiteService().getCurrentSite();
        modelAndView.addObject("wordpressCSS", site.getWordpressCSS());
        modelAndView.addObject("wordpressJS", site.getWordpressJS());
    }

    @Required
    public void setCmsSiteService(final CMSSiteService cmsSiteService)
    {
        this.cmsSiteService = cmsSiteService;
    }

    protected CMSSiteService getCmsSiteService()
    {
        return cmsSiteService;
    }
}
