<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--  WordPress CSS files --%>
<c:if test="${not empty wordpressCSS}">
	<c:forEach items="${wordpressCSS}" var="wordpressCSSUrl">
		<link rel="stylesheet" type="text/css" media="all" href="${wordpressCSSUrl}"/>
	</c:forEach>
</c:if>
