<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--  WordPress CSS files --%>
<c:if test="${not empty wordpressJS}">
	<c:forEach items="${wordpressJS}" var="wordpressJSUrl">
		<script type="text/javascript" src="${wordpressJSUrl}"></script>
	</c:forEach>
</c:if>
