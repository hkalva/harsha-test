/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.productcompare.facades.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.ConfigurablePopulator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.capgemini.productcompare.facades.constants.ProductCompareConstants;
import com.capgemini.services.exception.ProductCompareException;


/**
 *
 */
@UnitTest
public class ProductCompareFacadeImplTest
{

    /**
     * the facade to test.
     */
    private ProductCompareFacadeImpl facade;

    /**
     * the product compare options.
     */
    @Mock
    private Collection<ProductOption> productCompareOptions;

    /**
     * the product configured poopulator.
     */
    @Mock
    private ConfigurablePopulator<ProductModel, ProductData, ProductOption> productConfiguredPopulator;

    /**
     * the configuration service.
     */
    @Mock
    private ConfigurationService configurationService;

    /**
     * the configuration.
     */
    @Mock
    private Configuration configuration;

    /**
     * the session service.
     */
    @Mock
    private SessionService sessionService;

    /**
     * the product facade.
     */
    @Mock
    private ProductFacade productFacade;

    /**
     * the product service.
     */
    @Mock
    private ProductService productService;

    /**
     * the product.
     */
    @Mock
    private ProductModel product;

    /**
     * the map to mimic session service.
     */
    private Map<String, Object> map;

    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        map = new HashMap<>();
        facade = new ProductCompareFacadeImpl();
        facade.setConfigurationService(configurationService);
        facade.setProductCompareOptions(productCompareOptions);
        facade.setProductConfiguredPopulator(productConfiguredPopulator);
        facade.setProductFacade(productFacade);
        facade.setProductService(productService);
        facade.setSessionService(sessionService);
        Mockito.when(configurationService.getConfiguration()).thenReturn(configuration);
        Mockito.when(configuration.getInt(ProductCompareConstants.PRODUCTCOMPAREMAXSELECTEDCONFIG))
                .thenReturn(Integer.valueOf(4));

        Mockito.doAnswer(new Answer() {

            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                map.put(invocation.getArgumentAt(0, String.class), invocation.getArgumentAt(1, Object.class));
                return null;
            }

        }).when(sessionService).setAttribute(Mockito.anyString(), Mockito.anyObject());

        Mockito.doAnswer(new Answer() {

            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                map.remove(invocation.getArgumentAt(0, String.class));
                return null;
            }

        }).when(sessionService).removeAttribute(Mockito.anyString());

        Mockito.doAnswer(new Answer() {

            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                return map.get(invocation.getArgumentAt(0, String.class));
            }

        }).when(sessionService).getAttribute(Mockito.anyString());

        Mockito.when(productFacade.getProductForCodeAndOptions(Mockito.anyString(), Mockito.anyCollection()))
                .thenAnswer(new Answer<ProductData>()
                {

                    @Override
                    public ProductData answer(final InvocationOnMock invocation) throws Throwable
                    {
                        final ProductData result = new ProductData();
                        result.setCode(invocation.getArgumentAt(0, String.class));
                        return result;
                    }

                });

        Mockito.when(productService.getProductForCode(Mockito.anyString())).thenAnswer(new Answer<ProductModel>() {

            @Override
            public ProductModel answer(final InvocationOnMock invocation) throws Throwable
            {
                Mockito.when(product.getCode()).thenReturn(invocation.getArgumentAt(0, String.class));
                return product;
            }

        });

    }

    /**
     * test getSessionProductCompare.
     */
    @Test
    public void testGetSessionProductCompare()
    {
        List<String> result = facade.getSessionProductCompare();
        Assert.assertTrue(CollectionUtils.isEmpty(result));
        map.put(ProductCompareConstants.PRODUCTCOMPARELISTSESSIONKEY, Collections.singletonList("test"));
        result = facade.getSessionProductCompare();
        Assert.assertTrue(CollectionUtils.isNotEmpty(result));
    }

    /**
     * test addSessionProductCompare maxed.
     */
    @Test(expected = ProductCompareException.class)
    public void testAddSessionProductCompareMaxed()
    {
        map.put(ProductCompareConstants.PRODUCTCOMPARELISTSESSIONKEY, Arrays.asList("code1", "code2", "code3", "code4"));
        facade.addSessionProductCompare("code5");
    }

    /**
     * test addSessionProductCompare exists.
     */
    @Test
    public void testAddSessionProductCompareExists()
    {
        map.put(ProductCompareConstants.PRODUCTCOMPARELISTSESSIONKEY, Arrays.asList("code11", "code12", "code13"));
        final boolean result = facade.addSessionProductCompare("code13");
        Assert.assertFalse(result);
        Mockito.verify(sessionService, Mockito.times(0)).setAttribute(Mockito.anyString(), Mockito.any(List.class));
    }

    /**
     * test addSessionProductCompare not exists.
     */
    @Test
    public void testAddSessionProductCompareNotExists()
    {
        map.put(ProductCompareConstants.PRODUCTCOMPARELISTSESSIONKEY, Arrays.asList("code10", "code20", "code30"));
        Mockito.when(product.getCode()).thenReturn("code40");
        final boolean result = facade.addSessionProductCompare("code40");
        Assert.assertTrue(result);
        Mockito.verify(sessionService, Mockito.times(1)).setAttribute(Mockito.anyString(), Mockito.any(List.class));
    }

    /**
     * test removeSessionProductCompare.
     */
    @Test
    public void testRemoveSessionProductCompare()
    {
        map.put(ProductCompareConstants.PRODUCTCOMPARELISTSESSIONKEY, Arrays.asList("code6", "code7", "code8"));
        final boolean result = facade.removeSessionProductCompare("code6");
        Assert.assertTrue(result);
        Mockito.verify(sessionService, Mockito.times(1)).setAttribute(Mockito.anyString(), Mockito.any(List.class));
    }

    /**
     * test clearSessionProductCompare.
     */
    @Test
    public void testClearSessionProductCompare()
    {
        facade.clearSessionProductCompare();
        Mockito.verify(sessionService, Mockito.times(1)).removeAttribute(ProductCompareConstants.PRODUCTCOMPARELISTSESSIONKEY);
    }

    /**
     * test addProductCompare and removeProductCompare.
     */
    @Test
    public void testAddAndRemoveProductCompare()
    {
        List<ProductData> result = facade.addProductCompare("code123");
        Assert.assertTrue(CollectionUtils.size(result) == 1);
        result = facade.addProductCompare("code123");
        Assert.assertTrue(CollectionUtils.size(result) == 1);
        result = facade.addProductCompare("code456");
        Assert.assertTrue(CollectionUtils.size(result) == 2);
        result = facade.removeProductCompare("code789");
        Assert.assertTrue(CollectionUtils.size(result) == 2);
        result = facade.removeProductCompare("code123");
        Assert.assertTrue(CollectionUtils.size(result) == 1);
    }

    /**
     * test clearProductCompare.
     */
    @Test
    public void testClearProductCompare()
    {
        final List<ProductData> result = facade.clearProductCompare();
        Assert.assertTrue(CollectionUtils.isEmpty(result));
    }
}
