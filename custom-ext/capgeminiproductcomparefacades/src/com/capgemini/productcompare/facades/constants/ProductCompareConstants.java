/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.productcompare.facades.constants;

/**
 *
 */
public final class ProductCompareConstants
{

	public static final String PRODUCTCOMPARELISTSESSIONKEY = "productCompare";

	public static final String PRODUCTCOMPAREMAXSELECTEDCONFIG = "capgeminiproductcomparefacades.product.compare.max.selected";

}
