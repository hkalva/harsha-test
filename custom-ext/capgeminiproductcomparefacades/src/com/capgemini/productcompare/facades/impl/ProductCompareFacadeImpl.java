/*
* Copyright (c) 2019 Capgemini. All rights reserved.
*
* This software is the confidential and proprietary information of Capgemini
* ("Confidential Information"). You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the
* license agreement you entered into with Capgemini.
*/
package com.capgemini.productcompare.facades.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.ConfigurablePopulator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.capgemini.productcompare.facades.ProductCompareFacade;
import com.capgemini.productcompare.facades.constants.ProductCompareConstants;
import com.capgemini.services.exception.ProductCompareException;


/**
 * Help us this class make the operations like add ,remove ,clear and get the list of products for product compare.
 */
public class ProductCompareFacadeImpl implements ProductCompareFacade
{
    /**
     * the product compare options.
     */
    private Collection<ProductOption> productCompareOptions;

    /**
     * the product configured poopulator.
     */
    private ConfigurablePopulator<ProductModel, ProductData, ProductOption> productConfiguredPopulator;

    /**
     * the configuration service.
     */
    private ConfigurationService configurationService;

    /**
     * the session service.
     */
    private SessionService sessionService;

    /**
     * the product facade.
     */
    private ProductFacade productFacade;

    /**
     * the product service.
     */
    private ProductService productService;

    /**
     * Retrieve currently selected product codes for product compare from session. Return an empty list, if session
     * attribute is empty
     */
    @Override
    public List<String> getSessionProductCompare()
    {
        final List<String> productCompare = getSessionService()
                .getAttribute(ProductCompareConstants.PRODUCTCOMPARELISTSESSIONKEY);

        if (CollectionUtils.isNotEmpty(productCompare))
        {
            return productCompare;
        }

        return Collections.emptyList();
    }

    /**
     * This method perform the adding the products into list
     */
    @Override
    public boolean addSessionProductCompare(final String productCode) throws ProductCompareException
    {
        //Retrieve the currently selected items from session:
        final List<String> productCompare = getSessionProductCompare();

        //Check whether list has reached max size:
        if (productCompare.size() >= getConfigurationService().getConfiguration()
                .getInt(ProductCompareConstants.PRODUCTCOMPAREMAXSELECTEDCONFIG))
        {
            throw new ProductCompareException("Maximum product compare selections exceeded.");
        }

        //Check whether product exists in selected items, if not, validate product exists and add it to the selected list (product service will
        final List<String> updatedList = new ArrayList<>(productCompare);

        if (updatedList.stream().noneMatch(code -> code.trim().equals(productCode)))
        {
            final ProductModel productModel = getProductService().getProductForCode(productCode);

            if (null != productModel)
            {
                updatedList.add(productModel.getCode());
                getSessionService().setAttribute(ProductCompareConstants.PRODUCTCOMPARELISTSESSIONKEY, updatedList);
                return true;
            }
        }

        return false;
    }

    /**
     * remove the product from the session if product is not empty
     */
    @Override
    public boolean removeSessionProductCompare(final String productCode)
    {
        //Retrieve the currently selected items from session, filtering out the provided productCode:
        final List<String> productCompareFiltered = getSessionProductCompare().stream()
                .filter(remove -> !remove.equals(productCode.trim())).collect(Collectors.toList());
        getSessionService().setAttribute(ProductCompareConstants.PRODUCTCOMPARELISTSESSIONKEY, productCompareFiltered);
        return true;
    }

    /**
     * This method perform the clearing the compare list
     */
    @Override
    public void clearSessionProductCompare()
    {
        // Clear the session attribute for product compare:
        getSessionService().removeAttribute(ProductCompareConstants.PRODUCTCOMPARELISTSESSIONKEY);
    }

    @Override
    public List<ProductData> addProductCompare(final String productCode) throws ProductCompareException
    {
        //Validate the required fields are populated
        validateParameterNotNullStandardMessage("productCode", productCode);
        Assert.hasText(productCode, "The field [productCode] cannot be empty");

        //Add the item to the comparison selection in the session
        addSessionProductCompare(productCode);
        return getProductCompare();
    }

    /**
     * remove the product from productCompare
     */

    @Override
    public List<ProductData> removeProductCompare(final String productCode) throws ProductCompareException
    {
        //Validate the required fields are populated
        validateParameterNotNullStandardMessage("productCode", productCode);
        Assert.hasText(productCode, "The field [productCode] cannot be empty");

        //Remove the provided product code from the selected compare items:
        removeSessionProductCompare(productCode);
        return getProductCompare();
    }

    @Override
    public List<ProductData> clearProductCompare() throws ProductCompareException
    {
        //
        clearSessionProductCompare();
        return Collections.<ProductData> emptyList();
    }

    @Override
    public List<ProductData> getProductCompare() throws ProductCompareException
    {
        final List<String> productCompareCodes = getSessionProductCompare();

        if (CollectionUtils.isEmpty(productCompareCodes))
        {
            return Collections.emptyList();
        }

        final List<ProductData> compareData = new ArrayList<>();

        for (final String productCode : productCompareCodes)
        {
            final ProductData productData = getProductFacade().getProductForCodeAndOptions(productCode,
                    getProductCompareOptions());
            compareData.add(productData);
        }

        return compareData;
    }

    /**
     * @return the productCompareOptions
     */
    public Collection<ProductOption> getProductCompareOptions()
    {
        return productCompareOptions;
    }

    /**
     * @param productCompareOptions
     *            the productCompareOptions to set
     */
    @Required
    public void setProductCompareOptions(final Collection<ProductOption> productCompareOptions)
    {
        this.productCompareOptions = productCompareOptions;
    }

    /**
     * @return the productConfiguredPopulator
     */
    public ConfigurablePopulator<ProductModel, ProductData, ProductOption> getProductConfiguredPopulator()
    {
        return productConfiguredPopulator;
    }

    /**
     * @param productConfiguredPopulator
     *            the productConfiguredPopulator to set
     */
    @Required
    public void setProductConfiguredPopulator(
            final ConfigurablePopulator<ProductModel, ProductData, ProductOption> productConfiguredPopulator)
    {
        this.productConfiguredPopulator = productConfiguredPopulator;
    }

    /**
     * @return the configurationService
     */
    public ConfigurationService getConfigurationService()
    {
        return configurationService;
    }

    /**
     * @param configurationService
     *            the configurationService to set
     */
    @Required
    public void setConfigurationService(final ConfigurationService configurationService)
    {
        this.configurationService = configurationService;
    }

    /**
     * @return the sessionService
     */
    public SessionService getSessionService()
    {
        return sessionService;
    }

    /**
     * @param sessionService
     *            the sessionService to set
     */
    @Required
    public void setSessionService(final SessionService sessionService)
    {
        this.sessionService = sessionService;
    }

    /**
     * @return the productFacade
     */
    public ProductFacade getProductFacade()
    {
        return productFacade;
    }

    /**
     * @param productFacade
     *            the productFacade to set
     */
    @Required
    public void setProductFacade(final ProductFacade productFacade)
    {
        this.productFacade = productFacade;
    }

    /**
     * @return the productService
     */
    public ProductService getProductService()
    {
        return productService;
    }

    /**
     * @param productService
     *            the productService to set
     */
    @Required
    public void setProductService(final ProductService productService)
    {
        this.productService = productService;
    }

}
