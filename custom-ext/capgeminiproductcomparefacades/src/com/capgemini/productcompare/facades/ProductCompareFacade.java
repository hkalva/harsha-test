/*
* Copyright (c) 2019 Capgemini. All rights reserved.
*
* This software is the confidential and proprietary information of Capgemini
* ("Confidential Information"). You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the
* license agreement you entered into with Capgemini.
*/
package com.capgemini.productcompare.facades;

import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.List;

import com.capgemini.services.exception.ProductCompareException;


/**
 * This class help us to make the operations like add , remove and clear the product compare operations
 */
public interface ProductCompareFacade
{

    /**
     * Return the list of product codes from the session.
     *
     * @return List<String>
     */
    List<String> getSessionProductCompare();

    /**
     * Adding the product into product compare if it is invalid throw error accordingly.
     *
     * @param productCode
     *            - the product code.
     *
     * @return boolean
     *
     * @throws ProductCompareException
     *             - the product compare exception.
     */
    boolean addSessionProductCompare(String productCode) throws ProductCompareException;

    /**
     * Remove the desired product from the product compare list.
     *
     * @param productCode
     *            - the product code.
     *
     * @return boolean
     */
    boolean removeSessionProductCompare(String productCode);

    /**
     * Clear all the products from the product compare list.
     */
    void clearSessionProductCompare();

    /**
     * Adding the product into product compare list and also verify max limit validation and invalid product as well.
     *
     * @param productCode
     *            - the product code.
     *
     * @return List<ProductData>
     *
     * @throws ProductCompareException
     *             - the product compare exception.
     */
    List<ProductData> addProductCompare(String productCode) throws ProductCompareException;

    /**
     * Remove the product from the product compare list.
     *
     * @param productCode
     *            - the product code.
     *
     * @return List<ProductData>
     *
     * @throws ProductCompareException
     *             - the product compare exception.
     */
    List<ProductData> removeProductCompare(String productCode) throws ProductCompareException;

    /**
     * Clear all the products from the product compare list.
     *
     * @return List<ProductData>
     *
     * @throws ProductCompareException
     *             - the product compare exception.
     */
    List<ProductData> clearProductCompare() throws ProductCompareException;

    /**
     * Returns the list of products.
     *
     * @return List<ProductData>
     *
     * @throws ProductCompareException
     *             - the product compare exception.
     */
    List<ProductData> getProductCompare() throws ProductCompareException;

}
