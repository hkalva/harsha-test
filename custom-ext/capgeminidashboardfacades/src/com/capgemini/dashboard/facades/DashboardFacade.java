/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.dashboard.facades;

import java.util.List;

import com.capgemini.dashboard.facades.data.DashboardBalanceData;
import com.capgemini.dashboard.facades.data.DashboardBudgetData;
import com.capgemini.dashboard.facades.data.DashboardOrderData;
import com.capgemini.dashboard.facades.data.DashboardQuoteData;


/**
 * Dashboard Facade definition.
 */
public interface DashboardFacade
{
	/**
	 * get orders based on count value.
	 *
	 * @param count
	 *           - the count size.
	 * @return List<DashboardOrderData>
	 */
	List<DashboardOrderData> getOrders(int count);

	/**
	 * get quotes based on count value.
	 *
	 * @param count
	 *           - the count size.
	 * @return List<DashboardQuoteData>
	 */
	List<DashboardQuoteData> getQuotes(int count);

	/**
	 * get budgets.
	 * 
	 * @return List<DashboardBudgetData
	 */
	List<DashboardBudgetData> getBudgets();
	
	/**
	 * get balances.
	 * 
	 * @return List<DashboardBalanceData>
	 */
	List<DashboardBalanceData> getBalances();
}
