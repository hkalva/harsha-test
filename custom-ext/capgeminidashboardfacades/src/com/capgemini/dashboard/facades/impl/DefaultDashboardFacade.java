/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.dashboard.facades.impl;


import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.capgemini.dashboard.facades.DashboardFacade;
import com.capgemini.dashboard.facades.data.DashboardBalanceData;
import com.capgemini.dashboard.facades.data.DashboardBudgetData;
import com.capgemini.dashboard.facades.data.DashboardOrderData;
import com.capgemini.dashboard.facades.data.DashboardQuoteData;
import com.capgemini.dashboard.services.DashboardService;

import de.hybris.platform.b2b.model.B2BCostCenterModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.order.strategies.QuoteUserIdentificationStrategy;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.servicelayer.data.PaginationData;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.services.BaseStoreService;


/**
 * Default implementation of DashboardFacade.
 */
public class DefaultDashboardFacade implements DashboardFacade
{

    /**
     * the base store service.
     */
    private BaseStoreService baseStoreService;
    
    /**
     * the dashboard service.
     */
    private DashboardService dashboardService;

    /**
     * the user service.
     */
    private UserService userService;
    
    /**
     * the common i18n service.
     */
    private CommonI18NService commonI18NService;
    
    /**
     * the quote user identification strategy.
     */
    private QuoteUserIdentificationStrategy quoteUserIdentificationStrategy;
    
    /**
     * the dashboard order data converter.
     */
    private Converter<OrderModel, DashboardOrderData> dashboardOrderDataConverter;

    /**
     * the dashboard quote data converter.
     */
    private Converter<QuoteModel, DashboardQuoteData> dashboardQuoteDataConverter;

    /**
     * the dashboard budget data converter.
     */
    private Converter<B2BCostCenterModel, DashboardBudgetData> dashboardBudgetDataConverter;

    /**
     * the dashboard balance data converter.
     */
    private Converter<B2BUnitModel, DashboardBalanceData> dashboardBalanceDataConverter;
    
	@Override
	public List<DashboardOrderData> getOrders(final int count) 
	{
        final PaginationData pagination = new PaginationData();
        pagination.setPageSize(count);
        final SearchPageData<OrderModel> searchPageData = new SearchPageData<>();
        searchPageData.setPagination(pagination);
        SearchPageData<OrderModel> result = getDashboardService().getOrders((CustomerModel) getUserService().getCurrentUser(), 
                getBaseStoreService().getCurrentBaseStore(), searchPageData);
        
        if (CollectionUtils.isEmpty(result.getResults()))
        {
            return Collections.emptyList();
        }

        return Converters.convertAll(result.getResults(), getDashboardOrderDataConverter());
	}

	@Override
	public List<DashboardQuoteData> getQuotes(final int count)
	{
        final PaginationData pagination = new PaginationData();
        pagination.setPageSize(count);
        final SearchPageData<QuoteModel> searchPageData = new SearchPageData<>();
        searchPageData.setPagination(pagination);
        SearchPageData<QuoteModel> result = getDashboardService().getQuotes((CustomerModel) getUserService().getCurrentUser(), 
                getQuoteUserIdentificationStrategy().getCurrentQuoteUser(), getBaseStoreService().getCurrentBaseStore(), 
                searchPageData);
        
        if (CollectionUtils.isEmpty(result.getResults()))
        {
            return Collections.emptyList();
        }

        return Converters.convertAll(result.getResults(), getDashboardQuoteDataConverter());
	}

	@Override
	public List<DashboardBudgetData> getBudgets()
	{
        List<B2BCostCenterModel> costCenters = getDashboardService().getCostCenters(getCommonI18NService().getCurrentCurrency());
        
        if (CollectionUtils.isEmpty(costCenters))
        {
            return Collections.emptyList();
        }
        
        return Converters.convertAll(costCenters, getDashboardBudgetDataConverter());
	}

    @Override
    public List<DashboardBalanceData> getBalances()
    {
        Set<B2BUnitModel> units = getDashboardService().getUnits((B2BCustomerModel) getUserService().getCurrentUser());
        
        if (CollectionUtils.isEmpty(units))
        {
            return Collections.emptyList();
        }
        
        return Converters.convertAll(units, getDashboardBalanceDataConverter());
    }
	
	/**
	 * 
	 * @param baseStoreService - the base storeservice.
	 */
	@Required
	public void setBaseStoreService(BaseStoreService baseStoreService)
	{
	    this.baseStoreService = baseStoreService;
	}

    /**
     * 
     * @param dashboardService - the dashboard service.
     */
    @Required
    public void setDashboardService(DashboardService dashboardService)
    {
        this.dashboardService = dashboardService;
    }

    /**
     * 
     * @param userService - the user service.
     */
    @Required
    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }

    /**
     * 
     * @param commonI18NService - the common i18n service.
     */
    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService)
    {
        this.commonI18NService = commonI18NService;
    }
    
    /**
     * 
     * @param quoteUserIdentificationStrategy - the quote user identification strategy.
     */
    @Required
    public void setQuoteUserIdentificationStrategy(final QuoteUserIdentificationStrategy quoteUserIdentificationStrategy)
    {
        this.quoteUserIdentificationStrategy = quoteUserIdentificationStrategy;
    }
    
    /**
     * 
     * @param dashboardOrderDataConverter - the dashboard order data converter.
     */
    @Required
    public void setDashboardOrderDataConverter(Converter<OrderModel, DashboardOrderData> dashboardOrderDataConverter)
    {
        this.dashboardOrderDataConverter = dashboardOrderDataConverter;
    }
    
    /**
     * 
     * @param dashboardQuoteDataConverter - the dashboard quote data converter.
     */
    @Required
    public void setDashboardQuoteDataConverter(Converter<QuoteModel, DashboardQuoteData> dashboardQuoteDataConverter)
    {
        this.dashboardQuoteDataConverter = dashboardQuoteDataConverter;
    }
    
    /**
     * 
     * @param dashboardBudgetDataConverter - the dashboard budget data converter.
     */
    @Required
    public void setDashboardBudgetDataConverter(Converter<B2BCostCenterModel, DashboardBudgetData> dashboardBudgetDataConverter)
    {
        this.dashboardBudgetDataConverter = dashboardBudgetDataConverter;
    }
    
    /**
     * 
     * @param dashboardBalanceDataConverter - the dashboard balance data converter.
     */
    @Required
    public void setDashboardBalanceDataConverter(Converter<B2BUnitModel, DashboardBalanceData> dashboardBalanceDataConverter)
    {
        this.dashboardBalanceDataConverter = dashboardBalanceDataConverter;
    }
    
	/**
	 * 
	 * @return BaseStoreService
	 */
	protected BaseStoreService getBaseStoreService()
	{
	    return baseStoreService;
	}

    /**
     * 
     * @return DashboardService
     */
    protected DashboardService getDashboardService()
    {
        return dashboardService;
    }

    /**
     * 
     * @return UserService
     */
    protected UserService getUserService()
    {
        return userService;
    }
    
    /**
     * 
     * @return CommonI18NService
     */
    protected CommonI18NService getCommonI18NService()
    {
        return commonI18NService;
    }
    
    /**
     * 
     * @return QuoteUserIdentificationStrategy
     */
    protected QuoteUserIdentificationStrategy getQuoteUserIdentificationStrategy()
    {
        return quoteUserIdentificationStrategy;
    }

    /**
     * 
     * @return Converter<OrderModel, DashboardOrderData>
     */
    protected Converter<OrderModel, DashboardOrderData> getDashboardOrderDataConverter()
    {
        return dashboardOrderDataConverter;
    }

    /**
     * 
     * @return Converter<QuoteModel, DashboardQuoteData>
     */
    protected Converter<QuoteModel, DashboardQuoteData> getDashboardQuoteDataConverter()
    {
        return dashboardQuoteDataConverter;
    }
    
    /**
     * 
     * @return Converter<B2BCostCenterModel, DashboardBudgetData>
     */
    protected Converter<B2BCostCenterModel, DashboardBudgetData> getDashboardBudgetDataConverter()
    {
        return dashboardBudgetDataConverter;
    }
    
    /**
     * 
     * @return Converter<B2BUnitModel, DashboardBalanceData>
     */
    protected Converter<B2BUnitModel, DashboardBalanceData> getDashboardBalanceDataConverter()
    {
        return dashboardBalanceDataConverter;
    }
}
