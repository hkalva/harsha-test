/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.dashboard.facades.converters.populator;

import com.capgemini.dashboard.facades.data.DashboardQuoteData;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.QuoteModel;

public class DashboardQuoteDataPopulator implements Populator<QuoteModel, DashboardQuoteData>
{

    @Override
    public void populate(QuoteModel source, DashboardQuoteData target)
    {
        target.setCode(source.getCode());
        target.setStatus(source.getState() == null ? null : source.getState().getCode());
    }

}
