/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.dashboard.facades.converters.populator;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.capgemini.dashboard.facades.data.DashboardBudgetData;

import de.hybris.platform.b2b.model.B2BBudgetModel;
import de.hybris.platform.b2b.model.B2BCostCenterModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.services.B2BCostCenterService;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


public class DashboardBudgetDataPopulator implements Populator<B2BCostCenterModel, DashboardBudgetData>
{

    /**
     * the b2b cost center service.
     */
    private B2BCostCenterService<B2BCostCenterModel, B2BCustomerModel> b2bCostCenterService;

    /**
     * the price data factory.
     */
    private PriceDataFactory priceDataFactory;

    @Override
    public void populate(B2BCostCenterModel source, DashboardBudgetData target) throws ConversionException
    {
        target.setCostCenterCode(source.getCode());
        target.setCostCenterName(source.getName());
        BigDecimal totalBudget = BigDecimal.ZERO;
        BigDecimal totalSpent = BigDecimal.ZERO;
        BigDecimal remainingBudget = BigDecimal.ZERO;

        if (CollectionUtils.isNotEmpty(source.getBudgets()))
        {
            Date startDate = Calendar.getInstance().getTime();
            Date endDate = Calendar.getInstance().getTime();

            for (final B2BBudgetModel budget : source.getBudgets())
            {
                if (!source.getCurrency().equals(budget.getCurrency()))
                {
                    continue;
                }

                totalBudget = totalBudget.add(budget.getBudget());

                if (startDate.after(budget.getDateRange().getStart()))
                {
                    startDate = budget.getDateRange().getStart();
                }

                if (endDate.before(budget.getDateRange().getEnd()))
                {
                    endDate = budget.getDateRange().getEnd();
                }
            }

            final Double totalCost = getB2bCostCenterService().getTotalCost(source, startDate, endDate);
            totalSpent = totalSpent.add(BigDecimal.valueOf(totalCost.doubleValue()));
            remainingBudget = totalBudget.subtract(totalSpent);
        }

        target.setTotalBudget(getPriceDataFactory().create(PriceDataType.BUY, totalBudget, source.getCurrency()));
        target.setTotalSpent(getPriceDataFactory().create(PriceDataType.BUY, totalSpent, source.getCurrency()));
        target.setRemainingBudget(getPriceDataFactory().create(PriceDataType.BUY, remainingBudget, source.getCurrency()));
    }

    /**
     * 
     * @param b2bCostCenterService
     *            - the b2b cost center service.
     */
    public void setB2bCostCenterService(final B2BCostCenterService<B2BCostCenterModel, B2BCustomerModel> b2bCostCenterService)
    {
        this.b2bCostCenterService = b2bCostCenterService;
    }

    /**
     * 
     * @param priceDataFactory
     *            - the price data factory.
     */
    @Required
    public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
    {
        this.priceDataFactory = priceDataFactory;
    }

    /**
     * 
     * @return B2BCostCenterService
     */
    protected B2BCostCenterService<B2BCostCenterModel, B2BCustomerModel> getB2bCostCenterService()
    {
        return b2bCostCenterService;
    }

    /**
     * 
     * @return PriceDataFactory
     */
    protected PriceDataFactory getPriceDataFactory()
    {
        return priceDataFactory;
    }

}
