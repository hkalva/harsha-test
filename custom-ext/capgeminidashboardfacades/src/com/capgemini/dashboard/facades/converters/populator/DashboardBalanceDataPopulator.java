/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.dashboard.facades.converters.populator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;

import org.springframework.beans.factory.annotation.Required;

import com.capgemini.dashboard.facades.data.DashboardBalanceData;

import de.hybris.platform.accountsummaryaddon.document.data.B2BAmountBalanceData;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;

/**
 *
 * @author lyonscg
 *
 */
public class DashboardBalanceDataPopulator implements Populator<B2BUnitModel, DashboardBalanceData>
{

    /**
     * the b2b amount balance converter.
     */
    private Converter<B2BUnitModel, B2BAmountBalanceData> b2bAmountBalanceConverter;

    /**
     * the i18n service.
     */
    private I18NService i18NService;

    /**
     * the common i18n service.
     */
    private CommonI18NService commonI18NService;

    /**
     * the price data factory.
     */
    private PriceDataFactory priceDataFactory;

    @Override
    public void populate(B2BUnitModel source, DashboardBalanceData target) throws ConversionException
    {
        target.setUnitUid(source.getUid());
        target.setUnitName(source.getName());
        B2BAmountBalanceData amountBalance = getB2bAmountBalanceConverter().convert(source);
        CurrencyModel currency = getCommonI18NService().getCurrentCurrency();
        DecimalFormat format = getFormat(currency);
        target.setCurrentBalance(getPriceData(format, currency, amountBalance.getCurrentBalance()));
        target.setOpenBalance(getPriceData(format, currency, amountBalance.getOpenBalance()));
        target.setPastDueBalance(getPriceData(format, currency, amountBalance.getPastDueBalance()));
    }

    /**
     * @param b2bAmountBalanceConverter
     *            the b2bAmountBalanceConverter to set
     */
    @Required
    public void setB2bAmountBalanceConverter(final Converter<B2BUnitModel, B2BAmountBalanceData> b2bAmountBalanceConverter)
    {
        this.b2bAmountBalanceConverter = b2bAmountBalanceConverter;
    }

    /**
     * 
     * @param i18nService
     *            - the i18n service.
     */
    @Required
    public void setI18NService(final I18NService i18NService)
    {
        this.i18NService = i18NService;
    }

    /**
     * 
     * @param commonI18NService
     *            - the common i18n service.
     */
    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService)
    {
        this.commonI18NService = commonI18NService;
    }

    /**
     * 
     * @param priceDataFactory
     *            - the price data factory.
     */
    @Required
    public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
    {
        this.priceDataFactory = priceDataFactory;
    }

    /**
     * @return Converter<B2BUnitModel, B2BAmountBalanceData>
     */
    protected Converter<B2BUnitModel, B2BAmountBalanceData> getB2bAmountBalanceConverter()
    {
        return b2bAmountBalanceConverter;
    }

    /**
     * 
     * @return I18NService
     */
    protected I18NService getI18NService()
    {
        return i18NService;
    }

    /**
     * 
     * @return CommonI18NService
     */
    protected CommonI18NService getCommonI18NService()
    {
        return commonI18NService;
    }

    /**
     * 
     * @return PriceDataFactory
     */
    protected PriceDataFactory getPriceDataFactory()
    {
        return priceDataFactory;
    }

    /**
     * 
     * @param format
     *            - the decimal format.
     * @param currency
     *            - the currency.
     * @param amount
     *            - the amount in string.
     * @return PriceData
     * @throws ConversionException
     */
    protected PriceData getPriceData(DecimalFormat format, CurrencyModel currency, String amount) throws ConversionException
    {
        PriceData result = null;

        try
        {
            double amountInDouble = format.parse(amount).doubleValue();
            BigDecimal value = BigDecimal.valueOf(amountInDouble).setScale(currency.getDigits().intValue(),
                    RoundingMode.HALF_EVEN);
            result = getPriceDataFactory().create(PriceDataType.BUY, value, currency);
        }
        catch (ParseException e)
        {
            throw new ConversionException("Error retrieving price data", e);
        }

        return result;
    }
    
    /**
     * 
     * @param currencyModel - the currency.
     * @return DecimalFormat
     */
    protected DecimalFormat getFormat(CurrencyModel currencyModel)
    {
        DecimalFormat result = (DecimalFormat) NumberFormat.getCurrencyInstance(getI18NService().getCurrentLocale());
        final int tempDigits = currencyModel.getDigits() == null ? 0 : currencyModel.getDigits().intValue();
        final int digits = Math.max(0, tempDigits);
        result.setMaximumFractionDigits(digits);
        result.setMinimumFractionDigits(digits);

        if (digits == 0)
        {
            result.setDecimalSeparatorAlwaysShown(false);
        }
        
        final String symbol = currencyModel.getSymbol();
        
        if (symbol != null)
        {
            final DecimalFormatSymbols symbols = result.getDecimalFormatSymbols();
            final String iso = currencyModel.getIsocode();
            boolean changed = false;
            
            if (!iso.equalsIgnoreCase(symbols.getInternationalCurrencySymbol()))
            {
                symbols.setInternationalCurrencySymbol(iso);
                changed = true;
            }
            
            if (!symbol.equals(symbols.getCurrencySymbol()))
            {
                symbols.setCurrencySymbol(symbol);
                changed = true;
            }
            
            if (changed)
            {
                result.setDecimalFormatSymbols(symbols);
            }
        }
        
        return result;
    }
    
}
