package com.capgemini.dashboard.facades.converters.populator;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.capgemini.dashboard.facades.data.DashboardBalanceData;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.accountsummaryaddon.document.data.B2BAmountBalanceData;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;

/**
 * 
 * @author lyonscg
 *
 */
@UnitTest
public class DashboardBalanceDataPopulatorTest
{
    /**
     * the populator to test.
     */
    private DashboardBalanceDataPopulator populator;
    
    /**
     * the target dto.
     */
    private DashboardBalanceData target;

    /**
     * the b2b amount balance converter.
     */
    @Mock
    private Converter<B2BUnitModel, B2BAmountBalanceData> b2bAmountBalanceConverter;

    /**
     * the i18n service.
     */
    @Mock
    private I18NService i18NService;

    /**
     * the common i18n service.
     */
    @Mock
    private CommonI18NService commonI18NService;

    /**
     * the price data factory.
     */
    @Mock
    private PriceDataFactory priceDataFactory;
    
    /**
     * the currency.
     */
    @Mock
    private CurrencyModel currency;
    
    /**
     * the balance data.
     */
    @Mock
    private B2BAmountBalanceData b2bAmountBalanceData;
    
    /**
     * the source b2b unit.
     */
    @Mock
    private B2BUnitModel source;
    
    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        populator = new DashboardBalanceDataPopulator();
        target = new DashboardBalanceData();
        populator.setB2bAmountBalanceConverter(b2bAmountBalanceConverter);
        populator.setCommonI18NService(commonI18NService);
        populator.setI18NService(i18NService);
        populator.setPriceDataFactory(priceDataFactory);
        Mockito.when(i18NService.getCurrentLocale()).thenReturn(Locale.ENGLISH);
        Mockito.when(priceDataFactory.create(Mockito.any(PriceDataType.class), Mockito.any(BigDecimal.class), Mockito.any(CurrencyModel.class))).thenAnswer(new Answer<PriceData> () {

            @Override
            public PriceData answer(InvocationOnMock invocation) throws Throwable
            {
                Object[] args = invocation.getArguments();
                PriceData result = new PriceData();
                result.setPriceType((PriceDataType) args[0]);
                result.setValue((BigDecimal) args[1]);
                result.setCurrencyIso(((CurrencyModel) args[2]).getIsocode());
                return result;
            }
            
        });
        
        Mockito.when(currency.getDigits()).thenReturn(Integer.valueOf(2));
        Mockito.when(currency.getSymbol()).thenReturn("$");
        Mockito.when(currency.getIsocode()).thenReturn("USD");
        Mockito.when(commonI18NService.getCurrentCurrency()).thenReturn(currency);
        Mockito.when(b2bAmountBalanceData.getCurrentBalance()).thenReturn("$230.00");
        Mockito.when(b2bAmountBalanceData.getOpenBalance()).thenReturn("$300.00");
        Mockito.when(b2bAmountBalanceData.getPastDueBalance()).thenReturn("$70.00");
        Mockito.when(b2bAmountBalanceConverter.convert(Mockito.any(B2BUnitModel.class))).thenReturn(b2bAmountBalanceData);
        Mockito.when(source.getUid()).thenReturn("uid");
        Mockito.when(source.getName()).thenReturn("name");
    }
    
    /**
     * test get format no digits and symbol.
     */
    @Test
    public void getFormatNoDigitsAndSymbol()
    {
        Mockito.when(currency.getDigits()).thenReturn(Integer.valueOf(0));
        Mockito.when(currency.getSymbol()).thenReturn(null);
        DecimalFormat result = populator.getFormat(currency);
        Assert.assertFalse(("$").equals(result.getDecimalFormatSymbols().getCurrencySymbol()));
    }
    
    /**
     * test get format with digits and symbol.
     */
    @Test
    public void getFormatWithDigitsAndSymbol()
    {
        DecimalFormat result = populator.getFormat(currency);
        Assert.assertTrue(("$").equals(result.getDecimalFormatSymbols().getCurrencySymbol()));
    }
    
    /**
     * test get price data.
     */
    @Test
    public void getPriceData()
    {
        DecimalFormat format = populator.getFormat(currency);
        PriceData result = populator.getPriceData(format, currency, "$100.00");
        Assert.assertTrue(currency.getIsocode().equals(result.getCurrencyIso()));
        Assert.assertTrue(BigDecimal.valueOf(100).compareTo(result.getValue()) == 0);
    }
    
    /**
     * test populate.
     */
    @Test
    public void populate()
    {
        populator.populate(source, target);
        Assert.assertTrue(source.getUid().equals(target.getUnitUid()));
        Assert.assertTrue(source.getName().equals(target.getUnitName()));
        Assert.assertTrue(BigDecimal.valueOf(300).compareTo(target.getOpenBalance().getValue()) == 0);
        Assert.assertTrue(BigDecimal.valueOf(230).compareTo(target.getCurrentBalance().getValue()) == 0);
        Assert.assertTrue(BigDecimal.valueOf(70).compareTo(target.getPastDueBalance().getValue()) == 0);
    }
    
}
