package com.capgemini.dashboard.facades.converters.populator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.dashboard.facades.data.DashboardOrderData;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;

@UnitTest
public class DashboardOrderDataPopulatorTest
{

    /**
     * the populator to test.
     */
    private DashboardOrderDataPopulator populator;
    
    /**
     * the order model.
     */
    @Mock
    private OrderModel source;

    /**
     * the dashboard order data.
     */
    private DashboardOrderData target;
    
    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        populator = new DashboardOrderDataPopulator();
        target = new DashboardOrderData();
        Mockito.when(source.getCode()).thenReturn("code");
        Mockito.when(source.getStatusDisplay()).thenReturn("statusDisplay");
    }
    
    /**
     * test.
     */
    @Test
    public void test()
    {
        populator.populate(source, target);
        Assert.assertTrue(source.getCode().equals(target.getCode()));
        Assert.assertTrue(source.getStatusDisplay().equals(target.getStatus()));
    }
}
