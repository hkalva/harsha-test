package com.capgemini.dashboard.facades.converters.populator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.dashboard.facades.data.DashboardQuoteData;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.QuoteState;
import de.hybris.platform.core.model.order.QuoteModel;

@UnitTest
public class DashboardQuoteDataPopulatorTest
{


    /**
     * the populator to test.
     */
    private DashboardQuoteDataPopulator populator;
    
    /**
     * the quote model.
     */
    @Mock
    private QuoteModel source;

    /**
     * the dashboard quote data.
     */
    private DashboardQuoteData target;
    
    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        populator = new DashboardQuoteDataPopulator();
        target = new DashboardQuoteData();
        Mockito.when(source.getCode()).thenReturn("code");
        Mockito.when(source.getState()).thenReturn(QuoteState.CREATED);
    }
    
    /**
     * test.
     */
    @Test
    public void test()
    {
        populator.populate(source, target);
        Assert.assertTrue(source.getCode().equals(target.getCode()));
        Assert.assertTrue(source.getState().getCode().equals(target.getStatus()));
    }
    
}
