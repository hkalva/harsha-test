package com.capgemini.dashboard.facades.converters.populator;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.capgemini.dashboard.facades.data.DashboardBudgetData;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BBudgetModel;
import de.hybris.platform.b2b.model.B2BCostCenterModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.services.B2BCostCenterService;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.util.StandardDateRange;

@UnitTest
public class DashboardBudgetDataPopulatorTest
{

    /**
     * the populator to test.
     */
    private DashboardBudgetDataPopulator populator;
    
    /**
     * the b2b cost center service.
     */
    @Mock
    private B2BCostCenterService<B2BCostCenterModel, B2BCustomerModel> b2bCostCenterService;

    /**
     * the price data factory.
     */
    @Mock
    private PriceDataFactory priceDataFactory;
    
    /**
     * the usd currency.
     */
    @Mock
    private CurrencyModel usCurrency;

    /**
     * the cad currency.
     */
    @Mock
    private CurrencyModel caCurrency;
    
    /**
     * the b2b cost center model.
     */
    @Mock
    private B2BCostCenterModel source;
    
    /**
     * the budget.
     */
    @Mock
    private B2BBudgetModel budget;
    
    /**
     * the date range.
     */
    @Mock
    private StandardDateRange dateRange;
    
    /**
     * the dashboard budget data.
     */
    private DashboardBudgetData target;
    
    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        populator = new DashboardBudgetDataPopulator();
        populator.setB2bCostCenterService(b2bCostCenterService);
        populator.setPriceDataFactory(priceDataFactory);
        target = new DashboardBudgetData();
        Mockito.when(source.getCode()).thenReturn("code");
        Mockito.when(source.getName()).thenReturn("name");
        Mockito.when(source.getCurrency()).thenReturn(usCurrency);
        Mockito.when(usCurrency.getIsocode()).thenReturn("USD");
        Mockito.when(caCurrency.getIsocode()).thenReturn("CAD");
        Mockito.when(dateRange.getStart()).thenReturn(new Date());
        Mockito.when(dateRange.getEnd()).thenReturn(new Date());
        Mockito.when(budget.getDateRange()).thenReturn(dateRange);
        Mockito.when(budget.getBudget()).thenReturn(BigDecimal.TEN);
        Mockito.when(b2bCostCenterService.getTotalCost(Mockito.any(B2BCostCenterModel.class), Mockito.any(Date.class), Mockito.any(Date.class))).thenReturn(Double.valueOf(0));
        Mockito.when(priceDataFactory.create(Mockito.any(PriceDataType.class), Mockito.any(BigDecimal.class), Mockito.any(CurrencyModel.class))).thenAnswer(new Answer<PriceData> () {

            @Override
            public PriceData answer(InvocationOnMock invocation) throws Throwable
            {
                Object[] args = invocation.getArguments();
                PriceData result = new PriceData();
                result.setPriceType((PriceDataType) args[0]);
                result.setValue((BigDecimal) args[1]);
                result.setCurrencyIso(((CurrencyModel) args[2]).getIsocode());
                return result;
            }
            
        });
    }
    
    /**
     * test no budgets.
     */
    @Test
    public void testNoBudgets()
    {
        Mockito.when(source.getBudgets()).thenReturn(Collections.emptySet());
        populator.populate(source, target);
        Mockito.verify(b2bCostCenterService, Mockito.times(0)).getTotalCost(Mockito.any(B2BCostCenterModel.class), Mockito.any(Date.class), Mockito.any(Date.class));
        Assert.assertTrue(source.getCode().equals(target.getCostCenterCode()));
        Assert.assertTrue(source.getName().equals(target.getCostCenterName()));
        Assert.assertTrue(target.getTotalBudget().getValue().compareTo(BigDecimal.ZERO) == 0);
        Assert.assertTrue(target.getTotalSpent().getValue().compareTo(BigDecimal.ZERO) == 0);
        Assert.assertTrue(target.getRemainingBudget().getValue().compareTo(BigDecimal.ZERO) == 0);
    }

    /**
     * test budgets with different currency.
     */
    @Test
    public void testBudgetsWithDifferentCurrency()
    {
        Mockito.when(budget.getCurrency()).thenReturn(caCurrency);
        Mockito.when(source.getBudgets()).thenReturn(Collections.singleton(budget));
        populator.populate(source, target);
        Mockito.verify(b2bCostCenterService, Mockito.times(1)).getTotalCost(Mockito.any(B2BCostCenterModel.class), Mockito.any(Date.class), Mockito.any(Date.class));
        Assert.assertTrue(source.getCode().equals(target.getCostCenterCode()));
        Assert.assertTrue(source.getName().equals(target.getCostCenterName()));
        Assert.assertTrue(target.getTotalBudget().getValue().compareTo(BigDecimal.ZERO) == 0);
        Assert.assertTrue(target.getTotalSpent().getValue().compareTo(BigDecimal.ZERO) == 0);
        Assert.assertTrue(target.getRemainingBudget().getValue().compareTo(BigDecimal.ZERO) == 0);
    }

    /**
     * test budgets with same currency.
     */
    @Test
    public void testBudgetsWithSameCurrency()
    {
        Mockito.when(budget.getCurrency()).thenReturn(usCurrency);
        Mockito.when(source.getBudgets()).thenReturn(Collections.singleton(budget));
        populator.populate(source, target);
        Mockito.verify(b2bCostCenterService, Mockito.times(1)).getTotalCost(Mockito.any(B2BCostCenterModel.class), Mockito.any(Date.class), Mockito.any(Date.class));
        Assert.assertTrue(source.getCode().equals(target.getCostCenterCode()));
        Assert.assertTrue(source.getName().equals(target.getCostCenterName()));
        Assert.assertTrue(target.getTotalBudget().getValue().compareTo(BigDecimal.ZERO) > 0);
        Assert.assertTrue(target.getTotalSpent().getValue().compareTo(BigDecimal.ZERO) == 0);
        Assert.assertTrue(target.getRemainingBudget().getValue().compareTo(BigDecimal.ZERO) > 0);
    }
    
}
