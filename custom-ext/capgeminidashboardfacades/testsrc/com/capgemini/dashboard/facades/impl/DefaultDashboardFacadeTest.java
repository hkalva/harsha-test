package com.capgemini.dashboard.facades.impl;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.dashboard.facades.data.DashboardBudgetData;
import com.capgemini.dashboard.facades.data.DashboardOrderData;
import com.capgemini.dashboard.facades.data.DashboardQuoteData;
import com.capgemini.dashboard.services.DashboardService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCostCenterModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.order.strategies.QuoteUserIdentificationStrategy;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

@UnitTest
public class DefaultDashboardFacadeTest
{

    /**
     * the facade class to test.
     */
    private DefaultDashboardFacade facade;
    
    /**
     * the base store service.
     */
    @Mock
    private BaseStoreService baseStoreService;
    
    /**
     * the dashboard service.
     */
    @Mock
    private DashboardService dashboardService;

    /**
     * the user service.
     */
    @Mock
    private UserService userService;
    
    /**
     * the common i18n service.
     */
    @Mock
    private CommonI18NService commonI18NService;
    
    /**
     * the quote user identification strategy.
     */
    @Mock
    private QuoteUserIdentificationStrategy quoteUserIdentificationStrategy;
    
    /**
     * the dashboard order data converter.
     */
    @Mock
    private Converter<OrderModel, DashboardOrderData> dashboardOrderDataConverter;

    /**
     * the dashboard quote data converter.
     */
    @Mock
    private Converter<QuoteModel, DashboardQuoteData> dashboardQuoteDataConverter;

    /**
     * the dashboard budget data converter.
     */
    @Mock
    private Converter<B2BCostCenterModel, DashboardBudgetData> dashboardBudgetDataConverter;
    
    /**
     * the customer.
     */
    @Mock
    private B2BCustomerModel customer;
    
    /**
     * the store.
     */
    @Mock
    private BaseStoreModel store;
    
    /**
     * the currency.
     */
    @Mock
    private CurrencyModel currency;
    
    /**
     * the search page data for orders.
     */
    @Mock
    private SearchPageData<OrderModel> orderSearchPageData;

    /**
     * the search page data for quotes.
     */
    @Mock
    private SearchPageData<QuoteModel> quoteSearchPageData;
    
    /**
     * the order.
     */
    @Mock
    private OrderModel order;

    /**
     * the quote.
     */
    @Mock
    private QuoteModel quote;
    
    /**
     * the cost center.
     */
    @Mock
    private B2BCostCenterModel costCenter;
    
    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        facade = new DefaultDashboardFacade();
        facade.setBaseStoreService(baseStoreService);
        facade.setCommonI18NService(commonI18NService);
        facade.setDashboardBudgetDataConverter(dashboardBudgetDataConverter);
        facade.setDashboardOrderDataConverter(dashboardOrderDataConverter);
        facade.setDashboardQuoteDataConverter(dashboardQuoteDataConverter);
        facade.setDashboardService(dashboardService);
        facade.setQuoteUserIdentificationStrategy(quoteUserIdentificationStrategy);
        facade.setUserService(userService);
        Mockito.when(dashboardOrderDataConverter.convert(Mockito.any(OrderModel.class))).thenReturn(new DashboardOrderData());
        Mockito.when(dashboardQuoteDataConverter.convert(Mockito.any(QuoteModel.class))).thenReturn(new DashboardQuoteData());
        Mockito.when(dashboardBudgetDataConverter.convert(Mockito.any(B2BCostCenterModel.class))).thenReturn(new DashboardBudgetData());
        Mockito.when(userService.getCurrentUser()).thenReturn(customer);
        Mockito.when(baseStoreService.getCurrentBaseStore()).thenReturn(store);
        Mockito.when(commonI18NService.getCurrentCurrency()).thenReturn(currency);
        Mockito.when(dashboardService.getOrders(Mockito.any(CustomerModel.class), Mockito.any(BaseStoreModel.class), Mockito.any(SearchPageData.class))).thenReturn(orderSearchPageData);
        Mockito.when(dashboardService.getQuotes(Mockito.any(CustomerModel.class), Mockito.any(UserModel.class), Mockito.any(BaseStoreModel.class), Mockito.any(SearchPageData.class))).thenReturn(quoteSearchPageData);
    }
    
    /**
     * test getOrders.
     */
    @Test
    public void testGetOrders()
    {
        //test no orders
        Mockito.when(orderSearchPageData.getResults()).thenReturn(Collections.emptyList());
        List<DashboardOrderData> result = facade.getOrders(2);
        Assert.assertTrue(CollectionUtils.isEmpty(result));
        
        //test with orders
        Mockito.when(orderSearchPageData.getResults()).thenReturn(Collections.singletonList(order));
        result = facade.getOrders(2);
        Assert.assertTrue(CollectionUtils.isNotEmpty(result));
        
        Mockito.verify(dashboardService, Mockito.times(2)).getOrders(Mockito.any(CustomerModel.class), Mockito.any(BaseStoreModel.class), Mockito.any(SearchPageData.class));
    }

    /**
     * test getQuotes.
     */
    @Test
    public void testGetQuotes()
    {
        //test no quotes
        Mockito.when(quoteSearchPageData.getResults()).thenReturn(Collections.emptyList());
        List<DashboardQuoteData> result = facade.getQuotes(2);
        Assert.assertTrue(CollectionUtils.isEmpty(result));
        
        //test with quotes
        Mockito.when(quoteSearchPageData.getResults()).thenReturn(Collections.singletonList(quote));
        result = facade.getQuotes(2);
        Assert.assertTrue(CollectionUtils.isNotEmpty(result));
        
        Mockito.verify(dashboardService, Mockito.times(2)).getQuotes(Mockito.any(CustomerModel.class), Mockito.any(UserModel.class), Mockito.any(BaseStoreModel.class), Mockito.any(SearchPageData.class));
    }

    /**
     * test getBudgets.
     */
    @Test
    public void testGetBudgets()
    {
        //test no cost centers
        Mockito.when(dashboardService.getCostCenters(Mockito.any(CurrencyModel.class))).thenReturn(Collections.emptyList());
        List<DashboardBudgetData> result = facade.getBudgets();
        Assert.assertTrue(CollectionUtils.isEmpty(result));
        
        //test with quotes
        Mockito.when(dashboardService.getCostCenters(Mockito.any(CurrencyModel.class))).thenReturn(Collections.singletonList(costCenter));
        result = facade.getBudgets();
        Assert.assertTrue(CollectionUtils.isNotEmpty(result));
        
        Mockito.verify(dashboardService, Mockito.times(2)).getCostCenters(Mockito.any(CurrencyModel.class));
    }
    
    
}
