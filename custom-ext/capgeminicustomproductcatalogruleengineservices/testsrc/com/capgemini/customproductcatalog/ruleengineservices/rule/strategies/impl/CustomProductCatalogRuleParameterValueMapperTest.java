package com.capgemini.customproductcatalog.ruleengineservices.rule.strategies.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.customproductcatalog.model.CustomProductCatalogModel;
import com.capgemini.customproductcatalog.service.CustomProductCatalogService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ruleengineservices.rule.strategies.RuleParameterValueMapperException;

@UnitTest
public class CustomProductCatalogRuleParameterValueMapperTest
{

    private CustomProductCatalogRuleParameterValueMapper mapper;
    
    @Mock
    private CustomProductCatalogService customProductCatalogService;
    
    @Mock
    private CustomProductCatalogModel customProductCatalog;
    
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        mapper = new CustomProductCatalogRuleParameterValueMapper();
        mapper.setCustomProductCatalogService(customProductCatalogService);
        Mockito.when(customProductCatalog.getCode()).thenReturn("code");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void fromStringNull()
    {
        mapper.fromString(null);
    }

    @Test(expected = RuleParameterValueMapperException.class)
    public void fromStringInvalidCode()
    {
        Mockito.when(customProductCatalogService.getCustomProductCatalogForCode(Mockito.anyString())).thenThrow(Exception.class);
        mapper.fromString("blah");
    }

    @Test
    public void fromString()
    {
        Mockito.when(customProductCatalogService.getCustomProductCatalogForCode(Mockito.anyString())).thenReturn(customProductCatalog);
        CustomProductCatalogModel result = mapper.fromString("code");
        Assert.assertTrue(result != null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void toStringNull()
    {
        mapper.toString(null);
    }

    @Test
    public void toStringValid()
    {
        String result = mapper.toString(customProductCatalog);
        Assert.assertTrue(customProductCatalog.getCode().equals(result));
    }
    
}
