package com.capgemini.customproductcatalog.ruleengineservices.converters.populator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.customproductcatalog.model.CustomProductCatalogModel;
import com.capgemini.customproductcatalog.ruleengineservices.rao.CustomProductCatalogRAO;

import de.hybris.bootstrap.annotations.UnitTest;

@UnitTest
public class CustomProductCatalogRaoPopulatorTest
{

    private CustomProductCatalogRaoPopulator populator;
    
    private CustomProductCatalogRAO target;
    
    @Mock
    private CustomProductCatalogModel source;
    
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        populator = new CustomProductCatalogRaoPopulator();
        target = new CustomProductCatalogRAO();
        Mockito.when(source.getCode()).thenReturn("code");
    }
    
    @Test
    public void populate()
    {
        populator.populate(source, target);
        Assert.assertTrue(source.getCode().equals(target.getCode()));
    }
}
