package com.capgemini.customproductcatalog.ruleengineservices.converters.populator;

import java.util.Collections;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.customproductcatalog.model.CustomProductCatalogModel;
import com.capgemini.customproductcatalog.ruleengineservices.rao.CustomProductCatalogRAO;
import com.capgemini.customproductcatalog.service.CustomProductCatalogService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ruleengineservices.rao.ProductRAO;
import de.hybris.platform.servicelayer.dto.converter.Converter;

@UnitTest
public class ProductCustomProductCatalogsRaoPopulatorTest
{
    private ProductCustomProductCatalogsRaoPopulator populator;
    
    private ProductRAO target;
    
    @Mock
    private CustomProductCatalogService customProductCatalogService;
    
    @Mock
    private Converter<CustomProductCatalogModel, CustomProductCatalogRAO> customProductCatalogRaoConverter;
    
    @Mock
    private ProductModel source;
    
    @Mock
    private CustomProductCatalogModel customProductCatalog;
    
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        target = new ProductRAO();
        populator = new ProductCustomProductCatalogsRaoPopulator();
        populator.setCustomProductCatalogRaoConverter(customProductCatalogRaoConverter);
        populator.setCustomProductCatalogService(customProductCatalogService);
        Mockito.when(customProductCatalogRaoConverter.convert(Mockito.any(CustomProductCatalogModel.class))).thenReturn(new CustomProductCatalogRAO());
    }
    
    @Test
    public void populateNoCatalogs()
    {
        Mockito.when(customProductCatalogService.getCustomProductCatalogForProduct(Mockito.any(ProductModel.class))).thenReturn(null);
        populator.populate(source, target);
        Mockito.verify(customProductCatalogRaoConverter, Mockito.never()).convert(Mockito.any(CustomProductCatalogModel.class));
        Assert.assertTrue(CollectionUtils.isEmpty(target.getCustomProductCatalogs()));
    }
    
    @Test
    public void populate()
    {
        Mockito.when(customProductCatalogService.getCustomProductCatalogForProduct(Mockito.any(ProductModel.class))).thenReturn(Collections.singletonList(customProductCatalog));
        populator.populate(source, target);
        Mockito.verify(customProductCatalogRaoConverter, Mockito.times(1)).convert(Mockito.any(CustomProductCatalogModel.class));
        Assert.assertFalse(CollectionUtils.isEmpty(target.getCustomProductCatalogs()));
    }
    
}
