package com.capgemini.customproductcatalog.ruleengineservices.rao.providers.impl;

import java.util.Collections;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.customproductcatalog.ruleengineservices.rao.CustomProductCatalogRAO;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.ruleengineservices.rao.ProductRAO;

@UnitTest
public class CustomProductCatalogRAOFactsExtractorTest
{

    private CustomProductCatalogRAOFactsExtractor extractor;
    
    @Mock
    private CartRAO cartRao;
    
    @Mock
    private OrderEntryRAO orderEntryRao;
    
    @Mock
    private ProductRAO productRao;
    
    @Mock
    private CustomProductCatalogRAO customProductCatalogRao;
    
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        extractor = new CustomProductCatalogRAOFactsExtractor();
    }
    
    @Test
    public void getTriggeringOption()
    {
        String triggerOption = extractor.getTriggeringOption();
        Assert.assertTrue(("EXPAND_CUSTOM_PRODUCT_CATALOGS").equals(triggerOption));
    }
    
    @Test
    public void isDefault()
    {
        Assert.assertTrue(extractor.isDefault());
    }
    
    @Test
    public void isMinOption()
    {
        Assert.assertFalse(extractor.isMinOption());
    }
    
    @Test
    public void expandFact()
    {
        Set result = extractor.expandFact(orderEntryRao);
        Assert.assertTrue(CollectionUtils.isEmpty(result));
        result = extractor.expandFact(cartRao);
        Assert.assertTrue(CollectionUtils.isEmpty(result));
        Mockito.when(cartRao.getEntries()).thenReturn(Collections.singleton(orderEntryRao));
        result = extractor.expandFact(cartRao);
        Assert.assertTrue(CollectionUtils.isEmpty(result));
        Mockito.when(orderEntryRao.getProduct()).thenReturn(productRao);
        result = extractor.expandFact(cartRao);
        Assert.assertTrue(CollectionUtils.isEmpty(result));
        Mockito.when(productRao.getCustomProductCatalogs()).thenReturn(Collections.singleton(customProductCatalogRao));
        result = extractor.expandFact(cartRao);
        Assert.assertTrue(CollectionUtils.isNotEmpty(result));
    }
    
}
