/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customproductcatalog.ruleengineservices.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.capgemini.customproductcatalog.model.CustomProductCatalogModel;
import com.capgemini.customproductcatalog.ruleengineservices.rao.CustomProductCatalogRAO;


/**
 * Populate CustomProductCatalogRAO with source CustomProductCatalogModel.
 */
public class CustomProductCatalogRaoPopulator implements Populator<CustomProductCatalogModel, CustomProductCatalogRAO>
{
	@Override
	public void populate(final CustomProductCatalogModel source, final CustomProductCatalogRAO target) throws ConversionException
	{
		target.setCode(source.getCode());
	}
}
