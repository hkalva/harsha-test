/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customproductcatalog.ruleengineservices.converters.populator;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ruleengineservices.rao.ProductRAO;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.capgemini.customproductcatalog.model.CustomProductCatalogModel;
import com.capgemini.customproductcatalog.ruleengineservices.rao.CustomProductCatalogRAO;
import com.capgemini.customproductcatalog.service.CustomProductCatalogService;


/**
 * We're adding a property to the OOTB ProductRAO DTO that will hold all the CustomProductCatalogRAOs for the product.
 */
public class ProductCustomProductCatalogsRaoPopulator implements Populator<ProductModel, ProductRAO>
{
	/**
	 * customProductCatalogService bean.
	 */
	private CustomProductCatalogService customProductCatalogService;
	/**
	 * custom product catalog converter.
	 */
	private Converter<CustomProductCatalogModel, CustomProductCatalogRAO> customProductCatalogRaoConverter;



	@Override
	public void populate(final ProductModel source, final ProductRAO target) throws ConversionException
	{
		final List<CustomProductCatalogModel> catalogs = getCustomProductCatalogService().getCustomProductCatalogForProduct(source);
		if (CollectionUtils.isNotEmpty(catalogs))
		{
			target.setCustomProductCatalogs(new HashSet<>(Converters.convertAll(catalogs, getCustomProductCatalogRaoConverter())));
		}
	}

	/**
	 * @return the customProductCatalogService
	 */
	public CustomProductCatalogService getCustomProductCatalogService()
	{
		return customProductCatalogService;
	}

	/**
	 * @param customProductCatalogService
	 *           the customProductCatalogService to set
	 */
	public void setCustomProductCatalogService(final CustomProductCatalogService customProductCatalogService)
	{
		this.customProductCatalogService = customProductCatalogService;
	}

	/**
	 * @return the customProductCatalogRaoConverter
	 */
	public Converter<CustomProductCatalogModel, CustomProductCatalogRAO> getCustomProductCatalogRaoConverter()
	{
		return customProductCatalogRaoConverter;
	}

	/**
	 * @param customProductCatalogRaoConverter
	 *           the customProductCatalogRaoConverter to set
	 */
	public void setCustomProductCatalogRaoConverter(
			final Converter<CustomProductCatalogModel, CustomProductCatalogRAO> customProductCatalogRaoConverter)
	{
		this.customProductCatalogRaoConverter = customProductCatalogRaoConverter;
	}
}
