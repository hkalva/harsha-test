/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customproductcatalog.ruleengineservices.rule.strategies.impl;

import de.hybris.platform.ruleengineservices.rule.strategies.RuleParameterValueMapper;
import de.hybris.platform.ruleengineservices.rule.strategies.RuleParameterValueMapperException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import com.capgemini.customproductcatalog.model.CustomProductCatalogModel;
import com.capgemini.customproductcatalog.service.CustomProductCatalogService;


/**
 * Implement RuleParameterValueMapper interface for CustomProductCatalogModel.
 */
public class CustomProductCatalogRuleParameterValueMapper implements RuleParameterValueMapper<CustomProductCatalogModel>
{

	/**
	 * customProductCatalogService bean.
	 */
	private CustomProductCatalogService customProductCatalogService;

	@Override
	public CustomProductCatalogModel fromString(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "Code cannot be null");

		try
		{
			return getCustomProductCatalogService().getCustomProductCatalogForCode(code);
		}
		catch (final Exception e)
		{
			throw new RuleParameterValueMapperException(
					"Error encountered while retrieving custom product catalog with code '" + code + "'", e);
		}
	}

	@Override
	public String toString(final CustomProductCatalogModel customProductCatalog)
	{
		ServicesUtil.validateParameterNotNull(customProductCatalog, "Custom product catalog cannot be null");
		return customProductCatalog.getCode();
	}

	/**
	 * @return the customProductCatalogService
	 */
	public CustomProductCatalogService getCustomProductCatalogService()
	{
		return customProductCatalogService;
	}

	/**
	 * @param customProductCatalogService
	 *           the customProductCatalogService to set
	 */
	public void setCustomProductCatalogService(final CustomProductCatalogService customProductCatalogService)
	{
		this.customProductCatalogService = customProductCatalogService;
	}

}
