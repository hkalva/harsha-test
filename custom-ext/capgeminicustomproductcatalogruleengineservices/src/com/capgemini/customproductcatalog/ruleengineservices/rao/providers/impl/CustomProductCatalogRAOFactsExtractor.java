/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customproductcatalog.ruleengineservices.rao.providers.impl;

import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.ruleengineservices.rao.providers.RAOFactsExtractor;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;


/**
 * Implement a RAOFactsExtractor that will provide the custom product catalogs associated with the products in the
 * cart/order.
 */
public class CustomProductCatalogRAOFactsExtractor implements RAOFactsExtractor
{
	@Override
	public Set expandFact(final Object object)
	{
		final Set<Object> result = new HashSet<>();
		Set<OrderEntryRAO> entries = null;

		if (object instanceof CartRAO)
		{
			final CartRAO cartRAO = (CartRAO) object;
			entries = cartRAO.getEntries();
		}

		if (CollectionUtils.isNotEmpty(entries))
		{
			for (final OrderEntryRAO entry : entries)
			{
				if ((entry.getProduct() != null) && (CollectionUtils.isNotEmpty(entry.getProduct().getCustomProductCatalogs())))
				{
					result.addAll(entry.getProduct().getCustomProductCatalogs());
				}
			}
		}
		return result;
	}

	@Override
	public String getTriggeringOption()
	{
		return "EXPAND_CUSTOM_PRODUCT_CATALOGS";
	}

	@Override
	public boolean isDefault()
	{
		return true;
	}

	@Override
	public boolean isMinOption()
	{
		return false;
	}

}
