/**
 *
 */
package com.lyonscg.paypaladdon.converters.populators.impl;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.session.SessionService;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;

import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentRequestType;
import urn.ebay.apis.eBLBaseComponents.DoExpressCheckoutPaymentRequestDetailsType;

import com.paypal.hybris.constants.PaypalConstants;
import com.paypal.hybris.data.DoExpressCheckoutPaymentRequestData;


/**
 * Populates DoExpressCheckoutPaymentRequestType.ButtonSource based on property Values.
 */
public class LeaPPDoExprCheckoutPaymentReqPaymentDetailsPopulator implements
		Populator<DoExpressCheckoutPaymentRequestData, DoExpressCheckoutPaymentRequestType>
{
	private ConfigurationService configurationService;
	private SessionService sessionService;
	private String paypalBnCreditKey, paypalBnKey;

	/**
	 * Populate the target instance with values from the source instance.
	 *
	 * @param requestData
	 *           the source object
	 * @param request
	 *           the target to fill
	 * @throws de.hybris.platform.servicelayer.dto.converter.ConversionException
	 *            if an error occurs
	 */
	@Override
	public void populate(final DoExpressCheckoutPaymentRequestData requestData, final DoExpressCheckoutPaymentRequestType request)
			throws ConversionException
	{
		final DoExpressCheckoutPaymentRequestDetailsType details = request.getDoExpressCheckoutPaymentRequestDetails();
		if (null == details)
		{
			throw new ConversionException("Unable to pupulate BN details.");
		}
		details.setButtonSource(getButtonSource());
	}

	/**
	 * Returns Configured ButtonSource based on Session attribute.
	 *
	 * @return - Button source
	 */
	protected String getButtonSource()
	{
		if (BooleanUtils.isTrue(getSessionService().getAttribute(PaypalConstants.IS_PAYPAL_CREDIT)))
		{
			return getConfigurationService().getConfiguration().getString(getPaypalBnCreditKey());
		}
		return getConfigurationService().getConfiguration().getString(getPaypalBnKey());
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected String getPaypalBnCreditKey()
	{
		return paypalBnCreditKey;
	}

	@Required
	public void setPaypalBnCreditKey(final String paypalBnCreditKey)
	{
		this.paypalBnCreditKey = paypalBnCreditKey;
	}

	protected String getPaypalBnKey()
	{
		return paypalBnKey;
	}

	@Required
	public void setPaypalBnKey(final String paypalBnKey)
	{
		this.paypalBnKey = paypalBnKey;
	}
}
