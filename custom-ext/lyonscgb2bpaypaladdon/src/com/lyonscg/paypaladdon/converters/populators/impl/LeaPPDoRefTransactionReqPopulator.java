/**
 *
 */
package com.lyonscg.paypaladdon.converters.populators.impl;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.beans.factory.annotation.Required;

import urn.ebay.api.PayPalAPI.DoReferenceTransactionRequestType;
import urn.ebay.apis.eBLBaseComponents.DoReferenceTransactionRequestDetailsType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsType;

import com.paypal.hybris.data.DoReferenceTransactionRequestData;


/**
 * Populates ButtonSource in DoReferenceTransactionRequestType based on property Values.
 */
public class LeaPPDoRefTransactionReqPopulator implements
		Populator<DoReferenceTransactionRequestData, DoReferenceTransactionRequestType>
{
	private ConfigurationService configurationService;
	private String paypalBnCreditKey, paypalBnKey;

	/**
	 * Populate the target instance with values from the source instance.
	 *
	 * @param requestData
	 *           the source object
	 * @param request
	 *           the target to fill
	 * @throws de.hybris.platform.servicelayer.dto.converter.ConversionException
	 *            if an error occurs
	 */
	@Override
	public void populate(final DoReferenceTransactionRequestData requestData, final DoReferenceTransactionRequestType request)
			throws ConversionException
	{
		final DoReferenceTransactionRequestDetailsType requestTransDetails = request.getDoReferenceTransactionRequestDetails();
		if (null != requestTransDetails)
		{
			final PaymentDetailsType paymentDetails = requestTransDetails.getPaymentDetails();
			if (paymentDetails != null)
			{
				paymentDetails.setButtonSource(getButtonSource(requestData));
				return;
			}
		}
		throw new ConversionException("Unable to populate BN details.");
	}

	/**
	 * Returns Configured ButtonSource based on {@link DoReferenceTransactionRequestData}.
	 *
	 * @return - Button source
	 */
	protected String getButtonSource(final DoReferenceTransactionRequestData requestData)
	{
		if (requestData.isCredit())
		{
			return getConfigurationService().getConfiguration().getString(getPaypalBnCreditKey());
		}
		return getConfigurationService().getConfiguration().getString(getPaypalBnKey());
	}

	protected String getPaypalBnCreditKey()
	{
		return paypalBnCreditKey;
	}

	@Required
	public void setPaypalBnCreditKey(final String paypalBnCreditKey)
	{
		this.paypalBnCreditKey = paypalBnCreditKey;
	}

	protected String getPaypalBnKey()
	{
		return paypalBnKey;
	}

	@Required
	public void setPaypalBnKey(final String paypalBnKey)
	{
		this.paypalBnKey = paypalBnKey;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
