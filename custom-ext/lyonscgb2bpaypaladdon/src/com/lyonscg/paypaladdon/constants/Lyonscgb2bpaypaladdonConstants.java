/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.lyonscg.paypaladdon.constants;

/**
 * Global class for all Lyonscgb2bpaypaladdon constants. You can add global constants for your extension into this class.
 */
public final class Lyonscgb2bpaypaladdonConstants extends GeneratedLyonscgb2bpaypaladdonConstants
{
	public static final String EXTENSIONNAME = "lyonscgb2bpaypaladdon";

	private Lyonscgb2bpaypaladdonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
