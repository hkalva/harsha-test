/**
 *
 */
package com.lyonscg.paypaladdon.converters.populators.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import urn.ebay.api.PayPalAPI.DoReferenceTransactionRequestType;
import urn.ebay.apis.eBLBaseComponents.DoReferenceTransactionRequestDetailsType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsType;

import com.paypal.hybris.data.DoReferenceTransactionRequestData;


/**
 * Test class for {@link LeaPPDoRefTransactionReqPopulator}.
 *
 */
@UnitTest
public class LeaPPDoRefTransactionReqPopulatorTest
{
	@InjectMocks
	private LeaPPDoRefTransactionReqPopulator testClass;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private Configuration configuration;
	private final String paypalBnCreditKey = "paypalBnCreditKey", paypalBnKey = "paypalBnKey";
	private final String paypalBnCreditVal = "paypalBnCreditVal", paypalBnVal = "paypalBnVal";
	private DoReferenceTransactionRequestData requestData;
	private DoReferenceTransactionRequestType request;

	/**Setting up test data.
	 * @throws InvalidCartException - to test for invalid cart.
	 */
	@Before
	public void setup() throws InvalidCartException
	{
		testClass = new LeaPPDoRefTransactionReqPopulator();
		MockitoAnnotations.initMocks(this);
		testClass.setPaypalBnCreditKey(paypalBnCreditKey);
		testClass.setPaypalBnKey(paypalBnKey);

		requestData = new DoReferenceTransactionRequestData();
		request = new DoReferenceTransactionRequestType();
		final DoReferenceTransactionRequestDetailsType requestTransDetails = new DoReferenceTransactionRequestDetailsType();
		request.setDoReferenceTransactionRequestDetails(requestTransDetails);
		requestTransDetails.setPaymentDetails(new PaymentDetailsType());

		Mockito.doReturn(configuration).when(configurationService).getConfiguration();
		Mockito.doReturn(paypalBnCreditVal).when(configuration).getString(paypalBnCreditKey);
		Mockito.doReturn(paypalBnVal).when(configuration).getString(paypalBnKey);
	}

	/**Paypal Express checkout test for.
	 * {@link com.lyonscg.paypaladdon.converters.populators.impl.LeaPPDoRefTransactionReqPopulator#populate(DoReferenceTransactionRequestData, DoReferenceTransactionRequestType)}
	 */
	@Test
	public void populatePaypalExpressTest()
	{
		requestData.setCredit(false);
		testClass.populate(requestData, request);
		Assert.assertEquals(paypalBnVal, request.getDoReferenceTransactionRequestDetails().getPaymentDetails().getButtonSource());
	}

	/**
	 * Paypal Credit checkout test for
	 * {@link com.lyonscg.paypaladdon.converters.populators.impl.LeaPPDoRefTransactionReqPopulator#populate(DoReferenceTransactionRequestData, DoReferenceTransactionRequestType)}
	 */
	@Test
	public void populatePaypalCreditTest()
	{
		requestData.setCredit(true);
		testClass.populate(requestData, request);
		Assert.assertEquals(paypalBnCreditVal, request.getDoReferenceTransactionRequestDetails().getPaymentDetails()
				.getButtonSource());
	}

	/**
	 * ConversionException test for
	 * {@link com.lyonscg.paypaladdon.converters.populators.impl.LeaPPDoRefTransactionReqPopulator#populate(DoReferenceTransactionRequestData, DoReferenceTransactionRequestType)}
	 */
	@Test(expected = ConversionException.class)
	public void populateNulReqExceptionTest()
	{
		request.setDoReferenceTransactionRequestDetails(null);
		testClass.populate(requestData, request);
	}

	/**
	 * ConversionException test for
	 * {@link com.lyonscg.paypaladdon.converters.populators.impl.LeaPPDoRefTransactionReqPopulator#populate(DoReferenceTransactionRequestData, DoReferenceTransactionRequestType)}
	 */
	@Test(expected = ConversionException.class)
	public void populateNullPaymentExceptionTest()
	{
		request.getDoReferenceTransactionRequestDetails().setPaymentDetails(null);
		testClass.populate(requestData, request);
	}
}
