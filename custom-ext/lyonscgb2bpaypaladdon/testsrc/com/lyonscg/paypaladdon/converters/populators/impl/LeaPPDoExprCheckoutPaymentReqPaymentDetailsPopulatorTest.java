/**
 *
 */
package com.lyonscg.paypaladdon.converters.populators.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.session.SessionService;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentRequestType;
import urn.ebay.apis.eBLBaseComponents.DoExpressCheckoutPaymentRequestDetailsType;

import com.paypal.hybris.constants.PaypalConstants;
import com.paypal.hybris.data.DoExpressCheckoutPaymentRequestData;


/**
 * Test class for {@link LeaPPDoExprCheckoutPaymentReqPaymentDetailsPopulator}.
 *
 */
@UnitTest
public class LeaPPDoExprCheckoutPaymentReqPaymentDetailsPopulatorTest
{
	@InjectMocks
	private LeaPPDoExprCheckoutPaymentReqPaymentDetailsPopulator testClass;
	private DoExpressCheckoutPaymentRequestData requestData;
	private DoExpressCheckoutPaymentRequestType request;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private SessionService sessionService;
	@Mock
	private Configuration configuration;
	private final String paypalBnCreditKey = "paypalBnCreditKey", paypalBnKey = "paypalBnKey";
	private final String paypalBnCreditVal = "paypalBnCreditVal", paypalBnVal = "paypalBnVal";

	/**
	 * Setup test data.
	 *
	 * @throws InvalidCartException
	 */
	@Before
	public void setup() throws InvalidCartException
	{
		testClass = new LeaPPDoExprCheckoutPaymentReqPaymentDetailsPopulator();
		MockitoAnnotations.initMocks(this);
		testClass.setPaypalBnCreditKey(paypalBnCreditKey);
		testClass.setPaypalBnKey(paypalBnKey);

		requestData = new DoExpressCheckoutPaymentRequestData();
		request = new DoExpressCheckoutPaymentRequestType();
		request.setDoExpressCheckoutPaymentRequestDetails(new DoExpressCheckoutPaymentRequestDetailsType());

		Mockito.doReturn(configuration).when(configurationService).getConfiguration();
		Mockito.doReturn(paypalBnCreditVal).when(configuration).getString(paypalBnCreditKey);
		Mockito.doReturn(paypalBnVal).when(configuration).getString(paypalBnKey);
	}

	/**
	 * Paypal Express checkout test for
	 * {@link com.lyonscg.paypaladdon.converters.populators.impl.LeaPPDoExprCheckoutPaymentReqPaymentDetailsPopulator#populate(DoExpressCheckoutPaymentRequestData, DoExpressCheckoutPaymentRequestType)}
	 */
	@Test
	public void populatePaypalExpressTest()
	{
		testClass.populate(requestData, request);
		Assert.assertEquals(paypalBnVal, request.getDoExpressCheckoutPaymentRequestDetails().getButtonSource());
	}

	/**
	 * Paypal Credit checkout test for
	 * {@link com.lyonscg.paypaladdon.converters.populators.impl.LeaPPDoExprCheckoutPaymentReqPaymentDetailsPopulator#populate(DoExpressCheckoutPaymentRequestData, DoExpressCheckoutPaymentRequestType)}
	 */
	@Test
	public void populatePaypalCreditTest()
	{
		Mockito.doReturn(Boolean.TRUE).when(sessionService).getAttribute(PaypalConstants.IS_PAYPAL_CREDIT);
		testClass.populate(requestData, request);
		Assert.assertEquals(paypalBnCreditVal, request.getDoExpressCheckoutPaymentRequestDetails().getButtonSource());
	}

	/**
	 * ConversionException test for
	 * {@link com.lyonscg.paypaladdon.converters.populators.impl.LeaPPDoExprCheckoutPaymentReqPaymentDetailsPopulator#populate(DoExpressCheckoutPaymentRequestData, DoExpressCheckoutPaymentRequestType)}
	 */
	@Test(expected = ConversionException.class)
	public void populatePaypalExceptionTest()
	{
		request.setDoExpressCheckoutPaymentRequestDetails(null);
		testClass.populate(requestData, request);
	}
}
