/**
 *
 */
package com.lyonscg.paypaladdon.controllers.pages;

import de.hybris.platform.addonsupport.interceptors.BeforeControllerHandlerAdaptee;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.method.HandlerMethod;


/**
 * Lea-Paypal Before Controller to block certain actions.
 */
public class LeaPPBeforeControllerHandler implements BeforeControllerHandlerAdaptee
{
	private List<String> paypalBlockerURLs;
	private String customRedirectURL;

	/**
	 * @param request
	 *           - HttpServletRequest
	 * @param response
	 *           - HttpServletResponse
	 * @param handler
	 *           - Controller that is supposed to handle the request.
	 * @return - Returns true if there are no changes.
	 */
	@Override
	public boolean beforeController(final HttpServletRequest request, final HttpServletResponse response,
			final HandlerMethod handler) throws Exception
	{
		final String requestURL = request.getRequestURI();
		if (getPaypalBlockerURLs().stream().filter(paypalBlockerURL -> StringUtils.contains(requestURL, paypalBlockerURL))
				.findFirst().isPresent())
		{
			response.sendRedirect(getCustomRedirectURL());
			return false;
		}
		return true;
	}

	protected List<String> getPaypalBlockerURLs()
	{
		return paypalBlockerURLs;
	}

	@Required
	public void setPaypalBlockerURLs(final List<String> paypalBlockerURLs)
	{
		this.paypalBlockerURLs = paypalBlockerURLs;
	}

	protected String getCustomRedirectURL()
	{
		return customRedirectURL;
	}

	@Required
	public void setCustomRedirectURL(final String customRedirectURL)
	{
		this.customRedirectURL = customRedirectURL;
	}
}
