/**
 *
 */
package com.lyonscg.paypaladdon.controllers.pages;

import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.addonsupport.interceptors.BeforeViewHandlerAdaptee;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.ui.ModelMap;


/**
 * Before View handler to change checkout step views.
 */
public class LeaPPBeforeViewHandler implements BeforeViewHandlerAdaptee
{
	private static final String PAY_PAL_CREDIT_SHORTCUT = "payPalCreditShortcut";
	private static final String PAYPAL_EXP_CHECKOUT_COMPONENT_ID = "PayPalExpressCheckoutShortcut";
	private static final String PAYPAL_EXP_CHECKOUT_ATTRIBUTE = "payPalExpressCheckoutShortcut";
	private AcceleratorCheckoutFacade acceleratorCheckoutFacade;
	private String paypalCheckoutSummaryPage, b2bCheckoutSummaryPage, paypalPaymentCode, paypalAddtoCart, leaPaypalAddtoCart,
			accountPaymentType, leaPPCheckoutSummaryPage, cartPage;
	private CMSComponentService cmsComponentService;

	/**
	 * Intercept JSP rendering and changes the view if required.
	 *
	 * @param request
	 *           - HttpServletRequest
	 * @param response
	 *           - HttpServletResponse
	 * @param model
	 *           - Map for use when building model data for use with UI tool.
	 * @param viewName
	 *           - JSP view
	 * @return - JSP view to render
	 */
	@Override
	public String beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
			final String viewName) throws Exception
	{
		if (getPaypalCheckoutSummaryPage().equals(viewName))
		{
			final CartData cartData = getAcceleratorCheckoutFacade().getCheckoutCart();
			if (null != cartData)
			{
				final CCPaymentInfoData paymentInfo = cartData.getPaymentInfo();
				if (null == paymentInfo)
				{
					final B2BPaymentTypeData paymentType = cartData.getPaymentType();
					if (getAccountPaymentType().equals(paymentType.getCode()))
					{
						return getB2bCheckoutSummaryPage();
					}
				}
				else
				{
					if (getPaypalPaymentCode().equals(paymentInfo.getCardType()))
					{
						return getLeaPPCheckoutSummaryPage();
					}
					else
					{
						return getB2bCheckoutSummaryPage();
					}
				}
			}
		}
		else if (getPaypalAddtoCart().equals(viewName))
		{
			model.remove(PAY_PAL_CREDIT_SHORTCUT);
			return getLeaPaypalAddtoCart();
		}
		else if (getCartPage().equals(viewName))
		{
			model.addAttribute(PAYPAL_EXP_CHECKOUT_ATTRIBUTE,
					getCmsComponentService().getSimpleCMSComponent(PAYPAL_EXP_CHECKOUT_COMPONENT_ID));
		}
		return viewName;
	}

	protected AcceleratorCheckoutFacade getAcceleratorCheckoutFacade()
	{
		return acceleratorCheckoutFacade;
	}

	@Required
	public void setAcceleratorCheckoutFacade(final AcceleratorCheckoutFacade acceleratorCheckoutFacade)
	{
		this.acceleratorCheckoutFacade = acceleratorCheckoutFacade;
	}

	protected String getPaypalCheckoutSummaryPage()
	{
		return paypalCheckoutSummaryPage;
	}

	@Required
	public void setPaypalCheckoutSummaryPage(final String paypalCheckoutSummaryPage)
	{
		this.paypalCheckoutSummaryPage = paypalCheckoutSummaryPage;
	}

	protected String getB2bCheckoutSummaryPage()
	{
		return b2bCheckoutSummaryPage;
	}

	@Required
	public void setB2bCheckoutSummaryPage(final String b2bCheckoutSummaryPage)
	{
		this.b2bCheckoutSummaryPage = b2bCheckoutSummaryPage;
	}

	protected String getPaypalPaymentCode()
	{
		return paypalPaymentCode;
	}

	@Required
	public void setPaypalPaymentCode(final String paypalPaymentCode)
	{
		this.paypalPaymentCode = paypalPaymentCode;
	}

	protected String getPaypalAddtoCart()
	{
		return paypalAddtoCart;
	}

	@Required
	public void setPaypalAddtoCart(final String paypalAddtoCart)
	{
		this.paypalAddtoCart = paypalAddtoCart;
	}

	protected String getAccountPaymentType()
	{
		return accountPaymentType;
	}

	@Required
	public void setAccountPaymentType(final String accountPaymentType)
	{
		this.accountPaymentType = accountPaymentType;
	}

	protected String getLeaPaypalAddtoCart()
	{
		return leaPaypalAddtoCart;
	}

	@Required
	public void setLeaPaypalAddtoCart(final String leaPaypalAddtoCart)
	{
		this.leaPaypalAddtoCart = leaPaypalAddtoCart;
	}

	protected String getLeaPPCheckoutSummaryPage()
	{
		return leaPPCheckoutSummaryPage;
	}

	@Required
	public void setLeaPPCheckoutSummaryPage(final String leaPPCheckoutSummaryPage)
	{
		this.leaPPCheckoutSummaryPage = leaPPCheckoutSummaryPage;
	}

	protected CMSComponentService getCmsComponentService()
	{
		return cmsComponentService;
	}

	@Required
	public void setCmsComponentService(final CMSComponentService cmsComponentService)
	{
		this.cmsComponentService = cmsComponentService;
	}

	protected String getCartPage()
	{
		return cartPage;
	}

	@Required
	public void setCartPage(final String cartPage)
	{
		this.cartPage = cartPage;
	}

}
