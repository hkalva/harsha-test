package com.capgemini.customerexport.service.impl;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.customerexport.enums.ExportedCustomerType;
import com.capgemini.customerexport.enums.TimeUnit;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

@UnitTest
public class DefaultLyonscgcustomerexportServiceTest
{

    /**
     * the service to test.
     */
    private DefaultLyonscgcustomerexportService service;

    /**
     * the flexible search service.
     */
    @Mock
    private FlexibleSearchService flexibleSearchService;

    /**
     * the search result.
     */
    @Mock
    private SearchResult<CustomerModel> searchResult;

    /**
     * the customer.
     */
    @Mock
    private CustomerModel customer;

    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        service = new DefaultLyonscgcustomerexportService();
        service.setFlexibleSearchService(flexibleSearchService);
        Mockito.when(flexibleSearchService.search(Mockito.anyString(), Mockito.anyMap())).thenReturn(searchResult);
    }

    /**
     * test searchCustomerForExport invalid customer type.
     */
    @Test
    public void testSearchCustomerForExportInvalidCustomerType()
    {
        List<CustomerModel> result = service.searchCustomerForExport(null, null, TimeUnit.YEAR);
        Assert.assertTrue(CollectionUtils.isEmpty(result));
        Mockito.verify(flexibleSearchService, Mockito.never()).search(Mockito.anyString(), Mockito.anyMap());
    }
    
    /**
     * test searchCustomerForExport no results.
     */
    @Test
    public void testSearchCustomerForExportNoResults()
    {
        List<CustomerModel> result = service.searchCustomerForExport(Integer.valueOf(0), ExportedCustomerType.B2BCUSTOMER, TimeUnit.DAY);
        Assert.assertTrue(CollectionUtils.isEmpty(result));
        Mockito.verify(flexibleSearchService, Mockito.times(1)).search(Mockito.anyString(), Mockito.anyMap());
    }

    /**
     * test searchCustomerForExport.
     */
    @Test
    public void testSearchCustomerForExport()
    {
        Mockito.when(searchResult.getResult()).thenReturn(Collections.singletonList(customer));
        List<CustomerModel> result = service.searchCustomerForExport(Integer.valueOf(1), ExportedCustomerType.CUSTOMER, TimeUnit.MONTH);
        Assert.assertTrue(CollectionUtils.isNotEmpty(result));
        Mockito.verify(flexibleSearchService, Mockito.times(1)).search(Mockito.anyString(), Mockito.anyMap());
    }
    
}
