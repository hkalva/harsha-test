/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customerexport.facade.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.customerexport.config.CustomerDataExportConfig;
import com.capgemini.customerexport.context.CustomerDataContext;
import com.capgemini.customerexport.enums.CustomerExportType;
import com.capgemini.customerexport.enums.ExportedCustomerType;
import com.capgemini.customerexport.enums.TimeUnit;
import com.capgemini.customerexport.facade.impl.DefaultCustomerDataExportFacadeImpl;
import com.capgemini.customerexport.service.LyonscgcustomerexportService;
import com.capgemini.customerexport.writer.CustomerDataFileWriter;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import net.sf.saxon.s9api.SaxonApiException;



/**
 *
 */

@UnitTest
public class DefaultCustomerDataExportFacadeImplTest
{
	@InjectMocks
	DefaultCustomerDataExportFacadeImpl customerDataExportFacade;
	@Mock
	LyonscgcustomerexportService customerExportService;
	@Mock
	Map<CustomerExportType, CustomerDataFileWriter> customerDataFileWriterMap;
	@Mock
	private CustomerDataFileWriter customerDataFileWriter;
	@Mock
	private Converter<B2BCustomerModel, CustomerData> b2bCustomerConverter;
	@Mock
	private Converter<CustomerModel, CustomerData> customerConverter;

	private RendererTemplateModel template;
	private String fileEncoding;

	List<B2BCustomerModel> customers;
	String givenFormat;
	CustomerDataExportConfig config;
	CustomerData data;



	@Before
	public void setUp()
	{
		customers = new ArrayList<B2BCustomerModel>();
		givenFormat = "yyyy-MM-dd-hh-mm-ss-SSS";
		MockitoAnnotations.initMocks(this);
		customerDataExportFacade = new DefaultCustomerDataExportFacadeImpl();
		customerDataExportFacade.setLyonscgcustomerexportService(customerExportService);
		customerDataExportFacade.setCustomerDataFileWriterMap(customerDataFileWriterMap);
		customerDataExportFacade.setB2bCustomerComverter(b2bCustomerConverter);
		customerDataExportFacade.setCustomerConverter(customerConverter);
		final B2BCustomerModel customer = new B2BCustomerModel();
		customer.setName("John Doe");
		customer.setEmail("john.doe@lyonscg.com");
		customer.setUid("john.doe@lyonscg.com");
		final AddressModel address = new AddressModel();
		address.setLine1("20 N Wacker Dr.");
		address.setLine2("Suite 1750");
		final RegionModel region = new RegionModel();
		region.setCountry(new CountryModel("US"));
		address.setRegion(region);
		customer.setDefaultPaymentAddress(address);
		customer.setDefaultShipmentAddress(address);
		final B2BUnitModel unit = new B2BUnitModel();
		unit.setUid("1234");
		unit.setName("test");
		customer.setDefaultB2BUnit(unit);
		customers.add(customer);

		config = new CustomerDataExportConfig();
		config.setCustomerExportStrategy(CustomerExportType.JAXB);
		config.setCustomerType(ExportedCustomerType.B2BCUSTOMER);
		config.setFileExportPath("/opt/hybris/");
		config.setFileNameExt("xml");
		config.setUnit(TimeUnit.DAY);

		data = new CustomerData();
		data.setUid("john.doe@lyonscg.com");
		doReturn(customers).when(customerExportService).searchCustomerForExport(Integer.valueOf(anyInt()),
				any(ExportedCustomerType.class), any(TimeUnit.class));
		given(customerDataFileWriterMap.get(Mockito.any(CustomerExportType.class))).willReturn(customerDataFileWriter);
		given(customerConverter.convert(Mockito.any(CustomerModel.class))).willReturn(data);
		given(b2bCustomerConverter.convert(Mockito.any(B2BCustomerModel.class))).willReturn(data);

	}

	/**
	 * Test method for
	 * {@link com.capgemini.customerexport.facade.impl.DefaultCustomerDataExportFacadeImpl#exportCustomerData(com.capgemini.customerexport.config.CustomerDataExportConfig)}
	 * .
	 */
	@Test
	public void testExportCustomerData()
	{
		try
		{
			given(
					Boolean.valueOf(customerDataFileWriter.writeCustomerDataToFile(Mockito.eq(template), Mockito.any(File.class),
							Mockito.eq(fileEncoding), Mockito.any(CustomerDataContext.class)))).willReturn(Boolean.TRUE);
		}
		catch (final IOException | SaxonApiException e)
		{
			fail(e.getMessage());
		}
		assertTrue(customerDataExportFacade.exportCustomerData(config));
		try
		{
			given(
					Boolean.valueOf(customerDataFileWriter.writeCustomerDataToFile(Mockito.eq(template), Mockito.any(File.class),
							Mockito.eq(fileEncoding), Mockito.any(CustomerDataContext.class)))).willReturn(Boolean.FALSE);
		}
		catch (final IOException  | SaxonApiException e)
		{
			fail(e.getMessage());
		}
		assertFalse(customerDataExportFacade.exportCustomerData(config));

	}


	public void testExportCustomerDataException()
	{
		try
		{
			doThrow(new IOException("Supposed to throw a IOException")).when(customerDataFileWriter).writeCustomerDataToFile(
					Mockito.eq(template), Mockito.any(File.class), Mockito.eq(fileEncoding), Mockito.any(CustomerDataContext.class));
		}
		catch (final IOException  | SaxonApiException e)
		{
			fail(e.getMessage());
		}
		assertFalse("Exception thrown, should be false", customerDataExportFacade.exportCustomerData(config));
	}

	@Test
	public void testExportCustomerDataFail()
	{
		try
		{
			given(
					Boolean.valueOf(customerDataFileWriter.writeCustomerDataToFile(Mockito.eq(template), Mockito.any(File.class),
							Mockito.eq(fileEncoding), Mockito.any(CustomerDataContext.class)))).willReturn(Boolean.FALSE);
		}
		catch (final IOException  | SaxonApiException e)
		{
			fail(e.getMessage());
		}
		assertFalse("exportCustomerData should be false", customerDataExportFacade.exportCustomerData(config));
	}

	/**
	 * Test method for
	 * {@link com.capgemini.customerexport.facade.impl.DefaultCustomerDataExportFacadeImpl#generateFileNamePattern(java.lang.String, java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public void testGenerateFileNamePattern()
	{

		final String path1 = "/hybris/data/";
		final String path2 = "/hybris/data";
		final String prefix = "CustomerData";
		final String suffix1 = "xml";
		final String suffix2 = ".xml";

		final String regex1 = path1 + prefix + "_\\d{17}" + suffix2;

		final String defaultFileName1 = customerDataExportFacade.generateFileNamePattern(prefix, path1, suffix1);
		final String defaultFileName2 = customerDataExportFacade.generateFileNamePattern(prefix, path1, suffix2);
		final String defaultFileName3 = customerDataExportFacade.generateFileNamePattern(prefix, path2, suffix1);
		final String defaultFileName4 = customerDataExportFacade.generateFileNamePattern(prefix, path2, suffix2);

		assertTrue("Received:" + defaultFileName1 + ", Regex: " + regex1 + "\n", Pattern.matches(regex1, defaultFileName1));
		assertTrue("Received:" + defaultFileName2 + ", Regex: " + regex1 + "\n", Pattern.matches(regex1, defaultFileName2));
		assertTrue("Received:" + defaultFileName3 + ", Regex: " + regex1 + "\n", Pattern.matches(regex1, defaultFileName3));
		assertTrue("Received:" + defaultFileName4 + ", Regex: " + regex1 + "\n", Pattern.matches(regex1, defaultFileName4));

		customerDataExportFacade.setTimeFormat(givenFormat);

		final String givenFileName1 = customerDataExportFacade.generateFileNamePattern(prefix, path1, suffix1);
		final String givenFileName2 = customerDataExportFacade.generateFileNamePattern(prefix, path1, suffix2);
		final String givenFileName3 = customerDataExportFacade.generateFileNamePattern(prefix, path2, suffix1);
		final String givenFileName4 = customerDataExportFacade.generateFileNamePattern(prefix, path2, suffix2);

		final String regex2 = path1 + prefix + "_\\d{4}-[0-1]{1}\\d-[0-3]\\d-[0-2][0-4]-[0-6]\\d-[0-6]\\d-\\d{3}" + suffix2;
		assertTrue("Received: " + givenFileName1 + ", Regex: " + regex2 + "\n", Pattern.matches(regex2, givenFileName1));
		assertTrue("Received: " + givenFileName2 + ", Regex: " + regex2 + "\n", Pattern.matches(regex2, givenFileName2));
		assertTrue("Received: " + givenFileName3 + ", Regex: " + regex2 + "\n", Pattern.matches(regex2, givenFileName3));
		assertTrue("Received: " + givenFileName4 + ", Regex: " + regex2 + "\n", Pattern.matches(regex2, givenFileName4));

	}
}
