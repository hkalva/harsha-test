/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customerexport.cronjob;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.solr.common.StringUtils;

import com.capgemini.customerexport.config.CustomerDataExportConfig;
import com.capgemini.customerexport.facade.CustomerDataExportFacade;
import com.capgemini.customerexport.model.CustomerDataExportCronJobModel;


/**
 * CronjobPerformable to perform CustomerDataExportCronjob
 */
public class CustomerDataExportCronJobPerformable extends AbstractJobPerformable<CustomerDataExportCronJobModel>
{
	private static final Logger LOG = Logger.getLogger(CustomerDataExportCronJobPerformable.class);

	@Resource(name = "customerDataExporter")
	private CustomerDataExportFacade customerDataExportFacade;

	@Resource
	private ConfigurationService configurationService;

	/**
	 * Perform cronjob
	 *
	 * @param cronJob
	 *           CustomerDataExportCronJobModel
	 * @return the perform result
	 */
	@Override
	public PerformResult perform(final CustomerDataExportCronJobModel cronJob)
	{
		LOG.info("Starting CustomerDataExportCronJob...");
		final CustomerDataExportConfig exportConfig = new CustomerDataExportConfig();
		exportConfig.setRendererTemplate(cronJob.getRendererTemplate());
		exportConfig.setFileExportPath(cronJob.getExportFilePath());
		exportConfig.setFileNameExt(cronJob.getFileNameExt());
		String fileEncoding = cronJob.getFileEncoding();
		if (StringUtils.isEmpty(fileEncoding))
		{
			fileEncoding = configurationService.getConfiguration().getString("encoding.charset", "UTF-8");
		}
		exportConfig.setFileEncoding(fileEncoding);
		exportConfig.setExportPeriod(cronJob.getExportPeriod());
		exportConfig.setCustomerType(cronJob.getCustomerType());
		exportConfig.setCustomerExportStrategy(cronJob.getExportStrategy());
		exportConfig.setUnit(cronJob.getTimeUnit());


		if (customerDataExportFacade.exportCustomerData(exportConfig))
		{
			LOG.info("CustomerDataExportCronJob completed successfully...");
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		else
		{
			LOG.error("CustomerDataExportCronJob failed...");
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
		}
	}
}
