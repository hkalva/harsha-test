/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customerexport.config;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.capgemini.customerexport.enums.CustomerExportType;
import com.capgemini.customerexport.enums.ExportedCustomerType;
import com.capgemini.customerexport.enums.TimeUnit;
import com.capgemini.services.AbstractExportConfig;





public class CustomerDataExportConfig extends AbstractExportConfig
{
	
	
	/**
	 * Product Export Strategy for exporting orders.
	 */
	private CustomerExportType customerExportStrategy;
	/**
	 * Type of customers to be exported (Customer, User or B2BCustomer)
	 */
	private ExportedCustomerType customerType;

	/**
	 * Hours/Days/Months/Years to look back for exporting updated customers
	 */
	private Integer exportPeriod;
	
	/**
	 * unit 
	 */
	private TimeUnit unit;
	


	/**
	 * @return the customerExportStrategy
	 */
	public CustomerExportType getCustomerExportStrategy()
	{
		return customerExportStrategy;
	}

	/**
	 * @param customerExportStrategy
	 *           the customerExportStrategy to set
	 */
	public void setCustomerExportStrategy(final CustomerExportType customerExportStrategy)
	{
		this.customerExportStrategy = customerExportStrategy;
	}

	/**
	 * @return the customerType
	 */
	public ExportedCustomerType getCustomerType()
	{
		return customerType;
	}

	/**
	 * @param customerType
	 *           the customerType to set
	 */
	public void setCustomerType(final ExportedCustomerType customerType)
	{
		this.customerType = customerType;
	}

	/**
	 * @return the exportPeriod
	 */
	public Integer getExportPeriod()
	{
		return exportPeriod;
	}

	/**
	 * @param exportPeriod
	 *           the exportPeriod to set
	 */
	public void setExportPeriod(final Integer exportPeriod)
	{
		this.exportPeriod = exportPeriod;
	}

	/**
	 * @return the unit
	 */
	public TimeUnit getUnit()
	{
		return unit;
	}

	/**
	 * @param unit
	 *           the unit to set
	 */
	public void setUnit(final TimeUnit unit)
	{
		this.unit = unit;
	}

	@Override
    public String toString()
	{
	   return ToStringBuilder.reflectionToString(this);
     }

}
