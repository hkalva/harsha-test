
/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */

package com.capgemini.customerexport.constants;

/**
 *
 *CapgeminicustomerexportservicesConstants helps these constants through out the extension.
 *
 */

@SuppressWarnings({"deprecation"})
public final class CapgeminicustomerexportservicesConstants extends GeneratedCapgeminicustomerexportservicesConstants
{
    /**
     *  EXTENSIONNAME load the extension name.
     */
    public static final String EXTENSIONNAME = "capgeminicustomerexportservices";
    /**
     * CapgeminicustomerexportservicesConstants constructor.
     */
	private CapgeminicustomerexportservicesConstants()
	{
		//empty
	}
	/**
	 * PLATFORM_LOGO_CODE image loads for the capgeminicustomerexportservices.
	 */
	 public static final String PLATFORM_LOGO_CODE = "lyonscgcustomerexportPlatformLogo";
	
}
