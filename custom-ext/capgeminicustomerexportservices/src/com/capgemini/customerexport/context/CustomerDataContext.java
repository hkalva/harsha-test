/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customerexport.context;

import de.hybris.platform.commercefacades.user.data.CustomerData;

import java.util.Collection;

import org.apache.velocity.VelocityContext;


/**
 * Wrapper for {@link CustomerData} to assist jaxb xml marshalling to a single file.
 */
public class CustomerDataContext extends VelocityContext
{
	private Collection<CustomerData> customerData;

	public Collection<CustomerData> getCustomerData()
	{
		return customerData;
	}

	public void setCustomerData(final Collection<CustomerData> customerData)
	{
		this.customerData = customerData;
	}
}
