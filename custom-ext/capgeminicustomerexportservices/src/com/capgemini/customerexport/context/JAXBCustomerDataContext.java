/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customerexport.context;



/**
 * Wrapper for {@link CustomerDataContext} to assist jaxb xml marshalling.
 */
public class JAXBCustomerDataContext
{
	private CustomerDataContext customerDataContext;

	public CustomerDataContext getCustomerDataContext()
	{
		return customerDataContext;
	}

	public void setCustomerDataContext(final CustomerDataContext customerDataContext)
	{
		this.customerDataContext = customerDataContext;
	}


}
