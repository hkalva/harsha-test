/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customerexport.service.impl;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Required;

import com.capgemini.customerexport.enums.ExportedCustomerType;
import com.capgemini.customerexport.enums.TimeUnit;
import com.capgemini.customerexport.service.LyonscgcustomerexportService;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

/**
 * 
 *This class helps all the customer data export into desired location as specified file format.
 *
 */
public class DefaultLyonscgcustomerexportService implements LyonscgcustomerexportService
{
    /**
     * the flexible search service.
     */
	private FlexibleSearchService flexibleSearchService;
	
	/**
	 * Search Customer Model to be exported
	 *
	 * @param timeValue
	 *           time to look back to search for modified customers
	 * @param customerType
	 *           the customer type, B2BCustomer or Customer
	 * @param unit
	 *           {@link TimeUnit} the unit for timeValue
	 * @return the list a list of B2BCustomerModel or CustomerModel.
	 */
	@Override
	public List<CustomerModel> searchCustomerForExport(final Integer timeValue, final ExportedCustomerType customerType,
			final TimeUnit unit)
	{
	    if (!ExportedCustomerType.B2BCUSTOMER.equals(customerType) && !ExportedCustomerType.CUSTOMER.equals(customerType))
	    {
	        return Collections.emptyList();
	    }
	    
		final Date startDate = getStartDate(timeValue, unit);
		final Map<String, Object> params = new HashMap<>();
		params.put("startDate", startDate);
		String customer = B2BCustomerModel._TYPECODE;
		
		if (ExportedCustomerType.CUSTOMER.equals(customerType))
		{
		    customer = CustomerModel._TYPECODE;
		}

		final String customerPK = "pk";
		final String customerModifiedTime = "modifiedtime";
		final String address = AddressModel._TYPECODE;
		final StringBuilder queryString = new StringBuilder("SELECT {").append(customer).append(":").append(customerPK)
				.append("} ").append("FROM {").append(customer).append(" AS ").append(customer).append(" LEFT JOIN ").append(address)
				.append(" AS ").append(address).append(" on {").append(customer).append(":").append(customerPK).append("} = {")
				.append(address).append(":").append(AddressModel.OWNER).append("}} WHERE {").append(customer).append(":")
				.append(customerModifiedTime).append("} >= ?startDate").append(" OR {").append(address).append(":")
				.append(AddressModel.MODIFIEDTIME).append("} >= ?startDate");
		final SearchResult<CustomerModel> searchResult = getFlexibleSearchService().search(queryString.toString(), params);
		return searchResult.getResult();
	}


	/**
	 * Gets the start date.
	 *
	 * @param timeValue
	 *           time to go back to look for modified customers
	 * @param unit
	 *           {@link TimeUnit} unit of time
	 * @return the start date to look for modified customers
	 */
	private Date getStartDate(final Integer timeValue, final TimeUnit unit)
	{
		final Calendar cal = Calendar.getInstance();
		final Date currentTime = cal.getTime();
		int timeUnit;
		switch (unit)
		{
			case HOUR:
				timeUnit = Calendar.HOUR_OF_DAY;
				break;
			case DAY:
				timeUnit = Calendar.DATE;
				break;
			case MONTH:
				timeUnit = Calendar.MONTH;
				break;
			case YEAR:
				timeUnit = Calendar.YEAR;
				break;
			default:
				timeUnit = Calendar.DATE;
		}
		if (timeValue == null || timeValue.intValue() == 0)
		{
			// If timeValue is not set, start from 0:00 of that day to look for modified customers
			return DateUtils.truncate(currentTime, Calendar.DATE);
		}
		else
		{
			cal.add(timeUnit, timeValue.intValue() * -1); // subtract the hours/days/months/years
			return cal.getTime();
		}
	}

	/**
	 * 
	 * @param flexibleSearchService - the flexible search service.
	 */
	@Required
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}
	
	/**
	 * 
	 * @return FlexibleSearchService
	 */
	protected FlexibleSearchService getFlexibleSearchService()
	{
	    return flexibleSearchService;
	}
}
