/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customerexport.service;

import java.util.List;

import com.capgemini.customerexport.enums.ExportedCustomerType;
import com.capgemini.customerexport.enums.TimeUnit;

import de.hybris.platform.core.model.user.CustomerModel;


/**
 * 
 * This class helps to bring the customer data from database.
 *
 */

public interface LyonscgcustomerexportService
{
    /**
     * Fetch all the customer data from the Database.
     * 
     * @param timeValue
     *            - the time value.
     * @param customerType
     *            - the customer type.
     * @param unit
     *            - the unit of time.
     * @return List<CustomerModel>
     */
    List<CustomerModel> searchCustomerForExport(Integer timeValue, ExportedCustomerType customerType, TimeUnit unit);
}
