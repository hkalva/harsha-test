/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customerexport.writer.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLStreamWriter;

import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.exception.VelocityException;

import com.capgemini.customerexport.context.CustomerDataContext;
import com.capgemini.customerexport.context.JAXBCustomerDataContext;
import com.capgemini.customerexport.writer.CustomerDataFileWriter;
import com.capgemini.services.util.CharsetUtil;

import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import net.sf.saxon.Configuration;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.Serializer.Property;


/**
 * Using JAXB to write customer data to file
 */
public class JAXBCustomerDataFileWriterImpl implements CustomerDataFileWriter
{

	private static final Logger LOG = Logger.getLogger(JAXBCustomerDataFileWriterImpl.class);

	/**
	 * Write customer data to file.
	 *
	 * @param template
	 *           {@link RendererTemplateModel} the template to use for writing data
	 * @param file
	 *           the file for exporting data
	 * @param fileEncoding
	 *           the file encoding used for exporting data
	 * @param context
	 *           the context for exporting data
	 * @return true, if writing customer data to file is successful, otherwise, return false.
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	@Override
	public boolean writeCustomerDataToFile(final RendererTemplateModel template, final File file, final String fileEncoding,
			final VelocityContext context) throws IOException,SaxonApiException
	{
		boolean succeed = false;
		try
		{
			if (file == null || !file.exists())
			{
				throw new IOException();
			}
			if (context instanceof CustomerDataContext)
			{
				succeed = jaxbWriteCustomerDataToFile(file, context);
			}
			else
			{
				throw new VelocityException("Invalid Context");
			}
		}
		catch (VelocityException e)
		{
			LOG.error("Exception writing product details to file", e);
		}
       
        
		return succeed;
	}

	/**
	 * Jaxb write customer data to file.
	 *
	 * @param file
	 *           the file for exporting data
	 * @param context
	 *           the context for exporting data
	 * @return true, if writing customer data to file is successful, otherwise, return false.
	 * @throws IOException 
	 * @throws Throwable 
	 */
	protected boolean jaxbWriteCustomerDataToFile(final File file, final VelocityContext context) throws IOException
	{
		final JAXBCustomerDataContext jaxbCustomerDataContext = new JAXBCustomerDataContext();
		if (!(context instanceof CustomerDataContext))
		{
			LOG.error("Context is not instance of productsdetailscontext, cannot cast to CustomerDataContext");
			return false;
		}
		
		XMLStreamWriter writer = null;
        Serializer serializer = null;
        
		try(FileOutputStream fos = new FileOutputStream(file);
                OutputStreamWriter out = new OutputStreamWriter(fos, CharsetUtil.getCharset());)
		{
		    jaxbCustomerDataContext.setCustomerDataContext((CustomerDataContext) context);
	        final Marshaller marshaller = JAXBContext.newInstance(CustomerDataContext.class).createMarshaller();
	        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
	        final JAXBElement<CustomerDataContext> rootElement = new JAXBElement<>(new QName("CustomerData"),
	                CustomerDataContext.class, jaxbCustomerDataContext.getCustomerDataContext());
	        LOG.debug("Writing to file " + file.getAbsolutePath());
	        
		    Processor processor = new Processor(Configuration.newConfiguration());
            serializer = processor.newSerializer();
            serializer.setOutputProperty(Property.METHOD, "xml");
            serializer.setOutputProperty(Property.INDENT, "yes");
            serializer.setOutputWriter(out);
			
			writer = serializer.getXMLStreamWriter();
			marshaller.marshal(rootElement, writer);
		}
		catch ( FactoryConfigurationError | IOException | JAXBException | SaxonApiException e)
		{
		    LOG.error(" exception occured -jaxbWriteCustomerDataToFile -processing",e);
			
		}
		finally
		{
            if (serializer != null)
            {
                try
                {
                    serializer.close();
                }
                catch (SaxonApiException e)
                {
                    LOG.warn("Error closing serializer", e);
                }
            }
		}

		return true;
	}

}
