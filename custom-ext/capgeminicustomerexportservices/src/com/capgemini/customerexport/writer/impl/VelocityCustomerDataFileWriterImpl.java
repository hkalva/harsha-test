/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customerexport.writer.impl;

import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.commons.renderer.RendererService;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.junit.Assert;

import com.capgemini.customerexport.writer.CustomerDataFileWriter;


/**
 *
 */
public class VelocityCustomerDataFileWriterImpl implements CustomerDataFileWriter
{

	private static final Logger LOG = Logger.getLogger(VelocityCustomerDataFileWriterImpl.class);

	@Resource
	protected RendererService rendererService;

	/**
	 * Write customer data to file.
	 *
	 * @param template
	 *           {@link RendererTemplateModel} the template to use for writing data
	 * @param file
	 *           the file for exporting data
	 * @param fileEncoding
	 *           the file encoding used for exporting data
	 * @param context
	 *           the context for exporting data
	 * @return true, if writing customer data to file is successful, otherwise, return false.
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	@Override
	public boolean writeCustomerDataToFile(final RendererTemplateModel template, final File file, final String fileEncoding,
			final VelocityContext context) throws IOException
	{
		Assert.assertNotNull("Template cannot be null", template);
		Assert.assertNotNull("File cannot be null", file);
		Assert.assertNotNull("Context cannot be null", context);
		Assert.assertTrue("File Encoding cannot be empty", StringUtils.isNotBlank(fileEncoding));
		return velocityRenderTemplateToFile(template, file, fileEncoding, context);
	}

	/**
	 * Write customer data to file using velocity template
	 *
	 * @param template
	 *           {@link RendererTemplateModel} the template to use for writing data
	 * @param file
	 *           the file for exporting data
	 * @param fileEncoding
	 *           the file encoding used for exporting data
	 * @param context
	 *           the context for exporting data
	 * @return true, if writing customer data to file is successful, otherwise, return false.
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	protected boolean velocityRenderTemplateToFile(final RendererTemplateModel template, final File file,
			final String fileEncoding, final VelocityContext context) throws IOException
	{
		final StringWriter productDetailsString = new StringWriter();
		rendererService.render(template, context, productDetailsString);
		LOG.debug("Writing to file " + file.getAbsolutePath());
		FileUtils.writeStringToFile(file, productDetailsString.getBuffer().toString(), fileEncoding);
		return true;
	}

}
