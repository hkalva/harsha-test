/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customerexport.writer;

import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import net.sf.saxon.s9api.SaxonApiException;

import java.io.File;
import java.io.IOException;

import org.apache.velocity.VelocityContext;


/**
 * Customer Data File Writer Interface
 */
public interface CustomerDataFileWriter
{
	boolean writeCustomerDataToFile(final RendererTemplateModel template, final File file, final String fileEncoding,
			final VelocityContext customerDataContext) throws IOException,SaxonApiException;
}
