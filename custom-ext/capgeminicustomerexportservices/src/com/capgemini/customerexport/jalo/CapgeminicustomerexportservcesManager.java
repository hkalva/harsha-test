package com.capgemini.customerexport.jalo;

import org.apache.log4j.Logger;

import com.capgemini.customerexport.constants.CapgeminicustomerexportservicesConstants;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;

@SuppressWarnings("PMD")
public class CapgeminicustomerexportservcesManager extends GeneratedCapgeminicustomerexportservcesManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( CapgeminicustomerexportservcesManager.class.getName() );
	
	public static final CapgeminicustomerexportservcesManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (CapgeminicustomerexportservcesManager) em.getExtension(CapgeminicustomerexportservicesConstants.EXTENSIONNAME);
	}
	
}
