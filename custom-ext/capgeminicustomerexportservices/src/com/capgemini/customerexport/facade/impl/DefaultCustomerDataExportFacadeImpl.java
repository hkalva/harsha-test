/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customerexport.facade.impl;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.apache.solr.common.StringUtils;

import com.capgemini.customerexport.config.CustomerDataExportConfig;
import com.capgemini.customerexport.context.CustomerDataContext;
import com.capgemini.customerexport.enums.CustomerExportType;
import com.capgemini.customerexport.enums.ExportedCustomerType;
import com.capgemini.customerexport.enums.TimeUnit;
import com.capgemini.customerexport.facade.CustomerDataExportFacade;
import com.capgemini.customerexport.service.LyonscgcustomerexportService;
import com.capgemini.customerexport.writer.CustomerDataFileWriter;
import com.meterware.httpunit.controls.IllegalParameterValueException;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import net.sf.saxon.s9api.SaxonApiException;


public class DefaultCustomerDataExportFacadeImpl implements CustomerDataExportFacade
{

	private static final  Logger LOG = Logger.getLogger(DefaultCustomerDataExportFacadeImpl.class);

	private Map<CustomerExportType, CustomerDataFileWriter> customerDataFileWriterMap;

	private String timeFormat;

	@Resource(name = "b2BCustomerConverter")
	private Converter<B2BCustomerModel, CustomerData> b2bCustomerConverter;

	@Resource(name = "customerConverter")
	private Converter<CustomerModel, CustomerData> customerConverter;

	@Resource(name = "capgeminicustomerexportService")
	private LyonscgcustomerexportService lyonscgcustomerexportService;

	/**
	 * Export customer data.
	 *
	 * @param {@link CustomerDataExportConfig} config the config used to control how data are written to the file
	 * @return true, if writing customer data to file is successful.
	 */
	@Override
	public boolean exportCustomerData(final CustomerDataExportConfig config)
	{

        if (config == null)
        {
            LOG.error("CustomerDataExportConfig cannot be null");
            return false;
        }

        final ExportedCustomerType customerType = config.getCustomerType();
        final Integer time = config.getExportPeriod();
        final TimeUnit unit = config.getUnit();
        if (customerType == null)
        {
            LOG.error("Customer type cannot be null");
            return false;
        }

        final List<? extends UserModel> users;
        switch (customerType)
        {
            case CUSTOMER:
            case B2BCUSTOMER:
                users = getData(customerType, time, unit);
                break;
            default:
                LOG.error("Unrecognized customer type - " + customerType.getCode());
                return false;
        }

        return isSuccessful(users,  customerType, time,  unit, config);

         
	}
	
	/**
	 * this method helps to export the customer data and verify it is data exported into file or not.
	 * @param users
	 * @param customerType
	 * @param time
	 * @param unit
	 * @param config
	 * @param isSuccessful
	 * @return
	 * @throws IOException 
	 */
	
	private boolean isSuccessful(List<? extends UserModel> users, ExportedCustomerType customerType,Integer time, TimeUnit unit,CustomerDataExportConfig config) 
	{
	    boolean checkFileExport = true;
	    
	    if (CollectionUtils.isEmpty(users))
        {
            StringBuilder message = new StringBuilder();
            message = displayMessages(message,null,customerType,time,unit);
            LOG.info(message.toString());
            return true;
        }
        else
        {
            StringBuilder message = new StringBuilder();
            message = displayMessages(message,users,customerType,time,unit);
         
            LOG.info(message.toString());

            final String fileExportPath = config.getFileExportPath();
            final String fileNameExtension = config.getFileNameExt();
            if (fileExportPath == null)
            {
                LOG.error("FilePath cannot be null");
                return false;
            }
            else
            {
                 if(!getFilePathExist(fileExportPath))
                 {
                     return false;
                 }
            }
            
            if(!getFileExtension(fileNameExtension))
            {
                return false;
            }
                   
            final CustomerExportType exportType = config.getCustomerExportStrategy();
            
            if(!getExportType(exportType))
            {
                return false;
            }
            
            final CustomerDataFileWriter customerDataFileWriter = getCustomerDataFileWriterMap().get(exportType);
            if (customerDataFileWriter == null)
            {
                LOG.error("No corresponding CustomerDataFileWriter implementations found for " + exportType.getCode());
                return false;
            }

            final MessageFormat fileNameFormat = new MessageFormat(generateFileNamePattern("{0}", config.getFileExportPath(),
                    config.getFileNameExt()));
            
            final CustomerDataContext customerDataContext = new CustomerDataContext();
            List<CustomerData> customerData = new ArrayList<CustomerData>();
            
            customerData =getCustomerData(users, customerType, customerData);
           
            customerDataContext.setCustomerData(customerData);
            
            checkFileExport=getWriteCustomerDataToFile(config,customerDataContext,customerDataFileWriter,fileNameFormat);
            
        }
	    return checkFileExport;
	}
	
	/**
	 * getWriteCustomerDataToFile writing the customer data into the desired file.
	 * @param config
	 * @param customerDataContext
	 * @param customerDataFileWriter
	 * @param fileNameFormat
	 * @return
	 */
	private boolean  getWriteCustomerDataToFile(CustomerDataExportConfig config, CustomerDataContext customerDataContext, CustomerDataFileWriter customerDataFileWriter, MessageFormat fileNameFormat)
	{
	     boolean checkFileExport= true;
	    try
        {
            checkFileExport = writeCustomerDataToFile(config, customerDataContext, customerDataFileWriter, fileNameFormat);
        }
        catch (final IOException e)
        {
            LOG.error("Failed to write to file... ", e);
            checkFileExport = false;
        }
	    return checkFileExport;

	}
	/**
	 * Log message 
	 * @param message
	 * @param users
	 * @param customerType
	 * @param time
	 * @param unit
	 * @return
	 */
	   private StringBuilder displayMessages(StringBuilder message, List<? extends UserModel> users, ExportedCustomerType customerType,
	            Integer time, TimeUnit unit)
	    {
	       if(CollectionUtils.isNotEmpty(users))
	       {
    	        return  message.append(users.size()).append(" ").append(customerType.getCode()).append("(s) have been updated within the last ")
    	        .append(time == null ? 1 : time.intValue()).append(" ").append(unit.getCode()).append("(s).");
	       }
	       else
	       {
	           return message.append("No ").append(customerType.getCode()).append(" has been updated within the last ")
	                    .append(time == null ? 1 : time.intValue()).append(" ").append(unit.getCode()).append("(s).");
	       }
	    }
	
	/**
	 * verify the fileNameExtension is null or not 
	 * @param fileNameExtension
	 * @return
	 */
	
	private boolean getFileExtension(String fileNameExtension)
	{
	    
	    if (fileNameExtension == null)
        {
            LOG.error("FileExt cannot be null");
            return false;
        }
      return true;
	}
	/**
	 * verify the customerexportType
	 * @param exportType
	 * @return
	 */
	private boolean getExportType(CustomerExportType exportType)
	{
	    if (exportType == null)
        {
            LOG.error("Export type cannot be null");
            return false;
        }
	    return true;
	}
	
	/**
	 * It verify file is exist or not.
	 * @param fileExportPath
	 * @return boolean
	 */

	private boolean getFilePathExist(String fileExportPath)
	{
	    boolean flag =true;
	    final File folder = new File(fileExportPath);
        if (!folder.exists())
        {
            LOG.info("Folder [" + fileExportPath + "] doesn't exist, create folder now");
            if (!folder.mkdirs())
            {
                LOG.error("Failed to create folder [" + fileExportPath + "]");
                return false;
            }
            else
            {
                LOG.info("Successfully created folder [" + fileExportPath + "]");
            }
        }
        return flag;
	}
	/**
	 *  Returns the list of customersData.
	 * @param users
	 * @param customerType
	 * @param customerData
	 * @return
	 */
	private List<CustomerData> getCustomerData(List<? extends UserModel> users,ExportedCustomerType customerType,List<CustomerData> customerData)
	{
	    
	    for (final UserModel user : users)
        {
            final CustomerData customer;
            if (customerType.equals(ExportedCustomerType.CUSTOMER))
            {
                customer = customerConverter.convert((CustomerModel) user);
            }
            else
            {
                customer = b2bCustomerConverter.convert((B2BCustomerModel) user);
            }

            customerData.add(customer);
        }
	    return customerData;
	}

	/**
	 * Gets the Customer Data depending on customerType
	 *
	 * @param customerType
	 *           {@link ExportedCustomerType} the customer type, Customer or B2BCustomer
	 * @param timeValue
	 *           time value that we want to include in this export
	 * @param unit
	 *           {@link TimeUnit} the unit of time we want to use for exporting
	 *
	 * @return List<? extends UserModel>, a list of B2BCustomerModel or a list of CustomerModel depending on the given
	 *         parameter
	 */
	public List<CustomerModel> getData(final ExportedCustomerType customerType, final Integer timeValue, final TimeUnit unit)
	{
		List<CustomerModel> customers;
		if (customerType.equals(ExportedCustomerType.B2BCUSTOMER) || customerType.equals(ExportedCustomerType.CUSTOMER))
		{
			customers = lyonscgcustomerexportService.searchCustomerForExport(timeValue, customerType, unit);
		}
		else
		{
			final String[] allowedTypes =
			{ "b2bCustomer", "customer" };
			throw new IllegalParameterValueException("customerType", customerType.getCode(), allowedTypes);
		}
		return customers;
	}


	/**
	 * Write customer data to file.
	 *
	 * @param config
	 *           {@Link CustomerDataExportConfig} config the config used to control how data are written to the
	 *           file
	 * @param customerDataContext
	 *           {@link CustomerDataContext} the customer data context
	 * @param customerDataFileWriter
	 *           {@link CustomerDataFileWriter} the customer data file writer to write the data to file
	 * @param fileNameFormat
	 *           {@link MessageFormat} Formatter to format the file name
	 * @return true, if successfully writing to file, otherwise, return false
	 * @throws IOException
	 *            Exception will be thrown if writing to file failed due to IOException
	 */
	private boolean writeCustomerDataToFile(final CustomerDataExportConfig config, final CustomerDataContext customerDataContext,
			final CustomerDataFileWriter customerDataFileWriter, final MessageFormat fileNameFormat) throws IOException
	{
		boolean succeed;
		final File exportFile = new File(fileNameFormat.format(new Object[]
		{ "CustomerData" }));
		if (!exportFile.exists() && !exportFile.createNewFile())
		{
			
			LOG.error("Failed to create file [" + exportFile.getAbsolutePath() + "]");
			throw new IOException("Failed to create file");
			
		}
		try
		{
			succeed = customerDataFileWriter.writeCustomerDataToFile(config.getRendererTemplate(), exportFile,
					config.getFileEncoding(), customerDataContext);
		}
		catch (final IOException | SaxonApiException e)
		{
			LOG.error("Error writing customer data to file...", e);
			succeed = false;
		}
       
		return succeed;
	}

	/**
	 * Generate file name.
	 *
	 * @param filePrefix
	 *           prefix of the file name
	 * @param filePath
	 *           file path to export
	 * @param fileExt
	 *           file name extension
	 * @return full file name
	 */
	protected String generateFileNamePattern(final String filePrefix, final String filePath, final String fileExt)
	{
		boolean tail = false;
		if (!filePath.endsWith("/"))
		{
			tail = true;
		}
		final Date currentTime = Calendar.getInstance().getTime();
		final String formatter = StringUtils.isEmpty(getTimeFormat()) ? "yyyyMMddHHmmssSSS" : getTimeFormat();
		final SimpleDateFormat dateFormatter = new SimpleDateFormat(formatter);
		final StringBuilder fileName = new StringBuilder();
		fileName.append(filePath);
		if (tail)
		{
			fileName.append("/");
		}
		boolean dotNeeded = false;
		if (!fileExt.startsWith("."))
		{
			dotNeeded = true;
		}

		fileName.append(filePrefix).append("_").append(dateFormatter.format(currentTime));
		if (dotNeeded)
		{
			fileName.append(".");
		}
		fileName.append(fileExt);
		return fileName.toString();

	}


	public Map<CustomerExportType, CustomerDataFileWriter> getCustomerDataFileWriterMap()
	{
		return customerDataFileWriterMap;
	}



	public void setCustomerDataFileWriterMap(final Map<CustomerExportType, CustomerDataFileWriter> customerDataFileWriterMap)
	{
		this.customerDataFileWriterMap = customerDataFileWriterMap;
	}




	public String getTimeFormat()
	{
		return timeFormat;
	}

	public void setTimeFormat(final String timeFormat)
	{
		this.timeFormat = timeFormat;
	}

	public LyonscgcustomerexportService getLyonscgcustomerexportService()
	{
		return lyonscgcustomerexportService;
	}

	public void setLyonscgcustomerexportService(final LyonscgcustomerexportService lyonscgcustomerexportService)
	{
		this.lyonscgcustomerexportService = lyonscgcustomerexportService;
	}

	public Converter<B2BCustomerModel, CustomerData> getB2bCustomerComverter()
	{
		return b2bCustomerConverter;
	}

	public void setB2bCustomerComverter(final Converter<B2BCustomerModel, CustomerData> b2bCustomerComverter)
	{
		this.b2bCustomerConverter = b2bCustomerComverter;
	}

	public Converter<CustomerModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}

	public void setCustomerConverter(final Converter<CustomerModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}
}
