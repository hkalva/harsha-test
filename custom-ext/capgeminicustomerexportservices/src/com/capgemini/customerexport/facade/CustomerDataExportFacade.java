/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customerexport.facade;

import com.capgemini.customerexport.config.CustomerDataExportConfig;


/**
 * 
 * Export the customer data into desired location.
 *
 */

public interface CustomerDataExportFacade
{
    /**
     * This method helps to export the customer data and verify export is successful or failure based on output value.
     * 
     * @param config
     *            - the export configuration.
     * @return boolean
     */
    boolean exportCustomerData(CustomerDataExportConfig config);
}
