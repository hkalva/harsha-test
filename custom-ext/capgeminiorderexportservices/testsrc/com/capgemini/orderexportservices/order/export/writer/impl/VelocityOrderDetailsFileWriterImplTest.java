package com.capgemini.orderexportservices.order.export.writer.impl;

import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.commons.renderer.RendererService;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.orderexportservices.order.export.context.OrderDetailsContext;


/** Unit test for {@link VelocityOrderDetailsFileWriterImpl} */

@UnitTest
public class VelocityOrderDetailsFileWriterImplTest
{
	private VelocityOrderDetailsFileWriterImpl velocityOrderDetailsFileWriterImpl;
	@Mock
	private RendererService rendererService;
	private OrderDetailsContext context;
	private String encoding;
	private RendererTemplateModel template;

	/**
	 * Setup initial data
	 */
	@Before
	public void setUp()
	{
		velocityOrderDetailsFileWriterImpl = new VelocityOrderDetailsFileWriterImpl();
		MockitoAnnotations.initMocks(this);
		velocityOrderDetailsFileWriterImpl.setRendererService(rendererService);
		context = new OrderDetailsContext();
		template = new RendererTemplateModel();
		encoding = "UTF-8";
		context.setOrderData(new OrderData());
		Mockito
				.doNothing()
				.when(rendererService)
				.render(Mockito.any(RendererTemplateModel.class), Mockito.any(OrderDetailsContext.class),
						Mockito.any(StringWriter.class));
	}

	/**
	 * Test Write Order To File
	 *
	 * @throws IOException
	 */
	@Test
	public void testWriteOrderToFile() throws IOException
	{
		final File file = File.createTempFile("test", ".xml");
		final String encoding = "UTF-8";
		Assert.assertTrue(velocityOrderDetailsFileWriterImpl.writeOrderToFile(template, file, encoding, context));
		verify(rendererService).render(Mockito.eq(template), Mockito.eq(context), Mockito.any(StringWriter.class));
		Assert.assertTrue(file.exists());
	}

	/**
	 * Test Assert File null Error
	 *
	 * @throws IOException
	 */
	@Test(expected = AssertionError.class)
	public void testWriteOrderToFileFileNullError() throws IOException
	{
		velocityOrderDetailsFileWriterImpl.writeOrderToFile(null, null, null, context);
	}

	/**
	 * Test Assert Context null Error
	 *
	 * @throws IOException
	 */
	@Test(expected = AssertionError.class)
	public void testWriteOrderToFileContextNullError() throws IOException
	{
		final File file = File.createTempFile("test", ".xml");
		velocityOrderDetailsFileWriterImpl.writeOrderToFile(null, file, null, null);
	}

	/**
	 * Test Assert Encoding null Error
	 *
	 * @throws IOException
	 */
	@Test(expected = AssertionError.class)
	public void testWriteOrderToFileEncodingNullError() throws IOException
	{
		final File file = File.createTempFile("test", ".xml");
		velocityOrderDetailsFileWriterImpl.writeOrderToFile(template, file, null, context);
	}

	/**
	 * Test Assert Template null Error
	 *
	 * @throws IOException
	 */
	@Test(expected = AssertionError.class)
	public void testWriteOrderToFileTemplateNullError() throws IOException
	{
		final File file = File.createTempFile("test", ".xml");
		velocityOrderDetailsFileWriterImpl.writeOrderToFile(null, file, encoding, context);
	}
}
