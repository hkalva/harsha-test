package com.capgemini.orderexportservices.order.export.cronjob;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Collections;
import java.util.HashMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.orderexportservices.enums.OrderExportType;
import com.capgemini.orderexportservices.model.OrderExportCronJobModel;
import com.capgemini.orderexportservices.order.export.OrderExportFacade;
import com.capgemini.orderexportservices.order.export.config.OrderExportConfig;


/**
 * Unit test for {@link OrderExportCronJobPerformable}.
 */
@UnitTest
public class OrderExportCronJobPerformableTest
{
	/**
	 * OrderExportCronJobPerformable to be tested.
	 */
	private OrderExportCronJobPerformable orderExportCronJobPerformable;
	/**
	 * orderExportFacade for OrderExportCronJobPerformable.
	 */
	@Mock
	private OrderExportFacade orderExportFacade;
	/**
	 * cronJob for OrderExportCronJobPerformable.
	 */
	private OrderExportCronJobModel cronJob;

	/**
	 * Setup initial data.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		orderExportCronJobPerformable = new OrderExportCronJobPerformable();
		orderExportCronJobPerformable.setOrderExportFacade(orderExportFacade);
		cronJob = new OrderExportCronJobModel();
		cronJob.setQueryEnabled(Boolean.FALSE);
		cronJob.setQuery("");
		cronJob.setQuerySearchParameter(new HashMap<String, String>());
		cronJob.setStore(new BaseStoreModel());
		cronJob.setOrderStatuses(Collections.singletonList(OrderStatus.APPROVED));
		cronJob.setRendererTemplate(new RendererTemplateModel());
		cronJob.setExportFilePath("");
		cronJob.setFileNameExt("");
		cronJob.setFileEncoding("");
		cronJob.setExportStrategy(OrderExportType.JAXB);
	}

	/**
	 * Test perform success.
	 */
	@Test
	public void testPerformForSuccess()
	{
		given(orderExportFacade.exportOrders(Mockito.any(OrderExportConfig.class))).willReturn(true);
		final PerformResult result = orderExportCronJobPerformable.perform(cronJob);
		verify(orderExportFacade).exportOrders(Mockito.any(OrderExportConfig.class));
		Assert.assertEquals("Cronjob should be successful", CronJobResult.SUCCESS, result.getResult());
		Assert.assertEquals("Cronjob should have finished", CronJobStatus.FINISHED, result.getStatus());
	}

	/**
	 * Test perform failure.
	 */
	@Test
	public void testPerformForFailure()
	{
		given(orderExportFacade.exportOrders(Mockito.any(OrderExportConfig.class))).willReturn(false);
		final PerformResult result = orderExportCronJobPerformable.perform(cronJob);
		verify(orderExportFacade).exportOrders(Mockito.any(OrderExportConfig.class));
		Assert.assertEquals("Cronjob should be successful", CronJobResult.FAILURE, result.getResult());
		Assert.assertEquals("Cronjob should have finished", CronJobStatus.FINISHED, result.getStatus());
	}

}
