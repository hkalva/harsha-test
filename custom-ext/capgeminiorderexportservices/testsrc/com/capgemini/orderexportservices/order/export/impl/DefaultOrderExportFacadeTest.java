package com.capgemini.orderexportservices.order.export.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.capgemini.orderexportservices.enums.OrderExportType;
import com.capgemini.orderexportservices.order.export.config.OrderExportConfig;
import com.capgemini.orderexportservices.order.export.context.OrderDetailsContext;
import com.capgemini.orderexportservices.order.export.writer.OrderDetailsFileWriter;
import com.capgemini.orderexportservices.servicelayer.order.export.impl.DefaultOrderExportService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;


/**
 * Unit test for {@link DefaultOrderExportFacade}.
 */
@UnitTest
public class DefaultOrderExportFacadeTest
{
    /**
     * DefaultOrderExportFacade to be tested.
     */
    @InjectMocks
    private DefaultOrderExportFacade defaultOrderExportFacade;

    /**
     * orderConverter for DefaultOrderExportFacade.
     */
    @Mock
    private Converter<OrderModel, OrderData> orderConverter;
    /**
     * flexibleSearchService for DefaultOrderExportFacade.
     */
    @Mock
    private FlexibleSearchService flexibleSearchService;
    /**
     * orderDetailsFileWriterMap for DefaultOrderExportFacade.
     */
    @Mock
    private Map<OrderExportType, OrderDetailsFileWriter> orderDetailsFileWriterMap;
    /**
     * orderDetailsFileWriter for DefaultOrderExportFacade.
     */
    @Mock
    private OrderDetailsFileWriter orderDetailsFileWriter;
    /**
     * orderExportConfig for Testing.
     */
    private OrderExportConfig orderExportConfig;
    /**
     * store for Testing.
     */
    private BaseStoreModel store;
    /**
     * fileEncoding for Testing.
     */
    private String fileEncoding;
    /**
     * template for Testing.
     */
    private RendererTemplateModel template;
    /**
     * orderStatuses for Testing.
     */
    private List<OrderStatus> orderStatuses;
    /**
     * defQueryParams for Testing.
     */
    private Map<String, Object> defQueryParams;
    /**
     * customQuery for Testing.
     */
    private String customQuery;
    /**
     * customQueryParams for Testing.
     */
    private Map<String, String> customQueryParams;
    /**
     * searchResults for Testing.
     */
    @Mock
    private SearchResult searchResults;
    /**
     * orderModel for Testing.
     */
    @Mock
    private OrderModel orderModel;
    /**
     * orderData for Testing.
     */
    private OrderData orderData;
    /**
     * orderNumber for Testing.
     */
    private String orderNumber;
    /**
     * defQuery for Testing.
     */
    private String defQuery;
    /**
     * Configuration Service class
     */
    @Mock
    private ConfigurationService configurationService;
    @Mock
    private Configuration configuration;
    @Mock
    private CatalogVersionService catalogVersionService;
    @Mock
    private SessionService sessionService;
    @Mock
    private UserService userService;
    @Mock
    private UserModel userModel;
    @Mock
    private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;
    @Mock
    private ModelService modelService;
    @Mock
    private BaseSiteService baseSiteService;
    @Mock
    private BaseSiteModel baseSite;

    private DefaultOrderExportService defaultOrderExportService;

    private static final String CONFIG_STORE_ID_PREFIX = "impex.store.prefix.id";

    /**
     * Setup initial data.
     */
    @Before
    public void setUp()
    {
        orderNumber = "000000001";
        defQuery = "SELECT {pk} FROM {Order} WHERE {store} = ?store AND {status} IN (?statusList) AND {versionID} is NULL";
        customQuery = "SELECT {pk} FROM {Order} WHERE {code} =?code";
        customQueryParams = new HashMap<String, String>(1);
        customQueryParams.put("code", orderNumber);
        defaultOrderExportFacade = new DefaultOrderExportFacade();
        defaultOrderExportService = new DefaultOrderExportService();
        MockitoAnnotations.initMocks(this);
        defaultOrderExportFacade.setDateFormat("yyyy_MM_dd_HH_mm_ss_SSS");
        defaultOrderExportFacade.setOrderExportService(defaultOrderExportService);
        defaultOrderExportService.setFlexibleSearchService(flexibleSearchService);
        defaultOrderExportFacade.setOrderConverter(orderConverter);
        defaultOrderExportFacade.setOrderDetailsFileWriterMap(orderDetailsFileWriterMap);
        defaultOrderExportFacade.setDateTimeFormatter(DateTimeFormatter.ofPattern(defaultOrderExportFacade.getDateFormat()));
        orderData = new OrderData();
        orderData.setCode(orderNumber);

        userModel = new UserModel();

        orderExportConfig = new OrderExportConfig();
        fileEncoding = "UTF-8";
        template = new RendererTemplateModel();
        store = new BaseStoreModel();
        orderStatuses = Collections.singletonList(OrderStatus.APPROVED);
        orderExportConfig.setRendererTemplate(template);
        orderExportConfig.setFileExportPath("/path/");
        orderExportConfig.setFileNameExt(".xml");
        orderExportConfig.setFileEncoding(fileEncoding);

        defQueryParams = new HashMap<String, Object>(2);
        defQueryParams.put("store", store);
        defQueryParams.put("statusList", orderStatuses);

        given(flexibleSearchService.search(anyString(), anyMap())).willReturn(searchResults);
        given(orderConverter.convert(Mockito.any(OrderModel.class))).willReturn(orderData);
        given(searchResults.getResult()).willReturn(Collections.singletonList(orderModel));
        given(orderDetailsFileWriterMap.get(Mockito.any(OrderExportType.class))).willReturn(orderDetailsFileWriter);
        given(configurationService.getConfiguration()).willReturn(configuration);
        given(configurationService.getConfiguration().getString(CONFIG_STORE_ID_PREFIX)).willReturn("lea");
        given(orderModel.getUser()).willReturn(userModel);
        given(baseSiteService.getCurrentBaseSite()).willReturn(baseSite);
        given(baseSite.getUid()).willReturn("lea");
        //To execute & return the value from the local session execution method
        Mockito.when(sessionService.executeInLocalView(Mockito.any(SessionExecutionBody.class))).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                final SessionExecutionBody args = (SessionExecutionBody) invocation.getArguments()[0];
                return args.execute();
            }
        });

    }

    /**
     * Test default order export for success.
     *
     * @throws IOException
     *             When writing to file
     */
    @Test
    public void testDefExportOrdersForSuccess() throws IOException
    {
        populateForDefaultQuery();
        given(
                orderDetailsFileWriter.writeOrderToFile(Mockito.eq(template), Mockito.any(File.class), Mockito.eq(fileEncoding),
                        Mockito.any(OrderDetailsContext.class))).willReturn(true);

        Assert.assertTrue(defaultOrderExportFacade.exportOrders(orderExportConfig));
        verify(flexibleSearchService).search(defQuery, defQueryParams);
        verify(orderConverter).convert(orderModel);
        verify(orderDetailsFileWriter).writeOrderToFile(Mockito.eq(template), Mockito.any(File.class), Mockito.eq(fileEncoding),
                Mockito.any(OrderDetailsContext.class));
    }

    /**
     * Test default order export for success and make them wait for order export.
     *
     * @throws IOException
     *             When writing to file
     */
    @Test
    public void testDefExportOrdersForSuccessWithOrderExport() throws IOException
    {
        given(orderModel.getStatus()).willReturn(OrderStatus.EXPORT_READY);
        testDefExportOrdersForSuccess();
        verify(orderModel).setStatus(OrderStatus.WAIT_FOR_UPDATE);
        verify(modelService).save(orderModel);
    }

    /**
     * Test default order export for success and make them skip wait for order export.
     *
     * @throws IOException
     *             When writing to file
     */
    @Test
    public void testDefExportOrdersForSuccessSkipOrderExport() throws IOException
    {
        given(orderModel.getStatus()).willReturn(OrderStatus.CREATED);
        testDefExportOrdersForSuccess();
        verify(orderModel, Mockito.never()).setStatus(OrderStatus.WAIT_FOR_UPDATE);
        verify(modelService, Mockito.never()).save(orderModel);
    }

    /**
     * Test default order export for failure.
     *
     * @throws IOException
     *             When writing to file
     */
    @Test
    public void testDefExportOrdersForFailure() throws IOException
    {
        populateForDefaultQuery();
        given(
                orderDetailsFileWriter.writeOrderToFile(Mockito.any(RendererTemplateModel.class), Mockito.any(File.class),
                        Mockito.any(String.class), Mockito.any(OrderDetailsContext.class))).willThrow(new IOException());
        Assert.assertFalse(defaultOrderExportFacade.exportOrders(orderExportConfig));
        verify(flexibleSearchService).search(defQuery, defQueryParams);
        verify(orderConverter).convert(orderModel);
        verify(orderDetailsFileWriter).writeOrderToFile(Mockito.eq(template), Mockito.any(File.class), Mockito.eq(fileEncoding),
                Mockito.any(OrderDetailsContext.class));
    }

    /**
     * Test default order export for null store.
     *
     */
    @Test(expected = AssertionError.class)
    public void testDefExportOrdersForNullStore()
    {
        populateForDefaultQuery();
        orderExportConfig.setStore(null);
        defaultOrderExportFacade.exportOrders(orderExportConfig);
    }

    /**
     * Test default order export for null order status.
     *
     */
    @Test(expected = AssertionError.class)
    public void testDefExportOrdersForNullOrderStatus()
    {
        populateForDefaultQuery();
        orderExportConfig.setOrderStatuses(null);
        defaultOrderExportFacade.exportOrders(orderExportConfig);
    }

    /**
     * Test custom order export for success.
     *
     * @throws IOException
     *             When writing to file
     */
    @Test
    public void testCustomExportOrdersForSuccess() throws IOException
    {
        populateForCustomQuery();
        given(
                orderDetailsFileWriter.writeOrderToFile(Mockito.any(RendererTemplateModel.class), Mockito.any(File.class),
                        Mockito.any(String.class), Mockito.any(OrderDetailsContext.class))).willReturn(true);
        Assert.assertTrue(defaultOrderExportFacade.exportOrders(orderExportConfig));
        verify(flexibleSearchService).search(customQuery, customQueryParams);
        verify(orderConverter).convert(orderModel);
        verify(orderDetailsFileWriter).writeOrderToFile(Mockito.eq(template), Mockito.any(File.class), Mockito.eq(fileEncoding),
                Mockito.any(OrderDetailsContext.class));
    }

    /**
     * Test custom order export for failure.
     *
     * @throws IOException
     *             When writing to file
     */
    @Test
    public void testCustomExportOrdersForFailure() throws IOException
    {
        populateForCustomQuery();
        given(
                orderDetailsFileWriter.writeOrderToFile(Mockito.eq(template), Mockito.any(File.class), Mockito.eq(fileEncoding),
                        Mockito.any(OrderDetailsContext.class))).willThrow(new IOException());
        Assert.assertFalse(defaultOrderExportFacade.exportOrders(orderExportConfig));
        verify(flexibleSearchService).search(customQuery, customQueryParams);
        verify(orderConverter).convert(orderModel);
        verify(orderDetailsFileWriter).writeOrderToFile(Mockito.eq(template), Mockito.any(File.class), Mockito.eq(fileEncoding),
                Mockito.any(OrderDetailsContext.class));
    }

    /**
     * Test custom order export for null query.
     *
     */
    @Test(expected = AssertionError.class)
    public void testCustomExportOrdersForNullQuery()
    {
        populateForCustomQuery();
        orderExportConfig.setQuery(null);
        defaultOrderExportFacade.exportOrders(orderExportConfig);
    }

    /**
     * Test custom order export for null query
     *
     */
    @Test(expected = AssertionError.class)
    public void testCustomExportOrdersForFilePathQuery()
    {
        populateForCustomQuery();
        orderExportConfig.setFileExportPath(null);
        defaultOrderExportFacade.exportOrders(orderExportConfig);
    }

    /**
     * Test custom order export for null query
     *
     */
    @Test(expected = AssertionError.class)
    public void testCustomExportOrdersForFileExtQuery()
    {
        populateForCustomQuery();
        orderExportConfig.setFileNameExt(null);
        defaultOrderExportFacade.exportOrders(orderExportConfig);
    }

    private void populateForDefaultQuery()
    {
        orderExportConfig.setIsQueryEnabled(Boolean.FALSE);
        orderExportConfig.setOrderExportStrategy(OrderExportType.VELOCITY);
        orderExportConfig.setOrderStatuses(orderStatuses);
        orderExportConfig.setStore(store);
        orderExportConfig.setQuery(null);
        orderExportConfig.setQueryParams(null);
    }

    private void populateForCustomQuery()
    {
        orderExportConfig.setIsQueryEnabled(Boolean.TRUE);
        orderExportConfig.setOrderExportStrategy(OrderExportType.VELOCITY);
        orderExportConfig.setOrderStatuses(null);
        orderExportConfig.setStore(null);
        orderExportConfig.setQuery(customQuery);
        orderExportConfig.setQueryParams(customQueryParams);
    }
}
