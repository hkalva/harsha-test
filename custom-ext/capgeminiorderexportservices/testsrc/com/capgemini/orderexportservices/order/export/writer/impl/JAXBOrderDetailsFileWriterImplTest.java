package com.capgemini.orderexportservices.order.export.writer.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;

import com.capgemini.orderexportservices.order.export.context.OrderDetailsContext;


/** Unit test for {@link JAXBOrderDetailsFileWriterImpl}. */

@RunWith(HybrisJUnit4ClassRunner.class)
@UnitTest
public class JAXBOrderDetailsFileWriterImplTest
{
	/**
	 * JAXBOrderDetailsFileWriterImpl to be tested.
	 */
	private JAXBOrderDetailsFileWriterImpl jaxbOrderDetailsFileWriterImpl;
	/**
	 * OrderDetails Context.
	 */
	private OrderDetailsContext context;

	/**
	 * Setup initial data.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		jaxbOrderDetailsFileWriterImpl = new JAXBOrderDetailsFileWriterImpl();
		context = new OrderDetailsContext();
		context.setOrderData(new OrderData());
	}

	/**
	 * Test Write Order To File.
	 *
	 * @throws IOException
	 *            When writing to file
	 */
	@Test
	public void testWriteOrderToFile() throws IOException
	{
		final File file = File.createTempFile("test", ".xml");
		Assert.assertTrue(jaxbOrderDetailsFileWriterImpl.writeOrderToFile(null, file, null, context));
		Assert.assertTrue(file.exists());

	}

	/**
	 * Test Assert File null Error.
	 *
	 * @throws IOException
	 *            When writing to file
	 */
	@Test(expected = AssertionError.class)
	public void testWriteOrderToFileFileNullError() throws IOException
	{
		jaxbOrderDetailsFileWriterImpl.writeOrderToFile(null, null, null, context);
	}

	/**
	 * Test Assert Context null Error.
	 *
	 * @throws IOException
	 *            When writing to file
	 */
	@Test(expected = AssertionError.class)
	public void testWriteOrderToFileContextNullError() throws IOException
	{
		final File file = File.createTempFile("test", ".xml");
		jaxbOrderDetailsFileWriterImpl.writeOrderToFile(null, file, null, null);
	}
}
