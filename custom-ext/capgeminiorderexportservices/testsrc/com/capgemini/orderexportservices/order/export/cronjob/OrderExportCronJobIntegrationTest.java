package com.capgemini.orderexportservices.order.export.cronjob;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.cronjob.CronJobDao;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.io.File;
import java.io.FilenameFilter;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.capgemini.orderexportservices.model.OrderExportCronJobModel;


/**
 * Integration for {@link OrderExportCronJobPerformable}.
 */
@IntegrationTest
public class OrderExportCronJobIntegrationTest extends ServicelayerTransactionalTest
{
	/**
	 * Impex location for testing.
	 */
	private static final String IMPEX_LOCATION = "/capgeminiorderexport/test/impex/testOrderExportCronJob.impex";
	/**
	 * Output order xml file path.
	 */
	private static final String ORDER_XML_FILE_PATH = "/opt/lyonscg/sapcc-rlp/hybris-suite/hybris/temp/hybris/junit_capgeminiorderexport/";
	/**
	 * OrderExportCronJobPerformable to be tested.
	 */
	@Resource
	private OrderExportCronJobPerformable orderExportCronJob;
	/**
	 * CronJobDao to query cronjobs.
	 */
	@Resource
	private CronJobDao cronJobDao;
	/**
	 * different OrderExportCronJobModels.
	 */
	private OrderExportCronJobModel orderExportJAXQueryBased, orderExportVelocityQueryBasedFul, orderExportJAXDefaultQuery,
			orderExportVelocityDefaultQueryFul;
	/**
	 * default order number.
	 */
	private String orderNumber;

	/**
	 * Setup data for integration test.
	 *
	 * @throws Exception
	 *            Setup test data
	 */
	@Before
	public void setUp() throws Exception
	{
		setupTestDataEnvironment();
		final File directory = new File(ORDER_XML_FILE_PATH);
		if (!directory.exists())
		{
			directory.mkdirs();
		}
		deleteOrderXMLs();
		orderExportJAXQueryBased = (OrderExportCronJobModel) cronJobDao.findCronJobs("orderExportJAXQueryBased").iterator().next();
		orderExportVelocityQueryBasedFul = (OrderExportCronJobModel) cronJobDao.findCronJobs("orderExportVelocityQueryBasedFul")
				.iterator().next();
		orderExportJAXDefaultQuery = (OrderExportCronJobModel) cronJobDao.findCronJobs("orderExportJAXDefaultQuery").iterator()
				.next();
		orderExportVelocityDefaultQueryFul = (OrderExportCronJobModel) cronJobDao
				.findCronJobs("orderExportVelocityDefaultQueryFul").iterator().next();
		orderNumber = "exportTestOrder";
	}

	/**
	 * Tear down.
	 */
	@After
	public void tearDown()
	{
		deleteOrderXMLs();
	}

	/**
	 * Test JAXB Order export custom Query.
	 *
	 * @throws Exception
	 */
	@Test
	public void testOrderJAXBExportCustomQuery() throws Exception
	{
		final PerformResult result = orderExportCronJob.perform(orderExportJAXQueryBased);
		Assert.assertEquals("Cronjob should be successful", CronJobResult.SUCCESS, result.getResult());
		Assert.assertEquals("Cronjob should have finished", CronJobStatus.FINISHED, result.getStatus());
		final File[] matchingFiles = findOrderXml(orderNumber);
		Assert.assertTrue(matchingFiles != null && matchingFiles.length == 1);
		validateOrderXML(matchingFiles[0]);
	}

	/**
	 * Test Velocity Order export custom query
	 *
	 * @throws Exception
	 */
	@Test
	public void testOrderVelocityExportCustomQuery() throws Exception
	{
		final PerformResult result = orderExportCronJob.perform(orderExportVelocityQueryBasedFul);
		Assert.assertEquals("Cronjob should be successful", CronJobResult.SUCCESS, result.getResult());
		Assert.assertEquals("Cronjob should have finished", CronJobStatus.FINISHED, result.getStatus());
		final File[] matchingFiles = findOrderXml(orderNumber);
		Assert.assertTrue(matchingFiles != null && matchingFiles.length == 1);
		validateOrderXML(matchingFiles[0]);
	}

	/**
	 * Test JAXB Order export default query
	 *
	 * @throws Exception
	 */
	@Test
	public void testOrderJAXBExportDefaultQuery() throws Exception
	{
		final PerformResult result = orderExportCronJob.perform(orderExportJAXDefaultQuery);
		Assert.assertEquals("Cronjob should be successful", CronJobResult.SUCCESS, result.getResult());
		Assert.assertEquals("Cronjob should have finished", CronJobStatus.FINISHED, result.getStatus());
		final File[] matchingFiles = findOrderXml(orderNumber);
		Assert.assertTrue(matchingFiles != null && matchingFiles.length == 1);
		validateOrderXML(matchingFiles[0]);
	}

	/**
	 * Test Velocity Order export default query
	 *
	 * @throws Exception
	 */
	@Test
	public void testOrderVelocityExportDefaultQuery() throws Exception
	{
		final PerformResult result = orderExportCronJob.perform(orderExportVelocityDefaultQueryFul);
		Assert.assertEquals("Cronjob should be successful", CronJobResult.SUCCESS, result.getResult());
		Assert.assertEquals("Cronjob should have finished", CronJobStatus.FINISHED, result.getStatus());
		final File[] matchingFiles = findOrderXml(orderNumber);
		Assert.assertTrue(matchingFiles != null && matchingFiles.length == 1);
		validateOrderXML(matchingFiles[0]);
	}

	private File[] findOrderXml(final String orderNumber)
	{
		final File dir = new File(ORDER_XML_FILE_PATH);
		final File[] matchingFiles = dir.listFiles(new FilenameFilter()
		{
			public boolean accept(final File dir, final String name)
			{
				return name.contains(orderNumber) && name.contains(".xml");
			}
		});
		return matchingFiles;
	}

	private void setupTestDataEnvironment() throws ImpExException
	{
		importCsv(IMPEX_LOCATION, "utf-8");
	}

	private void deleteOrderXMLs()
	{
		final File dir = new File(ORDER_XML_FILE_PATH);
		final File[] matchingFiles = dir.listFiles(new FilenameFilter()
		{
			public boolean accept(final File dir, final String name)
			{
				return name.contains(".xml");
			}
		});
		for (final File file : matchingFiles)
		{
			file.delete();
		}
	}

	private void validateOrderXML(final File orderXml) throws Exception
	{
		final String orderXmlString = FileUtils.readFileToString(orderXml, "UTF-8");
		Assert.assertTrue(orderXmlString.contains("<code>" + orderNumber + "</code>"));
	}
}
