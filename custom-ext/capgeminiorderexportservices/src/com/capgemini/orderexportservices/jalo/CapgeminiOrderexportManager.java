package com.capgemini.orderexportservices.jalo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.orderexportservices.constants.CapgeminiOrderexportConstants;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;

@SuppressWarnings("PMD")
public class CapgeminiOrderexportManager extends GeneratedCapgeminiOrderexportManager
{
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger( CapgeminiOrderexportManager.class.getName() );
	
	public static final CapgeminiOrderexportManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (CapgeminiOrderexportManager) em.getExtension(CapgeminiOrderexportConstants.EXTENSIONNAME);
	}
	
}
