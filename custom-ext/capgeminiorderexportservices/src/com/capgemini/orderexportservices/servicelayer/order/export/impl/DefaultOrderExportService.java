/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.orderexportservices.servicelayer.order.export.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.slf4j.Logger;
import org.junit.Assert;
import org.slf4j.LoggerFactory;

import com.capgemini.orderexportservices.servicelayer.order.export.OrderExportService;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;


/**
 * FlexibleSearch query for orders to export.
 * 
 * @author lyonscg
 *
 */
public class DefaultOrderExportService implements OrderExportService
{
    /**
     * LOGGER for DefaultOrderExportService.
     */
    private static final Logger LOG = LoggerFactory.getLogger(DefaultOrderExportService.class);

    /**
     * Default query.
     */
    private static final String REF_QUERY_DEFAULT = "SELECT {pk} FROM {Order} WHERE {store} = ?store AND {status} IN (?statusList) AND {versionID} is NULL";

    /**
     * Flexiblesearchservice to query orders.
     */
    private FlexibleSearchService flexibleSearchService;

    @Override
    public List<OrderModel> getOrders(final Boolean isQueryEnabled, final String query, final Map<String, String> params,
            final BaseStoreModel store, final List<OrderStatus> orderStatuses)
    {
        SearchResult<OrderModel> results = null;
        if (BooleanUtils.isTrue(isQueryEnabled))
        {
            Assert.assertNotNull("Query cannot be null", query);
            LOG.info("Querying for orders using input query....");
            results = getFlexibleSearchService().search(query, params);
        }
        else
        {
            Assert.assertNotNull("Store cannot be null", store);
            Assert.assertFalse("orderStatuses cannot be Empty", CollectionUtils.isEmpty(orderStatuses));
            final Map<String, Object> queryParams = new HashMap<String, Object>(2);
            queryParams.put("store", store);
            queryParams.put("statusList", orderStatuses);
            LOG.info("Querying for orders using default query....");
            results = getFlexibleSearchService().search(REF_QUERY_DEFAULT, queryParams);
        }
        return results.getResult();
    }

    public FlexibleSearchService getFlexibleSearchService()
    {
        return flexibleSearchService;
    }

    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
    {
        this.flexibleSearchService = flexibleSearchService;
    }
}
