/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.orderexportservices.servicelayer.order.export;

import java.util.List;
import java.util.Map;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.store.BaseStoreModel;


/**
 * Interface for DefaultOrderService.
 *
 * @author lyonscg
 *
 */
public interface OrderExportService
{

    /**
     * Return orders based on parameters provided.
     * 
     * @param isQueryEnabled
     *            - flag if query is enabled.
     * @param query
     *            - the query.
     * @param params
     *            - key-value pair for parameters.
     * @param store
     *            - the store.
     * @param orderStatuses
     *            - the list of order status.
     * @return List<OrderModel>
     */
    List<OrderModel> getOrders(final Boolean isQueryEnabled, final String query, final Map<String, String> params,
            final BaseStoreModel store, final List<OrderStatus> orderStatuses);

}
