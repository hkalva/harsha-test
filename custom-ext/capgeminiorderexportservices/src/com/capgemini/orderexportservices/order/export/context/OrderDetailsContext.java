/**
 *
 */
package com.capgemini.orderexportservices.order.export.context;

import de.hybris.platform.commercefacades.order.data.OrderData;

import org.apache.velocity.VelocityContext;


/**@author lyonscg.
 * Implements Order detail context class.
 */
public class OrderDetailsContext extends VelocityContext
{
	private OrderData orderData;

	/**
	 * @return the orderData
	 */
	public OrderData getOrderData()
	{
		return orderData;
	}

	/**
	 * @param orderData
	 *           the orderData to set
	 */
	public void setOrderData(final OrderData orderData)
	{
		this.orderData = orderData;
	}
}
