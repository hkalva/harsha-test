/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.orderexportservices.order.export.writer.impl;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.orderexportservices.order.export.context.OrderDetailsContext;
import com.capgemini.orderexportservices.order.export.writer.OrderDetailsFileWriter;

import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.commons.renderer.RendererService;


/**
 * Velocity template based OrderDetailsFileWriter implementation.
 */
public class VelocityOrderDetailsFileWriterImpl implements OrderDetailsFileWriter
{
	private static final Logger LOG = LoggerFactory.getLogger(VelocityOrderDetailsFileWriterImpl.class.getName());
	private RendererService rendererService;

	@Override
	public boolean writeOrderToFile(final RendererTemplateModel template, final File file, final String fileEncoding,
			final OrderDetailsContext context) throws IOException
	{
		Assert.assertNotNull("Template cannot be null", template);
		Assert.assertNotNull("File cannot be null", file);
		Assert.assertNotNull("Context cannot be null", context);
		Assert.assertTrue("File Encoding cannot be empty", StringUtils.isNotBlank(fileEncoding));
		final StringWriter orderDetailsString = new StringWriter();
		getRendererService().render(template, context, orderDetailsString);
		LOG.debug("Writing to file " + file.getAbsolutePath());
		FileUtils.writeStringToFile(file, orderDetailsString.getBuffer().toString(), fileEncoding);
		return true;
	}

	public RendererService getRendererService()
	{
		return rendererService;
	}

	public void setRendererService(final RendererService rendererService)
	{
		this.rendererService = rendererService;
	}

}
