/**
 *
 */
package com.capgemini.orderexportservices.order.export.writer.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamWriter;

import net.sf.saxon.Configuration;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.Serializer.Property;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.orderexportservices.order.export.context.OrderDetailsContext;
import com.capgemini.orderexportservices.order.export.writer.OrderDetailsFileWriter;
import com.capgemini.services.util.CharsetUtil;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;


/**
 * @author lyonscg. implementation of JAXB order details file writer.
 */
public class JAXBOrderDetailsFileWriterImpl implements OrderDetailsFileWriter
{
    /**
     * the logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(JAXBOrderDetailsFileWriterImpl.class);

    @Override
    public boolean writeOrderToFile(final RendererTemplateModel template, final File file, final String fileEncoding,
            final OrderDetailsContext context) throws IOException
    {
        Serializer serializer = null;
        
        
        try (OutputStreamWriter outputStream = new OutputStreamWriter(new FileOutputStream(file), CharsetUtil.getCharset()))
        {
            Assert.assertNotNull("File cannot be null", file);
            Assert.assertNotNull("Context cannot be null", context);
            Processor processor = new Processor(Configuration.newConfiguration());
            serializer = processor.newSerializer();
            serializer.setOutputProperty(Property.METHOD, "xml");
            serializer.setOutputProperty(Property.INDENT, "yes");
            serializer.setOutputWriter(outputStream);
            final Marshaller marshaller = JAXBContext.newInstance(OrderData.class).createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            final JAXBElement<OrderData> rootElement = new JAXBElement<>(new QName("OrderDetails"), OrderData.class,
                    context.getOrderData());
            LOG.debug("Writing to file {}", file.getAbsolutePath());
            XMLStreamWriter writer = serializer.getXMLStreamWriter();
            marshaller.marshal(rootElement, writer);
            return true;
        }
        catch (final JAXBException | SaxonApiException e)
        {
            LOG.error("Exception writing order details to file", e);
        }
        finally
        {
            if (serializer != null)
            {
                try
                {
                    serializer.close();
                }
                catch (SaxonApiException e)
                {
                    LOG.warn("Error closing serializer", e);
                }
            }
        }
        
        return false;
    }

    
}
