/**
 * Copyright (c) 2019 Capgemini. All rights reserved.
 *
 * This software is the confidential and proprietary information of Capgemini
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Capgemini.
 */
package com.capgemini.orderexportservices.order.export.impl;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.capgemini.orderexportservices.enums.OrderExportType;
import com.capgemini.orderexportservices.order.export.OrderExportFacade;
import com.capgemini.orderexportservices.order.export.config.OrderExportConfig;
import com.capgemini.orderexportservices.order.export.context.OrderDetailsContext;
import com.capgemini.orderexportservices.order.export.writer.OrderDetailsFileWriter;
import com.capgemini.orderexportservices.servicelayer.order.export.OrderExportService;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;


/**
 * Default implementation of {@link OrderExportFacade}.
 */
public class DefaultOrderExportFacade implements OrderExportFacade
{
    /**
     * LOGGER for DefaultOrderExportFacade.
     */
    private static final Logger LOG = LoggerFactory.getLogger(DefaultOrderExportFacade.class);
    
    /**
     * OrderModel to OrderData converter.
     */
    private Converter<OrderModel, OrderData> orderConverter;
    
    /**
     * Map of different OrderDetailsFileWriter implementations.
     */
    private Map<OrderExportType, OrderDetailsFileWriter> orderDetailsFileWriterMap;
    
    /**
     * Order export file name date format.
     */
    private String dateFormat;
    
    /**
     * Date time formatter for order export file names.
     */
    private DateTimeFormatter dateTimeFormatter;

    /**
     * catalogVersionService to set the Specific catalog to retrieve products from.
     */
    private CatalogVersionService catalogVersionService;

    /**
     * To create a Local session to execute in a different user session.
     */
    private SessionService sessionService;

    /**
     * To set the user in the Session.
     */
    private UserService userService;
    
    /**
     * Service for getting orders.
     */
    private OrderExportService orderExportService;
    
    /**
     * To set the Branch specific to the Customer in the session.
     */
    private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;

    /**
     * BaseSiteService bean to set the base site within the local session.
     */
    private BaseSiteService baseSiteService;

    /**
     * the model service.
     */
    private ModelService modelService;

    @Override
    public boolean exportOrders(final OrderExportConfig exportConfig)
    {
        Assert.assertNotNull("OrderExportConfig cannot be null", exportConfig);
        boolean result = true;
        final List<OrderModel> orders = getOrderExportService().getOrders(exportConfig.getIsQueryEnabled(),
                exportConfig.getQuery(), exportConfig.getQueryParams(), exportConfig.getStore(), exportConfig.getOrderStatuses());
        
        if (CollectionUtils.isEmpty(orders))
        {
            LOG.warn("No Orders to export");
        }
        else
        {
            Assert.assertNotNull("FilePath cannot be null", exportConfig.getFileExportPath());
            Assert.assertNotNull("FileExt cannot be null", exportConfig.getFileNameExt());
            final OrderDetailsFileWriter orderDetailsFileWriter = getOrderDetailsFileWriterMap()
                    .get(exportConfig.getOrderExportStrategy());
            Assert.assertNotNull("No corresponding OrderDetailsFileWriter implementations found", orderDetailsFileWriter);
            final MessageFormat fileNameFormat = new MessageFormat(
                    generateFileNamePattern("{0}", exportConfig.getFileExportPath(), exportConfig.getFileNameExt()));
            
            for (final OrderModel order : orders)
            {
                result = result && exportOrder(order, orderDetailsFileWriter, exportConfig.getRendererTemplate(), fileNameFormat,
                        exportConfig.getFileEncoding()).booleanValue();
            }
        }
        
        return result;
    }

    private String generateFileNamePattern(final String orderNumber, final String filePath, final String fileExt)
    {
        final String fileName = filePath + orderNumber;
        
        if (getDateTimeFormatter() != null)
        {
            return fileName + "_" + ZonedDateTime.now().format(getDateTimeFormatter()) + fileExt;
        }
        
        return fileName + fileExt;
    }

    @PostConstruct
    private void initDateTimeFormatter()
    {
        final String dateFormatString = getDateFormat();
        
        if (StringUtils.isNotEmpty(dateFormatString))
        {
            setDateTimeFormatter(DateTimeFormatter.ofPattern(dateFormatString));
        }
    }

    @Required
    public void setOrderDetailsFileWriterMap(final Map<OrderExportType, OrderDetailsFileWriter> orderDetailsFileWriterMap)
    {
        this.orderDetailsFileWriterMap = orderDetailsFileWriterMap;
    }

    public Converter<OrderModel, OrderData> getOrderConverter()
    {
        return orderConverter;
    }

    @Required
    public void setOrderConverter(final Converter<OrderModel, OrderData> orderConverter)
    {
        this.orderConverter = orderConverter;
    }

    public OrderExportService getOrderExportService()
    {
        return orderExportService;
    }

    @Required
    public void setOrderExportService(final OrderExportService orderExportService)
    {
        this.orderExportService = orderExportService;
    }

    public String getDateFormat()
    {
        return dateFormat;
    }

    public void setDateFormat(final String dateFormat)
    {
        this.dateFormat = dateFormat;
    }

    public DateTimeFormatter getDateTimeFormatter()
    {
        return dateTimeFormatter;
    }

    public void setDateTimeFormatter(final DateTimeFormatter dateTimeFormatter)
    {
        this.dateTimeFormatter = dateTimeFormatter;
    }

    public Map<OrderExportType, OrderDetailsFileWriter> getOrderDetailsFileWriterMap()
    {
        return orderDetailsFileWriterMap;
    }

    /**
     * @return the catalogVersionService
     */
    public CatalogVersionService getCatalogVersionService()
    {
        return catalogVersionService;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
    {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @return the b2bUnitService
     */
    public B2BUnitService<B2BUnitModel, B2BCustomerModel> getB2bUnitService()
    {
        return b2bUnitService;
    }

    /**
     * @param b2bUnitService
     *            the b2bUnitService to set
     */
    public void setB2bUnitService(final B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService)
    {
        this.b2bUnitService = b2bUnitService;
    }

    /**
     * @return the userService
     */
    public UserService getUserService()
    {
        return userService;
    }

    /**
     * @param userService
     *            the userService to set
     */
    public void setUserService(final UserService userService)
    {
        this.userService = userService;
    }

    /**
     * @return the sessionService
     */
    public SessionService getSessionService()
    {
        return sessionService;
    }

    /**
     * @param sessionService
     *            the sessionService to set
     */
    public void setSessionService(final SessionService sessionService)
    {
        this.sessionService = sessionService;
    }

    /**
     * @return the baseSiteService
     */
    public BaseSiteService getBaseSiteService()
    {
        return baseSiteService;
    }

    /**
     * @param baseSiteService
     *            the baseSiteService to set
     */
    public void setBaseSiteService(final BaseSiteService baseSiteService)
    {
        this.baseSiteService = baseSiteService;
    }

    protected ModelService getModelService()
    {
        return modelService;
    }

    @Required
    public void setModelService(ModelService modelService)
    {
        this.modelService = modelService;
    }

    /**
     * 
     * @param order - the order.
     * @param orderDetailsFileWriter - the file writer.
     * @param rendererTemplate - the renderer template.
     * @param fileNameFormat - the file name format.
     * @param fileEncoding - the file encoding.
     * @return boolean
     */
    protected Boolean exportOrder(OrderModel order, OrderDetailsFileWriter orderDetailsFileWriter,
            RendererTemplateModel rendererTemplate, MessageFormat fileNameFormat, String fileEncoding)
    {
        SessionExecutionBody sessionExecutionBody = new OrderExportSessionExecutionBody(order, orderDetailsFileWriter, rendererTemplate, fileNameFormat, fileEncoding);
        return getSessionService().executeInLocalView(sessionExecutionBody);
    }
    
    private class OrderExportSessionExecutionBody extends SessionExecutionBody
    {
        private OrderModel order;
        private OrderDetailsFileWriter orderDetailsFileWriter;
        private RendererTemplateModel rendererTemplate;
        private MessageFormat fileNameFormat;
        private String fileEncoding;
        
        private OrderExportSessionExecutionBody(OrderModel order, OrderDetailsFileWriter orderDetailsFileWriter,
            RendererTemplateModel rendererTemplate, MessageFormat fileNameFormat, String fileEncoding)
        {
            this.order = order;
            this.orderDetailsFileWriter = orderDetailsFileWriter;
            this.rendererTemplate = rendererTemplate;
            this.fileNameFormat = fileNameFormat;
            this.fileEncoding = fileEncoding;
        }
        
        @Override
        public Object execute()
        {
            boolean result = true;
            CMSSiteModel cmsSite = (CMSSiteModel) order.getSite();
            getBaseSiteService().setCurrentBaseSite(cmsSite, true);
            getUserService().setCurrentUser(order.getUser());
            getCatalogVersionService().setSessionCatalogVersions(
                    Collections.singletonList(cmsSite.getDefaultCatalog().getActiveCatalogVersion()));
            getB2bUnitService().updateBranchInSession(getSessionService().getCurrentSession(), order.getUser());

            try
            {
                final OrderData orderData = getOrderConverter().convert(order);
                
                if (orderData == null)
                {
                    result = false;
                    LOG.error("OrderData is null. Unable to write order.");
                }
                else
                {
                    final OrderDetailsContext orderDetailsContext = new OrderDetailsContext();
                    orderDetailsContext.setOrderData(orderData);
                    result = orderDetailsFileWriter.writeOrderToFile(rendererTemplate,
                            new File(fileNameFormat.format(new Object[]
                    { orderData.getCode() })), fileEncoding, orderDetailsContext);
                    updateOrderStatus(order, result);
                }
            }
            catch (final IOException e)
            {
                result = false;
                LOG.error("Error writing order to file.", e);
            }

            return Boolean.valueOf(result);
        }

        private void updateOrderStatus(final OrderModel order, final boolean writeToFileSuccess)
        {
            if (writeToFileSuccess && OrderStatus.EXPORT_READY.equals(order.getStatus()))
            {
                OrderStatus orderStatus = OrderStatus.WAIT_FOR_UPDATE;
                order.setStatus(orderStatus);
                LOG.info("Order {} status updated to {}", order.getCode(), orderStatus);
                getModelService().save(order);
            }
        }
        
    }
}
