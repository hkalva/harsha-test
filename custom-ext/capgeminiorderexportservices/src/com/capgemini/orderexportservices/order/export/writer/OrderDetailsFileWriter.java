/**
 *
 */
package com.capgemini.orderexportservices.order.export.writer;

import de.hybris.platform.commons.model.renderer.RendererTemplateModel;

import java.io.File;
import java.io.IOException;

import com.capgemini.orderexportservices.order.export.context.OrderDetailsContext;


/**@author lyonscg.
 * Order detail file writer interface.
 */
public interface OrderDetailsFileWriter
{
	boolean writeOrderToFile(final RendererTemplateModel template, final File file, final String fileEncoding,
			final OrderDetailsContext orderDetailsContext) throws IOException;
}
