/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.orderexportservices.order.export.cronjob;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.orderexportservices.model.OrderExportCronJobModel;
import com.capgemini.orderexportservices.order.export.OrderExportFacade;
import com.capgemini.orderexportservices.order.export.config.OrderExportConfig;


/**
 * Implements abstract class orderexport cronjob performable.
 * @spring.bean orderExportCronJob
 */
public class OrderExportCronJobPerformable extends AbstractJobPerformable<OrderExportCronJobModel>
{
	/**
	 * LOGGER for OrderExportCronJobPerformable.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(OrderExportCronJobPerformable.class);

	/**
	 * OrderExportFacade for order export.
	 */
	private OrderExportFacade orderExportFacade;

	@Override
	public PerformResult perform(final OrderExportCronJobModel cronJob)
	{
		LOG.info("Starting OrderExportCronJob...");
		final OrderExportConfig exportConfig = new OrderExportConfig();
		exportConfig.setIsQueryEnabled(cronJob.getQueryEnabled());
		exportConfig.setQuery(cronJob.getQuery());
		exportConfig.setQueryParams(cronJob.getQuerySearchParameter());
		exportConfig.setStore(cronJob.getStore());
		exportConfig.setOrderStatuses(cronJob.getOrderStatuses());
		exportConfig.setRendererTemplate(cronJob.getRendererTemplate());
		exportConfig.setFileExportPath(cronJob.getExportFilePath());
		exportConfig.setFileNameExt(cronJob.getFileNameExt());
		exportConfig.setFileEncoding(cronJob.getFileEncoding());
		exportConfig.setOrderExportStrategy(cronJob.getExportStrategy());

		if (getOrderExportFacade().exportOrders(exportConfig))
		{
			LOG.info("OrderExportCronJob completed successfully...");
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		LOG.info("OrderExportCronJob completed unsuccessfully...");
		return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
	}

	/**
	 *
	 * @return OrderExportFacade
	 */
	public OrderExportFacade getOrderExportFacade()
	{
		return orderExportFacade;
	}

	/**
	 *
	 * @param orderExportFacade - the order export facade.
	 */
	public void setOrderExportFacade(final OrderExportFacade orderExportFacade)
	{
		this.orderExportFacade = orderExportFacade;
	}

}
