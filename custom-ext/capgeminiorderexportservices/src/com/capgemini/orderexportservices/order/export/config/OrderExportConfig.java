/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.orderexportservices.order.export.config;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.capgemini.orderexportservices.enums.OrderExportType;
import com.capgemini.services.AbstractExportConfig;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.store.BaseStoreModel;


/**
 * Order Export Configuration.
 */
public class OrderExportConfig extends AbstractExportConfig
{
	
	/**
	 * BaseStore parameter when using default query.
	 */
	private BaseStoreModel store;
	/**
	 * OrderStatuses parameter when using default query.
	 */
	private List<OrderStatus> orderStatuses;
	
	/**
	 * Order Export Strategy for exporting orders.
	 */
	private OrderExportType orderExportStrategy;

	
	/**
	 * @return the store
	 */
	public BaseStoreModel getStore()
	{
		return store;
	}

	/**
	 * @param store
	 *           the store to set
	 */
	public void setStore(final BaseStoreModel store)
	{
		this.store = store;
	}

	/**
	 * @return the orderStatuses
	 */
	public List<OrderStatus> getOrderStatuses()
	{
		return orderStatuses;
	}

	/**
	 * @param orderStatuses
	 *           the orderStatuses to set
	 */
	public void setOrderStatuses(final List<OrderStatus> orderStatuses)
	{
		this.orderStatuses = orderStatuses;
	}

	/**
	 * @return the orderExportStrategy
	 */
	public OrderExportType getOrderExportStrategy()
	{
		return orderExportStrategy;
	}

	/**
	 * @param orderExportStrategy
	 *           the orderExportStrategy to set
	 */
	public void setOrderExportStrategy(final OrderExportType orderExportStrategy)
	{
		this.orderExportStrategy = orderExportStrategy;
	}

	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this);
	}
}
