/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.orderexportservices.order.export;

import com.capgemini.orderexportservices.order.export.config.OrderExportConfig;


/**
 * Order Export Facade definition.
 */
public interface OrderExportFacade
{
	/**
	 * Export orders based on OrderExportConfig.
	 *
	 * @param exportConfig
	 *           Order export config.
	 * @return returns true if successful.
	 */
	boolean exportOrders(OrderExportConfig exportConfig);
}
