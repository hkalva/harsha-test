/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.capgemini.orderexportservices.setup;

import java.util.Collections;
import java.util.List;

import com.capgemini.orderexportservices.constants.CapgeminiOrderexportConstants;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;


/**
 * This class provides hooks into the system's initialization and update processes.
 *
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = CapgeminiOrderexportConstants.EXTENSIONNAME)
public class CoreSystemSetup extends AbstractSystemSetup
{

    @Override
    public List<SystemSetupParameter> getInitializationOptions()
    {
        return Collections.emptyList();
    }
    
    @SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
    public void createEssentialData(final SystemSetupContext context)
    {
        importImpexFile(context, "/capgeminiorderexportservices/import/essentialdata-cronjobs.impex", true);
    }

    
}
