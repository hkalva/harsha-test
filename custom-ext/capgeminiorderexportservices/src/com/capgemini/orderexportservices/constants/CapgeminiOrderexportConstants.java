package com.capgemini.orderexportservices.constants;

/**
 *
 * @author lyonscg
 *
 */
public class CapgeminiOrderexportConstants extends GeneratedCapgeminiOrderexportConstants
{
    /**
     * constant for extension name.
     */
	public static final String EXTENSIONNAME = "capgeminiorderexportservices";

	/**
	 * the default private constructor.
	 */
	private CapgeminiOrderexportConstants()
	{
		//empty
	}
	
	
}
