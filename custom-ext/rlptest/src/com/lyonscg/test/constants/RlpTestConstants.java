/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lyonscg.test.constants;

/**
 * 
 */
public class RlpTestConstants extends GeneratedRlpTestConstants
{

	public static final String EXTENSIONNAME = "rlptest";

}
