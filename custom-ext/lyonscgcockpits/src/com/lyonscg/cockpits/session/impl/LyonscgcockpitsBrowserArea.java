/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.lyonscg.cockpits.session.impl;

import de.hybris.platform.cockpit.session.impl.DefaultSearchBrowserArea;

import org.apache.log4j.Logger;


/**
 * Lyonscgcockpits browser area.
 */
public class LyonscgcockpitsBrowserArea extends DefaultSearchBrowserArea
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(LyonscgcockpitsBrowserArea.class);

}
