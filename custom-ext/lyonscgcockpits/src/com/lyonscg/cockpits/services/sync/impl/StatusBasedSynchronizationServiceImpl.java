/**
 *
 */
package com.lyonscg.cockpits.services.sync.impl;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.enums.CmsApprovalStatus;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cockpit.daos.SynchronizationServiceDao;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.sync.impl.SynchronizationServiceImpl;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.Item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;


/**
 * Extending ProductCockpit's SynchronizationServiceImpl to override the performSynchronization method to ensure
 * Synchronization can happen only for products that have its Approval Status as Approved and has atleast one super
 * category.
 *
 */
public class StatusBasedSynchronizationServiceImpl extends SynchronizationServiceImpl
{
	private static final Logger LOG = Logger.getLogger(StatusBasedSynchronizationServiceImpl.class);

	/**
	 * Override the OOTB method to check for Product Approval Status and ensure that there is atleast one super Category.
	 */
	@Override
	public Collection<TypedObject> performSynchronization(final Collection<? extends Object> items,
			final List<String> syncJobPkList, final CatalogVersionModel targetCatalogVersion, final String qualifier)
	{
		LOG.info("WorkflowSynchronizationServiceImpl : Entered");

		if (items != null && !items.isEmpty())
		{
			final ArrayList checkedItems = new ArrayList();
			final Iterator arg6 = items.iterator();

			while (arg6.hasNext())
			{
				final Object object = arg6.next();
				TypedObject wrappedItem = null;
				if (object instanceof Item)
				{
					wrappedItem = this.getTypeService().wrapItem(((Item) object).getPK());
				}
				else if (object instanceof ItemModel)
				{
					wrappedItem = this.getTypeService().wrapItem(((ItemModel) object).getPk());
				}
				else if (object instanceof TypedObject)
				{
					wrappedItem = (TypedObject) object;
				}

				if (wrappedItem != null)
				{
					if (wrappedItem.getObject() instanceof ProductModel)
					{
						final ProductModel product = (ProductModel) wrappedItem.getObject();
						if (product.getApprovalStatus().equals(ArticleApprovalStatus.APPROVED) && product.getSupercategories() != null
								&& !product.getSupercategories().isEmpty())
						{
							checkedItems.add(object);
						}
					}
					else if (wrappedItem.getObject() instanceof AbstractPageModel)
					{
						final AbstractPageModel page = (AbstractPageModel) wrappedItem.getObject();
						if (page.getApprovalStatus().equals(CmsApprovalStatus.UNAPPROVED)
								|| page.getApprovalStatus().equals(CmsApprovalStatus.REQUESTAPPROVAL))
						{
							LOG.info("Skipping sync for page " + page.getUid());
						}
						else
						{
							checkedItems.add(object);
						}
					}
					else
					{
						checkedItems.add(object);
					}
				}
				else
				{
					LOG.error("Couldn\'t wrap item \'" + object + "\' into a typed object.");
				}
			}
			if (!checkedItems.isEmpty())
			{
				return super.performSynchronization(checkedItems, syncJobPkList, targetCatalogVersion, qualifier);
			}
			else
			{
				return Collections.EMPTY_LIST;
			}
		}
		else
		{
			return Collections.EMPTY_LIST;
		}
	}

	@Override
	public void setSynchronizationServiceDao(final SynchronizationServiceDao synchronizationServiceDao)
	{
		super.setSynchronizationServiceDao(synchronizationServiceDao);
	}

}
