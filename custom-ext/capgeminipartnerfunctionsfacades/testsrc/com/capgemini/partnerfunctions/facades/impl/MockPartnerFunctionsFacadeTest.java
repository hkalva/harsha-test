package com.capgemini.partnerfunctions.facades.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

@UnitTest
public class MockPartnerFunctionsFacadeTest
{

    /**
     * the facade to test.
     */
    private MockPartnerFunctionsFacade facade;
    
    /**
     * a map to mimic session service.
     */
    private Map<String, Object> map;
    
    /**
     * the user service.
     */
    @Mock
    private UserService userService;

    /**
     * the b2b unit service.
     */
    @Mock
    private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;

    /**
     * the session service.
     */
    @Mock
    private SessionService sessionService;
    
    /**
     * the customer.
     */
    @Mock
    private CustomerModel customer;
    
    /**
     * the b2b customer.
     */
    @Mock
    private B2BCustomerModel b2bCustomer;

    /**
     * the b2b unit.
     */
    @Mock
    private B2BUnitModel b2bUnit;
    
    
    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        map = new HashMap<>();
        facade = new MockPartnerFunctionsFacade();
        facade.setB2bUnitService(b2bUnitService);
        facade.setSessionService(sessionService);
        facade.setUserService(userService);
        Mockito.when(b2bUnit.getUid()).thenReturn("uid");
        Mockito.when(b2bUnit.getName()).thenReturn("name");

        Mockito.doAnswer(new Answer() {

            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                map.put(invocation.getArgumentAt(0, String.class), invocation.getArgumentAt(1, Object.class));
                return null;
            }

        }).when(sessionService).setAttribute(Mockito.anyString(), Mockito.anyObject());

        Mockito.doAnswer(new Answer() {

            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                map.remove(invocation.getArgumentAt(0, String.class));
                return null;
            }

        }).when(sessionService).removeAttribute(Mockito.anyString());

        Mockito.doAnswer(new Answer() {

            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                return map.get(invocation.getArgumentAt(0, String.class));
            }

        }).when(sessionService).getAttribute(Mockito.anyString());
        
        Mockito.when(b2bUnitService.getUnitForUid("uid")).thenReturn(b2bUnit);
    }
    
    /**
     * test getSoldTos with non-b2b customer.
     */
    @Test(expected = IllegalStateException.class)
    public void getSoldTosNonB2BCustomer()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(customer);
        facade.getSoldTos();
    }
    
    /**
     * test getSoldTos with no b2b unit.
     */
    @Test(expected = IllegalStateException.class)
    public void getSoldTosNoB2BUnit()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(b2bCustomer);
        Mockito.when(b2bUnitService.getParent(Mockito.any(B2BCustomerModel.class))).thenReturn(null);
        facade.getSoldTos();
    }

    /**
     * test getSoldTos.
     */
    @Test
    public void getSoldTos()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(b2bCustomer);
        Mockito.when(b2bUnitService.getParent(Mockito.any(B2BCustomerModel.class))).thenReturn(b2bUnit);
        List<B2BUnitData> result = facade.getSoldTos();
        Assert.assertTrue(CollectionUtils.isNotEmpty(result));
        Assert.assertTrue(b2bUnit.getUid().equals(result.get(0).getUid()));
        Assert.assertTrue(b2bUnit.getName().equals(result.get(0).getName()));
    }
    
    /**
     * test getShipTos with non-b2b customer.
     */
    @Test(expected = IllegalStateException.class)
    public void getShipTosNonB2BCustomer()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(customer);
        facade.getShipTos(null);
    }
    
    /**
     * test getShipTos null sold-to uid.
     */
    @Test
    public void getShipTosNullSoldToUid()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(b2bCustomer);
        List<B2BUnitData> result = facade.getShipTos(null);
        Assert.assertTrue(CollectionUtils.isEmpty(result));
    }
    
    /**
     * test getShipTos with no b2b unit.
     */
    @Test(expected = IllegalStateException.class)
    public void getShipTosNoB2BUnit()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(b2bCustomer);
        Mockito.when(b2bUnitService.getParent(Mockito.any(B2BCustomerModel.class))).thenReturn(null);
        facade.getShipTos("uid");
    }

    /**
     * test getShipTos.
     */
    @Test
    public void getShipTos()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(b2bCustomer);
        Mockito.when(b2bUnitService.getParent(Mockito.any(B2BCustomerModel.class))).thenReturn(b2bUnit);
        List<B2BUnitData> result = facade.getShipTos("uid");
        Assert.assertTrue(CollectionUtils.isNotEmpty(result));
        Assert.assertTrue(b2bUnit.getUid().equals(result.get(0).getUid()));
        Assert.assertTrue(b2bUnit.getName().equals(result.get(0).getName()));
    }
    
    /**
     * test setSoldTo non-b2b customer.
     */
    @Test(expected = IllegalStateException.class)
    public void setSoldToNonB2BCustomer()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(customer);
        facade.setSoldTo(null);
    }
    
    /**
     * test setSoldTo null uid.
     */
    @Test
    public void setSoldToNullUid()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(b2bCustomer);
        List<B2BUnitData> result = facade.setSoldTo(null);
        Assert.assertTrue(CollectionUtils.isEmpty(result));
        Mockito.verify(sessionService, Mockito.times(2)).removeAttribute(Mockito.anyString());
    }
    
    /**
     * test setSoldTo invalid uid.
     */
    @Test(expected = IllegalArgumentException.class)
    public void setSoldToInvalidUid()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(b2bCustomer);
        facade.setSoldTo("uid1");
    }

    /**
     * test setSoldTo.
     */
    @Test
    public void setSoldTo()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(b2bCustomer);
        Mockito.when(b2bUnitService.getParent(Mockito.any(B2BCustomerModel.class))).thenReturn(b2bUnit);
        List<B2BUnitData> result = facade.setSoldTo("uid");
        Assert.assertTrue(CollectionUtils.isNotEmpty(result));
        Mockito.verify(sessionService, Mockito.times(1)).setAttribute(Mockito.anyString(), Mockito.anyObject());
        Mockito.verify(sessionService, Mockito.times(1)).removeAttribute(Mockito.anyString());
    }

    /**
     * test setShipTo non-b2b customer.
     */
    @Test(expected = IllegalStateException.class)
    public void setShipToNonB2BCustomer()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(customer);
        facade.setShipTo(null);
    }
    
    /**
     * test setShipTo null uid.
     */
    @Test
    public void setShipToNullUid()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(b2bCustomer);
        facade.setShipTo(null);
        Mockito.verify(sessionService, Mockito.times(1)).removeAttribute(Mockito.anyString());
    }
    
    /**
     * test setShipTo invalid uid.
     */
    @Test(expected = IllegalArgumentException.class)
    public void setShipToInvalidUid()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(b2bCustomer);
        facade.setShipTo("uid1");
    }

    /**
     * test setShipTo.
     */
    @Test
    public void setShipTo()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(b2bCustomer);
        facade.setShipTo("uid");
        Mockito.verify(sessionService, Mockito.times(1)).setAttribute(Mockito.anyString(), Mockito.anyObject());
    }
    
    /**
     * test getCurrentSoldTo non-b2b customer.
     */
    @Test(expected = IllegalStateException.class)
    public void getCurrentSoldToNonB2BCustomer()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(customer);
        facade.getCurrentSoldTo(Boolean.FALSE);
    }

    /**
     * test getCurrentSoldTo.
     */
    @Test
    public void getCurrentSoldTo()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(b2bCustomer);
        B2BUnitData result = facade.getCurrentSoldTo(Boolean.FALSE);
        Assert.assertTrue(result == null);
    }

    /**
     * test getCurrentSoldTo preselect.
     */
    @Test
    public void getCurrentSoldToPreselect()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(b2bCustomer);
        Mockito.when(b2bUnitService.getParent(Mockito.any(B2BCustomerModel.class))).thenReturn(b2bUnit);
        B2BUnitData result = facade.getCurrentSoldTo(Boolean.TRUE);
        Assert.assertTrue(result != null);
    }

    /**
     * test getCurrentShipTo non-b2b customer.
     */
    @Test(expected = IllegalStateException.class)
    public void getCurrentShipToNonB2BCustomer()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(customer);
        facade.getCurrentShipTo(Boolean.FALSE);
    }

    /**
     * test getCurrentShipTo.
     */
    @Test
    public void getCurrentShipTo()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(b2bCustomer);
        B2BUnitData result = facade.getCurrentShipTo(Boolean.FALSE);
        Assert.assertTrue(result == null);
    }

    /**
     * test getCurrentShipTo preselect.
     */
    @Test
    public void getCurrentShipToPreselect()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(b2bCustomer);
        Mockito.when(b2bUnitService.getParent(Mockito.any(B2BCustomerModel.class))).thenReturn(b2bUnit);
        B2BUnitData result = facade.getCurrentShipTo(Boolean.TRUE);
        Assert.assertTrue(result != null);
    }
    
}
