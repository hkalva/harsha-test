/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.partnerfunctions.facades;

import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;

import java.util.List;


/**
 * This class helps to get the list of ship to sold-to data display in browser
 */
public interface PartnerFunctionsFacade
{
	/**
	 * Get all the SoldTo's from database.
	 *
	 * @return List<B2BUnitData>
	 */
	List<B2BUnitData> getSoldTos();

	/**
	 * Get the all shipTo from the database.
	 *
	 * @param shipToUid - the ship-to uid.
	 * @return List<B2BUnitData>
	 */
	List<B2BUnitData> getShipTos(String shipToUid);

	/**
	 * Get the current soldTo.
	 *
	 * @param preSelect - flag if pre-select.
	 * @return B2BUnitData
	 */
	B2BUnitData getCurrentSoldTo(Boolean preSelect);

	/**
	 * Getting the current ShipTo.
	 *
	 * @param preSelect - flag if pre-select.
	 * @return B2BUnitData
	 */
	B2BUnitData getCurrentShipTo(Boolean preSelect);

	/**
	 * setting Sold to data.
	 *
	 * @param soldToUid - the sold-to uid.
	 * @return List<B2BUnitData>
	 */
	List<B2BUnitData> setSoldTo(String soldToUid);

	/**
	 * Setting shipTo data
	 *
	 * @param shipToUid - the ship-to uid.
	 */
	void setShipTo(String shipToUid);

}
