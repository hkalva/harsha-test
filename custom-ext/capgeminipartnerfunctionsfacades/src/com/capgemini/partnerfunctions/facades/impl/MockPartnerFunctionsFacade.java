/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.partnerfunctions.facades.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.capgemini.partnerfunctions.facades.PartnerFunctionsFacade;


/**
 * a mock implementation of PartnerFunctionsFacade. since different projects set up sold-to/ship-to differently, each
 * project should write its own implementation.
 */
public class MockPartnerFunctionsFacade implements PartnerFunctionsFacade
{

    /**
     * constant for currentSoldTo.
     */
    private static final String CURRENT_SOLD_TO = "currentSoldTo";

    /**
     * constant for currentShipTo.
     */
    private static final String CURRENT_SHIP_TO = "currentShipTo";
    
    /**
     * the user service.
     */
    private UserService userService;

    /**
     * the b2b unit service.
     */
    private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;

    /**
     * the session service.
     */
    private SessionService sessionService;

    /**
     * @return List<B2BUnitData> Get the B2bUnit data from database.
     */
    @Override
    public List<B2BUnitData> getSoldTos()
    {
        List<B2BUnitData> result = new ArrayList<>();
        final UserModel currentUser = getUserService().getCurrentUser();

        if (!(currentUser instanceof B2BCustomerModel))
        {
            return result;
        }

        final B2BUnitModel parent = (B2BUnitModel) getB2bUnitService().getParent((B2BCustomerModel) currentUser);

        if (parent != null)
        {
            final B2BUnitData data = new B2BUnitData();
            data.setUid(parent.getUid());
            data.setName(parent.getName());
            result.add(data);
        }
        
        return result;
    }

    /**
     * @param soldToUid - the sold to uid.
     * @return List<B2BUnitData> Get the Listof B2bunit based on soldToUid
     */
    @Override
    public List<B2BUnitData> getShipTos(final String soldToUid)
    {
        List<B2BUnitData> result = new ArrayList<>();
        final UserModel currentUser = getUserService().getCurrentUser();

        if (!(currentUser instanceof B2BCustomerModel))
        {
            return result;
        }
        
        if (StringUtils.isBlank(soldToUid))
        {
            return result;
        }

        final B2BUnitModel parent = (B2BUnitModel) getB2bUnitService().getParent((B2BCustomerModel) currentUser);
        
        if (parent != null)
        {
            final B2BUnitData data = new B2BUnitData();
            data.setUid(parent.getUid());
            data.setName(parent.getName());
            result.add(data);
        }

        return result;
    }

    /**
     * @param preSelect - the pre-select flag.
     * @return B2BUnitData Get the currentSoldto based on selection of preSelect.
     */
    @Override
    public B2BUnitData getCurrentSoldTo(final Boolean preSelect)
    {
        B2BUnitData result = null;
        
        if (!(getUserService().getCurrentUser() instanceof B2BCustomerModel))
        {
            return result;
        }

        B2BUnitModel currentSoldTo = getSessionService().getAttribute(CURRENT_SOLD_TO);

        if ((currentSoldTo == null) && preSelect)
        {
            final List<B2BUnitData> soldTos = getSoldTos();

            if (CollectionUtils.isNotEmpty(soldTos))
            {
                setSoldTo(soldTos.get(0).getUid());
                currentSoldTo = getSessionService().getAttribute(CURRENT_SOLD_TO);
            }

        }

        if (currentSoldTo != null)
        {
            result = new B2BUnitData();
            result.setUid(currentSoldTo.getUid());
            result.setName(currentSoldTo.getName());
        }

        return result;
    }

    /**
     * 
     * @param preSelect - the pre-select flag.
     * @return B2BUnitData
     */
    @Override
    public B2BUnitData getCurrentShipTo(final Boolean preSelect)
    {
        B2BUnitData result = null;
        
        if (!(getUserService().getCurrentUser() instanceof B2BCustomerModel))
        {
            return result;
        }

        B2BUnitModel currentShipTo = getSessionService().getAttribute(CURRENT_SHIP_TO);

        if ((currentShipTo == null) && preSelect)
        {
            final B2BUnitData soldTo = getCurrentSoldTo(preSelect);
            List<B2BUnitData> shipTos = Collections.emptyList();

            if (soldTo != null)
            {
                shipTos = getShipTos(soldTo.getUid());
            }

            if (CollectionUtils.isNotEmpty(shipTos))
            {
                setShipTo(shipTos.get(0).getUid());
                currentShipTo = getSessionService().getAttribute(CURRENT_SHIP_TO);
            }
        }

        if (currentShipTo != null)
        {
            result = new B2BUnitData();
            result.setUid(currentShipTo.getUid());
            result.setName(currentShipTo.getName());
        }
        
        return result;
    }

    /**
     * @param soldToUid - the sold-to uid.
     * @return List<B2BUnitData>
     */
    @Override
    public List<B2BUnitData> setSoldTo(final String soldToUid)
    {
        if (!(getUserService().getCurrentUser() instanceof B2BCustomerModel))
        {
            return Collections.emptyList();
        }
        
        if (StringUtils.isBlank(soldToUid))
        {
            getSessionService().removeAttribute(CURRENT_SOLD_TO);
            getSessionService().removeAttribute(CURRENT_SHIP_TO);
            return Collections.emptyList();
        }

        final B2BUnitModel soldTo = (B2BUnitModel) getB2bUnitService().getUnitForUid(soldToUid);
        
        if (soldTo == null)
        {
            return Collections.emptyList();
        }

        getSessionService().setAttribute(CURRENT_SOLD_TO, soldTo);
        getSessionService().removeAttribute(CURRENT_SHIP_TO);
        return getShipTos(soldToUid);
    }

    /**
     * @param shipToUid - the ship-to uid.
     */
    @Override
    public void setShipTo(final String shipToUid)
    {
        if (getUserService().getCurrentUser() instanceof B2BCustomerModel)
        {
            if (StringUtils.isBlank(shipToUid))
            {
                getSessionService().removeAttribute(CURRENT_SHIP_TO);
            }
            else
            {
                final B2BUnitModel shipTo = (B2BUnitModel) getB2bUnitService().getUnitForUid(shipToUid);
    
                if (shipTo != null)
                {
                    getSessionService().setAttribute(CURRENT_SHIP_TO, shipTo);
                }
            }
        }
    }

    /**
     * @return UserService
     */
    public UserService getUserService()
    {
        return userService;
    }

    /**
     * @param userService - 
     *            the userService to set.
     */
    public void setUserService(final UserService userService)
    {
        this.userService = userService;
    }

    /**
     * @return B2BUnitService<B2BUnitModel, B2BCustomerModel>
     */
    public B2BUnitService<B2BUnitModel, B2BCustomerModel> getB2bUnitService()
    {
        return b2bUnitService;
    }

    /**
     * @param b2bUnitService - 
     *            the b2bUnitService to set.
     */
    public void setB2bUnitService(final B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService)
    {
        this.b2bUnitService = b2bUnitService;
    }

    /**
     * @return SessionService
     */
    public SessionService getSessionService()
    {
        return sessionService;
    }

    /**
     * @param sessionService - 
     *            the sessionService to set.
     */
    public void setSessionService(final SessionService sessionService)
    {
        this.sessionService = sessionService;
    }

}
