/**
 *
 */
package com.capgemini.partnerfunctionsaddon.component.renderer;

import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.capgemini.partnerfunctions.facades.PartnerFunctionsFacade;
import com.capgemini.partnerfunctionsaddon.model.PartnerFunctionsComponentModel;


/**
 * @author lyonscg
 *
 */
public class PartnerFunctionsComponentRenderer<C extends PartnerFunctionsComponentModel>
        extends DefaultAddOnCMSComponentRenderer<C>
{

    /**
     * constant for currentSoldTo.
     */
    private static final String CURRENT_SOLD_TO = "currentSoldTo";

    /**
     * constant for currentShipTo.
     */
    private static final String CURRENT_SHIP_TO = "currentShipTo";
    
    /**
     * constant for soldTos.
     */
    private static final String SOLD_TOS = "soldTos";

    /**
     * constant for soldTos.
     */
    private static final String SHIP_TOS = "shipTos";
    
    /**
     * the partner functions facade.
     */
    private PartnerFunctionsFacade partnerFunctionsFacade;

    @Override

    public void renderComponent(final PageContext pageContext, final C component) throws ServletException, IOException

    {
        final B2BUnitData currentSoldTo = getPartnerFunctionsFacade().getCurrentSoldTo(component.getPreSelectSoldTo());
        final B2BUnitData currentShipTo = getPartnerFunctionsFacade().getCurrentShipTo(component.getPreSelectShipTo());
        final List<B2BUnitData> soldTos = getPartnerFunctionsFacade().getSoldTos();
        List<B2BUnitData> shipTos = null;

        if (currentSoldTo != null)
        {
            shipTos = getPartnerFunctionsFacade().getShipTos(currentSoldTo.getUid());
        }

        final StringBuilder moduleView = new StringBuilder();
        moduleView.append("/WEB-INF/views/addons/");
        moduleView.append(getAddonUiExtensionName(component)).append("/");
        moduleView.append(getUIExperienceFolder());
        moduleView.append("/cms/partnerfunctionscomponent.jsp");
        final Map<String, Object> exposedVariables = exposeVariables(pageContext, component);
        exposedVariables.put(CURRENT_SOLD_TO, currentSoldTo);
        exposedVariables.put(CURRENT_SHIP_TO, currentShipTo);
        exposedVariables.put(SOLD_TOS, soldTos);
        exposedVariables.put(SHIP_TOS, shipTos);
        pageContext.setAttribute(CURRENT_SOLD_TO, currentSoldTo, getScopeForVariableName(CURRENT_SOLD_TO));
        pageContext.setAttribute(CURRENT_SHIP_TO, currentShipTo, getScopeForVariableName(CURRENT_SHIP_TO));
        pageContext.setAttribute(SOLD_TOS, soldTos, getScopeForVariableName(SOLD_TOS));
        pageContext.setAttribute(SHIP_TOS, shipTos, getScopeForVariableName(SHIP_TOS));
        pageContext.include(moduleView.toString());
        unExposeVariables(pageContext, component, exposedVariables);
    }

    /**
     * @return the partnerFunctionsFacade
     */
    public PartnerFunctionsFacade getPartnerFunctionsFacade()
    {
        return partnerFunctionsFacade;
    }

    /**
     * @param partnerFunctionsFacade
     *            the partnerFunctionsFacade to set
     */
    public void setPartnerFunctionsFacade(final PartnerFunctionsFacade partnerFunctionsFacade)
    {
        this.partnerFunctionsFacade = partnerFunctionsFacade;
    }

}
