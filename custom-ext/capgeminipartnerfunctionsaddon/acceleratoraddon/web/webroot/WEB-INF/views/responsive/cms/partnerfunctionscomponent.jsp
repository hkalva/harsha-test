<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="partner-functions container-fluid form-inline">
    <div class="row">
        <div class="sold-to-partner-function partner-function form-group">
            <label for="sold-to" class="partner-function__label"><spring:theme code="label.sold.to"/></label>
            <select id="sold-to" class="form-control">
                <option value=""><spring:theme code="select.sold.to"/></option>
                <c:if test="${not empty soldTos}">
                    <c:forEach items="${soldTos}" var="soldTo">
                        <option value="${soldTo.uid}" ${((not empty currentSoldTo) && (currentSoldTo.uid eq soldTo.uid)) ? ' selected="selected"' : ''}>${soldTo.name}</option>
                    </c:forEach>
                    </c:if>
            </select>
        </div>
        <div class="ship-to-partner-function partner-function form-group">
            <label for="ship-to" class="partner-function__label"><spring:theme code="label.ship.to"/></label>
            <select id="ship-to" class="form-control">
                <option value=""><spring:theme code="select.ship.to"/></option>
                    <c:if test="${not empty shipTos}">
                    <c:forEach items="${shipTos}" var="shipTo">
                        <option value="${shipTo.uid}" ${((not empty currentShipTo) && (currentShipTo.uid eq shipTo.uid)) ? ' selected="selected"' : ''}>${shipTo.name}</option>
                    </c:forEach>
                </c:if>
            </select>
        </div>
    </div>
</div>