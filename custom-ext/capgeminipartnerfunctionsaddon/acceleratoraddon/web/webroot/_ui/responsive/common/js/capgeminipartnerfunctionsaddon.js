var ACC = ACC || {}; // make sure ACC is available
 
ACC.partnerfunctions = {
        _autoload: [
            'bindSoldTo',
            'bindShipTo'
        ],
         
        bindSoldTo: function() {
            $('select#sold-to').on('change', function() {
                var $self = $(this);
                 
                $.ajax({
                    url: '/partner-functions/set-sold-to?soldToUid=' + $self.val(),
                    type: 'GET',
                    dataType: 'JSON'
                }).done(function(json) {
                    if ($.isPlainObject(json)) {
                        console.log('success');
                    } else {
                        console.log('error');
                    }
                }).fail(function(xhr, status, error) {
                    console.log('fail');
                }).always(function() {
                    window.location.reload();
                })
            });
        },
         
        bindShipTo: function() {
            $('select#ship-to').on('change', function() {
                var $self = $(this);
                 
                $.ajax({
                    url: '/partner-functions/set-ship-to?shipToUid=' + $self.val(),
                    type: 'GET',
                    dataType: 'JSON'
                }).done(function(json) {
                    if ($.isPlainObject(json)) {
                        console.log('success');
                    } else {
                        console.log('error');
                    }
                }).fail(function(xhr, status, error) {
                    console.log('fail');
                }).always(function() {
                    window.location.reload();
                })
            });
        }
}