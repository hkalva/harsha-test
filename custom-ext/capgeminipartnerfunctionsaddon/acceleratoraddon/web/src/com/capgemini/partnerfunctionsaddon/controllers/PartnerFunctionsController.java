/**
 *
 */
package com.capgemini.partnerfunctionsaddon.controllers;

import de.hybris.platform.addonsupport.controllers.AbstractAddOnController;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.capgemini.partnerfunctions.facades.PartnerFunctionsFacade;


/**
 * @author lyonscg
 *
 */
@Controller
@RequestMapping("/partner-functions")
public class PartnerFunctionsController extends AbstractAddOnController
{

	@Resource(name = "partnerFunctionsFacade")
	private PartnerFunctionsFacade partnerFunctionsFacade;

	@ResponseBody
	@GetMapping(value = "/set-sold-to", produces = MediaType.APPLICATION_JSON_VALUE)
	public B2BUnitData setSoldTo(@RequestParam(value = "soldToUid", required = false)
	final String soldToUid)
	{
		partnerFunctionsFacade.setSoldTo(soldToUid);
		B2BUnitData result = partnerFunctionsFacade.getCurrentSoldTo(false);

		if (result == null)
		{
			result = getEmptyB2BUnitData();
		}

		return result;
	}


	@ResponseBody
	@GetMapping(value = "/set-ship-to", produces = MediaType.APPLICATION_JSON_VALUE)
	public B2BUnitData setShipTo(@RequestParam(value = "shipToUid", required = false)
	final String shipToUid)
	{
		partnerFunctionsFacade.setShipTo(shipToUid);
		B2BUnitData result = partnerFunctionsFacade.getCurrentShipTo(false);

		if (result == null)
		{
			result = getEmptyB2BUnitData();
		}

		return result;
	}
	
	/**
	 * 
	 * @return B2BUnitData
	 */
	protected B2BUnitData getEmptyB2BUnitData()
	{
		B2BUnitData result = new B2BUnitData();
		result.setUid(StringUtils.EMPTY);
		result.setName(StringUtils.EMPTY);
		return result;
	}
}
