package com.capgemini.googletagmanager.web.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.ui.ModelMap;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.HostConfigService;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;

@UnitTest
public class GoogleTagManagerBeforeViewHandlerTest
{
    /**
     * constant for google tag manager.
     */
    private static final String GOOGLE_TAGMANAGER_PREFIX = "googletagmanager";
    
    /**
     * constant for container id.
     */
    private static final String PROPERTIES_GTM_CONTAINER_ID = "containerId";
    
    /**
     * constant for ecommerce.
     */
    private static final String PROPERTIES_ECOMMERCE = "ecommerce";
    
    /**
     * constant for checkout step.
     */
    private static final String CHECKOUT_STEP = "checkoutStep";
    
    /**
     * the handler to test.
     */
    @Spy
    private GoogleTagManagerBeforeViewHandler handler = new GoogleTagManagerBeforeViewHandler();
    
    /**
     * the site config service.
     */
    @Mock
    private SiteConfigService siteConfigService;
    
    /**
     * the http request.
     */
    @Mock
    private HttpServletRequest request;
    
    /**
     * the http response.
     */
    @Mock
    private HttpServletResponse response;
    
    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        handler.setSiteConfigService(siteConfigService);
        Mockito.when(siteConfigService.getProperty(GOOGLE_TAGMANAGER_PREFIX + "." + CHECKOUT_STEP + "2")).thenReturn("addEditDeliveryAddressPage.jsp");
        Mockito.when(siteConfigService.getProperty(GOOGLE_TAGMANAGER_PREFIX + "." + CHECKOUT_STEP + "3")).thenReturn("chooseDeliveryMethodPage.jsp");
        Mockito.when(siteConfigService.getProperty(GOOGLE_TAGMANAGER_PREFIX + "." + PROPERTIES_GTM_CONTAINER_ID)).thenReturn("container");
        Mockito.when(siteConfigService.getProperty(GOOGLE_TAGMANAGER_PREFIX + "." + PROPERTIES_ECOMMERCE)).thenReturn("enhanced");
    }
    
    /**
     * test beforeView.
     */
    @Test
    public void testBeforeView() throws Exception
    {
        ModelMap model = new ModelMap();
        String viewName = "viewName";
        String result = handler.beforeView(request, response, model, viewName);
        Assert.assertTrue(viewName.equals(result));
        Assert.assertTrue(model.size() == 3);
        Mockito.verify(siteConfigService, Mockito.times(5)).getProperty(Mockito.anyString());
    }
}
