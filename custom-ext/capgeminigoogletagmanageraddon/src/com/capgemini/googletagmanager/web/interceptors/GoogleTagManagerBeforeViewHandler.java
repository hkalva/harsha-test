/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/

package com.capgemini.googletagmanager.web.interceptors;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;

import java.util.List;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import de.hybris.platform.addonsupport.interceptors.BeforeViewHandlerAdaptee;


/**
 * @author lyonscg.
 *
 */
public class GoogleTagManagerBeforeViewHandler implements BeforeViewHandlerAdaptee
{
    /**
     * constant for properties prefix.
     */
    private static final String GOOGLE_TAGMANAGER_PREFIX = "googletagmanager";
    
    /**
     * constant for container id property.
     */
    private static final String PROPERTIES_GTM_CONTAINER_ID = "containerId";

    /**
     * constant for e-commerce property.
     */
    private static final String PROPERTIES_ECOMMERCE = "ecommerce";

    /**
     * constant for checkout step property.
     */
    private static final String CHECKOUT_STEP = "checkoutStep";

    /**
     * constant for container id variable.
     */
    private static final String GTM_CONTAINER_ID = "ContainerId";
    
    /**
     * constant for checkout steps list variable.
     */
    private static final String CHECKOUT_STEPS_LIST = "checkoutStepsList";

    /**
     * constant for name space.
     */
    private static final String TAG_NAMESPACE = "gtm";

    /**
     * the site config service.
     */
    private SiteConfigService siteConfigService;

    @Override
    public String beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
            final String viewName) throws Exception
    {
        final String containerId = getSiteConfigService()
                .getProperty(GOOGLE_TAGMANAGER_PREFIX + "." + PROPERTIES_GTM_CONTAINER_ID);
        model.addAttribute(TAG_NAMESPACE + GTM_CONTAINER_ID, containerId);
        model.addAttribute(TAG_NAMESPACE + CHECKOUT_STEPS_LIST, getCheckoutStepsList());
        final String ecommerce = getSiteConfigService().getProperty(GOOGLE_TAGMANAGER_PREFIX + "." + PROPERTIES_ECOMMERCE);
        model.addAttribute(TAG_NAMESPACE + PROPERTIES_ECOMMERCE, ecommerce);
        return viewName;
    }

    /**
     *
     * @return List<String>
     */
    protected List<String> getCheckoutStepsList()
    {
        final List<String> result = new ArrayList<>();

        //Allow maximum of 20 steps
        for (int i = 2; i <= 20; i++)
        {
            final String checkoutStep = getSiteConfigService().getProperty(GOOGLE_TAGMANAGER_PREFIX + "." + CHECKOUT_STEP + i);

            if (checkoutStep == null)
            {
                break;
            }

            result.add(checkoutStep);
        }

        return result;
    }

    /**
     *
     * @param siteConfigService
     *            - the site config service.
     */
    public void setSiteConfigService(final SiteConfigService siteConfigService)
    {
        this.siteConfigService = siteConfigService;
    }

    /**
     *
     * @return SiteConfigService
     */
    protected SiteConfigService getSiteConfigService()
    {
        return siteConfigService;
    }

}
