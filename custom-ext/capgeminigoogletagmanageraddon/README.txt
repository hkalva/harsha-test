Introduction to Google Tag Manager:-

 The SAP Hybris extend Google Tag Manager (GTM) extension to support addition of marketing, 
 analytics and conversion tracking tags,  and will add and configure Google's Universal 
 Analytics within GTM for the primary site. GTM containers and respective GA tracking codes 
 will be installed and GTM container will live on all pages, along with a datalayer to 
 support Google Analytics' enhanced ecommerce reporting. Through this tagging/tracking 
 infrastructure, marketers will be able to add common tags without development efforts, and
 they'll have immediate visibility into data points across various dimensions: session 
 activity, engagement, devices, browsers, traffic mediums, frequency, content consumption, 
 and pathing. The enhanced ecommerce tracking will provide data around both finalized 
 transaction activity, and the behavior that contributed to those conversions: product views, 
 add to cart, initiate cart, etc.

The following will be configured within GTM and Google Universal Analytics:-

	* The global Universal Tag, which will live on all pages, aggregating data for all OOTB 
	  Universal Analytics (UA) Reports
	* UA Event Tags will be provided for "Add to Cart", "Checkout Step", "Product Impressions", 
	  and "Remove from Cart".
	* UA Demographic Reporting will be enabled within GTM
	* Internal Site Search Reports will be enabled within UA
	* Enhanced Ecommerce settings will be configured within GTM and UA; this includes Site 
	  Behavior and Checkout Funnels
