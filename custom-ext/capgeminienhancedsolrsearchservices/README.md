
# Lyons CG Enhanced Solr Search

This extension enhances the hybris solr search features and options.

Additional configuration options have been added to the SearchQueryTemplate and the SearchQueryProperty. These properties have been exposed in the backoffice in the "Enhanced" editor tab.
The lcgenhancedsolrsearchbackoffice extension must be installed to access the custom tabs.


Below are the features currently supported.

## Edismax Query
This extension exposes most of the edismax parameters via the backoffice properties.

### SearchQueryTemplate
#### edismaxEnabled
This is a boolean flag that will instruct hybris to use the edismax query builder, rather than the OOTB free text query builder.
#### splitOnWhitespace
This is a boolean flag that, if set, will instruct hybris to include the sow edismax parameter to be sent with the query.
#### boost
This is a localized raw boost function expression. There is no field translation taking place on the value, so fields must match the real solr fields being boosted. A simple example is:

```
recip(ord(pk),1,1000,1000)

```
This is a contrived example. Real world usage will vary. If the localized fields will be referenced, multilingual implementation will may need an entry for each language.

#### minimumShouldMatch
This property corresponds to Solr's mm parameter.

#### phraseSlop
Integer value for ps param.

#### phraseSlop2
Integer value for ps2 param.

#### phraseSlop3
Integer value for ps3 param.

#### queryPhraseSlop
Integer value for qs param.

#### tie
Float value for tie parameter.

#### stopwords
This is a boolean flag that will instruct hybris to send the stopwords parameter with the query.

#### mmAutoRelax
This is a boolean flag that will instruct hybris to send the mm.autoRelax parameter with the query.

The Solr edismax query parser does not play nice when searching localized and unlocalized fields at the same time when mm=100%. 
The reason for this is related to stop words being applied to the localized text fields and not the general text fields. 
To workaround this problem, there are some possible solutions:

1. Send the mm.autoRelax parameter in the edismax query.
2. Sync the stopwords between localized and unlocalized text fields.
3. Remove stopwords
4. Use only unlocalized search fields
5. Use only localized search fields.

We used the fifth option and are using only localized fields in our edismax query.

For more information on this issue, refer the following:

[SOLR-3085](https://issues.apache.org/jira/browse/SOLR-3085)

https://stackoverflow.com/questions/30925996/edismax-queries-with-stopwords-and-language-specific-fields

### SearchQueryProperty

#### additiveBoostFunctionExp

This is an optional field. It may be used to add an additive boost function to the edismax bf parameter. It may either be a raw value (using real solr fields), or using field name replacement. Consider the following example:

given field name is modifiedtime
given field type is date
given additive boost function value is recip(rord($field),1,1000,1000)
then bf param will be recip(rord(modifiedtime_date),1,1000,1000)

#### queryField
The field will be included in the qf parameter.

#### queryFieldBoost
The amount of boost to apply to the field in the qf param.

#### phraseField
The field will be included in the pf parameter.

#### phraseFieldBoost
The amount of boost to apply to the field in the pf param.

#### phraseField2
The field will be included in the pf2 parameter.

#### phraseField2Boost
The amount of boost to apply to the field in the pf2 param.

#### phraseField3
The field will be included in the pf3 parameter.

#### phraseField3Boost
The amount of boost to apply to the field in the pf3 param.


## Multiplicative boost functions.

OOTB, hybris includes some basic boost capabilities. When a field is boosted in hybris, it will normally be included in the *ymb* multiplicative boost parameter. This property allows a boost function associated with the property to be included in the ymb parameter.


```
recip(ord($field),1,1000,1000)

```
This multiplicative boost function is not dependent upon the edismax query builder to function, and can be used with standard queries.

### SearchQueryProperty

multiplicativeBoostFunctionExp

## Example Query

Below is an example query using a multiplicative boost function expression and the edismax query builder. Not all edismax query options were selected for this example.

```
ymb=product(recip(rord(modifiedtime_date),1,1000,1000))
&yq=*:*
&q={!boost+b%3D$ymb}({!edismax+boost%3D"recip(ord(pk),1,1000,1000)"+pf2%3D"name_text_en^1.2"+qf%3D"description_text_en+categoryName_text_en_mv+name_text_en^10.0"+qs%3D"3"+tie%3D"1.0"+mm%3D"100%25"+stopwords%3D"true"+bf%3D"sum(recip(rord(modifiedtime_date),1,1000,1000),recip(ord(creationtime_date),1,1000,1000))"+v%3D"camera"})
&fq=(catalogId:"lcgb2cProductCatalog"+AND+catalogVersion:"Online")&sort=inStockFlag_boolean+desc,score+desc&start=0&rows=20&facet.field=Resolution,+80_string&facet.field=Colour+of+product,+1766_en_string&facet.field=Mounting,+1867_en_string&facet.field=price_usd_string&facet.field=categoryPath_string_mv&facet.field=allPromotions_string_mv&facet.field=Lens+type,+472_en_string_mv&facet.field=Megapixel,+63_string&facet.field=category_string_mv&facet.field=brand_string_mv&facet.field=availableInStores_string_mv&facet=true&facet.sort=count&facet.mincount=1&facet.limit=50&fl=score,*&hl.fl=summary_text_en&hl.fl=name_text_en&hl=true&hl.snippets=3&hl.requireFieldMatch=false&hl.method=unified&hl.tag.pre=<em+class%3D"search-results-highlight">&hl.tag.post=</em>&spellcheck=true&spellcheck.q=camera&spellcheck.dictionary=en&spellcheck.collate=true
```

The ymb parameter has a product function wrapping a recip function. The product function is rendered by hybris. The recip function was defined on a custom modifiedtime field query property.

The edismax query is a bit more complicated. It includes an example additive boost in the *bf* parameter and an example function boost in the *boost* parameter.


## Using Edismax
Edismax can provide more relevant search results than the OOTB hybris query builder. That being said, optimization is a tuning exercise, and cannot be accomplished without some understanding of how Solr works.

It may also be necessary to modify the OOTB Solr schema that ships with hybris. By way of example, the OOTB Free Text Query Builder includes a means to specify "wildcard" queries (e.g. foo*). Edismax uses a very simplified expression in the query sent to Solr. No wildcard support is provided in this extension.

To remedy this shortcoming, one can replace the standard Solr tokenizer with the EdgeNGramTokenizer. Also suggested, is eliminating the SnowBallPorterFilterFactory. Below is example, showing these changes made to the text_en field. The commented code is the original hybris config.

```xml
		<fieldType name="text_en" class="solr.TextField" positionIncrementGap="100">
			<!--<analyzer>-->
				<!--<tokenizer class="solr.StandardTokenizerFactory" />-->
				<!--<filter class="solr.SynonymGraphFilterFactory" ignoreCase="true" synonyms="synonyms.txt"/>-->
				<!--<filter class="solr.ManagedSynonymGraphFilterFactory" managed="en" />-->
				<!--<filter class="solr.StopFilterFactory" ignoreCase="true" words="lang/stopwords_en.txt" />-->
				<!--<filter class="solr.ManagedStopFilterFactory" managed="en" />-->
				<!--<filter class="solr.WordDelimiterGraphFilterFactory"  preserveOriginal="1"-->
						<!--generateWordParts="1" generateNumberParts="1" catenateWords="1"-->
						<!--catenateNumbers="1" catenateAll="0" splitOnCaseChange="1" />-->
				<!--<filter class="solr.LowerCaseFilterFactory" />-->
				<!--<filter class="solr.ASCIIFoldingFilterFactory" />-->
				<!--<filter class="solr.SnowballPorterFilterFactory" language="English" />-->
			<!--</analyzer>-->
			<analyzer type="index">
				<tokenizer class="solr.EdgeNGramTokenizerFactory" minGramSize="2" maxGramSize="10"/>
				<filter class="solr.SynonymGraphFilterFactory" ignoreCase="true" synonyms="synonyms.txt"/>
				<filter class="solr.ManagedSynonymGraphFilterFactory" managed="en" />
				<filter class="solr.StopFilterFactory" ignoreCase="true" words="lang/stopwords_en.txt" />
				<filter class="solr.ManagedStopFilterFactory" managed="en" />
				<filter class="solr.WordDelimiterGraphFilterFactory"  preserveOriginal="1"
					generateWordParts="1" generateNumberParts="1" catenateWords="1"
					catenateNumbers="1" catenateAll="0" splitOnCaseChange="1" />
				<filter class="solr.LowerCaseFilterFactory" />
				<filter class="solr.ASCIIFoldingFilterFactory" />
			</analyzer>
			<analyzer type="query">
				<tokenizer class="solr.StandardTokenizerFactory" />
				<filter class="solr.SynonymGraphFilterFactory" ignoreCase="true" synonyms="synonyms.txt"/>
				<filter class="solr.ManagedSynonymGraphFilterFactory" managed="en" />
				<filter class="solr.StopFilterFactory" ignoreCase="true" words="lang/stopwords_en.txt" />
				<filter class="solr.ManagedStopFilterFactory" managed="en" />
				<filter class="solr.WordDelimiterGraphFilterFactory"  preserveOriginal="1"
						generateWordParts="1" generateNumberParts="1" catenateWords="1"
						catenateNumbers="1" catenateAll="0" splitOnCaseChange="1" />
				<filter class="solr.LowerCaseFilterFactory" />
				<filter class="solr.ASCIIFoldingFilterFactory" />
			</analyzer>
		</fieldType>
```