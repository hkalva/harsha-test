# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------

# put localizations of item types into this file
# Note that you can also add special locatizations which 
# can be retrieved with the 
#
# ...tools.localization.Localization.getLocalizedString(...)
# 
# methods.
#
# syntax for type localizations:
#
type.SolrSearchQueryProperty.multiplicativeBoostFunctionExp.name=Multiplicative Boost Function Expression
type.SolrSearchQueryProperty.multiplicativeBoostFunctionExp.description=A multiplicative boost function expression. The field in the expression must be "$field".

type.SolrSearchQueryProperty.additiveBoostFunctionExp.name=Additive Boost Function Expression (bf param)
type.SolrSearchQueryProperty.additiveBoostFunctionExp.description=A additive boost function expression to be added to the bf parameter. The field in the expression must be "$field".

type.SolrSearchQueryProperty.queryField.name=Add to qf (query field) param?
type.SolrSearchQueryProperty.queryField.description=Determines if this field should be included in the qf parameter.
type.SolrSearchQueryProperty.queryFieldBoost.name=Query Field (qf) Boost
type.SolrSearchQueryProperty.queryFieldBoost.description=Determines the boost value used for this field in the qf parameter. Defaults to 0.0. Default boost of 0.0 will not be rendered.

type.SolrSearchQueryProperty.phraseField.name=Add to pf (phrase field) param?
type.SolrSearchQueryProperty.phraseField.description=Determines if this field should be included in the pf parameter.
type.SolrSearchQueryProperty.phraseFieldBoost.name=Phrase Field (pf) Boost
type.SolrSearchQueryProperty.phraseFieldBoost.description=Determines the boost value used for this field in the pf parameter. Defaults to 0.0. Default boost of 0.0 will not be rendered.

type.SolrSearchQueryProperty.phraseField2.name=Add to pf2 (phrase field 2) param?
type.SolrSearchQueryProperty.phraseField2.description=Determines if this field should be included in the pf2 parameter.
type.SolrSearchQueryProperty.phraseField2Boost.name=Phrase Field 2 (pf2) Boost
type.SolrSearchQueryProperty.phraseField2Boost.description=Determines the boost value used for this field in the pf2 parameter. Defaults to 0.0. Default boost of 0.0 will not be rendered.

type.SolrSearchQueryProperty.phraseField3.name=Add to pf3 (phrase field 3) param?
type.SolrSearchQueryProperty.phraseField3.description=Determines if this field should be included in the pf3 parameter.
type.SolrSearchQueryProperty.phraseField3Boost.name=Phrase Field 3 (pf3) Boost
type.SolrSearchQueryProperty.phraseField3Boost.description=Determines the boost value used for this field in the pf3 parameter. Defaults to 0.0. Default boost of 0.0 will not be rendered.

type.SolrSearchQueryTemplate.edismaxEnabled.name=Enable Edismax Query Builder
type.SolrSearchQueryTemplate.edismaxEnabled.description=Enables the edismax query builder for this template.

type.SolrSearchQueryTemplate.minimumShouldMatch.name=Minimum Should Match (mm) Parameter
type.SolrSearchQueryTemplate.minimumShouldMatch.description=Edismax mm parameter. Defaults to 100%. See Solr documentation for possible values.

type.SolrSearchQueryTemplate.phraseSlop.name=Phrase Slop (ps param)
type.SolrSearchQueryTemplate.phraseSlop.description=Phrase Slop (ps parameter). Defaults to 0. Only rendered in query if pf2 field has been specified on one or more SolrQueryProperties.


type.SolrSearchQueryTemplate.phraseSlop2.name=Phrase Slop (ps2 param)
type.SolrSearchQueryTemplate.phraseSlop2.description=Phrase Slop (ps2 parameter). Defaults to 0. Only rendered in query if pf2 field has been specified on one or more SolrQueryProperties.

type.SolrSearchQueryTemplate.phraseSlop3.name=Phrase Slop (ps3 param)
type.SolrSearchQueryTemplate.phraseSlop3.description=Phrase Slop (ps3 parameter). Defaults to 0. Only rendered in query if pf3 field has been specified on one or more SolrQueryProperties.

type.SolrSearchQueryTemplate.queryPhraseSlop.name=Query Phrase Slop (qs param)
type.SolrSearchQueryTemplate.queryPhraseSlop.description=Phrase Slop (qs parameter). Defaults to 0. Only rendered in query if pf3 field has been specified on one or more SolrQueryProperties.

type.SolrSearchQueryTemplate.tie.name=Tie (tie param)
type.SolrSearchQueryTemplate.tie.description=Tie parameter. Defaults to 0.

type.SolrSearchQueryTemplate.boost.name=Edismax Boost (boost param)
type.SolrSearchQueryTemplate.boost.description=A multivalued list of strings parsed as queries with scores multiplied by the score from the main query for all matching documents. This parameter is shorthand for wrapping the query produced by eDisMax using the BoostQParserPlugin.

type.SolrSearchQueryTemplate.splitOnWhitespace.name=Split on Whitespace (sow param)

type.SolrSearchQueryTemplate.stopwords.name=Stopwords
type.SolrSearchQueryTemplate.stopwords.description=If this is set to false, then the StopFilterFactory in the query analyzer is ignored.

type.SolrSearchQueryTemplate.mmAutoRelax.name=Enable mm.autoRelax param?
type.SolrSearchQueryTemplate.mmAutoRelax.description=Please consult the solr documentation before sending this parameter.
