package com.capgemini.enhancedsolrsearch.converters.populator;

import com.capgemini.enhancedsolrsearch.query.EdismaxQueryBuilder;
import com.capgemini.enhancedsolrsearch.query.EnhancedSearchQuery;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.FreeTextQueryBuilder;
import de.hybris.platform.solrfacetsearch.search.FreeTextQueryBuilderFactory;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;

import org.apache.solr.client.solrj.SolrQuery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * Test class for {@link EnhancedFacetSearchQueryBasicPopulator}.
 */
@UnitTest
public class EnhancedFacetSearchQueryBasicPopulatorTest
{
    @InjectMocks
    private EnhancedFacetSearchQueryBasicPopulator populator;

    @Mock
    private EdismaxQueryBuilder edismaxQueryBuilder;

    @Mock
    private FreeTextQueryBuilder freeTextQueryBuilder;

    @Mock
    private FreeTextQueryBuilderFactory freeTextQueryBuilderFactory;

    private SearchQueryConverterData searchQueryConverterData;
    private FacetSearchConfig facetSearchConfig;
    private IndexedType indexedType;
    private EnhancedSearchQuery searchQuery;
    private SolrQuery solrQuery;

    @Mock
    private FacetSearchContext facetSearchContext;

    /**
     * Setup.
     */
    @Before
    public void setup()
    {
        solrQuery = new SolrQuery();
        populator = new EnhancedFacetSearchQueryBasicPopulator();
        MockitoAnnotations.initMocks(this);
        searchQueryConverterData = new SearchQueryConverterData();
        facetSearchConfig = new FacetSearchConfig();
        indexedType = new IndexedType();
        searchQuery = new EnhancedSearchQuery(facetSearchConfig, indexedType);

        Mockito.doReturn(searchQuery).when(facetSearchContext).getSearchQuery();
        Mockito.doReturn(indexedType).when(facetSearchContext).getIndexedType();
        Mockito.doReturn(facetSearchConfig).when(facetSearchContext).getFacetSearchConfig();

        searchQueryConverterData.setSearchQuery(searchQuery);
        searchQueryConverterData.setFacetSearchContext(facetSearchContext);
        Mockito.doReturn("{!edismax}").when(edismaxQueryBuilder).buildQuery(searchQuery);
        Mockito.doReturn(freeTextQueryBuilder).when(freeTextQueryBuilderFactory).createQueryBuilder(searchQuery);
        Mockito.doReturn("(foo)").when(freeTextQueryBuilder).buildQuery(searchQuery);
    }

    /**
     * Verifies that the edismaxQueryBuilder is not invoked if not enabled.
     */
    @Test
    public void testDefaultFreeTextQueryBuilderIsInvokedWhenNoEdismaxQuery()
    {
        searchQuery.setUserQuery("Chicago Bulls");
        populator.populate(searchQueryConverterData, solrQuery);
        Mockito.verify(edismaxQueryBuilder, Mockito.never()).buildQuery(searchQuery);
        Mockito.verify(freeTextQueryBuilder).buildQuery(searchQuery);
        Assert.assertEquals("{!boost}(+{!lucene v=$yq})", solrQuery.getQuery());
    }

    /**
     * Verifies that the edismaxQueryBuilder is invoked when enabled.
     */
    @Test
    public void testEdismaxQueryBuilderIsInvokedWhenEnabled()
    {
        searchQuery.setUserQuery("Chicago Bulls");
        searchQuery.setEdismaxEnabled(true);
        populator.populate(searchQueryConverterData, solrQuery);
        Mockito.verify(edismaxQueryBuilder).buildQuery(searchQuery);
        Mockito.verify(freeTextQueryBuilder, Mockito.never()).buildQuery(searchQuery);
        Assert.assertEquals("{!boost}({!edismax})", solrQuery.getQuery());
    }

    @Test
    public void testEdismaxNotRendererWhenUserQueryNotSet()
    {

    }
}