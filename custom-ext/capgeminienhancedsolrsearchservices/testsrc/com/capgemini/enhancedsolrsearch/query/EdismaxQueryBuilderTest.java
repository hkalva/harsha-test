package com.capgemini.enhancedsolrsearch.query;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.search.BoostField;
import de.hybris.platform.solrfacetsearch.search.FieldNameTranslator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
/**
 * 
 * @author lyonscg
 *
 */
@UnitTest
public class EdismaxQueryBuilderTest
{
    @Mock
    private FieldNameTranslator fieldNameTranslator;

    @InjectMocks
    private EdismaxQueryBuilder builder;

    private EnhancedSearchQuery searchQuery;

    @Before
    public void setUp()
    {
        searchQuery = new EnhancedSearchQuery(new FacetSearchConfig(), new IndexedType());
        builder = new EdismaxQueryBuilder();
        MockitoAnnotations.initMocks(this);

        Mockito.doReturn("foo_text_en").when(fieldNameTranslator).translate(searchQuery, "foo", FieldNameProvider.FieldType.INDEX);
        Mockito.doReturn("bar_text_en").when(fieldNameTranslator).translate(searchQuery, "bar", FieldNameProvider.FieldType.INDEX);
    }

    @Test
    public void fullOptions()
    {
        searchQuery.setMinimumShouldMatch("100%");
        searchQuery.setMmAutoRelax(true);
        searchQuery.setStopwords(false);
        searchQuery.setTie(Float.valueOf(0));
        searchQuery.setPhraseSlop(Integer.valueOf(1));
        searchQuery.setPhraseSlop2(Integer.valueOf(2));
        searchQuery.setPhraseSlop3(Integer.valueOf(3));
        searchQuery.setQueryPhraseSlop(Integer.valueOf(4));
        searchQuery.setSplitOnWhitespace(true);
        searchQuery.setBoost("foo^1.0");
        searchQuery.setUserQuery("Chicago Bulls");

        searchQuery.addBoostFunction(new BoostFunction("foo", "recip(ord($field),1,1000,1000)",BoostField.BoostType.ADDITIVE));
        searchQuery.addBoostFunction(new BoostFunction("bar", "recip(ord($field),1,1000,1000)",BoostField.BoostType.ADDITIVE));

        searchQuery.addPhraseField(new ParameterField("foo", Float.valueOf(0)));
        searchQuery.addPhraseField(new ParameterField("bar", Float.valueOf(0.5f)));

        searchQuery.addPhraseField2(new ParameterField("foo", Float.valueOf(0)));
        searchQuery.addPhraseField2(new ParameterField("bar", Float.valueOf(0.5f)));

        searchQuery.addPhraseField3(new ParameterField("foo", Float.valueOf(0)));
        searchQuery.addPhraseField3(new ParameterField("bar", Float.valueOf(0.5f)));

        searchQuery.addQueryField(new ParameterField("foo", Float.valueOf(0)));
        searchQuery.addQueryField(new ParameterField("bar", Float.valueOf(0.5f)));

        Assert.assertEquals("{!edismax boost=\"foo^1.0\" sow=\"true\" pf=\"foo_text_en bar_text_en^0.5\" ps=\"1\" pf2=\"foo_text_en bar_text_en^0.5\" ps2=\"2\" pf3=\"foo_text_en bar_text_en^0.5\" ps3=\"3\" qf=\"foo_text_en bar_text_en^0.5\" qs=\"4\" tie=\"0.0\" mm=\"100%\" mm.autoRelax=\"true\" stopwords=\"true\" bf=\"sum(recip(ord(foo_text_en),1,1000,1000),recip(ord(bar_text_en),1,1000,1000))\" v=\"Chicago\\ Bulls\"}", builder.buildQuery(searchQuery));
    }

    @Test
    public void whenPhraseFieldsNotDefinedThenPhraseSlopParamIsNotRendered()
    {
        final EnhancedSearchQuery searchQuery = new EnhancedSearchQuery(new FacetSearchConfig(), new IndexedType());
        searchQuery.setPhraseSlop(Integer.valueOf(1));
        searchQuery.setPhraseSlop2(Integer.valueOf(2));
        searchQuery.setPhraseSlop3(Integer.valueOf(3));
        searchQuery.setQueryPhraseSlop(Integer.valueOf(4));
        searchQuery.setUserQuery("Chicago Bulls");

        Assert.assertEquals("{!edismax v=\"Chicago\\ Bulls\"}", builder.buildQuery(searchQuery));
    }

    @Test
    public void scenarioPf2andQf()
    {
        searchQuery.setUserQuery("Chicago Bulls");
        searchQuery.addPhraseField2(new ParameterField("foo", Float.valueOf(0)));
        searchQuery.addPhraseField2(new ParameterField("bar", Float.valueOf(0.5f)));

        searchQuery.addQueryField(new ParameterField("bar", Float.valueOf(1.5f)));

        Assert.assertEquals("{!edismax pf2=\"foo_text_en bar_text_en^0.5\" qf=\"bar_text_en^1.5\" v=\"Chicago\\ Bulls\"}", builder.buildQuery(searchQuery));
    }
}
