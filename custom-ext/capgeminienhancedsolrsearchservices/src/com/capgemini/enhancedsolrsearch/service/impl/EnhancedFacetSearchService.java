/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.capgemini.enhancedsolrsearch.service.impl;

import com.capgemini.enhancedsolrsearch.query.EnhancedSearchQuery;
import com.capgemini.enhancedsolrsearch.query.ParameterField;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.SearchQueryProperty;
import de.hybris.platform.solrfacetsearch.config.SearchQueryTemplate;
import de.hybris.platform.solrfacetsearch.search.BoostField;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.DefaultFacetSearchService;

import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
/**
 * Extends the OOTB {@link DefaultFacetSearchService}, adding support for edismax and boost functions.
 */
public class EnhancedFacetSearchService extends DefaultFacetSearchService
{
    /**
     * Overrides the default method used to create a {@link SearchQuery} and creates a custom {@link EnhancedSearchQuery}
     * supporting custom incomplete phrase searches.
     *
     * @param facetSearchConfig
     * @param indexedType
     * @return searchQuery
     */
    @Override
    public SearchQuery createSearchQuery(final FacetSearchConfig facetSearchConfig, final IndexedType indexedType)
    {
        return new EnhancedSearchQuery(facetSearchConfig, indexedType);
    }

    /**
     * Extends the default method defined on the parent class, and adds support for edismax query.
     *
     * @param facetSearchConfig
     * @param indexedType
     * @param searchQueryTemplate
     * @param searchQuery
     * @param userQuery
     */
    @Override
    protected void populateFreeTextQuery(final FacetSearchConfig facetSearchConfig, final IndexedType indexedType,
                                         final SearchQueryTemplate searchQueryTemplate, final SearchQuery searchQuery, final String userQuery)
    {
        super.populateFreeTextQuery(facetSearchConfig, indexedType, searchQueryTemplate, searchQuery, userQuery);
        if (!(searchQuery instanceof EnhancedSearchQuery))
        {
            return;
        }

        final EnhancedSearchQuery enhancedSearchQuery = (EnhancedSearchQuery)searchQuery;
        final Iterator it = searchQueryTemplate.getSearchQueryProperties().values().iterator();

        while (it.hasNext())
        {
            final SearchQueryProperty searchQueryProperty = (SearchQueryProperty) it.next();

            if(searchQueryProperty.isPhraseField())
            {
                enhancedSearchQuery.addPhraseField(new ParameterField(searchQueryProperty.getIndexedProperty(), searchQueryProperty.getPhraseFieldBoost()));
            }

            if(searchQueryProperty.isPhraseField2())
            {
                enhancedSearchQuery.addPhraseField2(new ParameterField(searchQueryProperty.getIndexedProperty(), searchQueryProperty.getPhraseField2Boost()));
            }

            if(searchQueryProperty.isPhraseField3())
            {
                enhancedSearchQuery.addPhraseField3(new ParameterField(searchQueryProperty.getIndexedProperty(), searchQueryProperty.getPhraseField3Boost()));
            }

            if(searchQueryProperty.isQueryField())
            {
                enhancedSearchQuery.addQueryField(new ParameterField(searchQueryProperty.getIndexedProperty(), searchQueryProperty.getQueryFieldBoost()));
            }

        }

        enhancedSearchQuery.setEdismaxEnabled(searchQueryTemplate.isEdismaxEnabled());
        enhancedSearchQuery.setMinimumShouldMatch(searchQueryTemplate.getMinimumShouldMatch());
        enhancedSearchQuery.setMmAutoRelax(searchQueryTemplate.isMmAutoRelax());
        enhancedSearchQuery.setStopwords(searchQueryTemplate.isStopwords());
        enhancedSearchQuery.setTie(searchQueryTemplate.getTie());
        enhancedSearchQuery.setPhraseSlop(searchQueryTemplate.getPhraseSlop());
        enhancedSearchQuery.setPhraseSlop2(searchQueryTemplate.getPhraseSlop2());
        enhancedSearchQuery.setPhraseSlop3(searchQueryTemplate.getPhraseSlop3());
        enhancedSearchQuery.setQueryPhraseSlop(searchQueryTemplate.getQueryPhraseSlop());
        enhancedSearchQuery.setBoost(searchQueryTemplate.getBoost());
        enhancedSearchQuery.setSplitOnWhitespace(searchQueryTemplate.isSplitOnWhitespace());

    }

    /**
     * Overrides OOTB method of the same name and adds custom boost functions to SearchQuery.
     *
     * @param facetSearchConfig
     * @param indexedType
     * @param searchQueryTemplate
     * @param searchQuery
     */
    @Override
    protected void populateFields(final FacetSearchConfig facetSearchConfig, final IndexedType indexedType,
                                  final SearchQueryTemplate searchQueryTemplate, final SearchQuery searchQuery)
    {
        super.populateFields(facetSearchConfig, indexedType, searchQueryTemplate, searchQuery);
        populateBoostFunctions(searchQueryTemplate, searchQuery);
    }

    /**
     * Adds boost functions to the Solr SearchQuery, if defined on the SearchQueryProperty.
     *
     * @param searchQueryTemplate
     * @param searchQuery
     */
    protected void populateBoostFunctions(final SearchQueryTemplate searchQueryTemplate, final SearchQuery searchQuery)
    {
        if (!(searchQuery instanceof EnhancedSearchQuery))
        {
            return;
        }
        final Iterator it = searchQueryTemplate.getSearchQueryProperties().values().iterator();

        while (it.hasNext())
        {
            final SearchQueryProperty searchQueryProperty = (SearchQueryProperty) it.next();

            if (StringUtils.isNotBlank(searchQueryProperty.getMultiplicativeBoostFunctionExp()))
            {
                ((EnhancedSearchQuery) searchQuery).addBoostFunction(searchQueryProperty.getIndexedProperty(),
                        searchQueryProperty.getMultiplicativeBoostFunctionExp(), BoostField.BoostType.MULTIPLICATIVE);
            }

            if (StringUtils.isNotBlank(searchQueryProperty.getAdditiveBoostFunctionExp()))
            {
                ((EnhancedSearchQuery) searchQuery).addBoostFunction(searchQueryProperty.getIndexedProperty(),
                        searchQueryProperty.getAdditiveBoostFunctionExp(), BoostField.BoostType.ADDITIVE);
            }
        }
    }
}
