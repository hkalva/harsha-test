/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.capgemini.enhancedsolrsearch.query;

import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.search.BoostField;
import de.hybris.platform.solrfacetsearch.search.FieldNameTranslator;
import de.hybris.platform.solrfacetsearch.search.FreeTextQueryBuilder;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.springframework.beans.factory.annotation.Required;
import java.util.List;
import java.util.stream.Collectors;
/**
 * @author lyonscg
 */
public class EdismaxQueryBuilder implements FreeTextQueryBuilder
{
    /**
     * declare sting constant.
     */
    private static final String FIELD_PLACEHOLDER = "$field";
    /**
     * the fieldNameTranslator.
     */
    private FieldNameTranslator fieldNameTranslator;

    /**
     * 
     * @return
     */
    protected FieldNameTranslator getFieldNameTranslator()
    {
        return fieldNameTranslator;
    }

    /**
     * 
     * @param fieldNameTranslator
     */
    @Required
    public void setFieldNameTranslator(final FieldNameTranslator fieldNameTranslator) {
        this.fieldNameTranslator = fieldNameTranslator;
    }

    private String renderFieldFragment(final EnhancedSearchQuery searchQuery, final List<ParameterField> fields)
    {
        int length = fields.size();
        final StringBuilder fragment = new StringBuilder();
        for(int i = 0; i < length; i++)
        {
            final ParameterField field = fields.get(i);
            if(i > 0)
            {
                fragment.append(" ");
            }

            fragment.append(getFieldNameTranslator().translate(searchQuery, field.getField(),
                    FieldNameProvider.FieldType.INDEX));

            if(field.getBoost() != null && field.getBoost() > 0)
            {
                fragment.append("^").append(field.getBoost());
            }
        }

        return fragment.toString();
    }

    private String renderBoostFunctions(final EnhancedSearchQuery searchQuery)
    {
        final List<String> boostFunctions = searchQuery.getBoostFunctions().stream().map(bf -> {
                    final String convertedField = getFieldNameTranslator().translate(searchQuery, bf.getField(),
                            FieldNameProvider.FieldType.INDEX);
                    return bf.getExpression().replace(FIELD_PLACEHOLDER, convertedField);
                }
        ).collect(Collectors.toList());

        final StringBuilder query = new StringBuilder(256);
        query.append("sum(");
        query.append(StringUtils.join(boostFunctions, ","));
        query.append(')');
        return query.toString();

    }

    @Override
    public String buildQuery(final SearchQuery searchQuery)
    {
        final EnhancedSearchQuery enhancedSearchQuery = (EnhancedSearchQuery)searchQuery;
        StringBuilder query = new StringBuilder();
        query.append("{!edismax");

        if(StringUtils.isNotBlank(enhancedSearchQuery.getBoost()))
        {
            query.append(" boost=\"");
            query.append(enhancedSearchQuery.getBoost());
            query.append("\"");
        }

        if(enhancedSearchQuery.isSplitOnWhitespace())
        {
            query.append(" sow=\"true\"");
        }
        
        query=appendPhraseFieldParam(query,enhancedSearchQuery);
        
        query=appendPhraseField2Param(query,enhancedSearchQuery);
        
        query=appendPhraseField3Param(query,enhancedSearchQuery);
        
        query=appendQueryFieldParam(query,enhancedSearchQuery);

        if(enhancedSearchQuery.getTie() != null)
        {
            query.append(" tie=\"").append(enhancedSearchQuery.getTie()).append("\"");
        }

        if(enhancedSearchQuery.getMinimumShouldMatch() != null)
        {
            query.append(" mm=\"").append(enhancedSearchQuery.getMinimumShouldMatch()).append("\"");
        }

        if(enhancedSearchQuery.isMmAutoRelax())
        {
            query.append( " mm.autoRelax=\"true\"");
        }

        if(!enhancedSearchQuery.isStopwords())
        {
            query.append(" stopwords=\"true\"");
        }

        final List<BoostFunction> additiveBoostFunctions = ((EnhancedSearchQuery) searchQuery).getBoostFunctions().stream().filter(
                bf -> BoostField.BoostType.ADDITIVE.equals(bf.getBoostType())).collect(Collectors.toList());

        if(CollectionUtils.isNotEmpty(additiveBoostFunctions))
        {
            query.append(" bf=\"");
            query.append(renderBoostFunctions(enhancedSearchQuery));
            query.append("\"");
        }

        query.append(" v=\"");
        query.append(ClientUtils.escapeQueryChars(searchQuery.getUserQuery()));
        query.append("\"");
        query.append("}");
        return query.toString();
    }

    private StringBuilder appendPhraseFieldParam(StringBuilder query, EnhancedSearchQuery enhancedSearchQuery)
    {
        if(CollectionUtils.isNotEmpty(enhancedSearchQuery.getPhraseFieldParam()))
        {
            query.append(" pf=\"");
            query.append(renderFieldFragment(enhancedSearchQuery, enhancedSearchQuery.getPhraseFieldParam()));
            query.append("\"");

            if(enhancedSearchQuery.getPhraseSlop() != null)
            {
                query.append(" ps=\"").append(enhancedSearchQuery.getPhraseSlop()).append("\"");
            }
        }
        return query;
    }
    
    private StringBuilder appendPhraseField2Param(StringBuilder query, EnhancedSearchQuery enhancedSearchQuery)
    {
        if(CollectionUtils.isNotEmpty(enhancedSearchQuery.getPhraseField2Param()))
        {
            query.append(" pf2=\"");
            query.append(renderFieldFragment(enhancedSearchQuery, enhancedSearchQuery.getPhraseField2Param()));
            query.append("\"");

            if(enhancedSearchQuery.getPhraseSlop2() != null)
            {
                query.append(" ps2=\"").append(enhancedSearchQuery.getPhraseSlop2()).append("\"");
            }
        }
        return query;
    }
    
    private StringBuilder appendPhraseField3Param(StringBuilder query, EnhancedSearchQuery enhancedSearchQuery)
    {
        if(CollectionUtils.isNotEmpty(enhancedSearchQuery.getPhraseField3Param()))
        {
            query.append(" pf3=\"");
            query.append(renderFieldFragment(enhancedSearchQuery, enhancedSearchQuery.getPhraseField3Param()));
            query.append("\"");

            if(enhancedSearchQuery.getPhraseSlop3() != null)
            {
                query.append(" ps3=\"").append(enhancedSearchQuery.getPhraseSlop3()).append("\"");
            }
        }
        return query;
    }
    
    private StringBuilder appendQueryFieldParam(StringBuilder query, EnhancedSearchQuery enhancedSearchQuery)
    {
        if(CollectionUtils.isNotEmpty(enhancedSearchQuery.getQueryFieldParam()))
        {
            query.append(" qf=\"");
            query.append(renderFieldFragment(enhancedSearchQuery, enhancedSearchQuery.getQueryFieldParam()));
            query.append("\"");

            if(enhancedSearchQuery.getQueryPhraseSlop() != null)
            {
                query.append(" qs=\"").append(enhancedSearchQuery.getQueryPhraseSlop()).append("\"");
            }
        }
        return query;
    }
}
