/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.capgemini.enhancedsolrsearch.query;

import java.io.Serializable;


/**
 * Represents a field with optional boost.
 *
 * @author lyonscg
 */
public class ParameterField implements Serializable
{
    private static final long serialVersionUID = 1L;
    private String field;
    private Float boost;

    public ParameterField(final String field)
    {
        this.field = field;
    }


    public ParameterField(final String field, final Float boost)
    {
        this.field = field;
        this.boost = boost;
    }

    public String getField()
    {
        return field;
    }

    public void setField(final String field)
    {
        this.field = field;
    }

    public Float getBoost()
    {
        return boost;
    }

    public void setBoost(final Float boost)
    {
        this.boost = boost;
    }
}
