/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.capgemini.enhancedsolrsearch.query;
import de.hybris.platform.solrfacetsearch.search.BoostField;

import java.io.Serializable;


/**
 * Custom Boost Function for SearchQuery.
 *
 * @author lyonscg
 */
public class BoostFunction implements Serializable
{
    private static final long serialVersionUID = 1L;

    private BoostField.BoostType boostType;
    private String field;
    private String expression;

    /**
     * Ctor.
     *
     * @param field
     * @param expression
     * @param boostType
     */
    public BoostFunction(final String field, final String expression, final BoostField.BoostType boostType)
    {
        this.boostType = boostType;
        this.expression = expression;
        this.field = field;
    }

    public BoostField.BoostType getBoostType()
    {
        return boostType;
    }

    public String getField()
    {
        return field;
    }

    public String getExpression()
    {
        return expression;
    }
}
