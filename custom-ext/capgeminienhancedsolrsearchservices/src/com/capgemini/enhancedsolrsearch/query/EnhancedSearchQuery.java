/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.capgemini.enhancedsolrsearch.query;

import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.BoostField;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import java.util.ArrayList;
import java.util.List;
/**
 * enhanced based on searh query.
 */
public class EnhancedSearchQuery extends SearchQuery
{
    /**
     * the boostFunctions.
     */
    private final List<BoostFunction> boostFunctions;
    /**
     * the phraseFieldParam.
     */
    private List<ParameterField> phraseFieldParam;
    /**
     * the phraseField2Param.
     */
    private List<ParameterField> phraseField2Param;
    /**
     * the phraseField3Param.
     */
    private List<ParameterField> phraseField3Param;
    /**
     * the queryFieldParam.
     */
    private List<ParameterField> queryFieldParam;
    /**
     * the tie.
     */
    private Float tie;
    /**
     * the phraseSlop.
     */
    private Integer phraseSlop;
    /**
     * the phraseSlop2.
     */
    private Integer phraseSlop2;
    /**
     * the phraseSlop3.
     */
    private Integer phraseSlop3;
    /**
     * the queryPhraseSlop.
     */
    private Integer queryPhraseSlop;
    /**
     * the minimumShouldMatch.
     */
    private String minimumShouldMatch;
    /**
     * the boost.
     */
    private String boost;
    /**
     * boolean mmAutoRelax.
     */
    private boolean mmAutoRelax = false;
    /**
     * boolean stopwords.
     */
    private boolean stopwords = true;
    /**
     * splitOnWhitespace.
     */
    private boolean splitOnWhitespace = false;
    /**
     * the edismaxEnabled.
     */
    private boolean edismaxEnabled = false;

    /**
     * 
     * @param facetSearchConfig
     * @param indexedType
     */
    public EnhancedSearchQuery(final FacetSearchConfig facetSearchConfig, final IndexedType indexedType)
    {
        super(facetSearchConfig, indexedType);
        boostFunctions = new ArrayList<>();
        phraseFieldParam = new ArrayList<>();
        phraseField2Param = new ArrayList<>();
        phraseField3Param = new ArrayList<>();
        queryFieldParam = new ArrayList<>();
    }

    /**
     * Add a new Boost Function to the Solr SearchQuery.
     *
     * @param field
     * @param expression
     * @param boostType
     */
    public void addBoostFunction(final String field, final String expression, final BoostField.BoostType boostType)
    {
        addBoostFunction(new BoostFunction(field, expression, boostType));
    }

    /**
     * Add a new Boost Function to the Solr SearchQuery
     *
     * @param boostFunction
     */
    public void addBoostFunction(final BoostFunction boostFunction)
    {
        boostFunctions.add(boostFunction);
    }

    public void addQueryField(final ParameterField field)
    {
        queryFieldParam.add(field);
    }

    public void addPhraseField(final ParameterField field)
    {
        phraseFieldParam.add(field);
    }

    public void addPhraseField2(final ParameterField field)
    {
        phraseField2Param.add(field);
    }

    public void addPhraseField3(final ParameterField field)
    {
        phraseField3Param.add(field);
    }

    public List<ParameterField> getPhraseFieldParam()
    {
        return phraseFieldParam;
    }

    public List<ParameterField> getPhraseField2Param()
    {
        return phraseField2Param;
    }

    public List<ParameterField> getPhraseField3Param()
    {
        return phraseField3Param;
    }

    public List<ParameterField> getQueryFieldParam()
    {
        return queryFieldParam;
    }

    public List<BoostFunction> getBoostFunctions()
    {
        return boostFunctions;
    }

    /**
     * Returns the edismaxEnabled flag value.
     *
     * @return edismaxEnabled flag value.
     */
    public boolean isEdismaxEnabled()
    {
        return edismaxEnabled;
    }

    public void setEdismaxEnabled(final boolean edismaxEnabled)
    {
        this.edismaxEnabled = edismaxEnabled;
    }

    public void setBoost(final String boost)
    {
        this.boost = boost;
    }

    public void setSplitOnWhitespace(final boolean splitOnWhitespace) {
        this.splitOnWhitespace = splitOnWhitespace;
    }

    public boolean isSplitOnWhitespace()
    {
        return splitOnWhitespace;
    }

    public String getBoost()
    {
        return boost;
    }

    public Float getTie()
    {
        return tie;
    }

    public void setTie(final Float tie)
    {
        this.tie = tie;
    }

    public Integer getPhraseSlop()
    {
        return phraseSlop;
    }

    public void setPhraseSlop(final Integer phraseSlop)
    {
        this.phraseSlop = phraseSlop;
    }

    public Integer getPhraseSlop2()
    {
        return phraseSlop2;
    }

    public void setPhraseSlop2(final Integer phraseSlop2)
    {
        this.phraseSlop2 = phraseSlop2;
    }

    public Integer getPhraseSlop3()
    {
        return phraseSlop3;
    }

    public void setPhraseSlop3(final Integer phraseSlop3)
    {
        this.phraseSlop3 = phraseSlop3;
    }

    public Integer getQueryPhraseSlop()
    {
        return queryPhraseSlop;
    }

    public void setQueryPhraseSlop(final Integer queryPhraseSlop)
    {
        this.queryPhraseSlop = queryPhraseSlop;
    }

    public String getMinimumShouldMatch()
    {
        return minimumShouldMatch;
    }

    public void setMinimumShouldMatch(final String minimumShouldMatch)
    {
        this.minimumShouldMatch = minimumShouldMatch;
    }

    public boolean isMmAutoRelax()
    {
        return mmAutoRelax;
    }

    public void setMmAutoRelax(boolean mmAutoRelax)
    {
        this.mmAutoRelax = mmAutoRelax;
    }

    public boolean isStopwords()
    {
        return stopwords;
    }

    public void setStopwords(boolean stopwords)
    {
        this.stopwords = stopwords;
    }
}
