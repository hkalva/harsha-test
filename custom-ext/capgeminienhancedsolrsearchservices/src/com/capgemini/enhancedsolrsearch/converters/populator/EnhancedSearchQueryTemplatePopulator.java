/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.capgemini.enhancedsolrsearch.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.config.SearchQueryTemplate;
import de.hybris.platform.solrfacetsearch.model.SolrSearchQueryTemplateModel;
/**
 * Populator class to convert the model object to the SearchQueryTemplate object.
 */
public class EnhancedSearchQueryTemplatePopulator implements Populator<SolrSearchQueryTemplateModel, SearchQueryTemplate>
{
    @Override
    public void populate(final SolrSearchQueryTemplateModel solrSearchQueryTemplateModel,
                         final SearchQueryTemplate searchQueryTemplate) throws ConversionException
    {
        searchQueryTemplate.setEdismaxEnabled(solrSearchQueryTemplateModel.isEdismaxEnabled());
        searchQueryTemplate.setMinimumShouldMatch(solrSearchQueryTemplateModel.getMinimumShouldMatch());
        searchQueryTemplate.setMmAutoRelax(searchQueryTemplate.isMmAutoRelax());
        searchQueryTemplate.setPhraseSlop(solrSearchQueryTemplateModel.getPhraseSlop());
        searchQueryTemplate.setPhraseSlop2(solrSearchQueryTemplateModel.getPhraseSlop2());
        searchQueryTemplate.setPhraseSlop3(solrSearchQueryTemplateModel.getPhraseSlop3());
        searchQueryTemplate.setQueryPhraseSlop(solrSearchQueryTemplateModel.getQueryPhraseSlop());
        searchQueryTemplate.setTie(solrSearchQueryTemplateModel.getTie());
        searchQueryTemplate.setStopwords(searchQueryTemplate.isStopwords());
        searchQueryTemplate.setSplitOnWhitespace(solrSearchQueryTemplateModel.isSplitOnWhitespace());
        searchQueryTemplate.setBoost(solrSearchQueryTemplateModel.getBoost());
    }

}
