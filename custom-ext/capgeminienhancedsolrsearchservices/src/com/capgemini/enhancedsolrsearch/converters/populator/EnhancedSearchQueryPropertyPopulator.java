/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.capgemini.enhancedsolrsearch.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.config.SearchQueryProperty;
import de.hybris.platform.solrfacetsearch.model.SolrSearchQueryPropertyModel;
/**
 * @author lyonscg
 * 
 * populate the search query property value.
 */
public class EnhancedSearchQueryPropertyPopulator implements Populator<SolrSearchQueryPropertyModel, SearchQueryProperty>
{
    @Override
    public void populate(final SolrSearchQueryPropertyModel source, final SearchQueryProperty target)
            throws ConversionException
    {
        target.setMultiplicativeBoostFunctionExp(source.getMultiplicativeBoostFunctionExp());
        target.setPhraseField(source.isPhraseField());
        target.setPhraseFieldBoost(source.getPhraseFieldBoost());
        target.setPhraseField2(source.isPhraseField2());
        target.setPhraseField2Boost(source.getPhraseField2Boost());
        target.setPhraseField3(source.isPhraseField3());
        target.setPhraseField3Boost(source.getPhraseField3Boost());
        target.setQueryField(source.isQueryField());
        target.setQueryFieldBoost(source.getQueryFieldBoost());
        target.setAdditiveBoostFunctionExp(source.getAdditiveBoostFunctionExp());
    }
}
