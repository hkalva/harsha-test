/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.capgemini.enhancedsolrsearch.converters.populator;

import com.capgemini.enhancedsolrsearch.query.BoostFunction;
import com.capgemini.enhancedsolrsearch.query.EnhancedSearchQuery;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.search.BoostField;
import de.hybris.platform.solrfacetsearch.search.FreeTextQueryBuilder;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.populators.FacetSearchQueryBasicPopulator;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.springframework.beans.factory.annotation.Required;
/**
 * populator.
 */
public class EnhancedFacetSearchQueryBasicPopulator extends FacetSearchQueryBasicPopulator
{
    /**
     * the text query builder.
     */
    private FreeTextQueryBuilder edismaxQueryBuilder;

    /**
     * the constant for field placeholder.
     */
    private static final String FIELD_PLACEHOLDER = "$field";

    /**
     * Extends the OOTB method of the same name. Adds custom function boost queries.
     *
     * @param searchQuery
     * @param multiplicativeBoosts
     * @param additiveBoosts
     */
    @Override
    protected void generateBoostQueries(final SearchQuery searchQuery, final List<String> multiplicativeBoosts,
                                        final List<String> additiveBoosts)
    {
        super.generateBoostQueries(searchQuery, multiplicativeBoosts, additiveBoosts);

        if (searchQuery instanceof EnhancedSearchQuery)
        {
            for (final BoostFunction bf : ((EnhancedSearchQuery) searchQuery).getBoostFunctions())
            {
                final String convertedField = getFieldNameTranslator().translate(searchQuery, bf.getField(),
                        FieldNameProvider.FieldType.INDEX);
                final String expression = bf.getExpression().replace(FIELD_PLACEHOLDER, convertedField);
                if (BoostField.BoostType.MULTIPLICATIVE.equals(bf.getBoostType()))
                {
                    multiplicativeBoosts.add(expression);
                }
            }
        }
    }

    protected boolean isEdisMaxEnabled(final SearchQuery searchQuery)
    {
        return searchQuery instanceof EnhancedSearchQuery && ((EnhancedSearchQuery) searchQuery).isEdismaxEnabled();
    }

    /**
     * If edismax is enabled, the OOTB free text query builder will not be used.
     */
    @Override
    protected void generateFreeTextQuery(final SearchQuery searchQuery, final List<String> queries)
    {
        if (!isEdisMaxEnabled(searchQuery))
        {
            super.generateFreeTextQuery(searchQuery, queries);
        }
    }

    /**
     * Overrides OOTB method to conditionally build an edismax query rather than using the OOTB lucene-based query.
     */
    @Override
    protected void populateSolrQuery(final SolrQuery solrQuery, final SearchQuery searchQuery, final List<String> queries,
                                     final List<String> multiplicativeBoosts, final List<String> additiveBoosts)
    {
        if (!isEdisMaxEnabled(searchQuery))
        {
            super.populateSolrQuery(solrQuery, searchQuery, queries, multiplicativeBoosts, additiveBoosts);
            return;
        }

        final String query = this.buildQuery(searchQuery, queries);
        final String multiplicativeBoostsFunction = this.buildMultiplicativeBoostsFunction(searchQuery, multiplicativeBoosts);
        final String additiveBoostsFunction = this.buildAdditiveBoostsFunction(searchQuery, additiveBoosts);
        final StringBuilder queryBuilder = new StringBuilder(256);
        queryBuilder.append("{!boost");
        if (StringUtils.isNotBlank(multiplicativeBoostsFunction))
        {
            queryBuilder.append(" b=$");
            queryBuilder.append("ymb");
            solrQuery.set("ymb", multiplicativeBoostsFunction);
        }

        queryBuilder.append("}(");
        if (StringUtils.isNotBlank(query))
        {
            queryBuilder.append(getEdismaxQueryBuilder().buildQuery(searchQuery));
            solrQuery.set("yq", this.buildQuery(searchQuery, queries));
        }

        if (StringUtils.isNotBlank(query) && StringUtils.isNotBlank(additiveBoostsFunction))
        {
            queryBuilder.append(' ');
        }

        if (StringUtils.isNotBlank(additiveBoostsFunction))
        {
            queryBuilder.append("{!func v=$");
            queryBuilder.append("yab");
            queryBuilder.append('}');
            solrQuery.set("yab", additiveBoostsFunction);
        }

        queryBuilder.append(')');
        solrQuery.setQuery(queryBuilder.toString());
    }

    protected FreeTextQueryBuilder getEdismaxQueryBuilder()
    {
        return edismaxQueryBuilder;
    }

    @Required
    public void setEdismaxQueryBuilder(final FreeTextQueryBuilder edismaxQueryBuilder)
    {
        this.edismaxQueryBuilder = edismaxQueryBuilder;
    }
}
