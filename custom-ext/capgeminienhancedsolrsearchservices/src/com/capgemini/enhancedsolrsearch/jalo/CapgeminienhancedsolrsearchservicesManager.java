package com.capgemini.enhancedsolrsearch.jalo;

import com.capgemini.enhancedsolrsearch.constants.CapgeminienhancedsolrsearchservicesConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("PMD")
public class CapgeminienhancedsolrsearchservicesManager extends GeneratedCapgeminienhancedsolrsearchservicesManager
{
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger( CapgeminienhancedsolrsearchservicesManager.class.getName() );
	
	public static final CapgeminienhancedsolrsearchservicesManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (CapgeminienhancedsolrsearchservicesManager) em.getExtension(CapgeminienhancedsolrsearchservicesConstants.EXTENSIONNAME);
	}
	
}
