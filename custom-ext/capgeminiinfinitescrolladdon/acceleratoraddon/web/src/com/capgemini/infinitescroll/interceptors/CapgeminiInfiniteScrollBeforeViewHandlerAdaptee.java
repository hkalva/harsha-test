package com.capgemini.infinitescroll.interceptors;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.ui.ModelMap;

import com.capgemini.infinitescroll.constants.CapgeminiinfinitescrolladdonConstants;

import de.hybris.platform.acceleratorcms.data.CmsPageRequestContextData;
import de.hybris.platform.acceleratorcms.services.CMSPageContextService;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorservices.data.RequestContextData;
import de.hybris.platform.acceleratorservices.storefront.data.JavaScriptVariableData;
import de.hybris.platform.acceleratorservices.util.SpringHelper;
import de.hybris.platform.addonsupport.config.javascript.BeforeViewJsPropsHandlerAdaptee;
import de.hybris.platform.addonsupport.config.javascript.JavaScriptVariableDataFactory;
import de.hybris.platform.addonsupport.interceptors.BeforeViewHandlerAdaptee;

/**
 *
 * @author lyonscg
 *
 */
public class CapgeminiInfiniteScrollBeforeViewHandlerAdaptee implements BeforeViewHandlerAdaptee
{
    /**
     * the logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(CapgeminiInfiniteScrollBeforeViewHandlerAdaptee.class);
    
    /**
     * constant for current page.
     */
    private static final String SEARCH_PAGED_DATA_CURRENT_PAGE = "searchPageData.pagination.currentPage";
    
    /**
     * constant for number of pages.
     */
    private static final String SEARCH_PAGED_DATA_NUMBER_OF_PAGES = "searchPageData.pagination.numberOfPages";
    
    /**
     * constant for sticky header.
     */
    private static final String STICKY_HEADER = "infinitescroll.stickyheader";
    
    /**
     * the site config service.
     */
    private SiteConfigService siteConfigService;
    
    /**
     * the cms page context service.
     */
    private CMSPageContextService cmsPageContextService;

    @Override
    public String beforeView(HttpServletRequest request, HttpServletResponse response, ModelMap model, String viewName)
            throws Exception
    {
        RequestContextData requestContext = getRequestContextData(request);
        
        if ((requestContext != null) && (requestContext.getSearch() != null) && (requestContext.getSearch().getPagination() != null))
        {
            Map<String, List<JavaScriptVariableData>> jsAddOnsVariables = (Map<String, List<JavaScriptVariableData>>) model
                    .get(BeforeViewJsPropsHandlerAdaptee.JS_VARIABLES_MODEL_NAME);
            
            if (jsAddOnsVariables == null)
            {
                jsAddOnsVariables = new HashMap<>();
                model.addAttribute(BeforeViewJsPropsHandlerAdaptee.JS_VARIABLES_MODEL_NAME, jsAddOnsVariables);
            }
            
            List<JavaScriptVariableData> variables = jsAddOnsVariables.get(CapgeminiinfinitescrolladdonConstants.EXTENSIONNAME);
            
            if (variables == null)
            {
                variables = new LinkedList<>();
                jsAddOnsVariables.put(CapgeminiinfinitescrolladdonConstants.EXTENSIONNAME, variables);
            }
            
            //store the search-related data needed by acc.pagination.js
            int numberOfPages = requestContext.getSearch().getPagination().getNumberOfPages();
            LOG.debug("numberOfPages: {}", numberOfPages);
            JavaScriptVariableData data = JavaScriptVariableDataFactory.create(SEARCH_PAGED_DATA_NUMBER_OF_PAGES, String.valueOf(numberOfPages));
            variables.add(data);
            int currentPage = requestContext.getSearch().getPagination().getCurrentPage();
            LOG.debug("currentPage: {}", currentPage);
            data = JavaScriptVariableDataFactory.create(SEARCH_PAGED_DATA_CURRENT_PAGE, String.valueOf(currentPage));
            variables.add(data);
            boolean isStickyHeader = true;
            
            final CmsPageRequestContextData cmsPageRequestContextData = getCmsPageContextService()
                    .initialiseCmsPageContextForRequest(request);

            if ((cmsPageRequestContextData != null) && cmsPageRequestContextData.isPreview())
            {
                LOG.debug("Page in preview mode");
                isStickyHeader = false;
            }
            
            LOG.debug("isStickyHeader: {}", isStickyHeader);
            data = JavaScriptVariableDataFactory.create(STICKY_HEADER, String.valueOf(isStickyHeader));
            variables.add(data);
        }
        
        return viewName;
    }
    
    /**
     * 
     * @param siteConfigService - the site config service.
     */
    @Required
    public void setSiteConfigService(SiteConfigService siteConfigService)
    {
        this.siteConfigService = siteConfigService;
    }

    /**
    *
    * @param cmsPageContextService
    *            - the cms page context service.
    */
   @Required
   public void setCmsPageContextService(final CMSPageContextService cmsPageContextService)
   {
       this.cmsPageContextService = cmsPageContextService;
   }

    /**
     * 
     * @param request - the http servlet request.
     * @return RequestContextData
     */
    protected RequestContextData getRequestContextData(final HttpServletRequest request)
    {
        return SpringHelper.getSpringBean(request, "requestContextData", RequestContextData.class, true);
    }
    
    /**
     * 
     * @return SiteConfigService
     */
    protected SiteConfigService getSiteConfigService()
    {
        return siteConfigService;
    }

    /**
    *
    * @return CMSPageContextService
    */
   protected CMSPageContextService getCmsPageContextService()
   {
       return cmsPageContextService;
   }
    
    
}
