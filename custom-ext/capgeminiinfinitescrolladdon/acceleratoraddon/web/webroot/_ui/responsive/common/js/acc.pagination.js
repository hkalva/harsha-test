ACC.pagination = {
	$pagination: $('.pagination-wrap > .pagination'),
	$productGrid: $('.product__listing.product__grid'),
	
	_autoload: [
		['stickyHeader', $('.page-login').length === 0],
		['init', $('.product__listing.product__grid').length > 0]
	],

	currentPage: 0,
	maxPages: 0,
	pauseLoading: false,
	fetchInterval: null,

	init: function() {
		ACC.pagination.$pagination.css('display', 'none');
		ACC.pagination.$productGrid.addClass('js-lazy-load');
		ACC.pagination.currentPage = ACC.addons.capgeminiinfinitescrolladdon['searchPageData.pagination.currentPage'] ? Number(ACC.addons.capgeminiinfinitescrolladdon['searchPageData.pagination.currentPage']) : 0;
		ACC.pagination.maxPages = ACC.addons.capgeminiinfinitescrolladdon['searchPageData.pagination.numberOfPages'] ? Number(ACC.addons.capgeminiinfinitescrolladdon['searchPageData.pagination.numberOfPages']) : 0;
		ACC.pagination.setupScroll();
	},
	
	stickyHeader: function() {
		if (ACC.addons.capgeminiinfinitescrolladdon['infinitescroll.stickyheader'] === 'true') {
			$('header').addClass('navbar-fixed-top');
			$('.branding-mobile').addClass('navbar-fixed-top');
		}
	},
    
  setupScroll: function() {
		var didScroll = false,
			scrollTop, cHeight, cPosition, wHeight, loadMore;

		$(document).on('scroll', function() {
			didScroll = true;
		});

		ACC.pagination.fetchInterval = setInterval(function() {
			if(didScroll && $('.js-lazy-load').length > 0) {
				var $target = $('.js-lazy-load');

				didScroll = false,
				scrollTop = $(window).scrollTop(),
				cHeight = $target.height(),
				cPosition = $target.position(),
				wHeight = $(window).height();

				var atBottom = (cPosition.top + cHeight - scrollTop) < wHeight;

				if(atBottom && ACC.pagination.pauseLoading === false) {
					// Stopping timer
					ACC.pagination.pauseLoading = true;

					//ACC.util.showLoading();
					
					ACC.pagination.fetchPage();
				}
			}
		}, 350);
	},

	fetchPage: function() {
		var nextPage = Number(ACC.pagination.currentPage) + 1;
			url = ACC.pagination.generateAjaxUrl(nextPage);

		if(nextPage < ACC.pagination.maxPages) {
			$.ajax({
				type: 'GET',
				url: url,
				success: ACC.pagination.appendProducts,
				error: function(data) {
					console.log('Error: ', data);
				},
				complete: function() {
					ACC.pagination.pauseLoading = false;
					ACC.pagination.currentPage = nextPage;
					ACC.product.enableAddToCartButton();
				}
			});
		}

		
	},

	appendProducts: function(data) {
		var $results = $(data).find('.product__listing.product__grid'),
			$resultItems = $results.find('.product-item');

		ACC.pagination.$productGrid.append($resultItems);
	},

	returnURLParams: function() {
		var currentUrl = window.location.origin + window.location.pathname,
			currentQuery = window.location.search.substring(1),
			params = {};

		if(currentQuery) {
			params = ACC.pagination.parseParams(currentQuery);
		}

		if(params.q) {
			params.q = decodeURIComponent(params.q);
		}

		return params;
	},

	parseParams: function(search) {
		if(search.indexOf('=') > 0) {
			return JSON.parse('{"' + search.replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
		} else {
			return false;
		}
	},

	generateAjaxUrl: function(page) {
		var uri = window.location.href.split('#')[0],
			url = uri.split('?')[0],
			params = ACC.pagination.returnURLParams();

		url = url.replace(/\/$/,"");

		if(params.q) {
			params.q = decodeURIComponent(params.q);
		} else {
			params.q = '';
		}

		if(params.text) {
			params.q = (params.q) ? params.text + '::' + params.q : params.text;
		}

		if(page) {
			params.page = page;
		}

		return url + '?' + decodeURIComponent($.param(params));
	}
}