/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customproductcatalog.ruledefinitions.conditions;

import de.hybris.platform.ruledefinitions.AmountOperator;
import de.hybris.platform.ruledefinitions.CollectionOperator;
import de.hybris.platform.ruledefinitions.conditions.AbstractRuleConditionTranslator;
import de.hybris.platform.ruledefinitions.conditions.builders.IrConditions;
import de.hybris.platform.ruledefinitions.conditions.builders.RuleIrAttributeConditionBuilder;
import de.hybris.platform.ruledefinitions.conditions.builders.RuleIrAttributeRelConditionBuilder;
import de.hybris.platform.ruledefinitions.conditions.builders.RuleIrGroupConditionBuilder;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerContext;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeOperator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeRelCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrExistsCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupOperator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrLocalVariablesContainer;
import de.hybris.platform.ruleengineservices.compiler.RuleIrNotCondition;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.ruleengineservices.rao.ProductRAO;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionDefinitionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleParameterData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.capgemini.customproductcatalog.ruleengineservices.rao.CustomProductCatalogRAO;


/**
 * This class that extends AbstractRuleConditionTranslator that will house and evaluate custom product catalog
 * inclusions and/or exclusions. The class will function similarly to the OOTB
 * RuleQualifyingCategoriesConditionTranslator.
 */
public class RuleQualifyingCustomProductCatalogsConditionTranslator extends AbstractRuleConditionTranslator
{
    
    /**
     * constant for customProductCatalogs.
     */
    private static final String CUSTOM_PRODUCT_CATALOGS = "customProductCatalogs";
    
	protected void addExcludedProductsAndCustomProductCatalogConditions(final RuleCompilerContext context,
			final RuleParameterData excludedCustomProductCatalogsParameter, final RuleParameterData excludedProductsParameter,
			final RuleIrGroupCondition irQualifyingCustomProductCatalogsCondition)
	{
		final String productRaoVariable = context.generateVariable(ProductRAO.class);

		if (verifyAllPresent(excludedCustomProductCatalogsParameter, excludedCustomProductCatalogsParameter)
				&& CollectionUtils.isNotEmpty((Collection) excludedCustomProductCatalogsParameter.getValue()))
		{
			final RuleIrLocalVariablesContainer variablesContainer = context.createLocalContainer();
			final String excludedCustomProductCatalogRaoVariable = context.generateLocalVariable(variablesContainer,
					CustomProductCatalogRAO.class);
			final RuleIrAttributeCondition irExcludedCustomProductCatalogCondition = RuleIrAttributeConditionBuilder
					.newAttributeConditionFor(excludedCustomProductCatalogRaoVariable).withAttribute("code")
					.withOperator(RuleIrAttributeOperator.IN).withValue(excludedCustomProductCatalogsParameter.getValue()).build();
			final RuleIrAttributeRelCondition irExcludedProductCustomProductCatalogRel = RuleIrAttributeRelConditionBuilder
					.newAttributeRelationConditionFor(productRaoVariable).withAttribute(CUSTOM_PRODUCT_CATALOGS)
					.withOperator(RuleIrAttributeOperator.CONTAINS).withTargetVariable(excludedCustomProductCatalogRaoVariable)
					.build();
			final RuleIrNotCondition irExcludedCustomProductCatalogsCondition = new RuleIrNotCondition();
			irExcludedCustomProductCatalogsCondition.setVariablesContainer(variablesContainer);
			irExcludedCustomProductCatalogsCondition.setChildren(
					(List) Arrays.asList(irExcludedCustomProductCatalogCondition, irExcludedProductCustomProductCatalogRel));
			irQualifyingCustomProductCatalogsCondition.getChildren().add(irExcludedCustomProductCatalogsCondition);
		}

		if ((excludedProductsParameter != null) && CollectionUtils.isNotEmpty((Collection) excludedProductsParameter.getValue()))
		{
			final RuleIrGroupCondition baseProductNotOrGroupCondition = RuleIrGroupConditionBuilder
					.newGroupConditionOf(RuleIrGroupOperator.AND).build();
			final List<String> products = excludedProductsParameter.getValue();
			baseProductNotOrGroupCondition.getChildren()
					.add(RuleIrAttributeConditionBuilder.newAttributeConditionFor(productRaoVariable).withAttribute("code")
							.withOperator(RuleIrAttributeOperator.NOT_IN).withValue(products).build());

			for (final String product : products)
			{
				baseProductNotOrGroupCondition.getChildren()
						.add(RuleIrAttributeConditionBuilder.newAttributeConditionFor(productRaoVariable)
								.withAttribute("baseProductCodes").withOperator(RuleIrAttributeOperator.NOT_CONTAINS).withValue(product)
								.build());
				irQualifyingCustomProductCatalogsCondition.getChildren().add(baseProductNotOrGroupCondition);
			}
		}
	}

	protected void addContainsAllCustomProductCatalogsConditions(final RuleCompilerContext context,
			final List<String> customProductCatalogs, final RuleIrGroupCondition irQualifyingCustomProductCatalogsCondition)
	{
		final String productRaoVariable = context.generateVariable(ProductRAO.class);

		for (final String customProductCatalog : customProductCatalogs)
		{
			final RuleIrLocalVariablesContainer variablesContainer = context.createLocalContainer();
			final String containsCustomProductCatalogRaoVariable = context.generateLocalVariable(variablesContainer,
					CustomProductCatalogRAO.class);
			final RuleIrAttributeCondition irContainsCustomProductCatalogCondition = RuleIrAttributeConditionBuilder
					.newAttributeConditionFor(containsCustomProductCatalogRaoVariable).withAttribute("code")
					.withOperator(RuleIrAttributeOperator.EQUAL).withValue(customProductCatalog).build();
			final RuleIrAttributeRelCondition irContainsProductCustomProductCatalogRel = RuleIrAttributeRelConditionBuilder
					.newAttributeRelationConditionFor(productRaoVariable).withAttribute(CUSTOM_PRODUCT_CATALOGS)
					.withOperator(RuleIrAttributeOperator.CONTAINS).withTargetVariable(containsCustomProductCatalogRaoVariable)
					.build();
			final RuleIrExistsCondition irExistsCustomProductCatalogCondition = new RuleIrExistsCondition();
			irExistsCustomProductCatalogCondition.setVariablesContainer(variablesContainer);
			irExistsCustomProductCatalogCondition.setChildren(
					(List) Arrays.asList(irContainsCustomProductCatalogCondition, irContainsProductCustomProductCatalogRel));
			irQualifyingCustomProductCatalogsCondition.getChildren().add(irExistsCustomProductCatalogCondition);
		}
	}

	protected void evaluateCustomProductCatalogsOperator(final RuleCompilerContext context,
			final CollectionOperator customProductCatalogsOperator, final List<String> customProductCatalogs,
			final RuleIrGroupCondition irQualifyingCustomProductCatalogsCondition, final List<RuleIrCondition> irConditions)
	{
		if (CollectionOperator.NOT_CONTAINS.equals(customProductCatalogsOperator))
		{
			final RuleIrNotCondition irNotProductCondition = new RuleIrNotCondition();
			irNotProductCondition.setChildren(irConditions);
			irQualifyingCustomProductCatalogsCondition.getChildren().add(irNotProductCondition);
		}
		else
		{
			irQualifyingCustomProductCatalogsCondition.getChildren().addAll(irConditions);

			if (CollectionOperator.CONTAINS_ALL.equals(customProductCatalogsOperator))
			{
				addContainsAllCustomProductCatalogsConditions(context, customProductCatalogs,
						irQualifyingCustomProductCatalogsCondition);
			}
		}
	}

	protected void addQualifyingCustomProductCatalogsConditions(final RuleCompilerContext context, final AmountOperator operator,
			final Integer quantity, final CollectionOperator customProductCatalogsOperator, final List<String> customProductCatalogs,
			final RuleIrGroupCondition irQualifyingCustomProductCatalogsCondition)
	{
		final String customProductCatalogRaoVariable = context.generateVariable(CustomProductCatalogRAO.class);
		final String productRaoVariable = context.generateVariable(ProductRAO.class);
		final String orderEntryRaoVariable = context.generateVariable(OrderEntryRAO.class);
		final String cartRaoVariable = context.generateVariable(CartRAO.class);
		final List<RuleIrCondition> irConditions = new ArrayList<>();
		irConditions.add(RuleIrAttributeConditionBuilder.newAttributeConditionFor(customProductCatalogRaoVariable)
				.withAttribute("code").withOperator(RuleIrAttributeOperator.IN).withValue(customProductCatalogs).build());
		irConditions.add(RuleIrAttributeRelConditionBuilder.newAttributeRelationConditionFor(productRaoVariable)
				.withAttribute(CUSTOM_PRODUCT_CATALOGS).withOperator(RuleIrAttributeOperator.CONTAINS)
				.withTargetVariable(customProductCatalogRaoVariable).build());
		irConditions.add(RuleIrAttributeRelConditionBuilder.newAttributeRelationConditionFor(orderEntryRaoVariable)
				.withAttribute("product").withOperator(RuleIrAttributeOperator.EQUAL).withTargetVariable(productRaoVariable).build());
		irConditions.add(RuleIrAttributeConditionBuilder.newAttributeConditionFor(orderEntryRaoVariable).withAttribute("quantity")
				.withOperator(RuleIrAttributeOperator.valueOf(operator.name())).withValue(quantity).build());
		irConditions
				.add(RuleIrAttributeRelConditionBuilder.newAttributeRelationConditionFor(cartRaoVariable).withAttribute("entries")
						.withOperator(RuleIrAttributeOperator.CONTAINS).withTargetVariable(orderEntryRaoVariable).build());
		irConditions.addAll(createProductConsumedCondition(context, orderEntryRaoVariable));
		evaluateCustomProductCatalogsOperator(context, customProductCatalogsOperator, customProductCatalogs,
				irQualifyingCustomProductCatalogsCondition, irConditions);
	}

	@Override
	public RuleIrCondition translate(final RuleCompilerContext context, final RuleConditionData condition,
			final RuleConditionDefinitionData conditionDefinition)
	{
		final Map<String, RuleParameterData> conditionParameters = condition.getParameters();
		final RuleParameterData operatorParameter = conditionParameters.get("operator");
		final RuleParameterData quantityParameter = conditionParameters.get("quantity");
		final RuleParameterData customProductCatalogsOperatorParameter = conditionParameters.get("customproductcatalogs_operator");
		final RuleParameterData customProductCatalogsParameter = conditionParameters.get("customproductcatalogs");
		final RuleParameterData excludedCustomProductCatalogsParameter = conditionParameters.get("excluded_customproductcatalogs");
		final RuleParameterData excludedProductsParameter = conditionParameters.get("excluded_products");

		if (verifyAllPresent(operatorParameter, quantityParameter, customProductCatalogsOperatorParameter,
				customProductCatalogsParameter))
		{
			final AmountOperator operator = operatorParameter.getValue();
			final Integer quantity = quantityParameter.getValue();
			final CollectionOperator customProductCatalogsOperator = customProductCatalogsOperatorParameter.getValue();
			final List<String> customProductCatalogs = customProductCatalogsParameter.getValue();

			if (verifyAllPresent(operator, quantity, customProductCatalogsOperator, customProductCatalogs))
			{
				final RuleIrGroupCondition irQualifyingCustomProductCatalogsCondition = RuleIrGroupConditionBuilder
						.newGroupConditionOf(RuleIrGroupOperator.AND).build();
				addQualifyingCustomProductCatalogsConditions(context, operator, quantity, customProductCatalogsOperator,
						customProductCatalogs, irQualifyingCustomProductCatalogsCondition);

				if (!CollectionOperator.NOT_CONTAINS.equals(customProductCatalogsOperator))
				{
					addExcludedProductsAndCustomProductCatalogConditions(context, excludedCustomProductCatalogsParameter,
							excludedProductsParameter, irQualifyingCustomProductCatalogsCondition);
				}

				return irQualifyingCustomProductCatalogsCondition;
			}
		}

		return IrConditions.newIrRuleFalseCondition();
	}

}
