package com.capgemini.customproductcatalog.ruledefinitions.conditions;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.customproductcatalog.ruleengineservices.rao.CustomProductCatalogRAO;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ruledefinitions.AmountOperator;
import de.hybris.platform.ruledefinitions.CollectionOperator;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerContext;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeOperator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeRelCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrFalseCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrLocalVariablesContainer;
import de.hybris.platform.ruleengineservices.compiler.RuleIrNotCondition;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.ruleengineservices.rao.ProductRAO;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionDefinitionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleParameterData;

@UnitTest
public class RuleQualifyingCustomProductCatalogsConditionTranslatorTest
{

    private static final String OPERATOR_PARAM = "operator";
    private static final String QUANTITY_PARAM = "quantity";
    private static final String EXCLUDED_CUSTOM_PRODUCT_CATALOGS_PARAM = "excluded_customproductcatalogs";
    private static final String EXCLUDED_PRODUCTS_PARAM = "excluded_products";
    private static final String CUSTOM_PRODUCT_CATALOGS_OPERATOR = "customproductcatalogs_operator";
    private static final String CUSTOM_PRODUCT_CATALOGS = "customproductcatalogs";
    private static final String CUSTOM_PRODUCT_CATALOG_RAO_VAR = "customProductCatalogRaoVariable";
    private static final String AVAILABLE_QUANTITY_VAR = "availableQuantity";
    private static final String ORDER_ENTRY_RAO_VAR = "orderEntryRaoVariable";
    private static final String CART_RAO_VAR = "cartRaoVariable";
    private static final String PRODUCT_RAO_VAR = "productRaoVariable";
    private static final String CART_RAO_ENTRIES_ATTRIBUTE = "entries";
    private static final String ORDER_ENTRY_RAO_PRODUCT_ATTRIBUTE = "product";
    private static final String PRODUCT_RAO_CUSTOM_PRODUCT_CATALOGS_ATTRIBUTE = "customProductCatalogs";
    private static final String CUSTOM_PRODUCT_CATALOG_RAO_CODE_ATTRIBUTE = "code";
    private static final String PRODUCT_CODE_ATTRIBUTE = "code";
    private static final String PRODUCT_BASE_PRODUCT_CODES_ATTRIBUTE = "baseProductCodes";
    
    private RuleQualifyingCustomProductCatalogsConditionTranslator translator;
    
    @Mock
    private RuleCompilerContext context;

    @Mock
    private RuleConditionData condition;

    @Mock
    private RuleConditionDefinitionData conditionDefinition;

    @Mock
    private Map<String, RuleParameterData> parameters;

    @Mock
    private RuleIrLocalVariablesContainer variablesContainer;
    
    @Mock
    private RuleParameterData operatorParameter;
    
    @Mock
    private RuleParameterData quantityParameter;
    
    @Mock
    private RuleParameterData customProductCatalogsOperatorParameter;
    
    @Mock
    private RuleParameterData customProductCatalogsParameter;
    
    @Mock
    RuleParameterData excludedCustomProductCatalogsParameter;

    @Mock
    RuleParameterData excludedProductsParameter;
    
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        translator = new RuleQualifyingCustomProductCatalogsConditionTranslator();
        Mockito.when(condition.getParameters()).thenReturn(parameters);
        Mockito.when(parameters.get(OPERATOR_PARAM)).thenReturn(operatorParameter);
        Mockito.when(parameters.get(QUANTITY_PARAM)).thenReturn(quantityParameter);
        Mockito.when(parameters.get(CUSTOM_PRODUCT_CATALOGS_OPERATOR)).thenReturn(customProductCatalogsOperatorParameter);
        Mockito.when(parameters.get(CUSTOM_PRODUCT_CATALOGS)).thenReturn(customProductCatalogsParameter);
        Mockito.when(operatorParameter.getValue()).thenReturn(AmountOperator.GREATER_THAN);
        Mockito.when(quantityParameter.getValue()).thenReturn(Integer.valueOf(1));
        Mockito.when(context.createLocalContainer()).thenReturn(variablesContainer);
        Mockito.when(context.generateLocalVariable(variablesContainer, CustomProductCatalogRAO.class)).thenReturn(CUSTOM_PRODUCT_CATALOG_RAO_VAR);
        Mockito.when(context.generateVariable(OrderEntryRAO.class)).thenReturn(ORDER_ENTRY_RAO_VAR);
        Mockito.when(context.generateVariable(CartRAO.class)).thenReturn(CART_RAO_VAR);
        Mockito.when(context.generateVariable(ProductRAO.class)).thenReturn(PRODUCT_RAO_VAR);
        Mockito.when(context.generateVariable(CustomProductCatalogRAO.class)).thenReturn(CUSTOM_PRODUCT_CATALOG_RAO_VAR);
        Mockito.when(customProductCatalogsParameter.getValue()).thenReturn(Arrays.asList("code1", "code2"));
        
    }
    
    @Test
    public void testTranslateOperatorParamNull()
    {
        Mockito.when(parameters.get(OPERATOR_PARAM)).thenReturn(null);
        final RuleIrCondition ruleIrCondition = translator.translate(context, condition, conditionDefinition);
        Assert.assertTrue(ruleIrCondition instanceof RuleIrFalseCondition);
    }
    
    @Test
    public void testTranslateCategoriesOperatorParamNull()
    {
        Mockito.when(parameters.get(CUSTOM_PRODUCT_CATALOGS_OPERATOR)).thenReturn(null);
        final RuleIrCondition ruleIrCondition = translator.translate(context, condition, conditionDefinition);
        Assert.assertTrue(ruleIrCondition instanceof RuleIrFalseCondition);
    }

    @Test
    public void testTranslateCategoriesParamNull()
    {
        Mockito.when(parameters.get(CUSTOM_PRODUCT_CATALOGS)).thenReturn(null);
        final RuleIrCondition ruleIrCondition = translator.translate(context, condition, conditionDefinition);
        Assert.assertTrue(ruleIrCondition instanceof RuleIrFalseCondition);
    }
    
    @Test
    public void testTranslateNotOperatorCondition()
    {
        Mockito.when(customProductCatalogsOperatorParameter.getValue()).thenReturn(CollectionOperator.NOT_CONTAINS);
        final RuleIrCondition ruleIrCondition = translator.translate(context, condition, conditionDefinition);
        Assert.assertTrue(ruleIrCondition instanceof RuleIrGroupCondition);
        final RuleIrGroupCondition irGroupCondition = (RuleIrGroupCondition) ruleIrCondition;
        final List<RuleIrCondition> childCondition = irGroupCondition.getChildren();
        Assert.assertTrue(childCondition.get(0) instanceof RuleIrNotCondition);
        final RuleIrNotCondition irNotCondition = (RuleIrNotCondition) childCondition.get(0);
        Assert.assertTrue(irNotCondition.getChildren().size() == 7);
        checkChildConditions(irNotCondition.getChildren());
    }

    @Test
    public void testTranslateAnyOperatorCondition()
    {
        Mockito.when(customProductCatalogsOperatorParameter.getValue()).thenReturn(CollectionOperator.CONTAINS_ANY);
        final RuleIrCondition ruleIrCondition = translator.translate(context, condition, conditionDefinition);
        Assert.assertTrue(ruleIrCondition instanceof RuleIrGroupCondition);
        final RuleIrGroupCondition irGroupCondition = (RuleIrGroupCondition) ruleIrCondition;
        Assert.assertTrue(irGroupCondition.getChildren().size() == 7);
        checkChildConditions(irGroupCondition.getChildren());
    }
    
    @Test
    public void testTranslateAnyOperatorWithExcludeCustomProductCatalogCondition()
    {
        Mockito.when(customProductCatalogsOperatorParameter.getValue()).thenReturn(CollectionOperator.CONTAINS_ANY);
        Mockito.when(parameters.get(EXCLUDED_CUSTOM_PRODUCT_CATALOGS_PARAM)).thenReturn(excludedCustomProductCatalogsParameter);
        Mockito.when(excludedCustomProductCatalogsParameter.getValue()).thenReturn(Arrays.asList("code3"));

        final RuleIrCondition ruleIrCondition = translator.translate(context, condition, conditionDefinition);
        Assert.assertTrue(ruleIrCondition instanceof RuleIrGroupCondition);
        final RuleIrGroupCondition irGroupCondition = (RuleIrGroupCondition) ruleIrCondition;
        Assert.assertTrue(irGroupCondition.getChildren().size() == 8);
        checkChildConditions(irGroupCondition.getChildren());
        checkExcludeCustomProductCatalogCondition(irGroupCondition.getChildren().get(7));
    }

    @Test
    public void testTranslateAnyOperatorWithExcludeProductCondition()
    {
        Mockito.when(customProductCatalogsOperatorParameter.getValue()).thenReturn(CollectionOperator.CONTAINS_ANY);
        Mockito.when(parameters.get(EXCLUDED_PRODUCTS_PARAM)).thenReturn(excludedProductsParameter);
        Mockito.when(excludedProductsParameter.getValue()).thenReturn(Arrays.asList("code4"));

        final RuleIrCondition ruleIrCondition = translator.translate(context, condition, conditionDefinition);
        Assert.assertTrue(ruleIrCondition instanceof RuleIrGroupCondition);
        final RuleIrGroupCondition irGroupCondition = (RuleIrGroupCondition) ruleIrCondition;
        Assert.assertTrue(irGroupCondition.getChildren().size() == 8);
        checkChildConditions(irGroupCondition.getChildren());
        checkExcludeProductCondition(irGroupCondition.getChildren().get(7));
    }
    
    private void checkChildConditions(final List<RuleIrCondition> ruleIrConditions)
    {
        Assert.assertTrue(ruleIrConditions.get(0) instanceof RuleIrAttributeCondition);
        final RuleIrAttributeCondition ruleIrAttributeCondition = (RuleIrAttributeCondition) ruleIrConditions.get(0);
        Assert.assertTrue(RuleIrAttributeOperator.IN.equals(ruleIrAttributeCondition.getOperator()));
        final List<String> customProductCatalogs = (List<String>) ruleIrAttributeCondition.getValue();
        Assert.assertTrue(customProductCatalogs.contains("code1"));
        Assert.assertTrue(customProductCatalogs.contains("code2"));

        Assert.assertTrue(ruleIrConditions.get(1) instanceof RuleIrAttributeRelCondition);
        final RuleIrAttributeRelCondition ruleIrAttributeRelCondition = (RuleIrAttributeRelCondition) ruleIrConditions.get(1);
        Assert.assertTrue(RuleIrAttributeOperator.CONTAINS.equals(ruleIrAttributeRelCondition.getOperator()));
        Assert.assertTrue(PRODUCT_RAO_VAR.equals(ruleIrAttributeRelCondition.getVariable()));
        Assert.assertTrue(PRODUCT_RAO_CUSTOM_PRODUCT_CATALOGS_ATTRIBUTE.equals(ruleIrAttributeRelCondition.getAttribute()));
        Assert.assertTrue(CUSTOM_PRODUCT_CATALOG_RAO_VAR.equals(ruleIrAttributeRelCondition.getTargetVariable()));

        Assert.assertTrue(ruleIrConditions.get(2) instanceof RuleIrAttributeRelCondition);
        final RuleIrAttributeRelCondition ruleIrAttributeRelConditionOrderEntry = (RuleIrAttributeRelCondition) ruleIrConditions
                .get(2);
        Assert.assertTrue(RuleIrAttributeOperator.EQUAL.equals(ruleIrAttributeRelConditionOrderEntry.getOperator()));
        Assert.assertTrue(ORDER_ENTRY_RAO_VAR.equals(ruleIrAttributeRelConditionOrderEntry.getVariable()));
        Assert.assertTrue(ORDER_ENTRY_RAO_PRODUCT_ATTRIBUTE.equals(ruleIrAttributeRelConditionOrderEntry.getAttribute()));
        Assert.assertTrue(PRODUCT_RAO_VAR.equals(ruleIrAttributeRelConditionOrderEntry.getTargetVariable()));

        Assert.assertTrue(ruleIrConditions.get(3) instanceof RuleIrAttributeCondition);
        final RuleIrAttributeCondition ruleIrAttributeOrderEntryQuantityCondition = (RuleIrAttributeCondition) ruleIrConditions
                .get(3);
        Assert.assertTrue(RuleIrAttributeOperator.GREATER_THAN.equals(ruleIrAttributeOrderEntryQuantityCondition.getOperator()));
        Assert.assertTrue(Integer.valueOf(1).equals(ruleIrAttributeOrderEntryQuantityCondition.getValue()));

        Assert.assertTrue(ruleIrConditions.get(4) instanceof RuleIrAttributeRelCondition);
        final RuleIrAttributeRelCondition ruleIrCartOrderEntryRelCondition = (RuleIrAttributeRelCondition) ruleIrConditions.get(4);
        Assert.assertTrue(RuleIrAttributeOperator.CONTAINS.equals(ruleIrCartOrderEntryRelCondition.getOperator()));
        Assert.assertTrue(CART_RAO_VAR.equals(ruleIrCartOrderEntryRelCondition.getVariable()));
        Assert.assertTrue(ORDER_ENTRY_RAO_VAR.equals(ruleIrCartOrderEntryRelCondition.getTargetVariable()));
        Assert.assertTrue(CART_RAO_ENTRIES_ATTRIBUTE.equals(ruleIrCartOrderEntryRelCondition.getAttribute()));

        Assert.assertTrue(ruleIrConditions.get(5) instanceof RuleIrAttributeRelCondition);
        final RuleIrAttributeRelCondition ruleIrCartConsumedCondition = (RuleIrAttributeRelCondition) ruleIrConditions.get(5);
        Assert.assertTrue(RuleIrAttributeOperator.EQUAL.equals(ruleIrCartConsumedCondition.getOperator()));
        Assert.assertTrue(ORDER_ENTRY_RAO_VAR.equals(ruleIrCartConsumedCondition.getTargetVariable()));

        Assert.assertTrue(ruleIrConditions.get(6) instanceof RuleIrAttributeCondition);
        final RuleIrAttributeCondition ruleIrAvailableQtyCondition = (RuleIrAttributeCondition) ruleIrConditions.get(6);
        Assert.assertTrue(RuleIrAttributeOperator.GREATER_THAN_OR_EQUAL.equals(ruleIrAvailableQtyCondition.getOperator()));
        Assert.assertTrue(AVAILABLE_QUANTITY_VAR.equals(ruleIrAvailableQtyCondition.getAttribute()));
    }
    
    private void checkExcludeCustomProductCatalogCondition(final RuleIrCondition ruleIrCondition)
    {
        Assert.assertTrue(ruleIrCondition instanceof RuleIrNotCondition);
        final RuleIrNotCondition excludeCondition = (RuleIrNotCondition) ruleIrCondition;
        Assert.assertTrue(excludeCondition.getChildren().size() == 2);

        final RuleIrAttributeCondition attributeCondition = (RuleIrAttributeCondition) excludeCondition.getChildren().get(0);
        Assert.assertTrue(RuleIrAttributeOperator.IN.equals(attributeCondition.getOperator()));
        Assert.assertTrue(CUSTOM_PRODUCT_CATALOG_RAO_VAR.equals(attributeCondition.getVariable()));
        Assert.assertTrue(CUSTOM_PRODUCT_CATALOG_RAO_CODE_ATTRIBUTE.equals(attributeCondition.getAttribute()));
        final List<String> excludedCustomProductCatalogs= (List<String>) attributeCondition.getValue();
        Assert.assertTrue(excludedCustomProductCatalogs.contains("code3"));

        final RuleIrAttributeRelCondition attributeRelCondition = (RuleIrAttributeRelCondition) excludeCondition.getChildren()
                .get(1);
        Assert.assertTrue(RuleIrAttributeOperator.CONTAINS.equals(attributeRelCondition.getOperator()));
        Assert.assertTrue(PRODUCT_RAO_VAR.equals(attributeRelCondition.getVariable()));
        Assert.assertTrue(PRODUCT_RAO_CUSTOM_PRODUCT_CATALOGS_ATTRIBUTE.equals(attributeRelCondition.getAttribute()));
        Assert.assertTrue(CUSTOM_PRODUCT_CATALOG_RAO_VAR.equals(attributeRelCondition.getTargetVariable()));
    }
    
    private void checkExcludeProductCondition(final RuleIrCondition ruleIrCondition)
    {
        Assert.assertTrue(ruleIrCondition instanceof RuleIrGroupCondition);
        final RuleIrGroupCondition excludeCondition = (RuleIrGroupCondition) ruleIrCondition;
        Assert.assertTrue(excludeCondition.getChildren().size() == 2);

        RuleIrAttributeCondition attributeCondition = (RuleIrAttributeCondition) excludeCondition.getChildren().get(0);
        Assert.assertTrue(RuleIrAttributeOperator.NOT_IN.equals(attributeCondition.getOperator()));
        Assert.assertTrue(PRODUCT_RAO_VAR.equals(attributeCondition.getVariable()));
        Assert.assertTrue(PRODUCT_CODE_ATTRIBUTE.equals(attributeCondition.getAttribute()));
        List<String> excludedProducts = (List<String>) attributeCondition.getValue();
        Assert.assertTrue(excludedProducts.contains("code4"));
        
        attributeCondition = (RuleIrAttributeCondition) excludeCondition.getChildren().get(1);
        Assert.assertTrue(RuleIrAttributeOperator.NOT_CONTAINS.equals(attributeCondition.getOperator()));
        Assert.assertTrue(PRODUCT_RAO_VAR.equals(attributeCondition.getVariable()));
        Assert.assertTrue(PRODUCT_BASE_PRODUCT_CODES_ATTRIBUTE.equals(attributeCondition.getAttribute()));
        String excludedProduct = (String) attributeCondition.getValue();
        Assert.assertTrue(("code4").equals(excludedProduct));
    }
    
    
}
