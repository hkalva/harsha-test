package com.lyonscg.lyonscgcistaxaddon.order.impl;

import static org.mockito.BDDMockito.given;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.platform.commerceservices.externaltax.ExternalTaxesService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.strategies.calculation.OrderRequiresCalculationStrategy;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.TaxValue;


/**
 * Unit tests for {@link LeaTaxCalculationService}.
 */
public class LeaTaxCalculationServiceTest
{
    @InjectMocks
    private LeaTaxCalculationService testClass;
    @Mock
    private AbstractOrderModel order;
    @Mock
    private AbstractOrderEntryModel entry;
    @Mock
    private ExternalTaxesService externalTaxesService;
    @Mock
    private OrderRequiresCalculationStrategy orderRequiresCalculationStrategy;
    @Mock
    private CommonI18NService commonI18NService;
    @Mock
    private ModelService modelService;
    @Mock
    private CurrencyModel usdCurrency;
    private String usdSymbol = "USD";
    private String entryTaxValueString = "<TV<STATE#10.00#true#10.00#" + usdSymbol + ">VT>";
    private String shippingTaxValueString = "<TV<STATE#1.00#true#1.00#" + usdSymbol + ">VT>";
    private TaxValue entryTaxValue, shippingTaxValue;
    private Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap;
    private int quantity = 1, digits = 2;
    private double lineItemsTax = 10, shippingTax = 1, totalTax = lineItemsTax + shippingTax;
    private final double entryPrice = 50;

    /** Setup data for tests. */
    @Before
    public void setUp()
    {
        testClass = new LeaTaxCalculationService();
        entryTaxValue = TaxValue.parseTaxValue(entryTaxValueString);
        shippingTaxValue = TaxValue.parseTaxValue(shippingTaxValueString);
        taxValueMap = new HashMap<>();
        Map<Set<TaxValue>, Double> taxGroup = new HashMap<>();
        taxValueMap.put(entryTaxValue.unapply(), taxGroup);
        taxGroup.put(Collections.singleton(entryTaxValue.unapply()), new Double(quantity));
        MockitoAnnotations.initMocks(this);
        given(order.getEntries()).willReturn(Collections.singletonList(entry));

        given(entry.getTaxValues()).willReturn(Collections.singletonList(entryTaxValue));
        given(entry.getOrder()).willReturn(order);
        given(entry.getBasePrice()).willReturn(entryPrice);
        given(entry.getQuantity()).willReturn(new Long(quantity).longValue());
        given(entry.getTotalPrice()).willReturn(entryPrice);
        given(order.getDeliveryCostTaxValues()).willReturn(Collections.singletonList(shippingTaxValue));
        given(order.getDeliveryCostTax()).willReturn(shippingTax);

        given(order.getCurrency()).willReturn(usdCurrency);
        given(usdCurrency.getIsocode()).willReturn(usdSymbol);
        given(usdCurrency.getDigits()).willReturn(digits);

        given(commonI18NService.roundCurrency(entryPrice, digits)).willReturn(entryPrice);

    }

    /**
     * Unit test for {@link LeaTaxCalculationService#addDeliveryTaxValues(AbstractOrderModel, boolean, double)} .
     */
    @Test
    public void addDeliveryTaxValuesRecalculationTest()
    {
        boolean recalculate = true;
        given(orderRequiresCalculationStrategy.requiresCalculation(order)).willReturn(true);
        given(order.getTotalTaxValues()).willReturn(Collections.singletonList(entryTaxValue));
        Assert.assertEquals(totalTax, testClass.addDeliveryTaxValues(order, recalculate, lineItemsTax), 0);
        Mockito.verify(order).setTotalTaxValues(Arrays.asList(entryTaxValue, shippingTaxValue));
    }

    /**
     * Unit test for {@link LeaTaxCalculationService#addDeliveryTaxValues(AbstractOrderModel, boolean, double)} with no
     * recalculation .
     */
    @Test
    public void addDeliveryTaxValuesNoRecalculationTest()
    {
        boolean recalculate = false;
        given(orderRequiresCalculationStrategy.requiresCalculation(order)).willReturn(false);
        Assert.assertEquals(lineItemsTax, testClass.addDeliveryTaxValues(order, recalculate, lineItemsTax), 0);
        Mockito.verify(order, Mockito.times(0)).setTotalTaxValues(Mockito.any());
    }

    /**
     * Unit test for {@link LeaTaxCalculationService#calculateExternalTax(AbstractOrderModel, boolean, Map)}.
     */
    @Test
    public void calculateExternalTaxTest()
    {
        String unappliedLineItemTax = "[<TV<STATE#10.0#true#0.0#USD>VT>]";
        boolean recalculate = true;
        given(orderRequiresCalculationStrategy.requiresCalculation(order)).willReturn(false);
        given(externalTaxesService.calculateExternalTaxes(order)).willReturn(true);
        given(order.getTotalTaxValues()).willReturn(Arrays.asList(entryTaxValue, shippingTaxValue));
        Map<TaxValue, Map<Set<TaxValue>, Double>> taxMap = testClass.calculateExternalTax(order, recalculate, taxValueMap);
        Assert.assertEquals(unappliedLineItemTax, TaxValue.toString(taxMap.keySet()));
        Map<Set<TaxValue>, Double> taxGroup = taxMap.get(taxMap.keySet().iterator().next());
        Assert.assertEquals(unappliedLineItemTax, TaxValue.toString(taxGroup.keySet().iterator().next()));
        Assert.assertEquals(new Double(quantity), taxGroup.get(taxGroup.keySet().iterator().next()));
        Mockito.verify(order).setSubtotal(entryPrice);
    }

    /**
     * Unit test for {@link LeaTaxCalculationService#calculateExternalTax(AbstractOrderModel, boolean, Map)} with no
     * recalculation.
     */
    @Test
    public void calculateExternalTaxNoRecalculationTest()
    {
        boolean recalculate = false;
        given(orderRequiresCalculationStrategy.requiresCalculation(order)).willReturn(false);
        Assert.assertEquals(taxValueMap, testClass.calculateExternalTax(order, recalculate, taxValueMap));
    }

    /**
     * Unit test for {@link LeaTaxCalculationService#calculateExternalTax(AbstractOrderModel, boolean, Map)} with no
     * recalculation.
     */
    @Test
    public void calculateExternalTaxFailedExternalTaxTest()
    {
        boolean recalculate = true;
        given(orderRequiresCalculationStrategy.requiresCalculation(order)).willReturn(true);
        given(externalTaxesService.calculateExternalTaxes(order)).willReturn(false);
        Assert.assertEquals(taxValueMap, testClass.calculateExternalTax(order, recalculate, taxValueMap));
    }
}
