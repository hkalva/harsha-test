/**
 *
 */
package com.lyonscg.lyonscgcistaxaddon.order.converters.populator;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.lyonscg.lyonscgcistaxaddon.externaltax.TaxSource;


/**
 * Unit tests for {@link LeaCisTaxCalculationService}.
 *
 * @author lyonscg
 */
@UnitTest
public class LeaOrderTaxSourcePopulatorTest
{
	/** The class to be tested. */
	private LeaOrderTaxSourcePopulator leaOrderTaxSourcePopulator;
	/** TaxSource mapping. */
	private Map<String, TaxSource> taxSourceCodeToTaxSourceDataMapping;
	/** Mock AbstractOrderModel. */
	@Mock
	private AbstractOrderModel orderModel;
	/** AbstractOrderData instance */
	private AbstractOrderData orderData;

	/** Setup data for tests. */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		leaOrderTaxSourcePopulator = new LeaOrderTaxSourcePopulator();

		taxSourceCodeToTaxSourceDataMapping = new HashMap<String, TaxSource>();
		taxSourceCodeToTaxSourceDataMapping.put("external", TaxSource.EXTERNAL);
		taxSourceCodeToTaxSourceDataMapping.put("fallback", TaxSource.FALLBACK);
		taxSourceCodeToTaxSourceDataMapping.put("default", TaxSource.DEFAULT);
		leaOrderTaxSourcePopulator.setTaxSourceCodeToTaxSourceDataMapping(taxSourceCodeToTaxSourceDataMapping);
		orderData = new AbstractOrderData();

	}

	@Test
	public void testPopulate()
	{
		given(orderModel.getTaxSource()).willReturn(com.lyonscg.lyonscgcistaxaddon.enums.TaxSource.DEFAULT);
		leaOrderTaxSourcePopulator.populate(orderModel, orderData);
		Assert.assertTrue(TaxSource.DEFAULT.equals(orderData.getTaxSource()));

		given(orderModel.getTaxSource()).willReturn(com.lyonscg.lyonscgcistaxaddon.enums.TaxSource.FALLBACK);
		leaOrderTaxSourcePopulator.populate(orderModel, orderData);
		Assert.assertTrue(TaxSource.FALLBACK.equals(orderData.getTaxSource()));

		given(orderModel.getTaxSource()).willReturn(com.lyonscg.lyonscgcistaxaddon.enums.TaxSource.EXTERNAL);
		leaOrderTaxSourcePopulator.populate(orderModel, orderData);
		Assert.assertTrue(TaxSource.EXTERNAL.equals(orderData.getTaxSource()));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testErrorPopulate()
	{
		leaOrderTaxSourcePopulator.setTaxSourceCodeToTaxSourceDataMapping(new HashMap<String, TaxSource>());
		given(orderModel.getTaxSource()).willReturn(com.lyonscg.lyonscgcistaxaddon.enums.TaxSource.DEFAULT);
		leaOrderTaxSourcePopulator.populate(orderModel, orderData);
	}
}
