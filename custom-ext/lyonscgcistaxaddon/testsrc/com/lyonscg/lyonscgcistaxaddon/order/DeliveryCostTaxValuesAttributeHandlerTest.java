package com.lyonscg.lyonscgcistaxaddon.order;

import static org.mockito.BDDMockito.given;

import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.util.TaxValue;


/**
 * Unit tests for {@link DeliveryCostTaxValuesAttributeHandler}.
 */
@UnitTest
public class DeliveryCostTaxValuesAttributeHandlerTest
{
    @InjectMocks
    private DeliveryCostTaxValuesAttributeHandler testClass;
    @Mock
    private AbstractOrderModel abstractOrderModel;
    private String taxValueString = "[<TV<STATE#10.62#true#0.0#USD>VT>]";

    /** Setup data for tests. */
    @Before
    public void setUp()
    {
        testClass = new DeliveryCostTaxValuesAttributeHandler();
        MockitoAnnotations.initMocks(this);
        given(abstractOrderModel.getDeliveryCostTaxInternal()).willReturn(taxValueString);
    }

    /**
     * Unit test for {@link DeliveryCostTaxValuesAttributeHandler#get(AbstractOrderModel)}.
     */
    @Test
    public void getTest()
    {
        Collection<TaxValue> taxValues = testClass.get(abstractOrderModel);
        Assert.assertTrue(taxValues.size() == 1);
        TaxValue taxValue = taxValues.iterator().next();
        Assert.assertEquals("USD", taxValue.getCurrencyIsoCode());
        Assert.assertTrue(0 == taxValue.getAppliedValue());
        Assert.assertTrue(10.62d == taxValue.getValue());
        Assert.assertTrue(taxValue.isAbsolute());
    }

    /**
     * Unit test for {@link DeliveryCostTaxValuesAttributeHandler#set(AbstractOrderModel, Collection)}.
     */
    @Test
    public void setTest()
    {
        Collection<TaxValue> taxValues = TaxValue.parseTaxValueCollection(taxValueString);
        testClass.set(abstractOrderModel, taxValues);
        Mockito.verify(abstractOrderModel).setDeliveryCostTaxInternal(taxValueString);
    }
}
