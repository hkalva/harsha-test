/**
 *
 */
package com.lyonscg.lyonscgcistaxaddon.actions.order;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.lyonscg.lyonscgcistaxaddon.enums.TaxSource;


/**
 * Unit tests for {@link CheckOrderTaxSourceAction}.
 *
 * @author lyonscg
 */
@UnitTest
public class CheckOrderTaxSourceActionTest
{
	/** The class to be tested. */
	private CheckOrderTaxSourceAction checkOrderTaxSourceAction;
	/** Approved source system for tax. */
	private TaxSource approvedTaxSource;
	/** On hold order status for orders with incomplete tax. */
	private OrderStatus holdOrderStatus;
	/** Mock OrderProcessModel. */
	@Mock
	private OrderProcessModel orderProcess;
	/** Mock OrderModel. */
	@Mock
	private OrderModel order;
	/** Mock ModelService. */
	@Mock
	private ModelService modelService;

	/** Setup data for tests. */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		checkOrderTaxSourceAction = new CheckOrderTaxSourceAction();
		checkOrderTaxSourceAction.setModelService(modelService);
		approvedTaxSource = TaxSource.EXTERNAL;
		checkOrderTaxSourceAction.setApprovedTaxSource(approvedTaxSource);
		holdOrderStatus = OrderStatus.TAX_HOLD;
		checkOrderTaxSourceAction.setHoldOrderStatus(holdOrderStatus);
		given(orderProcess.getOrder()).willReturn(order);
		Mockito.doNothing().when(modelService).save(order);
	}

	/**
	 * OK action test for {@link CheckOrderTaxSourceAction#execute(OrderProcessModel)}.
	 *
	 * @throws Exception
	 * @throws RetryLaterException
	 */
	@Test
	public void testExecuteOKAction() throws RetryLaterException, Exception
	{
		given(order.getTaxSource()).willReturn(approvedTaxSource);
		Assert.assertTrue("OK".equals(checkOrderTaxSourceAction.execute(orderProcess)));
	}

	/**
	 * WAIT action test for {@link CheckOrderTaxSourceAction#execute(OrderProcessModel)}.
	 *
	 * @throws Exception
	 * @throws RetryLaterException
	 */
	@Test
	public void testExecuteWAITAction() throws RetryLaterException, Exception
	{
		given(order.getTaxSource()).willReturn(TaxSource.DEFAULT);
		Assert.assertTrue("WAIT".equals(checkOrderTaxSourceAction.execute(orderProcess)));
		Mockito.verify(order).setStatus(holdOrderStatus);
	}

	/**
	 * NOK action test for {@link CheckOrderTaxSourceAction#execute(OrderProcessModel)}.
	 *
	 * @throws Exception
	 * @throws RetryLaterException
	 */
	@Test
	public void testExecuteNOKAction() throws RetryLaterException, Exception
	{
		given(orderProcess.getOrder()).willReturn(null);
		Assert.assertTrue("NOK".equals(checkOrderTaxSourceAction.execute(orderProcess)));
	}
}
