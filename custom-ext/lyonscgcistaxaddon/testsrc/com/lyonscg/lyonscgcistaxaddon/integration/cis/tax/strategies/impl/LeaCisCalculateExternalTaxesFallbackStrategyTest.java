/**
 *
 */
package com.lyonscg.lyonscgcistaxaddon.integration.cis.tax.strategies.impl;

import static org.mockito.BDDMockito.given;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.lyonscg.lyonscgcistaxaddon.externaltax.LeaExternalTaxDocument;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.externaltax.ExternalTaxDocument;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.TaxValue;


/**
 * Unit tests for {@link LeaCisCalculateExternalTaxesFallbackStrategy}.
 *
 * @author lyonscg
 *
 */
@UnitTest
public class LeaCisCalculateExternalTaxesFallbackStrategyTest
{
    /** The class to be tested. */
    @Mock
    private LeaCisCalculateExternalTaxesFallbackStrategy leaCisCalculateExternalTaxesFallbackStrategy;
    /** Tax Document POJO. */
    private ExternalTaxDocument leaExternalTaxDocument;
    /** Mock AbstractOrderModel */
    @Mock
    private AbstractOrderModel abstractOrder;
    /** Mock BaseSiteModel */
    @Mock
    private BaseSiteModel baseSite;
    /** Mock AbstractOrderEntryModel */
    @Mock
    private AbstractOrderEntryModel entryModel;
    /** Mock SiteConfigService */
    @Mock
    private SiteConfigService siteConfigService;
    /** Mock CurrencyModel */
    @Mock
    private CurrencyModel currency;
    /** Mock BaseSiteService */
    @Mock
    private BaseSiteService baseSiteService;

    private Double taxPercent;
    private String taxPercentString;
    private final double itemPrice = 100;
    private final double shippingCost = 10;

    /** Setup data for tests. */
    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        taxPercentString = "10.0";
        taxPercent = Double.valueOf(taxPercentString);
        leaExternalTaxDocument = new LeaExternalTaxDocument();
        given(leaCisCalculateExternalTaxesFallbackStrategy.calculateExternalTaxes(abstractOrder)).willCallRealMethod();
        given(leaCisCalculateExternalTaxesFallbackStrategy.getTaxPercentage(abstractOrder)).willCallRealMethod();

        given(leaCisCalculateExternalTaxesFallbackStrategy.createExternalTaxDocument()).willReturn(leaExternalTaxDocument);
        given(leaCisCalculateExternalTaxesFallbackStrategy.getSiteConfigService()).willReturn(siteConfigService);
        given(leaCisCalculateExternalTaxesFallbackStrategy.getBaseSiteService()).willReturn(baseSiteService);
        given(siteConfigService.getString("cistax.taxCalculation.fallback.fixedpercentage", taxPercentString)).willReturn(
                taxPercentString);
        given(abstractOrder.getEntries()).willReturn(Collections.singletonList(entryModel));
        given(entryModel.getTotalPrice()).willReturn(Double.valueOf(itemPrice));
        given(entryModel.getEntryNumber()).willReturn(Integer.valueOf(0));
        given(abstractOrder.getDeliveryCost()).willReturn(Double.valueOf(shippingCost));
        given(abstractOrder.getCurrency()).willReturn(currency);
        given(currency.getIsocode()).willReturn("USD");
        given(baseSiteService.getCurrentBaseSite()).willReturn(baseSite);
    }

    /**
     * Unit test for {@link LeaCisCalculateExternalTaxesFallbackStrategy#calculateExternalTaxes(AbstractOrderModel)}.
     */
    @Test
    public void testCalculateExternalTaxes()
    {
        final ExternalTaxDocument externalTaxDocument = leaCisCalculateExternalTaxesFallbackStrategy
                .calculateExternalTaxes(abstractOrder);
        Assert.assertTrue(externalTaxDocument.equals(leaExternalTaxDocument));
        final List<TaxValue> taxValue = externalTaxDocument.getTaxesForOrderEntry(0);
        Assert.assertTrue(Double.doubleToLongBits(taxValue.get(0).getValue()) == Double.doubleToLongBits(itemPrice
                / taxPercent.doubleValue()));
        final List<TaxValue> shippingTaxValue = externalTaxDocument.getShippingCostTaxes();
        Assert.assertTrue(Double.doubleToLongBits(shippingTaxValue.get(0).getValue()) == Double.doubleToLongBits(shippingCost
                / taxPercent.doubleValue()));
    }

    /**
     * Unit test for {@link LeaCisCalculateExternalTaxesFallbackStrategy#createExternalTaxDocument()}.
     */
    @Test(expected = IllegalStateException.class)
    public void testCreateExternalTaxDocument()
    {
        new LeaCisCalculateExternalTaxesFallbackStrategy().createExternalTaxDocument();
    }

}
