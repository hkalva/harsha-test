/**
 *
 */
package com.lyonscg.lyonscgcistaxaddon.integration.cis.tax.service.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.externaltax.CalculateExternalTaxesStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.externaltax.ExternalTaxDocument;
import de.hybris.platform.integration.cis.tax.CisTaxDocOrder;
import de.hybris.platform.integration.commons.hystrix.HystrixExecutable;
import de.hybris.platform.integration.commons.hystrix.OndemandHystrixCommandConfiguration;
import de.hybris.platform.integration.commons.hystrix.OndemandHystrixCommandFactory;
import de.hybris.platform.integration.commons.hystrix.OndemandHystrixCommandFactory.OndemandHystrixCommand;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.tx.Transaction;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.hybris.cis.api.model.CisAddress;
import com.hybris.cis.api.model.CisOrder;
import com.hybris.cis.api.tax.model.CisTaxDoc;
import com.hybris.cis.client.rest.tax.TaxClient;
import com.hybris.commons.client.RestResponse;
import com.lyonscg.lyonscgcistaxaddon.externaltax.LeaExternalTaxDocument;
import com.lyonscg.lyonscgcistaxaddon.externaltax.TaxSource;


/**
 * Unit test for LeaCisTaxCalculationService.
 *
 * @author lyonscg
 */
@UnitTest
public class LeaCisTaxCalculationServiceTest
{
	/** The class to be tested */
	private LeaCisTaxCalculationService leaCisTaxCalculationService;

	/** Mock cisOrderConverter. */
	@Mock
	private Converter<AbstractOrderModel, CisOrder> cisOrderConverter;
	/** Mock TaxClient. */
	@Mock
	private TaxClient taxClient;
	/** Mock ConfigurationService. */
	@Mock
	private ConfigurationService configurationService;
	/** Mock externalTaxDocumentConverter. */
	@Mock
	private Converter<CisTaxDocOrder, ExternalTaxDocument> externalTaxDocumentConverter;
	/** Mock OndemandHystrixCommandConfiguration. */
	@Mock
	private OndemandHystrixCommandConfiguration hystrixCommandConfig;
	/** Mock CalculateExternalTaxesStrategy. */
	@Mock
	private CalculateExternalTaxesStrategy calculateExternalTaxesFallbackStrategy;
	/** Mock cisAddressConverter. */
	@Mock
	private Converter<AddressModel, CisAddress> cisAddressConverter;
	/** Mock SessionService. */
	@Mock
	private SessionService sessionService;
	/** Mock OndemandHystrixCommandFactory. */
	@Mock
	private OndemandHystrixCommandFactory ondemandHystrixCommandFactory;
	/** Mock AbstractOrderModel. */
	@Mock
	private AbstractOrderModel abstractOrder;
	/** Mock CartModel. */
	@Mock
	private CartModel cart;
	/** Mock CisOrder. */
	@Mock
	private CisOrder cisOrder;
	/** LeaExternalTaxDocument data. */
	private LeaExternalTaxDocument leaExternalTaxDocument;

	private Map<String, TaxSource> taxSourceCodeToTaxSourceDataMapping;
	/** Mock OndemandHystrixCommand. */
	@Mock
	private OndemandHystrixCommand<ExternalTaxDocument> ondemandHystrixCommand;
	/** Mock RestResponse<CisTaxDoc>. */
	@Mock
	private RestResponse<CisTaxDoc> cisTaxDocRestResponse;
	/** Mock Transaction. */
	@Mock
	private Transaction transaction;

	private String orderGuid;



	/** Setup data for tests */
	@Before
	public void setUp()
	{
		taxSourceCodeToTaxSourceDataMapping = new HashMap<String, TaxSource>();
		taxSourceCodeToTaxSourceDataMapping.put("default", TaxSource.DEFAULT);
		taxSourceCodeToTaxSourceDataMapping.put("external", TaxSource.EXTERNAL);
		taxSourceCodeToTaxSourceDataMapping.put("fallback", TaxSource.FALLBACK);
		leaExternalTaxDocument = new LeaExternalTaxDocument();
		orderGuid = "orderGuid";

		leaCisTaxCalculationService = new LeaCisTaxCalculationService();
		MockitoAnnotations.initMocks(this);
		leaCisTaxCalculationService.setCalculateExternalTaxesFallbackStrategy(calculateExternalTaxesFallbackStrategy);
		leaCisTaxCalculationService.setCisAddressConverter(cisAddressConverter);
		leaCisTaxCalculationService.setCisOrderConverter(cisOrderConverter);
		leaCisTaxCalculationService.setConfigurationService(configurationService);
		leaCisTaxCalculationService.setExternalTaxDocumentConverter(externalTaxDocumentConverter);
		leaCisTaxCalculationService.setHystrixCommandConfig(hystrixCommandConfig);
		leaCisTaxCalculationService.setOndemandHystrixCommandFactory(ondemandHystrixCommandFactory);
		leaCisTaxCalculationService.setSessionService(sessionService);
		leaCisTaxCalculationService.setTaxClient(taxClient);
		leaCisTaxCalculationService.setTaxSourceCodeToTaxSourceDataMapping(taxSourceCodeToTaxSourceDataMapping);


		given(
				ondemandHystrixCommandFactory.newCommand(Mockito.any(OndemandHystrixCommandConfiguration.class),
						(HystrixExecutable<ExternalTaxDocument>) Mockito.any())).willReturn(ondemandHystrixCommand);
		given(calculateExternalTaxesFallbackStrategy.calculateExternalTaxes(abstractOrder)).willReturn(leaExternalTaxDocument);
		given(calculateExternalTaxesFallbackStrategy.calculateExternalTaxes(cart)).willReturn(leaExternalTaxDocument);
		given(abstractOrder.getGuid()).willReturn(orderGuid);
		given(cart.getGuid()).willReturn(orderGuid);
		given(taxClient.quote(orderGuid, cisOrder)).willReturn(cisTaxDocRestResponse);
		given(taxClient.post(orderGuid, cisOrder)).willReturn(cisTaxDocRestResponse);
		given(externalTaxDocumentConverter.convert(Mockito.any(CisTaxDocOrder.class))).willReturn(leaExternalTaxDocument);
	}

	/**
	 * Unit test for {@link LeaCisTaxCalculationService#getExternalTaxDocument(AbstractOrderModel, CisOrder)}.
	 */
	@Test
	public void testGetExternalTaxDocument()
	{
		leaCisTaxCalculationService.getExternalTaxDocument(abstractOrder, cisOrder);
		Mockito.verify(ondemandHystrixCommand).execute();
	}

	/**
	 * Unit test for {@link LeaCisTaxCalculationService#callDefaultTaxService(AbstractOrderModel, Transaction)}.
	 */
	@Test
	public void testCallDefaultTaxService()
	{
		final LeaExternalTaxDocument actualExternalTaxDocument = (LeaExternalTaxDocument) leaCisTaxCalculationService
				.callDefaultTaxService(abstractOrder, transaction);
		Assert.assertTrue(TaxSource.DEFAULT.equals(actualExternalTaxDocument.getTaxSource()));
	}

	/**
	 * Unit test for {@link LeaCisTaxCalculationService#callFallbackTaxSource(AbstractOrderModel, Transaction)}.
	 */
	@Test
	public void testCallFallbackTaxSource()
	{
		final LeaExternalTaxDocument actualExternalTaxDocument = (LeaExternalTaxDocument) leaCisTaxCalculationService
				.callFallbackTaxSource(abstractOrder, transaction);
		Assert.assertTrue(TaxSource.FALLBACK.equals(actualExternalTaxDocument.getTaxSource()));
	}

	/**
	 * Unit test for {@link LeaCisTaxCalculationService#callExternalTaxSource(AbstractOrderModel, CisOrder, Transaction)}
	 * .
	 */
	@Test
	public void testCallExternalTaxSource()
	{
		LeaExternalTaxDocument actualExternalTaxDocument = (LeaExternalTaxDocument) leaCisTaxCalculationService
				.callExternalTaxSource(abstractOrder, cisOrder, transaction);
		Assert.assertTrue(TaxSource.EXTERNAL.equals(actualExternalTaxDocument.getTaxSource()));
		actualExternalTaxDocument = (LeaExternalTaxDocument) leaCisTaxCalculationService.callExternalTaxSource(cart, cisOrder,
				transaction);
		Assert.assertTrue(TaxSource.EXTERNAL.equals(actualExternalTaxDocument.getTaxSource()));
	}

	/**
	 * Unit test for
	 * {@link LeaCisTaxCalculationService#populateAdditionalTaxData(de.hybris.platform.externaltax.ExternalTaxDocument, TaxSource)}
	 * .
	 *
	 */
	@Test
	public void testPopulateAdditionalTaxData()
	{
		LeaExternalTaxDocument externalTaxDocument = new LeaExternalTaxDocument();
		leaCisTaxCalculationService.populateAdditionalTaxData(externalTaxDocument, TaxSource.EXTERNAL);
		Assert.assertTrue(TaxSource.EXTERNAL.equals(externalTaxDocument.getTaxSource()));

		externalTaxDocument = new LeaExternalTaxDocument();
		leaCisTaxCalculationService.populateAdditionalTaxData(externalTaxDocument, TaxSource.DEFAULT);
		Assert.assertTrue(TaxSource.DEFAULT.equals(externalTaxDocument.getTaxSource()));

		externalTaxDocument = new LeaExternalTaxDocument();
		leaCisTaxCalculationService.populateAdditionalTaxData(externalTaxDocument, TaxSource.FALLBACK);
		Assert.assertTrue(TaxSource.FALLBACK.equals(externalTaxDocument.getTaxSource()));
	}
}
