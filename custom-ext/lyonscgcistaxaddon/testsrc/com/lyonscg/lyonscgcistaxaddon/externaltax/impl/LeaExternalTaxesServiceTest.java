/**
 *
 */
package com.lyonscg.lyonscgcistaxaddon.externaltax.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.externaltax.CalculateExternalTaxesStrategy;
import de.hybris.platform.commerceservices.externaltax.DecideExternalTaxesStrategy;
import de.hybris.platform.commerceservices.externaltax.RecalculateExternalTaxesStrategy;
import de.hybris.platform.commerceservices.externaltax.impl.DefaultExternalTaxesService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.externaltax.ApplyExternalTaxesStrategy;
import de.hybris.platform.externaltax.ExternalTaxDocument;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.util.TaxValue;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.lyonscg.lyonscgcistaxaddon.externaltax.LeaExternalTaxDocument;
import com.lyonscg.lyonscgcistaxaddon.externaltax.TaxSource;



/**
 * Unit tests for {@link LeaExternalTaxesService}.
 *
 * @author lyonscg
 *
 */
@UnitTest
public class LeaExternalTaxesServiceTest
{
	/** LeaExternalTaxesService to be tested */
	private LeaExternalTaxesService leaExternalTaxesService;
	/** Mock DecideExternalTaxesStrategy. */
	@Mock
	private DecideExternalTaxesStrategy decideExternalTaxesStrategy;
	/** Mock RecalculateExternalTaxesStrategy. */
	@Mock
	private RecalculateExternalTaxesStrategy recalculateExternalTaxesStrategy;
	/** Mock CalculateExternalTaxesStrategy. */
	@Mock
	private CalculateExternalTaxesStrategy calculateExternalTaxesStrategy;
	/** Mock ApplyExternalTaxesStrategy. */
	@Mock
	private ApplyExternalTaxesStrategy applyExternalTaxesStrategy;
	/** Mock SessionService. */
	@Mock
	private SessionService sessionService;
	/** Tax source mapping. */
	private Map<TaxSource, String> taxSourceDataToTaxSourceCodeMapping;
	/** Mock AbstractOrderModel. */
	@Mock
	private AbstractOrderModel abstractOrder;

	private ExternalTaxDocument exTaxDocument;
	/** Mock ExternalTaxDocument. */
	@Mock
	private LeaExternalTaxDocument exTaxDocumentMock;
	/** Mock ModelService. */
	@Mock
	private ModelService modelService;
	/** Mock allTaxes. */
	@Mock
	private Map<Integer, List<TaxValue>> allTaxes;
	/** Mock shippingTaxes. */
	@Mock
	private List<TaxValue> shippingTaxes;

	/** Setup data for tests. */
	@Before
	public void setUp()
	{
		leaExternalTaxesService = new LeaExternalTaxesService();
		MockitoAnnotations.initMocks(this);

		exTaxDocument = new LeaExternalTaxDocument();

		taxSourceDataToTaxSourceCodeMapping = new HashMap<TaxSource, String>();
		taxSourceDataToTaxSourceCodeMapping.put(TaxSource.DEFAULT, "default");
		taxSourceDataToTaxSourceCodeMapping.put(TaxSource.EXTERNAL, "external");
		taxSourceDataToTaxSourceCodeMapping.put(TaxSource.FALLBACK, "fallback");


		leaExternalTaxesService.setDecideExternalTaxesStrategy(decideExternalTaxesStrategy);
		leaExternalTaxesService.setRecalculateExternalTaxesStrategy(recalculateExternalTaxesStrategy);
		leaExternalTaxesService.setCalculateExternalTaxesStrategy(calculateExternalTaxesStrategy);
		leaExternalTaxesService.setApplyExternalTaxesStrategy(applyExternalTaxesStrategy);
		leaExternalTaxesService.setSessionService(sessionService);
		leaExternalTaxesService.setTaxSourceDataToTaxSourceCodeMapping(taxSourceDataToTaxSourceCodeMapping);
		leaExternalTaxesService.setModelService(modelService);

		given(decideExternalTaxesStrategy.shouldCalculateExternalTaxes(abstractOrder)).willReturn(Boolean.TRUE);
		given(recalculateExternalTaxesStrategy.recalculate(abstractOrder)).willReturn(Boolean.TRUE);
		given(calculateExternalTaxesStrategy.calculateExternalTaxes(abstractOrder)).willReturn(exTaxDocumentMock);
		given(exTaxDocumentMock.getAllTaxes()).willReturn(allTaxes);
		given(exTaxDocumentMock.getShippingCostTaxes()).willReturn(shippingTaxes);
		given(sessionService.getAttribute(DefaultExternalTaxesService.SESSION_EXTERNAL_TAX_DOCUMENT)).willReturn(null);
		given(abstractOrder.getEntries()).willReturn(Collections.EMPTY_LIST);

		Mockito.doNothing().when(applyExternalTaxesStrategy).applyExternalTaxes(abstractOrder, exTaxDocumentMock);

		Mockito.doNothing().when(sessionService)
				.setAttribute(DefaultExternalTaxesService.SESSION_EXTERNAL_TAX_DOCUMENT, exTaxDocumentMock);
		Mockito.doNothing().when(sessionService)
				.removeAttribute(RecalculateExternalTaxesStrategy.SESSION_ATTIR_ORDER_RECALCULATION_HASH);
		Mockito.doNothing().when(modelService).save(Matchers.any());
		Mockito.doNothing().when(modelService).saveAll(Matchers.any());
		Mockito.doNothing().when(abstractOrder).setCalculated(Boolean.TRUE);
		Mockito.doNothing().when(abstractOrder).setTotalTaxValues(Collections.EMPTY_LIST);
	}

	/**
	 * Positive Unit test for {@link LeaExternalTaxesService#calculateExternalTaxes(AbstractOrderModel)}.
	 */
	@Test
	public void testCalculateExternalTaxesSuccess()
	{
		given(allTaxes.isEmpty()).willReturn(Boolean.FALSE);
		given(shippingTaxes.isEmpty()).willReturn(Boolean.FALSE);

		given(exTaxDocumentMock.getTaxSource()).willReturn(TaxSource.DEFAULT);
		leaExternalTaxesService.calculateExternalTaxes(abstractOrder);
		Mockito.verify(abstractOrder).setTaxSource(com.lyonscg.lyonscgcistaxaddon.enums.TaxSource.DEFAULT);

		given(exTaxDocumentMock.getTaxSource()).willReturn(TaxSource.EXTERNAL);
		leaExternalTaxesService.calculateExternalTaxes(abstractOrder);
		Mockito.verify(abstractOrder).setTaxSource(com.lyonscg.lyonscgcistaxaddon.enums.TaxSource.EXTERNAL);

		given(exTaxDocumentMock.getTaxSource()).willReturn(TaxSource.FALLBACK);
		leaExternalTaxesService.calculateExternalTaxes(abstractOrder);
		Mockito.verify(abstractOrder).setTaxSource(com.lyonscg.lyonscgcistaxaddon.enums.TaxSource.FALLBACK);
	}

	/**
	 * Negative Unit test for {@link LeaExternalTaxesService#calculateExternalTaxes(AbstractOrderModel)}.
	 */
	@Test
	public void testCalculateExternalTaxesFailure()
	{
		given(allTaxes.isEmpty()).willReturn(Boolean.TRUE);
		given(shippingTaxes.isEmpty()).willReturn(Boolean.TRUE);

		leaExternalTaxesService.calculateExternalTaxes(abstractOrder);
		Mockito.verify(abstractOrder).setTaxSource(null);
	}

	/**
	 * Unit test for {@link LeaExternalTaxesService#populateTaxSourceInOrder(AbstractOrderModel, ExternalTaxDocument)}.
	 */
	@Test
	public void testPopulateTaxSourceInOrder()
	{
		final LeaExternalTaxDocument leaExternalTaxDocument = (LeaExternalTaxDocument) exTaxDocument;

		leaExternalTaxDocument.setTaxSource(TaxSource.DEFAULT);
		leaExternalTaxesService.populateTaxSourceInOrder(abstractOrder, exTaxDocument);
		Mockito.verify(abstractOrder).setTaxSource(com.lyonscg.lyonscgcistaxaddon.enums.TaxSource.DEFAULT);

		leaExternalTaxDocument.setTaxSource(TaxSource.EXTERNAL);
		leaExternalTaxesService.populateTaxSourceInOrder(abstractOrder, exTaxDocument);
		Mockito.verify(abstractOrder).setTaxSource(com.lyonscg.lyonscgcistaxaddon.enums.TaxSource.EXTERNAL);

		leaExternalTaxDocument.setTaxSource(TaxSource.FALLBACK);
		leaExternalTaxesService.populateTaxSourceInOrder(abstractOrder, exTaxDocument);
		Mockito.verify(abstractOrder).setTaxSource(com.lyonscg.lyonscgcistaxaddon.enums.TaxSource.FALLBACK);
	}

}
