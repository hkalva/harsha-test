package com.lyonscg.lyonscgcistaxaddon.externaltax.impl;

import static org.mockito.BDDMockito.given;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.externaltax.ExternalTaxDocument;
import de.hybris.platform.util.TaxValue;


/**
 * Unit tests for {@link LeaApplyExternalTaxesStrategy}.
 */
@UnitTest
public class LeaApplyExternalTaxesStrategyTest
{
    @InjectMocks
    private LeaApplyExternalTaxesStrategy testClass;
    @Mock
    private AbstractOrderModel order;
    @Mock
    private AbstractOrderEntryModel entry;
    @Mock
    private ExternalTaxDocument externalTaxes;
    @Mock
    private CurrencyModel usdCurrency;

    private int entryNumber = 0;
    private long quantity = 2;
    private String usdSymbol = "USD";
    private String entryTaxValueString = "[<TV<STATE#10.00#true#10.00#" + usdSymbol + ">VT>]";
    private String shippingTaxValueString = "[<TV<STATE#1.00#true#1.00#" + usdSymbol + ">VT>]";

    public Map<Integer, List<TaxValue>> allEntryTaxes;
    public List<TaxValue> entrytaxValues;
    public List<TaxValue> shippingtaxValues;

    /** Setup data for tests. */
    @Before
    public void setUp()
    {
        testClass = new LeaApplyExternalTaxesStrategy();
        MockitoAnnotations.initMocks(this);

        allEntryTaxes = new HashMap<Integer, List<TaxValue>>(1);
        entrytaxValues = new ArrayList(TaxValue.parseTaxValueCollection(entryTaxValueString));
        shippingtaxValues = new ArrayList(TaxValue.parseTaxValueCollection(shippingTaxValueString));
        allEntryTaxes.put(Integer.valueOf(entryNumber), entrytaxValues);

        given(order.getNet()).willReturn(Boolean.TRUE);
        given(order.getEntries()).willReturn(Collections.singletonList(entry));
        given(order.getCurrency()).willReturn(usdCurrency);
        given(entry.getEntryNumber()).willReturn(Integer.valueOf(entryNumber));
        given(entry.getQuantity()).willReturn(Long.valueOf(quantity));
        given(externalTaxes.getAllTaxes()).willReturn(allEntryTaxes);
        given(externalTaxes.getTaxesForOrderEntry(entryNumber)).willReturn(entrytaxValues);
        given(externalTaxes.getShippingCostTaxes()).willReturn(shippingtaxValues);
        given(usdCurrency.getIsocode()).willReturn(usdSymbol);
        given(usdCurrency.getDigits()).willReturn(2);

    }

    /**
     * Unit test for {@link LeaApplyExternalTaxesStrategy#applyExternalTaxes(AbstractOrderModel, ExternalTaxDocument)}.
     */
    @Test
    public void applyExternalTaxesTest()
    {
        testClass.applyExternalTaxes(order, externalTaxes);
        Mockito.verify(order).setTotalTax(Double.valueOf(11));
        Mockito.verify(order).setDeliveryCostTax(Double.valueOf(1));
        Mockito.verify(order).setTotalTaxValues(
                TaxValue.parseTaxValueCollection("[<TV<STATE#5.0#true#5.0#USD>VT>|<TV<STATE#1.0#true#1.0#USD>VT>]"));
        Mockito.verify(order).setDeliveryCostTaxValues(TaxValue.parseTaxValueCollection(shippingTaxValueString));
    }

    /**
     * Failure(Order Not Net) Unit test for
     * {@link LeaApplyExternalTaxesStrategy#applyExternalTaxes(AbstractOrderModel, ExternalTaxDocument)}.
     */
    @Test(expected = IllegalStateException.class)
    public void applyExternalTaxesNotNetFailureTest()
    {
        given(order.getNet()).willReturn(Boolean.FALSE);
        testClass.applyExternalTaxes(order, externalTaxes);
    }

    /**
     * Failure(No entry number) Unit test for
     * {@link LeaApplyExternalTaxesStrategy#applyExternalTaxes(AbstractOrderModel, ExternalTaxDocument)}.
     */
    @Test(expected = IllegalStateException.class)
    public void applyExternalTaxesNoEntryNoFailureTest()
    {
        given(entry.getEntryNumber()).willReturn(null);
        testClass.applyExternalTaxes(order, externalTaxes);
    }

    /**
     * Failure(no currency digits) Unit test for
     * {@link LeaApplyExternalTaxesStrategy#applyExternalTaxes(AbstractOrderModel, ExternalTaxDocument)}.
     */
    @Test(expected = IllegalStateException.class)
    public void applyExternalTaxesNoCurrDigitsFailureTest()
    {
        given(usdCurrency.getDigits()).willReturn(null);
        testClass.applyExternalTaxes(order, externalTaxes);
    }

    /**
     * Failure(Additional tax entries) Unit test for
     * {@link LeaApplyExternalTaxesStrategy#applyExternalTaxes(AbstractOrderModel, ExternalTaxDocument)}.
     */
    @Test(expected = IllegalArgumentException.class)
    public void applyExternalTaxesMoreTaxEntriesFailureTest()
    {
        allEntryTaxes.put(Integer.valueOf(entryNumber + 1), entrytaxValues);
        testClass.applyExternalTaxes(order, externalTaxes);
    }

    /**
     * Failure(Different currency iso) Unit test for
     * {@link LeaApplyExternalTaxesStrategy#applyExternalTaxes(AbstractOrderModel, ExternalTaxDocument)}.
     */
    @Test(expected = IllegalArgumentException.class)
    public void applyExternalTaxesNoCurrIsoFailureTest()
    {
        given(usdCurrency.getIsocode()).willReturn(StringUtils.EMPTY);
        testClass.applyExternalTaxes(order, externalTaxes);
    }
}
