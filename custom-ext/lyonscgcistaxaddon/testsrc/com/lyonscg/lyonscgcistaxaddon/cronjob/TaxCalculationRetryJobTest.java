/**
 *
 */
package com.lyonscg.lyonscgcistaxaddon.cronjob;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.externaltax.ExternalTaxesService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.lyonscg.lyonscgcistaxaddon.enums.TaxSource;
import com.lyonscg.lyonscgcistaxaddon.model.TaxCalculationRetryCronjobModel;


//import com.lyonscg.lyonscgcistaxaddon.cronjob.TaxCalculationRetryJob.LeaTaxRetryExecutionBody;


/**
 * Unit tests for {@link TaxCalculationRetryJob}.
 *
 * @author lyonscg
 */
@UnitTest
public class TaxCalculationRetryJobTest
{
	private TaxCalculationRetryJob taxCalculationRetryJob;
	/** Query to search for orders. */
	private String query;
	/** Tax calculation success event. */
	private String taxSuccessEvent;
	/** Mock ExternalTaxesService. */
	@Mock
	private ExternalTaxesService externalTaxesService;
	/** Mock BaseSiteService. */
	@Mock
	private BaseSiteService baseSiteService;
	/** Mock BusinessProcessService. */
	@Mock
	private BusinessProcessService businessProcessService;
	/** Mock ModelService. */
	@Mock
	private ModelService modelService;
	/** Mock SessionService. */
	@Mock
	private SessionService sessionService;
	/** Mock FlexibleSearchService. */
	@Mock
	private FlexibleSearchService flexibleSearchService;
	/** Mock TaxCalculationRetryCronjobModel. */
	@Mock
	private TaxCalculationRetryCronjobModel taxCalculationRetryCronjobModel;
	/** Mock BaseSiteModel. */
	@Mock
	private BaseSiteModel baseSiteModel;
	/** Mock SearchResult. */
	@Mock
	private SearchResult<OrderModel> result;
	/** Mock OrderModel. */
	@Mock
	private OrderModel order;
	/** Mock UserModel. */
	@Mock
	private UserModel user;

	private String orderNumber;
	private int retryCount;
	private int waitTime;


	/** Setup data for tests. */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		taxCalculationRetryJob = new TaxCalculationRetryJob();

		query = "SELECT {pk} FROM {Order} WHERE {site} = ?site AND {status} IN (?statusList) AND {versionID} is NULL";
		taxSuccessEvent = "ExternalOrderTaxCalculation";
		orderNumber = "000001";
		retryCount = 3;
		waitTime = 10;
		taxCalculationRetryJob.setQuery(query);
		taxCalculationRetryJob.setTaxSuccessEvent(taxSuccessEvent);
		taxCalculationRetryJob.setExternalTaxesService(externalTaxesService);
		taxCalculationRetryJob.setBaseSiteService(baseSiteService);
		taxCalculationRetryJob.setBusinessProcessService(businessProcessService);
		taxCalculationRetryJob.setModelService(modelService);
		taxCalculationRetryJob.setFlexibleSearchService(flexibleSearchService);
		taxCalculationRetryJob.setSessionService(sessionService);

		given(taxCalculationRetryCronjobModel.getOrderStatusesToProcess()).willReturn(
				Collections.singletonList(OrderStatus.TAX_HOLD));
		given(taxCalculationRetryCronjobModel.getBaseSite()).willReturn(baseSiteModel);
		given(taxCalculationRetryCronjobModel.getInProgressOrderStatus()).willReturn(OrderStatus.TAX_PROCESSING);
		given(taxCalculationRetryCronjobModel.getFailureOrderStatus()).willReturn(OrderStatus.TAX_ERROR);
		given(taxCalculationRetryCronjobModel.getSuccessOrderStatus()).willReturn(OrderStatus.COMPLETED);
		given(taxCalculationRetryCronjobModel.getRetryFor()).willReturn(Integer.valueOf(retryCount));
		given(taxCalculationRetryCronjobModel.getWaitTime()).willReturn(Integer.valueOf(waitTime));
		given(flexibleSearchService.search(Mockito.anyString(), Mockito.anyMap())).willReturn(result);
		given(result.getResult()).willReturn(Collections.singletonList(order));
		given(order.getUser()).willReturn(user);
		given(order.getCode()).willReturn(orderNumber);
	}

	/**
	 * Test for for
	 * {@link TaxCalculationRetryJob#perform(com.lyonscg.lyonscgcistaxaddon.model.TaxCalculationRetryCronjobModel)} .
	 */
	@Test
	public void testPerformWithOrders()
	{
		taxCalculationRetryJob.perform(taxCalculationRetryCronjobModel);
		Mockito.verify(sessionService).executeInLocalView(Mockito.any(SessionExecutionBody.class), Mockito.any(UserModel.class));
	}

	/**
	 * Test for for
	 * {@link TaxCalculationRetryJob#perform(com.lyonscg.lyonscgcistaxaddon.model.TaxCalculationRetryCronjobModel)} .
	 */
	@Test
	public void testPerformWithoutOrders()
	{
		given(result.getResult()).willReturn(Collections.EMPTY_LIST);
		taxCalculationRetryJob.perform(taxCalculationRetryCronjobModel);
		Mockito.verify(sessionService, Mockito.never()).executeInLocalView(Mockito.any(SessionExecutionBody.class),
				Mockito.any(UserModel.class));
	}

	/**
	 * Test for for {@link TaxCalculationRetryJob#retryTaxCalculation(TaxCalculationRetryCronjobModel, OrderModel)} .
	 */
	@Test
	public void testRetryTaxCalculationTaxFailure()
	{
		taxCalculationRetryJob.retryTaxCalculation(taxCalculationRetryCronjobModel, order);
		given(order.getTaxSource()).willReturn(TaxSource.FALLBACK);
		Mockito.verify(order).setStatus(taxCalculationRetryCronjobModel.getInProgressOrderStatus());
		Mockito.verify(externalTaxesService, Mockito.times(retryCount)).calculateExternalTaxes(order);
		Mockito.verify(order).setStatus(taxCalculationRetryCronjobModel.getFailureOrderStatus());
	}

	/**
	 * Test for for {@link TaxCalculationRetryJob#retryTaxCalculation(TaxCalculationRetryCronjobModel, OrderModel)} .
	 */
	@Test
	public void testRetryTaxCalculationTaxSuccess()
	{
		Mockito.when(externalTaxesService.calculateExternalTaxes(order)).thenAnswer(new Answer<Boolean>()
		{
			@Override
			public Boolean answer(final InvocationOnMock invocation) throws Throwable
			{
				given(order.getTaxSource()).willReturn(TaxSource.EXTERNAL);
				return Boolean.TRUE;
			}
		});
		given(order.getTaxSource()).willReturn(TaxSource.FALLBACK);
		taxCalculationRetryJob.retryTaxCalculation(taxCalculationRetryCronjobModel, order);
		Mockito.verify(order).setStatus(taxCalculationRetryCronjobModel.getInProgressOrderStatus());
		Mockito.verify(externalTaxesService).calculateExternalTaxes(order);
		Mockito.verify(order).setStatus(taxCalculationRetryCronjobModel.getSuccessOrderStatus());
		Mockito.verify(businessProcessService).triggerEvent(orderNumber + "_" + taxCalculationRetryJob.getTaxSuccessEvent());
	}
}
