/**
 *
 */
package com.lyonscg.lyonscgcistaxaddon.integration.cis.tax.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.externaltax.ExternalTaxDocument;
import de.hybris.platform.integration.cis.tax.CisTaxDocOrder;
import de.hybris.platform.integration.cis.tax.service.impl.DefaultCisTaxCalculationService;
import de.hybris.platform.integration.commons.hystrix.HystrixExecutable;
import de.hybris.platform.tx.Transaction;

import java.util.Map;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.hybris.cis.api.model.CisOrder;
import com.hybris.cis.api.tax.model.CisTaxDoc;
import com.hybris.commons.client.RestResponse;
import com.lyonscg.lyonscgcistaxaddon.externaltax.LeaExternalTaxDocument;
import com.lyonscg.lyonscgcistaxaddon.externaltax.TaxSource;


/** @author lyonscg.
 * Lea tax implementation wrapper.
 *
 */
public class LeaCisTaxCalculationService extends DefaultCisTaxCalculationService
{
	/** Key value for Default Taxsource enum. */
	private static final String DEFAULT_TAX_SOURCE = "default";
	/** Key value for Fallback Taxsource enum. */
	private static final String FALLBACK_TAX_SOURCE = "fallback";
	/** Key value for EXTERNAL Taxsource enum. */
	private static final String EXTERNAL_TAX_SOURCE = "external";
	/** Logger for LeaCisTaxCalculationService */
	private static final Logger LOG = Logger.getLogger(LeaCisTaxCalculationService.class);
	/** TaxSource map */
	private Map<String, TaxSource> taxSourceCodeToTaxSourceDataMapping;

	@Override
	protected ExternalTaxDocument getExternalTaxDocument(final AbstractOrderModel abstractOrder, final CisOrder cisOrder)
	{
		final Transaction transaction = Transaction.current();
		return getOndemandHystrixCommandFactory().newCommand(getHystrixCommandConfig(),
				new HystrixExecutable<ExternalTaxDocument>()
				{
					@Override
					public ExternalTaxDocument runEvent()
					{
						return callExternalTaxSource(abstractOrder, cisOrder, transaction);
					}

					@Override
					public ExternalTaxDocument fallbackEvent()
					{
						return callFallbackTaxSource(abstractOrder, transaction);
					}

					@Override
					public ExternalTaxDocument defaultEvent()
					{
						return callDefaultTaxService(abstractOrder, transaction);
					}

				}).execute();
	}

	/** Call to default tax service. */
	protected ExternalTaxDocument callDefaultTaxService(final AbstractOrderModel abstractOrder, final Transaction transaction)
	{
		setAsCurrentTransaction(transaction);
		return populateAdditionalTaxData(getCalculateExternalTaxesFallbackStrategy().calculateExternalTaxes(abstractOrder),
				getTaxSourceCodeToTaxSourceDataMapping().get(DEFAULT_TAX_SOURCE));
	}

	/** Call to External tax service. */
	protected ExternalTaxDocument callExternalTaxSource(final AbstractOrderModel abstractOrder, final CisOrder cisOrder,
			final Transaction transaction)
	{
		setAsCurrentTransaction(transaction);
		final RestResponse<CisTaxDoc> cisTaxDocRestResponse;

		final String cisClientRef = abstractOrder.getGuid();
		if (abstractOrder instanceof CartModel)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug(String.format("Getting taxes from external tax service for cart: %s %s", abstractOrder.getCode(),
						ReflectionToStringBuilder.toString(cisOrder, ToStringStyle.SHORT_PREFIX_STYLE)));
			}

			cisTaxDocRestResponse = getTaxClient().quote(cisClientRef, cisOrder);
		}
		else
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug(String.format("Getting taxes from external tax service for order: %s %s", abstractOrder.getCode(),
						ReflectionToStringBuilder.toString(cisOrder, ToStringStyle.SHORT_PREFIX_STYLE)));
			}

			cisTaxDocRestResponse = getTaxClient().post(cisClientRef, cisOrder);
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug(String.format("External Tax Service returned Tax Document for order %s %s", abstractOrder.getCode(),
					ReflectionToStringBuilder.toString(cisTaxDocRestResponse.getResult(), ToStringStyle.SHORT_PREFIX_STYLE)));
		}
		return populateAdditionalTaxData(
				getExternalTaxDocumentConverter().convert(new CisTaxDocOrder(cisTaxDocRestResponse.getResult(), abstractOrder)),
				getTaxSourceCodeToTaxSourceDataMapping().get(EXTERNAL_TAX_SOURCE));
	}

	/** Call to fallback tax service. */
	protected ExternalTaxDocument callFallbackTaxSource(final AbstractOrderModel abstractOrder, final Transaction transaction)
	{
		setAsCurrentTransaction(transaction);
		return populateAdditionalTaxData(getCalculateExternalTaxesFallbackStrategy().calculateExternalTaxes(abstractOrder),
				getTaxSourceCodeToTaxSourceDataMapping().get(FALLBACK_TAX_SOURCE));
	}

	private void setAsCurrentTransaction(final Transaction transaction)
	{
		if (transaction.isRunning() && !transaction.isCurrent())
		{
			LOG.debug("Activating current thread as Current Transaction");
			transaction.activateAsCurrentTransaction();
		}
	}

	/**
	 * Populates additional tax data.
	 *
	 * @param externalTaxDocument
	 *           external tax data(without tax source)
	 * @param taxSource
	 *           tax source
	 * @return an ExternalTaxDocument that represents the taxes
	 */
	protected ExternalTaxDocument populateAdditionalTaxData(final ExternalTaxDocument externalTaxDocument,
			final TaxSource taxSource)
	{
		if (externalTaxDocument instanceof LeaExternalTaxDocument)
		{
			final LeaExternalTaxDocument leaExternalTaxDocument = (LeaExternalTaxDocument) externalTaxDocument;
			leaExternalTaxDocument.setTaxSource(taxSource);
		}
		return externalTaxDocument;
	}

	/**
	 * @return the taxSourceCodeToTaxSourceDataMapping
	 */
	public Map<String, TaxSource> getTaxSourceCodeToTaxSourceDataMapping()
	{
		return taxSourceCodeToTaxSourceDataMapping;
	}

	/**
	 * @param taxSourceCodeToTaxSourceDataMapping
	 *           the taxSourceCodeToTaxSourceDataMapping to set
	 */
	@Required
	public void setTaxSourceCodeToTaxSourceDataMapping(final Map<String, TaxSource> taxSourceCodeToTaxSourceDataMapping)
	{
		this.taxSourceCodeToTaxSourceDataMapping = taxSourceCodeToTaxSourceDataMapping;
	}

}
