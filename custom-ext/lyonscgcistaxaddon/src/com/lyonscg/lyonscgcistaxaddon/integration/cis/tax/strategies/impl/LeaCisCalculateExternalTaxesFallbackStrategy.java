/**
 *
 */
package com.lyonscg.lyonscgcistaxaddon.integration.cis.tax.strategies.impl;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.externaltax.ExternalTaxDocument;
import de.hybris.platform.integration.cis.tax.strategies.impl.DefaultCisCalculateExternalTaxesFallbackStrategy;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.TaxValue;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.commons.configuration.PropertyConverter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;


/**@author lyonscg.
 *implementation to calculate external tax.
 */
public class LeaCisCalculateExternalTaxesFallbackStrategy extends DefaultCisCalculateExternalTaxesFallbackStrategy
{
	/** Tax percentage key. */
	private static final String CISTAX_TAX_CALCULATION_FALLBACK_FIXEDPERCENTAGE = "cistax.taxCalculation.fallback.fixedpercentage";
	private static final BigDecimal HUNDRED = BigDecimal.valueOf(100D);
	/** Logger for the class. */
	private static final Logger LOG = Logger.getLogger(LeaCisCalculateExternalTaxesFallbackStrategy.class);
	/** siteConfigService bean. */
	private SiteConfigService siteConfigService;
	/** baseSiteService bean. */
	private BaseSiteService baseSiteService;

	@Override
	public ExternalTaxDocument calculateExternalTaxes(final AbstractOrderModel abstractOrder)
	{
		LOG.info("Calculating default percentage tax.");
		Assert.notNull(abstractOrder, "Order should not be null");
		final BigDecimal fixedPercentage = getTaxPercentage(abstractOrder);
		final String currencyCode = abstractOrder.getCurrency().getIsocode();
		final ExternalTaxDocument taxDocument = createExternalTaxDocument();
		for (final AbstractOrderEntryModel orderEntryModel : abstractOrder.getEntries())
		{
			final BigDecimal taxAmount = BigDecimal.valueOf(orderEntryModel.getTotalPrice().doubleValue())
					.multiply(fixedPercentage.divide(HUNDRED)).setScale(2, RoundingMode.HALF_EVEN);
			taxDocument.setTaxesForOrderEntry(orderEntryModel.getEntryNumber().intValue(),
					new TaxValue("ENTRY DEFAULT", taxAmount.doubleValue(), true, taxAmount.doubleValue(), currencyCode));
		}
		if (abstractOrder.getDeliveryCost() != null)
		{
			final BigDecimal shippingTax = BigDecimal.valueOf(abstractOrder.getDeliveryCost().doubleValue())
					.multiply(fixedPercentage.divide(HUNDRED)).setScale(2, RoundingMode.HALF_EVEN);
			taxDocument.setShippingCostTaxes(new TaxValue("SHIPPING DEFAULT", shippingTax.doubleValue(), true, shippingTax
					.doubleValue(), currencyCode));
		}

		return taxDocument;
	}

	/**
	 * Reads tax property using SiteConfigService if BaseSite is set in the session else gets base site uid from the
	 * order to find the property.
	 */
	protected BigDecimal getTaxPercentage(final AbstractOrderModel abstractOrder)
	{
		BigDecimal fixedPercentage;
		if (getBaseSiteService().getCurrentBaseSite() == null)
		{
			LOG.warn("BaseSite not set in session....");
			final BaseSiteModel site = abstractOrder.getSite();
			if (site == null)
			{
				LOG.warn("BaseSite not set in Order....");
				fixedPercentage = getConfigurationService().getConfiguration().getBigDecimal(
						CISTAX_TAX_CALCULATION_FALLBACK_FIXEDPERCENTAGE, BigDecimal.valueOf(10d));
			}
			else
			{
				LOG.debug("Got base site from order....");
				fixedPercentage = getConfigurationService().getConfiguration().getBigDecimal(
						CISTAX_TAX_CALCULATION_FALLBACK_FIXEDPERCENTAGE + "." + site.getUid(), BigDecimal.valueOf(10d));
			}
		}
		else
		{
			fixedPercentage = PropertyConverter.toBigDecimal(getSiteConfigService().getString(
					CISTAX_TAX_CALCULATION_FALLBACK_FIXEDPERCENTAGE, "10.0"));
		}
		return fixedPercentage;
	}

	/**
	 * Creates tax data wrapper by spring lookup method.
	 *
	 * @return an ExternalTaxDocument that represents the taxes
	 */
	protected ExternalTaxDocument createExternalTaxDocument()
	{
		throw new IllegalStateException("Instance should be created by lookup-method");
	}

	/**
	 * @return the siteConfigService
	 */
	public SiteConfigService getSiteConfigService()
	{
		return siteConfigService;
	}

	/**
	 * @param siteConfigService
	 *           the siteConfigService to set
	 */
	@Required
	public void setSiteConfigService(final SiteConfigService siteConfigService)
	{
		this.siteConfigService = siteConfigService;
	}

	/**
	 * @return the baseSiteService
	 */
	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	/**
	 * @param baseSiteService
	 *           the baseSiteService to set
	 */
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}
}
