/**
 *
 */
package com.lyonscg.lyonscgcistaxaddon.actions.order;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.lyonscg.lyonscgcistaxaddon.enums.TaxSource;



/**
 * Checks order status to find tax's source system and routes to corresponding action.
 *
 * @author lyonscg.
 *
 */
public class CheckOrderTaxSourceAction extends AbstractAction<OrderProcessModel>
{
	/** Logger for this class. */
	private static final Logger LOG = Logger.getLogger(CheckOrderTaxSourceAction.class);
	/** Approved source system for tax. */
	private TaxSource approvedTaxSource;

	/** On hold order status for orders with incomplete tax. */
	private OrderStatus holdOrderStatus;

	/** Transition enums. */
	public enum Transition
	{
		OK, NOK, WAIT;

		public static Set<String> getStringValues()
		{
			final Set<String> res = new HashSet<String>();
			for (final Transition transitions : Transition.values())
			{
				res.add(transitions.toString());
			}
			return res;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.processengine.spring.Action#execute(de.hybris.platform.processengine.model.BusinessProcessModel
	 * )
	 */
	@Override
	public final String execute(final OrderProcessModel process) throws RetryLaterException, Exception
	{
		return executeAction(process).toString();
	}

	protected Transition executeAction(final OrderProcessModel orderProcess)
	{
		final OrderModel order = orderProcess.getOrder();
		if (order != null)
		{
			if (getApprovedTaxSource().equals(order.getTaxSource()))
			{
				LOG.info("Tax calculation looks good. Moving on....");
				return Transition.OK;
			}
			else
			{
				order.setStatus(getHoldOrderStatus());
				getModelService().save(order);
				LOG.info("Waiting for tax to be calculated...");
				return Transition.WAIT;
			}
		}
		return Transition.NOK;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.processengine.spring.Action#getTransitions()
	 */
	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}

	/**
	 * @return the approvedTaxSource
	 */
	public TaxSource getApprovedTaxSource()
	{
		return approvedTaxSource;
	}

	/**
	 * @param approvedTaxSource
	 *           the approvedTaxSource to set
	 */
	@Required
	public void setApprovedTaxSource(final TaxSource approvedTaxSource)
	{
		this.approvedTaxSource = approvedTaxSource;
	}

	/**
	 * @return the holdOrderStatus
	 */
	public OrderStatus getHoldOrderStatus()
	{
		return holdOrderStatus;
	}

	/**
	 * @param holdOrderStatus
	 *           the holdOrderStatus to set
	 */
	@Required
	public void setHoldOrderStatus(final OrderStatus holdOrderStatus)
	{
		this.holdOrderStatus = holdOrderStatus;
	}


}
