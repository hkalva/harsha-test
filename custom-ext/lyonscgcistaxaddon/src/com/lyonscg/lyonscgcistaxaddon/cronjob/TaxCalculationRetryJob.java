package com.lyonscg.lyonscgcistaxaddon.cronjob;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.externaltax.ExternalTaxesService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.site.BaseSiteService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.lyonscg.lyonscgcistaxaddon.enums.TaxSource;
import com.lyonscg.lyonscgcistaxaddon.model.TaxCalculationRetryCronjobModel;


/**
 * TaxCalculationjob to calculate tax for orders using cis tax.
 *
 * @author lyonscg
 */
public class TaxCalculationRetryJob extends AbstractJobPerformable<TaxCalculationRetryCronjobModel>
{
	/** Logger for the class. */
	private static final Logger LOG = Logger.getLogger(TaxCalculationRetryJob.class);
	/** Query to search for orders. */
	private String query;
	/** externalTaxesService bean. */
	private ExternalTaxesService externalTaxesService;
	/** baseSiteService bean. */
	private BaseSiteService baseSiteService;
	/** businessProcessService bean. */
	private BusinessProcessService businessProcessService;
	/** Tax calculation success event. */
	private String taxSuccessEvent;

	/**
	 * Calculates tax for orders.
	 *
	 */
	@Override
	public PerformResult perform(final TaxCalculationRetryCronjobModel taxCalculationRetryCronjob)
	{
		LOG.info("Starting TaxCalculationRetryJob....");
		final List<OrderModel> orders = getOrdersToCalculateTax(taxCalculationRetryCronjob.getOrderStatusesToProcess(),
				taxCalculationRetryCronjob.getBaseSite());
		if (CollectionUtils.isEmpty(orders))
		{
			LOG.info("No orders found to calculate tax");
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		for (final OrderModel orderModel : orders)
		{
			sessionService.executeInLocalView(new SessionExecutionBody()
			{
				@Override
				public void executeWithoutResult()
				{
					retryTaxCalculation(taxCalculationRetryCronjob, orderModel);
				}
			}, orderModel.getUser());
		}
		LOG.info("TaxCalculationRetryJob completed....");
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	private List<OrderModel> getOrdersToCalculateTax(final List<OrderStatus> orderStatuses, final BaseSiteModel baseSite)
	{
		LOG.info("Querying for orders");
		final Map<String, Object> queryParams = new HashMap<String, Object>(2);
		queryParams.put("site", baseSite);
		queryParams.put("statusList", orderStatuses);
		final SearchResult<OrderModel> result = flexibleSearchService.search(getQuery(), queryParams);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Found " + result.getTotalCount() + " order to calculate tax");
		}
		return result.getResult();
	}

	/**
	 * Calculates tax for order.
	 *
	 */
	protected void retryTaxCalculation(final TaxCalculationRetryCronjobModel taxCalculationRetryCronjob,
			final OrderModel orderModel)
	{
		final String orderCode = orderModel.getCode();
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Trying to calculate tax for order " + orderCode);
		}
		orderModel.setStatus(taxCalculationRetryCronjob.getInProgressOrderStatus());
		modelService.save(orderModel);
		getBaseSiteService().setCurrentBaseSite(orderModel.getSite(), true);
		int noOfTimes = 1;
		while (!TaxSource.EXTERNAL.equals(orderModel.getTaxSource())
				&& noOfTimes <= taxCalculationRetryCronjob.getRetryFor().intValue())
		{
			waitTime(taxCalculationRetryCronjob, noOfTimes);
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Trying to get tax for " + noOfTimes + " time for order " + orderCode);
			}
			getExternalTaxesService().calculateExternalTaxes(orderModel);
			noOfTimes++;
		}
		OrderStatus finalOrderStatus = null;
		final boolean isTaxCalculationSuccess = TaxSource.EXTERNAL.equals(orderModel.getTaxSource());
		if (isTaxCalculationSuccess)
		{
			finalOrderStatus = taxCalculationRetryCronjob.getSuccessOrderStatus();
		}
		else
		{
			finalOrderStatus = taxCalculationRetryCronjob.getFailureOrderStatus();
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Final order status for order " + orderCode + " is " + finalOrderStatus);
		}
		orderModel.setStatus(finalOrderStatus);
		modelService.save(orderModel);
		if (isTaxCalculationSuccess)
		{
			LOG.info("Triggering event to continue order approval process");
			getBusinessProcessService().triggerEvent(orderCode + "_" + getTaxSuccessEvent());
		}
	}

	private void waitTime(final TaxCalculationRetryCronjobModel taxCalculationCronjob, final int noOfTimes)
	{
		if (noOfTimes != 1)
		{
			try
			{
				if (LOG.isDebugEnabled())
				{
					LOG.debug("Waiting for " + taxCalculationCronjob.getWaitTime().intValue() + " seconds");
				}
				Thread.sleep(taxCalculationCronjob.getWaitTime().intValue());
			}
			catch (final InterruptedException e)
			{
				LOG.error("Unable to wait", e);
			}
		}
	}


	/**
	 * @return the query
	 */
	public String getQuery()
	{
		return query;
	}

	/**
	 * @param query
	 *           the query to set
	 */
	@Required
	public void setQuery(final String query)
	{
		this.query = query;
	}

	/**
	 * @return the externalTaxesService
	 */
	public ExternalTaxesService getExternalTaxesService()
	{
		return externalTaxesService;
	}

	/**
	 * @param externalTaxesService
	 *           the externalTaxesService to set
	 */
	@Required
	public void setExternalTaxesService(final ExternalTaxesService externalTaxesService)
	{
		this.externalTaxesService = externalTaxesService;
	}

	/**
	 * @return the baseSiteService
	 */
	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	/**
	 * @param baseSiteService
	 *           the baseSiteService to set
	 */
	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	/**
	 * @return the businessProcessService
	 */
	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * @param businessProcessService
	 *           the businessProcessService to set
	 */
	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * @return the taxSuccessEvent
	 */
	public String getTaxSuccessEvent()
	{
		return taxSuccessEvent;
	}

	/**
	 * @param taxSuccessEvent
	 *           the taxSuccessEvent to set
	 */
	@Required
	public void setTaxSuccessEvent(final String taxSuccessEvent)
	{
		this.taxSuccessEvent = taxSuccessEvent;
	}

}
