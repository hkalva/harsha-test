package com.lyonscg.lyonscgcistaxaddon.order.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.commerceservices.externaltax.ExternalTaxesService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.impl.DefaultCalculationService;
import de.hybris.platform.order.strategies.calculation.OrderRequiresCalculationStrategy;
import de.hybris.platform.util.TaxValue;


/**
 * Adds and accounts for delivery tax values to calculation.
 */
public class LeaTaxCalculationService extends DefaultCalculationService
{
    private ExternalTaxesService externalTaxesService;
    private OrderRequiresCalculationStrategy orderRequiresCalculationStrategy;

    /**
     * Adds delivery tax values in calculation.
     * 
     * @param order
     *            - {@link AbstractOrderModel} for calculation.
     * @param recalculate
     *            - true to force recalculation.
     * @param digits
     *            - Decimal places
     * @param taxAdjustmentFactor
     *            - Tax adjustment factor.
     * @param taxValueMap
     *            - All Tax values.
     * @return total tax amount
     */
    @Override
    protected double calculateTotalTaxValues(final AbstractOrderModel order, final boolean recalculate, final int digits,
            final double taxAdjustmentFactor, Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap)
    {
        return addDeliveryTaxValues(
                order,
                recalculate,
                super.calculateTotalTaxValues(order, recalculate, digits, taxAdjustmentFactor,
                        calculateExternalTax(order, recalculate, taxValueMap)));
    }

    /**
     * Add delivery tax values to order.
     * 
     * @param order
     *            - {@link AbstractOrderModel} for calculation.
     * @param recalculate
     *            - true to force recalculation.
     * @param totalTax
     *            - total tax amount
     * @return total tax amount after calculation.
     */
    protected double addDeliveryTaxValues(final AbstractOrderModel order, final boolean recalculate, double totalTax)
    {
        if (recalculate || getOrderRequiresCalculationStrategy().requiresCalculation(order))
        {
            Collection<TaxValue> deliveryCostTaxValues = order.getDeliveryCostTaxValues();
            if (CollectionUtils.isNotEmpty(deliveryCostTaxValues))
            {
                Collection<TaxValue> allTaxValues = new ArrayList<>();
                Collection<TaxValue> totalTaxValues = order.getTotalTaxValues();
                if (CollectionUtils.isNotEmpty(totalTaxValues))
                {
                    allTaxValues.addAll(totalTaxValues);
                }
                allTaxValues.addAll(deliveryCostTaxValues);
                order.setTotalTaxValues(allTaxValues);
                return totalTax + order.getDeliveryCostTax();
            }
            else
            {
                order.setDeliveryCostTax(Double.valueOf(0));
            }
        }
        return totalTax;
    }

    /**
     * Calculate tax values with external tax service.
     * 
     * @param order
     *            - {@link AbstractOrderModel} for calculation.
     * @param recalculate
     *            - true to force recalculation.
     * @param taxValueMap
     *            - All Tax values map.
     * @return - All Tax values map after calculation.
     */
    protected Map<TaxValue, Map<Set<TaxValue>, Double>> calculateExternalTax(final AbstractOrderModel order,
            final boolean recalculate, Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap)
    {
        if (recalculate || getOrderRequiresCalculationStrategy().requiresCalculation(order))
        {
            if (getExternalTaxesService().calculateExternalTaxes(order))
            {
                return calculateSubtotal(order, recalculate);
            }
        }
        return taxValueMap;
    }

    protected ExternalTaxesService getExternalTaxesService()
    {
        return externalTaxesService;
    }

    @Required
    public void setExternalTaxesService(ExternalTaxesService externalTaxesService)
    {
        this.externalTaxesService = externalTaxesService;
    }

    protected OrderRequiresCalculationStrategy getOrderRequiresCalculationStrategy()
    {
        return orderRequiresCalculationStrategy;
    }

    @Required
    public void setOrderRequiresCalculationStrategy(OrderRequiresCalculationStrategy orderRequiresCalculationStrategy)
    {
        super.setOrderRequiresCalculationStrategy(orderRequiresCalculationStrategy);
        this.orderRequiresCalculationStrategy = orderRequiresCalculationStrategy;
    }
}
