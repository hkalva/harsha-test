/**
 *
 */
package com.lyonscg.lyonscgcistaxaddon.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.lyonscg.lyonscgcistaxaddon.externaltax.TaxSource;


/**@author lyonscg.
 * Populates tax source for AbstractOrderData.
 *
 */
public class LeaOrderTaxSourcePopulator implements Populator<AbstractOrderModel, AbstractOrderData>
{
	/** TaxSource map */
	private Map<String, TaxSource> taxSourceCodeToTaxSourceDataMapping;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target) throws ConversionException
	{
		final com.lyonscg.lyonscgcistaxaddon.enums.TaxSource taxSource = source.getTaxSource();
		if (taxSource != null)
		{
			final TaxSource taxSourceData = getTaxSourceCodeToTaxSourceDataMapping().get(taxSource.getCode().toLowerCase(Locale.getDefault()));
			Assert.notNull(taxSourceData, "TaxSource cannot not be null");
			target.setTaxSource(taxSourceData);
		}
	}

	/**
	 * @return the taxSourceCodeToTaxSourceDataMapping
	 */
	public Map<String, TaxSource> getTaxSourceCodeToTaxSourceDataMapping()
	{
		return taxSourceCodeToTaxSourceDataMapping;
	}

	/**
	 * @param taxSourceCodeToTaxSourceDataMapping
	 *           the taxSourceCodeToTaxSourceDataMapping to set
	 */
	@Required
	public void setTaxSourceCodeToTaxSourceDataMapping(final Map<String, TaxSource> taxSourceCodeToTaxSourceDataMapping)
	{
		this.taxSourceCodeToTaxSourceDataMapping = taxSourceCodeToTaxSourceDataMapping;
	}



}
