package com.lyonscg.lyonscgcistaxaddon.order;

import java.util.Collection;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;
import de.hybris.platform.util.TaxValue;


/**
 * Dynamic attribute handler for {@link AbstractOrderModel#DELIVERYCOSTTAXVALUES}.
 *
 */
public class DeliveryCostTaxValuesAttributeHandler implements DynamicAttributeHandler<Collection<TaxValue>, AbstractOrderModel>
{
    /**
     * Getter implementation of {@link AbstractOrderModel#getDeliveryCostTaxValues()}
     */
    @Override
    public Collection<TaxValue> get(AbstractOrderModel model)
    {
        return TaxValue.parseTaxValueCollection(model.getDeliveryCostTaxInternal());
    }

    /**
     * Setter implementation of {@link AbstractOrderModel#setDeliveryCostTaxValues(Collection)}
     */
    @Override
    public void set(AbstractOrderModel model, Collection<TaxValue> taxValues)
    {
        model.setDeliveryCostTaxInternal(TaxValue.toString(taxValues));
    }

}
