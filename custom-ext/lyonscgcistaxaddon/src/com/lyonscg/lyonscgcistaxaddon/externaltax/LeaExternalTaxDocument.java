/**
 *
 */
package com.lyonscg.lyonscgcistaxaddon.externaltax;

import de.hybris.platform.externaltax.ExternalTaxDocument;


/**@author lyonscg.
 * This class is a Wrapper around {@link ExternalTaxDocument} to display tax source.
 *
 */
public class LeaExternalTaxDocument extends ExternalTaxDocument
{
	/** Enum to indicate the tax source */
	private TaxSource taxSource;

	/**
	 * @return the taxSource
	 */
	public TaxSource getTaxSource()
	{
		return taxSource;
	}

	/**
	 * @param taxSource
	 *           the taxSource to set
	 */
	public void setTaxSource(final TaxSource taxSource)
	{
		this.taxSource = taxSource;
	}
}
