package com.lyonscg.lyonscgcistaxaddon.externaltax.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.hybris.platform.core.CoreAlgorithms;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.externaltax.ApplyExternalTaxesStrategy;
import de.hybris.platform.externaltax.ExternalTaxDocument;
import de.hybris.platform.util.TaxValue;


/**
 * Separates line item taxes from shipping taxes and stores them in separate attributes.
 */
public class LeaApplyExternalTaxesStrategy implements ApplyExternalTaxesStrategy
{

    /**
     * Wrapper class for TaxData.
     */
    private static class OrderTaxValues
    {
        private BigDecimal totalTaxAmount;
        private List<TaxValue> allTaxValues;

        /**
         * Constructor for OrderTaxValues.
         *
         * @param totalTaxAmount
         *            - Total tax amount.
         * @param allTaxValues
         *            - Tax Values.
         */
        protected OrderTaxValues(BigDecimal totalTaxAmount, List<TaxValue> allTaxValues)
        {
            this.totalTaxAmount = totalTaxAmount;
            this.allTaxValues = allTaxValues;
        }

        /**
         * Getter for totalTaxAmount.
         *
         * @return the totalTaxAmount
         */
        protected BigDecimal getTotalTaxAmount()
        {
            return totalTaxAmount;
        }

        /**
         * Getter for allTaxValues.
         *
         * @return the allTaxValues
         */
        protected List<TaxValue> getAllTaxValues()
        {
            return allTaxValues;
        }
    }

    /**
     * Populates tax amount and tax values in {@link AbstractOrderModel}.
     *
     * @param order
     *            - {@link AbstractOrderModel} to populate tax.
     * @param externalTaxes
     *            - Tax details.
     */
    @Override
    public void applyExternalTaxes(final AbstractOrderModel order, final ExternalTaxDocument externalTaxes)
    {
        if (!Boolean.TRUE.equals(order.getNet()))
        {
            throw new IllegalStateException("Order " + order.getCode() + " must be of type NET to apply external taxes to it.");
        }
        OrderTaxValues entryTaxSum = applyEntryTaxes(order, externalTaxes);
        OrderTaxValues shippingTaxSum = applyShippingCostTaxes(order, externalTaxes);
        entryTaxSum.getAllTaxValues().addAll(shippingTaxSum.getAllTaxValues());
        order.setTotalTaxValues(entryTaxSum.getAllTaxValues());
        setTotalTax(order, entryTaxSum.getTotalTaxAmount().add(shippingTaxSum.getTotalTaxAmount()));
    }

    /**
     * Populates tax amount and tax values for all {@link AbstractOrderEntryModel} in {@link AbstractOrderModel}.
     * 
     * @param order
     *            - {@link AbstractOrderModel} to populate tax.
     * @param externalTaxes
     *            - Tax details.
     * @return - {@link OrderTaxValues} for line items.
     */
    protected OrderTaxValues applyEntryTaxes(final AbstractOrderModel order, final ExternalTaxDocument taxDoc)
    {
        BigDecimal totalTax = BigDecimal.ZERO;
        final Set<Integer> consumedEntryNumbers = new HashSet<Integer>(taxDoc.getAllTaxes().keySet());
        final List<TaxValue> allTaxValues = new ArrayList<>();
        for (final AbstractOrderEntryModel entry : order.getEntries())
        {
            final Integer entryNumber = entry.getEntryNumber();
            if (entryNumber == null)
            {
                throw new IllegalStateException("Order entry " + order.getCode() + "." + entry
                        + " does not have a entry number. Cannot apply external tax to it.");
            }
            final List<TaxValue> modifiedTaxValues = new ArrayList<>();
            final List<TaxValue> taxesForOrderEntry = taxDoc.getTaxesForOrderEntry(entryNumber.intValue());
            if (taxesForOrderEntry != null)
            {
                for (final TaxValue taxForOrderEntry : taxesForOrderEntry)
                {
                    assertValidTaxValue(order, taxForOrderEntry);
                    totalTax = totalTax.add(new BigDecimal(taxForOrderEntry.getAppliedValue()));
                    modifiedTaxValues.add(convertTaxValueForUnitQuantity(taxForOrderEntry, entry.getQuantity().longValue()));
                }
            }
            entry.setTaxValues(modifiedTaxValues);
            consumedEntryNumbers.remove(entryNumber);
            allTaxValues.addAll(modifiedTaxValues);
        }

        if (!consumedEntryNumbers.isEmpty())
        {
            throw new IllegalArgumentException("External tax document " + taxDoc
                    + " seems to contain taxes for more lines items than available within " + order.getCode());
        }

        return new OrderTaxValues(totalTax, allTaxValues);
    }

    /**
     * Populates shipping tax amount and tax values in {@link AbstractOrderModel}.
     * 
     * @param order
     *            - {@link AbstractOrderModel} to populate tax.
     * @param externalTaxes
     *            - Tax details.
     * @return - {@link OrderTaxValues} for shipping amount.
     */
    protected OrderTaxValues applyShippingCostTaxes(final AbstractOrderModel order, final ExternalTaxDocument taxDoc)
    {
        BigDecimal totalTax = BigDecimal.ZERO;
        final List<TaxValue> allTaxValues = new ArrayList<>();
        final List<TaxValue> shippingTaxes = taxDoc.getShippingCostTaxes();
        if (shippingTaxes != null)
        {
            for (final TaxValue taxForOrderEntry : shippingTaxes)
            {
                assertValidTaxValue(order, taxForOrderEntry);
                totalTax = totalTax.add(new BigDecimal(taxForOrderEntry.getAppliedValue()));
                allTaxValues.add(taxForOrderEntry);
            }
        }
        order.setDeliveryCostTax(totalTax.doubleValue());
        order.setDeliveryCostTaxValues(allTaxValues);
        return new OrderTaxValues(totalTax, allTaxValues);
    }

    /**
     * Rounds and sets order total.
     * 
     * @param order
     *            - {@link AbstractOrderModel} to set total tax.
     * @param totalTaxSum
     *            - Order total
     */
    protected void setTotalTax(final AbstractOrderModel order, final BigDecimal totalTaxSum)
    {
        final Integer digits = order.getCurrency().getDigits();
        if (digits == null)
        {
            throw new IllegalStateException("Order " + order.getCode()
                    + " has got a currency without decimal digits defined. Cannot apply external taxes.");
        }
        order.setTotalTax(Double.valueOf(CoreAlgorithms.round(totalTaxSum.doubleValue(), digits.intValue())));
    }

    /**
     * Validates {@link TaxValue}
     * 
     * @param order
     *            - {@link AbstractOrderModel} to set total tax.
     * @param value
     *            - {@link TaxValue} for validation.
     */
    protected void assertValidTaxValue(final AbstractOrderModel order, final TaxValue value)
    {
        if (!value.isAbsolute())
        {
            throw new IllegalArgumentException("External tax " + value + " is not absolute. Cannot apply it to order "
                    + order.getCode());
        }
        if (!order.getCurrency().getIsocode().equalsIgnoreCase(value.getCurrencyIsoCode()))
        {
            throw new IllegalArgumentException("External tax " + value + " currency " + value.getCurrencyIsoCode()
                    + " does not match order currency " + order.getCurrency().getIsocode() + ". Cannot apply.");
        }
    }

    /**
     * Converts tax values for per unit quantity.
     * 
     * @param quantity
     *            - line item quantity.
     * @param taxValue
     *            - {@link TaxValue} for conversion.
     */
    protected TaxValue convertTaxValueForUnitQuantity(final TaxValue taxValue, final long quantity)
    {
        final double unitQuantityTax = taxValue.getValue() / quantity;
        return new TaxValue(taxValue.getCode(), unitQuantityTax, taxValue.isAbsolute(), taxValue.getValue(),
                taxValue.getCurrencyIsoCode());
    }
}
