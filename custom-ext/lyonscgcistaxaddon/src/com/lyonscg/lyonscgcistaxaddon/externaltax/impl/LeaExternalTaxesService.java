/**
 *
 */
package com.lyonscg.lyonscgcistaxaddon.externaltax.impl;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.lyonscg.lyonscgcistaxaddon.externaltax.LeaExternalTaxDocument;
import com.lyonscg.lyonscgcistaxaddon.externaltax.TaxSource;

import de.hybris.platform.commerceservices.externaltax.RecalculateExternalTaxesStrategy;
import de.hybris.platform.commerceservices.externaltax.impl.DefaultExternalTaxesService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.externaltax.ExternalTaxDocument;


/**
 * LeaExternalTaxesService implementation.
 *
 * @author lyonscg
 */
public class LeaExternalTaxesService extends DefaultExternalTaxesService
{
    /** Tax Source Mapping. */
    private Map<TaxSource, String> taxSourceDataToTaxSourceCodeMapping;
    private static final Logger LOG = Logger.getLogger(LeaExternalTaxesService.class);

    /**
     * Calculate external taxes.
     * 
     * @param abstractOrder
     *            - {@link AbstractOrderModel} to calculate tax.
     * @return - Returns true if successfully calculated.
     */
    @Override
    public boolean calculateExternalTaxes(final AbstractOrderModel abstractOrder)
    {
        if (getDecideExternalTaxesStrategy().shouldCalculateExternalTaxes(abstractOrder))
        {
            if (getRecalculateExternalTaxesStrategy().recalculate(abstractOrder))
            {
                LOG.info("Trying to call external tax service...");
                final ExternalTaxDocument exTaxDocument = getCalculateExternalTaxesStrategy().calculateExternalTaxes(
                        abstractOrder);
                Assert.notNull(exTaxDocument, "ExternalTaxDocument should not be null");
                // check if external tax calculation was successful
                if (!exTaxDocument.getAllTaxes().isEmpty() && !exTaxDocument.getShippingCostTaxes().isEmpty())
                {
                    getApplyExternalTaxesStrategy().applyExternalTaxes(abstractOrder, exTaxDocument);
                    getSessionService().setAttribute(SESSION_EXTERNAL_TAX_DOCUMENT, exTaxDocument);
                    populateTaxSourceInOrder(abstractOrder, exTaxDocument);
                    saveOrder(abstractOrder);
                    return true;
                }
                else
                {
                    // the external tax calculation failed
                    getSessionService().removeAttribute(RecalculateExternalTaxesStrategy.SESSION_ATTIR_ORDER_RECALCULATION_HASH);
                    clearSessionTaxDocument();
                    clearTaxValues(abstractOrder);
                    abstractOrder.setTaxSource(null);
                    saveOrder(abstractOrder);
                }
            }
            else
            {
                LOG.info("External tax service call not required...");
                // get the cached tax document
                getApplyExternalTaxesStrategy().applyExternalTaxes(abstractOrder, getSessionExternalTaxDocument(abstractOrder));
                saveOrder(abstractOrder);
                return true;
            }
        }
        return false;
    }

    /**
     * Populate additional attribute in {@link AbstractOrderModel}
     * 
     * @param abstractOrder
     *            - Abstract Order Model
     * @param exTaxDocument
     *            - Tax data
     */
    protected void populateTaxSourceInOrder(final AbstractOrderModel abstractOrder, final ExternalTaxDocument exTaxDocument)
    {
        if (exTaxDocument instanceof LeaExternalTaxDocument)
        {
            final LeaExternalTaxDocument leaExternalTaxDocument = (LeaExternalTaxDocument) exTaxDocument;
            abstractOrder.setTaxSource(com.lyonscg.lyonscgcistaxaddon.enums.TaxSource
                    .valueOf(getTaxSourceDataToTaxSourceCodeMapping().get(leaExternalTaxDocument.getTaxSource())));
        }

    }

    /**
     * @return the taxSourceDataToTaxSourceCodeMapping
     */
    public Map<TaxSource, String> getTaxSourceDataToTaxSourceCodeMapping()
    {
        return taxSourceDataToTaxSourceCodeMapping;
    }

    /**
     * @param taxSourceDataToTaxSourceCodeMapping
     *            the taxSourceDataToTaxSourceCodeMapping to set
     */
    @Required
    public void setTaxSourceDataToTaxSourceCodeMapping(final Map<TaxSource, String> taxSourceDataToTaxSourceCodeMapping)
    {
        this.taxSourceDataToTaxSourceCodeMapping = taxSourceDataToTaxSourceCodeMapping;
    }
}
