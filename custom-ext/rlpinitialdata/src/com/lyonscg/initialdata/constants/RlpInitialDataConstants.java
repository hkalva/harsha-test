/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lyonscg.initialdata.constants;

/**
 * Global class for all RlpInitialData constants.
 */
public final class RlpInitialDataConstants extends GeneratedRlpInitialDataConstants
{
	public static final String EXTENSIONNAME = "rlpinitialdata";

	private RlpInitialDataConstants()
	{
		//empty
	}
}
