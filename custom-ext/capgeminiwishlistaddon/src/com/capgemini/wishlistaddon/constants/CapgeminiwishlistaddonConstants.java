package com.capgemini.wishlistaddon.constants;

/**
 *
 * @author lyonscg
 *
 */
public class CapgeminiwishlistaddonConstants extends GeneratedCapgeminiwishlistaddonConstants
{
    /**
     * constant for extension name.
     */
	public static final String EXTENSIONNAME = "capgeminiwishlistaddon";

	/**
	 * default private constructor. 
	 */
	private CapgeminiwishlistaddonConstants()
	{
		//empty
	}
	
	
}
