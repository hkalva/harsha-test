package com.capgemini.wishlistaddon.constants;


/**

* Global class for all Lyonscgwishlistaddon constants. You can add global constants for your extension into this class.

*/

public class LyonscgwishlistaddonConstants  extends GeneratedCapgeminiwishlistaddonConstants
{
    public static final String EXTENSIONNAME = "capgeminiwishlistaddon";
    
    private LyonscgwishlistaddonConstants ()
    {
        //empty
    }
    
    
}

