var ACC = ACC || {}; // make sure ACC is available

ACC.wishlist = {
	_autoload: [
		'init',
		'bindAddToList',
		'bindCreateListChange',
		'bindWishlistAddToCart',
		'bindAddToCartAll',
		'validateForm',
		'renderListingPageButtons',
		'bindListingPageListener'
	],
	
	wishlistAllowed: false,

	init: function() {
		ACC.wishlist.wishlistAllowed = ACC.addons.capgeminiwishlistaddon['wishlistAllowed'] === 'true';
	},
	
	renderListingPageButtons: function() {
		if (ACC.wishlist.wishlistAllowed) {
			$('.product-item .addtocart').each(function() {
				if ($(this).next('.addToList').length == 0) {
					var productCode = $(':input[name="productCodePost"]', this).val();
					$(this).after('<div class="addToList"><a class="btn btn-default btn-icon js-add-to-cart glyphicon-list js-addToList" data-productcode="' + productCode + '">' + ACC.addons.capgeminiwishlistaddon["text.wishlist.addToList"] + '</a></div>');
				}
			});
		}
	},
	
	bindListingPageListener: function() {
		var $observerTarget = $('.product__listing.product__grid');
		
		if ($observerTarget.length > 0) {
			var observer = new MutationObserver(function(mutations) {
				ACC.wishlist.renderListingPageButtons();   
			});
			
			var observerConfig = {childList: true};
			observer.observe($observerTarget[0], observerConfig);
		}
	},
	
	validateForm: function() {

		$(document).on('click', '#addToWishlistForm button', function(e) {
			e.preventDefault();

			var $form = $(this).closest('form'),
				url = $form.attr('action');

			ACC.wishlist.submitAddToList(url, $form);
		});
	},

	wishlistDisplayMsg: function(showError, msg) {
		var $errorContainer = $('.js-wishlist-msg');

		(showError) ? $errorContainer.addClass('error') : $errorContainer.removeClass('error');

		$errorContainer.text(msg).show();

		ACC.colorbox.resize();

	},
	
	serializeForm: function(form) {
		var o = {};
		var a = form.serializeArray();
		
		$.each(a, function() {
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		
		return o;
	},

	submitAddToList: function(url, form) {

		var dataObj = ACC.wishlist.serializeForm(form);

		$.ajax({
			url: url,
			method: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(dataObj),
			success: function(data) {
				if(data.status == 200) {
					$("#cboxClose").click();
				} else {
					ACC.wishlist.wishlistDisplayMsg(true, data.msg);
				}
			},
			error: function(data) {
				ACC.wishlist.wishlistDisplayMsg(true, data.msg);
			}
		});

	},

	bindAddToList: function() {

		$(document).on('click', '.js-addToList', function(e) {
			e.preventDefault();

			var productCode = $(this).data('productcode'),
				title = $(this).text().trim(),
				lang = $('html').attr('lang');

			$.ajax({
				type: 'GET',
				url: ACC.config.contextPath + '/wishlist/addToWishlist?productCode=' + productCode,
				success: function(data) {
					ACC.colorbox.open(title, {
						html: data,
						width: '380px'
					})
				},
				error: function() {

				}
			});

		});
	},

	bindCreateListChange: function() {
		$(document).on('change', '#wishlistCode', function() {
			var isCreate = (!$(this).val()) ? true : false;
			$('#create').val(isCreate);
		});
	},

	bindWishlistAddToCart: function() {
		$(document).on('click touchstart', '.js-wishlist-addToCart', function(e) {
			e.preventDefault();

			var $thisForm = $(this).closest('.js-wishlist-addToCartForm');

			$thisForm.ajaxForm({
				success: ACC.wishlist.showAddToCartPopup
			});

			$thisForm.submit();
		});
	},

	showAddToCartPopup: function(cartResult, statusText, xhr, formElement) {
		
		$('#addToCartLayer').remove();

		if (typeof ACC.minicart.updateMiniCartDisplay == 'function') {
			ACC.minicart.updateMiniCartDisplay();
		}
		var titleHeader = $('#addToCartTitle').html();

		ACC.colorbox.open(titleHeader, {
			html: cartResult.addToCartLayer,
			width: "460px"
		});

		var productCode = $('[name=productCodePost]', formElement).val();
		var quantityField = $('[name=qty]', formElement).val();

		var quantity = 1;
		if (quantityField != undefined) {
			quantity = quantityField;
		}

		var cartAnalyticsData = cartResult.cartAnalyticsData;

		var cartData = {
			"cartCode": cartAnalyticsData.cartCode,
			"productCode": productCode, "quantity": quantity,
			"productPrice": cartAnalyticsData.productPostPrice,
			"productName": cartAnalyticsData.productName
		};
		ACC.track.trackAddToCart(productCode, quantity, cartData);

	},

	bindAddToCartAll: function() {
		var bulkAddToCartUrl;

		$(document).on('click touchstart', '.js-wishlist-addToCart-all', function(e) {
			e.preventDefault();

			bulkAddToCartUrl = $(this).data('addtocart-url');
			ACC.wishlist.bulkAddToCart(bulkAddToCartUrl)

		});
	},

	bulkAddToCart: function(url) {
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			contentType: 'application/json',
			data: ACC.wishlist.getAllProdcuts(),
			async: false,
			success: ACC.wishlist.showBulkAddToCartPopup,
			error: function (jqXHR, textStatus, errorThrown) {
				console.log("The following error occurred: " + textStatus, errorThrown);
			}
		});
	},

	showBulkAddToCartPopup: function(cartResult, statusText, xhr, formElement) {
		$('#addToCartLayer').remove();

		if (typeof ACC.minicart.updateMiniCartDisplay == 'function') {
			ACC.minicart.updateMiniCartDisplay();
		}
		var titleHeader = $('#addToCartTitle').html();

		ACC.colorbox.open(titleHeader, {
			html: cartResult.addToCartLayer,
			width: "460px"
		});
	},

	getAllProdcuts: function() {
		var productList = [], productId, productQty;

		$('.wishlist_product').each(function() {
			productId = $(this).data('product-id');
			productQty = $(this).data('product-qty');

			productList.push({
				"product": {
					"code": productId
				},
				"quantity": productQty
			});

		});

		return JSON.stringify({'cartEntries': productList});
	}
};