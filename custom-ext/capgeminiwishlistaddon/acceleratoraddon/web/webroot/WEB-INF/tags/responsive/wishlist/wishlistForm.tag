<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="org-common" tagdir="/WEB-INF/tags/addons/commerceorgaddon/responsive/common" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="saveUrl" required="true" type="java.lang.String"%>
<%@ attribute name="cancelUrl" required="true" type="java.lang.String"%>
<%@ attribute name="wishlistForm" required="true" type="com.capgemini.wishlistaddon.forms.WishlistForm"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<div class="account-section-content row">
    <form:form action="${saveUrl}" id="wishlistForm" modelAttribute="wishlistForm" method="POST">
        <div class="clearfix">
				<div class="col-xs-12 col-sm-6">
                <formElement:formInputBox
                        idKey="text.account.wishlist.name.label"
                        labelKey="text.account.manageWishlists.name.label" path="name"
                        inputCSS="text"/>
            </div>
            <div class="col-xs-12 col-sm-6">
            <formElement:formInputBox
            idKey="text.account.wishlist.description.label"
            labelKey="text.account.manageWishlists.description.label" path="description"
            /> 
            </div>
        </div>

        <div class="accountActions-bottom">
            <div class="col-sm-3 col-sm-push-9">
                <button type="submit" class="confirm btn btn-block btn-primary">
                    <spring:theme code="text.account.manageWishlists.edit.saveButton"/>
                </button>
            </div>
            <div class="col-sm-3 col-sm-push-3">
                <org-common:back cancelUrl="${cancelUrl}" displayTextMsgKey="text.account.manageWishlists.edit.cancelButton"/>
            </div>
        </div>
    </form:form>
</div>
