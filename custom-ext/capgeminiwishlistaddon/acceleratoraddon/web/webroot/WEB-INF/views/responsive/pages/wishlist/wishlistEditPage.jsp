<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="wishlist" tagdir="/WEB-INF/tags/addons/capgeminiwishlistaddon/responsive/wishlist" %>
<spring:htmlEscape defaultHtmlEscape="true"/>

<c:if test="${empty cancelUrl}">
    <spring:url value="/my-account/manage-wishlists/view" var="cancelUrl" htmlEscape="false">
        <spring:param name="code" value="${wishlistForm.code}"/>
    </spring:url>
</c:if>
<c:if test="${empty saveUrl}">
    <spring:url value="/my-account/manage-wishlists/edit" var="saveUrl" htmlEscape="false">
        <spring:param name="code" value="${wishlistForm.code}"/>
    </spring:url>
</c:if>
<template:page pageTitle="${pageTitle}">
    <div class="account-section">
        <div class="back-link">
            <a href="${cancelUrl}">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <span class="label"><spring:theme code="text.account.manageWishlists.edit.page.title"/></span>
        </div>
        <wishlist:wishlistForm cancelUrl="${cancelUrl}" saveUrl="${saveUrl}" wishlistForm="${wishlistForm}"/>
    </div>
</template:page>
