<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<spring:htmlEscape defaultHtmlEscape="true"/>

<spring:url value="/my-account/manage-wishlists/add" var="createUrl" htmlEscape="false"/>
<c:set var="searchUrl" value="/my-account/manage-wishlists?sort=${ycommerce:encodeUrl(searchPageData.pagination.sort)}"/>

<jsp:useBean id="additionalParams" class="java.util.HashMap"/>
<c:set target="${additionalParams}" property="user" value="${param.user}" />

<template:page pageTitle="${pageTitle}">
    <div class="account-section">

       <div class="account-section-header">
            <spring:theme code="text.account.${action}.title"/>

            <div class="account-section-header-add pull-right">
                <a href="${createUrl}" class="button add"><spring:theme code="text.account.${action}.addButton.displayName"/></a>
            </div>
        </div>


       <c:if test="${empty searchPageData.results}">
            <div class="account-section-content	col-md-6 col-md-push-3 content-empty">
                <spring:theme code="text.account.noentries"/>
            </div>
        </c:if>

       <c:if test="${not empty searchPageData.results}">
            <div class="account-section-content	">
                <div class="account-orderhistory-pagination">
                    <nav:pagination top="true" supportShowPaged="${isShowPageAllowed}"
                                    supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}"
                                    searchUrl="${searchUrl}"
                                    msgKey="text.account.${action}.page"
                                    numberPagesShown="${numberPagesShown}"
                                    hideRefineButton="true" additionalParams="${additionalParams}"/>
                </div>
                <div class="account-overview-table">
                    <table class="responsive-table">
                        <tr class="account-orderhistory-table-head responsive-table-head hidden-xs">
                            <th>
                                <spring:theme code="text.account.manageWishlists.column.name"/>
                            </th>
                            <th>
                                <spring:theme code="text.account.manageWishlists.column.description"/>
                            </th>
                            <th>
                                <spring:theme code="text.account.manageWishlists.column.numberOfWishlistEntries"/>
                            </th>
                            <th>
                                <spring:theme code="text.account.manageWishlists.column.lastmodifiedby"/>
                            </th>
                            <th>
                                <spring:theme code="text.account.manageWishlists.column.modifiedtime"/>
                            </th>
                        </tr>
                        <c:forEach items="${searchPageData.results}" var="result">
                            <spring:url value="/my-account/manage-wishlists/view"
                                        var="viewWishlistDetailsUrl" htmlEscape="false">
                                 <spring:param name="code" value="${result.code}"/>
                            </spring:url>
                            <tr class="responsive-table-item">
                                <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.manageWishlists.column.name"/></td>
                                <td class="responsive-table-cell">
                                    <a href="${viewWishlistDetailsUrl}">${fn:escapeXml(result.name)}</a>
                                </td>
                                <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.manageWishlists.column.description"/></td>
                                <td class="responsive-table-cell">
                                    ${fn:escapeXml(result.description)}
                                </td>
                                 <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.manageWishlists.column.numberOfWishlistEntries"/></td>
                                <td class="responsive-table-cell">
                                    ${fn:escapeXml(result.numberOfWishlistEntries)}
                                </td>
                                <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.manageWishlists.column.lastmodifiedby"/></td>
                                <td class="responsive-table-cell">
                                    ${fn:escapeXml(result.lastModifiedBy.name)}
                                </td>
                                <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.manageWishlists.column.modifiedtime"/></td>
                                   <td class="responsive-table-cell">
                                    <fmt:formatDate value="${result.dateModified}" dateStyle="medium" timeStyle="short" type="both" />
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div class="account-orderhistory-pagination">
                    <nav:pagination top="false" supportShowPaged="${isShowPageAllowed}"
                                    supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}"
                                    searchUrl="${searchUrl}"
                                    msgKey="text.account.${action}.page"
                                    numberPagesShown="${numberPagesShown}"
                                    hideRefineButton="true" additionalParams="${additionalParams}"/>
                </div>
            </div>
        </c:if>
     </div>
</template:page>
