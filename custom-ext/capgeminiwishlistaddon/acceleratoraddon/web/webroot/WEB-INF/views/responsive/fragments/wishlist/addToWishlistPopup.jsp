<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="wishlist" tagdir="/WEB-INF/tags/addons/lyonscgwishlistaddon/responsive/wishlist"%>
<spring:htmlEscape defaultHtmlEscape="true" />
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="org-common" tagdir="/WEB-INF/tags/addons/commerceorgaddon/responsive/common" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<%-- TEMP PAGE TO TEST ADD TO WISHLIST FUNCTIONALITY --%>
<spring:url value="/my-account/manage-wishlists" var="cancelUrl" htmlEscape="false"/>
<spring:url value="/wishlist/addToWishlist" var="saveUrl" htmlEscape="false"/>

<div class="wishlist_popup_msg js-wishlist-msg" style="display: none;"></div>

<form:form action="${saveUrl}" id="addToWishlistForm" modelAttribute="addToWishlistForm" method="POST">
    
    <form:input type="hidden" name="productCode" path="productCode" id="productCode" />

    <form:input type="hidden" name="create" path="create" id="create" value="true" />
                        
    <formElement:formSelectBox idKey="wishlistCode"
                               labelKey="text.wishlist.selectWishlist"
                               skipBlankMessageKey="text.company.managePermissions.selectBox.permissionType"
                               path="wishlistCode" selectCSSClass="form-control" items="${wishlistList}" />



    <h4><spring:theme code="text.wishlist.createWishlist" /></h4>
  
    <formElement:formInputBox idKey="wishlistName"
                              labelKey="text.wishlist.name" path="name"
                              inputCSS="text" />

    <formElement:formInputBox idKey="wishlistDescription"
                              labelKey="text.wishlist.description" path="description" /> 


    <div class="accountActions-bottom">
        <button type="submit" class="confirm btn btn-block btn-primary">
            <spring:theme code="text.account.manageWishlists.edit.saveButton" />
        </button>
    </div>

</form:form>
