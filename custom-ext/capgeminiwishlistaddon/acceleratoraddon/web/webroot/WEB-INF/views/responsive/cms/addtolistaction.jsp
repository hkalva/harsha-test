<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:if test="${wishlistAllowed}">
	<div class="addToList">
	    <a class="btn btn-default btn-icon js-add-to-cart glyphicon-list js-addToList" data-productcode="${product.code}">
	    	<spring:theme code="text.wishlist.addToList" />
	    </a>
	</div>
</c:if>