<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="wishlist" tagdir="/WEB-INF/tags/addons/capgeminiwishlistaddon/responsive/wishlist" %>
<%@ taglib prefix="org-common" tagdir="/WEB-INF/tags/addons/commerceorgaddon/responsive/common" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:url value="/my-account/manage-wishlists/edit" var="editWishlistDetailsUrl" htmlEscape="false">
	<spring:param name="code" value="${wishlistData.code}"/>
</spring:url>
<spring:url value="/my-account/manage-wishlists" var="backUrl" htmlEscape="false"/>
<spring:url value="/my-account/manage-wishlists/delete" var="confirmDeleteUrl" htmlEscape="false">
	<spring:param name="code" value="${wishlistData.code}"/>
</spring:url>

<template:page pageTitle="${pageTitle}">

	<div class="account-section">
		<org-common:headline url="${backUrl}" labelKey="text.account.manageWishlists.viewDetails.page.title"/>

		<div class="account-section-content">
			<div class="well well-lg well-tertiary">
				<div class="col-sm-10 col-no-padding">
					<div class="col-sm-4 item-wrapper">
						<div class="item-group">
							<span class="item-label">
								<spring:theme code="text.account.manageWishlists.name.label"/>
							</span>
							<span class="item-value">
								${fn:escapeXml(wishlistData.name)}
							</span>
						</div>
						<div class="item-group">
							<span class="item-label">
								<spring:theme code="text.account.manageWishlists.description.label"/>
							</span>
							<span class="item-value">
								${fn:escapeXml(wishlistData.description)}
							</span>
						</div>
					</div>
				</div>
	
				<div class="col-sm-2 item-action">
					<a href="${editWishlistDetailsUrl}" class="button edit btn btn-block btn-primary">
						<spring:theme code="text.account.manageWishlists.edit.button.displayName"/>
					</a>
				</div>
			</div>

		<h2 class="wishlist_heading">
			<spring:theme code="text.account.manageWishlists.products.label"/>
		</h2>
   
		<c:if test="${not empty wishlistData.wishlistEntries}">

			<div class="wishlist_products">

				<c:forEach items="${wishlistData.wishlistEntries}" var="wishlistEntryData">
					
					<c:set value="${wishlistEntryData.product}" var="wishlistProduct"/>
					<c:url value="${wishlistProduct.url}" var="productUrl"/>

					<div class="wishlist_product" data-product-id="${wishlistProduct.code}" data-product-qty="${wishlistEntryData.desired}">

						<spring:url value="/my-account/manage-wishlists/removeEntry" var="removeEntryUrl" htmlEscape="false">
							<spring:param name="code" value="${wishlistData.code}"/>
							<spring:param name="productCode" value="${wishlistEntryData.product.code}"/>
						</spring:url>

						<a class="wishlist_product_img" href="${productUrl}" title="${wishlistProduct.name}">
							<product:productPrimaryImage product="${wishlistProduct}" format="thumbnail"/>
						</a>

						<div class="wishlist_product_info">
							<a class="wishlist_product_name" href="${productUrl}" title="${wishlistProduct.name}">
								${fn:escapeXml(wishlistProduct.name)}
							</a>

							<span class="wishlist_product_code"><span class="product_id">ID:</span> ${fn:escapeXml(wishlistProduct.code)}</span>

							<c:if test="${not empty product.summary}">
								<span class="wishlist_product_summary">${wishlistProduct.summary}</span>
							</c:if>
						</div>

						<div class="wishlist_product_price">
							<product:productListerItemPrice product="${wishlistProduct}"/>
							<product:productPromotionSection product="${wishlistProduct}"/><br/>
						</div>

						<div class="wishlist_product_actions">

							<div class="wishlist_product_addToCart">
								<c:url var="addToCartUrl" value="/cart/add" />

								<c:set var="isPurchaseable" value="false" />

								<c:if test="${wishlistProduct.purchasable and wishlistProduct.stock.stockLevelStatus.code ne 'outOfStock' }">
									<c:set var="isPurchaseable" value="true" />
								</c:if>

								<form:form method="post" class="js-wishlist-addToCartForm wishlist-addToCartForm" action="${addToCartUrl}">
									<input type="hidden" name="productCodePost" value="${wishlistEntryData.product.code}" />

									<c:if test="${isPurchaseable}">
										<input type="hidden" maxlength="3" size="1" id="qty" name="qty" value="${wishlistEntryData.desired}" />
									</c:if>

									<button class="btn btn-primary btn-icon glyphicon-shopping-cart js-wishlist-addToCart">
										<spring:theme code="basket.add.to.basket"/>
									</button>
								</form:form>

							</div>

							<form:form action="${removeEntryUrl}" class="url-holder wishlist_product_remove" method="POST">
								<button type="submit" class="btn btn-default">
									<spring:theme code="text.account.manageWishlists.remove.button.displayName"/>
								</button>
							</form:form>
						</div>

					</div>
				</c:forEach>
			</div> 
	   
		</c:if>

			<div class="accountActions-link">
				<c:url value="/my-account/manage-wishlists/addWishlistToCart" var="wishlistAddToCartAll" />

				<div class="wishlist_bulkadd_btn">
					<span class="btn btn-primary btn-icon glyphicon-shopping-cart js-wishlist-addToCart-all" data-addToCart-Url="${wishlistAddToCartAll}"><spring:theme code="text.account.manageWishlists.addAllToCart.button.displayName"/></span>
				</div>

				<div class="js-action-confirmation-modal disable-link">
					<a href="#" data-action-confirmation-modal-title="<spring:theme code="text.account.manageWishlists.delete.confirm"/>" data-action-confirmation-modal-id="delete">
						<spring:theme code="text.account.manageWishlists.delete.button.displayName"/>
					</a>
				</div>

				<wishlist:actionConfirmationModal id="delete" targetUrl="${confirmDeleteUrl}" messageKey="text.account.manageWishlists.delete.confirm.message"/>
			</div>

		</div>

		<div id="addToCartTitle" class="display-none">
			<div class="add-to-cart-header">
				<div class="headline">
					<span class="headline-text"><spring:theme code="basket.added.to.basket"/></span>
				</div>
			</div>
		</div>
	</div>
</template:page>