<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="wishlist" tagdir="/WEB-INF/tags/addons/capgeminiwishlistaddon/responsive/wishlist"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<spring:url value="/my-account/manage-wishlists" var="cancelUrl" htmlEscape="false"/>
<spring:url value="/my-account/manage-wishlists/add" var="saveUrl" htmlEscape="false"/>

<template:page pageTitle="${pageTitle}">
    <div class="account-section">
        <div class="back-link">
            <a href="${cancelUrl}">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <span class="label"><spring:theme code="text.account.manageWishlists.add.page.title"/></span>
        </div>
			<wishlist:wishlistForm cancelUrl="${cancelUrl}" saveUrl="${saveUrl}" wishlistForm="${wishlistForm}" />
	</div>
</template:page>