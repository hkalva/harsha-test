<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="wishlist" tagdir="/WEB-INF/tags/addons/lyonscgwishlistaddon/responsive/wishlist"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<spring:htmlEscape defaultHtmlEscape="true" />

<spring:url value="/login" var="loginUrl" htmlEscape="false"/>

<p><a href=${loginUrl}><spring:theme code="text.wishlist.loginWishlist" /></a></p>