/**
 *
 */
package com.capgemini.wishlistaddon.controllers.pages;

import de.hybris.platform.acceleratorfacades.product.data.ProductWrapperData;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.capgemini.wishlistaddon.controllers.LyonscgwishlistaddonControllerConstants;
import com.capgemini.wishlistaddon.forms.AddToCartWishlistForm;
import com.capgemini.wishlistaddon.forms.WishlistForm;
import com.capgemini.wishlistaddon.forms.validator.WishlistFormValidator;
import com.capgemini.wishlistservices.data.WishlistData;
import com.capgemini.wishlistservices.facades.WishlistFacade;



/**
 * Wishlist Controller to handle the requests pertaining to Wishlist such as Add, Edit, Manage Wishlist - Paginated
 * format, Delete wishlist & Remove wishlist entry.
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/my-account/manage-wishlists")
public class WishlistPageController extends AbstractSearchPageController
{
	private static final Logger LOG = LogManager.getLogger(WishlistPageController.class);
	private static final String WISHLIST_CMS_PAGE = "wishlist";
	private static final String WISHLIST_ADD_EDIT_CMS_PAGE = "add-edit-wishlist";
	private static final String WISHLIST_VIEW_CMS_PAGE = "view-wishlist";
	private static final String BREADCRUMBS_ATTR = "breadcrumbs";
	private static final String TEXT_ACCOUNT_WISHLIST = "text.account.manageWishlists.page";
	private static final String TEXT_ACCOUNT_WISHLIST_ADD = "text.account.manageWishlists.add.page";
	private static final String TEXT_ACCOUNT_WISHLIST_EDIT = "text.account.manageWishlists.edit.page";
	private static final String TEXT_ACCOUNT_WISHLIST_VIEW = "text.account.manageWishlists.view.page";
	private static final String TEXT_FORM_GLOBAL_ERROR = "form.global.error";
	private static final String TEXT_ERROR_WISHLIST_NOTFOUND = "text.error.wishlist.notfound";
	private static final String TEXT_ERROR_WISHLIST_UPDATE_ERROR = "tet.error.wishlist.update.error";
	private static final String TEXT_ERROR_WISHLIST_ADD_ERROR = "text.error.wishlist.addnew.error";
	private static final String TEXT_ERROR_WISHLIST_REMOVE_ENTRY_ERROR = "text.error.wishlist.remove.entry.error";
	private static final String TEXT_ERROR_WISHLIST_DELETE_ERROR = "text.error.wishlist.delete.error";
	private static final String TEXT_ERROR_WISHLIST_ADD_TO_CART_ERROR = "text.error.wishlist.add.to.cart.error";

	private static final String TEXT_CONF_WISHLIST_CREATED = "text.confirmation.manageWishlists.wishlist.created";
	private static final String TEXT_CONF_WISHLIST_UPDATED = "text.confirmation.manageWishlists.wishlist.updated";
	private static final String TEXT_CONF_WISHLIST_DELETED = "text.confirmation.manageWishlists.wishlist.deleted";

	private static final String MANAGE_WISHLIST_URL = "/my-account/manage-wishlists";
	private static final String MANAGE_WISHLIST_VIEW_URL = "/my-account/manage-wishlists/view/?code=%s";
	private static final String MANAGE_WISHLIST_EDIT_URL = "/my-account/manage-wishlists/edit/?code=%s";
	private static final String MANAGE_WISHLIST_ADD_URL = "/my-account/manage-wishlists/add";
	private static final String REDIRECT_TO_WISHLIST_DETAILS_PAGE = REDIRECT_PREFIX + MANAGE_WISHLIST_VIEW_URL;
	private static final String REDIRECT_TO_WISHLIST_PAGE = REDIRECT_PREFIX + MANAGE_WISHLIST_URL;

	@Resource
	private WishlistFacade wishlistFacade;
	@Resource
	private WishlistFormValidator wishlistFormValidator;
	@Resource(name = "wishlistBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder wishlistBreadcrumbBuilder;
	@Resource
	private CartFacade cartFacade;

	/**
	 * To retrieve a SearchPageData Object of the list of Wishlists for a user, with pagination specific attributes &
	 * Sort operations.
	 *
	 * @param page
	 *           page number of page being viewed
	 * @param showMode
	 *           - possible values - All, Page
	 * @param sortCode
	 *           - field name on which Sort is to be performed - values - name, creationtime, modifiedtime
	 * @param model
	 *           model object
	 * @return Wishlist page path
	 * @throws CMSItemNotFoundException
	 *            throws CMSItemNotFoundException exception when CMS page content id is not found
	 */
	@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	public String manageWishlists(@RequestParam(value = "page", defaultValue = "0") final int page, @RequestParam(value = "show",
			defaultValue = "Page") final AbstractSearchPageController.ShowMode showMode, @RequestParam(value = "sort",
			defaultValue = Wishlist2Model.NAME) final String sortCode, final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(WISHLIST_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(WISHLIST_CMS_PAGE));

		// Handle paged search results
		final PageableData pageableData = createPageableData(page, getSearchPageSize(), sortCode, showMode);
		final SearchPageData<WishlistData> searchPageData = getWishlistFacade().getPagedWishlists(pageableData);
		populateModel(model, searchPageData, showMode);

		//Print the Wishlists
		searchPageData.getResults().stream().forEach(e -> LOG.debug(e.getName()));

		model.addAttribute("action", "manageWishlists");
		model.addAttribute(BREADCRUMBS_ATTR, wishlistBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_WISHLIST));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return LyonscgwishlistaddonControllerConstants.Views.Pages.Wishlist.WishlistPage;
	}

	/**
	 * To display the Add New Wish list page.
	 *
	 * @param model
	 *           model object
	 * @return Wish list page to be rendered
	 * @throws CMSItemNotFoundException
	 *            throws exception when CMS Page content is not found
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getAddWishlistPage(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(WISHLIST_ADD_EDIT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(WISHLIST_ADD_EDIT_CMS_PAGE));

		model.addAttribute(new WishlistForm());

		model.addAttribute(BREADCRUMBS_ATTR, createWishlistAddPageBreadcrumb());
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		return LyonscgwishlistaddonControllerConstants.Views.Pages.Wishlist.WishlistAddPage;
	}

	/**
	 * To create a new Wish list.
	 *
	 * @param wishlistForm
	 *           Wish list form passed from the JSP
	 * @param bindingResult
	 * @param model
	 *           model object
	 * @param redirectModel
	 * @return String Path to the Details page with the wishlist Code as a parameter
	 * @throws CMSItemNotFoundException
	 *            throws exception when CMS Page content is not found
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@RequireHardLogIn
	public String addNewWishlist(@Valid final WishlistForm wishlistForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(WISHLIST_ADD_EDIT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(WISHLIST_ADD_EDIT_CMS_PAGE));

		//Validate Form
		getWishlistFormValidator().validate(wishlistForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			model.addAttribute(wishlistForm);
			GlobalMessages.addErrorMessage(model, TEXT_FORM_GLOBAL_ERROR);
			return LyonscgwishlistaddonControllerConstants.Views.Pages.Wishlist.WishlistAddPage;
		}

		WishlistData wishlistData = populateWishlistDataFromForm(wishlistForm);

		try
		{
			//Call the Create Method
			wishlistData = getWishlistFacade().createWishlist(wishlistData);
			model.addAttribute("wishlistData", wishlistData);
		}
		catch (final Exception e)
		{
			model.addAttribute(wishlistForm);
			GlobalMessages.addErrorMessage(model, TEXT_ERROR_WISHLIST_ADD_ERROR);
			return LyonscgwishlistaddonControllerConstants.Views.Pages.Wishlist.WishlistAddPage;
		}

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, TEXT_CONF_WISHLIST_CREATED);
		return String.format(REDIRECT_TO_WISHLIST_DETAILS_PAGE, wishlistData.getCode());
	}

	/**
	 * To display the details of a specific Wish list based on the code passed as an input.
	 *
	 * @param wishlistCode
	 *           code to uniquely identify a Wish list
	 * @param model
	 *           model object
	 * @return path to the Details Page
	 * @throws CMSItemNotFoundException
	 *            Exception thrown when CMS Page Content is not found
	 */
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@RequireHardLogIn
	public String viewWishlistDetails(@RequestParam("code") final String wishlistCode, final Model model)
			throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(WISHLIST_VIEW_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(WISHLIST_VIEW_CMS_PAGE));
		WishlistData wishlistData = null;
		try
		{
			wishlistData = getWishlistFacade().getWishlistDetails(wishlistCode);
			model.addAttribute("wishlistData", wishlistData);
			model.addAttribute(new AddToCartWishlistForm());
		}
		catch (final Exception e)
		{
			LOG.warn("Exception while retrieving the wishlist details " + e);
			GlobalMessages.addErrorMessage(model, TEXT_ERROR_WISHLIST_NOTFOUND);
			model.addAttribute(BREADCRUMBS_ATTR, createManageWishlistPageBreadcrumb());
			return LyonscgwishlistaddonControllerConstants.Views.Pages.Wishlist.WishlistPage;
		}

		model.addAttribute(BREADCRUMBS_ATTR, createWishlistViewPageBreadcrumb(wishlistCode, wishlistData.getName()));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return LyonscgwishlistaddonControllerConstants.Views.Pages.Wishlist.WishlistDetailsPage;
	}

	/**
	 * To retrieve the Edit Wishlist page.
	 *
	 * @param wishlistCode
	 *           code to identify Wishlist
	 * @param model
	 *           model object
	 * @return View page
	 * @throws CMSItemNotFoundException
	 *            Exception thrown when CMS Page Content is not found
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getEditWishlistPage(@RequestParam("code") final String wishlistCode, final Model model)
			throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(WISHLIST_ADD_EDIT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(WISHLIST_ADD_EDIT_CMS_PAGE));
		WishlistData wishlistData = null;
		try
		{
			wishlistData = getWishlistFacade().getWishlistDetails(wishlistCode);
			if (!model.containsAttribute("wishlistForm"))
			{
				final WishlistForm wishlistForm = new WishlistForm();
				wishlistForm.setCode(wishlistData.getCode());
				wishlistForm.setName(wishlistData.getName());
				wishlistForm.setDescription(wishlistData.getDescription());
				model.addAttribute(wishlistForm);
			}
		}
		catch (final Exception e)
		{
			LOG.warn("Exception while retrieving the wishlist details " + e);
			GlobalMessages.addErrorMessage(model, TEXT_ERROR_WISHLIST_NOTFOUND);
			wishlistData = getWishlistFacade().getWishlistDetails(wishlistCode);
			model.addAttribute("wishlistData", wishlistData);
			model.addAttribute(BREADCRUMBS_ATTR, createWishlistViewPageBreadcrumb(wishlistCode, wishlistData.getName()));
			return LyonscgwishlistaddonControllerConstants.Views.Pages.Wishlist.WishlistDetailsPage;
		}

		model.addAttribute("breadcrumbs", createWishlistEditPageBreadcrumb(wishlistCode, wishlistData.getName()));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return LyonscgwishlistaddonControllerConstants.Views.Pages.Wishlist.WishlistEditPage;
	}

	/**
	 * To submit the Edited details of a Wishlist.
	 *
	 * @param wishlistForm
	 *           Form with the details from the front end
	 * @param bindingResult
	 *           Binding result object
	 * @param model
	 *           model object
	 * @param redirectModel
	 *           redirect model for setting redirecting attributes
	 * @return View Page
	 * @throws CMSItemNotFoundException
	 *            Exception thrown when CMS Content page is not found for the Id passed
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@RequireHardLogIn
	public String editWishlist(@Valid final WishlistForm wishlistForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(WISHLIST_ADD_EDIT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(WISHLIST_ADD_EDIT_CMS_PAGE));

		getWishlistFormValidator().validate(wishlistForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			model.addAttribute(wishlistForm);
			GlobalMessages.addErrorMessage(model, TEXT_FORM_GLOBAL_ERROR);
			return getEditWishlistPage(wishlistForm.getCode(), model);
		}

		final WishlistData wishlistData = populateWishlistDataFromForm(wishlistForm);

		try
		{
			getWishlistFacade().updateWishlistDetails(wishlistData);
		}
		catch (final Exception e)
		{
			LOG.warn("Exception while saving the wishlist details " + e);
			model.addAttribute(wishlistForm);
			GlobalMessages.addErrorMessage(model, TEXT_ERROR_WISHLIST_UPDATE_ERROR);
			return getEditWishlistPage(wishlistForm.getCode(), model);
		}

		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, TEXT_CONF_WISHLIST_UPDATED);
		return String.format(REDIRECT_TO_WISHLIST_DETAILS_PAGE, urlEncode(wishlistData.getCode()));
	}

	/**
	 * To submit the delete operation for a specific wishlist code.
	 *
	 * @param wishlistCode
	 *           code to uniquely identify Wishlist
	 * @param model
	 *           model object
	 * @param redirectModel
	 *           redirect model to pass the redirect attributes
	 * @return View page to redirect to
	 * @throws CMSItemNotFoundException
	 *            Exception thrown when CMS Content page is not found for the Id passed
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@RequireHardLogIn
	public String deleteWishlist(@RequestParam(value = "code") final String wishlistCode, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(WISHLIST_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(WISHLIST_CMS_PAGE));

		try
		{
			getWishlistFacade().deleteWishlist(wishlistCode);
		}
		catch (final IllegalArgumentException iae)
		{
			LOG.error("Exception while deleting the wishlist : Wishlist Cannot be null or invalid " + iae);
			model.addAttribute(BREADCRUMBS_ATTR, createWishlistViewPageBreadcrumb(wishlistCode, ""));
			GlobalMessages.addErrorMessage(model, TEXT_ERROR_WISHLIST_DELETE_ERROR);
			return LyonscgwishlistaddonControllerConstants.Views.Pages.Wishlist.WishlistDetailsPage;
		}
		catch (final Exception e)
		{
			LOG.error("Exception while deleting the wishlist " + e);
			final WishlistData wishlistData = getWishlistFacade().getWishlistDetails(wishlistCode);
			model.addAttribute("wishlistData", wishlistData);
			model.addAttribute(BREADCRUMBS_ATTR, createWishlistViewPageBreadcrumb(wishlistCode, wishlistData.getName()));
			GlobalMessages.addErrorMessage(model, TEXT_ERROR_WISHLIST_DELETE_ERROR);
			return LyonscgwishlistaddonControllerConstants.Views.Pages.Wishlist.WishlistDetailsPage;
		}

		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, TEXT_CONF_WISHLIST_DELETED);

		return REDIRECT_TO_WISHLIST_PAGE;
	}

	/**
	 * Method to remove WishlistEntry from a Wishlist.
	 *
	 * @param wishlistCode
	 *           code to uniquely identify the Wishlist
	 * @param productCode
	 *           Product code that needs to be added to the wishlist
	 * @param model
	 *           model object
	 * @return HTML View page of Details page
	 * @throws CMSItemNotFoundException
	 *            Exception thrown when CMS Content page is not found for the Id passed
	 */
	@RequestMapping(value = "/removeEntry", method = RequestMethod.POST)
	@RequireHardLogIn
	public String removeWishlistEntry(@RequestParam(value = "code") final String wishlistCode,
			@RequestParam(value = "productCode") final String productCode, final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(WISHLIST_VIEW_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(WISHLIST_VIEW_CMS_PAGE));

		try
		{
			getWishlistFacade().removeProductFromWishlist(wishlistCode, productCode);
		}
		catch (final IllegalArgumentException iae)
		{
			if (iae.getMessage().contains("wishlistModel"))
			{
				LOG.error("Exception while removing WishlistEntry from the wishlist : Wishlist not found :" + iae);
				model.addAttribute(BREADCRUMBS_ATTR, createWishlistViewPageBreadcrumb(wishlistCode, ""));
				GlobalMessages.addErrorMessage(model, TEXT_ERROR_WISHLIST_REMOVE_ENTRY_ERROR);
				return LyonscgwishlistaddonControllerConstants.Views.Pages.Wishlist.WishlistDetailsPage;
			}
			else
			{
				LOG.error("Exception while removing WishlistEntry from the wishlist : ProductModel not found " + iae);
				final WishlistData wishlistData = getWishlistFacade().getWishlistDetails(wishlistCode);
				model.addAttribute("wishlistData", wishlistData);
				model.addAttribute(BREADCRUMBS_ATTR, createWishlistViewPageBreadcrumb(wishlistCode, wishlistData.getName()));
				GlobalMessages.addErrorMessage(model, TEXT_ERROR_WISHLIST_REMOVE_ENTRY_ERROR);
				return LyonscgwishlistaddonControllerConstants.Views.Pages.Wishlist.WishlistDetailsPage;
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception while removing WishlistEntry from the wishlist " + e);
			final WishlistData wishlistData = getWishlistFacade().getWishlistDetails(wishlistCode);
			model.addAttribute("wishlistData", wishlistData);
			model.addAttribute(BREADCRUMBS_ATTR, createWishlistViewPageBreadcrumb(wishlistCode, wishlistData.getName()));
			GlobalMessages.addErrorMessage(model, TEXT_ERROR_WISHLIST_REMOVE_ENTRY_ERROR);
			return LyonscgwishlistaddonControllerConstants.Views.Pages.Wishlist.WishlistDetailsPage;
		}

		return String.format(REDIRECT_TO_WISHLIST_DETAILS_PAGE, urlEncode(wishlistCode));
	}

	/**
	 * This method takes AddToCartWishlistForm as an input with the list of Cart Entries that need to be added to Cart.
	 * In Front end, set only the wishlist entries that has to be added to cart as cart Entries in this form.
	 *
	 * @param addToCartWishlistForm
	 *           Form which holds the wishlist entries to be added to the cart
	 * @param model
	 *           model object
	 * @return Redirect to the Add To Cart Popup
	 */
	@RequestMapping(value = "/addWishlistToCart", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequireHardLogIn
	public final String addWishlistToCart(@RequestBody final AddToCartWishlistForm addToCartWishlistForm, final Model model)
	{
		final List<CartModificationData> modificationDataList = new ArrayList();
		final List<ProductWrapperData> productWrapperDataList = new ArrayList();
		addToCartWishlistForm.getCartEntries().forEach(cartEntry -> {
			String errorMsg = StringUtils.EMPTY;
			final String sku = !isValidProductEntry(cartEntry) ? StringUtils.EMPTY : cartEntry.getProduct().getCode();
			if (StringUtils.isEmpty(sku))
			{
				errorMsg = "text.wishlist.product.code.invalid";
			}
			else if (!isValidQuantity(cartEntry))
			{
				errorMsg = "text.wishlist.product.quantity.invalid";
			}
			else
			{
				errorMsg = addEntryToCart(modificationDataList, cartEntry, false);
			}

			if (StringUtils.isNotEmpty(errorMsg))
			{
				productWrapperDataList.add(createProductWrapperData(sku, errorMsg));
			}
		});

		if (CollectionUtils.isNotEmpty(productWrapperDataList))
		{
			GlobalMessages.addErrorMessage(model, TEXT_ERROR_WISHLIST_ADD_TO_CART_ERROR);
			/* Add to Cart popup page has slots to render quick Order Error Data & Message, hence utilizing the same. */
			model.addAttribute("quickOrderErrorData", productWrapperDataList);
			model.addAttribute("quickOrderErrorMsg", "basket.wishlist.error");
		}

		if (CollectionUtils.isNotEmpty(modificationDataList))
		{
			model.addAttribute("modifications", modificationDataList);
		}

		return LyonscgwishlistaddonControllerConstants.Views.Fragments.Cart.AddToCartPopup;
	}

	/**
	 * Create a ProductWrapperData object holding the product sku id and the error message.
	 *
	 * @param sku
	 *           Sku Id
	 * @param errorMsg
	 *           Error message to be set
	 * @return populated ProductWrapperData
	 */
	protected ProductWrapperData createProductWrapperData(final String sku, final String errorMsg)
	{
		final ProductWrapperData productWrapperData = new ProductWrapperData();
		final ProductData productData = new ProductData();
		productData.setCode(sku);
		productWrapperData.setProductData(productData);
		productWrapperData.setErrorMsg(errorMsg);
		return productWrapperData;
	}

	/**
	 * Checks if Debug level is enabled and then writes to log.
	 *
	 * @param ex
	 */
	protected void logDebugException(final Exception ex)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug(ex);
		}
	}

	/**
	 * addEntryToCart method processes every OrderEntryData/cart entry data object present in the form. It invokes the
	 * Cart Facade's addToCart method to add the sku mentioned in the OrderEntryData object to cart.
	 *
	 * @param modificationDataList
	 *           Output of addToCart() method is cartModificationData, which is added to this list for further processing
	 * @param cartEntry
	 *           OrderEntryData object
	 * @param isReducedQtyError
	 * @return Returns a String of error messages
	 */
	protected String addEntryToCart(final List<CartModificationData> modificationDataList, final OrderEntryData cartEntry,
			final boolean isReducedQtyError)
	{
		String errorMsg = StringUtils.EMPTY;
		try
		{
			final long qty = cartEntry.getQuantity().longValue();
			final CartModificationData cartModificationData = cartFacade.addToCart(cartEntry.getProduct().getCode(), qty);
			if (cartModificationData.getQuantityAdded() == 0L)
			{
				errorMsg = "basket.information.quantity.noItemsAdded." + cartModificationData.getStatusCode();
			}
			else if (cartModificationData.getQuantityAdded() < qty && isReducedQtyError)
			{
				errorMsg = "basket.information.quantity.reducedNumberOfItemsAdded." + cartModificationData.getStatusCode();
			}

			modificationDataList.add(cartModificationData);

		}
		catch (final CommerceCartModificationException ex)
		{
			errorMsg = "basket.error.occurred";
			logDebugException(ex);
		}
		return errorMsg;
	}

	/**
	 * Verifies that the product code is not empty.
	 *
	 * @param cartEntry
	 *           OrderEntryData
	 * @return true or false
	 */
	protected boolean isValidProductEntry(final OrderEntryData cartEntry)
	{
		return cartEntry.getProduct() != null && StringUtils.isNotBlank(cartEntry.getProduct().getCode());
	}

	/**
	 * Verifies that the Product's Cart entry quantity is not null & greater or equal to 1.
	 *
	 * @param cartEntry
	 *           OrderEntryData
	 * @return true or false
	 */
	protected boolean isValidQuantity(final OrderEntryData cartEntry)
	{
		return cartEntry.getQuantity() != null && cartEntry.getQuantity().longValue() >= 1L;
	}

	/**
	 * To populate the WishlistData object with the values from the form.
	 *
	 * @param wishlistForm
	 */
	private WishlistData populateWishlistDataFromForm(final WishlistForm wishlistForm)
	{
		final WishlistData wishlistData = new WishlistData();
		wishlistData.setName(wishlistForm.getName());
		if (StringUtils.isNotEmpty(wishlistForm.getDescription()))
		{
			wishlistData.setDescription(wishlistForm.getDescription());
		}
		if (StringUtils.isNoneEmpty(wishlistForm.getCode()))
		{
			wishlistData.setCode(wishlistForm.getCode());
		}

		return wishlistData;

	}

	/**
	 * To create the breadcrumb for the View WIshlist Page.
	 *
	 * @param wishlistCode
	 *           Code to identify wishlist
	 * @param wishlistName
	 *           Name of Wishlist
	 * @return list of breadcrumbs to be displayed on the screen
	 */
	private List<Breadcrumb> createWishlistViewPageBreadcrumb(final String wishlistCode, final String wishlistName)
	{
		final List<Breadcrumb> breadcrumbs = createManageWishlistPageBreadcrumb();
		breadcrumbs.add(new Breadcrumb(String.format(MANAGE_WISHLIST_VIEW_URL, wishlistCode), getMessageSource().getMessage(
				TEXT_ACCOUNT_WISHLIST_VIEW, new Object[]
				{ wishlistName }, "View Wishlist Details {0}", getI18nService().getCurrentLocale()), null));
		return breadcrumbs;
	}

	/**
	 * To create the bread crumb for Manage Wishlist page with link.
	 *
	 * @return breadcrumb list
	 */
	private List<Breadcrumb> createManageWishlistPageBreadcrumb()
	{
		final List<Breadcrumb> breadcrumbs = wishlistBreadcrumbBuilder.getBreadcrumbs(null);
		breadcrumbs.add(new Breadcrumb(MANAGE_WISHLIST_URL, getMessageSource().getMessage(TEXT_ACCOUNT_WISHLIST, null,
				getI18nService().getCurrentLocale()), null));
		return breadcrumbs;
	}

	/**
	 * To create the breadcrumbs for the Wishlist Edit Page.
	 *
	 * @param wishlistCode
	 *           code to identify wishlist
	 * @param wishlistName
	 *           Name of wishlist
	 * @return List of breadcrumbs
	 */
	private List<Breadcrumb> createWishlistEditPageBreadcrumb(final String wishlistCode, final String wishlistName)
	{
		final List<Breadcrumb> breadcrumbs = createManageWishlistPageBreadcrumb();
		breadcrumbs.add(new Breadcrumb(String.format(MANAGE_WISHLIST_EDIT_URL, urlEncode(wishlistCode)), getMessageSource()
				.getMessage(TEXT_ACCOUNT_WISHLIST_EDIT, new Object[]
				{ wishlistName }, "Edit Wishlist {0}", getI18nService().getCurrentLocale()), null));
		return breadcrumbs;
	}

	/**
	 * To create a bread crumb list for Add Wish list Page.
	 *
	 * @return List of Bread crumbs
	 */
	private List<Breadcrumb> createWishlistAddPageBreadcrumb()
	{
		final List<Breadcrumb> breadcrumbs = createManageWishlistPageBreadcrumb();
		breadcrumbs.add(new Breadcrumb(MANAGE_WISHLIST_ADD_URL, getMessageSource().getMessage(TEXT_ACCOUNT_WISHLIST_ADD,
				new Object[] {}, "Add Wishlist", getI18nService().getCurrentLocale()), null));
		return breadcrumbs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController#getSearchPageSize()
	 * 
	 * Overriding the Search Page size property locally in the extension
	 */
	@Override
	protected int getSearchPageSize()
	{
		return getSiteConfigService().getInt("capgeminiwishlistaddon.search.pageSize", 10);
	}

	/**
	 * @return the wishlistFormValidator
	 */
	public WishlistFormValidator getWishlistFormValidator()
	{
		return wishlistFormValidator;
	}

	/**
	 * @param wishlistFormValidator
	 *           the wishlistFormValidator to set
	 */
	public void setWishlistFormValidator(final WishlistFormValidator wishlistFormValidator)
	{
		this.wishlistFormValidator = wishlistFormValidator;
	}

	/**
	 * @return the wishlistFacade
	 */
	public WishlistFacade getWishlistFacade()
	{
		return wishlistFacade;
	}

	/**
	 * @param wishlistFacade
	 *           the wishlistFacade to set
	 */
	public void setWishlistFacade(final WishlistFacade wishlistFacade)
	{
		this.wishlistFacade = wishlistFacade;
	}
}
