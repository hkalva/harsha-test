/**
 *
 */
package com.capgemini.wishlistaddon.forms;

import javax.validation.constraints.Size;


/**
 * Add to Wish list Form - POJO class.
 */
public class AddToWishlistForm
{
    /**
     * the wishlist code.
     */
	private String wishlistCode;

	/**
	 * the product code.
	 */
	private String productCode;

	/**
	 * the create flag.
	 */
	private boolean create;

	/**
	 * the wishlist name.
	 */
	private String name;

	/**
	 * the wishlist description.
	 */
	private String description;

    /**
     * constant for name max size.
     */
	private static final int WISHLIST_NAME_MAX_LIMIT = 255;

    /**
     * constant for description max size.
     */
	private static final int WISHLIST_DESC_MAX_LIMIT = 4000;



	/**
	 * Getter method for name.
	 *
	 * @return the name
	 */
	@Size(max = WISHLIST_NAME_MAX_LIMIT, message = "{text.validation.wishlist.name.maxlimit}")
	public String getName()
	{
		return name;
	}

	/**
	 * Setter method for name.
	 *
	 * @param name
	 *           the name to set
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * Getter method for description.
	 *
	 * @return the description
	 */
	@Size(max = WISHLIST_DESC_MAX_LIMIT, message = "{text.validation.wishlist.description.maxlimit}")
	public String getDescription()
	{
		return description;
	}

	/**
	 * Setter method for description.
	 *
	 * @param description
	 *           the description to set
	 */
	public void setDescription(final String description)
	{
		this.description = description;
	}

	/**
	 * Getter method for Wishlist code.
	 *
	 * @return the wishlistCode
	 */
	public String getWishlistCode()
	{
		return wishlistCode;
	}

	/**
	 * Setter method to set Wishlist code.
	 *
	 * @param wishlistCode
	 *           the wishlistCode to set
	 */
	public void setWishlistCode(final String wishlistCode)
	{
		this.wishlistCode = wishlistCode;
	}

	/**
	 * Getter method to get product code.
	 *
	 * @return the productCode
	 */
	public String getProductCode()
	{
		return productCode;
	}

	/**
	 * Setter method to set product code.
	 *
	 * @param productCode
	 *           the productCode to set
	 */
	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}

	/**
	 * Boolean flag to identify if its to create a new wishlist or use existing wishlist in the process of adding a
	 * wishlist entry.
	 *
	 * @return the create
	 */
	public boolean isCreate()
	{
		return create;
	}

	/**
	 * Method to set the isCreate boolean flag.
	 *
	 * @param create
	 *           the create to set
	 */
	public void setCreate(final boolean create)
	{
		this.create = create;
	}

}
