/**
 *
 */
package com.capgemini.wishlistaddon.user.utils;

import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.util.CookieGenerator;


/**
 * Implementation of the RequireHardLoginEvaluator class as a helper class, so as to utilize its logic as a function
 * call for evaluation within classes, instead of annotation.
 */
public class UserHelper
{
    /**
     * the logger.
     */
	private static final Logger LOG = LoggerFactory.getLogger(UserHelper.class);

	/**
	 * the user service.
	 */
	@Resource
	private UserService userService;

	/**
	 * the session service.
	 */
	@Resource
	private SessionService sessionService;

	/**
	 * the guid cookie generator.
	 */
	@Resource(name = "guidCookieGenerator")
	private CookieGenerator cookieGenerator;

	/**
	 * the cart service.
	 */
	@Resource
	private CartService cartService;

	/**
	 * constant for request.
	 */
	private static final String REQUEST_MODEL_ATTRIBUTE_NAME = "request";

	/**
	 * constant for response.
	 */
	private static final String RESPONSE_MODEL_ATTRIBUTE_NAME = "response";

	/**
	 * constant for secure guid key.
	 */
	private static final String SECURE_GUID_SESSION_KEY = "acceleratorSecureGUID";
	
	/**
	 * constant for anonymous checkout.
	 */
	private static final String ANONYMOUS_CHECKOUT = "anonymous_checkout";

    /**
     * constant for anonymous checkout guid.
     */
	private static final String ANONYMOUS_CHECKOUT_GUID = "anonymous_checkout_guid";

	/**
	 * Method to implement the conditional check if there is a valid user session similar to the
	 * RequireHardLoginEvaluator Class.
	 *
	 * @param model
	 *           Model Object.
	 * @return boolean true if a Hard Login is required and false if not.
	 */
	public boolean isRequireHardLogin(final Model model)
	{
		final HttpServletRequest request = (HttpServletRequest) model.asMap().get(REQUEST_MODEL_ATTRIBUTE_NAME);
		final HttpServletResponse response = (HttpServletResponse) model.asMap().get(RESPONSE_MODEL_ATTRIBUTE_NAME);
		final String guid = (String) request.getSession().getAttribute(SECURE_GUID_SESSION_KEY);
		boolean result = true;

		if ((!getUserService().isAnonymousUser(getUserService().getCurrentUser()) || checkForAnonymousCheckout())
				&& checkForGUIDCookie(request, response, guid))
		{
			result = false;
		}

		if (result)
		{
			LOG.warn((guid == null ? "missing secure token in session" : "no matching guid cookie") + ", login required");
		}

		return result;
	}

	/**
	 * Checks for a valid Cookie.
	 *
	 * @param request
	 *           HttpServletRequest.
	 * @param response
	 *           HttpServletResponse.
	 * @param guid
	 *           SECURE_GUID_SESSION_KEY string present within the session.
	 * @return boolean true or false based on a valid cookie is present or not.
	 */
	protected boolean checkForGUIDCookie(final HttpServletRequest request, final HttpServletResponse response, final String guid)
	{
		if (guid != null && request.getCookies() != null)
		{
			final String guidCookieName = getCookieGenerator().getCookieName();
			if (guidCookieName != null)
			{
				return isGuidStoredinCookies(request, response, guid, guidCookieName);
			}
		}

		return false;
	}

	/**
	 * Checks if the Guid is stored as one of the cookies or not.
	 *
	 * @param request
	 *           HttpServletRequest.
	 * @param response
	 *           HttpServletResponse.
	 * @param guid
	 *           SECURE_GUID_SESSION_KEY String.
	 * @param guidCookieName
	 *           Cookie Name.
	 * @return boolean true or false based on whether it is present or not.
	 */
	protected boolean isGuidStoredinCookies(final HttpServletRequest request, final HttpServletResponse response,
			final String guid, final String guidCookieName)
	{
		for (final Cookie cookie : request.getCookies())
		{
			if (guidCookieName.equals(cookie.getName()))
			{
				if (guid.equals(cookie.getValue()))
				{
					return true;
				}
				else
				{
					LOG.info("Found secure cookie with invalid value. expected [" + guid + "] actual [" + cookie.getValue()
							+ "]. removing.");
					getCookieGenerator().removeCookie(response);
				}
			}
		}

		return false;
	}

	/**
	 * Checks if Anonymous Checkout is enabled or not.
	 *
	 * @return boolean true or false flag to check if Anonymous checkout is enabled for this session.
	 */
	protected boolean checkForAnonymousCheckout()
	{
		if (Boolean.TRUE.equals(getSessionService().getAttribute(ANONYMOUS_CHECKOUT)))
		{
			if (getSessionService().getAttribute(ANONYMOUS_CHECKOUT_GUID) == null)
			{
				getSessionService().setAttribute(ANONYMOUS_CHECKOUT_GUID,
						StringUtils.substringBefore(getCartService().getSessionCart().getUser().getUid(), "|"));
			}
			return true;
		}

		return false;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * @return the cookieGenerator
	 */
	public CookieGenerator getCookieGenerator()
	{
		return cookieGenerator;
	}

	/**
	 * @param cookieGenerator
	 *           the cookieGenerator to set
	 */
	public void setCookieGenerator(final CookieGenerator cookieGenerator)
	{
		this.cookieGenerator = cookieGenerator;
	}

	/**
	 * @return the sessionService
	 */
	public SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * @param sessionService
	 *           the sessionService to set
	 */
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	/**
	 * @return the cartService
	 */
	public CartService getCartService()
	{
		return cartService;
	}

	/**
	 * @param cartService
	 *           the cartService to set
	 */
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

}
