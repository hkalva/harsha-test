/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.capgemini.wishlistaddon.controllers;

import com.capgemini.wishlistaddon.constants.LyonscgwishlistaddonConstants;


/**
 */
public interface LyonscgwishlistaddonControllerConstants
{
	interface Views
	{
		String ADD_ON_PREFIX = "addon:";
		String VIEW_PAGE_PREFIX = ADD_ON_PREFIX + "/" + LyonscgwishlistaddonConstants.EXTENSIONNAME + "/";
		String VIEW_FRAGMENT_PREFIX = ADD_ON_PREFIX + "/" + LyonscgwishlistaddonConstants.EXTENSIONNAME + "/";

		interface Pages
		{
			interface Error
			{
				String ErrorNotFoundPage = "pages/error/errorNotFoundPage";
			}

			interface Wishlist
			{
				String WishlistPage = VIEW_PAGE_PREFIX + "pages/wishlist/wishlistPage";
				String WishlistAddPage = VIEW_PAGE_PREFIX + "pages/wishlist/wishlistAddPage";
				String WishlistEditPage = VIEW_PAGE_PREFIX + "pages/wishlist/wishlistEditPage";
				String WishlistDetailsPage = VIEW_PAGE_PREFIX + "pages/wishlist/wishlistDetailsPage";
			}
		}

		interface Fragments
		{
			interface Cart // NOSONAR
			{
				String AddToCartPopup = "fragments/cart/addToCartPopup";
			}

			interface Wishlist // NOSONAR
			{
				String AddToWishlistPopup = VIEW_FRAGMENT_PREFIX + "fragments/wishlist/addToWishlistPopup";
				String LoginRedirectPopup = VIEW_FRAGMENT_PREFIX + "fragments/wishlist/loginRedirectPopup";
			}
		}
	}
}
