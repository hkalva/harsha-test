package com.capgemini.wishlistaddon.interceptors;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import com.capgemini.wishlistaddon.constants.CapgeminiwishlistaddonConstants;

import de.hybris.platform.acceleratorservices.storefront.data.JavaScriptVariableData;
import de.hybris.platform.addonsupport.config.javascript.BeforeViewJsPropsHandlerAdaptee;
import de.hybris.platform.addonsupport.config.javascript.JavaScriptVariableDataFactory;
import de.hybris.platform.addonsupport.interceptors.BeforeViewHandlerAdaptee;

/**
 *
 * @author lyonscg
 *
 */
public class CapgeminiWishlistBeforeViewHandlerAdaptee implements BeforeViewHandlerAdaptee
{

    /**
     * constant for B2B customer group role.
     */
    private static final String ROLE_B2BCUSTOMERGROUP = "ROLE_B2BCUSTOMERGROUP";

    /**
     * constant for wishlist allowed flag attribute.
     */
    private static final String WISHLIST_ALLOWED = "wishlistAllowed";

    @Override
    public String beforeView(HttpServletRequest request, HttpServletResponse response, ModelMap model, String viewName)
            throws Exception
    {
        Map<String, List<JavaScriptVariableData>> jsAddOnsVariables = (Map<String, List<JavaScriptVariableData>>) model
                .get(BeforeViewJsPropsHandlerAdaptee.JS_VARIABLES_MODEL_NAME);

        if (jsAddOnsVariables == null)
        {
            jsAddOnsVariables = new HashMap<>();
            model.addAttribute(BeforeViewJsPropsHandlerAdaptee.JS_VARIABLES_MODEL_NAME, jsAddOnsVariables);
        }

        List<JavaScriptVariableData> variables = jsAddOnsVariables.get(CapgeminiwishlistaddonConstants.EXTENSIONNAME);

        if (variables == null)
        {
            variables = new LinkedList<>();
            jsAddOnsVariables.put(CapgeminiwishlistaddonConstants.EXTENSIONNAME, variables);
        }

        Boolean wishlistAllowed = Boolean.valueOf(request.isUserInRole(ROLE_B2BCUSTOMERGROUP));
        JavaScriptVariableData data = JavaScriptVariableDataFactory.create(WISHLIST_ALLOWED, wishlistAllowed.toString());
        variables.add(data);
        model.addAttribute(WISHLIST_ALLOWED, wishlistAllowed);
        return viewName;
    }

}
