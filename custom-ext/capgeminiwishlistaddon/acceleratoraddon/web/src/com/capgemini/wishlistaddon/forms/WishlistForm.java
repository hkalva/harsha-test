/**
 *
 */
package com.capgemini.wishlistaddon.forms;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.capgemini.wishlistservices.data.WishlistEntryData;


/**
 * Wish list Form - POJO class.
 */
public class WishlistForm
{
    /**
     * the code.
     */
	private String code;

	/**
	 * the name.
	 */
	private String name;

	/**
	 * the description.
	 */
	private String description;

	/**
	 * the wishlist products.
	 */
	private List<WishlistEntryData> wishlistProducts;

	/**
	 * constant for name max size.
	 */
	private static final int WISHLIST_NAME_MAX_LIMIT = 255;

	/**
	 * constant for description max size.
	 */
	private static final int WISHLIST_DESC_MAX_LIMIT = 4000;

	/**
	 * Getter method for name.
	 *
	 * @return the name
	 */
	@NotNull(message = "{general.required}")
	@Size(min = 1, max = WISHLIST_NAME_MAX_LIMIT, message = "{general.required}")
	public String getName()
	{
		return name;
	}

	/**
	 * Setter method for name.
	 *
	 * @param name
	 *           the name to set
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * Getter method for description.
	 *
	 * @return the description
	 */
	@Size(max = WISHLIST_DESC_MAX_LIMIT, message = "{text.validation.wishlist.description.maxlimit}")
	public String getDescription()
	{
		return description;
	}

	/**
	 * Setter method for description.
	 *
	 * @param description
	 *           the description to set
	 */
	public void setDescription(final String description)
	{
		this.description = description;
	}

	/**
	 * Getter method for products.
	 *
	 * @return the wishlistProducts
	 */
	public List<WishlistEntryData> getWishlistProducts()
	{
		return wishlistProducts;
	}

	/**
	 * Setter method for products.
	 *
	 * @param wishlistProducts
	 *           the wishlistProducts to set
	 */
	public void setWishlistProducts(final List<WishlistEntryData> wishlistProducts)
	{
		this.wishlistProducts = wishlistProducts;
	}

	/**
	 * Getter method for code.
	 *
	 * @return the code
	 */
	public String getCode()
	{
		return code;
	}

	/**
	 * Setter method for code.
	 *
	 * @param code
	 *           the code to set
	 */
	public void setCode(final String code)
	{
		this.code = code;
	}
}
