/**
 *
 */
package com.capgemini.wishlistaddon.forms.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.capgemini.wishlistaddon.forms.WishlistForm;


/**
 * Validator to validate Wishlist Form.
 */
public class WishlistFormValidator implements Validator
{
    /**
     * constant for wishlist description max size.
     */
	private static final int WISHLIST_DESC_MAX_LIMIT = 4000;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(final Class<?> clazz)
	{
		return WishlistForm.class.equals(clazz);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
	 */
	@Override
	public void validate(final Object arg0, final Errors errors)
	{
		final WishlistForm form = (WishlistForm) arg0;

		if (StringUtils.isEmpty(form.getName()))
		{
			errors.rejectValue("Name", "general.required");
		}

		if (StringUtils.isNotEmpty(form.getDescription()) && StringUtils.length(form.getDescription()) > WISHLIST_DESC_MAX_LIMIT)
		{
			errors.rejectValue("Description", "text.validation.wishlist.description.maxlimit");
		}

	}

}
