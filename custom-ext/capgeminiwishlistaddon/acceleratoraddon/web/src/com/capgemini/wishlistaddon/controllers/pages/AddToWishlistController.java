/**
 *
 */
package com.capgemini.wishlistaddon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.capgemini.wishlistaddon.controllers.LyonscgwishlistaddonControllerConstants;
import com.capgemini.wishlistaddon.forms.AddToWishlistForm;
import com.capgemini.wishlistaddon.user.utils.UserHelper;
import com.capgemini.wishlistservices.data.WishlistData;
import com.capgemini.wishlistservices.data.WishlistEntryData;
import com.capgemini.wishlistservices.facades.WishlistFacade;


/**
 * Class which implements methods to Add Product to Wish list functionality.
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/wishlist")
public class AddToWishlistController extends AbstractPageController
{

	private static final Logger LOG = LogManager.getLogger(AddToWishlistController.class);

	private static final String WISHLIST_ADD_PRODUCT_CMS_PAGE = "add-product-wishlist";
	private static final String TEXT_ERROR_INVALID_USER_SESSION = "text.error.invalid.user.session";
	private static final String TEXT_ERROR_PRODUCT_ALREADY_EXISTS = "text.error.product.already.exists";
	private static final String TEXT_ERROR_WISHLIST_CREATE_AND_ADD_ERROR = "text.error.wishlist.create.addto.error";
	private static final String TEXT_ERROR_ADD_TO_WISHLIST_ERROR = "text.error.addtowishlist.error";
	private static final String TEXT_CONF_WISHLIST_PRODUCT_ADDED = "text.confirmation.wishlist.productadded";
	private static final String TEXT_CONF_WISHLIST_CREATED_PRODUCT_ADDED = "text.confirmation.wishlist.created.productadded";
	private static final String TEXT_VALIDATION_WISHLIST_PRODUCT_REQUIRED = "text.validation.wishlist.product.required";
	private static final String TEXT_VALIDATION_WISHLIST_NONESELECTED = "text.validation.wishlist.noneselected";
	private static final String TEXT_VALIDATION_WISHLIST_NAME_REQUIRED = "text.validation.wishlist.name.required";
	private static final String TEXT_VALIDATION_WISHLIST_NAME_MAXLIMIT = "text.validation.wishlist.name.maxlimit";
	private static final String TEXT_VALIDATION_WISHLIST_DESCRIPTION_MAXLIMIT = "text.validation.wishlist.description.maxlimit";
	private static final String TEXT_VALIDATION_WISHLIST_CODE_REQUIRED = "text.validation.wishlist.wishlistcode.required";
	private static final String WISHLIST_STATUS_SUCCESS = "200";
	private static final String WISHLIST_STATUS_ERROR = "500";
	private static final String WISHLIST_KEY_STATUS = "status";
	private static final String WISHLIST_KEY_MSG = "msg";
	private static final String WISHLIST_KEY_WISHLISTLIST = "wishlistList";
	private static final int WISHLIST_DESC_MAX_LIMIT = 4000;
	private static final int WISHLIST_NAME_MAX_LIMIT = 255;

	@Resource
	private WishlistFacade wishlistFacade;
	@Resource
	private ProductFacade productFacade;
	@Resource
	private UserService userService;
	@Resource
	private CommerceCommonI18NService commerceCommonI18NService;
	@Resource
	private UserHelper userHelper;

	/**
	 * Method to get the Add to Wishlist page. Checks for a valid logged in user, if not redirects to a popup with a Link
	 * to Login page.
	 *
	 * @param productCode
	 *           Product code to be added to the wishlist
	 * @param model
	 * @return HTML View page URL String of Add to Wishlist page or the Page with a link to Login page.
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/addToWishlist", method = RequestMethod.GET)
	public String addToWishlist(@RequestParam("productCode") final String productCode, final Model model)
			throws CMSItemNotFoundException
	{
		//Check to see if the User is logged in or not. User has to be a logged in user to Add to Wishlist
		if (getUserHelper().isRequireHardLogin(model))
		{
			//User not logged in, Direct to a page with a link to Login.
			return LyonscgwishlistaddonControllerConstants.Views.Fragments.Wishlist.LoginRedirectPopup;
		}
		else
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(WISHLIST_ADD_PRODUCT_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(WISHLIST_ADD_PRODUCT_CMS_PAGE));

			//Set values in the model to re-use in popup
			final AddToWishlistForm wishlistForm = new AddToWishlistForm();
			wishlistForm.setProductCode(productCode);
			model.addAttribute(wishlistForm);
			model.addAttribute(WISHLIST_KEY_WISHLISTLIST, getWishlistFacade().getMyWishlists());

			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
			return LyonscgwishlistaddonControllerConstants.Views.Fragments.Wishlist.AddToWishlistPopup;
		}
	}

	/**
	 * Add Product to Wish list. Either Add product to the selected Wish list or create a new wish list and add to it.
	 * Consume a serialized form through an Ajax console and return a Json response stating Success or Error.
	 *
	 * @param addToWishlistForm
	 *           Form holding the Create or Add to existing wish list details.
	 * @param model
	 *           Model Object.
	 * @param bindingResult
	 *           BindingResult Object.
	 * @return JSON response of a Map of Status, Msg and Wish list List.
	 */
	@RequestMapping(value = "/addToWishlist", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, Object> addToWishlist(@RequestBody final AddToWishlistForm addToWishlistForm, final Model model,
			final BindingResult bindingResult)
	{
		final Map<String, Object> jsonResponse = new HashMap<String, Object>();
		if (getUserHelper().isRequireHardLogin(model))
		{
			//User not logged in, Returning JSON response with invalid user session error. Ajax needs to redirect to a page with a link to Login.
			jsonResponse.put(WISHLIST_KEY_STATUS, WISHLIST_STATUS_ERROR);
			jsonResponse.put(WISHLIST_KEY_MSG, getMessage(TEXT_ERROR_INVALID_USER_SESSION));
			return jsonResponse;
		}

		String errors;
		if (StringUtils.isNotEmpty(errors = validateAddToWishlistForm(addToWishlistForm)))
		{
			jsonResponse.put(WISHLIST_KEY_STATUS, WISHLIST_STATUS_ERROR);
			jsonResponse.put(WISHLIST_KEY_MSG, errors);
			return jsonResponse;
		}

		//Populate WishlistEntryData object with the passed values
		final WishlistEntryData wishlistEntryData = populateWishlistEntryDataFromForm(addToWishlistForm.getProductCode());
		if (addToWishlistForm.isCreate())
		{
			return createAndAddProductToWishlist(addToWishlistForm, wishlistEntryData);
		}
		else
		{
			return addProductToWishlist(addToWishlistForm, wishlistEntryData);
		}
	}

	/**
	 * Add Product to an existing Wish list selected from Front end.
	 *
	 * @param addToWishlistForm
	 *           wishlist form with values populated to perform add Product to Wish list.
	 * @param wishlistEntryData
	 *           Data object to pass to backend for addition.
	 * @return JSON Response Map holding the value of status, msg and Wishlist List.
	 */
	private Map<String, Object> addProductToWishlist(final AddToWishlistForm addToWishlistForm,
			final WishlistEntryData wishlistEntryData)
	{
		final Map<String, Object> jsonResponse = new HashMap<String, Object>();

		try
		{
			/* Add product to the selected wish list */
			getWishlistFacade().addProductToWishlist(addToWishlistForm.getWishlistCode(), wishlistEntryData);
			jsonResponse.put(WISHLIST_KEY_STATUS, WISHLIST_STATUS_SUCCESS);
			jsonResponse.put(WISHLIST_KEY_MSG, getMessage(TEXT_CONF_WISHLIST_PRODUCT_ADDED));
		}
		catch (final AssertionError ae)
		{
			jsonResponse.put(WISHLIST_KEY_STATUS, WISHLIST_STATUS_ERROR);
			jsonResponse.put(WISHLIST_KEY_MSG, getMessage(TEXT_ERROR_PRODUCT_ALREADY_EXISTS));
		}
		catch (final Exception e)
		{
			jsonResponse.put(WISHLIST_KEY_STATUS, getMessage(WISHLIST_STATUS_ERROR));
			jsonResponse.put(WISHLIST_KEY_MSG, getMessage(TEXT_ERROR_ADD_TO_WISHLIST_ERROR));
		}
		return jsonResponse;
	}


	/**
	 * To create a new Wish list and add the product passed to it.
	 *
	 * @param addToWishlistForm
	 *           wishlist form with values populated to perform add Product to Wish list.
	 * @param wishlistEntryData
	 *           Data object to pass to backend for addition.
	 * @return JSON Response with values for status, msg and wishlistLst.
	 */
	private Map<String, Object> createAndAddProductToWishlist(final AddToWishlistForm addToWishlistForm,
			final WishlistEntryData wishlistEntryData)
	{
		LOG.info("Wishlist creation, followed by product add to wishlist");
		final Map<String, Object> jsonResponse = new HashMap<String, Object>();
		WishlistData wishlistData = populateWishlistDataFromForm(addToWishlistForm);
		try
		{
			/* Create a new Wish list. */
			wishlistData = getWishlistFacade().createWishlist(wishlistData);
			/* Add product to this newly created wish list */
			getWishlistFacade().addProductToWishlist(wishlistData.getCode(), wishlistEntryData);
			jsonResponse.put(WISHLIST_KEY_STATUS, WISHLIST_STATUS_SUCCESS);
			jsonResponse.put(WISHLIST_KEY_MSG, getMessage(TEXT_CONF_WISHLIST_CREATED_PRODUCT_ADDED));
		}
		catch (final Exception e)
		{
			jsonResponse.put(WISHLIST_KEY_STATUS, WISHLIST_STATUS_ERROR);
			jsonResponse.put(WISHLIST_KEY_MSG, getMessage(TEXT_ERROR_WISHLIST_CREATE_AND_ADD_ERROR));
		}

		return jsonResponse;
	}

	/**
	 * Method to validate the AddToWishlistForm and return the error messages.
	 *
	 * @param form
	 * @return Error Message.
	 */
	private String validateAddToWishlistForm(final AddToWishlistForm form)
	{
		final List errorLst = new ArrayList<String>();
		if (StringUtils.isEmpty(form.getProductCode()))
		{
			errorLst.add(TEXT_VALIDATION_WISHLIST_PRODUCT_REQUIRED);
		}
		else if (StringUtils.isEmpty(form.getName()) && StringUtils.isEmpty(form.getWishlistCode())
				&& StringUtils.isEmpty(form.getDescription()))
		{
			errorLst.add(TEXT_VALIDATION_WISHLIST_NONESELECTED);
			return getErrorMessageForErrorList(errorLst);
		}

		if (form.isCreate())
		{
			if (StringUtils.isEmpty(form.getName()))
			{
				errorLst.add(TEXT_VALIDATION_WISHLIST_NAME_REQUIRED);
			}
			if (StringUtils.isNotEmpty(form.getName()) && StringUtils.length(form.getName()) > WISHLIST_NAME_MAX_LIMIT)
			{
				errorLst.add(TEXT_VALIDATION_WISHLIST_NAME_MAXLIMIT);
			}
			if (StringUtils.isNotEmpty(form.getDescription()) && StringUtils.length(form.getDescription()) > WISHLIST_DESC_MAX_LIMIT)
			{
				errorLst.add(TEXT_VALIDATION_WISHLIST_DESCRIPTION_MAXLIMIT);
			}
		}
		else
		{
			if (StringUtils.isEmpty(form.getWishlistCode()))
			{
				errorLst.add(TEXT_VALIDATION_WISHLIST_CODE_REQUIRED);
			}
		}
		return getErrorMessageForErrorList(errorLst);
	}

	/**
	 * To concatenate all the error messaages to be rendered on screen and get their corresponding localized messages.
	 *
	 * @param errorMsgLst
	 * @return String Error Message.
	 */
	private String getErrorMessageForErrorList(final List<String> errorMsgLst)
	{
		if (errorMsgLst.isEmpty())
		{
			return StringUtils.EMPTY;
		}
		final StringBuilder errorMsg = new StringBuilder();
		for (final String error : errorMsgLst)
		{
			if (StringUtils.isNotEmpty(errorMsg.toString()))
			{
				errorMsg.append(",");
			}
			errorMsg.append(getMessage(error));
		}
		return errorMsg.toString();
	}


	/**
	 * To retrieve the localized message to be rendered in the screen.
	 *
	 * @param code
	 *           message code.
	 * @return Localized message.
	 */
	protected String getMessage(final String code)
	{
		return getMessageSource().getMessage(code, null, getCommerceCommonI18NService().getCurrentLocale());
	}

	/**
	 * Populate WishlistData Object from AddToWishlist Form.
	 *
	 * @param addToWishlistForm
	 * @return WishlistData
	 */
	private WishlistData populateWishlistDataFromForm(final AddToWishlistForm addToWishlistForm)
	{
		final WishlistData wishlistData = new WishlistData();
		wishlistData.setName(addToWishlistForm.getName());
		wishlistData.setDescription(addToWishlistForm.getDescription());
		return wishlistData;
	}

	/**
	 * Populate WishlistEntryData object with data from AddToWishlistForm. WishlistEntry priority is defaulted to medium
	 * and Desired quantity is defaulted to 1.
	 *
	 * @param productCode
	 * @return WishlistEntryData
	 */
	private WishlistEntryData populateWishlistEntryDataFromForm(final String productCode)
	{
		final WishlistEntryData wishlistEntryData = new WishlistEntryData();
		wishlistEntryData.setDesired(Integer.valueOf(1));
		wishlistEntryData.setProduct(getProductFacade()
				.getProductForCodeAndOptions(productCode, Arrays.asList(ProductOption.BASIC)));
		wishlistEntryData.setPriority(Wishlist2EntryPriority.MEDIUM);
		return wishlistEntryData;
	}

	/**
	 * @return the wishlistFacade
	 */
	public WishlistFacade getWishlistFacade()
	{
		return wishlistFacade;
	}

	/**
	 * @param wishlistFacade
	 *           the wishlistFacade to set
	 */
	public void setWishlistFacade(final WishlistFacade wishlistFacade)
	{
		this.wishlistFacade = wishlistFacade;
	}

	/**
	 * @return the productFacade
	 */
	public ProductFacade getProductFacade()
	{
		return productFacade;
	}

	/**
	 * @param productFacade
	 *           the productFacade to set
	 */
	public void setProductFacade(final ProductFacade productFacade)
	{
		this.productFacade = productFacade;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}


	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * @return the commerceCommonI18NService
	 */
	public CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	/**
	 * @param commerceCommonI18NService
	 *           the commerceCommonI18NService to set
	 */
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}

	/**
	 * @return the userHelper
	 */
	public UserHelper getUserHelper()
	{
		return userHelper;
	}

	/**
	 * @param userHelper
	 *           the userHelper to set
	 */
	public void setUserHelper(final UserHelper userHelper)
	{
		this.userHelper = userHelper;
	}

}
