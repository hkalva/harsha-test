/**
 *
 */
package com.capgemini.wishlistaddon.forms;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;

import java.util.List;


/**
 * AddToCartWishlistForm to represent the Wishlist in the form of Cart entries.
 */
public class AddToCartWishlistForm
{
    /**
     * the cart entries.
     */
	private List<OrderEntryData> cartEntries;

	/** Getter method for retrieving the list of Cart Entries.
	 * @return Return the cartEntries.
	 */
	public List<OrderEntryData> getCartEntries()
	{
		return cartEntries;
	}

	/**
	 * Setter method for setting the list of Cart Entries into the list.
	 *
	 * @param cartEntries
	 *           The cartEntries to set.
	 */
	public void setCartEntries(final List<OrderEntryData> cartEntries)
	{
		this.cartEntries = cartEntries;
	}
}
