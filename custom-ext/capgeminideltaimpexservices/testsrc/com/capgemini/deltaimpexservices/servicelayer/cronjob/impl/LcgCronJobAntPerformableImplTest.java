package com.capgemini.deltaimpexservices.servicelayer.cronjob.impl;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

/**
 * 
 * @author lyonscg
 *
 */
@UnitTest
public class LcgCronJobAntPerformableImplTest
{
    
    /**
     * the performable to test.
     */
    @Mock
    private LcgCronJobAntPerformableImpl performable;
    
    /**
     * the model service.
     */
    @Mock
    private ModelService modelService;
    
    /**
     * the default cronjob service.
     */
    @Mock
    private CronJobService cronJobService;
    
    /**
     * the cron job.
     */
    @Mock
    private CronJobModel cronJobModel;
    
    /**
     * set up data.
     */
    @Before
    public void setup() throws Exception
    {
        MockitoAnnotations.initMocks(this);
        Mockito.when(performable.getBean("cronJobService")).thenReturn(cronJobService);
        Mockito.when(performable.getBean("modelService")).thenReturn(modelService);
        Mockito.doCallRealMethod().when(performable).validate();
        Mockito.doCallRealMethod().when(performable).performImpl();
        Mockito.doNothing().when(cronJobService).performCronJob(Mockito.any(CronJobModel.class), Mockito.anyBoolean());
        Mockito.doNothing().when(modelService).save(Mockito.anyObject());
        Mockito.doNothing().when(modelService).refresh(Mockito.anyObject());
        Mockito.when(cronJobModel.getLogs()).thenReturn(Collections.emptyList());
    }
    
    /**
     * test validate error.
     */
    @Test
    public void testValidateError()
    {
        Mockito.when(cronJobService.getCronJob(Mockito.anyString())).thenThrow(new UnknownIdentifierException("test"));
        Assert.assertFalse(performable.validate());
        Mockito.verify(performable, Mockito.times(1)).getBean(Mockito.anyString());
    }
    
    /**
     * test validate.
     */
    @Test
    public void testValidate()
    {
        Mockito.when(cronJobService.getCronJob(Mockito.anyString())).thenReturn(null);
        Assert.assertFalse(performable.validate());
        Mockito.verify(performable, Mockito.times(1)).getBean(Mockito.anyString());
        Mockito.when(cronJobService.getCronJob(Mockito.anyString())).thenReturn(cronJobModel);
        Assert.assertTrue(performable.validate());
        Mockito.verify(performable, Mockito.times(3)).getBean(Mockito.anyString());
    }
    
    /**
     * test performImpl invalid cronjob.
     */
    @Test(expected = Exception.class)
    public void testPerformImplInvalidCronJob() throws Exception
    {
        Mockito.when(cronJobService.getCronJob(Mockito.anyString())).thenReturn(null);
        performable.validate();
        performable.performImpl();
    }

    /**
     * test performImpl.
     */
    @Test()
    public void testPerformImpl() throws Exception
    {
        Mockito.when(cronJobService.getCronJob(Mockito.anyString())).thenReturn(cronJobModel);
        performable.validate();
        performable.performImpl();
        Mockito.verify(cronJobService, Mockito.times(2)).getCronJob(Mockito.anyString());
        Mockito.verify(cronJobService, Mockito.times(1)).performCronJob(Mockito.any(CronJobModel.class), Mockito.anyBoolean());
        Mockito.verify(modelService, Mockito.times(1)).save(Mockito.any(CronJobModel.class));
        Mockito.verify(modelService, Mockito.times(1)).refresh(Mockito.any(CronJobModel.class));
    }
    
}
