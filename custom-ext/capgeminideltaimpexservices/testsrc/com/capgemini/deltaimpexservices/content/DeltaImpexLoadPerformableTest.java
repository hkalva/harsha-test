package com.capgemini.deltaimpexservices.content;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.internal.model.ServicelayerJobModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.capgemini.deltaimpexservices.model.DeltaImpexLoadCronJobModel;
import com.capgemini.deltaimpexservices.model.DeltaImpexLoadProcessedFileModel;


/**
 * Tests for the Delta impex loader class.
 */
@UnitTest
public class DeltaImpexLoadPerformableTest extends ServicelayerTest
{
	/** Logger for the class. */
	private static final Logger LOG = LogManager.getLogger(DeltaImpexLoadPerformableTest.class.getName());

	/** Injected cronJobService bean. */
	@Resource
	private CronJobService cronJobService;

	/** Injected modelService bean. */
	@Resource
	private ModelService modelService;

	/** Injected flexibleSearchService bean. */
	@Resource
	private FlexibleSearchService flexibleSearchService;

	/** servicelayerJobModel for testing. */
	private ServicelayerJobModel servicelayerJobModel = null;

	/** deltaImpexLoadCJ for testing. */
	private DeltaImpexLoadCronJobModel deltaImpexLoadCJ = null;


	private final FlexibleSearchQuery query = new FlexibleSearchQuery("SELECT {pk} FROM {DeltaImpexLoadProcessedFile}");
	private final FlexibleSearchQuery successQuery = new FlexibleSearchQuery(
			"SELECT {pk} FROM {DeltaImpexLoadProcessedFile} where {success}=true");

	@Before
	public void setUp()
	{
		//The update of the JUnit tenant creates automatically an instance of the defined deltaImpexImporter job
		//Search for it
		final ServicelayerJobModel sjm = new ServicelayerJobModel();
		sjm.setSpringId("deltaImpexLoadPerformable");
		try
		{
			servicelayerJobModel = flexibleSearchService.getModelByExample(sjm); //searching by example
		}
		catch (final ModelNotFoundException e)
		{
			//The cronjob functionality in the processing extension creates for each JobPerformable a ServicelayerJob where the springID is equal to the job bean id
			//You just create a job here
			servicelayerJobModel = modelService.create(ServicelayerJobModel.class);
			servicelayerJobModel.setSpringId("deltaImpexImporter");
			servicelayerJobModel.setCode("deltaImpexImporter");
			modelService.save(servicelayerJobModel);
			//Keep in mind that creating models in the catch clause is bad style
		}
	}

	@Test
	public void testIfThePerformableExist()
	{
		//Check if there is an instance of myJobPerformable
		assertNotNull("***************No performable with springID *deltaImpexImporter* found perhaps you have to "
				+ "Update your JunitTenant to let create an instance!", servicelayerJobModel);
	}

	@Test
	public void testLoadAll()
	{
		// make sure processed file table is empty
		modelService.removeAll(getProcessedFiles());

		// Create a CronJob and set the servicelayerJob
		deltaImpexLoadCJ = modelService.create(DeltaImpexLoadCronJobModel.class);
		deltaImpexLoadCJ.setActive(Boolean.TRUE);
		deltaImpexLoadCJ.setJob(servicelayerJobModel);
		deltaImpexLoadCJ.setManifestPath("/releases/test/testManifest.txt");
		modelService.save(deltaImpexLoadCJ);

		List<DeltaImpexLoadProcessedFileModel> processedFiles = getProcessedFiles();

		assertEquals("There should be no records in the DeltaImpexLoadProcessedFile table", 0, processedFiles.size());
		//Perform the CronJob once for the test
		cronJobService.performCronJob(deltaImpexLoadCJ);

		//Wait for the result to be written
		try
		{
			Thread.sleep(2000);
		}
		catch (final InterruptedException e)
		{
			e.printStackTrace();
		}

		LOG.info("*************** lets wait 2 seconds for the result  ***************");

		//Test if the job was executed successfully, if it fails here then try to extend the time
		assertEquals("*************** The perfromable has not finished successfull or more wait is required on this machine!",
				CronJobResult.SUCCESS, deltaImpexLoadCJ.getResult());
		processedFiles = getProcessedFiles();

		assertEquals("There should be 2 records in the DeltaImpexLoadProcessedFile table", 2, processedFiles.size());

	}

	private List<DeltaImpexLoadProcessedFileModel> getProcessedFiles()
	{
		SearchResult<DeltaImpexLoadProcessedFileModel> result;
		List<DeltaImpexLoadProcessedFileModel> processedFiles;
		result = flexibleSearchService.search(query);
		processedFiles = result.getResult();
		return processedFiles;
	}

	private List<DeltaImpexLoadProcessedFileModel> getSuccessfullyProcessedFiles()
	{
		SearchResult<DeltaImpexLoadProcessedFileModel> result;
		List<DeltaImpexLoadProcessedFileModel> processedFiles;
		result = flexibleSearchService.search(successQuery);
		processedFiles = result.getResult();
		return processedFiles;
	}

	@Test
	public void testFirstAlreadyLoaded()
	{
		// Create a CronJob and set the servicelayerJob
		deltaImpexLoadCJ = modelService.create(DeltaImpexLoadCronJobModel.class);
		deltaImpexLoadCJ.setActive(Boolean.TRUE);
		deltaImpexLoadCJ.setJob(servicelayerJobModel);
		deltaImpexLoadCJ.setManifestPath("/releases/test/testManifest.txt");
		modelService.save(deltaImpexLoadCJ);

		// load a record into the processed table
		final DeltaImpexLoadProcessedFileModel processedFile = modelService.create(DeltaImpexLoadProcessedFileModel.class);
		processedFile.setProcessedFile("/releases/test/Sprint1/client-123-01.impex");
		processedFile.setSuccess(Boolean.TRUE);
		modelService.save(processedFile);

		final List<DeltaImpexLoadProcessedFileModel> beforeTestExecProcessedFiles = getProcessedFiles();
		final int beforeExecCount = beforeTestExecProcessedFiles.size();

		List<DeltaImpexLoadProcessedFileModel> processedFiles = getProcessedFiles();

		assertEquals("There should be 1 records in the DeltaImpexLoadProcessedFile table", beforeExecCount, processedFiles.size());
		//Perform the CronJob once for the test
		cronJobService.performCronJob(deltaImpexLoadCJ);

		//Wait for the result to be written
		try
		{
			Thread.sleep(2000);
		}
		catch (final InterruptedException e)
		{
			e.printStackTrace();
		}

		LOG.info("*************** lets wait 2 seconds for the result  ***************");

		//Test if the job was executed successfully, if it fails here then try to extend the time
		assertEquals("*************** The perfromable has not finished successfull or more wait is required on this machine!",
				CronJobResult.SUCCESS, deltaImpexLoadCJ.getResult());
		processedFiles = getProcessedFiles();

		assertEquals("There should be 2 records in the DeltaImpexLoadProcessedFile table", beforeExecCount + 1,
				processedFiles.size());

	}

	@Test
	public void testBreakOnError()
	{
		// Create a CronJob and set the servicelayerJob
		deltaImpexLoadCJ = modelService.create(DeltaImpexLoadCronJobModel.class);
		deltaImpexLoadCJ.setActive(Boolean.TRUE);
		deltaImpexLoadCJ.setJob(servicelayerJobModel);
		deltaImpexLoadCJ.setManifestPath("/releases/test/testBreakOnErrorManifest.txt");
		modelService.save(deltaImpexLoadCJ);

		final List<DeltaImpexLoadProcessedFileModel> beforeTestExecProcessedFiles = getProcessedFiles();
		final int beforeCount = beforeTestExecProcessedFiles.size();
		LOG.info("*************** # of records in DeltaImpexLoadProcessedFile table : " + beforeCount);

		//Perform the CronJob once for the test
		cronJobService.performCronJob(deltaImpexLoadCJ);

		//Wait for the result to be written
		try
		{
			Thread.sleep(2000);
		}
		catch (final InterruptedException e)
		{
			e.printStackTrace();
		}

		LOG.info("*************** lets wait 2 seconds for the result  ***************");

		//Test if the job was executed successfully, if it fails here then try to extend the time
		assertEquals("*************** This job should have failed", CronJobResult.FAILURE, deltaImpexLoadCJ.getResult());
		final List<DeltaImpexLoadProcessedFileModel> processedFiles = getProcessedFiles();
		final List<DeltaImpexLoadProcessedFileModel> successfullyProcessedFiles = getSuccessfullyProcessedFiles();

		assertEquals("There should be " + beforeCount + " successful records in the DeltaImpexLoadProcessedFile table", beforeCount,
				successfullyProcessedFiles.size());
		assertEquals("There should be " + (beforeCount + 1) + " of records in the DeltaImpexLoadProcessedFile table",
				beforeCount + 1, processedFiles.size());
	}

}
