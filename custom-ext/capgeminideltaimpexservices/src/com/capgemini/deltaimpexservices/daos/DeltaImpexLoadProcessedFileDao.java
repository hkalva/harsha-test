/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.deltaimpexservices.daos;

import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;

import com.capgemini.deltaimpexservices.model.DeltaImpexLoadProcessedFileModel;


/**
 * @author lyonscg. Dao class for DeltaImpexLoadProcessedFileDao.
 */
public interface DeltaImpexLoadProcessedFileDao extends Dao
{
	/**
	 * @return all DeltaImpexLoadProcessedFileModels
	 */
	List<DeltaImpexLoadProcessedFileModel> getAllDeltaImpexLoadProcessedFile();
}
