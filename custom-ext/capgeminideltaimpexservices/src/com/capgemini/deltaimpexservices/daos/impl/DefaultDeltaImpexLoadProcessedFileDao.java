/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.deltaimpexservices.daos.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.List;

import com.capgemini.deltaimpexservices.daos.DeltaImpexLoadProcessedFileDao;
import com.capgemini.deltaimpexservices.model.DeltaImpexLoadProcessedFileModel;


/**
 * @author lyonscg. Dao implementation class for DeltaImpexLoadProcessedFileDao.
 */
public class DefaultDeltaImpexLoadProcessedFileDao extends DefaultGenericDao<DeltaImpexLoadProcessedFileModel>
		implements DeltaImpexLoadProcessedFileDao
{

	/**
	 * Default Constructor
	 */
	public DefaultDeltaImpexLoadProcessedFileDao()
	{
		super(DeltaImpexLoadProcessedFileModel._TYPECODE);
	}

	@Override
	public List<DeltaImpexLoadProcessedFileModel> getAllDeltaImpexLoadProcessedFile()
	{
		return find();
	}

}
