/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.deltaimpexservices.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.capgemini.services.util.CharsetUtil;


/**
 * Helper class to handle common file read operations.
 *
 */
public final class FileReadHelper
{
	/**
	 * Logger is used to displaying log massages.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(FileReadHelper.class);

	/**
	 * Using private constructor instead of default constructor to avoid checkstyle errors.
	 */
	private FileReadHelper()
	{
		// Do nothing
	}

	/**
	 * Reads a file (that is in the classpath) putting each line into a List<String>
	 *
	 * @param filePath
	 *           {@link String} Path to the file you want read.
	 * @return linesInFile {@link List}<{@link String}> containing the lines of the file
	 */
	public static List<String> getResourceFileLines(final String filePath)
	{
		Assert.notNull(filePath, "Parameter filePath cannot be null.");
		final List<String> linesInFile = new ArrayList<String>();
		BufferedReader txtReader = null;
		LOG.debug("Getting number of lines in: " + filePath);
		final Charset charset = CharsetUtil.getCharset();

		try
		{
			txtReader = new BufferedReader(new InputStreamReader(FileReadHelper.class.getResourceAsStream(filePath), charset));
			String line;

			while ((line = txtReader.readLine()) != null)
			{
				if (StringUtils.isNotBlank(line))
				{
					linesInFile.add(line);
				}
			}

			txtReader.close();
		}
		catch (final IOException e)
		{
			LOG.error("Unable to read from: " + filePath, e);
		}
		finally
		{
			IOUtils.closeQuietly(txtReader);
		}

		LOG.debug("Number of lines: " + linesInFile.size());
		return linesInFile;
	}
}
