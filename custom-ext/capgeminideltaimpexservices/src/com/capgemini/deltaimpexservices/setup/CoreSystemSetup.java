/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.capgemini.deltaimpexservices.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.capgemini.deltaimpexservices.constants.CapgeminideltaimpexservicesConstants;
import com.capgemini.deltaimpexservices.daos.DeltaImpexLoadProcessedFileDao;
import com.capgemini.deltaimpexservices.helper.FileReadHelper;
import com.capgemini.deltaimpexservices.model.DeltaImpexLoadProcessedFileModel;
import com.capgemini.deltaimpexservices.servicelayer.cronjob.impl.LcgCronJobAntPerformableImpl;


/**
 * This class provides hooks into the system's initialization and update processes.
 *
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = CapgeminideltaimpexservicesConstants.EXTENSIONNAME)
public class CoreSystemSetup extends AbstractSystemSetup
{
	private static final String MANIFEST_FILE_LOCATION = "manifest.file.location";
	private static final Logger LOG = LogManager.getLogger(CoreSystemSetup.class.getName());
	@Autowired
	private ConfigurationService configurationService;
	@Autowired
	private DeltaImpexLoadProcessedFileDao deltaImpexLoadProcessedFileDao;

	/**
	 * Generates the Dropdown and Multi-select boxes for the project data import
	 */
	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<SystemSetupParameter>();
		//params.add(createBooleanSystemSetupParameter(IMPORT_ORDER_EXPORT_CRONJOBS, "Import Order Export Cronjobs", true));
		return params;
	}

	/**
	 * Runs the Delta Impex cron job
	 *
	 * @param context
	 */
	@SystemSetup(type = Type.ESSENTIAL, process = Process.UPDATE)
	public void runDeltaImpexJob(final SystemSetupContext context)
	{
		final de.hybris.ant.taskdefs.AntPerformable p = new LcgCronJobAntPerformableImpl("deltaImpexLoadCronJob");
		try
		{
			logInfo(context, "Starting delta impex load...");
			p.doPerform();
		}
		catch (final Exception e)
		{
			LOG.error("Failed to execute delta impex cron job", e);
		}
		Registry.activateMasterTenant();
		final List<DeltaImpexLoadProcessedFileModel> deltaFilesModels = deltaImpexLoadProcessedFileDao
				.getAllDeltaImpexLoadProcessedFile();
		final List<String> filesInManifest = FileReadHelper
				.getResourceFileLines(configurationService.getConfiguration().getString(MANIFEST_FILE_LOCATION));
		final List<String> filesToProcess = new ArrayList<>(filesInManifest.size());
		for (final String fileName : filesInManifest)
		{
			if (!StringUtils.startsWith(fileName, "#"))
			{
				filesToProcess.add(fileName);
			}
		}
		final List<String> filesProcessedSuccessfully = new ArrayList<>(filesInManifest.size());
		boolean failedImpex = false;
		for (final DeltaImpexLoadProcessedFileModel deltaImpexLoadProcessedFileModel : deltaFilesModels)
		{
			if (BooleanUtils.isFalse(deltaImpexLoadProcessedFileModel.getSuccess()))
			{
				logError(context, "Delta impex " + deltaImpexLoadProcessedFileModel.getProcessedFile() + " failed", null);
				failedImpex = true;
			}
			else
			{
				filesProcessedSuccessfully.add(deltaImpexLoadProcessedFileModel.getProcessedFile());
			}
		}
		failedImpex = failedImpex || !filesProcessedSuccessfully.containsAll(filesToProcess);
		filesToProcess.removeAll(filesProcessedSuccessfully);
		if (failedImpex)
		{
			logError(context, "Delta impex load completed with failed status.", null);
			logError(context, "Files " + filesToProcess + " not executed successfully", null);
		}
		else
		{
			logInfo(context, "Delta impex load completed with success status.");
		}
	}

}
