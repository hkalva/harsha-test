/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.capgemini.deltaimpexservices.setup;

import de.hybris.platform.core.initialization.SystemSetup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.deltaimpexservices.constants.CapgeminideltaimpexservicesConstants;


@SystemSetup(extension = CapgeminideltaimpexservicesConstants.EXTENSIONNAME)
public class CapgeminideltaimpexservicesSystemSetup
{
    /**
     * the logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(CapgeminideltaimpexservicesSystemSetup.class);
    
	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
	    LOG.debug("In createEssentialData method");
	}

}
