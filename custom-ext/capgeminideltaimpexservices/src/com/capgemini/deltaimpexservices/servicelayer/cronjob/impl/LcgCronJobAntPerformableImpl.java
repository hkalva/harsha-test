/**
 *
 */
package com.capgemini.deltaimpexservices.servicelayer.cronjob.impl;

import de.hybris.platform.core.Registry;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.cronjob.model.JobLogModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.cronjob.impl.CronJobAntPerformableImpl;
import de.hybris.platform.servicelayer.model.ModelService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * LCG MODIFICATION: store cronJobModel in modelService + logging.
 */
public class LcgCronJobAntPerformableImpl extends CronJobAntPerformableImpl
{
    /**
     * the logger.
     */
	private static final Logger LOG = LoggerFactory.getLogger(LcgCronJobAntPerformableImpl.class);

	/**
	 * the model service.
	 */
	private ModelService modelService;

	/**
	 * the cron job service.
	 */
	private CronJobService service;

	/**
	 * the cron job id.
	 */
	private final String cronJobId;

	/**
	 * LCG: Unmodified from original.
	 *
	 * @param cronjobCode
	 *           - Cronjob id.
	 */
	public LcgCronJobAntPerformableImpl(final String cronjobCode)
	{
		this(cronjobCode, Registry.getMasterTenant().getTenantID());
	}

	/**
	 * LCG: Log cronJobCode + set cronJobId.
	 *
	 * @param cronjobCode
	 *           - Cronjob id.
	 * @param currentTennantId
	 *           - Tenant to run the cronjob.
	 */
	public LcgCronJobAntPerformableImpl(final String cronjobCode, final String currentTennantId)
	{
		super(currentTennantId);
		LOG.info("Performing cron job {}", cronjobCode);
		this.cronJobId = cronjobCode;
	}

	/**
	 * LCG: set modelService.
	 */
	@Override
	public boolean validate()
	{
		try
		{
			this.service = (CronJobService) getBean("cronJobService");

			if (this.service.getCronJob(this.cronJobId) == null)
			{
				LOG.info("Cannot find cron job with code {} for tenant {}", this.cronJobId, this.tenantId);
				return false;
			}

			this.modelService = (ModelService) getBean("modelService");
		}
		catch (final Exception e)
		{
			LOG.error("Error on validate", e);
			return false;
		}

		return true;
	}

	/**
	 * LCG: save cronJobModel to modelService.
	 *
	 * @throws Exception
	 */
	@Override
	protected void performImpl() throws Exception
	{
		CronJobStatus status = null;

		try
		{
			final CronJobModel cronJobModel = this.service.getCronJob(this.cronJobId);
			this.service.performCronJob(cronJobModel, true);

			for (final JobLogModel jlm : cronJobModel.getLogs())
			{
				LOG.info(jlm.getShortMessage());
			}

			LOG.info("Log text: {}", cronJobModel.getLogText());
			status = cronJobModel.getStatus();
			modelService.save(cronJobModel);
			modelService.refresh(cronJobModel);
		}
		finally
		{
			if (status != null)
			{
				LOG.info("Cronjob status after execution: {}", status.getCode());
			}
		}
	}

	/**
	 * wrapper method for more testability.
	 * 
	 * @param beanName - the bean name.
	 * @return Object
	 */
	protected Object getBean(String beanName)
	{
	    return getApplicationContext().getBean(beanName);
	}
	
}
