/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.deltaimpexservices.content;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.impex.ImpExResource;
import de.hybris.platform.servicelayer.impex.ImportResult;
import de.hybris.platform.servicelayer.impex.impl.DefaultImportService;
import de.hybris.platform.servicelayer.impex.impl.StreamBasedImpExResource;
import de.hybris.platform.servicelayer.media.MediaService;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.capgemini.deltaimpexservices.daos.DeltaImpexLoadProcessedFileDao;
import com.capgemini.deltaimpexservices.helper.FileReadHelper;
import com.capgemini.deltaimpexservices.model.DeltaImpexLoadCronJobModel;
import com.capgemini.deltaimpexservices.model.DeltaImpexLoadProcessedFileModel;


/**
 * Job performable class responsible for loading delta impexes. All impex files are to be located under the 'releases'
 * folder in the 'resources' folder of the capgeminideltaimpexservices extension. Files are processed in order. As soon
 * as a single file fails to load the process stops. Check the console logs for names of files that failed to run.
 */
public class DeltaImpexLoadPerformable extends AbstractJobPerformable<DeltaImpexLoadCronJobModel>
{
    /** defaultImportService bean. */
    private DefaultImportService defaultImportService;
    /** deltaImpexLoadProcessedFileDao bean. */
    private DeltaImpexLoadProcessedFileDao deltaImpexLoadProcessedFileDao;
    /** mediaService bean. */
    private MediaService mediaService;
    /** mediaCodeDateFormat bean. */
    private SimpleDateFormat mediaCodeDateFormat;
    /** catalogVersionService bean. */
    private CatalogVersionService catalogVersionService;
    /** catalogId and catalogVersionName for the impex saved as media. */
    private String catalogId, catalogVersionName;
    /** Logger for the class. */
    private static final Logger LOG = LoggerFactory.getLogger(DeltaImpexLoadPerformable.class.getName());

    /*
     * (non-Javadoc)
     *
     * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.
     * CronJobModel )
     */
    @Override
    public PerformResult perform(final DeltaImpexLoadCronJobModel cronJobModel)
    {
        final List<String> impexesToProcess = FileReadHelper.getResourceFileLines(cronJobModel.getManifestPath());
        final List<String> impexesAlreadyProcessed = new ArrayList<>();

        final List<DeltaImpexLoadProcessedFileModel> processedFiles = getDeltaImpexLoadProcessedFileDao()
                .getAllDeltaImpexLoadProcessedFile();
        final Map<String, DeltaImpexLoadProcessedFileModel> failureMap = new HashMap<>(1);

        getProcessedFileNames(processedFiles, impexesAlreadyProcessed, failureMap);

        final boolean allFilesRanSuccessful = processImpexesFromManifestPath(impexesToProcess, impexesAlreadyProcessed,
                failureMap);

        return new PerformResult((allFilesRanSuccessful ? CronJobResult.SUCCESS : CronJobResult.FAILURE), CronJobStatus.FINISHED);
    }

    /**
     * we just need the file names
     *
     * @param failureMap
     * @param impexesAlreadyProcessed
     *
     */
    private void getProcessedFileNames(final List<DeltaImpexLoadProcessedFileModel> processedFiles,
            final List<String> impexesAlreadyProcessed, final Map<String, DeltaImpexLoadProcessedFileModel> failureMap)
    {
        for (final DeltaImpexLoadProcessedFileModel processedFileModel : processedFiles)
        {
            if (Boolean.TRUE.equals(processedFileModel.getSuccess()))
            {
                impexesAlreadyProcessed.add(processedFileModel.getProcessedFile());
                LOG.debug("Previously successfully processed file: {}", processedFileModel.getProcessedFile());
            }
            else
            {
                failureMap.put(processedFileModel.getProcessedFile(), processedFileModel);
                LOG.debug("Failed file {} will be rerun", processedFileModel.getProcessedFile());
            }
        }

    }

    /**
     * @param failureMap
     * @param impexesAlreadyProcessed
     *
     */
    private boolean processImpexesFromManifestPath(final List<String> impexesToProcess,
            final List<String> impexesAlreadyProcessed, final Map<String, DeltaImpexLoadProcessedFileModel> failureMap)
    {
        boolean allFilesRanSuccessful = true;
        final Iterator<String> iterator = impexesToProcess.iterator();

        while (allFilesRanSuccessful && iterator.hasNext())
        {
            final String toProcess = iterator.next();

            // skip lines that are commented out and any file name alreadRy processed.
            if (StringUtils.startsWith(toProcess, "#") || impexesAlreadyProcessed.contains(toProcess))
            {
                continue;
            }

            // process file
            LOG.info("Processing file: {}", toProcess);
            
            InputStream fileToProcessReader = null;

            try
            {
                fileToProcessReader = DeltaImpexLoadPerformable.class.getResourceAsStream(toProcess);
                final ImpExResource script = new StreamBasedImpExResource(fileToProcessReader, "UTF-8");
                final ImportResult importResult = getDefaultImportService().importData(script);

                if (importResult.isSuccessful())
                {
                    updateProcessedFile(toProcess, true, failureMap);
                }
                else
                {
                    LOG.error("{} has errors.  Unresolved lines: {}", toProcess, importResult.getUnresolvedLines());
                    updateProcessedFile(toProcess, false, failureMap);
                    allFilesRanSuccessful = false;
                }
            }
            catch (final Exception e)
            {
                LOG.error("Exception occured while processing Delta Impex files", e);
                allFilesRanSuccessful = false;
            }
            finally
            {
                if (fileToProcessReader != null)
                {
                    try
                    {
                        fileToProcessReader.close();
                    }
                    catch (IOException e)
                    {
                        LOG.warn("Error closing inputStream", e);
                    }
                }
            }
        }

        return allFilesRanSuccessful;
    }

    protected void updateProcessedFile(final String fileProcessed, final boolean success,
            final Map<String, DeltaImpexLoadProcessedFileModel> failureMap)
    {
        LOG.debug("Adding: {} to database. Success = {}", fileProcessed, success);
        DeltaImpexLoadProcessedFileModel processedFile = null;
        if (failureMap.containsKey(fileProcessed))
        {
            processedFile = failureMap.get(fileProcessed);
            final MediaModel impexMedia = processedFile.getImpexFile();
            if (null != impexMedia)
            {
                modelService.remove(impexMedia);
                LOG.debug("Old media is removed....");
            }
        }
        else
        {
            processedFile = modelService.create(DeltaImpexLoadProcessedFileModel.class);
            processedFile.setProcessedFile(fileProcessed);
        }
        processedFile.setSuccess(Boolean.valueOf(success));
        modelService.save(processedFile);
        try
        {
            saveImpexMedia(fileProcessed, processedFile);
        }
        catch (final Exception e)
        {
            LOG.error("Error saving impex as media....", e);
        }
    }

    protected void saveImpexMedia(final String fullFileName, final DeltaImpexLoadProcessedFileModel processedFile)
    {
        final MediaModel newMedia = modelService.create(MediaModel.class);
        modelService.initDefaults(newMedia);
        newMedia.setOwner(processedFile);

        final String fileName = fullFileName.substring(fullFileName.lastIndexOf('/') + 1);
        newMedia.setCode(getMediaCodeDateFormat().format(new Date()) + "-" + fileName);
        newMedia.setCatalogVersion(getCatalogVersionService().getCatalogVersion(getCatalogId(), getCatalogVersionName()));
        newMedia.setRealFileName(fileName);
        processedFile.setImpexFile(newMedia);
        modelService.saveAll(newMedia, processedFile);

        final InputStream inputStream = DeltaImpexLoadPerformable.class.getResourceAsStream(fullFileName);
        try
        {
            getMediaService().setStreamForMedia(newMedia, inputStream);
            inputStream.close();
        }
        catch (final IOException e)
        {
            LOG.error("Error saving media.", e);
        }
        finally
        {
            IOUtils.closeQuietly(inputStream);
        }
    }

    /**
     * @return the defaultImportService
     */
    public DefaultImportService getDefaultImportService()
    {
        return defaultImportService;
    }

    /**
     * @param defaultImportService
     *            the defaultImportService to set
     */
    @Required
    public void setDefaultImportService(final DefaultImportService defaultImportService)
    {
        this.defaultImportService = defaultImportService;
    }

    /**
     * @return the deltaImpexLoadProcessedFileDao
     */
    public DeltaImpexLoadProcessedFileDao getDeltaImpexLoadProcessedFileDao()
    {
        return deltaImpexLoadProcessedFileDao;
    }

    /**
     * @param deltaImpexLoadProcessedFileDao
     *            the deltaImpexLoadProcessedFileDao to set
     */
    @Required
    public void setDeltaImpexLoadProcessedFileDao(final DeltaImpexLoadProcessedFileDao deltaImpexLoadProcessedFileDao)
    {
        this.deltaImpexLoadProcessedFileDao = deltaImpexLoadProcessedFileDao;
    }

    /**
     * @return the mediaService
     */
    public MediaService getMediaService()
    {
        return mediaService;
    }

    /**
     * @param mediaService
     *            the mediaService to set
     */
    @Required
    public void setMediaService(final MediaService mediaService)
    {
        this.mediaService = mediaService;
    }

    /**
     * @return the mediaCodeDateFormat
     */
    public SimpleDateFormat getMediaCodeDateFormat()
    {
        return mediaCodeDateFormat;
    }

    /**
     * @param mediaCodeDateFormat
     *            the mediaCodeDateFormat to set
     */
    @Required
    public void setMediaCodeDateFormat(final SimpleDateFormat mediaCodeDateFormat)
    {
        this.mediaCodeDateFormat = mediaCodeDateFormat;
    }

    /**
     * @return the catalogVersionService
     */
    public CatalogVersionService getCatalogVersionService()
    {
        return catalogVersionService;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
    {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @return the catalogId
     */
    public String getCatalogId()
    {
        return catalogId;
    }

    /**
     * @param catalogId
     *            the catalogId to set
     */
    @Required
    public void setCatalogId(final String catalogId)
    {
        this.catalogId = catalogId;
    }

    /**
     * @return the catalogVersionName
     */
    public String getCatalogVersionName()
    {
        return catalogVersionName;
    }

    /**
     * @param catalogVersionName
     *            the catalogVersionName to set
     */
    @Required
    public void setCatalogVersionName(final String catalogVersionName)
    {
        this.catalogVersionName = catalogVersionName;
    }
}
