/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.dashboard.services.dao;

import java.util.List;
import java.util.Set;

import de.hybris.platform.b2b.model.B2BCostCenterModel;
import de.hybris.platform.core.enums.QuoteState;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.store.BaseStoreModel;


/**
 *
 */
public interface DashboardDao
{

    /**
     * @param customer
     *            - the customer.
     * @param baseStore
     *            - the base store.
     * @param searchPageData
     *            - the search page data.
     * @return SearchPageData<OrderModel>
     */
    SearchPageData<OrderModel> getOrders(CustomerModel customer, BaseStoreModel baseStore, SearchPageData searchPageData);

    /**
     *
     * @param customer
     *            - the customer.
     * @param baseStore
     *            - the base store.
     * @param quoteStates
     *            - the quote states.
     * @param searchPageData
     *            - the search page data.
     * @return SearchPageData<QuoteModel>
     */
    SearchPageData<QuoteModel> getQuotes(CustomerModel customer, BaseStoreModel baseStore, Set<QuoteState> quoteStates,
            SearchPageData searchPageData);

    /**
     *
     * @param currency
     *            - the currency.
     * @return List<B2BCostCenterModel>
     */
    List<B2BCostCenterModel> getCostCenters(CurrencyModel currency);

}
