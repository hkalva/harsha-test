/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.dashboard.services.dao.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Required;

import com.capgemini.dashboard.services.dao.DashboardDao;

import de.hybris.platform.b2b.model.B2BCostCenterModel;
import de.hybris.platform.core.enums.QuoteState;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.search.paginated.PaginatedFlexibleSearchParameter;
import de.hybris.platform.servicelayer.search.paginated.PaginatedFlexibleSearchService;
import de.hybris.platform.store.BaseStoreModel;


public class DefaultDashboardDao implements DashboardDao
{

    private static final String FIND_ORDERS_BY_CUSTOMER_STORE_QUERY = "SELECT {" + OrderModel.PK + "}, {"
            + OrderModel.MODIFIEDTIME + "}, {" + OrderModel.CODE + "} FROM {" + OrderModel._TYPECODE + "}  WHERE {"
            + OrderModel.USER + "} = ?customer AND {" + OrderModel.VERSIONID + "} IS NULL AND {" + OrderModel.STORE
            + "} = ?baseStore ORDER BY {" + OrderModel.MODIFIEDTIME + "} DESC ";

    private static final String FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY = "SELECT {q1:" + QuoteModel.PK + "}  FROM {"
            + QuoteModel._TYPECODE + " as q1} WHERE {q1:" + QuoteModel.STATE + "} IN (?quoteStates) AND {q1:" + QuoteModel.USER
            + "} = ?customer AND {q1:" + QuoteModel.STORE + "} = ?baseStore AND {q1:" + QuoteModel.VERSION
            + "} = ({{ SELECT MAX({" + QuoteModel.VERSION + "}) FROM {" + QuoteModel._TYPECODE + "} WHERE {" + QuoteModel.CODE
            + "} = {q1:" + QuoteModel.CODE + "} AND {" + QuoteModel.STATE + "} IN (?quoteStates) AND {" + QuoteModel.USER
            + "} = ?customer AND {" + QuoteModel.STORE + "} = ?baseStore}}) ORDER BY {q1:" + QuoteModel.MODIFIEDTIME + "} DESC";

    private static final String FIND_COST_CENTER_QUERY = "SELECT {" + B2BCostCenterModel.PK + "} FROM  {"
            + B2BCostCenterModel._TYPECODE + "} WHERE  {" + B2BCostCenterModel.CURRENCY + "} = ?currency ";

    /**
     * the paginated flexible search service.
     */
    private PaginatedFlexibleSearchService paginatedFlexibleSearchService;

    /**
     * the flexible search service.
     */
    private FlexibleSearchService flexibleSearchService;

    @Override
    public SearchPageData<OrderModel> getOrders(final CustomerModel customer, final BaseStoreModel baseStore,
            final SearchPageData searchPageData)
    {
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(FIND_ORDERS_BY_CUSTOMER_STORE_QUERY);
        final Map<String, Object> parameters = new HashMap<>();
        parameters.put("customer", customer);
        parameters.put("baseStore", baseStore);
        searchQuery.addQueryParameters(parameters);
        final PaginatedFlexibleSearchParameter parameter = new PaginatedFlexibleSearchParameter();
        parameter.setSearchPageData(searchPageData);
        parameter.setFlexibleSearchQuery(searchQuery);
        return getPaginatedFlexibleSearchService().search(parameter);
    }

    @Override
    public SearchPageData<QuoteModel> getQuotes(final CustomerModel customer, final BaseStoreModel baseStore,
            final Set<QuoteState> quoteStates, final SearchPageData searchPageData)
    {
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY);
        final Map<String, Object> parameters = new HashMap<>();
        parameters.put("customer", customer);
        parameters.put("baseStore", baseStore);
        parameters.put("quoteStates", quoteStates);
        searchQuery.addQueryParameters(parameters);
        final PaginatedFlexibleSearchParameter parameter = new PaginatedFlexibleSearchParameter();
        parameter.setSearchPageData(searchPageData);
        parameter.setFlexibleSearchQuery(searchQuery);
        return getPaginatedFlexibleSearchService().search(parameter);
    }

    @Override
    public List<B2BCostCenterModel> getCostCenters(final CurrencyModel currency)
    {
        final SearchResult<B2BCostCenterModel> searchResult = getFlexibleSearchService().search(FIND_COST_CENTER_QUERY,
                Collections.singletonMap("currency", currency));
        return searchResult.getResult();
    }

    /**
     *
     * @param paginatedFlexibleSearchService
     *            - the paginated flexible search service.
     */
    @Required
    public void setPaginatedFlexibleSearchService(final PaginatedFlexibleSearchService paginatedFlexibleSearchService)
    {
        this.paginatedFlexibleSearchService = paginatedFlexibleSearchService;
    }

    /**
     *
     * @param flexibleSearchService
     *            - the flexible search service.
     */
    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
    {
        this.flexibleSearchService = flexibleSearchService;
    }

    protected PaginatedFlexibleSearchService getPaginatedFlexibleSearchService()
    {
        return paginatedFlexibleSearchService;
    }

    protected FlexibleSearchService getFlexibleSearchService()
    {
        return flexibleSearchService;
    }

}
