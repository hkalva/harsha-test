/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.dashboard.services.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Required;

import com.capgemini.dashboard.services.DashboardService;
import com.capgemini.dashboard.services.dao.DashboardDao;

import de.hybris.platform.b2b.model.B2BCostCenterModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.catalog.model.CompanyModel;
import de.hybris.platform.commerceservices.enums.QuoteAction;
import de.hybris.platform.commerceservices.order.strategies.QuoteStateSelectionStrategy;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.BaseStoreModel;


/**
 * implementing class to retrieve dashboard-pertinent data.
 */
public class DefaultDashboardService implements DashboardService
{

    /**
     * the dashboard dao.
     */
    private DashboardDao dashboardDao;

    /**
     * the quote state selection strategy.
     */
    private QuoteStateSelectionStrategy quoteStateSelectionStrategy;

    /**
     * the b2b unit service.
     */
    private B2BUnitService b2bUnitService;

    @Override
    public SearchPageData<OrderModel> getOrders(final CustomerModel customer, final BaseStoreModel baseStore,
            final SearchPageData searchPageData)
    {
        ServicesUtil.validateParameterNotNullStandardMessage("customer", customer);
        ServicesUtil.validateParameterNotNullStandardMessage("baseStore", baseStore);
        ServicesUtil.validateParameterNotNullStandardMessage("searchPageData", searchPageData);
        return getDashboardDao().getOrders(customer, baseStore, searchPageData);
    }

    @Override
    public SearchPageData<QuoteModel> getQuotes(final CustomerModel customer, final UserModel quoteUser,
            final BaseStoreModel baseStore, final SearchPageData searchPageData)
    {
        ServicesUtil.validateParameterNotNullStandardMessage("customer", customer);
        ServicesUtil.validateParameterNotNullStandardMessage("quoteUser", quoteUser);
        ServicesUtil.validateParameterNotNullStandardMessage("baseStore", baseStore);
        ServicesUtil.validateParameterNotNullStandardMessage("searchPageData", searchPageData);
        return getDashboardDao().getQuotes(customer, baseStore,
                getQuoteStateSelectionStrategy().getAllowedStatesForAction(QuoteAction.VIEW, quoteUser), searchPageData);
    }

    @Override
    public List<B2BCostCenterModel> getCostCenters(final CurrencyModel currency)
    {
        ServicesUtil.validateParameterNotNullStandardMessage("currency", currency);
        return getDashboardDao().getCostCenters(currency);
    }

    @Override
    public Set<B2BUnitModel> getUnits(final B2BCustomerModel customer)
    {
        final CompanyModel parent = getB2bUnitService().getParent(customer);
        return getB2bUnitService().getBranch(parent);
    }

    /**
     *
     * @param dashboardDao
     *            - the dashboard dao.
     */
    @Required
    public void setDashboardDao(final DashboardDao dashboardDao)
    {
        this.dashboardDao = dashboardDao;
    }

    /**
     *
     * @param quoteStateSelectionStrategy
     *            - the quote state selection strategy.
     */
    @Required
    public void setQuoteStateSelectionStrategy(final QuoteStateSelectionStrategy quoteStateSelectionStrategy)
    {
        this.quoteStateSelectionStrategy = quoteStateSelectionStrategy;
    }

    /**
     *
     * @param b2bUnitService
     *            - the b2b unit service.
     */
    @Required
    public void setB2bUnitService(final B2BUnitService b2bUnitService)
    {
        this.b2bUnitService = b2bUnitService;
    }

    /**
     *
     * @return DashboardDao
     */
    protected DashboardDao getDashboardDao()
    {
        return dashboardDao;
    }

    /**
     *
     * @return QuoteStateSelectionStrategy
     */
    protected QuoteStateSelectionStrategy getQuoteStateSelectionStrategy()
    {
        return quoteStateSelectionStrategy;
    }

    /**
     *
     * @return B2BUnitService
     */
    protected B2BUnitService getB2bUnitService()
    {
        return b2bUnitService;
    }

}
