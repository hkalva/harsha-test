/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.dashboard.services;

import java.util.List;
import java.util.Set;

import de.hybris.platform.b2b.model.B2BCostCenterModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.store.BaseStoreModel;


/**
 * the service to retrieve dashboard-related data.
 */
public interface DashboardService
{

    /**
     * retrieves the orders for the given customer and store.
     *
     * @param customer
     *            - the customer.
     * @param baseStore
     *            - the base store.
     * @param searchPageData
     *            - the search page data.
     * @return SearchPageData<OrderModel>
     */
    SearchPageData<OrderModel> getOrders(CustomerModel customer, BaseStoreModel baseStore, SearchPageData searchPageData);

    /**
     * retrieves the quotes for the given customer, quote user and store.
     *
     * @param customer
     *            - the customer.
     * @param quoteUser
     *            - the quote user.
     * @param baseStore
     *            - the base store.
     * @param searchPageData
     *            - the search page data.
     * @return SearchPageData<QuoteModel>
     */
    SearchPageData<QuoteModel> getQuotes(CustomerModel customer, UserModel quoteUser, BaseStoreModel baseStore,
            SearchPageData searchPageData);

    /**
     * retrieves the cost centers visible to the current user.
     *
     * @param currency
     *            - the currency.
     * @return List<B2BCostCenterModel>
     */
    List<B2BCostCenterModel> getCostCenters(CurrencyModel currency);

    /**
     * returns the b2b units associated with the current user.
     *
     * @param customer
     *            - the b2b customer.
     * @return Set<B2BUnitModel>
     */
    Set<B2BUnitModel> getUnits(B2BCustomerModel customer);
}
