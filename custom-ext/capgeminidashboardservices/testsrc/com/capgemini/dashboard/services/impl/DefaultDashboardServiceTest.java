package com.capgemini.dashboard.services.impl;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.dashboard.services.dao.DashboardDao;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.QuoteAction;
import de.hybris.platform.commerceservices.order.strategies.QuoteStateSelectionStrategy;
import de.hybris.platform.core.enums.QuoteState;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.store.BaseStoreModel;


@UnitTest
public class DefaultDashboardServiceTest
{

    /**
     * the service to test.
     */
    private DefaultDashboardService service;

    /**
     * the dashboard dao.
     */
    @Mock
    private DashboardDao dashboardDao;

    /**
     * the quote state selection strategy.
     */
    @Mock
    private QuoteStateSelectionStrategy quoteStateSelectionStrategy;

    /**
     * the customer.
     */
    @Mock
    private CustomerModel customer;

    /**
     * the store.
     */
    @Mock
    private BaseStoreModel baseStore;

    /**
     * the currency
     */
    @Mock
    private CurrencyModel currency;

    /**
     * the search page data for orders.
     */
    @Mock
    private SearchPageData<OrderModel> orderSearchPageData;

    /**
     * the search page data for quotes.
     */
    @Mock
    private SearchPageData<QuoteModel> quoteDearchPageData;

    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        service = new DefaultDashboardService();
        service.setDashboardDao(dashboardDao);
        service.setQuoteStateSelectionStrategy(quoteStateSelectionStrategy);
        Mockito.when(quoteStateSelectionStrategy.getAllowedStatesForAction(Mockito.any(QuoteAction.class),
                Mockito.any(UserModel.class))).thenReturn(Collections.singleton(QuoteState.CREATED));
        Mockito.when(dashboardDao.getOrders(Mockito.any(CustomerModel.class), Mockito.any(BaseStoreModel.class),
                Mockito.any(SearchPageData.class))).thenReturn(orderSearchPageData);
        Mockito.when(dashboardDao.getQuotes(Mockito.any(CustomerModel.class), Mockito.any(BaseStoreModel.class), Mockito.anySet(),
                Mockito.any(SearchPageData.class))).thenReturn(quoteDearchPageData);
        Mockito.when(dashboardDao.getCostCenters(Mockito.any(CurrencyModel.class))).thenReturn(Collections.emptyList());
    }

    /**
     * test getOrders.
     */
    @Test
    public void testGetOrders()
    {
        service.getOrders(customer, baseStore, orderSearchPageData);
        Mockito.verify(dashboardDao, Mockito.times(1)).getOrders(Mockito.any(CustomerModel.class),
                Mockito.any(BaseStoreModel.class), Mockito.any(SearchPageData.class));
    }

    /**
     * test getQuotes.
     */
    @Test
    public void testGetQuotes()
    {
        service.getQuotes(customer, customer, baseStore, quoteDearchPageData);
        Mockito.verify(dashboardDao, Mockito.times(1)).getQuotes(Mockito.any(CustomerModel.class),
                Mockito.any(BaseStoreModel.class), Mockito.anySet(), Mockito.any(SearchPageData.class));
    }

    /**
     * test getCostCenters.
     */
    @Test
    public void testCostCenters()
    {
        service.getCostCenters(currency);
        Mockito.verify(dashboardDao, Mockito.times(1)).getCostCenters(Mockito.any(CurrencyModel.class));
    }

}
