package com.lyonscg.b2borderprocess.actions.replenishment;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.worldpay.strategy.WorldpayAuthenticatedShopperIdStrategy;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2bacceleratorservices.model.process.ReplenishmentProcessModel;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;
import de.hybris.platform.processengine.helpers.ProcessParameterHelper;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;


/**
 * Test class for {@link LeaAuthorizePaymentAction}.
 */
@UnitTest
public class LeaAuthorizePaymentActionTest
{
    @InjectMocks
    private LeaAuthorizePaymentAction testClass;
    @Mock
    private CommerceCheckoutService b2bCommerceCheckoutService;
    @Mock
    private ImpersonationService impersonationService;
    @Mock
    private WorldpayAuthenticatedShopperIdStrategy worldpayAuthenticatedShopperIdStrategy;
    @Mock
    private ModelService modelService;
    @Mock
    private ProcessParameterHelper processParameterHelper;
    @Mock
    private ReplenishmentProcessModel process;
    @Mock
    private BusinessProcessParameterModel clonedCartParameter;
    @Mock
    private CartModel clonedCart;
    @Mock
    private CreditCardPaymentInfoModel cardPayment;
    @Mock
    private CustomerModel customer;
    @Mock
    private PaymentTransactionEntryModel paymentTransactionEntryModel;
    @Mock
    private PaypalPaymentInfoModel paypalPayment;
    @Mock
    private AbstractSimpleDecisionAction<ReplenishmentProcessModel> payPalAuthorizePaymentAction;

    /**
     * Setup test data.
     */
    @Before
    public void setUp()
    {
        testClass = new LeaAuthorizePaymentAction();
        MockitoAnnotations.initMocks(this);
        testClass.setB2bCommerceCheckoutService(b2bCommerceCheckoutService);
        testClass.setImpersonationService(impersonationService);
        testClass.setWorldpayAuthenticatedShopperIdStrategy(worldpayAuthenticatedShopperIdStrategy);
        testClass.setModelService(modelService);
        testClass.setProcessParameterHelper(processParameterHelper);
        Mockito.doReturn(clonedCartParameter).when(processParameterHelper).getProcessParameterByName(process, "cart");
        Mockito.doReturn(clonedCart).when(clonedCartParameter).getValue();

        Mockito.doReturn(customer).when(clonedCart).getUser();
        Mockito.doReturn("unitTestUser").when(worldpayAuthenticatedShopperIdStrategy).getAuthenticatedShopperId(customer);
        Mockito.doReturn(paymentTransactionEntryModel).when(b2bCommerceCheckoutService)
                .authorizePayment(Mockito.any(CartModel.class), Mockito.anyString(), Mockito.any());
    }

    /**
     * Unit test for
     * {@link LeaAuthorizePaymentAction#executeAction(de.hybris.platform.b2bacceleratorservices.model.process.ReplenishmentProcessModel)}
     * to return OK
     * 
     * @throws Exception
     * 
     */
    @Test
    public void executeActionOKTest() throws Exception
    {
        Mockito.doReturn(Transition.OK).when(impersonationService).executeInContext(Mockito.any(), Mockito.any());
        Assert.assertEquals(Transition.OK, testClass.executeAction(process));
    }

    /**
     * Unit test for
     * {@link LeaAuthorizePaymentAction#executeAction(de.hybris.platform.b2bacceleratorservices.model.process.ReplenishmentProcessModel)}
     * to return NOK
     * 
     * @throws Exception
     * 
     */
    @Test
    public void executeActionNOKTest() throws Exception
    {
        Mockito.doReturn(Transition.NOK).when(impersonationService).executeInContext(Mockito.any(), Mockito.any());
        Assert.assertEquals(Transition.NOK, testClass.executeAction(process));
    }

    /**
     * Unit test for
     * {@link LeaAuthorizePaymentAction#executeAction(de.hybris.platform.b2bacceleratorservices.model.process.ReplenishmentProcessModel)}
     * to return NOK on Exception.
     * 
     * @throws Exception
     * 
     */
    @Test
    public void executeActionNOKExceptionTest() throws Exception
    {
        Mockito.doThrow(Exception.class).when(impersonationService).executeInContext(Mockito.any(), Mockito.any());
        Assert.assertEquals(Transition.NOK, testClass.executeAction(process));
    }

    /**
     * Unit test for {@link LeaAuthorizePaymentAction#authPayment(ReplenishmentProcessModel, CartModel) for successful
     * transaction. *
     */
    @Test
    public void authPaymentCardSuccessTest()
    {
        Mockito.doReturn(cardPayment).when(clonedCart).getPaymentInfo();
        Mockito.doReturn(TransactionStatus.ACCEPTED.name()).when(paymentTransactionEntryModel).getTransactionStatus();
        Assert.assertEquals(Transition.OK, testClass.authPayment(clonedCart));
    }

    /**
     * Unit test for {@link LeaAuthorizePaymentAction#authPayment(ReplenishmentProcessModel, CartModel) for unsuccessful
     * transaction. *
     */
    @Test
    public void authPaymentCardNotSuccessTest()
    {
        Mockito.doReturn(cardPayment).when(clonedCart).getPaymentInfo();
        Mockito.doReturn("NOT" + TransactionStatus.ACCEPTED.name()).when(paymentTransactionEntryModel).getTransactionStatus();
        Assert.assertEquals(Transition.NOK, testClass.authPayment(clonedCart));
    }

/**
     * Unit test for {@link LeaAuthorizePaymentAction#authPayment(ReplenishmentProcessModel, CartModel) for successful
     * paypal transaction.
     * 
     * @throws Exception
     * @throws RetryLaterException
     */
    @Test
    public void authPaymentPaypalSuccessTest() throws RetryLaterException, Exception
    {
        Mockito.doReturn(paypalPayment).when(clonedCart).getPaymentInfo();
        Mockito.doReturn(Transition.OK).when(payPalAuthorizePaymentAction).executeAction(process);
        Assert.assertEquals(Transition.OK, testClass.executeAction(process));
        Mockito.verify(payPalAuthorizePaymentAction).executeAction(process);
    }

/**
     * Unit test for {@link LeaAuthorizePaymentAction#authPayment(ReplenishmentProcessModel, CartModel) for unsuccessful
     * paypal transaction.
     * 
     * @throws Exception
     * @throws RetryLaterException
     */
    @Test
    public void authPaymentPaypalNotSuccessTest() throws RetryLaterException, Exception
    {
        Mockito.doReturn(paypalPayment).when(clonedCart).getPaymentInfo();
        Mockito.doReturn(Transition.NOK).when(payPalAuthorizePaymentAction).executeAction(process);
        Assert.assertEquals(Transition.NOK, testClass.executeAction(process));
        Mockito.verify(payPalAuthorizePaymentAction).executeAction(process);
    }
}
