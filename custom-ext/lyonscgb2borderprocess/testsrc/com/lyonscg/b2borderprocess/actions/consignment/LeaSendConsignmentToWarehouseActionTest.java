package com.lyonscg.b2borderprocess.actions.consignment;

import static org.mockito.BDDMockito.given;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.warehouse.Process2WarehouseAdapter;


/**
 * Test class for {@link LeaSendConsignmentToWarehouseAction}.
 */
@UnitTest
public class LeaSendConsignmentToWarehouseActionTest
{
    @InjectMocks
    private LeaSendConsignmentToWarehouseAction testClass;
    @Mock
    private Process2WarehouseAdapter process2WarehouseAdapter;
    @Mock
    private ModelService modelService;
    @Mock
    private ConsignmentProcessModel process;
    @Mock
    private ConfigurationService configurationService;
    @Mock
    private Configuration configuration;
    @Mock
    private ConsignmentModel consignment;
    private String mockEnabledKey = "mockEnabledKey";

    /**
     * Setup test data.
     */
    @Before
    public void setUp()
    {
        testClass = new LeaSendConsignmentToWarehouseAction();
        testClass.setMockEnabledKey(mockEnabledKey);
        MockitoAnnotations.initMocks(this);
        given(configurationService.getConfiguration()).willReturn(configuration);
        given(process.getConsignment()).willReturn(consignment);
    }

    /**
     * Unit test for invoke mock consignment process
     * {@link LeaSendConsignmentToWarehouseAction#executeAction(de.hybris.platform.ordersplitting.model.ConsignmentProcessModel)}
     * .
     */
    @Test
    public void executeActionMockTest()
    {
        given(configuration.getBoolean(mockEnabledKey)).willReturn(true);
        testClass.executeAction(process);
        Mockito.verify(process).setWaitingForConsignment(true);
        Mockito.verify(modelService).save(process);
        Mockito.verify(process2WarehouseAdapter).prepareConsignment(consignment);
    }

    /**
     * Unit test
     * {@link LeaSendConsignmentToWarehouseAction#executeAction(de.hybris.platform.ordersplitting.model.ConsignmentProcessModel)}
     * .
     */
    @Test
    public void executeActionTest()
    {
        given(configuration.getBoolean(mockEnabledKey)).willReturn(false);
        testClass.executeAction(process);
        Mockito.verify(process).setWaitingForConsignment(true);
        Mockito.verify(modelService).save(process);
        Mockito.verify(process2WarehouseAdapter, Mockito.never()).prepareConsignment(consignment);
    }
}
