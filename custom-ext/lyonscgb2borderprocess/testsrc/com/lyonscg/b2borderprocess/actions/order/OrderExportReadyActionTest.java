package com.lyonscg.b2borderprocess.actions.order;

import static org.mockito.BDDMockito.given;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.warehouse.Process2WarehouseAdapter;


/**
 * Test class for {@link OrderExportReadyAction}.
 */
@UnitTest
public class OrderExportReadyActionTest
{
    @InjectMocks
    private OrderExportReadyAction testClass;
    @Mock
    private Process2WarehouseAdapter process2WarehouseAdapter;
    @Mock
    private ConfigurationService configurationService;
    @Mock
    private Configuration configuration;
    @Mock
    private OrderProcessModel orderProcess;
    @Mock
    private ModelService modelService;
    @Mock
    private OrderModel order;
    private String skipExportKey = "skipExportKey";

    /**
     * Setup test data.
     */
    @Before
    public void setUp()
    {
        testClass = new OrderExportReadyAction();
        testClass.setSkipExportKey(skipExportKey);
        MockitoAnnotations.initMocks(this);
        given(configurationService.getConfiguration()).willReturn(configuration);
        given(orderProcess.getOrder()).willReturn(order);
    }

    /**
     * Unit test for {@link OrderExportReadyAction#execute(OrderProcessModel)} to invoke wait for export.
     */
    @Test
    public void executeExportTest() throws RetryLaterException, Exception
    {
        given(configuration.getBoolean(skipExportKey)).willReturn(false);
        Assert.assertEquals("WAIT_FOR_EXPORT", testClass.execute(orderProcess));
        Mockito.verify(order).setStatus(OrderStatus.EXPORT_READY);
        Mockito.verify(modelService).save(order);
    }

    /**
     * Unit test for {@link OrderExportReadyAction#execute(OrderProcessModel)} to continue without export.
     */
    @Test
    public void executeNoExportTest() throws RetryLaterException, Exception
    {
        given(configuration.getBoolean(skipExportKey)).willReturn(true);
        Assert.assertEquals("NO_EXPORT", testClass.execute(orderProcess));
        Mockito.verify(order, Mockito.never()).setStatus(OrderStatus.EXPORT_READY);
        Mockito.verify(modelService, Mockito.never()).save(order);
    }
}
