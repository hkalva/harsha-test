package com.lyonscg.b2borderprocess.actions.order;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.paypal.hybris.model.PaypalPaymentInfoModel;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;


/**
 * Test class for {@link LeaCheckAuthorizeOrderPaymentAction}.
 */
@UnitTest
public class LeaCheckAuthorizeOrderPaymentActionTest
{
    private static final String WAIT = "WAIT";
    private static final String NOK = "NOK";
    private static final String OK = "OK";
    @InjectMocks
    private LeaCheckAuthorizeOrderPaymentAction testClass;
    @Mock
    private AbstractAction<OrderProcessModel> worldpayCheckAuthorizeOrderPaymentAction;
    @Mock
    private OrderProcessModel process;
    @Mock
    private OrderModel order;
    @Mock
    private InvoicePaymentInfoModel invoicePaymentInfoModel;
    @Mock
    private CreditCardPaymentInfoModel creditCardPaymentInfoModel;
    @Mock
    private PaypalPaymentInfoModel paypalPaymentInfoModel;
    @Mock
    private PaymentTransactionModel paymentTransactionModel;
    @Mock
    private PaymentTransactionEntryModel paymentTransactionEntryModel;
    @Mock
    private ModelService modelService;

    /**
     * Setup test data.
     */
    @Before
    public void setup()
    {
        testClass = new LeaCheckAuthorizeOrderPaymentAction();
        MockitoAnnotations.initMocks(this);
        Mockito.when(process.getOrder()).thenReturn(order);
    }

    /**
     * Unit test for {@link LeaCheckAuthorizeOrderPaymentAction#execute(OrderProcessModel)} with
     * InvoicePaymentInfoModel.
     *
     * @throws Exception
     * @throws RetryLaterException
     */
    @Test
    public void executeInvoiceTest() throws RetryLaterException, Exception
    {
        Mockito.when(order.getPaymentInfo()).thenReturn(invoicePaymentInfoModel);
        Assert.assertEquals(OK, testClass.execute(process));
    }

    /**
     * Unit test for {@link LeaCheckAuthorizeOrderPaymentAction#execute(OrderProcessModel)} with
     * CreditCardPaymentInfoModel.
     *
     * @throws Exception
     * @throws RetryLaterException
     */
    @Test
    public void executeCCTest() throws RetryLaterException, Exception
    {
        Mockito.when(order.getPaymentInfo()).thenReturn(creditCardPaymentInfoModel);
        verifyActionForCC(OK);
        verifyActionForCC(NOK);
        verifyActionForCC(WAIT);
    }

    private void verifyActionForCC(String transition) throws Exception
    {
        Mockito.when(worldpayCheckAuthorizeOrderPaymentAction.execute(process)).thenReturn(transition);
        Assert.assertEquals(transition, testClass.execute(process));
    }

    /**
     * Unit OK test for {@link LeaCheckAuthorizeOrderPaymentAction#execute(OrderProcessModel)} with
     * PaypalPaymentInfoModel.
     *
     * @throws Exception
     * @throws RetryLaterException
     */
    @Test
    public void executePaypalOKTest() throws RetryLaterException, Exception
    {
        Mockito.when(order.getPaymentInfo()).thenReturn(paypalPaymentInfoModel);
        Mockito.when(order.getPaymentTransactions()).thenReturn(Collections.singletonList(paymentTransactionModel));
        Mockito.when(paymentTransactionModel.getEntries()).thenReturn(Collections.singletonList(paymentTransactionEntryModel));
        Mockito.when(paymentTransactionEntryModel.getType()).thenReturn(PaymentTransactionType.AUTHORIZATION);
        Mockito.when(paymentTransactionEntryModel.getTransactionStatus()).thenReturn(TransactionStatus.ACCEPTED.name());
        Assert.assertEquals(OK, testClass.execute(process));
    }

    /**
     * Unit NOK test for {@link LeaCheckAuthorizeOrderPaymentAction#execute(OrderProcessModel)} with
     * PaypalPaymentInfoModel.
     *
     * @throws Exception
     * @throws RetryLaterException
     */
    @Test
    public void executePaypalNOKTest() throws RetryLaterException, Exception
    {
        Mockito.when(order.getPaymentInfo()).thenReturn(paypalPaymentInfoModel);
        Mockito.when(order.getPaymentTransactions()).thenReturn(Collections.singletonList(paymentTransactionModel));
        Mockito.when(paymentTransactionModel.getEntries()).thenReturn(Collections.singletonList(paymentTransactionEntryModel));

        Mockito.when(paymentTransactionEntryModel.getType()).thenReturn(PaymentTransactionType.CAPTURE);
        Mockito.when(paymentTransactionEntryModel.getTransactionStatus()).thenReturn(TransactionStatus.ACCEPTED.name());
        Assert.assertEquals(NOK, testClass.execute(process));

        Mockito.when(paymentTransactionEntryModel.getType()).thenReturn(PaymentTransactionType.AUTHORIZATION);
        Mockito.when(paymentTransactionEntryModel.getTransactionStatus()).thenReturn(TransactionStatus.ERROR.name());
        Assert.assertEquals(NOK, testClass.execute(process));
    }
}
