/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.lyonscg.b2borderprocess.constants;

/**
 * Global class for all Lyonscgb2borderprocess constants. You can add global constants for your extension into this
 * class.
 */
public final class Lyonscgb2borderprocessConstants extends GeneratedLyonscgb2borderprocessConstants
{
    public static final String EXTENSIONNAME = "lyonscgb2borderprocess";

    private Lyonscgb2borderprocessConstants()
    {
        //empty to avoid instantiating this constant class
    }

    // implement here constants used by this extension
}
