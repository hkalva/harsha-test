package com.lyonscg.b2borderprocess.event.listener;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.event.AbstractSiteEventListener;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.events.AuthorizationFailedEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.ticket.enums.CsTicketCategory;
import de.hybris.platform.ticket.enums.CsTicketPriority;
import de.hybris.platform.ticket.events.model.CsCustomerEventModel;
import de.hybris.platform.ticket.model.CsTicketModel;
import de.hybris.platform.ticket.service.TicketBusinessService;
import de.hybris.platform.util.localization.Localization;

import org.springframework.beans.factory.annotation.Required;


/**
 * AuthorizationFailedEvent Listener.
 *
 */
public class AuthorizationFailedEventListener extends AbstractSiteEventListener<AuthorizationFailedEvent>
{
	private ModelService modelService;
	private BusinessProcessService businessProcessService;
	private String processName;
	private boolean createCsTicket;
	private TicketBusinessService ticketBusinessService;

	@Override
	protected void onSiteEvent(final AuthorizationFailedEvent authorizationFailedEvent)
	{
		final OrderModel orderModel = authorizationFailedEvent.getProcess().getOrder();
		final OrderProcessModel orderProcessModel = (OrderProcessModel) getBusinessProcessService().createProcess(
				getProcessName() + "-" + orderModel.getCode() + "-" + System.currentTimeMillis(), getProcessName());
		orderProcessModel.setOrder(orderModel);
		getModelService().save(orderProcessModel);
		getBusinessProcessService().startProcess(orderProcessModel);
		if (isCreateCsTicket())
		{
			createTicket(orderModel, CsTicketCategory.PROBLEM, CsTicketPriority.HIGH);
		}
	}

	@Override
	protected boolean shouldHandleEvent(final AuthorizationFailedEvent authorizationFailedEvent)
	{
		final AbstractOrderModel order = authorizationFailedEvent.getProcess().getOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		final BaseSiteModel site = order.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return SiteChannel.B2B.equals(site.getChannel());
	}
/**
 * Creating customer service ticket and setting its priority, category,order details and customer details.
 */
	protected CsTicketModel createTicket(final OrderModel order, final CsTicketCategory category, final CsTicketPriority priority)
	{
		final String subject = Localization.getLocalizedString("message.ticket.order.auth.failed.title", new Object[]
		{ order.getCode() });
		final String description = Localization.getLocalizedString("message.ticket.order.auth.failed.content", new Object[]
		{ order.getCode() });
		final CsTicketModel newTicket = getModelService().create(CsTicketModel.class);
		newTicket.setHeadline(subject);
		newTicket.setCategory(category);
		newTicket.setPriority(priority);
		newTicket.setOrder(order);
		newTicket.setCustomer(order.getUser());

		final CsCustomerEventModel newTicketEvent = new CsCustomerEventModel();
		newTicketEvent.setText(description);

		return getTicketBusinessService().createTicket(newTicket, newTicketEvent);
	}


	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}


	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}


	protected String getProcessName()
	{
		return processName;
	}


	@Required
	public void setProcessName(final String processName)
	{
		this.processName = processName;
	}


	protected boolean isCreateCsTicket()
	{
		return createCsTicket;
	}


	public void setCreateCsTicket(final boolean createCsTicket)
	{
		this.createCsTicket = createCsTicket;
	}


	protected TicketBusinessService getTicketBusinessService()
	{
		return ticketBusinessService;
	}


	@Required
	public void setTicketBusinessService(final TicketBusinessService ticketBusinessService)
	{
		this.ticketBusinessService = ticketBusinessService;
	}
}
