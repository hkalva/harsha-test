package com.lyonscg.b2borderprocess.event.listener;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.event.AbstractSiteEventListener;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.events.OrderCompletedEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.springframework.beans.factory.annotation.Required;


/**
 * OrderCompletedEvent Listener.
 *
 */
public class OrderCompletedEventListener extends AbstractSiteEventListener<OrderCompletedEvent>
{
	private ModelService modelService;
	private BusinessProcessService businessProcessService;
	private String processName;

	@Override
	protected void onSiteEvent(final OrderCompletedEvent orderCompletedEvent)
	{
		final OrderModel orderModel = orderCompletedEvent.getProcess().getOrder();
		final OrderProcessModel orderProcessModel = (OrderProcessModel) getBusinessProcessService().createProcess(
				getProcessName() + "-" + orderModel.getCode() + "-" + System.currentTimeMillis(), getProcessName());
		orderProcessModel.setOrder(orderModel);
		getModelService().save(orderProcessModel);
		getBusinessProcessService().startProcess(orderProcessModel);
	}

	@Override
	protected boolean shouldHandleEvent(final OrderCompletedEvent orderCompletedEvent)
	{
		final AbstractOrderModel order = orderCompletedEvent.getProcess().getOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		final BaseSiteModel site = order.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return SiteChannel.B2B.equals(site.getChannel());
	}

	protected ModelService getModelService()
	{
		return modelService;
	}


	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}


	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}


	protected String getProcessName()
	{
		return processName;
	}


	@Required
	public void setProcessName(final String processName)
	{
		this.processName = processName;
	}

}
