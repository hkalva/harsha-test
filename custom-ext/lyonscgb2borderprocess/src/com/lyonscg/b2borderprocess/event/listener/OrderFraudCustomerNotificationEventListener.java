package com.lyonscg.b2borderprocess.event.listener;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.event.AbstractSiteEventListener;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.events.OrderFraudCustomerNotificationEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.ticket.enums.CsTicketCategory;
import de.hybris.platform.ticket.enums.CsTicketPriority;
import de.hybris.platform.ticket.events.model.CsCustomerEventModel;
import de.hybris.platform.ticket.model.CsTicketModel;
import de.hybris.platform.ticket.service.TicketBusinessService;
import de.hybris.platform.util.localization.Localization;

import org.springframework.beans.factory.annotation.Required;


/**
 * OrderFraudCustomerNotificationEvent Listener.
 *
 */
public class OrderFraudCustomerNotificationEventListener extends AbstractSiteEventListener<OrderFraudCustomerNotificationEvent>
{
	private ModelService modelService;
	private BusinessProcessService businessProcessService;
	private String processName;
	private boolean createCsTicket;
	private TicketBusinessService ticketBusinessService;

	@Override
	protected void onSiteEvent(final OrderFraudCustomerNotificationEvent orderFraudCustomerNotificationEvent)
	{
		final OrderModel orderModel = orderFraudCustomerNotificationEvent.getProcess().getOrder();
		final OrderProcessModel orderProcessModel = (OrderProcessModel) getBusinessProcessService().createProcess(
				getProcessName() + "-" + orderModel.getCode() + "-" + System.currentTimeMillis(), getProcessName());
		orderProcessModel.setOrder(orderModel);
		getModelService().save(orderProcessModel);
		getBusinessProcessService().startProcess(orderProcessModel);
		if (isCreateCsTicket())
		{
			createTicket(orderModel, CsTicketCategory.FRAUD, CsTicketPriority.HIGH);
		}
	}

	@Override
	protected boolean shouldHandleEvent(final OrderFraudCustomerNotificationEvent orderFraudCustomerNotificationEvent)
	{
		final AbstractOrderModel order = orderFraudCustomerNotificationEvent.getProcess().getOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		final BaseSiteModel site = order.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return SiteChannel.B2B.equals(site.getChannel());
	}

	/**
	 * creating ticket for the issue.
	 * @param order - taking order model.
	 * @param category - category of the issue.
	 * @param priority - priority of the issue. 
	 * @return - created ricket.
	 */
	protected CsTicketModel createTicket(final OrderModel order, final CsTicketCategory category, final CsTicketPriority priority)
	{
		final String subject = Localization.getLocalizedString("message.ticket.order.fraud.title", new Object[]
		{ order.getCode() });
		final String description = Localization.getLocalizedString("message.ticket.order.fraud.content", new Object[]
		{ order.getCode() });
		final CsTicketModel newTicket = getModelService().create(CsTicketModel.class);
		newTicket.setHeadline(subject);
		newTicket.setCategory(category);
		newTicket.setPriority(priority);
		newTicket.setOrder(order);
		newTicket.setCustomer(order.getUser());

		final CsCustomerEventModel newTicketEvent = new CsCustomerEventModel();
		newTicketEvent.setText(description);

		return getTicketBusinessService().createTicket(newTicket, newTicketEvent);
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the businessProcessService
	 */
	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * @param businessProcessService
	 *           the businessProcessService to set
	 */
	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * @return the processName
	 */
	protected String getProcessName()
	{
		return processName;
	}

	/**
	 * @param processName
	 *           the processName to set
	 */
	@Required
	public void setProcessName(final String processName)
	{
		this.processName = processName;
	}

	/**
	 * @return the createCsTicket
	 */
	protected boolean isCreateCsTicket()
	{
		return createCsTicket;
	}

	/**
	 * @param createCsTicket
	 *           the createCsTicket to set
	 */
	public void setCreateCsTicket(final boolean createCsTicket)
	{
		this.createCsTicket = createCsTicket;
	}

	/**
	 * @return the ticketBusinessService
	 */
	protected TicketBusinessService getTicketBusinessService()
	{
		return ticketBusinessService;
	}

	/**
	 * @param ticketBusinessService
	 *           the ticketBusinessService to set
	 */
	@Required
	public void setTicketBusinessService(final TicketBusinessService ticketBusinessService)
	{
		this.ticketBusinessService = ticketBusinessService;
	}
}
