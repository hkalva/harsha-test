package com.lyonscg.b2borderprocess.replenishment.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.util.localization.Localization;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Email context for AuthFailedEmail.
 *
 */
public class AuthFailedEmailContext extends AbstractEmailContext<OrderProcessModel>
{
	private static final String TO_EMAIL_ID = "auth.failed.email.to.id";
	private static final String TO_EMAIL_DISPLAY_NAME = "auth.failed.email.to.name";
	private Converter<OrderModel, OrderData> orderConverter;
	private OrderData orderData;
	private String orderCode;
	private String orderGuid;
	private boolean guest;
	private String storeName;
	private SessionService sessionService;
	private CommonI18NService commonI18NService;

	@Override
	public void init(final OrderProcessModel orderProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(orderProcessModel, emailPageModel);
		orderData = getOrderConverter().convert(orderProcessModel.getOrder());
		orderCode = orderProcessModel.getOrder().getCode();
		orderGuid = orderProcessModel.getOrder().getGuid();
		guest = CustomerType.GUEST.equals(getCustomer(orderProcessModel).getType());
		storeName = orderProcessModel.getOrder().getStore().getName();
		populateToAddress(orderProcessModel);
	}
/**
 *Populate information for the destination email address.
 *@param orderProcessModel is generated when order is processing.
 */
	protected void populateToAddress(final OrderProcessModel orderProcessModel)
	{
		getSessionService().executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public void executeWithoutResult()
			{
				final LanguageModel language = getEmailLanguage(orderProcessModel);
				getCommonI18NService().setCurrentLanguage(language);
				final Configuration configuration = getConfigurationService().getConfiguration();
				final String configPrefix = StringUtils.isBlank(orderData.getSite()) ? StringUtils.EMPTY : orderData.getSite() + ".";
				String toEmailId = configuration.getString(configPrefix + TO_EMAIL_ID);
				if (StringUtils.isBlank(toEmailId))
				{
					toEmailId = configuration.getString(TO_EMAIL_ID);
				}
				put(EMAIL, toEmailId);
				final String toDisplayName = Localization.getLocalizedString(TO_EMAIL_DISPLAY_NAME);
				put(DISPLAY_NAME, toDisplayName);
			}
		});

	}

	@Override
	protected BaseSiteModel getSite(final OrderProcessModel orderProcessModel)
	{
		return orderProcessModel.getOrder().getSite();
	}

	@Override
	protected CustomerModel getCustomer(final OrderProcessModel orderProcessModel)
	{
		return (CustomerModel) orderProcessModel.getOrder().getUser();
	}

	protected Converter<OrderModel, OrderData> getOrderConverter()
	{
		return orderConverter;
	}

	@Required
	public void setOrderConverter(final Converter<OrderModel, OrderData> orderConverter)
	{
		this.orderConverter = orderConverter;
	}

	public OrderData getOrder()
	{
		return orderData;
	}

	@Override
	protected LanguageModel getEmailLanguage(final OrderProcessModel orderProcessModel)
	{
		return orderProcessModel.getOrder().getLanguage();
	}

	public OrderData getOrderData()
	{
		return orderData;
	}

	public void setOrderData(final OrderData orderData)
	{
		this.orderData = orderData;
	}

	public String getOrderCode()
	{
		return orderCode;
	}

	public void setOrderCode(final String orderCode)
	{
		this.orderCode = orderCode;
	}

	public String getOrderGuid()
	{
		return orderGuid;
	}

	public void setOrderGuid(final String orderGuid)
	{
		this.orderGuid = orderGuid;
	}

	public boolean isGuest()
	{
		return guest;
	}

	public void setGuest(final boolean guest)
	{
		this.guest = guest;
	}

	public String getStoreName()
	{
		return storeName;
	}

	public void setStoreName(final String storeName)
	{
		this.storeName = storeName;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}


	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}


	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}


	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}
}

