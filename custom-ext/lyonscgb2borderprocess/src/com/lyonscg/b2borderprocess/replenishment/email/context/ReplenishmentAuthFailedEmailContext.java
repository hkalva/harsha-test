package com.lyonscg.b2borderprocess.replenishment.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.b2bacceleratorfacades.order.data.ScheduledCartData;
import de.hybris.platform.b2bacceleratorservices.model.process.ReplenishmentProcessModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.orderscheduling.model.CartToOrderCronJobModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.util.localization.Localization;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Email context for ReplenishmentAuthFailedEmail.
 *
 */
public class ReplenishmentAuthFailedEmailContext extends AbstractEmailContext<ReplenishmentProcessModel>
{
	private Converter<CartToOrderCronJobModel, ScheduledCartData> scheduledCartConverter;
	private ScheduledCartData scheduledCartData;
	private SessionService sessionService;
	private CommonI18NService commonI18NService;
	private static final String TO_EMAIL_ID = "replenishment.auth.failed.email.to.id";
	private static final String TO_EMAIL_DISPLAY_NAME = "replenishment.auth.failed.email.to.name";

	@Override
	public void init(final ReplenishmentProcessModel replenishmentProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(replenishmentProcessModel, emailPageModel);
		scheduledCartData = getScheduledCartConverter().convert(replenishmentProcessModel.getCartToOrderCronJob());
	}

	/**
	 * Populates to address details.
	 *@param replenishmentProcessModel - getting process model.
	 */
	protected void populateToAddress(final ReplenishmentProcessModel replenishmentProcessModel)
	{
		getSessionService().executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public void executeWithoutResult()
			{
				final CartModel cart = replenishmentProcessModel.getCartToOrderCronJob().getCart();
				final LanguageModel language = getLanguage(cart.getUser());
				getCommonI18NService().setCurrentLanguage(language);
				final Configuration configuration = getConfigurationService().getConfiguration();
				final BaseSiteModel site = cart.getSite();
				final String configPrefix = site == null ? StringUtils.EMPTY : site.getUid() + ".";
				String toEmailId = configuration.getString(configPrefix + TO_EMAIL_ID);
				if (StringUtils.isBlank(toEmailId))
				{
					toEmailId = configuration.getString(TO_EMAIL_ID);
				}
				put(EMAIL, toEmailId);
				final String toDisplayName = Localization.getLocalizedString(TO_EMAIL_DISPLAY_NAME);
				put(DISPLAY_NAME, toDisplayName);
			}
		});

	}



	/**
	 * Getting session language.
	 * @param customer - current user.
	 * @return language which user is using to browse.
	 */
	protected LanguageModel getLanguage(final UserModel customer)
	{
		return customer == null ? null : customer.getSessionLanguage();
	}

	@Override
	protected BaseSiteModel getSite(final ReplenishmentProcessModel replenishmentProcessModel)
	{
		return replenishmentProcessModel.getCartToOrderCronJob().getCart().getSite();
	}

	@Override
	protected CustomerModel getCustomer(final ReplenishmentProcessModel replenishmentProcessModel)
	{
		return (CustomerModel) replenishmentProcessModel.getCartToOrderCronJob().getCart().getUser();
	}


	public ScheduledCartData getScheduledCartData()
	{
		return scheduledCartData;
	}

	protected Converter<CartToOrderCronJobModel, ScheduledCartData> getScheduledCartConverter()
	{
		return scheduledCartConverter;
	}


	@Required
	public void setScheduledCartConverter(final Converter<CartToOrderCronJobModel, ScheduledCartData> scheduledCartConverter)
	{
		this.scheduledCartConverter = scheduledCartConverter;
	}

	@Override
	protected LanguageModel getEmailLanguage(final ReplenishmentProcessModel replenishmentProcessModel)
	{
		return replenishmentProcessModel.getCartToOrderCronJob().getCart().getUser().getSessionLanguage();
	}


	protected SessionService getSessionService()
	{
		return sessionService;
	}


	protected void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}


	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}


	protected void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}
}
