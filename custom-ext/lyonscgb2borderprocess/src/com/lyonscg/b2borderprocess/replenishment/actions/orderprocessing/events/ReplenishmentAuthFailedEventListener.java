package com.lyonscg.b2borderprocess.replenishment.actions.orderprocessing.events;

import de.hybris.platform.b2bacceleratorservices.model.process.ReplenishmentProcessModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.event.AbstractSiteEventListener;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.orderscheduling.model.CartToOrderCronJobModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.ticket.enums.CsTicketCategory;
import de.hybris.platform.ticket.enums.CsTicketPriority;
import de.hybris.platform.ticket.events.model.CsCustomerEventModel;
import de.hybris.platform.ticket.model.CsTicketModel;
import de.hybris.platform.ticket.service.TicketBusinessService;
import de.hybris.platform.util.localization.Localization;

import org.springframework.beans.factory.annotation.Required;


/**
 * ReplenishmentAuthFailedEventListener Listener for LeaAuthorizationFailedEvent.
 *
 */
public class ReplenishmentAuthFailedEventListener extends AbstractSiteEventListener<LeaAuthorizationFailedEvent>
{
	private ModelService modelService;
	private BusinessProcessService businessProcessService;
	private String processName;
	private boolean createCsTicket;
	private TicketBusinessService ticketBusinessService;

	@Override
	protected void onSiteEvent(final LeaAuthorizationFailedEvent leaAuthorizationFailedEvent)
	{
		final ReplenishmentProcessModel process = leaAuthorizationFailedEvent.getProcess();
		final CartToOrderCronJobModel cartToOrderCronJob = process.getCartToOrderCronJob();
		final CartModel cartModel = cartToOrderCronJob.getCart();
		final ReplenishmentProcessModel replenishmentProcessModel = (ReplenishmentProcessModel) getBusinessProcessService()
				.createProcess(getProcessName() + "-" + cartModel.getCode() + "-" + System.currentTimeMillis(), getProcessName());
		replenishmentProcessModel.setCartToOrderCronJob(cartToOrderCronJob);
		replenishmentProcessModel.setLanguage(cartToOrderCronJob.getSessionLanguage());
		getModelService().save(replenishmentProcessModel);
		getBusinessProcessService().startProcess(replenishmentProcessModel);
		if (isCreateCsTicket())
		{
			createTicket(cartModel, CsTicketCategory.PROBLEM, CsTicketPriority.HIGH);
		}
	}

	@Override
	protected boolean shouldHandleEvent(final LeaAuthorizationFailedEvent leaAuthorizationFailedEvent)
	{
		final CartModel cartModel = leaAuthorizationFailedEvent.getProcess().getCartToOrderCronJob().getCart();
		ServicesUtil.validateParameterNotNullStandardMessage("event.cart", cartModel);
		final BaseSiteModel site = cartModel.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.cart.site", site);
		return SiteChannel.B2B.equals(site.getChannel());
	}

	/**
	 * Creating ticket for the issue.
	 * @param order - to get the order you are working on.
	 * @param category - to know product category.
	 * @param priority - to know issue priority.
	 * @return - creates ticket for the issue.
	 */
	protected CsTicketModel createTicket(final CartModel order, final CsTicketCategory category, final CsTicketPriority priority)
	{
		final String subject = Localization.getLocalizedString("message.ticket.replenishment.auth.failed.title", new Object[]
		{ order.getCode() });
		final String description = Localization.getLocalizedString("message.ticket.replenishment.auth.failed.content", new Object[]
		{ order.getCode() });
		final CsTicketModel newTicket = getModelService().create(CsTicketModel.class);
		newTicket.setHeadline(subject);
		newTicket.setCategory(category);
		newTicket.setPriority(priority);
		newTicket.setOrder(order);
		newTicket.setCustomer(order.getUser());

		final CsCustomerEventModel newTicketEvent = new CsCustomerEventModel();
		newTicketEvent.setText(description);

		return getTicketBusinessService().createTicket(newTicket, newTicketEvent);
	}


	protected ModelService getModelService()
	{
		return modelService;
	}


	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}


	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}


	protected String getProcessName()
	{
		return processName;
	}


	@Required
	public void setProcessName(final String processName)
	{
		this.processName = processName;
	}

	/** checks if it says to create ticket or not.
	 * @return the createCsTicket.
	 */
	protected boolean isCreateCsTicket()
	{
		return createCsTicket;
	}


	public void setCreateCsTicket(final boolean createCsTicket)
	{
		this.createCsTicket = createCsTicket;
	}


	protected TicketBusinessService getTicketBusinessService()
	{
		return ticketBusinessService;
	}


	@Required
	public void setTicketBusinessService(final TicketBusinessService ticketBusinessService)
	{
		this.ticketBusinessService = ticketBusinessService;
	}
}
