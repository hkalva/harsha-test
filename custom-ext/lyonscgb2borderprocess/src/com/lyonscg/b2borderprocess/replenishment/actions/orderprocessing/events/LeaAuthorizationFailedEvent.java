package com.lyonscg.b2borderprocess.replenishment.actions.orderprocessing.events;

import de.hybris.platform.b2bacceleratorservices.model.process.ReplenishmentProcessModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


/**
 * Event to notify Replenishment Order Authorization Failure.
 *
 */
public class LeaAuthorizationFailedEvent extends AbstractEvent
{
	private static final long serialVersionUID = 2L;
	private final ReplenishmentProcessModel process;

	/**
	 * Constructor to create LeaAuthorizationFailedEvent.
	 * 
	 * @param replenishmentProcessModel
	 *           - replenishmentProcessModel for which Authorization failed
	 */
	public LeaAuthorizationFailedEvent(final ReplenishmentProcessModel replenishmentProcessModel)
	{
		this.process = replenishmentProcessModel;
	}

	/**
	 * @return the process
	 */
	public ReplenishmentProcessModel getProcess()
	{
		return process;
	}

}
