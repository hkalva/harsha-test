package com.lyonscg.b2borderprocess.replenishment.actions;

import de.hybris.platform.b2bacceleratorservices.model.process.ReplenishmentProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.lyonscg.b2borderprocess.replenishment.actions.orderprocessing.events.LeaAuthorizationFailedEvent;


/**
 * Triggers ReplenishmentAuthorizationFailedEvent.
 */
public class SendReplenishmentAuthFailedNotificationAction extends AbstractProceduralAction<ReplenishmentProcessModel>
{
	private static final Logger LOG = Logger.getLogger(SendReplenishmentAuthFailedNotificationAction.class);
	private EventService eventService;

	@Override
	public void executeAction(final ReplenishmentProcessModel process)
	{
		getEventService().publishEvent(new LeaAuthorizationFailedEvent(process));
		LOG.info("Process: " + process.getCode() + " in step " + getClass());
	}


	protected EventService getEventService()
	{
		return eventService;
	}


	@Required
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

}
