package com.lyonscg.b2borderprocess.actions.consignment;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.warehouse.Process2WarehouseAdapter;


/**
 * Custom implementation to prevent calls to MockProcess2WarehouseAdapter.
 */
public class LeaSendConsignmentToWarehouseAction extends AbstractProceduralAction<ConsignmentProcessModel>
{
    private static final Logger LOG = Logger.getLogger(LeaSendConsignmentToWarehouseAction.class);

    private Process2WarehouseAdapter process2WarehouseAdapter;
    private ConfigurationService configurationService;
    private String mockEnabledKey;

    /**
     * Calls {@link ConsignmentProcessModel#setWaitingForConsignment(boolean)} with true as a param and calls Mock
     * prepareConsignment if isMockEnabled is true.
     *
     *
     * @param process
     *            - {@link ConsignmentProcessModel}
     */
    @Override
    public void executeAction(final ConsignmentProcessModel process)
    {
        if (getConfigurationService().getConfiguration().getBoolean(getMockEnabledKey()))
        {
            LOG.info("Running mock prepareConsignment");
            getProcess2WarehouseAdapter().prepareConsignment(process.getConsignment());
        }
        process.setWaitingForConsignment(true);
        getModelService().save(process);
    }

    @Required
    public void setProcess2WarehouseAdapter(final Process2WarehouseAdapter process2WarehouseAdapter)
    {
        this.process2WarehouseAdapter = process2WarehouseAdapter;
    }

    protected Process2WarehouseAdapter getProcess2WarehouseAdapter()
    {
        return process2WarehouseAdapter;
    }

    protected String getMockEnabledKey()
    {
        return mockEnabledKey;
    }

    @Required
    public void setMockEnabledKey(String mockEnabledKey)
    {
        this.mockEnabledKey = mockEnabledKey;
    }

    protected ConfigurationService getConfigurationService()
    {
        return configurationService;
    }

    @Required
    public void setConfigurationService(ConfigurationService configurationService)
    {
        this.configurationService = configurationService;
    }

}
