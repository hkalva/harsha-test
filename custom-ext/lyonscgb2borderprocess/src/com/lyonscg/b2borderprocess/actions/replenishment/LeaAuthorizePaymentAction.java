package com.lyonscg.b2borderprocess.actions.replenishment;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.worldpay.order.data.WorldpayAdditionalInfoData;
import com.worldpay.strategy.WorldpayAuthenticatedShopperIdStrategy;
import com.worldpay.util.WorldpayUtil;

import de.hybris.platform.b2bacceleratorservices.model.process.ReplenishmentProcessModel;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;


/**
 * Authorizes payment for replenishment orders.
 */
public class LeaAuthorizePaymentAction extends AbstractSimpleDecisionAction<ReplenishmentProcessModel>
{
    private static final Logger LOG = Logger.getLogger(LeaAuthorizePaymentAction.class);
    private CommerceCheckoutService b2bCommerceCheckoutService;
    private ImpersonationService impersonationService;
    private WorldpayAuthenticatedShopperIdStrategy worldpayAuthenticatedShopperIdStrategy;
    private AbstractSimpleDecisionAction<ReplenishmentProcessModel> payPalAuthorizePaymentAction;

    /**
     * AUTHORIZE cart for replenishment order.
     * 
     * @param process
     *            - {@link ReplenishmentProcessModel}
     * @return - OK if successful.
     */
    @Override
    public Transition executeAction(final ReplenishmentProcessModel process) throws Exception
    {
        try
        {
            final BusinessProcessParameterModel clonedCartParameter = getProcessParameterHelper()
                    .getProcessParameterByName(process, "cart");
            final CartModel clonedCart = (CartModel) clonedCartParameter.getValue();
            getModelService().refresh(clonedCart);
            if (clonedCart.getPaymentInfo() instanceof PaypalPaymentInfoModel)
            {
                return getPayPalAuthorizePaymentAction().executeAction(process);
            }
            final ImpersonationContext context = new ImpersonationContext();
            context.setOrder(clonedCart);
            return getImpersonationService().executeInContext(context,
                    new ImpersonationService.Executor<Transition, ImpersonationService.Nothing>()
            {
                        @Override
                        public Transition execute()
                        {
                            return authPayment(clonedCart);
                        }
                    });
        }
        catch (final Exception e)
        {
            LOG.error("Error authorizing payment.", e);
            return Transition.NOK;
        }
    }

    /**
     * Authorize payment from payment info in cart.
     * 
     * @param clonedCart
     *            - Replenishment cart.
     * @return - OK if successful.
     */
    protected Transition authPayment(final CartModel clonedCart)
    {
        PaymentTransactionEntryModel paymentTransactionEntryModel = null;
        if (clonedCart.getPaymentInfo() instanceof CreditCardPaymentInfoModel)
        {
            paymentTransactionEntryModel = getB2bCommerceCheckoutService().authorizePayment(clonedCart,
                    createSerializedCVV(clonedCart), getB2bCommerceCheckoutService().getPaymentProvider());
            if (paymentTransactionEntryModel == null
                    || !TransactionStatus.ACCEPTED.name().equals(paymentTransactionEntryModel.getTransactionStatus()))
            {
                clonedCart.setStatus(OrderStatus.B2B_PROCESSING_ERROR);
                modelService.save(clonedCart);
                return Transition.NOK;
            }
        }
        return Transition.OK;
    }

    /**
     * Creates Serialized cvv String from cart.
     * 
     * @param cart
     *            - Replenishment Cart.
     * @return - Serialized payment information.
     */
    protected String createSerializedCVV(final CartModel cart)
    {
        return WorldpayUtil.serializeWorldpayAdditionalInfo(createWorldPayAdditionalInfoData(cart));
    }

    /**
     * Creates and populates WorldPayAdditionalInfoData.
     * 
     * @param cart
     *            - Replenishment Cart.
     * @return - {@link WorldpayAdditionalInfoData}
     */
    protected WorldpayAdditionalInfoData createWorldPayAdditionalInfoData(final CartModel cart)
    {
        final WorldpayAdditionalInfoData worldPayAdditionalInfoData = new WorldpayAdditionalInfoData();
        worldPayAdditionalInfoData
                .setAuthenticatedShopperId(getWorldpayAuthenticatedShopperIdStrategy().getAuthenticatedShopperId(cart.getUser()));
        return worldPayAdditionalInfoData;
    }

    @Required
    public void setB2bCommerceCheckoutService(CommerceCheckoutService b2bCommerceCheckoutService)
    {
        this.b2bCommerceCheckoutService = b2bCommerceCheckoutService;
    }

    @Required
    public void setImpersonationService(ImpersonationService impersonationService)
    {
        this.impersonationService = impersonationService;
    }

    @Required
    public void setWorldpayAuthenticatedShopperIdStrategy(
            WorldpayAuthenticatedShopperIdStrategy worldpayAuthenticatedShopperIdStrategy)
    {
        this.worldpayAuthenticatedShopperIdStrategy = worldpayAuthenticatedShopperIdStrategy;
    }

    @Required
    public void setPayPalAuthorizePaymentAction(
            AbstractSimpleDecisionAction<ReplenishmentProcessModel> payPalAuthorizePaymentAction)
    {
        this.payPalAuthorizePaymentAction = payPalAuthorizePaymentAction;
    }

    protected CommerceCheckoutService getB2bCommerceCheckoutService()
    {
        return b2bCommerceCheckoutService;
    }

    protected ImpersonationService getImpersonationService()
    {
        return impersonationService;
    }

    protected WorldpayAuthenticatedShopperIdStrategy getWorldpayAuthenticatedShopperIdStrategy()
    {
        return worldpayAuthenticatedShopperIdStrategy;
    }

    protected AbstractSimpleDecisionAction<ReplenishmentProcessModel> getPayPalAuthorizePaymentAction()
    {
        return payPalAuthorizePaymentAction;
    }

}
