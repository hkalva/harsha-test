package com.lyonscg.b2borderprocess.actions.order;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.task.RetryLaterException;


/**
 * CheckAuthorizeOrderPayment for Invoice, CreditCard and Paypal payment types.
 */
public class LeaCheckAuthorizeOrderPaymentAction extends AbstractAction<OrderProcessModel>
{
    private AbstractAction<OrderProcessModel> worldpayCheckAuthorizeOrderPaymentAction;

    /**
     * Possible Transition types.
     */
    public enum Transition
    {
        OK, NOK, WAIT;

        /**
         * Returns set of all possible String Transition values.
         *
         * @return - Set of String Transition values.
         */
        public static Set<String> getStringValues()
        {
            final Set<String> res = new HashSet<>();
            for (final Transition transitions : Transition.values())
            {
                res.add(transitions.toString());
            }
            return res;
        }
    }

    /**
     * Returns set of all possible String Transition values.
     *
     * @return - Set of String Transition values.
     */
    @Override
    public Set<String> getTransitions()
    {
        return Transition.getStringValues();
    }

    /**
     * Formulates the String Transition based on the payment information in the order.
     *
     * @param - The {@link OrderProcessModel}.
     * @return - Formulated String Transition value.
     */
    @Override
    public String execute(OrderProcessModel process) throws RetryLaterException, Exception
    {
        final OrderModel order = process.getOrder();
        if (order != null)
        {
            if (order.getPaymentInfo() instanceof InvoicePaymentInfoModel)
            {
                return Transition.OK.toString();
            }
            else if (order.getPaymentInfo() instanceof CreditCardPaymentInfoModel)
            {
                return getWorldpayCheckAuthorizeOrderPaymentAction().execute(process);
            }
            return assignStatusForOrder(order).toString();
        }
        return Transition.NOK.toString();
    }

    /**
     * Sets the status for given order in case on of its {@link PaymentTransactionEntryModel} matches proper
     * {@link PaymentTransactionType} and {@link TransactionStatus}.
     * 
     * @param order
     *            {@link OrderModel}
     * @return {@link Transition}
     */
    protected Transition assignStatusForOrder(final OrderModel order)
    {
        for (final PaymentTransactionModel transaction : order.getPaymentTransactions())
        {
            for (final PaymentTransactionEntryModel entry : transaction.getEntries())
            {
                if (entry.getType().equals(PaymentTransactionType.AUTHORIZATION)
                        && TransactionStatus.ACCEPTED.name().equals(entry.getTransactionStatus()))
                {
                    order.setStatus(OrderStatus.PAYMENT_AUTHORIZED);
                    modelService.save(order);
                    return Transition.OK;
                }
            }
        }
        return Transition.NOK;
    }

    protected AbstractAction<OrderProcessModel> getWorldpayCheckAuthorizeOrderPaymentAction()
    {
        return worldpayCheckAuthorizeOrderPaymentAction;
    }

    @Required
    public void setWorldpayCheckAuthorizeOrderPaymentAction(
            AbstractAction<OrderProcessModel> worldpayCheckAuthorizeOrderPaymentAction)
    {
        this.worldpayCheckAuthorizeOrderPaymentAction = worldpayCheckAuthorizeOrderPaymentAction;
    }

}
