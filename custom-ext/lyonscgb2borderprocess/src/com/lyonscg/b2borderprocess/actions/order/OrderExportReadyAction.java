package com.lyonscg.b2borderprocess.actions.order;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.servicelayer.config.ConfigurationService;


/**
 * Sets order status to {@link OrderStatus#EXPORT_READY} to make it ready for order export.
 */
public class OrderExportReadyAction extends AbstractAction<OrderProcessModel>
{
    private static final Logger LOG = Logger.getLogger(OrderExportReadyAction.class);
    private ConfigurationService configurationService;
    private String skipExportKey;

    /**
     * Possible Transition types.
     */
    public enum Transition
    {
        NO_EXPORT, WAIT_FOR_EXPORT;

        public static Set<String> getStringValues()
        {
            final Set<String> res = new HashSet<String>();
            for (final Transition transitions : Transition.values())
            {
                res.add(transitions.toString());
            }
            return res;
        }
    }

    /**
     * Returns set of all possible String Transition values.
     *
     * @return - Set of String Transition values.
     */
    @Override
    public Set<String> getTransitions()
    {
        return Transition.getStringValues();
    }

    /**
     * Sets order status to {@link OrderStatus#EXPORT_READY} to make it ready for order export.
     *
     * @param orderProcess
     *            - Order process.
     * @return - returns OK if successful.
     */
    @Override
    public final String execute(final OrderProcessModel orderProcess)
    {
        LOG.info("Process: " + orderProcess.getCode() + " in step " + getClass().getSimpleName());
        OrderModel order = orderProcess.getOrder();
        if (getConfigurationService().getConfiguration().getBoolean(getSkipExportKey()))
        {
            LOG.info("Skipping order export");
            return Transition.NO_EXPORT.toString();
        }
        OrderStatus orderStatus = OrderStatus.EXPORT_READY;
        orderProcess.getOrder().setStatus(orderStatus);
        getModelService().save(order);
        LOG.info("Order status set to " + orderStatus);
        return Transition.WAIT_FOR_EXPORT.toString();
    }

    protected String getSkipExportKey()
    {
        return skipExportKey;
    }

    @Required
    public void setSkipExportKey(String skipExportKey)
    {
        this.skipExportKey = skipExportKey;
    }

    protected ConfigurationService getConfigurationService()
    {
        return configurationService;
    }

    @Required
    public void setConfigurationService(ConfigurationService configurationService)
    {
        this.configurationService = configurationService;
    }
}
