/**
 *
 */
package com.lyonscg.lyonscgcistaxaddon.controllers.pages;

import de.hybris.platform.addonsupport.interceptors.BeforeViewHandlerAdaptee;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CartData;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;


/**@author lyonscg.
 *Implementation of tax for lea before view handler.
 */
public class LeaTaxSourceBeforeViewHandler implements BeforeViewHandlerAdaptee
{
	private static final String CART_TAX_SOURCE = "cartTaxSource";
	private static final String CHECKOUT = "checkout";
	private static final String CART = "cart";
	@Resource(name = "checkoutFacade")
	private CheckoutFacade checkoutFacade;

	@Override
	public String beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
			final String viewName) throws Exception
	{
		if (request.getRequestURI().contains(CART) || request.getRequestURI().contains(CHECKOUT))
		{
			final CartData cartData = checkoutFacade.getCheckoutCart();
			if (null != cartData)
			{
				model.addAttribute(CART_TAX_SOURCE, cartData.getTaxSource());
			}
		}
		return viewName;
	}

}
