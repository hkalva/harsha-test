/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lyonscg.facades.constants;

/**
 * Global class for all RlpFacades constants.
 */
public class RlpFacadesConstants extends GeneratedRlpFacadesConstants
{
	public static final String EXTENSIONNAME = "rlpfacades";

	private RlpFacadesConstants()
	{
		//empty
	}
}
