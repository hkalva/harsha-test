package com.capgemini.customproductcatalog.service.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.session.SessionService;

@UnitTest
public class SessionBasedCustomProductCatalogPrincipalStrategyTest
{

    private SessionBasedCustomProductCatalogPrincipalStrategy strategy;
    
    @Mock
    private SessionService sessionService;
    
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        strategy = new SessionBasedCustomProductCatalogPrincipalStrategy();
        strategy.setSessionService(sessionService);
        strategy.setAttributeName("user");
        Mockito.when(sessionService.getAttribute(Mockito.anyString())).thenReturn(null);
    }
    
    @Test
    public void getAttributeName()
    {
        String result = strategy.getAttributeName();
        Assert.assertTrue(("user").equals(result));
    }
    
    @Test
    public void getPrincipal()
    {
        strategy.getPrincipal();
        Mockito.verify(sessionService, Mockito.times(1)).getAttribute(Mockito.anyString());
    }
}
