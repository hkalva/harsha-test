/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customproductcatalog.search.solrfacetsearch.context.impl;

import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.SearchQuery.Operator;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchListener;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.capgemini.customproductcatalog.model.CustomProductCatalogModel;
import com.capgemini.customproductcatalog.service.CustomProductCatalogService;


/**
 * The Solr query needs to be adjusted to take into consideration the user's custom product catalogs. We only want
 * products returned that are included in the user's custom product catalogs. An implementation of the FacetListener
 * class is needed with only the beforeSearch method implemented.
 */
public class CustomProductCatalogFacetSearchListener implements FacetSearchListener
{
	/**
	 * customProductCatalogService bean.
	 */
	private CustomProductCatalogService customProductCatalogService;


	@Override
	public void beforeSearch(final FacetSearchContext facetSearchContext) throws FacetSearchException
	{
		final List<CustomProductCatalogModel> catalogs = getCustomProductCatalogService()
				.getCustomProductCatalogForCurrentPrincipal();

		if (CollectionUtils.isNotEmpty(catalogs))
		{
			final Set<String> codes = new HashSet<>();

			for (final CustomProductCatalogModel catalog : catalogs)
			{
				codes.add(catalog.getCode());
			}

			facetSearchContext.getSearchQuery().addFilterQuery("customProductCatalogsHidden_string_mv", Operator.OR, codes);
		}
	}

	@Override
	public void afterSearch(final FacetSearchContext arg0) throws FacetSearchException
	{
		// XXX Auto-generated method stub

	}

	@Override
	public void afterSearchError(final FacetSearchContext arg0) throws FacetSearchException
	{
		// XXX Auto-generated method stub

	}

	/**
	 * @return the customProductCatalogService
	 */
	public CustomProductCatalogService getCustomProductCatalogService()
	{
		return customProductCatalogService;
	}

	/**
	 * @param customProductCatalogService
	 *           the customProductCatalogService to set
	 */
	public void setCustomProductCatalogService(final CustomProductCatalogService customProductCatalogService)
	{
		this.customProductCatalogService = customProductCatalogService;
	}
}
