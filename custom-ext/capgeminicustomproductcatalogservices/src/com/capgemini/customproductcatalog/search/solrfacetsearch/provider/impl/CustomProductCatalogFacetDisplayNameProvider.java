/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customproductcatalog.search.solrfacetsearch.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractFacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.customproductcatalog.model.CustomProductCatalogModel;
import com.capgemini.customproductcatalog.service.CustomProductCatalogService;


/**
 * This facet display name provider for custom product catalogs is needed in case they will be used as visible facets in
 * the listing pages.
 */
public class CustomProductCatalogFacetDisplayNameProvider extends AbstractFacetValueDisplayNameProvider
{
	private static final Logger LOG = LoggerFactory.getLogger(CustomProductCatalogFacetDisplayNameProvider.class);

	/**
	 * customProductCatalogService bean.
	 */
	private CustomProductCatalogService customProductCatalogService;

	@Override
	public String getDisplayName(final SearchQuery searchQuery, final IndexedProperty indexedProperty, final String value)
	{
		if (StringUtils.isEmpty(value))
		{
			return StringUtils.EMPTY;
		}

		try
		{
			final CustomProductCatalogModel catalog = getCustomProductCatalogService().getCustomProductCatalogForCode(value);
			return catalog.getName();
		}
		catch (final Exception e)
		{
			LOG.warn("Error retrieving custom product catalog with code '{}'", value, e);
			return StringUtils.EMPTY;
		}
	}

	/**
	 * @return the customProductCatalogService
	 */
	public CustomProductCatalogService getCustomProductCatalogService()
	{
		return customProductCatalogService;
	}

	/**
	 * @param customProductCatalogService
	 *           the customProductCatalogService to set
	 */
	public void setCustomProductCatalogService(final CustomProductCatalogService customProductCatalogService)
	{
		this.customProductCatalogService = customProductCatalogService;
	}
}
