/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customproductcatalog.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.capgemini.customproductcatalog.model.CustomProductCatalogModel;
import com.capgemini.customproductcatalog.service.CustomProductCatalogService;


/**
 * This class used for adding custom product catalog codes in the Solr index.
 */
public class CustomProductCatalogsValueResolver extends AbstractValueResolver<ProductModel, Object, Object>
{
	/**
	 * customProductCatalogService bean.
	 */
	private CustomProductCatalogService customProductCatalogService;

	@Override
	protected void addFieldValues(final InputDocument inputDocument, final IndexerBatchContext indexerBatchContext,
			final IndexedProperty indexedProperty, final ProductModel product,
			final ValueResolverContext<Object, Object> valueResolverContext) throws FieldValueProviderException
	{
		boolean hasValue = false;
		final List<CustomProductCatalogModel> catalogs = getCustomProductCatalogService()
				.getCustomProductCatalogForProduct(product);

		if (CollectionUtils.isNotEmpty(catalogs))
		{
			hasValue = true;

			for (final CustomProductCatalogModel catalog : catalogs)
			{
				inputDocument.addField(indexedProperty, catalog.getCode(), valueResolverContext.getFieldQualifier());
			}
		}

		if (!hasValue)
		{
			final boolean isOptional = ValueProviderParameterUtils.getBoolean(indexedProperty, "optional", true);

			if (!isOptional)
			{
				throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
			}
		}
	}

	/**
	 * @return the customProductCatalogService
	 */
	public CustomProductCatalogService getCustomProductCatalogService()
	{
		return customProductCatalogService;
	}

	/**
	 * @param customProductCatalogService
	 *           the customProductCatalogService to set
	 */
	public void setCustomProductCatalogService(final CustomProductCatalogService customProductCatalogService)
	{
		this.customProductCatalogService = customProductCatalogService;
	}
}
