/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customproductcatalog.search.solrfacetsearch.impl.populators;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;
import de.hybris.platform.solrfacetsearch.search.impl.populators.AbstractFacetSearchQueryPopulator;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.solr.client.solrj.SolrQuery;

import com.capgemini.customproductcatalog.model.CustomProductCatalogModel;
import com.capgemini.customproductcatalog.service.CustomProductCatalogService;


/**
 * This class only return the custom product catalogs as facet values for the current user.
 */
public class CustomProductCatalogFacetSearchQueryPopulator extends AbstractFacetSearchQueryPopulator
{
	/**
	 * customProductCatalogService bean.
	 */
	private CustomProductCatalogService customProductCatalogService;

	@Override
	public void populate(final SearchQueryConverterData source, final SolrQuery target) throws ConversionException
	{
		final List<CustomProductCatalogModel> catalogs = getCustomProductCatalogService()
				.getCustomProductCatalogForCurrentPrincipal();

        final StringBuilder regex = new StringBuilder();
        
		if (CollectionUtils.isNotEmpty(catalogs))
		{
			for (final CustomProductCatalogModel catalog : catalogs)
			{
				if (regex.length() > 0)
				{
					regex.append("|");
				}

				regex.append(catalog.getCode());
			}
		}
		
        target.add("f.customProductCatalogsVisible_string_mv.facet.matches", regex.toString());
	}

	/**
	 * @return the customProductCatalogService
	 */
	public CustomProductCatalogService getCustomProductCatalogService()
	{
		return customProductCatalogService;
	}

	/**
	 * @param customProductCatalogService
	 *           the customProductCatalogService to set
	 */
	public void setCustomProductCatalogService(final CustomProductCatalogService customProductCatalogService)
	{
		this.customProductCatalogService = customProductCatalogService;
	}
}
