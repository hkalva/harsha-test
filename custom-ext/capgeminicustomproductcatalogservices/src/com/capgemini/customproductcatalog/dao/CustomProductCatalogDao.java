/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customproductcatalog.dao;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;

import com.capgemini.customproductcatalog.model.CustomProductCatalogModel;


/**
 *
 */
public interface CustomProductCatalogDao extends Dao
{
	/**
	 * To get the List<CustomProductCatalogModel> based on code.
	 */
	List<CustomProductCatalogModel> findCustomProductCatalogForCode(String code);

	/**
	 * To get the List<CustomProductCatalogModel> based on PrincipalModel.
	 */
	List<CustomProductCatalogModel> getCustomProductCatalogForPrincipal(PrincipalModel principal);

	/**
	 * To get the List<CustomProductCatalogModel> based on ProductModel.
	 */
	List<CustomProductCatalogModel> getCustomProductCatalogForProduct(ProductModel product);

}
