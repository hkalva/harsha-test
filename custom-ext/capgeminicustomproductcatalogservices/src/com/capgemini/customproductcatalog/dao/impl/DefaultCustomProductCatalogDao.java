/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customproductcatalog.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.util.FlexibleSearchUtils;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.capgemini.customproductcatalog.dao.CustomProductCatalogDao;
import com.capgemini.customproductcatalog.model.CustomProductCatalogModel;


/**
 * Provide a default implementation of the DAO.
 */
public class DefaultCustomProductCatalogDao extends DefaultGenericDao<CustomProductCatalogModel>
		implements CustomProductCatalogDao
{
	/**
	 * Provide an explicit constructor.
	 */
	public DefaultCustomProductCatalogDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<CustomProductCatalogModel> findCustomProductCatalogForCode(final String code)
	{
		validateParameterNotNull(code, "Code must not be null");
		return find(Collections.singletonMap(CustomProductCatalogModel.CODE, code));
	}

	@Override
	public List<CustomProductCatalogModel> getCustomProductCatalogForPrincipal(final PrincipalModel principal)
	{
		validateParameterNotNull(principal, "Principal must not be null");
		final Set<PrincipalModel> principals = new HashSet<>();
		principals.add(principal);
		final Set<PrincipalGroupModel> groups = principal.getAllGroups();

		if (CollectionUtils.isNotEmpty(groups))
		{
			principals.addAll(groups);
		}

		final Map<String, Object> params = new HashMap<>();
		final String whereClause = FlexibleSearchUtils.buildOracleCompatibleCollectionStatement("{target} in (?principals) ",
				"principals", "AND", principals, params);
		final StringBuilder query = new StringBuilder();
		query.append("select distinct {source} ");
		query.append("from {PrincipalCustomProductCatalog} ");
		query.append("where ").append(whereClause);
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query.toString(), params);
		final SearchResult<CustomProductCatalogModel> searchResult = getFlexibleSearchService().search(flexibleSearchQuery);
		return searchResult.getResult();
	}

	@Override
	public List<CustomProductCatalogModel> getCustomProductCatalogForProduct(final ProductModel product)
	{
		validateParameterNotNull(product, "Product must not be null");
		final Set<ProductModel> products = new HashSet<>();
		products.add(product);
		ProductModel currentProduct = product;

		while (currentProduct instanceof VariantProductModel)
		{
			currentProduct = ((VariantProductModel) currentProduct).getBaseProduct();
			products.add(currentProduct);
		}

		final Map<String, Object> params = new HashMap<>();
		final String whereClause = FlexibleSearchUtils.buildOracleCompatibleCollectionStatement("{target} in (?products) ",
				"products", "AND", products, params);
		final StringBuilder query = new StringBuilder();
		query.append("select distinct {source} ");
		query.append("from {ProductCustomProductCatalog} ");
		query.append("where ").append(whereClause);
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query.toString(), params);
		final SearchResult<CustomProductCatalogModel> searchResult = getFlexibleSearchService().search(flexibleSearchQuery);
		return searchResult.getResult();
	}
}
