/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customproductcatalog.service;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.security.PrincipalModel;

import java.util.List;

import com.capgemini.customproductcatalog.model.CustomProductCatalogModel;


/**
 * This interface defines for custom product catalog related operations.
 */
public interface CustomProductCatalogService
{
    /**
     * To retrieve custom product catalogs with the given code.
     *
     * @param code - the code.
     * @return CustomProductCatalogModel
     */
	CustomProductCatalogModel getCustomProductCatalogForCode(String code);

	/**
	 * To return the custom product catalogs associated with the given principal.
	 *
	 * @param principal - the principal
	 * @return List<CustomProductCatalogModel>
	 */
	List<CustomProductCatalogModel> getCustomProductCatalogForPrincipal(PrincipalModel principal);

	/**
	 * To return the custom product catalogs associated with the current principal.
	 *
	 * @return List<CustomProductCatalogModel>
	 */
	List<CustomProductCatalogModel> getCustomProductCatalogForCurrentPrincipal();

	/**
	 * To return the custom product catalogs associated with the product provided.
	 *
	 * @param product - the product.
	 * @return List<CustomProductCatalogModel>
	 */
	List<CustomProductCatalogModel> getCustomProductCatalogForProduct(ProductModel product);

	/**
     * Use a CustomProductCatalogPrincipalStrategy to determine the principal that drives the custom product catalogs
     * displayed on the site.
	 * 
	 * @return PrincipalModel
	 */
	PrincipalModel getPrincipal();
}
