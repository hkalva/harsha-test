/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customproductcatalog.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfSingleResult;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static java.lang.String.format;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.security.PrincipalModel;

import java.util.List;

import com.capgemini.customproductcatalog.dao.CustomProductCatalogDao;
import com.capgemini.customproductcatalog.model.CustomProductCatalogModel;
import com.capgemini.customproductcatalog.service.CustomProductCatalogPrincipalStrategy;
import com.capgemini.customproductcatalog.service.CustomProductCatalogService;


/**
 * Provide a default implementation of the CustomProductCatalogService interface.
 */
public class DefaultCustomProductCatalogService implements CustomProductCatalogService
{
	/**
	 * customProductCatalogDao bean.
	 */
	private CustomProductCatalogDao customProductCatalogDao;
	/**
	 * customProductCatalogPrincipalStrategy bean.
	 */
	private CustomProductCatalogPrincipalStrategy customProductCatalogPrincipalStrategy;

	@Override
	public CustomProductCatalogModel getCustomProductCatalogForCode(final String code)
	{
		validateParameterNotNull(code, "Parameter code must not be null");
		final List<CustomProductCatalogModel> catalogs = getCustomProductCatalogDao().findCustomProductCatalogForCode(code);
		validateIfSingleResult(catalogs, format("Custom product catalog with code '%s' not found!", code),
				format("Custom product catalog code '%s' is not unique, %d catalogs found!", code, Integer.valueOf(catalogs.size())));
		return catalogs.get(0);
	}

	@Override
	public List<CustomProductCatalogModel> getCustomProductCatalogForPrincipal(final PrincipalModel principal)
	{
		validateParameterNotNull(principal, "Parameter principal must not be null");
		return getCustomProductCatalogDao().getCustomProductCatalogForPrincipal(principal);
	}

	@Override
	public List<CustomProductCatalogModel> getCustomProductCatalogForCurrentPrincipal()
	{
		return getCustomProductCatalogForPrincipal(getPrincipal());
	}

	@Override
	public List<CustomProductCatalogModel> getCustomProductCatalogForProduct(final ProductModel product)
	{
		validateParameterNotNull(product, "Parameter product must not be null");
		return getCustomProductCatalogDao().getCustomProductCatalogForProduct(product);
	}

	@Override
	public PrincipalModel getPrincipal()
	{
		return getCustomProductCatalogPrincipalStrategy().getPrincipal();
	}

	/**
	 * @return the customProductCatalogDao.
	 */
	public CustomProductCatalogDao getCustomProductCatalogDao()
	{
		return customProductCatalogDao;
	}

	/**
	 * @param customProductCatalogDao
	 *           the customProductCatalogDao to set.
	 */
	public void setCustomProductCatalogDao(final CustomProductCatalogDao customProductCatalogDao)
	{
		this.customProductCatalogDao = customProductCatalogDao;
	}

	/**
	 * @return the customProductCatalogPrincipalStrategy
	 */
	public CustomProductCatalogPrincipalStrategy getCustomProductCatalogPrincipalStrategy()
	{
		return customProductCatalogPrincipalStrategy;
	}

	/**
	 * @param customProductCatalogPrincipalStrategy
	 *           the customProductCatalogPrincipalStrategy to set
	 */
	public void setCustomProductCatalogPrincipalStrategy(
			final CustomProductCatalogPrincipalStrategy customProductCatalogPrincipalStrategy)
	{
		this.customProductCatalogPrincipalStrategy = customProductCatalogPrincipalStrategy;
	}
}
