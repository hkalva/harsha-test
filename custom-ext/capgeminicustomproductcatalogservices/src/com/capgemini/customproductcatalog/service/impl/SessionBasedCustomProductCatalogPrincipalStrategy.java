/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customproductcatalog.service.impl;

import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.servicelayer.session.SessionService;

import com.capgemini.customproductcatalog.service.CustomProductCatalogPrincipalStrategy;


/**
 * To get the PrincipalModel from SessionService.
 */
public class SessionBasedCustomProductCatalogPrincipalStrategy implements CustomProductCatalogPrincipalStrategy
{
	/**
	 * sessionService bean.
	 */
	private SessionService sessionService;
	/**
	 * attributeName configured in xml file.
	 */
	private String attributeName;

	@Override
	public PrincipalModel getPrincipal()
	{
		return getSessionService().getAttribute(getAttributeName());
	}

	/**
	 * @return the sessionService
	 */
	public SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * @param sessionService
	 *           the sessionService to set
	 */
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	/**
	 * @return the attributeName
	 */
	public String getAttributeName()
	{
		return attributeName;
	}

	/**
	 * @param attributeName
	 *           the attributeName to set
	 */
	public void setAttributeName(final String attributeName)
	{
		this.attributeName = attributeName;
	}
}
