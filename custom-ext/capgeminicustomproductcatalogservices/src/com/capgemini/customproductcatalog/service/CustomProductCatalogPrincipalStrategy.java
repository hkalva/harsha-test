/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.customproductcatalog.service;

import de.hybris.platform.core.model.security.PrincipalModel;


/**
 * The implementing class should return the principal that drives the custom product catalogs displayed on the site.
 */
public interface CustomProductCatalogPrincipalStrategy
{
	/**
	 * To get the PrincipalModel.
	 */
	public PrincipalModel getPrincipal();

}
