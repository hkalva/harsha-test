/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lyonscg.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.lyonscg.fulfilmentprocess.constants.RlpFulfilmentProcessConstants;

public class RlpFulfilmentProcessManager extends GeneratedRlpFulfilmentProcessManager
{
	public static final RlpFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (RlpFulfilmentProcessManager) em.getExtension(RlpFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
