/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.wishlistservices.converters.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;

import org.springframework.util.Assert;

import com.capgemini.wishlistservices.data.WishlistEntryData;


/**
 *
 */
public class WishlistEntryPopulator implements Populator<Wishlist2EntryModel, WishlistEntryData>
{
	/**
	 * the product converter.
	 */
	private Converter<ProductModel, ProductData> productConverter;

	/**
	 * @return the productConverter
	 */
	public Converter<ProductModel, ProductData> getProductConverter()
	{
		return productConverter;
	}

	/**
	 * @param productConverter
	 *           the productConverter to set
	 */
	public void setProductConverter(final Converter<ProductModel, ProductData> productConverter)
	{
		this.productConverter = productConverter;
	}

	@Override
	public void populate(final Wishlist2EntryModel source, final WishlistEntryData target)
	{
		Assert.notNull(source);
		Assert.notNull(target);

		//Product Converter to convert ProductModel to ProductData
		if (source.getProduct() != null)
		{
			target.setProduct(getProductConverter().convert(source.getProduct()));
		}

		target.setComment(source.getComment());
		target.setAddedDate(source.getAddedDate());
		target.setPriority(source.getPriority());
		target.setDesired(source.getDesired());
		target.setReceived(source.getReceived());

	}

}
