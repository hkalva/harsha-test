/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.wishlistservices.converters.populator;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;

import com.capgemini.wishlistservices.data.WishlistData;
import com.capgemini.wishlistservices.data.WishlistEntryData;


/**
 * Populator class to convert the model object to the data object that can be used in the front end to display data.
 */
public class WishlistPopulator implements Populator<Wishlist2Model, WishlistData>
{
	/**
	 * the wishlist entry converter.
	 */
	private Converter<Wishlist2EntryModel, WishlistEntryData> wishlistEntryConverter;
	
	/**
	 * the customer converter.
	 */
	private Converter<UserModel, CustomerData> customerConverter;

	/**
	 * @return the customerConverter
	 */
	public Converter<UserModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}


	/**
	 * @param customerConverter
	 *           the customerConverter to set
	 */
	public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}


	/**
	 * @return the wishlistEntryConverter
	 */
	public Converter<Wishlist2EntryModel, WishlistEntryData> getWishlistEntryConverter()
	{
		return wishlistEntryConverter;
	}


	/**
	 * @param wishlistEntryConverter
	 *           the wishlistEntryConverter to set
	 */
	public void setWishlistEntryConverter(final Converter<Wishlist2EntryModel, WishlistEntryData> wishlistEntryConverter)
	{
		this.wishlistEntryConverter = wishlistEntryConverter;
	}


	/**
	 * To implement the populate method which sets the Wishlist2Model object values into the WishlistData object.
	 *
	 * @param source
	 *           Wishlist2Model Model object.
	 * @param target
	 *           WishlistData Data object.
	 * @throws ConversionException
	 *            Throws any exception caught during the conversion process.
	 */
	@Override
	public void populate(final Wishlist2Model source, final WishlistData target)
	{
		Assert.notNull(source);
		Assert.notNull(target);

		target.setName(source.getName());
		target.setDescription(source.getDescription());
		target.setDateCreated(source.getCreationtime());
		target.setDateModified(source.getModifiedtime());
		target.setCode(source.getCode());
		
		//Convert Each WishListEntryModel into WishListEntryData
		if (CollectionUtils.isEmpty(source.getEntries()))
		{
	        target.setNumberOfWishlistEntries(Integer.valueOf(0));
		}
		else
		{
		    target.setWishlistEntries(Converters.convertAll(source.getEntries(), getWishlistEntryConverter()));
            target.setNumberOfWishlistEntries(Integer.valueOf(source.getEntries().size()));
		}
		
		//Set the no. of wish list entries
		if (source.getLastModifiedBy() != null)
		{
			target.setLastModifiedBy(getCustomerConverter().convert(source.getLastModifiedBy()));
		}
	}

}
