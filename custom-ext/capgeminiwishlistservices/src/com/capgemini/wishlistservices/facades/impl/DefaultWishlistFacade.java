/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.wishlistservices.facades.impl;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.capgemini.wishlistservices.data.WishlistData;
import com.capgemini.wishlistservices.data.WishlistEntryData;
import com.capgemini.wishlistservices.facades.WishlistFacade;
import com.capgemini.wishlistservices.services.WishlistService;


/**
 * Default WishList Facade Implementation to invoke the Service Layer methods &
 *
 * Convert the Model Objects into Data Transfer Objects
 */
public class DefaultWishlistFacade implements WishlistFacade
{
	/**
	 * the wishlist converter.
	 */
	private Converter<Wishlist2Model, WishlistData> wishlistConverter;
	
	/**
	 * the wishlist service.
	 */
	@Resource
	private WishlistService wishlistService;

	/**
	 * To retrieve the list of available wishlists for a User
	 */
	@Override
	public List<WishlistData> getMyWishlists()
	{
		final List<Wishlist2Model> userWishLists = getWishlistService().getWishlists();
		
		if (CollectionUtils.isNotEmpty(userWishLists))
		{
			return Converters.convertAll(userWishLists, getWishlistConverter());
		}
		
		return Collections.emptyList();
	}

	/**
	 * To Retrieve the list of Wishlist Models from Service class and return the list WishlistData objects
	 */
	@Override
	public WishlistData getWishlistDetails(final String wishlistCode)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("wishlistCode", wishlistCode);

		final Wishlist2Model wishlistModel = getWishlistService().getMyWishlistByCode(wishlistCode);
		ServicesUtil.validateParameterNotNull(wishlistModel, String.format("No wishlist found for uid %s", wishlistCode));
		return getWishlistConverter().convert(wishlistModel);
	}

	/**
	 * Create a New Wishlist by invoking the Service class method
	 */
	@Override
	public WishlistData createWishlist(final WishlistData wishlistData)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("wishlistData object cannot be null", wishlistData);
		ServicesUtil.validateParameterNotNullStandardMessage("wishlist Name cannot be null", wishlistData.getName());

		final Wishlist2Model wishlistModel = getWishlistService().createWishlist(wishlistData.getName(),
				wishlistData.getDescription());

		ServicesUtil.validateParameterNotNullStandardMessage("Wishlist Object cannot be null", wishlistModel);
		return getWishlistConverter().convert(wishlistModel);
	}

	@Override
	public void deleteWishlist(final String wishlistCode)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("Wishlist Code cannot be null", wishlistCode);
		getWishlistService().deleteWishlist(wishlistCode);
	}

	/**
	 * To invoke the Update Wishlist details service class to update information
	 */
	@Override
	public void updateWishlistDetails(final WishlistData wishlistData)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("wishlistData", wishlistData);
		final Wishlist2Model wishlistModel = getWishlistService().updateWishlist(wishlistData);
		ServicesUtil.validateParameterNotNullStandardMessage("Wishlist Model cannot be null", wishlistModel);
	}

	/**
	 * To retrieve the Paged List of Wishlists for an User with all search parameters
	 */
	@Override
	public SearchPageData<WishlistData> getPagedWishlists(final PageableData pageableData)
	{
		final SearchPageData searchPageData = getWishlistService().getPagedWishlists(pageableData);
		return convertPageData(searchPageData, getWishlistConverter());
	}

	/**
	 * Method to convert SearchPageData from its source format to Target format. Convert the result set using the wish
	 * list converter.
	 */
	protected <S, T> SearchPageData<T> convertPageData(final SearchPageData<S> source, final Converter<S, T> converter)
	{
		final SearchPageData<T> result = new SearchPageData<T>();
		result.setPagination(source.getPagination());
		result.setSorts(source.getSorts());
		result.setResults(Converters.convertAll(source.getResults(), converter));
		return result;
	}

	/**
	 * To add a product to a Wishlist by wishlistcode.
	 */
	@Override
	public void addProductToWishlist(final String wishlistCode, final WishlistEntryData wishlistEntryData)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("WishlistCode cannot be null", wishlistCode);
		ServicesUtil.validateParameterNotNullStandardMessage("WishlistEntryData Code cannot be null", wishlistEntryData);
		ServicesUtil.validateParameterNotNullStandardMessage("Product cannot be null", wishlistEntryData.getProduct());

		getWishlistService().addProductToWishlist(wishlistCode, wishlistEntryData);
	}

	/**
	 * To remove a Wishlist Entry / Product from a specific wishlist.
	 */
	@Override
	public void removeProductFromWishlist(final String wishlistCode, final String productCode)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("WishlistCode", wishlistCode);
		ServicesUtil.validateParameterNotNullStandardMessage("ProductCode", productCode);

		getWishlistService().removeWishlistEntryForProduct(wishlistCode, productCode);
	}

	/**
	 * @return the wishListConverter
	 */
	public Converter<Wishlist2Model, WishlistData> getWishlistConverter()
	{
		return wishlistConverter;
	}

	/**
	 * @param wishlistConverter
	 *           the wishListConverter to set
	 */
	public void setWishlistConverter(final Converter<Wishlist2Model, WishlistData> wishlistConverter)
	{
		this.wishlistConverter = wishlistConverter;
	}

	/**
	 * @return the wishlistService
	 */
	public WishlistService getWishlistService()
	{
		return wishlistService;
	}

	/**
	 * @param wishlistService
	 *           the wishlistService to set
	 */
	public void setWishlistService(final WishlistService wishlistService)
	{
		this.wishlistService = wishlistService;
	}

}
