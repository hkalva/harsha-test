/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.wishlistservices.facades;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;

import java.util.List;

import com.capgemini.wishlistservices.data.WishlistData;
import com.capgemini.wishlistservices.data.WishlistEntryData;



/**
 * Facade to combine the various service calls for Wishlist and convert WishlistModel objects to Data Transfer Objects.
 */
public interface WishlistFacade
{
    /**
     * Return the list of Wishlists.
     * 
     * @return List<WishlistData>
     */
	List<WishlistData> getMyWishlists();

	/**
	 * Retrieve the Wishlist details.
	 * 
	 * @param wishlistCode - the wishlist code.
	 * @return WishlistData
	 */
	WishlistData getWishlistDetails(String wishlistCode);

	/**
	 * Create a Wishlist in the DB with name and description data from the WishlistData object.
	 * 
	 * @param wishlistData - the wishlist data.
	 * @return WishlistData
	 */
	WishlistData createWishlist(WishlistData wishlistData);

	/**
	 * Delete Wishlist.
	 * 
	 * @param wishlistCode - the wishlist code.
	 */
	void deleteWishlist(String wishlistCode);

	/**
	 * To retrieve the Paged Wishlists for a User.
	 * 
	 * @param pageableData - the pageable data.
	 * @return SearchPageData<WishlistData>
	 */
	SearchPageData<WishlistData> getPagedWishlists(PageableData pageableData);

	/**
	 * To update Wishlist details.
	 * 
	 * @param wishlistData - the wishlist data
	 */
	void updateWishlistDetails(WishlistData wishlistData);

	/**
	 * To add a product to a specific Wishlist.
	 * 
	 * @param wishlistCode - the wishlist code.
	 * @param wishlistEntryData - the wishlist entry data.
	 */
	void addProductToWishlist(String wishlistCode, WishlistEntryData wishlistEntryData);

	/**
	 * To remove a specific wishlist entry from a specific wishlist.
	 * 
	 * @param wishlistCode - the wishlist code.
	 * @param productCode - the product code.
	 */
	void removeProductFromWishlist(String wishlistCode, String productCode);

}
