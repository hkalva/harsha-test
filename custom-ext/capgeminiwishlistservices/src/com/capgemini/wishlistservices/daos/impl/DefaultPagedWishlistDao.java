/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.wishlistservices.daos.impl;

import de.hybris.platform.commerceservices.search.dao.impl.DefaultPagedGenericDao;
import de.hybris.platform.commerceservices.search.flexiblesearch.data.SortQueryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.servicelayer.internal.dao.SortParameters;
import de.hybris.platform.util.Config;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * Default Paged WishlistDao to implement the Pagination Parameters for front end
 */
public class DefaultPagedWishlistDao extends DefaultPagedGenericDao<Wishlist2Model>
{
	private static final String DEFAULT_SORT_CODE = Config.getString("wishlistapi.defaultSortCode", "byName");

	/**
	 * Super Constructor for DefaultPagedGenericDao, will be set through the Spring XMl
	 */
	public DefaultPagedWishlistDao(final String typeCode)
	{
		super(typeCode);
	}

	/**
	 * Retrieve the list of Paged Wish lists for the specific user passed as params.
	 *
	 * Create the possible sort queries to be used to apply Sort
	 */
	public SearchPageData<Wishlist2Model> getPagedWishlists(final Map<String, Object> params, final PageableData pageableData)
	{
		final List<SortQueryData> sortQueries = Arrays.asList(
				createSortQueryData("byName", params, SortParameters.singletonAscending(Wishlist2Model.NAME), pageableData),
				createSortQueryData("byLastModifiedBy", params, SortParameters.singletonAscending(Wishlist2Model.LASTMODIFIEDBY),
						pageableData),
				createSortQueryData("byModifiedTime", params, SortParameters.singletonDescending(Wishlist2Model.MODIFIEDTIME),
						pageableData));
		return getPagedFlexibleSearchService().search(sortQueries, DEFAULT_SORT_CODE, params, pageableData);

	}
}
