/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.wishlistservices.daos.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.wishlistservices.daos.UserWishlistDao;


/**
 * Custom Implementation of Wishlist Dao .
 *
 * OOTB Wishlist2Dao & DefaultWishlist2Dao class extends Dao and AbstractItemDao respectively which has deprecated
 * member variables & methods, hence created a new Interface and Implementation class that extends GenericDao<M> and
 * DefaultGenericDao<M> respectively
 */
public class DefaultUserWishlistDao extends DefaultGenericDao<Wishlist2Model> implements UserWishlistDao
{
    /**
     * the logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(DefaultUserWishlistDao.class);

	/**
	 * Constructor to initialize the class for a the Item Type
	 */
	public DefaultUserWishlistDao(final String typecode)
	{
		super(typecode);
	}

	/**
	 * To retrieve the wish list Model for the specific user with the specified code
	 */
	@Override
	public Wishlist2Model findMyWishlistByCode(final UserModel user, final String wishlistCode)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(
				"SELECT {pk} FROM {Wishlist2} WHERE {user} = ?user AND {code} = ?code");
		fQuery.addQueryParameter("user", user);
		fQuery.addQueryParameter("code", wishlistCode);

		final SearchResult<Wishlist2Model> result = this.getFlexibleSearchService().search(fQuery);

		if (result.getCount() > 1)
		{
			LOG.warn("More than one wishlist with the code {} found for user {}. Returning first!", wishlistCode, user.getName());
		}
		if (result.getCount() > 0)
		{
			return (result.getResult().iterator().next());
		}
		return null;
	}

	/**
	 * To find if the product already exists in a specific wishlist. To retrieve the Wishlistentry by its Wishlist model
	 * and Product model
	 */
	@Override
	public Wishlist2EntryModel findWishlistEntryForProduct(final Wishlist2Model wishlistModel, final ProductModel product)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(
				"SELECT {pk} FROM {Wishlist2Entry} WHERE {wishlist} = ?wishlist AND {product} = ?product");
		fQuery.addQueryParameter("wishlist", wishlistModel);
		fQuery.addQueryParameter("product", product);

		final SearchResult<Wishlist2EntryModel> result = this.getFlexibleSearchService().search(fQuery);

		if (result.getCount() > 1)
		{
			LOG.warn("Product {} exists in this wishlist. Returning first!", product.getCode());
		}
		if (result.getCount() > 0)
		{
			return (result.getResult().iterator().next());
		}
		return null;
	}
}
