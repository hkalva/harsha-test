/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.wishlistservices.daos;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;


/**
 * To implement Custom functionality for Wishlist.
 *
 * Since the OOTB WishlistDao extends Dao class and here we wanted to extend GenericDao<M> class, we had created a new
 * interface without any reference to the OOTb Dao Interface
 *
 * OOTB Wishlist2Dao & DefaultWishlist2Dao class extends Dao and AbstractItemDao respectively which has deprecated
 * member variables & methods, hence created a new Interface and Implementation class that extends GenericDao<M> and
 * DefaultGenericDao<M> respectively
 */
public interface UserWishlistDao extends GenericDao<Wishlist2Model>
{
	/**
	 * To retrieve the WishlistModel for a specific wishlist Code associated with a specific User.
	 */
	public Wishlist2Model findMyWishlistByCode(UserModel user, String wishlistCode);

	/**
	 * To find if the product already exists in a specific wishlist. To retrieve the WishlistEntry Model in a specific
	 * Wishlist by the product model.
	 */
	public Wishlist2EntryModel findWishlistEntryForProduct(Wishlist2Model wishlistModel, ProductModel product);
}
