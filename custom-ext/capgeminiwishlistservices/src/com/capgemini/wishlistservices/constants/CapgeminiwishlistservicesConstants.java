package com.capgemini.wishlistservices.constants;

/**
 *
 * @author lyonscg
 *
 */
public class CapgeminiwishlistservicesConstants extends GeneratedCapgeminiwishlistservicesConstants
{
    /**
     * constant for extension name.
     */
	public static final String EXTENSIONNAME = "capgeminiwishlistservices";

	/**
	 * default private constructor.
	 */
	private CapgeminiwishlistservicesConstants()
	{
		//empty
	}
	
	
}
