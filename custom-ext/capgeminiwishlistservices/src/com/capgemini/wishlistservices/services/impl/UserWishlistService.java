/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.wishlistservices.services.impl;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.wishlist2.impl.DefaultWishlist2Service;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Assert;

import com.capgemini.wishlistservices.daos.UserWishlistDao;
import com.capgemini.wishlistservices.daos.impl.DefaultPagedWishlistDao;
import com.capgemini.wishlistservices.data.WishlistData;
import com.capgemini.wishlistservices.data.WishlistEntryData;
import com.capgemini.wishlistservices.services.WishlistService;


/**
 * WishlistService implementation class to implement methods (1) to retrieve the Paged Wishlist lists, (2) create a new
 * Wishlist with a unique key generated field Code (3) to retrieve the wishlist by a User and wishlist Code#, (4) To
 * update Wishlist details
 */
public class UserWishlistService extends DefaultWishlist2Service implements WishlistService
{
    /**
     * the paged wishlist dao.
     */
	private DefaultPagedWishlistDao pagedWishlistDao;
	
	/**
	 * the user wishlist dao.
	 */
	private UserWishlistDao userWishlistDao;
	
	/**
	 * the user service.
	 */
	@Resource
	private UserService userService;
	
	/**
	 * the key generator.
	 */
	private KeyGenerator keyGenerator;
	
	/**
	 * the product service.
	 */
	@Resource
	private ProductService productService;

	/**
	 * To retrieve the paged list of Wish lists for specific Pagination parameters.
	 *
	 * @param pageableData
	 *           PageableData object with the params used for sorting.
	 * @return SearchPageData<Wishlist2Model>
	 */
	@Override
	public SearchPageData<Wishlist2Model> getPagedWishlists(final PageableData pageableData)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("user", getUserService().getCurrentUser());
		return getPagedWishlistDao().getPagedWishlists(params, pageableData);
	}

	/**
	 * Overriding the OOTB createWishlist method to invoke our custom createWishlistWithCode which will generate a unique
	 * key for every Wish list.
	 *
	 * @param user
	 *           User model object for whom the wish list is to be created.
	 * @param name
	 *           Name of the Wish list.
	 * @param description
	 *           Description for the wish list to be created.
	 * @return Wishlist2Model that was created.
	 */
	@Override
	public Wishlist2Model createWishlist(final UserModel user, final String name, final String description)
	{
		return createWishlistWithCode(user, name, description, Boolean.FALSE);
	}

	/**
	 * Method to retrieve the Wish list Model for a specific wish list by code of the current user
	 *
	 * @param wishlistCode
	 *           Code of the wish list to be retrieved.
	 * @return Wishlist2Model corresponding to the code passed.
	 */
	@Override
	public Wishlist2Model getMyWishlistByCode(final String wishlistCode)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("User Cannot be null", getUserService().getCurrentUser());
		return getUserWishlistDao().findMyWishlistByCode(getUserService().getCurrentUser(), wishlistCode);
	}

	/**
	 * Method to create a Wish list for a specific User. It will set the Name, Description and a new custom code field
	 * value generated by a key generator.
	 *
	 * @param user
	 *           User model object for whom the wish list is to be created.
	 * @param name
	 *           Name of the Wish list.
	 * @param description
	 *           Description for the wish list to be created.
	 * @param defaultWL
	 *           Flag to determine, if its a default Wish list for the user.
	 * @return Wishlist2Model object that was created.
	 */
	protected Wishlist2Model createWishlistWithCode(final UserModel user, final String name, final String description,
			final Boolean defaultWL)
	{
		final Wishlist2Model wishlist = new Wishlist2Model();
		wishlist.setName(name);
		wishlist.setDescription(description);
		wishlist.setDefault(defaultWL);
		wishlist.setUser(user);
		wishlist.setLastModifiedBy(user);
		wishlist.setCode(getKeyGenerator().generate().toString());
		if (saveWishlist(user))
		{
			getModelService().save(wishlist);
		}
		return wishlist;
	}

	/**
	 * To override the update the Wishlist Details method to perform null checks and set Name, Description & Last
	 * modified by.
	 *
	 * @param wishlistData
	 *           Wishlist Data object that needs to be updated.
	 * @return Updated model object.
	 */
	@Override
	public Wishlist2Model updateWishlist(final WishlistData wishlistData)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("WishlistData cannot be null", wishlistData);
		ServicesUtil.validateParameterNotNullStandardMessage("WishlistData Code cannot be null", wishlistData.getCode());

		final Wishlist2Model wishlistModel = getMyWishlistByCode(wishlistData.getCode());

		ServicesUtil.validateParameterNotNullStandardMessage("Wishlist Model cannot be null", wishlistModel);

		wishlistModel.setName(wishlistData.getName());
		wishlistModel.setDescription(wishlistData.getDescription());
		wishlistModel.setLastModifiedBy(getUserService().getCurrentUser());

		if (saveWishlist(getUserService().getCurrentUser()))
		{
			getModelService().save(wishlistModel);
		}

		return wishlistModel;
	}

	/**
	 * To override the super method to perform null checks before deletion.
	 *
	 * @param wishlistCode
	 *           Code to uniquely identify the Wishlist.
	 */
	@Override
	public void deleteWishlist(final String wishlistCode)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("Wishlist Code", wishlistCode);
		final Wishlist2Model wishlistModel = getMyWishlistByCode(wishlistCode);
		ServicesUtil.validateParameterNotNullStandardMessage("Wishlist Model", wishlistModel);

		if (saveWishlist(getUserService().getCurrentUser()))
		{
			getModelService().remove(wishlistModel);
		}
	}

	/**
	 * To override the add WishlistEntry for Product to Wishlist to perform Null checks and set the last modified by user
	 * to the current user.
	 *
	 * Check if the product doesn't exist in wishlist & then add to the Wishlist.
	 *
	 * @param wishlistCode
	 *           Code to uniquely identify the Wishlist.
	 * @param wishlistEntryData
	 *           Wish list Entry data object.
	 */
	@Override
	public void addProductToWishlist(final String wishlistCode, final WishlistEntryData wishlistEntryData)
	{
		final Wishlist2Model wishlistModel = getMyWishlistByCode(wishlistCode);
		ServicesUtil.validateParameterNotNullStandardMessage("wishlistModel cannot be null", wishlistModel);

		final ProductModel productModel = getProductService().getProductForCode(wishlistEntryData.getProduct().getCode());
		ServicesUtil.validateParameterNotNullStandardMessage("productModel cannot be null", productModel);

		//Verify if product already exists in the Wishlist
		Assert.assertNull("Product already exists in Wishlist",
				getUserWishlistDao().findWishlistEntryForProduct(wishlistModel, productModel));

		wishlistModel.setLastModifiedBy(userService.getCurrentUser());

		super.addWishlistEntry(wishlistModel, productModel, wishlistEntryData.getDesired(), wishlistEntryData.getPriority(),
				wishlistEntryData.getComment());
	}

	/**
	 * To override the remove the wish list entry method to add null checks and also set the last modifiedBy user.
	 *
	 * @param wishlistCode
	 *           Code to uniquely identify the Wish list.
	 * @param productCode
	 *           Product's code to be removed.
	 */
	@Override
	public void removeWishlistEntryForProduct(final String wishlistCode, final String productCode)
	{
		final Wishlist2Model wishlistModel = getMyWishlistByCode(wishlistCode);
		ServicesUtil.validateParameterNotNullStandardMessage("wishlistModel", wishlistModel);

		final ProductModel productModel = getProductService().getProductForCode(productCode);
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", productModel);

		wishlistModel.setLastModifiedBy(userService.getCurrentUser());

		super.removeWishlistEntryForProduct(productModel, wishlistModel);
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * @return the keyGenerator
	 */
	public KeyGenerator getKeyGenerator()
	{
		return keyGenerator;
	}

	/**
	 * @param keyGenerator
	 *           the keyGenerator to set
	 */
	public void setKeyGenerator(final KeyGenerator keyGenerator)
	{
		this.keyGenerator = keyGenerator;
	}

	/**
	 * @return the pagedWishlistDao
	 */
	public DefaultPagedWishlistDao getPagedWishlistDao()
	{
		return pagedWishlistDao;
	}

	/**
	 * @param pagedWishlistDao
	 *           the pagedWishlistDao to set
	 */
	public void setPagedWishlistDao(final DefaultPagedWishlistDao pagedWishlistDao)
	{
		this.pagedWishlistDao = pagedWishlistDao;
	}

	/**
	 * @return the userWishlistDao
	 */
	public UserWishlistDao getUserWishlistDao()
	{
		return userWishlistDao;
	}

	/**
	 * @param userWishlistDao
	 *           the userWishlistDao to set
	 */
	public void setUserWishlistDao(final UserWishlistDao userWishlistDao)
	{
		this.userWishlistDao = userWishlistDao;
	}

	/**
	 * @return the productService
	 */
	public ProductService getProductService()
	{
		return productService;
	}

	/**
	 * @param productService
	 *           the productService to set
	 */
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

}
