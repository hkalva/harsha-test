/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.wishlistservices.services;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.wishlist2.Wishlist2Service;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import com.capgemini.wishlistservices.data.WishlistData;
import com.capgemini.wishlistservices.data.WishlistEntryData;


/**
 * LEA Wishlist Service Interface to expose methods (1) to retrieve the Paged Wishlist lists, (2) to retrieve the
 * wishlist by a User and wishlist Code#, (3) To update Wishlist details
 */

public interface WishlistService extends Wishlist2Service
{
    /**
     * To retrieve the paged list of Wishlists for a User.
     * 
     * @param pageableData
     *            - the pageable data.
     * @return SearchPageData<Wishlist2Model>
     */
    SearchPageData<Wishlist2Model> getPagedWishlists(PageableData pageableData);

    /**
     * To find the Wishlist for a User by wishlist Code #.
     * 
     * @param wishlistCode
     *            - the wishlist code.
     * @return Wishlist2Model
     */
    Wishlist2Model getMyWishlistByCode(String wishlistCode);

    /**
     * To update the Wishlist details of the current user.
     * 
     * @param wishlistData
     *            - the wishlist data.
     * @return Wishlist2Model
     */
    Wishlist2Model updateWishlist(WishlistData wishlistData);

    /**
     * To update the Wishlist details of the current user.
     * 
     * @param wishlistCode
     *            - the wishlist code.
     */
    void deleteWishlist(String wishlistCode);

    /**
     * To add product to Wishlist if it doesnt already exist in the wishlist.
     * 
     * @param wishlistCode
     *            - the wishlist code.
     * @param wishlistEntryData
     *            - the wishlist entry data.
     */
    void addProductToWishlist(final String wishlistCode, final WishlistEntryData wishlistEntryData);

    /**
     * To remove a Wishlist Entry / Product from a specific wishlist.
     * 
     * @param wishlistCode
     *            - the wishlist code.
     * @param productCode
     *            - the product code.
     */
    void removeWishlistEntryForProduct(String wishlistCode, String productCode);
}
