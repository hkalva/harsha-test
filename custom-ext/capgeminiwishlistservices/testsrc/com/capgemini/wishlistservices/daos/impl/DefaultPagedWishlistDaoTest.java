/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.wishlistservices.daos.impl;

import static org.junit.Assert.assertEquals;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.HashMap;

import javax.annotation.Resource;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.capgemini.wishlistservices.services.WishlistService;


/**
 *
 */
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(LogRunListener.class)
public class DefaultPagedWishlistDaoTest extends ServicelayerTransactionalTest
{
	Logger LOG = LogManager.getLogger(DefaultPagedWishlistDaoTest.class);

	private static final String WL1 = "BWishlistOne";
	private static final String WL1_DESC = "WishlistOne Desc";
	private static final String WL2 = "AWishlistTwo";
	private static final String WL2_DESC = "WishlistTwo Desc";
	private static final int PAGE_SIZE = 5;

	@Resource
	private WishlistService wishlistService;
	@Resource
	private DefaultPagedWishlistDao pagedWishlistDao;
	@Resource
	private UserService userService;
	@Resource
	private ProductService productService;
	@Resource
	private CatalogVersionService catalogVersionService;


	private ProductModel product1;
	private ProductModel product2;
	private UserModel demoUser1;
	private UserModel demoUser2;

	@Before
	public void setUp() throws Exception
	{
		createCoreData();
		createHardwareCatalog();
		createDefaultUsers();

		final CatalogVersionModel catVersion = catalogVersionService.getCatalogVersion("hwcatalog", "Online");
		catalogVersionService.addSessionCatalogVersion(catVersion);

		product1 = productService.getProductForCode("HW2300-2356");
		product2 = productService.getProductForCode("HW2300-4121");

		demoUser2 = userService.getUserForUID("hweaving");
		userService.setCurrentUser(demoUser2);
		final Wishlist2Model wishlist1 = wishlistService.createWishlist(demoUser2, WL1, WL1_DESC);
		Assert.assertNotNull("Wishlist created for user hweaving successfully", wishlist1);

		demoUser1 = userService.getUserForUID("demo");
		userService.setCurrentUser(demoUser1);
	}

	/**
	 * To test if the Paged Results are returned if the available Wishlists
	 */
	@Test
	public void testGetPagedWishlists()
	{
		final Wishlist2Model wishlist1 = wishlistService.createWishlist(demoUser1, WL1, WL1_DESC);
		Assert.assertNotNull("Wishlist created successfully", wishlist1);

		final HashMap<String, Object> params = new HashMap();
		params.put("user", demoUser1);

		final PageableData pageableData = createPageableData();

		final SearchPageData<Wishlist2Model> wishlistSearchPageData = pagedWishlistDao.getPagedWishlists(params, pageableData);
		Assert.assertNotNull("wishlistSearchPageData cannot be null", wishlistSearchPageData);
		Assert.assertEquals("SearchPageData should have a result of 1", 1, wishlistSearchPageData.getResults().size());
		Assert.assertEquals("Wishlist Code of the result returned has to be " + wishlist1.getCode(), wishlist1.getCode(),
				wishlistSearchPageData.getResults().get(0).getCode());
	}

	/**
	 * To test if the result has the result set ordered by Name
	 */
	@Test
	public void testGetPagedWishlistsIsOrderedByName()
	{
		final Wishlist2Model wishlist1 = wishlistService.createWishlist(demoUser1, WL1, WL1_DESC);
		Assert.assertNotNull("Wishlist created successfully", wishlist1);

		final Wishlist2Model wishlist2 = wishlistService.createWishlist(demoUser1, WL2, WL2_DESC);
		Assert.assertNotNull("Wishlist created successfully", wishlist2);

		final HashMap<String, Object> params = new HashMap();
		params.put("user", demoUser1);

		final PageableData pageableData = createPageableData();

		final SearchPageData<Wishlist2Model> wishlistSearchPageData = pagedWishlistDao.getPagedWishlists(params, pageableData);
		Assert.assertNotNull("wishlistSearchPageData cannot be null", wishlistSearchPageData);
		Assert.assertEquals("SearchPageData should have a result of 2", 2, wishlistSearchPageData.getResults().size());
		Assert.assertEquals("Wishlist Name of the first result returned has to be " + wishlist2.getName(), wishlist2.getName(),
				wishlistSearchPageData.getResults().get(0).getName());
		Assert.assertEquals("Wishlist Name of the second result returned has to be " + wishlist1.getName(), wishlist1.getName(),
				wishlistSearchPageData.getResults().get(1).getName());
	}

	/**
	 * To test if the result has the result set ordered by Last Modified by
	 */
	@Test
	public void testGetPagedWishlistsIsOrderedByLastModifiedBy()
	{
		final Wishlist2Model wishlist1 = wishlistService.createWishlist(demoUser1, WL1, WL1_DESC);
		Assert.assertNotNull("Wishlist created successfully", wishlist1);

		final Wishlist2Model wishlist2 = wishlistService.createWishlist(demoUser1, WL2, WL2_DESC);
		Assert.assertNotNull("Wishlist created successfully", wishlist2);

		final HashMap<String, Object> params = new HashMap();
		params.put("user", demoUser1);

		final PageableData pageableData = createPageableData();

		final SearchPageData<Wishlist2Model> wishlistSearchPageData = pagedWishlistDao.getPagedWishlists(params, pageableData);
		Assert.assertNotNull("wishlistSearchPageData cannot be null", wishlistSearchPageData);
		Assert.assertEquals("SearchPageData should have a result of 2", 2, wishlistSearchPageData.getResults().size());
		Assert.assertEquals("Wishlist Name of the first result returned has to be " + wishlist2.getLastModifiedBy(),
				wishlist2.getLastModifiedBy(), wishlistSearchPageData.getResults().get(0).getLastModifiedBy());
		Assert.assertEquals("Wishlist Name of the second result returned has to be " + wishlist1.getLastModifiedBy(),
				wishlist1.getLastModifiedBy(), wishlistSearchPageData.getResults().get(1).getLastModifiedBy());
	}

	/**
	 * To test that the GetPagedWishlists method returns an empty results object
	 */
	@Test
	public void testGetPagedWishlistsInExistent()
	{
		final HashMap<String, Object> params = new HashMap();
		params.put("user", demoUser1);

		final PageableData pageableData = createPageableData();

		final SearchPageData<Wishlist2Model> wishlistSearchPageData = pagedWishlistDao.getPagedWishlists(params, pageableData);
		Assert.assertNotNull("wishlistSearchPageData cannot be null", wishlistSearchPageData);
		Assert.assertEquals("SearchPageData should have a result of 0", 0, wishlistSearchPageData.getResults().size());

	}

	/**
	 * To test that the method returns paginated number of entries based on the Page size and Current Page number passed
	 * as an input
	 */
	@Test
	public void testPagination()
	{
		final HashMap<String, Object> params = new HashMap();
		params.put("user", demoUser1);

		final int pageSize = 3;
		final Wishlist2Model[] models = new Wishlist2Model[5];
		for (int i = 0; i < models.length; i++)
		{
			models[i] = wishlistService.createWishlist(demoUser1, WL1 + i, WL1_DESC + i);
		}

		final PageableData pageableData = createPageableData();
		pageableData.setPageSize(pageSize);

		// get first page of results
		pageableData.setCurrentPage(0);
		final SearchPageData<Wishlist2Model> results0 = pagedWishlistDao.getPagedWishlists(params, pageableData);
		Assert.assertEquals("SearchPageData should have a result of " + pageSize, pageSize, results0.getResults().size());
		// check if 1st 3 budgets are in the 1st page
		for (int i = 0; i < pageSize; i++)
		{
			assertEquals(models[i].getName(), results0.getResults().get(i).getName());
		}

		// get second page of results
		pageableData.setCurrentPage(1);
		final SearchPageData<Wishlist2Model> results1 = pagedWishlistDao.getPagedWishlists(params, pageableData);
		Assert.assertEquals("SearchPageData should have a result of 2", 2, results1.getResults().size());
		// check if last 2 budgets are in the 2nd page
		for (int i = 0; i < 2; i++)
		{
			assertEquals(models[pageSize + i].getName(), results1.getResults().get(i).getName());
		}
	}

	public static PageableData createPageableData(final int pageNumber, final int pageSize, final String sortCode)
	{
		final PageableData pageableData = new PageableData();
		pageableData.setCurrentPage(pageNumber);
		pageableData.setSort(sortCode);
		pageableData.setPageSize(pageSize);
		return pageableData;
	}

	public static PageableData createPageableData()
	{
		return createPageableData(0, 5, null);
	}

}
