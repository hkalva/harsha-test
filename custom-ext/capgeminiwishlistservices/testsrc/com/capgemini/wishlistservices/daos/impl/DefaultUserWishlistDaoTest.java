/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.wishlistservices.daos.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.Arrays;

import javax.annotation.Resource;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.capgemini.wishlistservices.daos.UserWishlistDao;
import com.capgemini.wishlistservices.data.WishlistEntryData;
import com.capgemini.wishlistservices.services.WishlistService;


/**
 * Test Class to test DefaultUserWishlistDaoTest class
 */
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(LogRunListener.class)
public class DefaultUserWishlistDaoTest extends ServicelayerTransactionalTest
{
	Logger LOG = LogManager.getLogger(DefaultUserWishlistDaoTest.class);

	private static final String WL1 = "WishlistOne";
	private static final String WL1_DESC = "WishlistOne Desc";

	@Resource
	private WishlistService wishlistService;
	@Resource
	private UserWishlistDao userWishlistDao;
	@Resource
	private UserService userService;
	@Resource
	private ProductService productService;
	@Resource
	private CatalogVersionService catalogVersionService;
	@Resource
	private ProductFacade productFacade;

	private ProductModel product1;
	private ProductModel product2, product3;
	private UserModel demoUser1;
	private UserModel demoUser2;
	private ProductData productData1;
	private ProductData productData2;
	private WishlistEntryData wishlistEntryData1, wishlistEntryData2;

	@Before
	public void setUp() throws Exception
	{
		createCoreData();
		createHardwareCatalog();
		createDefaultUsers();

		final CatalogVersionModel catVersion = catalogVersionService.getCatalogVersion("hwcatalog", "Online");
		catalogVersionService.addSessionCatalogVersion(catVersion);

		product1 = productService.getProductForCode("HW2300-2356");
		product2 = productService.getProductForCode("HW2300-4121");
		product3 = productService.getProductForCode("HW2300-3843");
		productData1 = productFacade.getProductForCodeAndOptions("HW2300-2356", Arrays.asList(ProductOption.BASIC));
		productData2 = productFacade.getProductForCodeAndOptions("HW2300-4121", Arrays.asList(ProductOption.BASIC));

		wishlistEntryData1 = new WishlistEntryData();
		wishlistEntryData1.setProduct(productData1);
		wishlistEntryData1.setDesired(Integer.valueOf(1));
		wishlistEntryData1.setPriority(Wishlist2EntryPriority.MEDIUM);
		wishlistEntryData1.setComment("I like it");

		wishlistEntryData2 = new WishlistEntryData();
		wishlistEntryData2.setProduct(productData2);
		wishlistEntryData2.setDesired(Integer.valueOf(1));
		wishlistEntryData2.setPriority(Wishlist2EntryPriority.MEDIUM);
		wishlistEntryData2.setComment("Must Have");

		demoUser1 = userService.getUserForUID("demo");
		userService.setCurrentUser(demoUser1);
	}

	@Test
	public void testFindMyWishlistByCode()
	{
		final Wishlist2Model wishlist1 = wishlistService.createWishlist(demoUser1, WL1, WL1_DESC);
		Assert.assertNotNull("Wishlist created successfully", wishlist1);
		//Retrieve Wishlist by code
		Wishlist2Model wishlistFound = userWishlistDao.findMyWishlistByCode(demoUser1, wishlist1.getCode());
		Assert.assertNotNull("Demo User Wishlist is not null ", wishlistFound);
		Assert.assertEquals("Demo User's Wishlist Name matches", wishlist1.getName(), wishlistFound.getName());

		//Change User & try retrieving
		demoUser2 = userService.getUserForUID("hweaving");
		userService.setCurrentUser(demoUser2);
		wishlistFound = userWishlistDao.findMyWishlistByCode(demoUser2, wishlist1.getCode());
		Assert.assertNull("Demo2 User Wishlist should not contain wishlist with the code " + wishlist1.getCode(), wishlistFound);
	}

	/**
	 * Test that the find wishlist with invalid code returns null
	 */
	@Test
	public void testFindMyWishlistByCodeWithInvalidCode()
	{
		final Wishlist2Model wishlist1 = wishlistService.createWishlist(demoUser1, WL1, WL1_DESC);
		Assert.assertNotNull("Wishlist created successfully", wishlist1);
		//Retrieve Wishlist by some other code
		final Wishlist2Model wishlistFound = userWishlistDao.findMyWishlistByCode(demoUser1, "test");
		Assert.assertNull("Wishlist for the code test must be null ", wishlistFound);

	}

	/**
	 * To test the FindWishlistEntryForProduct() method.
	 *
	 * Verify that the product added to the wishlist is found by this method call.
	 *
	 * Also, verify that this method does'nt return an entry for a produc that was not added
	 */
	@Test
	public void testFindWishlistEntryForProduct()
	{
		final Wishlist2Model wishlist1 = wishlistService.createWishlist(demoUser1, WL1, WL1_DESC);
		Assert.assertNotNull("Wishlist created successfully", wishlist1);

		wishlistService.addProductToWishlist(wishlist1.getCode(), wishlistEntryData1);
		wishlistService.addProductToWishlist(wishlist1.getCode(), wishlistEntryData2);
		Assert.assertEquals("No of Products on wishlist", 2, wishlistService.getMyWishlistByCode(wishlist1.getCode()).getEntries()
				.size());

		Wishlist2EntryModel wishlistEntryModel = userWishlistDao.findWishlistEntryForProduct(wishlist1, product1);
		Assert.assertNotNull("Wishlist entry for the product1 should be found in this wishlist ", wishlistEntryModel);
		Assert.assertSame("Product Code has to be same", product1.getCode(), wishlistEntryModel.getProduct().getCode());

		wishlistEntryModel = userWishlistDao.findWishlistEntryForProduct(wishlist1, product2);
		Assert.assertNotNull("Wishlist entry for the product2 should be found in this wishlist ", wishlistEntryModel);
		Assert.assertSame("Product Code has to be same", product2.getCode(), wishlistEntryModel.getProduct().getCode());

		wishlistEntryModel = userWishlistDao.findWishlistEntryForProduct(wishlist1, product3);
		Assert.assertNull("Wishlist entry for the product3 should not be found in this wishlist", wishlistEntryModel);
	}
}
