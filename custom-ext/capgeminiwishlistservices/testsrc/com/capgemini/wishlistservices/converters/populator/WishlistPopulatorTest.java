package com.capgemini.wishlistservices.converters.populator;

import java.util.Collections;
import java.util.Date;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.wishlistservices.data.WishlistData;
import com.capgemini.wishlistservices.data.WishlistEntryData;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

@UnitTest
public class WishlistPopulatorTest
{
    /**
     * the populator to test.
     */
    private WishlistPopulator populator;
    
    /**
     * the target.
     */
    private WishlistData target;
    
    /**
     * the wishlist entry converter.
     */
    @Mock
    private Converter<Wishlist2EntryModel, WishlistEntryData> wishlistEntryConverter;
    
    /**
     * the customer converter.
     */
    @Mock
    private Converter<UserModel, CustomerData> customerConverter;
 
    /**
     * the target.
     */
    @Mock
    private Wishlist2Model source;

    /**
     * the user.
     */
    @Mock
    private UserModel user;

    /**
     * the wishlist entry.
     */
    @Mock
    private Wishlist2EntryModel entry;
    
    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        populator = new WishlistPopulator();
        populator.setCustomerConverter(customerConverter);
        populator.setWishlistEntryConverter(wishlistEntryConverter);
        target = new WishlistData();
        Mockito.when(customerConverter.convert(Mockito.any(UserModel.class))).thenReturn(new CustomerData());
        Mockito.when(wishlistEntryConverter.convert(Mockito.any(Wishlist2EntryModel.class))).thenReturn(new WishlistEntryData());
        Mockito.when(source.getName()).thenReturn("name");
        Mockito.when(source.getDescription()).thenReturn("description");
        Mockito.when(source.getCreationtime()).thenReturn(new Date());
        Mockito.when(source.getModifiedtime()).thenReturn(new Date());
        Mockito.when(source.getCode()).thenReturn("code");
    }
    
    /**
     * test no entries no user.
     */
    @Test
    public void testNoEntriesNoUser()
    {
        Mockito.when(source.getEntries()).thenReturn(null);
        Mockito.when(source.getLastModifiedBy()).thenReturn(null);
        populator.populate(source, target);
        Assert.assertTrue(source.getName().equals(target.getName()));
        Assert.assertTrue(source.getDescription().equals(target.getDescription()));
        Assert.assertTrue(source.getCreationtime().equals(target.getDateCreated()));
        Assert.assertTrue(source.getModifiedtime().equals(target.getDateModified()));
        Assert.assertTrue(source.getCode().equals(target.getCode()));
        Assert.assertTrue(target.getLastModifiedBy() == null);
        Assert.assertTrue(CollectionUtils.isEmpty(target.getWishlistEntries()));
        Assert.assertTrue(CollectionUtils.size(target.getWishlistEntries()) == target.getNumberOfWishlistEntries().intValue());
    }
    
    /**
     * test no entries.
     */
    @Test
    public void testNoEntries()
    {
        Mockito.when(source.getEntries()).thenReturn(null);
        Mockito.when(source.getLastModifiedBy()).thenReturn(user);
        populator.populate(source, target);
        Assert.assertTrue(source.getName().equals(target.getName()));
        Assert.assertTrue(source.getDescription().equals(target.getDescription()));
        Assert.assertTrue(source.getCreationtime().equals(target.getDateCreated()));
        Assert.assertTrue(source.getModifiedtime().equals(target.getDateModified()));
        Assert.assertTrue(source.getCode().equals(target.getCode()));
        Assert.assertTrue(target.getLastModifiedBy() != null);
        Assert.assertTrue(CollectionUtils.isEmpty(target.getWishlistEntries()));
        Assert.assertTrue(CollectionUtils.size(target.getWishlistEntries()) == target.getNumberOfWishlistEntries().intValue());
    }

    /**
     * test.
     */
    @Test
    public void test()
    {
        Mockito.when(source.getEntries()).thenReturn(Collections.singletonList(entry));
        Mockito.when(source.getLastModifiedBy()).thenReturn(user);
        populator.populate(source, target);
        Assert.assertTrue(source.getName().equals(target.getName()));
        Assert.assertTrue(source.getDescription().equals(target.getDescription()));
        Assert.assertTrue(source.getCreationtime().equals(target.getDateCreated()));
        Assert.assertTrue(source.getModifiedtime().equals(target.getDateModified()));
        Assert.assertTrue(source.getCode().equals(target.getCode()));
        Assert.assertTrue(target.getLastModifiedBy() != null);
        Assert.assertTrue(CollectionUtils.isNotEmpty(target.getWishlistEntries()));
        Assert.assertTrue(CollectionUtils.size(target.getWishlistEntries()) == target.getNumberOfWishlistEntries().intValue());
    }
    
}
