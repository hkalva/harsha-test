package com.capgemini.wishlistservices.converters.populator;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.wishlistservices.data.WishlistEntryData;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;

@UnitTest
public class WishlistEntryPopulatorTest
{
    /**
     * the populator to test.
     */
    private WishlistEntryPopulator populator;
    
    /**
     * the target.
     */
    private WishlistEntryData target;
    
    /**
     * the product converter.
     */
    @Mock
    private Converter<ProductModel, ProductData> productConverter;
    
    /**
     * the source.
     */
    @Mock
    private Wishlist2EntryModel source;
    
    /**
     * the product model.
     */
    @Mock
    private ProductModel productModel;
    
    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        populator = new WishlistEntryPopulator();
        populator.setProductConverter(productConverter);
        target = new WishlistEntryData();
        Mockito.when(productConverter.convert(Mockito.any(ProductModel.class))).thenReturn(new ProductData());
        Mockito.when(source.getComment()).thenReturn("comment");
        Mockito.when(source.getAddedDate()).thenReturn(new Date());
        Mockito.when(source.getPriority()).thenReturn(Wishlist2EntryPriority.HIGHEST);
        Mockito.when(source.getDesired()).thenReturn(Integer.valueOf(1));
        Mockito.when(source.getReceived()).thenReturn(Integer.valueOf(1));
    }
    
    /**
     * test without product.
     */
    @Test
    public void testWithoutProduct()
    {
        Mockito.when(source.getProduct()).thenReturn(null);
        populator.populate(source, target);
        Assert.assertTrue(target.getProduct() == null);
        Assert.assertTrue(source.getComment().equals(target.getComment()));
        Assert.assertTrue(source.getAddedDate().equals(target.getAddedDate()));
        Assert.assertTrue(source.getPriority().equals(target.getPriority()));
        Assert.assertTrue(source.getDesired().equals(target.getDesired()));
        Assert.assertTrue(source.getReceived().equals(target.getReceived()));
    }
    
    /**
     * test with product.
     */
    @Test
    public void testWithProduct()
    {
        Mockito.when(source.getProduct()).thenReturn(productModel);
        populator.populate(source, target);
        Assert.assertTrue(target.getProduct() != null);
        Assert.assertTrue(source.getComment().equals(target.getComment()));
        Assert.assertTrue(source.getAddedDate().equals(target.getAddedDate()));
        Assert.assertTrue(source.getPriority().equals(target.getPriority()));
        Assert.assertTrue(source.getDesired().equals(target.getDesired()));
        Assert.assertTrue(source.getReceived().equals(target.getReceived()));
    }
    
}
