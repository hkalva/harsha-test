/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.capgemini.wishlistservices.services.impl;

import java.util.ArrayList;
import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.capgemini.wishlistservices.daos.UserWishlistDao;
import com.capgemini.wishlistservices.data.WishlistData;
import com.capgemini.wishlistservices.data.WishlistEntryData;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;
import de.hybris.platform.wishlist2.impl.daos.Wishlist2Dao;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

/**
 * 
 * @author lyonscg
 *
 */
@UnitTest
public class UserWishlistServiceTest 
{
    /**
     * the service to test.
     */
    private UserWishlistService service;
    
    /**
     * the model service.
     */
    @Mock
    private ModelService modelService;

    /**
     * the user service.
     */
    @Mock
    private UserService userService;
    
    /**
     * the customer.
     */
    @Mock
    private CustomerModel customer;
    
    /**
     * the anonymous customer.
     */
    @Mock
    private CustomerModel anonymous;
    
    /**
     * the key generator.
     */
    @Mock
    private KeyGenerator keyGenerator;
    
    /**
     * the user wishlist dao.
     */
    @Mock
    private UserWishlistDao userWishlistDao;
    
    /**
     * the product service.
     */
    @Mock
    private ProductService productService;
    
    /**
     * the product.
     */
    @Mock
    private ProductModel product;
    
    /**
     * the wishlist dao.
     */
    @Mock
    private Wishlist2Dao wishlistDao;
    
    /**
     * the wishlist entry.
     */
    @Mock
    private Wishlist2EntryModel wishlistEntry;
    
    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        service = new UserWishlistService();
        service.setUserService(userService);
        service.setModelService(modelService);
        service.setKeyGenerator(keyGenerator);
        service.setUserWishlistDao(userWishlistDao);
        service.setProductService(productService);
        service.setWishlistDao(wishlistDao);
        Mockito.doNothing().when(modelService).save(Mockito.anyObject());
        Mockito.doNothing().when(modelService).remove(Mockito.anyObject());
        Mockito.when(customer.getUid()).thenReturn("customer");
        Mockito.when(anonymous.getUid()).thenReturn("anonymous");
        Mockito.when(keyGenerator.generate()).thenReturn(String.valueOf(System.currentTimeMillis()));
    }
    
    /**
     * test createWishlist for customer.
     */
    @Test
    public void testCreateWishlistCustomer()
    {
        String name = "wishlist name";
        String description = "wishlist description";
        Wishlist2Model wishlist = service.createWishlist(customer, name, description);
        Assert.assertTrue(name.equals(wishlist.getName()));
        Assert.assertTrue(description.equals(wishlist.getDescription()));
        Assert.assertTrue(Boolean.FALSE.equals(wishlist.getDefault()));
        Assert.assertTrue(customer.equals(wishlist.getUser()));
        Assert.assertTrue(customer.equals(wishlist.getLastModifiedBy()));
        Assert.assertTrue(StringUtils.isNotBlank(wishlist.getCode()));
        Mockito.verify(modelService, Mockito.times(1)).save(Mockito.any(Wishlist2Model.class));
    }
    
    /**
     * test createWishlist for anonymous.
     */
    @Test
    public void testCreateWishlistAnonymous()
    {
        String name = "wishlist name";
        String description = "wishlist description";
        Wishlist2Model wishlist = service.createWishlist(anonymous, name, description);
        Assert.assertTrue(name.equals(wishlist.getName()));
        Assert.assertTrue(description.equals(wishlist.getDescription()));
        Assert.assertTrue(Boolean.FALSE.equals(wishlist.getDefault()));
        Assert.assertTrue(anonymous.equals(wishlist.getUser()));
        Assert.assertTrue(anonymous.equals(wishlist.getLastModifiedBy()));
        Assert.assertTrue(StringUtils.isNotBlank(wishlist.getCode()));
        Mockito.verify(modelService, Mockito.never()).save(Mockito.any(Wishlist2Model.class));
    }
    
    /**
     * test getMyWishlistByCode null user.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetMyWishlistByCodeNullUser()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(null);
        service.getMyWishlistByCode("wishlistcode");
    }

    /**
     * test getMyWishlistByCode.
     */
    @Test
    public void testGetMyWishlistByCode()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(anonymous);
        service.getMyWishlistByCode("wishlistcode");
        Mockito.verify(userWishlistDao, Mockito.times(1)).findMyWishlistByCode(Mockito.any(UserModel.class), Mockito.anyString());
    }
    
    /**
     * test updateWishlist null wishlist data.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testUpdateWishlistNullWishlistData()
    {
        service.updateWishlist(null);
    }

    /**
     * test updateWishlist null wishlist code.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testUpdateWishlistNullWishlistCode()
    {
        WishlistData data = new WishlistData();
        service.updateWishlist(data);
    }

    /**
     * test updateWishlist invalid wishlist code.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testUpdateWishlistInvalidWishlistCode()
    {
        WishlistData data = new WishlistData();
        String code = "0000";
        Mockito.when(userService.getCurrentUser()).thenReturn(anonymous);
        Mockito.when(userWishlistDao.findMyWishlistByCode(anonymous, code)).thenReturn(null);
        service.updateWishlist(data);
    }
    
    /**
     * test updateWishlist anonymous.
     */
    @Test
    public void testUpdateWishlistAnonymous()
    {
        WishlistData data = new WishlistData();
        String code = "1234";
        String name = "test1234";
        String description = "testdesc";
        data.setCode(code);
        data.setName(name);
        data.setDescription(description);
        Mockito.when(userService.getCurrentUser()).thenReturn(anonymous);
        Mockito.when(userWishlistDao.findMyWishlistByCode(anonymous, code)).thenReturn(new Wishlist2Model());
        Wishlist2Model wishlist = service.updateWishlist(data);
        Assert.assertTrue(name.equals(wishlist.getName()));
        Assert.assertTrue(description.equals(wishlist.getDescription()));
        Assert.assertTrue(anonymous.equals(wishlist.getLastModifiedBy()));
        Mockito.verify(modelService, Mockito.never()).save(Mockito.any(Wishlist2Model.class));
    }

    /**
     * test updateWishlist customer.
     */
    @Test
    public void testUpdateWishlistCustomer()
    {
        WishlistData data = new WishlistData();
        String code = "5678";
        String name = "test5678";
        String description = "testdesc5678";
        data.setCode(code);
        data.setName(name);
        data.setDescription(description);
        Mockito.when(userService.getCurrentUser()).thenReturn(customer);
        Mockito.when(userWishlistDao.findMyWishlistByCode(customer, code)).thenReturn(new Wishlist2Model());
        Wishlist2Model wishlist = service.updateWishlist(data);
        Assert.assertTrue(name.equals(wishlist.getName()));
        Assert.assertTrue(description.equals(wishlist.getDescription()));
        Assert.assertTrue(customer.equals(wishlist.getLastModifiedBy()));
        Mockito.verify(modelService, Mockito.times(1)).save(Mockito.any(Wishlist2Model.class));
    }
    
    /**
     * test deleteWishlist null code.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testDeleteWishlistNullCode()
    {
        service.deleteWishlist(null);
    }
    
    /**
     * test deleteWishlist null code.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testDeleteWishlistInvalidCode()
    {
        String code = "9999";
        Mockito.when(userService.getCurrentUser()).thenReturn(anonymous);
        Mockito.when(userWishlistDao.findMyWishlistByCode(anonymous, code)).thenReturn(null);
        service.deleteWishlist(code);
    }

    /**
     * test deleteWishlist anonymous.
     */
    @Test
    public void testDeleteWishlistAnonymous()
    {
        String code = "8888";
        Mockito.when(userService.getCurrentUser()).thenReturn(anonymous);
        Mockito.when(userWishlistDao.findMyWishlistByCode(anonymous, code)).thenReturn(new Wishlist2Model());
        service.deleteWishlist(code);
        Mockito.verify(modelService, Mockito.never()).remove(Mockito.any(Wishlist2Model.class));
    }

    /**
     * test deleteWishlist customer.
     */
    @Test
    public void testDeleteWishlistCustomer()
    {
        String code = "7777";
        Mockito.when(userService.getCurrentUser()).thenReturn(customer);
        Mockito.when(userWishlistDao.findMyWishlistByCode(customer, code)).thenReturn(new Wishlist2Model());
        service.deleteWishlist(code);
        Mockito.verify(modelService, Mockito.times(1)).remove(Mockito.any(Wishlist2Model.class));
    }

    /**
     * test addProductToWishlist invalid wishlist code.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAddProductToWishlistInvalidWishlist()
    {
        String wishlistCode = "4567";
        String productCode = "0000000001234";
        ProductData productData = new ProductData();
        productData.setCode(productCode);
        WishlistEntryData wishlistEntryData = new WishlistEntryData();
        wishlistEntryData.setProduct(productData);
        Mockito.when(userService.getCurrentUser()).thenReturn(anonymous);
        Mockito.when(userWishlistDao.findMyWishlistByCode(anonymous, wishlistCode)).thenReturn(null);
        Mockito.when(productService.getProductForCode(productCode)).thenThrow(new UnknownIdentifierException("error"));
        service.addProductToWishlist(wishlistCode, wishlistEntryData);
    }

    /**
     * test addProductToWishlist invalid product.
     */
    @Test(expected = UnknownIdentifierException.class)
    public void testAddProductToWishlistInvalidProduct()
    {
        String wishlistCode = "5678";
        String productCode = "0000000004567";
        ProductData productData = new ProductData();
        productData.setCode(productCode);
        WishlistEntryData wishlistEntryData = new WishlistEntryData();
        wishlistEntryData.setProduct(productData);
        Mockito.when(userService.getCurrentUser()).thenReturn(anonymous);
        Mockito.when(userWishlistDao.findMyWishlistByCode(anonymous, wishlistCode)).thenReturn(new Wishlist2Model());
        Mockito.when(productService.getProductForCode(productCode)).thenThrow(new UnknownIdentifierException("error"));
        service.addProductToWishlist(wishlistCode, wishlistEntryData);
    }

    /**
     * test addProductToWishlist entry exists.
     */
    @Test(expected = AssertionError.class)
    public void testAddProductToWishlistEntryExists()
    {
        String wishlistCode = "6789";
        String productCode = "0000000005678";
        ProductData productData = new ProductData();
        productData.setCode(productCode);
        WishlistEntryData wishlistEntryData = new WishlistEntryData();
        wishlistEntryData.setProduct(productData);
        Mockito.when(userService.getCurrentUser()).thenReturn(anonymous);
        Mockito.when(userWishlistDao.findMyWishlistByCode(anonymous, wishlistCode)).thenReturn(new Wishlist2Model());
        Mockito.when(productService.getProductForCode(productCode)).thenReturn(product);
        Mockito.when(userWishlistDao.findWishlistEntryForProduct(Mockito.any(Wishlist2Model.class), Mockito.any(ProductModel.class))).thenReturn(new Wishlist2EntryModel());
        service.addProductToWishlist(wishlistCode, wishlistEntryData);
    }

    /**
     * test addProductToWishlist anonymous.
     */
    @Test
    public void testAddProductToWishlistAnonymous()
    {
        String wishlistCode = "6789";
        String productCode = "0000000005678";
        ProductData productData = new ProductData();
        productData.setCode(productCode);
        WishlistEntryData wishlistEntryData = new WishlistEntryData();
        wishlistEntryData.setProduct(productData);
        wishlistEntryData.setDesired(Integer.valueOf(1));
        wishlistEntryData.setPriority(Wishlist2EntryPriority.HIGHEST);
        wishlistEntryData.setComment("comment");
        Mockito.when(userService.getCurrentUser()).thenReturn(anonymous);
        Mockito.doAnswer(new Answer<Wishlist2Model>() {

            @Override
            public Wishlist2Model answer(InvocationOnMock invocation) throws Throwable
            {
                Wishlist2Model result = new Wishlist2Model();
                result.setEntries(new ArrayList<>());
                result.setUser(anonymous);
                return result;
            }
            
        }).when(userWishlistDao).findMyWishlistByCode(anonymous, wishlistCode);
        
        Mockito.when(productService.getProductForCode(productCode)).thenReturn(product);
        Mockito.when(userWishlistDao.findWishlistEntryForProduct(Mockito.any(Wishlist2Model.class), Mockito.any(ProductModel.class))).thenReturn(null);
        service.addProductToWishlist(wishlistCode, wishlistEntryData);
        Mockito.verify(modelService, Mockito.never()).save(Mockito.anyObject());
    }

    /**
     * test addProductToWishlist customer.
     */
    @Test
    public void testAddProductToWishlistCustomer()
    {
        String wishlistCode = "7890";
        String productCode = "0000000006789";
        ProductData productData = new ProductData();
        productData.setCode(productCode);
        WishlistEntryData wishlistEntryData = new WishlistEntryData();
        wishlistEntryData.setProduct(productData);
        wishlistEntryData.setDesired(Integer.valueOf(1));
        wishlistEntryData.setPriority(Wishlist2EntryPriority.HIGHEST);
        wishlistEntryData.setComment("comment2");
        Mockito.when(userService.getCurrentUser()).thenReturn(customer);
        Mockito.doAnswer(new Answer<Wishlist2Model>() {

            @Override
            public Wishlist2Model answer(InvocationOnMock invocation) throws Throwable
            {
                Wishlist2Model result = new Wishlist2Model();
                result.setEntries(new ArrayList<>());
                result.setUser(customer);
                return result;
            }
            
        }).when(userWishlistDao).findMyWishlistByCode(customer, wishlistCode);
        
        Mockito.when(productService.getProductForCode(productCode)).thenReturn(product);
        Mockito.when(userWishlistDao.findWishlistEntryForProduct(Mockito.any(Wishlist2Model.class), Mockito.any(ProductModel.class))).thenReturn(null);
        service.addProductToWishlist(wishlistCode, wishlistEntryData);
        Mockito.verify(modelService, Mockito.times(2)).save(Mockito.anyObject());
    }
    
    /**
     * test removeWishlistEntryForProduct invalid wishlist code.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testRemoveWishlistEntryForProductInvalidWishlist()
    {
        String wishlistCode = "4567";
        String productCode = "0000000001234";
        Mockito.when(userService.getCurrentUser()).thenReturn(anonymous);
        Mockito.when(userWishlistDao.findMyWishlistByCode(anonymous, wishlistCode)).thenReturn(null);
        Mockito.when(productService.getProductForCode(productCode)).thenThrow(new UnknownIdentifierException("error"));
        service.removeWishlistEntryForProduct(wishlistCode, productCode);
    }

    /**
     * test removeWishlistEntryForProduct invalid product.
     */
    @Test(expected = UnknownIdentifierException.class)
    public void testRemoveWishlistEntryForProductInvalidProduct()
    {
        String wishlistCode = "4567";
        String productCode = "0000000001234";
        Mockito.when(userService.getCurrentUser()).thenReturn(anonymous);
        Mockito.doAnswer(new Answer<Wishlist2Model>() {

            @Override
            public Wishlist2Model answer(InvocationOnMock invocation) throws Throwable
            {
                Wishlist2Model result = new Wishlist2Model();
                result.setEntries(new ArrayList<>());
                result.setUser(anonymous);
                return result;
            }
            
        }).when(userWishlistDao).findMyWishlistByCode(anonymous, wishlistCode);
        
        Mockito.when(productService.getProductForCode(productCode)).thenThrow(new UnknownIdentifierException("error"));
        service.removeWishlistEntryForProduct(wishlistCode, productCode);
    }

    /**
     * test removeWishlistEntryForProduct invalid entry.
     */
    @Test(expected = UnknownIdentifierException.class)
    public void testRemoveWishlistEntryForProductInvalidEntry()
    {
        String wishlistCode = "4567";
        String productCode = "0000000001234";
        Mockito.when(userService.getCurrentUser()).thenReturn(anonymous);
        Mockito.doAnswer(new Answer<Wishlist2Model>() {

            @Override
            public Wishlist2Model answer(InvocationOnMock invocation) throws Throwable
            {
                Wishlist2Model result = new Wishlist2Model();
                result.setEntries(new ArrayList<>());
                result.setUser(anonymous);
                return result;
            }
            
        }).when(userWishlistDao).findMyWishlistByCode(anonymous, wishlistCode);
        
        Mockito.when(productService.getProductForCode(productCode)).thenReturn(product);
        Mockito.when(wishlistDao.findWishlistEntryByProduct(Mockito.any(ProductModel.class), Mockito.any(Wishlist2Model.class))).thenReturn(Collections.emptyList());
        service.removeWishlistEntryForProduct(wishlistCode, productCode);
    }

    /**
     * test removeWishlistEntryForProduct anonymous.
     */
    @Test
    public void testRemoveWishlistEntryForProductAnonymous()
    {
        String wishlistCode = "2345";
        String productCode = "0000000002345";
        Mockito.when(userService.getCurrentUser()).thenReturn(anonymous);
        Mockito.doAnswer(new Answer<Wishlist2Model>() {

            @Override
            public Wishlist2Model answer(InvocationOnMock invocation) throws Throwable
            {
                Wishlist2Model result = new Wishlist2Model();
                result.setEntries(new ArrayList<>());
                result.setUser(anonymous);
                result.setEntries(Collections.singletonList(wishlistEntry));
                return result;
            }
            
        }).when(userWishlistDao).findMyWishlistByCode(anonymous, wishlistCode);
        
        Mockito.when(productService.getProductForCode(productCode)).thenReturn(product);
        Mockito.when(wishlistDao.findWishlistEntryByProduct(Mockito.any(ProductModel.class), Mockito.any(Wishlist2Model.class))).thenReturn(Collections.singletonList(wishlistEntry));
        service.removeWishlistEntryForProduct(wishlistCode, productCode);
        Mockito.verify(modelService, Mockito.never()).save(Mockito.anyObject());
    }

    /**
     * test removeWishlistEntryForProduct customer.
     */
    @Test
    public void testRemoveWishlistEntryForProductCustomer()
    {
        String wishlistCode = "2345";
        String productCode = "0000000002345";
        Mockito.when(userService.getCurrentUser()).thenReturn(customer);
        Mockito.doAnswer(new Answer<Wishlist2Model>() {

            @Override
            public Wishlist2Model answer(InvocationOnMock invocation) throws Throwable
            {
                Wishlist2Model result = new Wishlist2Model();
                result.setEntries(new ArrayList<>());
                result.setUser(customer);
                result.setEntries(Collections.singletonList(wishlistEntry));
                return result;
            }
            
        }).when(userWishlistDao).findMyWishlistByCode(customer, wishlistCode);
        
        Mockito.when(productService.getProductForCode(productCode)).thenReturn(product);
        Mockito.when(wishlistDao.findWishlistEntryByProduct(Mockito.any(ProductModel.class), Mockito.any(Wishlist2Model.class))).thenReturn(Collections.singletonList(wishlistEntry));
        service.removeWishlistEntryForProduct(wishlistCode, productCode);
        Mockito.verify(modelService, Mockito.times(1)).save(Mockito.anyObject());
    }
    
}
