package com.capgemini.wishlistservices.facades.impl;

import java.util.Collections;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.verification.VerificationMode;

import com.capgemini.wishlistservices.data.WishlistData;
import com.capgemini.wishlistservices.data.WishlistEntryData;
import com.capgemini.wishlistservices.services.WishlistService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

@UnitTest
public class DefaultWishlistFacadeTest
{

    /**
     * constant for wishlist code.
     */
    private static final String WISHLIST_CODE = "code";
    
    /**
     * the facade to test.
     */
    private DefaultWishlistFacade facade;
    
    /**
     * the wishlist converter.
     */
    @Mock
    private Converter<Wishlist2Model, WishlistData> wishlistConverter;
    
    /**
     * the wishlist service.
     */
    @Mock
    private WishlistService wishlistService;
    
    /**
     * the wishlist.
     */
    @Mock
    private Wishlist2Model wishlist;
    
    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        facade = new DefaultWishlistFacade();
        facade.setWishlistConverter(wishlistConverter);
        facade.setWishlistService(wishlistService);
        Mockito.when(wishlistConverter.convert(Mockito.any(Wishlist2Model.class))).thenReturn(new WishlistData());
        Mockito.when(wishlist.getCode()).thenReturn(WISHLIST_CODE);
        Mockito.when(wishlistService.getMyWishlistByCode(WISHLIST_CODE)).thenReturn(wishlist);
        Mockito.when(wishlistService.createWishlist(Mockito.anyString(), Mockito.anyString())).thenReturn(new Wishlist2Model());
        Mockito.doNothing().when(wishlistService).deleteWishlist(Mockito.anyString());
        Mockito.when(wishlistService.updateWishlist(Mockito.any(WishlistData.class))).thenReturn(new Wishlist2Model());
        Mockito.doNothing().when(wishlistService).addProductToWishlist(Mockito.anyString(), Mockito.any(WishlistEntryData.class));
        Mockito.doNothing().when(wishlistService).removeWishlistEntryForProduct(Mockito.anyString(), Mockito.anyString());
    }
    
    /**
     * test getMyWishlists.
     */
    @Test
    public void testGetMyWishlistsEmpty()
    {
        Mockito.when(wishlistService.getWishlists()).thenReturn(null);
        Assert.assertTrue(CollectionUtils.isEmpty(facade.getMyWishlists()));
    }

    /**
     * test getMyWishlists.
     */
    @Test
    public void testGetMyWishlists()
    {
        Mockito.when(wishlistService.getWishlists()).thenReturn(Collections.singletonList(wishlist));
        Assert.assertTrue(CollectionUtils.size(facade.getMyWishlists()) == 1);
    }
 
    /**
     * test getWishlistDetails.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetWishlistDetailsError()
    {
        facade.getWishlistDetails("test");
    }
    
    /**
     * test getWishlistDetails.
     */
    @Test
    public void testGetWishlistDetails()
    {
        Assert.assertTrue(facade.getWishlistDetails(WISHLIST_CODE) != null);
    }
    
    /**
     * test createWishlist.
     */
    @Test
    public void testCreateWishlist()
    {
        WishlistData data = new WishlistData();
        data.setCode("code");
        data.setName("name");
        Assert.assertTrue(facade.createWishlist(data) instanceof WishlistData);
    }
    
    /**
     * test deleteWishlist.
     */
    @Test
    public void testDeleteWishlist()
    {
        String code = "test";
        facade.deleteWishlist(code);
        Mockito.verify(wishlistService, Mockito.times(1)).deleteWishlist(code);
    }
    
    /**
     * test updateWishlistDetails.
     */
    @Test
    public void testUpdateWishlistDetails()
    {
        WishlistData data = new WishlistData();
        facade.updateWishlistDetails(data);
        Mockito.verify(wishlistService, Mockito.times(1)).updateWishlist(data);
    }
    
    /**
     * test addProductToWishlist.
     */
    @Test
    public void testAddProductToWishlist()
    {
        WishlistEntryData entry = new WishlistEntryData();
        entry.setProduct(new ProductData());
        String code = "code";
        facade.addProductToWishlist(code, entry);
        Mockito.verify(wishlistService, Mockito.times(1)).addProductToWishlist(code, entry);
    }
    
    @Test
    public void testRemoveProductFromWishlist()
    {
        String productCode = "productCode";
        String wishlistCode = "wishlistCode";
        facade.removeProductFromWishlist(wishlistCode, productCode);
        Mockito.verify(wishlistService, Mockito.times(1)).removeWishlistEntryForProduct(wishlistCode, productCode);
    }
}
