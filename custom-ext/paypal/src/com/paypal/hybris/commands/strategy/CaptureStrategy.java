/**
 *
 */
package com.paypal.hybris.commands.strategy;

import de.hybris.platform.payment.commands.request.CaptureRequest;


public interface CaptureStrategy
{
	public boolean allowCapture(CaptureRequest request);
}
