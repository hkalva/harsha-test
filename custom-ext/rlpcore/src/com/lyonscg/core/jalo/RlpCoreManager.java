/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.lyonscg.core.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.lyonscg.core.constants.RlpCoreConstants;
import com.lyonscg.core.setup.CoreSystemSetup;


/**
 * Do not use, please use {@link CoreSystemSetup} instead.
 * 
 */
public class RlpCoreManager extends GeneratedRlpCoreManager
{
	public static final RlpCoreManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (RlpCoreManager) em.getExtension(RlpCoreConstants.EXTENSIONNAME);
	}
}
