ACC.productcompare = {

    ajaxCallEvent: true,
    _autoload: [
        ["registerHandlebarsHelper", $('.page-login').length === 0],
        ["getProductCompare", $('.page-login').length === 0],
        ["clearOnLogout", $('.page-login').length === 0]
    ],

    /**
     * Checks to see if there is product compare data in local storage.  If there is none it will 
     * perform an AJAX call to get product compare data.
     */
    getProductCompare: function() {
        var productCompareData = [];
        if (localStorage.getItem('productCompareData') !== 'undefined') {
            productCompareData.product = JSON.parse(localStorage.getItem('productCompareData'));
            ACC.productcompare.compileTemplate(productCompareData);
        } else {
            $.ajax({
                method: 'GET',
                url: window.productCompareUrl,
                success: function(data) {
                    localStorage.setItem('productCompareData', JSON.stringify(data));
                    productCompareData.product = JSON.parse(localStorage.getItem('productCompareData'));
                    ACC.productcompare.compileTemplate(productCompareData);
                }
            });
        }
    },

    /**
     * Registers a handlebars helper for various comparisons used in product compare logic
     */
    registerHandlebarsHelper: function() {
        Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
            switch (operator) {
                case '==':
                    return (v1 == v2) ? options.fn(this) : options.inverse(this);
                case '===':
                    return (v1 === v2) ? options.fn(this) : options.inverse(this);
                case '!==':
                    return (v1 !== v2) ? options.fn(this) : options.inverse(this);
                case '<':
                    return (v1 < v2) ? options.fn(this) : options.inverse(this);
                case '<=':
                    return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                case '>':
                    return (v1 > v2) ? options.fn(this) : options.inverse(this);
                case '>=':
                    return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                case '&&':
                    return (v1 && v2) ? options.fn(this) : options.inverse(this);
                case '||':
                    return (v1 || v2) ? options.fn(this) : options.inverse(this);
                default:
                    return options.inverse(this);
            }
        });
    },

    /**
     * Compiles the handlebars template into the HTML for the product comparison tray and overlay.
     * 
     * @param {Object} productCompareData // Contains product comparison data
     */
    compileTemplate: function(productCompareData) {
        if (typeof productCompareData.product !== 'undefined' && productCompareData.product.length > 0) {
            var $productComparePlaceholder = $('.product-compare-placeholder');
            if ($productComparePlaceholder.length > 0) {
                var template = Handlebars.compile(document.getElementById('product-compare').innerHTML);
                $productComparePlaceholder.html(template(productCompareData));
                ACC.productcompare.bindToAddToCartForm();
                ACC.productcompare.showProductCompareTray();
            }
        }
    },

    /**
     * Binds the add to cart functionality for the product compare
     */
    bindToAddToCartForm: function () {
        var $addToCartForm = $('.product-compare-overlay__grid .add_to_cart_form');
        $addToCartForm.ajaxForm({
        	beforeSubmit: ACC.product.showRequest,
        	success: ACC.product.displayAddToCartPopup
         });    
        setTimeout(function(){
        	ACC.productcompare.ajaxCallEvent = true;
         }, 2000);
     },

    /**
     * Functionality for adding a product to product compare
     * 
     * @param {String} productCode 
     */
    addProductComare: function(productCode) {
        var productCompareData = [];
        $.ajax({
            method: 'POST',
            url: window.productCompareUrl + '/' + productCode,
            success: function(data) {
                localStorage.setItem('productCompareData', JSON.stringify(data));
                productCompareData.product = data;
                ACC.productcompare.compileTemplate(productCompareData);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                $('.product-compare-checkbox--' + prod).prop('checked', false);
                alert(window.productCompareLimitMsg);
            }
        });
    },

    /**
     * Functionality for removing a product from product compare
     * 
     * @param {String} productCode 
     */
    removeProductCompare: function(productCode) {
        $('.compared-product-' + productCode).remove();
        $.ajax({
            method: 'DELETE',
            url: window.productCompareUrl + '/' + productCode,
            success: function(data) {
                localStorage.setItem('productCompareData', JSON.stringify(data));
                $('.product-compare-checkbox--' + productCode).prop('checked', false);
                $('.product-compare__count').text($('.product-compare__count').text() - 1);
                if (data.length === 0) {
                    ACC.productcompare.clearProductCompare();
                }
            }
        });
    },

    /**
     * Functionality to remove all products from product compare
     */
    clearProductCompare: function() {
        $.ajax({
            method: 'PUT',
            url: window.productCompareUrl,
            success: function(data) {
                localStorage.setItem('productCompareData', JSON.stringify(data));
                $('.product-compare-overlay').remove();
                $('.product-compare-tray').remove();
                $('.product-compare-checkbox').prop('checked', false);
            }
        });
        
    },

    /**
     * Functionality to prevent compared products from previous user/login from showing up
     */
    clearOnLogout: function() {
    	$('.js-logout-link').click(function() {
    		ACC.productcompare.clearProductCompare();
    	});
    },
    
    /**
     *  Functionality to show the product comparison tray
     */
    showProductCompareTray: function() {
        $('.product-compare-overlay').hide();
        $('.product-compare-overlay__background').hide();
        $('.product-compare-tray').show();
    },

    /**
     * Functionality to show the product comparison overlay
     */
    showProductCompare: function() {
        $(window).scrollTop(0);
        $('.product-compare-tray').hide();
        $('.product-compare-overlay__background').show();
        $('.product-compare-overlay').show();
    },

    /**
     * Functionality to handle the toggling of the product comparison checkbox
     * 
     * @param {String} productCode 
     * @param {*} el 
     */
    compareToggle: function(productCode, el) {
        var $el = $(el);
        if ($el.is(':checked')) {
            ACC.productcompare.addProductComare(productCode);
        } else {
            ACC.productcompare.removeProductCompare(productCode);
        }
    }
};