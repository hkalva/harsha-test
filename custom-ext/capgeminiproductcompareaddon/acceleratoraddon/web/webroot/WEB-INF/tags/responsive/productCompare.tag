<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<spring:url value="/productcompare" var="productCompareUrl" htmlEscape="false"/>
<spring:url value="/cart/add" var="addToCartUrl" htmlEscape="false"/>

<script>
    var productCompareUrl = '${productCompareUrl}';
    var productCompareLimitMsg = '<spring:theme code="product.compare.limit.msg" />';
</script>
<script id="product-compare" type="text/x-handlebars-template">
    <div class="product-compare-tray">
        <div class="container-fluid">
            <div class="row hidden-sm hidden-xs">
                <h3 class="product-compare-tray__heading col-md-6"><spring:theme code="product.compare" /> ({{product.length}})</h3>
                <div class="product-compare-tray__buttons col-md-6">
                    <button class="product-compare-tray__clear" onclick="ACC.productcompare.clearProductCompare()"><spring:theme code="product.compare.clear.all" /></button>
                    <button class="product-compare-tray__compare" onclick="ACC.productcompare.showProductCompare()"><spring:theme code="product.compare" /></button>
                </div>
            </div>
            <div class="product-compare-grid">
                {{#each product}}
                    <div class="product-compare-tray__product compared-product-{{code}}">
                        <button class="product-compare-tray__product-remove" onclick="ACC.productcompare.removeProductCompare({{code}})">X</button>
                        <a href="{{url}}">
                            <img src="{{images.2.url}}" class="product-compare-tray__product-image" alt="{{images.2.alt}}">
                            <div class="product-compare-tray__product-title">
                                {{name}}
                            </div>
                        </a>
                    </div>
                {{/each}}
            </div>
            <div class="visible-sm visible-xs">
                <button class="product-compare-tray__compare product-compare-tray__compare--mobile" onclick="ACC.productcompare.showProductCompare()"><spring:theme code="product.compare" /></button>
            </div>
        </div>
    </div>

    <div class="product-compare-overlay__background"></div>
    <div class="product-compare-overlay">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="product-compare-overlay__heading"><spring:theme code="product.compare" /></h3>
                </div>
                <div class="col-md-6">
                    <button class="product-compare-overlay__close" onclick="ACC.productcompare.showProductCompareTray()"><spring:theme code="product.compare.close" /></button>
                </div>
            </div>
            <div class="product-compare-overlay__grid">
                {{#each product}}
                    <div class="product-compare-overlay__product compared-product-{{code}}">
                        <button class="product-compare-overlay__product-remove" onclick="ACC.productcompare.removeProductCompare({{code}})">X</button>
                        <a href="{{url}}">
                            <img src="{{images.1.url}}" alt="{{images.1.alt}}" class="product-compare-overlay__product-image">
                            <h3 class="product-compare-overlay__product-name">{{name}}</h3>
                        </a>
                        <dl class="product-compare-overlay__product-info">
                            <dt><spring:theme code="product.compare.product.code" />:</dt>
                            <dd>{{code}}</dd>
                        </dl>
                        <dl class="product-compare-overlay__product-info">
                            <dt><spring:theme code="product.compare.manufacturer" />:</dt>
                            <dd>{{manufacturer}}</dd>
                        </dl>
						{{#if classifications.length}}
							{{#each classifications.0.features}}
								{{#if featureValues.length}}
									<dl class="product-compare-overlay__product-info">
										<dt>{{name}}</dt>
										<dd>{{featureValues.0.value}}</dd>
									</dl>
								{{/if}}
							{{/each}}
						{{/if}}
                        <h4 class="product-compare-overlay__product-price">{{price.formattedValue}}</h4>
                        {{#ifCond stock.stockLevelStatus.code "===" 'outOfStock'}}
                            <button class="product-compare-overlay__add-to-cart product-compare-overlay__add-to-cart--out-of-stock" disabled><spring:theme code="product.compare.out.of.stock" /></button>
                        {{else}}
                            <form:form method="post" id="addToCartForm{{code}}" class="add_to_cart_form" action="${addToCartUrl}">
                                <input type="hidden" name="productCodePost" value="{{code}}"/>
                                <input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
                                <button class="product-compare-overlay__add-to-cart js-add-to-cart" type="submit"><spring:theme code="product.compare.add.to.cart"/></button>
                            </form:form>
                        {{/ifCond}}
                    </div>
                {{/each}}
            </div>
        </div>
    </div>

</script>

<div class="product-compare-placeholder"></div>