/**
 *
 */
package com.capgemini.productcompareaddon.interceptors.beforeview;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import com.capgemini.productcompare.facades.ProductCompareFacade;
import com.capgemini.productcompareaddon.constants.CapgeminiproductcompareaddonConstants;


/**
 * @author lyonscg
 *
 */
public class ProductCompareBeforeViewHandler implements BeforeViewHandler
{


	private ProductCompareFacade productCompareFacade;

	@Override
	public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView)
	{

		modelAndView.addObject(CapgeminiproductcompareaddonConstants.PRODUCT_COMPARE_LIST_SESSION_KEY,
				getProductCompareFacade().getSessionProductCompare());
	}


	/**
	 * @return the productCompareFacade
	 */
	public ProductCompareFacade getProductCompareFacade()
	{
		return productCompareFacade;
	}


	/**
	 * @param productCompareFacade
	 *           the productCompareFacade to set
	 */
	public void setProductCompareFacade(final ProductCompareFacade productCompareFacade)
	{
		this.productCompareFacade = productCompareFacade;
	}
}
