/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.capgemini.productcompareaddon.controllers.pages.productcompare;

import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.capgemini.productcompare.facades.ProductCompareFacade;
import com.capgemini.services.exception.ProductCompareException;


/**
 * @author lyonscg
 *
 */
@Controller
@RequestMapping(value = "/productcompare")
public class ProductCompareController
{
    /**
     * constant for product code path variable.
     */
    private static final String PRODUCT_CODE_PATH_VARIABLE_PATTERN = "/{productCode:.*}";

    @Resource(name = "productCompareFacade")
    private ProductCompareFacade productCompareFacade;

    @PostMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ProductData> addProductCompare(@PathVariable("productCode")
    final String productCode) throws ProductCompareException
    {
        return productCompareFacade.addProductCompare(productCode);
    }

    @DeleteMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ProductData> removeProductCompare(@PathVariable("productCode")
    final String productCode)
    {
        return productCompareFacade.removeProductCompare(productCode);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ProductData> clearProductCompare()
    {
        return productCompareFacade.clearProductCompare();
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ProductData> getProductCompare()
    {
        return productCompareFacade.getProductCompare();
    }
}
