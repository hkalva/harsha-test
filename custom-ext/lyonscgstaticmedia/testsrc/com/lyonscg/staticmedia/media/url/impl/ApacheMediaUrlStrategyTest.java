package com.lyonscg.staticmedia.media.url.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.media.MediaSource;
import de.hybris.platform.media.storage.impl.DefaultMediaFolderConfig;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Test class for {@link ApacheMediaUrlStrategy}.
 */
@UnitTest
public class ApacheMediaUrlStrategyTest
{
    @InjectMocks
    private ApacheMediaUrlStrategy apacheMediaUrlStrategy = new ApacheMediaUrlStrategy();

    @Mock
    MediaSource mediaSource;

    @Mock
    DefaultMediaFolderConfig mediaFolderConfig;

    /**
     * Method which initializes the class to be tested and
     * test data that is used for unit testing.
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Method returns the mediasource mock object based on the location passed.
     * @param location
     */
    private void mockMediaSourceWithLocation(String location) {
        mediaSource = new MediaSource() {
            @Override
            public Long getDataPk() {
                return null;
            }

            @Override
            public Long getMediaPk() {
                return null;
            }

            @Override
            public String getLocation() {
                return location;
            }

            @Override
            public String getLocationHash() {
                return null;
            }

            @Override
            public String getMime() {
                return null;
            }

            @Override
            public String getInternalUrl() {
                return null;
            }

            @Override
            public String getRealFileName() {
                return null;
            }

            @Override
            public String getFolderQualifier() {
                return null;
            }

            @Override
            public Long getSize() {
                return null;
            }

            @Override
            public Object getSource() {
                return null;
            }
        };
    }

    /**
     * Test getUrlForMedia returns modified url location based on the location
     * updated in the media source.
     */
    @Test
    public void testGetUrlForMedia()
    {
        apacheMediaUrlStrategy.setMediaHostName("mediahost");
        mockMediaSourceWithLocation("image1");
        String urlForMedia = apacheMediaUrlStrategy.getUrlForMedia(mediaFolderConfig, mediaSource);
        assertThat(urlForMedia).isNotNull();
    }

    /**
     * Test getUrlForMedia returns modified url if media host name is null.
     *
     */
    @Test
    public void testGetUrlForMediaWithNoHost()
    {
        mockMediaSourceWithLocation("testImage1");
        String urlForMedia = apacheMediaUrlStrategy.getUrlForMedia(mediaFolderConfig, mediaSource);
        assertThat(urlForMedia).isNotNull();
    }

    /**
     * Test getUrlForMedia returns null if media Source location is null.
     *
     */
    @Test
    public void testGetUrlForMediaWithNoMediaSource()
    {
        apacheMediaUrlStrategy.setMediaHostName("host");
        String urlForMedia = apacheMediaUrlStrategy.getUrlForMedia(mediaFolderConfig, mediaSource);
        assertThat(urlForMedia).isNull();
    }

    /**
     * Test getUrlForMedia returns modified url location based on the location
     * updated in the media source though mediaFolderConfig is not present.
     */
    @Test
    public void testGetUrlForMediaWithNoDefaultMediaFolderConfig()
    {
        apacheMediaUrlStrategy.setMediaHostName("host");
        mockMediaSourceWithLocation("testImage1");
        String urlForMedia = apacheMediaUrlStrategy.getUrlForMedia(null, mediaSource);
        assertThat(urlForMedia).isNotNull();
    }

}
