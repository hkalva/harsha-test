/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.lyonscg.staticmedia.media.url.impl;

import com.lyonscg.constants.LyonscgstaticmediaConstants;
import de.hybris.platform.media.MediaSource;
import de.hybris.platform.media.storage.MediaStorageConfigService.MediaFolderConfig;
import de.hybris.platform.media.url.MediaURLStrategy;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

/**
 * Overriding MediaUrl Strategy which helps serve media from web server.
 */
public class ApacheMediaUrlStrategy implements MediaURLStrategy
{
	/**
	 * Logger definition for {@link ApacheMediaUrlStrategy}
	 */
	private static final Logger LOG = Logger.getLogger(ApacheMediaUrlStrategy.class);

	private String mediaHostName;

	/**
	 * @return the mediaHostName
	 */
	public String getMediaHostName()
	{
		return mediaHostName;
	}

	/**
	 * @param mediaHostName
	 *           the mediaHostName to set
	 */
	@Required
	public void setMediaHostName(final String mediaHostName)
	{
		this.mediaHostName = mediaHostName;
	}

	/**
	 * Returns modified media url by appending the host where the media is served from. In this case
	 * media is always served from the web server.
	 *
	 * @param paramMediaFolderConfig
	 * @param paramMediaSource
	 * @return modified media url location by appending the webserver host name.
	 */
	@Override
	public String getUrlForMedia(final MediaFolderConfig paramMediaFolderConfig, final MediaSource paramMediaSource)
	{
		String url = null;
		try
		{
			final String mediaUrlLocation = paramMediaSource.getLocation();
			if(StringUtils.isNotEmpty(mediaUrlLocation)){
				url = new StringBuilder(getMediaHostName()).append(LyonscgstaticmediaConstants.FORWARD_SLASH).append(mediaUrlLocation).toString();
			}
		}catch(Exception ex){
			LOG.error("Exception modifying media url to be served from host " + mediaHostName + " for Media source " + paramMediaSource);
		}
		return url;
	}
}
