/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.lyonscg.setup;

import static com.lyonscg.constants.LyonscgstaticmediaConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.lyonscg.constants.LyonscgstaticmediaConstants;
import com.lyonscg.service.LyonscgstaticmediaService;


@SystemSetup(extension = LyonscgstaticmediaConstants.EXTENSIONNAME)
public class LyonscgstaticmediaSystemSetup
{
	private final LyonscgstaticmediaService lyonscgstaticmediaService;

	public LyonscgstaticmediaSystemSetup(final LyonscgstaticmediaService lyonscgstaticmediaService)
	{
		this.lyonscgstaticmediaService = lyonscgstaticmediaService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		lyonscgstaticmediaService.createLogo(PLATFORM_LOGO_CODE);
	}

	/*private InputStream getImageStream()
	{
		return LyonscgstaticmediaSystemSetup.class.getResourceAsStream("/lyonscgstaticmedia/sap-hybris-platform.png");
	}*/
}
