package com.capgemini.constants;

/**
*
* @author lyonscg
*
*/
public class CapgeminiperformanceaddonConstants extends GeneratedCapgeminiperformanceaddonConstants
{
    /**
     * constant for extension name.
     */
	public static final String EXTENSIONNAME = "capgeminiperformanceaddon";
	
	 /**
     * default private constructor. 
     */
	private CapgeminiperformanceaddonConstants()
	{
		//empty
	}
}
