/**
 *
 */
package com.capgemini.performance.cache.service.impl;

import de.hybris.platform.cms2.misc.CMSFilter;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.cache.ehcache.EhCacheCacheManager;

import com.capgemini.performance.cache.service.CacheService;


/**
 * @author
 *
 */
public class CacheServiceImpl implements CacheService
{
    /**
     * constant for Cache key separator.
     */
    private static final String KEY_SEPARATOR = "_";
    /**
     * the logger. 
     */
    private static final Logger LOG = LoggerFactory.getLogger(CacheServiceImpl.class);

    /**
     * the cache manager.
     */
    private EhCacheCacheManager cacheManager;
    /**
     * the cms site service.
     */
    private CMSSiteService cmsSiteService;
    /**
     * the i18 service.
     */
    private CommonI18NService commonI18NService;
    /**
     * the user service.
     */
    private UserService userService;


    @Override
    public EhCacheCacheManager getCacheManager()
    {
        return cacheManager;
    }

    @Override
    public String generateKey(final String id, final boolean userSpecific, final boolean siteSpecific,
            final boolean languageSpecific, final boolean currencySpecific)
    {
        final StringBuilder sb = new StringBuilder(id);

        //add user uid to the key if required
        addUserToKey(userSpecific, sb);
        //add site uid to the key if required
        addSiteToKey(siteSpecific, sb);
        //add language iso to the key if required
        addLanguageToKey(languageSpecific, sb);
        //add currency iso to the key if required
        addCurrencyToKey(currencySpecific, sb);

        final String key = sb.toString();
        LOG.debug("Cache key: {}", key);
        return key;
    }

    private void addCurrencyToKey(final boolean currencySpecific, final StringBuilder sb) {
        if (currencySpecific)
        {
            final String currentCurrency = null != commonI18NService.getCurrentCurrency() ? commonI18NService.getCurrentCurrency().getIsocode() : StringUtils.EMPTY;
            if (StringUtils.isNotEmpty(currentCurrency))
            {
                sb.append(CacheServiceImpl.KEY_SEPARATOR);
                sb.append(currentCurrency);
            } else {
                LOG.error("Current currency is empty.");
            }
        }
    }

    private void addLanguageToKey(final boolean languageSpecific, final StringBuilder sb) {
        if (languageSpecific)
        {
            final String currentLanguage = null != commonI18NService.getCurrentLanguage() ? commonI18NService.getCurrentLanguage().getIsocode() : StringUtils.EMPTY;
            if (StringUtils.isNotEmpty(currentLanguage))
            {
                sb.append(CacheServiceImpl.KEY_SEPARATOR);
                sb.append(currentLanguage);
            } else {
                LOG.error("Current language is empty.");
            }
        }
    }

    private void addSiteToKey(final boolean siteSpecific, final StringBuilder sb) {
        if (siteSpecific)
        {
            final String currentSite = null != cmsSiteService.getCurrentSite() ? cmsSiteService.getCurrentSite().getUid() : StringUtils.EMPTY;
            if (StringUtils.isNotEmpty(currentSite))
            {
                sb.append(CacheServiceImpl.KEY_SEPARATOR);
                sb.append(currentSite);
            } else {
                LOG.error("Current site is empty.");
            }
        }
    }

    private void addUserToKey(final boolean userSpecific, final StringBuilder sb) {
        if (userSpecific)
        {
            final String currentUser = null != userService.getCurrentUser() ? userService.getCurrentUser().getUid() : StringUtils.EMPTY;
            if (StringUtils.isNotEmpty(currentUser))
            {
                sb.append(CacheServiceImpl.KEY_SEPARATOR);
                sb.append(currentUser);
            } else {
                LOG.error("Current user is empty.");
            }
        }
    }

    @Override
    public boolean isPreviewRequest(final PageContext pageContext)
    {
        return StringUtils.isNotEmpty(pageContext.getRequest().getParameter(CMSFilter.PREVIEW_TICKET_ID_PARAM));
    }

    @Required
    public void setCacheManager(final EhCacheCacheManager cacheManager)
    {
        this.cacheManager = cacheManager;
    }

    @Required
    public void setCmsSiteService(final CMSSiteService cmsSiteService)
    {
        this.cmsSiteService = cmsSiteService;
    }

    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService)
    {
        this.commonI18NService = commonI18NService;
    }

    @Required
    public void setUserService(final UserService userService)
    {
        this.userService = userService;
    }
}
