/**
 *
 */
package com.capgemini.performance.cache.service;

import javax.servlet.jsp.PageContext;

import org.springframework.cache.ehcache.EhCacheCacheManager;


/**
 * Interface that provides all the functionality required for the lyonscg performanceaddon cache implementation.
 *
 * @author
 *
 */
public interface CacheService
{

    /**
     * Retrieves the cache manager.
     *
     * @return The CacheManager.
     */
    EhCacheCacheManager getCacheManager();

    /**
     * Generates the key for the cache entry. The key will be formatted in the order id, currentUser, currentSite,
     * currentLanguage, currentCurrency based on the values of userSpecific, siteSpecific, languageSpecific and currency
     * specific.
     *
     * @param id - the id.
     * @param userSpecific - the userSpecific.
     * @param siteSpecific - the siteSpecific.
     * @param languageSpecific - the languageSpecific.
     * @param currencySpecific - the currencySpecific.
     * @return The cache key to be used.
     */
    String generateKey(String id, boolean userSpecific, boolean siteSpecific, boolean languageSpecific, boolean currencySpecific);

    /**
     * Checks if the request is for a page preview or not.
     *
     * @param pageContext - the pageContext.
     * @return TRUE if the request is for a page preview and FALSE in other case.
     */
    boolean isPreviewRequest(PageContext pageContext);
}
