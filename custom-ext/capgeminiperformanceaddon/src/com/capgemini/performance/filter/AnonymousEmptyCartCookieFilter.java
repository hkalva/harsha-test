/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.capgemini.performance.filter;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;

import com.capgemini.performance.cookie.generator.EnhancedCookieGenerator;
import com.capgemini.performance.cookie.service.CookieService;


/**
 * Adds an empty cookie with name 'anonymousEmptyCartCookie' when customer is anonymous and cart is empty and removes it
 * if customer is not anonymous or the cart has any item. The cookie is a session cookie that will last during the whole
 * life of the session.
 *
 * @author
 */
public class AnonymousEmptyCartCookieFilter extends OncePerRequestFilter
{

	private static final Logger LOG = LoggerFactory.getLogger(AnonymousEmptyCartCookieFilter.class);
	private static final String FILTER_ENABLED = "capgeminiperformanceaddon.anonymousemptycartcookie.enabled";
	private CookieService cookieService;
	private EnhancedCookieGenerator cookieGenerator;
	private ConfigurationService configurationService;

	@Override
	protected void doFilterInternal(final HttpServletRequest req, final HttpServletResponse res, final FilterChain filterChain)
			throws ServletException, IOException
	{
		// if the cookie filter is enabled manage the cookie
		if (configurationService.getConfiguration().getBoolean(AnonymousEmptyCartCookieFilter.FILTER_ENABLED, false))
		{
			if (this.cookieService.anonymousWithEmptyCart())
			{
				// generate the session cookie
				LOG.debug("Adding anonymousEmptyCartCookie cookie");
				cookieGenerator.setCookieMaxAge(Integer.valueOf(-1));
				cookieGenerator.addCookie(res, StringUtils.EMPTY);
			}
			else
			{
				// remove the session cookie
				LOG.debug("Removing anonymousEmptyCartCookie");
				cookieGenerator.setCookieMaxAge(Integer.valueOf(0));
				cookieGenerator.addCookie(res, StringUtils.EMPTY);
			}
		}
		filterChain.doFilter(req, res);
	}

	@Required
	public void setCookieService(final CookieService cookieService)
	{
		this.cookieService = cookieService;
	}

	@Required
	public void setCookieGenerator(final EnhancedCookieGenerator cookieGenerator)
	{
		this.cookieGenerator = cookieGenerator;
	}
	
    @Required
    public void setConfigurationService(final ConfigurationService configurationService)
    {
        this.configurationService = configurationService;
    }
	
}
