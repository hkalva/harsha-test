/**
 *
 */
package com.capgemini.performance.storefront.tags.cache;

import de.hybris.platform.core.Registry;
import de.hybris.platform.util.Config;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.performance.cache.service.CacheService;


/**
 * Tag handler to manage the caching of front end tags. This tag caches the body
 *
 * @author
 *
 */
public class CacheTag extends BodyTagSupport
{
	private static final Logger LOG = LoggerFactory.getLogger(CacheTag.class);
	private static final String CACHE_ENABLED = "capgeminiperformanceaddon.webfragmentscache.enabled";
	private static final String CACHE_NAME = "webFragmentsCache";
	private static final String CACHE_SERVICE_BEAN = "cacheService";

	private final CacheService cacheService = (CacheService) Registry.getApplicationContext().getBean(CacheTag.CACHE_SERVICE_BEAN);
	private final Ehcache cache = (Ehcache) cacheService.getCacheManager().getCache(CacheTag.CACHE_NAME).getNativeCache();
	private String key;

	//tag attributes
	private String body;
	private String id;
	private int timeout = -1;
	private boolean userSpecific = false;
	private boolean siteSpecific = false;
	private boolean languageSpecific = false;
	private boolean currencySpecific = false;

	@Override
	public int doStartTag() throws JspException
	{
		this.key = cacheService.generateKey(this.id, this.userSpecific, this.siteSpecific, this.languageSpecific,
				this.currencySpecific);
		return EVAL_BODY_BUFFERED;
	}

	@Override
	public int doAfterBody() throws JspException
	{
		//save the evaluation of the tag in the cache
		this.body = getBodyContent().getString();
		if (this.cacheEnabled() && StringUtils.isNotEmpty(body))
		{
			final Element element = new Element(this.key, body);

			if (timeout > 0)
			{
				element.setTimeToLive(this.timeout);
			}
			if (cache.get(element) != null)
			{
				LOG.debug("Value for key {} evaluated and added to cache", this.key);
				cache.put(element);
			}
		}

		return super.doAfterBody();
	}

	@Override
	public int doEndTag() throws JspException
	{
		//
		//print the content if there is something to print
		if (StringUtils.isNotEmpty(this.body))
		{
			final JspWriter jspWriter = this.pageContext.getOut();
			try
			{
				jspWriter.write(this.body);
			}
			catch (final IOException e)
			{
				LOG.error("Error trying to print the cached body.", e);
			}
		}
		else
		{
			LOG.error("Nothing to render.");
		}
		return super.doEndTag();
	}
	/**
	 * Checks if the items should be cached or not based on cache enable and not preview mode.
	 *
	 * @return
	 */
	private boolean cacheEnabled()
	{
		return Config.getBoolean(CacheTag.CACHE_ENABLED, true) && !cacheService.isPreviewRequest(this.pageContext);
	}

	@Override
	public void setId(final String id)
	{
		this.id = id;
	}

	public void setUserSpecific(final boolean userSpecific)
	{
		this.userSpecific = userSpecific;
	}

	public void setSiteSpecific(final boolean siteSpecific)
	{
		this.siteSpecific = siteSpecific;
	}

	public void setLanguageSpecific(final boolean languageSpecific)
	{
		this.languageSpecific = languageSpecific;
	}

	public void setCurrencySpecific(final boolean currencySpecific)
	{
		this.currencySpecific = currencySpecific;
	}

	public void setTimeout(final int timeout)
	{
		this.timeout = timeout;
	}
}
