/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.capgemini.performance.cookie.service.impl;

import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.capgemini.performance.cookie.service.CookieService;


/**
 * @see CookieService
 * @author
 */
public class CookieServiceImpl implements CookieService
{
    /**
     * the user service.
     */
	private UserService userService;
	/**
     * the cart service.
     */
    private CartService cartService;
    /**
     * check anonymous user with cart.
     */
	@Override
	public boolean anonymousWithEmptyCart()
	{
		boolean isAnonymousEmpty = false;

		final String currentUser = null != getUserService().getCurrentUser() ? getUserService().getCurrentUser().getUid() : StringUtils.EMPTY;
		if (currentUser.equals(getUserService().getAnonymousUser().getUid()))
		{
			if (getCartService().hasSessionCart())
			{
				final int size = null != getCartService().getSessionCart().getEntries() ? getCartService().getSessionCart().getEntries().size()
						: 0;
				if (size > 0)
				{
					isAnonymousEmpty = false;
				}
				else
				{
					isAnonymousEmpty = true;
				}
			}
			else
			{
				//not even session in cart
				isAnonymousEmpty = true;
			}
		}
		return isAnonymousEmpty;
	}
	/**
	 * 
	 * @return userService
	 */
	public UserService getUserService()
    {
        return userService;
    }
/**
 * 
 * @param userService
 */
	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
	/**
	 * 
	 * @return cartService
	 */
	public CartService getCartService()
	{
	    return cartService;
	}
	/**
	 * 
	 * @param cartService
	 */
	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}
}
