/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.capgemini.performance.cookie.service;

/**
 * Service to provide required methods for creating a cookie for anonymous users without any entry in the basket.
 *
 * @author
 *
 */
public interface CookieService {

    /**
     * Checks if the current user is an anonymous user with an empty cart.
     * @return TRUE if the current user is anonymous with an empty cart, FALSE in other case.
     */
    boolean anonymousWithEmptyCart();
}
