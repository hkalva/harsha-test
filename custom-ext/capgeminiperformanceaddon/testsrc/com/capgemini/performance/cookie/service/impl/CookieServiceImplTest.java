package com.capgemini.performance.cookie.service.impl;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;

@UnitTest
public class CookieServiceImplTest
{

    /**
     * the service to test.
     */
    private CookieServiceImpl service;
    
    /**
     * the user service.
     */
    @Mock
    private UserService userService;
    
    /**
     * the cart service.
     */
    @Mock
    private CartService cartService;
    
    /**
     * the anonymous user.
     */
    @Mock
    private CustomerModel anonymousUser;
    
    /**
     * the cart.
     */
    @Mock
    private CartModel cart;
    
    /**
     * the cart entry.
     */
    @Mock
    private CartEntryModel entry;
    
    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        service = new CookieServiceImpl();
        service.setCartService(cartService);
        service.setUserService(userService);
        Mockito.when(anonymousUser.getUid()).thenReturn("anonymous");
        Mockito.when(userService.getAnonymousUser()).thenReturn(anonymousUser);
    }
    
    /**
     * test anonymousWithEmptyCart with null user.
     */
    @Test
    public void anonymousWithEmptyCartNullUser()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(null);
        Assert.assertFalse(service.anonymousWithEmptyCart());
    }
    
    /**
     * test anonymousWithEmptyCart without cart.
     */
    @Test
    public void anonymousWithEmptyCartNoCart()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(anonymousUser);
        Mockito.when(cartService.hasSessionCart()).thenReturn(Boolean.FALSE);
        Assert.assertTrue(service.anonymousWithEmptyCart());
    }

    /**
     * test anonymousWithEmptyCart with empty cart.
     */
    @Test
    public void anonymousWithEmptyCartEmptyCart()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(anonymousUser);
        Mockito.when(cartService.hasSessionCart()).thenReturn(Boolean.TRUE);
        Mockito.when(cartService.getSessionCart()).thenReturn(cart);
        Mockito.when(cart.getEntries()).thenReturn(null);
        Assert.assertTrue(service.anonymousWithEmptyCart());
    }

    /**
     * test anonymousWithEmptyCart with cart.
     */
    @Test
    public void anonymousWithEmptyCart()
    {
        Mockito.when(userService.getCurrentUser()).thenReturn(anonymousUser);
        Mockito.when(cartService.hasSessionCart()).thenReturn(Boolean.TRUE);
        Mockito.when(cartService.getSessionCart()).thenReturn(cart);
        Mockito.when(cart.getEntries()).thenReturn(Collections.singletonList(entry));
        Assert.assertFalse(service.anonymousWithEmptyCart());
    }
    
}
