package com.capgemini.performance.cookie;

import javax.servlet.http.Cookie;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
/**
 * 
 * @author lyonscg
 *
 */
@UnitTest
public class ServerCookieTest
{
    private static final String COOKIE_NAME = "cookie1";
    private static final String COOKIE_PATH = "\"\"";
    private static final String COOKIE_VALUE = "value1";
    private static final String COOKIE_COMMENT = "comment1";
    private static final String COOKIE_DOMAIN = "domain1";
    private static final int COOKIE_VERSION = 1;
    
    @Mock
    private Cookie cookie;
    

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        Mockito.when(cookie.getName()).thenReturn(COOKIE_NAME);
        Mockito.when(cookie.getPath()).thenReturn(COOKIE_PATH);
        Mockito.when(cookie.getValue()).thenReturn(COOKIE_VALUE);
        Mockito.when(cookie.getComment()).thenReturn(COOKIE_COMMENT);
        Mockito.when(cookie.getDomain()).thenReturn(COOKIE_DOMAIN);
        Mockito.when(cookie.getVersion()).thenReturn(COOKIE_VERSION);
    }

    @Test
    public void getNewVersionFromTokenInValueOrComment()
    {
        int result = ServerCookie.getNewVersionFromTokenInValueOrComment(COOKIE_VALUE, COOKIE_COMMENT, 0);
        Assert.assertTrue(result == 1);
        result = ServerCookie.getNewVersionFromTokenInValueOrComment(COOKIE_VALUE, COOKIE_COMMENT, 1);
        Assert.assertTrue(result == 1);
        result = ServerCookie.getNewVersionFromTokenInValueOrComment(COOKIE_VALUE, COOKIE_COMMENT, 2);
        Assert.assertTrue(result == 2);
    }
    
    @Test
    public void getNewVersionFromTokenInPathOrDomain()
    {
        int result = ServerCookie.getNewVersionFromTokenInPathOrDomain(COOKIE_PATH, COOKIE_DOMAIN, 0);
        Assert.assertTrue(result == 0);
        result = ServerCookie.getNewVersionFromTokenInPathOrDomain(COOKIE_PATH, COOKIE_DOMAIN, 1);
        Assert.assertTrue(result == 1);
        result = ServerCookie.getNewVersionFromTokenInPathOrDomain(COOKIE_PATH, COOKIE_DOMAIN, 2);
        Assert.assertTrue(result == 2);
    }
    
    @Test
    public void getNewVersionBeforeCookieHeader()
    {
        int result = ServerCookie.getNewVersionBeforeCookieHeader(COOKIE_VALUE, COOKIE_DOMAIN, COOKIE_PATH, COOKIE_COMMENT, 0);
        Assert.assertTrue(result == 1);
        result = ServerCookie.getNewVersionBeforeCookieHeader(COOKIE_VALUE, COOKIE_DOMAIN, COOKIE_PATH, COOKIE_COMMENT, 1);
        Assert.assertTrue(result == 1);
        result = ServerCookie.getNewVersionBeforeCookieHeader(COOKIE_VALUE, COOKIE_DOMAIN, COOKIE_PATH, COOKIE_COMMENT, 2);
        Assert.assertTrue(result == 2);
    }
    
    @Test
    public void addMaxAgeInfo()
    {
        StringBuffer result = new StringBuffer();
        ServerCookie.addMaxAgeInfo(-1, result, 0);
        Assert.assertTrue(result.length() == 0);
        result.setLength(0);
        ServerCookie.addMaxAgeInfo(0, result, 0);
        Assert.assertTrue(StringUtils.contains(result.toString(), "; Expires="));
        result.setLength(0);
        ServerCookie.addMaxAgeInfo(0, result, 1);
        Assert.assertTrue(StringUtils.contains(result.toString(), "; Max-Age="));
    }
    
    @Test
    public void addVersionAndDomainInfo()
    {
        StringBuffer result = new StringBuffer();
        ServerCookie.addVersionAndDomainInfo(COOKIE_VALUE, null, null, result, 0);
        Assert.assertFalse(StringUtils.contains(result.toString(), "; Version=1"));
        Assert.assertFalse(StringUtils.contains(result.toString(), "; Comment="));
        Assert.assertFalse(StringUtils.contains(result.toString(), "; Domain="));
        result.setLength(0);
        ServerCookie.addVersionAndDomainInfo(COOKIE_VALUE, null, null, result, 1);
        Assert.assertTrue(StringUtils.contains(result.toString(), "; Version=1"));
        Assert.assertFalse(StringUtils.contains(result.toString(), "; Comment="));
        Assert.assertFalse(StringUtils.contains(result.toString(), "; Domain="));
        result.setLength(0);
        ServerCookie.addVersionAndDomainInfo(COOKIE_VALUE, null, COOKIE_COMMENT, result, 1);
        Assert.assertTrue(StringUtils.contains(result.toString(), "; Version=1"));
        Assert.assertTrue(StringUtils.contains(result.toString(), "; Comment="));
        Assert.assertFalse(StringUtils.contains(result.toString(), "; Domain="));
        result.setLength(0);
        ServerCookie.addVersionAndDomainInfo(COOKIE_VALUE, COOKIE_DOMAIN, COOKIE_COMMENT, result, 1);
        Assert.assertTrue(StringUtils.contains(result.toString(), "; Version=1"));
        Assert.assertTrue(StringUtils.contains(result.toString(), "; Comment="));
        Assert.assertTrue(StringUtils.contains(result.toString(), "; Domain="));
    }
    
    @Test
    public void appendCookieValue()
    {
        StringBuilder result = new StringBuilder();
        ServerCookie.appendCookieValue(result, cookie, false);
        Assert.assertTrue(StringUtils.contains(result.toString(), "; Path="));
        Assert.assertFalse(StringUtils.contains(result.toString(), "; Secure"));
        Assert.assertFalse(StringUtils.contains(result.toString(), "; HttpOnly"));
        result.setLength(0);
        Mockito.when(cookie.getSecure()).thenReturn(Boolean.TRUE);
        ServerCookie.appendCookieValue(result, cookie, false);
        Assert.assertTrue(StringUtils.contains(result.toString(), "; Path="));
        Assert.assertTrue(StringUtils.contains(result.toString(), "; Secure"));
        Assert.assertFalse(StringUtils.contains(result.toString(), "; HttpOnly"));
        result.setLength(0);
        Mockito.when(cookie.getSecure()).thenReturn(Boolean.TRUE);
        ServerCookie.appendCookieValue(result, cookie, true);
        Assert.assertTrue(StringUtils.contains(result.toString(), "; Path="));
        Assert.assertTrue(StringUtils.contains(result.toString(), "; Secure"));
        Assert.assertTrue(StringUtils.contains(result.toString(), "; HttpOnly"));
    }
    
}

