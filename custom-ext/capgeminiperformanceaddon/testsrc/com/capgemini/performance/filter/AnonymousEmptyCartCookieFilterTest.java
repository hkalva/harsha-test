package com.capgemini.performance.filter;

import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.performance.cookie.generator.EnhancedCookieGenerator;
import com.capgemini.performance.cookie.service.CookieService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;

@UnitTest
public class AnonymousEmptyCartCookieFilterTest
{

    /**
     * constant for property.
     */
    private static final String FILTER_ENABLED = "capgeminiperformanceaddon.anonymousemptycartcookie.enabled";
    
    /**
     * the filter to test.
     */
    private AnonymousEmptyCartCookieFilter filter;
    
    /**
     * the cookie service.
     */
    @Mock
    private CookieService cookieService;
    
    /**
     * the cookie generator.
     */
    @Mock
    private EnhancedCookieGenerator cookieGenerator;
    
    /**
     * the configuration service.
     */
    @Mock
    private ConfigurationService configurationService;
    
    /**
     * the configuration.
     */
    @Mock
    private Configuration configuration;
    
    /**
     * the request.
     */
    @Mock
    private HttpServletRequest request;
    
    /**
     * the response.
     */
    @Mock
    private HttpServletResponse response;
    
    /**
     * the filter chain.
     */
    @Mock
    private FilterChain filterChain;
    
    /**
     * set up data.
     */
    @Before
    public void setup() throws Exception
    {
        MockitoAnnotations.initMocks(this);
        filter = new AnonymousEmptyCartCookieFilter();
        filter.setConfigurationService(configurationService);
        filter.setCookieGenerator(cookieGenerator);
        filter.setCookieService(cookieService);
        Mockito.when(configurationService.getConfiguration()).thenReturn(configuration);
        Mockito.doNothing().when(cookieGenerator).setCookieMaxAge(Mockito.any(Integer.class));
        Mockito.doNothing().when(cookieGenerator).addCookie(Mockito.any(HttpServletResponse.class), Mockito.anyString());
        Mockito.doNothing().when(filterChain).doFilter(Mockito.any(ServletRequest.class), Mockito.any(ServletResponse.class));
    }
    
    /**
     * test doFilterInternal disabled.
     */
    @Test
    public void testDoFilterInternalDisabled() throws Exception
    {
        Mockito.when(configuration.getBoolean(FILTER_ENABLED, false)).thenReturn(Boolean.FALSE);
        filter.doFilterInternal(request, response, filterChain);
        Mockito.verify(cookieService, Mockito.never()).anonymousWithEmptyCart();
        Mockito.verify(cookieGenerator, Mockito.never()).setCookieMaxAge(Mockito.any(Integer.class));
        Mockito.verify(cookieGenerator, Mockito.never()).addCookie(Mockito.any(HttpServletResponse.class), Mockito.anyString());
        Mockito.verify(filterChain, Mockito.times(1)).doFilter(Mockito.any(ServletRequest.class), Mockito.any(ServletResponse.class));
    }

    /**
     * test doFilterInternal enabled with anonymous empty cart.
     */
    @Test
    public void testDoFilterInternalEnabledAnonymousEmptyCart() throws Exception
    {
        Mockito.when(configuration.getBoolean(FILTER_ENABLED, false)).thenReturn(Boolean.TRUE);
        Mockito.when(cookieService.anonymousWithEmptyCart()).thenReturn(Boolean.TRUE);
        filter.doFilterInternal(request, response, filterChain);
        Mockito.verify(cookieService, Mockito.times(1)).anonymousWithEmptyCart();
        Mockito.verify(cookieGenerator, Mockito.times(1)).setCookieMaxAge(Integer.valueOf(-1));
        Mockito.verify(cookieGenerator, Mockito.times(1)).addCookie(Mockito.any(HttpServletResponse.class), Mockito.anyString());
        Mockito.verify(filterChain, Mockito.times(1)).doFilter(Mockito.any(ServletRequest.class), Mockito.any(ServletResponse.class));
    }

    /**
     * test doFilterInternal enabled without anonymous empty cart.
     */
    @Test
    public void testDoFilterInternalEnabledNonAnonymousEmptyCart() throws Exception
    {
        Mockito.when(configuration.getBoolean(FILTER_ENABLED, false)).thenReturn(Boolean.TRUE);
        Mockito.when(cookieService.anonymousWithEmptyCart()).thenReturn(Boolean.FALSE);
        filter.doFilterInternal(request, response, filterChain);
        Mockito.verify(cookieService, Mockito.times(1)).anonymousWithEmptyCart();
        Mockito.verify(cookieGenerator, Mockito.times(1)).setCookieMaxAge(Integer.valueOf(0));
        Mockito.verify(cookieGenerator, Mockito.times(1)).addCookie(Mockito.any(HttpServletResponse.class), Mockito.anyString());
        Mockito.verify(filterChain, Mockito.times(1)).doFilter(Mockito.any(ServletRequest.class), Mockito.any(ServletResponse.class));
    }
    
}
