/*
	Default exception handler for server errors.
*/

package com.lyonscg.b2bacceleratoraddon.storefront.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import org.apache.log4j.Logger;
/**@author lyonscg.
 *Global default exception handler.
 */
@ControllerAdvice
class GlobalDefaultExceptionHandler {
	private static final Logger LOG = Logger.getLogger(GlobalDefaultExceptionHandler.class);

	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	public String handleAllExceptions(final Exception exception)
	{
		LOG.error("ERROR: ", exception);
		
		return "forward:/error";
	}

}
