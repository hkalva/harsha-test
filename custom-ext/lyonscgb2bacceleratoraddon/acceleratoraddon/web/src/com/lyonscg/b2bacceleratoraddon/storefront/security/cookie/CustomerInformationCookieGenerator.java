/**
 *
 */
package com.lyonscg.b2bacceleratoraddon.storefront.security.cookie;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.lyonscg.b2bacceleratoraddon.store.data.CustomerLocaleData;

import de.hybris.platform.site.BaseSiteService;


/**
 * Cookie generator to include customer locale and currency.
 */
public class CustomerInformationCookieGenerator extends EnhancedCookieGenerator
{
    /** Locale separator. */
    public static final String LOCALE_SEPARATOR = "|";

    /** baseSiteService bean. */
    private BaseSiteService baseSiteService;

    /**
     * Creates a cookie with the data provided by the {@link CustomerLocaleData} parameter.
     *
     * @param response
     *            {@link HttpServletResponse}
     * @param customerLocaleData
     *            {@link CustomerLocaleData} object containing data to be added to the cookie
     */
    public void addCookie(final HttpServletResponse response, final CustomerLocaleData customerLocaleData)
    {
        final StringBuilder sb = new StringBuilder();
        sb.append(customerLocaleData.getLocale()).append(LOCALE_SEPARATOR).append(customerLocaleData.getCurrency());
        super.addCookie(response, sb.toString());
    }

    @Override
    public String getCookieName()
    {
        return StringUtils.deleteWhitespace(getBaseSiteService().getCurrentBaseSite().getUid()) + "-customerLocaleInformation";
    }

    protected BaseSiteService getBaseSiteService()
    {
        return baseSiteService;
    }

    @Required
    public void setBaseSiteService(final BaseSiteService baseSiteService)
    {
        this.baseSiteService = baseSiteService;
    }
}
