/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.lyonscg.b2bacceleratoraddon.controllers;

/**
 */
public interface Lyonscgb2bacceleratoraddonControllerConstants
{
    // implement here controller constants used by this extension
    /**
     * Extension Name Constant
     */
    public static final String EXTENSIONNAME = "lyonscgb2bacceleratoraddon";

    /**
     * Interface views with the constants for Page view names
     */
    interface Views
    {
        interface Pages
        {

            interface MyCompany
            {
                String ADD_ON_PREFIX = "addon:";
                String VIEW_PAGE_PREFIX = ADD_ON_PREFIX + "/" + EXTENSIONNAME + "/";

                String MyCompanyManageUnitAddAddressPage = VIEW_PAGE_PREFIX + "pages/company/myCompanyManageUnitAddAddressPage";
                String MyCompanyManageUnitAddressForm = VIEW_PAGE_PREFIX + "fragments/regions/addressForm";
                String MyCompanyManageUnitDetailsPage = VIEW_PAGE_PREFIX + "pages/company/myCompanyManageUnitDetailsPage";
            }
        }
    }
}
