/**
 *
 */
package com.lyonscg.b2bacceleratoraddon.storefront.wrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.lang.StringUtils;


/**@author lyonscg.
 *Http URL encode request wrapper.
 */
public class UrlEncodeHttpRequestWrapper extends HttpServletRequestWrapper
{
    private final String pattern;

    public UrlEncodeHttpRequestWrapper(final HttpServletRequest request, final String pattern)
    {
        super(request);
        this.pattern = pattern;
    }

    @Override
    public String getContextPath()
    {
        return super.getContextPath() + "/" + pattern;
    }

    @Override
    public String getRequestURI()
    {
        final String originalRequestURI = super.getRequestURI();
        final String originalContextPath = super.getContextPath();
        final String contextPath = this.getContextPath();
        final String originalRequestUriMinusAnyContextPath;

        if (StringUtils.startsWith(originalRequestURI, contextPath))
        {
            /*
             * HLP1-641 - Changed from remove() method to replaceOnce() method, so that only the first occurrence of the
             * context path is replaced
             */
            originalRequestUriMinusAnyContextPath = StringUtils.replaceOnce(originalRequestURI, contextPath, "");
        }
        else if (StringUtils.startsWith(originalRequestURI, originalContextPath))
        {
            originalRequestUriMinusAnyContextPath = StringUtils.replaceOnce(originalRequestURI, originalContextPath, "");
        }
        else
        {
            originalRequestUriMinusAnyContextPath = originalRequestURI;
        }

        return getContextPath() + originalRequestUriMinusAnyContextPath;
    }

    @Override
    public String getServletPath()
    {
        final String originalServletPath = super.getServletPath();
        if (("/").equals(originalServletPath) || ("/" + pattern).equals(originalServletPath)
                || ("/" + pattern + "/").equals(originalServletPath))
        {
            return "";
        }
        else if (urlPatternChecker(originalServletPath, pattern))
        {
            return StringUtils.replace(originalServletPath, "/" + pattern + "/", "/");
        }
        return originalServletPath;
    }

    protected boolean urlPatternChecker(final String urlToBeChecked, final String pattern)
    {
        boolean containsPattern = StringUtils.contains(urlToBeChecked, "/" + pattern + "/");
        if (!containsPattern)
        {
            final String[] splitUrl = urlToBeChecked.split("/");
            final String last = splitUrl[splitUrl.length - 1];
            if (last.equalsIgnoreCase(pattern))
            {
                containsPattern = true;
            }
        }
        return containsPattern;
    }
}
