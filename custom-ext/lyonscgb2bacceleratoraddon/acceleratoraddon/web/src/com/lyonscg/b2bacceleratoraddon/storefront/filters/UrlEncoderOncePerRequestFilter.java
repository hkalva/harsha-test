/**
 *
 */
package com.lyonscg.b2bacceleratoraddon.storefront.filters;

import de.hybris.platform.acceleratorfacades.urlencoder.UrlEncoderFacade;
import de.hybris.platform.acceleratorfacades.urlencoder.data.UrlEncoderData;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.util.Config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;

import com.lyonscg.b2bacceleratoraddon.storefront.wrapper.UrlEncodeHttpRequestWrapper;


/**@author lyonscg.
 *Implements once per request filter for URL encoder.
 */
public class UrlEncoderOncePerRequestFilter extends OncePerRequestFilter
{
    private static final Logger LOG = Logger.getLogger(UrlEncoderOncePerRequestFilter.class.getName());

    private UrlEncoderFacade urlEncoderFacade;

    private SessionService sessionService;

    /**
     * Processes filter requests.  This method ensures that the request URL contains the proper URL Encoding
     * Data elements.  If the URL is missing any of the elements the URL is rebuild with the proper URL
     * Encoding Data elements and the user is redirected to the new URL.
     * <p>
     * It is possible to bypass the check by providing a piped "|" delimited list of URL components in the local.propeties file.
     * For example: url.encoder.filter.whitelist.pattern=/cart|/checkout
     * 
     * 
     * @param request the {@link HttpServletRequest}
     * @param response the {@link HttpServletResponse}
     * @param filterChain the {@link FilterChain}
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
            final FilterChain filterChain) throws ServletException, IOException
    {
        final String currentRequestURL = request.getRequestURL().toString();
        final String currentRequestURI = request.getRequestURI();

        if (LOG.isDebugEnabled())
        {
            LOG.debug(" The incoming URL : [" + currentRequestURL + "]");
        }

        final List<UrlEncoderData> currentUrlEncoderDatas = getUrlEncoderFacade().getCurrentUrlEncodingData();

        // there are certain paths that we do not want to redirect namely those doing with cart and checkout
        boolean whiteListed = false;

        final String whitelistValues = Config.getParameter("url.encoder.filter.whitelist.pattern");

        if (LOG.isDebugEnabled())
        {
            LOG.debug(" URL Encoder whitelist values : [" + whitelistValues + "]");
        }

        if (StringUtils.isNotEmpty(whitelistValues))
        {
            if (LOG.isDebugEnabled())
            {
                LOG.debug(" The incoming URI : [" + currentRequestURL + "]");
            }

            final String[] values = StringUtils.split(whitelistValues, '|');
            final ArrayList<String> valueList = new ArrayList<String>(Arrays.asList(values));
            for (final String value : valueList)
            {
                if (StringUtils.containsIgnoreCase(currentRequestURI, value))
                {
                    whiteListed = true;
                    break;
                }
            }

            if (LOG.isDebugEnabled())
            {
                LOG.debug(" URL will bypass encoding redirect: [" + whiteListed + "]");
            }
        }

        if (currentUrlEncoderDatas != null && !currentUrlEncoderDatas.isEmpty())
        {
            final String currentPattern = getSessionService().getAttribute(WebConstants.URL_ENCODING_ATTRIBUTES);
            final String newPattern = getUrlEncoderFacade().calculateAndUpdateUrlEncodingData(currentRequestURI,
                    request.getContextPath());
            final String newPatternWithSlash = "/" + newPattern;

            if (!StringUtils.containsIgnoreCase(currentRequestURL, newPatternWithSlash) && !whiteListed)
            {
                // need to build a new URL and redirect to it as it does not contain the necessary URL encoding attributes
                LOG.info("Requested URL (" + currentRequestURL + ")");
                LOG.info("Requested URI (" + currentRequestURI + ")");
                LOG.info("Requested URL (" + currentRequestURL + ") is missing encoding attributes (" + newPatternWithSlash + ")");
                String basePath;
                if (currentRequestURI.equals("/")) 
                {
                	basePath = StringUtils.removeEnd(currentRequestURL, "/");
                }
                else
                {
                	basePath = StringUtils.substringBefore(currentRequestURL, currentRequestURI);
                }
                LOG.info("BasePath: " + basePath);
                final StringBuilder newPath = new StringBuilder().append(basePath).append(newPatternWithSlash)
                        .append(currentRequestURI);
                LOG.info("Redirecting to: " + newPath.toString());
                response.sendRedirect(newPath.toString());
                return;
            }

            if (!StringUtils.equalsIgnoreCase(currentPattern, newPatternWithSlash))
            {
                getUrlEncoderFacade().updateSiteFromUrlEncodingData();
                getSessionService().setAttribute(WebConstants.URL_ENCODING_ATTRIBUTES, newPatternWithSlash);
            }

            final UrlEncodeHttpRequestWrapper wrappedRequest = new UrlEncodeHttpRequestWrapper(request, newPattern);
            wrappedRequest.setAttribute(WebConstants.URL_ENCODING_ATTRIBUTES, newPatternWithSlash);
            wrappedRequest.setAttribute("originalContextPath",
                    StringUtils.isBlank(request.getContextPath()) ? "/" : request.getContextPath());
            if (LOG.isDebugEnabled())
            {
                LOG.debug("ContextPath=[" + wrappedRequest.getContextPath() + "]" + " Servlet Path= ["
                        + wrappedRequest.getServletPath() + "]" + " Request Url= [" + wrappedRequest.getRequestURL() + "]");
            }
            filterChain.doFilter(wrappedRequest, response);
        }
        else
        {
            if (LOG.isDebugEnabled())
            {
                LOG.debug(" No URL attributes defined");
            }
            request.setAttribute(WebConstants.URL_ENCODING_ATTRIBUTES, "");
            filterChain.doFilter(request, response);
        }
    }

    protected UrlEncoderFacade getUrlEncoderFacade()
    {
        return urlEncoderFacade;
    }

    @Required
    public void setUrlEncoderFacade(final UrlEncoderFacade urlEncoderFacade)
    {
        this.urlEncoderFacade = urlEncoderFacade;
    }

    protected SessionService getSessionService()
    {
        return sessionService;
    }

    @Required
    public void setSessionService(final SessionService sessionService)
    {
        this.sessionService = sessionService;
    }
}
