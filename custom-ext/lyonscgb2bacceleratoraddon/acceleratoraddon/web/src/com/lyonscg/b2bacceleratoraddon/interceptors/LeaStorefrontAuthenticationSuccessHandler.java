package com.lyonscg.b2bacceleratoraddon.interceptors;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import com.lyonscg.b2bacceleratoraddon.store.data.CustomerLocaleData;
import com.lyonscg.b2bacceleratoraddon.storefront.security.cookie.CustomerInformationCookieGenerator;

import de.hybris.platform.acceleratorfacades.customerlocation.CustomerLocationFacade;
import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.security.BruteForceAttackCounter;
import de.hybris.platform.acceleratorstorefrontcommons.strategy.CartRestorationStrategy;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commerceservices.enums.UiExperienceLevel;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.Constants;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;


/**
 *
 * This class is used to replace {@link SavedRequestAwareAuthenticationSuccessHandler}. The main logic is about the
 * same, the only change is this handler will restore {@link CustomerLocaleData} from cookie. Once Hybris is upgraded,
 * please make sure the logic in this handler is updated accordingly as well.
 * 
 * @author lyonscg
 *
 */
public class LeaStorefrontAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler
{
    private CustomerFacade customerFacade;
    private UiExperienceService uiExperienceService;
    private CartFacade cartFacade;
    private BruteForceAttackCounter bruteForceAttackCounter;
    private CartRestorationStrategy cartRestorationStrategy;
    private Map<UiExperienceLevel, Boolean> forceDefaultTargetForUiExperienceLevel;
    private List<String> restrictedPages;
    private List<String> listRedirectUrlsForceDefaultTarget;
    // Set locale to avoid findbug issue
    private GrantedAuthority adminAuthority = new SimpleGrantedAuthority("ROLE_"
            + Constants.USER.ADMIN_USERGROUP.toUpperCase(Locale.getDefault()));

    private static final String CHECKOUT_URL = "/checkout";
    private static final String CART_URL = "/cart";

    private static final Logger LOG = Logger.getLogger(LeaStorefrontAuthenticationSuccessHandler.class);

    @Resource
    private CustomerLocationFacade customerLocationFacade;
    @Resource
    private CustomerInformationCookieGenerator customerInfoCookieGenerator;
    @Resource
    private CommerceCommonI18NService commerceCommonI18NService;
    @Resource
    private CommonI18NService commonI18NService;

    @Resource(name = "storeSessionFacade")
    private StoreSessionFacade storeSessionFacade;

    @Resource(name = "userFacade")
    private UserFacade userFacade;

    @Override
    public void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response,
            final Authentication authentication) throws IOException, ServletException
    {
        //if redirected from some specific url, need to remove the cachedRequest to force use defaultTargetUrl
        final RequestCache requestCache = new HttpSessionRequestCache();
        final SavedRequest savedRequest = requestCache.getRequest(request, response);

        if (savedRequest != null)
        {
            for (final String redirectUrlForceDefaultTarget : getListRedirectUrlsForceDefaultTarget())
            {
                if (savedRequest.getRedirectUrl().contains(redirectUrlForceDefaultTarget))
                {
                    requestCache.removeRequest(request, response);
                    break;
                }
            }
        }
        getCustomerFacade().loginSuccess();
        request.setAttribute(WebConstants.CART_MERGED, Boolean.FALSE);

        // Check if the user is in role admingroup
        if (!isAdminAuthority(authentication))
        {
            getCartRestorationStrategy().restoreCart(request);
            getBruteForceAttackCounter().resetUserCounter(getCustomerFacade().getCurrentCustomerUid());
            // Customized code to restore customerLocaleData
            restoreCustomerLocaleDataFromCookie(request);
            super.onAuthenticationSuccess(request, response, authentication);
        }
        else
        {
            LOG.warn("Invalidating session for user in the " + Constants.USER.ADMIN_USERGROUP + " group");
            invalidateSession(request, response);
        }
    }

    /**
     * 
     * This method will find the correct cookie from {@link HttpServletRequest} and retrieve {@link CustomerLocaleData}
     * from it if there is any. Also, it will set currency and language as current language and current currency based
     * on what's set in the cookie
     * 
     * @param request
     *            {@link HttpServletRequest}
     */
    private void restoreCustomerLocaleDataFromCookie(final HttpServletRequest request)
    {
        final Cookie[] cookies = request.getCookies();

        if (cookies != null)
        {
            for (final Cookie cookie : cookies)
            {
                if (customerInfoCookieGenerator.getCookieName().equals(cookie.getName()))
                {
                    final CustomerLocaleData customerLocaleData = decipherCustomerLocaleData(StringUtils.remove(
                            cookie.getValue(), "\""));
                    commonI18NService.setCurrentLanguage(commonI18NService.getLanguage(customerLocaleData.getLocale()));
                    commerceCommonI18NService.setCurrentCurrency(commonI18NService.getCurrency(customerLocaleData
                            .getCurrency()));
                    // Do currency conversion if necessary
                    storeSessionFacade.setCurrentCurrency(customerLocaleData.getCurrency());
                    userFacade.syncSessionCurrency();
                    break;
                }
            }
        }
    }

    /**
     * Convert string stored in cookie into {@link CustomerLocaleData}
     * 
     * @param customerLocaleDataString
     *            CustomerLocaleData stored in cookie
     * @return {@link CustomerLocaleData} Convert customerLocaleDataString to CustomerLocaleData
     */
    protected CustomerLocaleData decipherCustomerLocaleData(final String customerLocaleDataString)
    {
        final CustomerLocaleData customerLocaleData = new CustomerLocaleData();
        final String locale = StringUtils.substringBefore(customerLocaleDataString,
                CustomerInformationCookieGenerator.LOCALE_SEPARATOR);
        final String currency = StringUtils.substringAfter(customerLocaleDataString,
                CustomerInformationCookieGenerator.LOCALE_SEPARATOR);

        // If currency is empty, use default currency instead
        customerLocaleData.setCurrency(StringUtils.isEmpty(currency) ? commerceCommonI18NService.getDefaultCurrency()
                .getIsocode() : currency);
        // If locale is empty, use default language instead
        customerLocaleData.setLocale(StringUtils.isEmpty(locale) ? commerceCommonI18NService.getDefaultLanguage().getIsocode()
                : locale);
        return customerLocaleData;
    }

    protected void invalidateSession(final HttpServletRequest request, final HttpServletResponse response) throws IOException
    {
        SecurityContextHolder.getContext().setAuthentication(null);
        request.getSession().invalidate();
        response.sendRedirect(request.getContextPath());
    }

    protected boolean isAdminAuthority(final Authentication authentication)
    {
        return CollectionUtils.isNotEmpty(authentication.getAuthorities())
                && authentication.getAuthorities().contains(adminAuthority);
    }

    protected List<String> getRestrictedPages()
    {
        return restrictedPages;
    }

    public void setRestrictedPages(final List<String> restrictedPages)
    {
        this.restrictedPages = restrictedPages;
    }

    protected CartFacade getCartFacade()
    {
        return cartFacade;
    }

    @Required
    public void setCartFacade(final CartFacade cartFacade)
    {
        this.cartFacade = cartFacade;
    }

    protected CustomerFacade getCustomerFacade()
    {
        return customerFacade;
    }

    @Required
    public void setCustomerFacade(final CustomerFacade customerFacade)
    {
        this.customerFacade = customerFacade;
    }

    /*
     * @see org.springframework.security.web.authentication.AbstractAuthenticationTargetUrlRequestHandler#
     * isAlwaysUseDefaultTargetUrl()
     */
    @Override
    protected boolean isAlwaysUseDefaultTargetUrl()
    {
        final UiExperienceLevel uiExperienceLevel = getUiExperienceService().getUiExperienceLevel();
        if (getForceDefaultTargetForUiExperienceLevel().containsKey(uiExperienceLevel))
        {
            return Boolean.TRUE.equals(getForceDefaultTargetForUiExperienceLevel().get(uiExperienceLevel));
        }
        else
        {
            return false;
        }
    }

    @Override
    protected String determineTargetUrl(final HttpServletRequest request, final HttpServletResponse response)
    {
        String targetUrl = super.determineTargetUrl(request, response);
        if (CollectionUtils.isNotEmpty(getRestrictedPages()))
        {
            for (final String restrictedPage : getRestrictedPages())
            {
                // When logging in from a restricted page, return user to default target url.
                if (targetUrl.contains(restrictedPage))
                {
                    targetUrl = super.getDefaultTargetUrl();
                }
            }
        }
        /*
         * If the cart has been merged and the user logging in through checkout, redirect to the cart page to display
         * the new cart
         */
        if (StringUtils.equals(targetUrl, CHECKOUT_URL)
                && BooleanUtils.toBoolean((Boolean) request.getAttribute(WebConstants.CART_MERGED)))
        {
            targetUrl = CART_URL;
        }

        return targetUrl;
    }

    protected Map<UiExperienceLevel, Boolean> getForceDefaultTargetForUiExperienceLevel()
    {
        return forceDefaultTargetForUiExperienceLevel;
    }

    @Required
    public void setForceDefaultTargetForUiExperienceLevel(
            final Map<UiExperienceLevel, Boolean> forceDefaultTargetForUiExperienceLevel)
    {
        this.forceDefaultTargetForUiExperienceLevel = forceDefaultTargetForUiExperienceLevel;
    }

    protected BruteForceAttackCounter getBruteForceAttackCounter()
    {
        return bruteForceAttackCounter;
    }

    @Required
    public void setBruteForceAttackCounter(final BruteForceAttackCounter bruteForceAttackCounter)
    {
        this.bruteForceAttackCounter = bruteForceAttackCounter;
    }

    protected UiExperienceService getUiExperienceService()
    {
        return uiExperienceService;
    }

    @Required
    public void setUiExperienceService(final UiExperienceService uiExperienceService)
    {
        this.uiExperienceService = uiExperienceService;
    }

    protected List<String> getListRedirectUrlsForceDefaultTarget()
    {
        return listRedirectUrlsForceDefaultTarget;
    }

    @Required
    public void setListRedirectUrlsForceDefaultTarget(final List<String> listRedirectUrlsForceDefaultTarget)
    {
        this.listRedirectUrlsForceDefaultTarget = listRedirectUrlsForceDefaultTarget;
    }

    /**
     * @param adminGroup
     *            the adminGroup to set
     */
    public void setAdminGroup(final String adminGroup)
    {
        if (StringUtils.isBlank(adminGroup))
        {
            adminAuthority = null;
        }
        else
        {
            adminAuthority = new SimpleGrantedAuthority(adminGroup);
        }
    }

    protected GrantedAuthority getAdminAuthority()
    {
        return adminAuthority;
    }

    protected CartRestorationStrategy getCartRestorationStrategy()
    {
        return cartRestorationStrategy;
    }

    @Required
    public void setCartRestorationStrategy(final CartRestorationStrategy cartRestorationStrategy)
    {
        this.cartRestorationStrategy = cartRestorationStrategy;
    }
}
