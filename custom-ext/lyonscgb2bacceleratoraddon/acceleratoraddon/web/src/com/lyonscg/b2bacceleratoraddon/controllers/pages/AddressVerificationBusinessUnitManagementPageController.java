/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.lyonscg.b2bacceleratoraddon.controllers.pages;

import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.lyonscg.b2bacceleratoraddon.controllers.Lyonscgb2bacceleratoraddonControllerConstants;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.verification.AddressVerificationResultHandler;
import de.hybris.platform.acceleratorstorefrontcommons.util.AddressDataUtil;
import de.hybris.platform.b2b.company.B2BCommerceUnitService;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.address.AddressVerificationFacade;
import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commerceorgaddon.controllers.ControllerConstants;
import de.hybris.platform.commerceorgaddon.controllers.pages.BusinessUnitManagementPageController;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;


/**
 * Controller defines routes to manage Business Units within My Company section. To implement Address Verification in My
 * Company Address Addition/Edit
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/my-company/organization-management/manage-units")
public class AddressVerificationBusinessUnitManagementPageController extends BusinessUnitManagementPageController
{
    private static final Logger LOG = Logger.getLogger(AddressVerificationBusinessUnitManagementPageController.class);

    @Resource(name = "addressVerificationFacade")
    private AddressVerificationFacade addressVerificationFacade;

    @Resource(name = "addressVerificationResultHandler")
    private AddressVerificationResultHandler addressVerificationResultHandler;

    @Resource(name = "i18NFacade")
    private I18NFacade i18NFacade;

    @Resource(name = "b2BCommerceUnitService")
    private B2BCommerceUnitService b2BCommerceUnitService;

    @Resource(name = "addressDataUtil")
    private AddressDataUtil addressDataUtil;

    protected AddressVerificationFacade getAddressVerificationFacade()
    {
        return addressVerificationFacade;
    }

    protected AddressVerificationResultHandler getAddressVerificationResultHandler()
    {
        return addressVerificationResultHandler;
    }

    protected I18NFacade getI18NFacade()
    {
        return i18NFacade;
    }

    protected B2BCommerceUnitService getB2BCommerceUnitService()
    {
        return b2BCommerceUnitService;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * de.hybris.platform.commerceorgaddon.controllers.pages.BusinessUnitManagementPageController#addAddress(java.lang
     * .String, org.springframework.ui.Model)
     */
    @Override
    @RequestMapping(value = "/add-address", method = RequestMethod.GET)
    @RequireHardLogIn
    public String addAddress(@RequestParam("unit") final String unit, final Model model) throws CMSItemNotFoundException
    {
        if (StringUtils.isEmpty(unit))
        {
            return REDIRECT_PREFIX + MANAGE_UNITS_BASE_URL;
        }
        final B2BUnitData unitData = b2bUnitFacade.getUnitForUid(unit);
        if (unitData == null)
        {
            return REDIRECT_PREFIX + MANAGE_UNITS_BASE_URL;
        }
        model.addAttribute("countryData", checkoutFacade.getDeliveryCountries());
        model.addAttribute("titleData", getUserFacade().getTitles());
        model.addAttribute("addressForm", new AddressForm());

        storeCmsPageInModel(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));

        final List<Breadcrumb> breadcrumbs = myCompanyBreadcrumbBuilder.createManageUnitsDetailsBreadcrumbs(unit);

        breadcrumbs.add(new Breadcrumb(String.format("/my-company/organization-management/manage-units/add-address/?unit=%s",
                urlEncode(unit)), getMessageSource().getMessage("text.company.manage.units.addAddress", new Object[]
        { unit }, "Add Address for {0} Business Unit ", getI18nService().getCurrentLocale()), null));

        model.addAttribute("unitName", unitData.getName());
        model.addAttribute("breadcrumbs", breadcrumbs);
        model.addAttribute("uid", unit);
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

        return Lyonscgb2bacceleratoraddonControllerConstants.Views.Pages.MyCompany.MyCompanyManageUnitAddAddressPage;
    }

    @RequestMapping(value =
    { "/region-data", "/edit-address/region-data", "/add-address/region-data" }, method = RequestMethod.POST)
    @RequireHardLogIn
    public String getCountryAddressForm(@RequestParam(value = "countryIsoCode", required = false) final String countryIsoCode,
            @RequestParam("unit") final String unit, @RequestParam("addressId") final String addressId, final Model model)
    {
        if (StringUtils.isEmpty(countryIsoCode))
        {
            return REDIRECT_PREFIX + MANAGE_UNITS_BASE_URL;
        }
        List<RegionData> regionData = i18NFacade.getRegionsForCountryIso(countryIsoCode);
        if (CollectionUtils.isNotEmpty(regionData))
        {
            model.addAttribute("regionData", regionData);
        }
        model.addAttribute("countryData", checkoutFacade.getDeliveryCountries());
        model.addAttribute("titleData", getUserFacade().getTitles());
        final AddressForm form = new AddressForm();
        model.addAttribute("addressForm", form);
        model.addAttribute("uid", unit);
        model.addAttribute("addressId", addressId);

        if (StringUtils.isNotEmpty(unit) && StringUtils.isNotEmpty(addressId))
        {
            final B2BUnitData unitData = b2bUnitFacade.getUnitForUid(unit);
            if (unitData != null)
            {
                for (final AddressData addressData : unitData.getAddresses())
                {
                    if (addressData.getId() != null && addressData.getId().equals(addressId))
                    {
                        model.addAttribute("addressData", addressData);
                        addressDataUtil.convert(addressData, form);
                        break;
                    }
                }
            }
        }

        form.setCountryIso(countryIsoCode);
        return Lyonscgb2bacceleratoraddonControllerConstants.Views.Pages.MyCompany.MyCompanyManageUnitAddressForm;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * de.hybris.platform.commerceorgaddon.controllers.pages.BusinessUnitManagementPageController#addAddress(java.lang
     * .String, de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm,
     * org.springframework.validation.BindingResult, org.springframework.ui.Model,
     * org.springframework.web.servlet.mvc.support.RedirectAttributes)
     */
    @Override
    @RequestMapping(value = "/add-address", method = RequestMethod.POST)
    @RequireHardLogIn
    public String addAddress(@RequestParam("unit") final String unit, @Valid final AddressForm addressForm,
            final BindingResult bindingResult, final Model model, final RedirectAttributes redirectModel)
            throws CMSItemNotFoundException
    {
        if (StringUtils.isEmpty(unit))
        {
            return REDIRECT_PREFIX + MANAGE_UNITS_BASE_URL;
        }
        final B2BUnitData unitData = b2bUnitFacade.getUnitForUid(unit);
        if (unitData == null)
        {
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "b2bunit.notfound");
            return REDIRECT_PREFIX + MANAGE_UNITS_BASE_URL;
        }
        String countIso = addressForm.getCountryIso();
        if (StringUtils.isNotEmpty(countIso))
        {
            List<RegionData> regionData = i18NFacade.getRegionsForCountryIso(countIso);
            if (CollectionUtils.isNotEmpty(regionData))
            {
                model.addAttribute("regionData", regionData);
            }
        }
        if (bindingResult.hasErrors())
        {
            final List<Breadcrumb> breadcrumbs = myCompanyBreadcrumbBuilder.createManageUnitsDetailsBreadcrumbs(unit);

            breadcrumbs.add(new Breadcrumb(String.format("/my-company/organization-management/manage-units/add-address/?unit=%s",
                    urlEncode(unit)), getMessageSource().getMessage("text.company.manage.units.addAddress", new Object[]
            { unit }, "Add Address to {0} Business Unit ", getI18nService().getCurrentLocale()), null));

            model.addAttribute("breadcrumbs", breadcrumbs);

            GlobalMessages.addErrorMessage(model, "form.global.error");
            storeCmsPageInModel(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));

            model.addAttribute("countryData", checkoutFacade.getDeliveryCountries());
            model.addAttribute("titleData", getUserFacade().getTitles());
            model.addAttribute("uid", unit);
            model.addAttribute("unitName", unitData.getName());

            return Lyonscgb2bacceleratoraddonControllerConstants.Views.Pages.MyCompany.MyCompanyManageUnitAddAddressPage;
        }

        final AddressData newAddress = addressDataUtil.convertToAddressData(addressForm);

        final AddressVerificationResult<AddressVerificationDecision> verificationResult = getAddressVerificationFacade()
                .verifyAddressData(newAddress);

        final boolean addressRequiresReview = getAddressVerificationResultHandler().handleResult(verificationResult, newAddress,
                model, redirectModel, bindingResult,
                getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(), "checkout.multi.address.added");

        model.addAttribute("country", addressForm.getCountryIso());
        model.addAttribute("countryData", checkoutFacade.getDeliveryCountries());
        model.addAttribute("titleData", getUserFacade().getTitles());
        model.addAttribute("uid", unit);

        model.addAttribute("unitName", unitData.getName());

        if (addressRequiresReview && verificationResult.getSuggestedAddresses().size() > 0)
        {
            storeCmsPageInModel(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
            return Lyonscgb2bacceleratoraddonControllerConstants.Views.Pages.MyCompany.MyCompanyManageUnitAddAddressPage;
        }

        try
        {
            b2bUnitFacade.addAddressToUnit(newAddress, unit);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
                    "account.confirmation.address.added");
        }
        catch (final Exception e)
        {
            LOG.error(e.getMessage(), e);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "form.global.error");
        }

        return String.format(REDIRECT_TO_UNIT_DETAILS, urlEncode(unit));
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * de.hybris.platform.commerceorgaddon.controllers.pages.BusinessUnitManagementPageController#editAddress(java.lang
     * .String, java.lang.String, org.springframework.ui.Model,
     * org.springframework.web.servlet.mvc.support.RedirectAttributes)
     */
    @Override
    @RequestMapping(value = "/edit-address", method =
    { RequestMethod.GET })
    @RequireHardLogIn
    public String editAddress(@RequestParam("unit") final String unit, @RequestParam("addressId") final String addressId,
            final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
    {
        if (StringUtils.isEmpty(unit))
        {
            return REDIRECT_PREFIX + MANAGE_UNITS_BASE_URL;
        }
        final B2BUnitData unitData = b2bUnitFacade.getUnitForUid(unit);
        if (unitData == null)
        {
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "b2bunit.notfound");
            return REDIRECT_PREFIX + MANAGE_UNITS_BASE_URL;
        }
        final AddressForm addressForm = new AddressForm();

        model.addAttribute("countryData", checkoutFacade.getDeliveryCountries());
        model.addAttribute("titleData", getUserFacade().getTitles());
        model.addAttribute("addressForm", addressForm);

        for (final AddressData addressData : unitData.getAddresses())
        {
            if (addressData.getId() != null && addressData.getId().equals(addressId))
            {
                model.addAttribute("addressData", addressData);
                if (addressData.getCountry() != null && StringUtils.isNotEmpty(addressData.getCountry().getIsocode()))
                {
                    List<RegionData> regionData = i18NFacade.getRegionsForCountryIso(addressData.getCountry().getIsocode());
                    if (CollectionUtils.isNotEmpty(regionData))
                    {
                        model.addAttribute("regionData", regionData);
                    }
                }
                addressDataUtil.convert(addressData, addressForm);
                break;
            }
        }

        storeCmsPageInModel(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
        final List<Breadcrumb> breadcrumbs = myCompanyBreadcrumbBuilder.createManageUnitsDetailsBreadcrumbs(unit);
        breadcrumbs.add(new Breadcrumb(String.format(
                "/my-company/organization-management/manage-units/edit-address/?unit=%s&addressId=%s", urlEncode(unit),
                urlEncode(addressId)), getMessageSource().getMessage("text.company.manage.units.editAddress", new Object[]
        { unit }, "Edit Address for {0} Business Unit ", getI18nService().getCurrentLocale()), null));

        model.addAttribute("breadcrumbs", breadcrumbs);
        model.addAttribute("uid", unit);
        model.addAttribute("unitName", unitData.getName());
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

        return Lyonscgb2bacceleratoraddonControllerConstants.Views.Pages.MyCompany.MyCompanyManageUnitAddAddressPage;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * de.hybris.platform.commerceorgaddon.controllers.pages.BusinessUnitManagementPageController#editAddress(java.lang
     * .String, java.lang.String, de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm,
     * org.springframework.validation.BindingResult, org.springframework.ui.Model,
     * org.springframework.web.servlet.mvc.support.RedirectAttributes)
     */
    @Override
    @RequestMapping(value = "/edit-address", method =
    { RequestMethod.POST })
    @RequireHardLogIn
    public String editAddress(@RequestParam("unit") final String unit, @RequestParam("addressId") final String addressId,
            @Valid final AddressForm addressForm, final BindingResult bindingResult, final Model model,
            final RedirectAttributes redirectModel) throws CMSItemNotFoundException
    {

        if (StringUtils.isEmpty(unit))
        {
            return REDIRECT_PREFIX + MANAGE_UNITS_BASE_URL;
        }
        final B2BUnitData unitData = b2bUnitFacade.getUnitForUid(unit);
        if (unitData == null)
        {
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "form.global.error");
            return REDIRECT_PREFIX + MANAGE_UNITS_BASE_URL;
        }
        String countIso = addressForm.getCountryIso();
        if (StringUtils.isNotEmpty(countIso))
        {
            List<RegionData> regionData = i18NFacade.getRegionsForCountryIso(countIso);
            if (CollectionUtils.isNotEmpty(regionData))
            {
                model.addAttribute("regionData", regionData);
            }
        }
        if (bindingResult.hasErrors())
        {
            final List<Breadcrumb> breadcrumbs = myCompanyBreadcrumbBuilder.createManageUnitsDetailsBreadcrumbs(unit);

            breadcrumbs.add(new Breadcrumb(String.format(
                    "/my-company/organization-management/manage-units/edit-address/?unit=%s&addressId=%s", urlEncode(unit),
                    urlEncode(addressId)), getMessageSource().getMessage("text.company.manage.units.editAddress.breadcrumb",
                    new Object[]
                    { unit }, "Edit Address of {0} Business Unit ", getI18nService().getCurrentLocale()), null));

            GlobalMessages.addErrorMessage(model, "form.global.error");
            storeCmsPageInModel(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));

            model.addAttribute("countryData", checkoutFacade.getDeliveryCountries());
            model.addAttribute("titleData", getUserFacade().getTitles());
            model.addAttribute("uid", unit);

            for (final AddressData addressData : unitData.getAddresses())
            {
                if (addressData.getId() != null && addressData.getId().equals(addressId))
                {
                    model.addAttribute("addressData", addressData);
                }
            }
            model.addAttribute("unitName", unitData.getName());

            return ControllerConstants.Views.Pages.MyCompany.MyCompanyManageUnitAddAddressPage;
        }

        final AddressData newAddress = addressDataUtil.convertToAddressData(addressForm);

        final AddressVerificationResult<AddressVerificationDecision> verificationResult = getAddressVerificationFacade()
                .verifyAddressData(newAddress);
        final boolean addressRequiresReview = getAddressVerificationResultHandler().handleResult(verificationResult, newAddress,
                model, redirectModel, bindingResult,
                getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(), "checkout.multi.address.updated");

        model.addAttribute("country", addressForm.getCountryIso());
        model.addAttribute("edit", Boolean.TRUE);
        model.addAttribute("countryData", checkoutFacade.getDeliveryCountries());
        model.addAttribute("titleData", getUserFacade().getTitles());
        model.addAttribute("uid", unit);
        model.addAttribute("addressId", addressId);

        for (final AddressData addressData : unitData.getAddresses())
        {
            if (addressData.getId() != null && addressData.getId().equals(addressId))
            {
                model.addAttribute("addressData", addressData);
            }
        }
        model.addAttribute("unitName", unitData.getName());

        if (addressRequiresReview && verificationResult.getSuggestedAddresses().size() > 0)
        {
            storeCmsPageInModel(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
            setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
            return Lyonscgb2bacceleratoraddonControllerConstants.Views.Pages.MyCompany.MyCompanyManageUnitAddAddressPage;
        }

        try
        {
            b2bUnitFacade.editAddressOfUnit(newAddress, unit);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
                    "account.confirmation.address.updated");
        }
        catch (final Exception e)
        {
            LOG.error(e.getMessage(), e);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "form.global.error");
        }

        return String.format(REDIRECT_TO_UNIT_DETAILS, urlEncode(unit));
    }

    /**
     * This method is used to set the selected address from the suggestions overlay into the Address of the Unit
     *
     * @param unit
     * @param addressForm
     * @param redirectModel
     * @return String
     */
    @RequestMapping(value = "/select-suggested-address", method = RequestMethod.POST)
    public String doSelectSuggestedAddress(@RequestParam("unit") final String unit, final AddressForm addressForm,
            final RedirectAttributes redirectModel)
    {
        final AddressData newAddress = addressDataUtil.convertToAddressData(addressForm);

        if (Boolean.TRUE.equals(addressForm.getEditAddress()))
        {
            try
            {
                b2bUnitFacade.editAddressOfUnit(newAddress, unit);
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
                        "account.confirmation.address.updated");
            }
            catch (final Exception e)
            {
                LOG.error(e.getMessage(), e);
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "form.global.error");
                return String.format(REDIRECT_TO_UNIT_DETAILS, urlEncode(unit));
            }
        }
        else
        {
            try
            {
                b2bUnitFacade.addAddressToUnit(newAddress, unit);
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
                        "account.confirmation.address.added");
            }
            catch (final Exception e)
            {
                LOG.error(e.getMessage(), e);
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "form.global.error");
                return String.format(REDIRECT_TO_UNIT_DETAILS, urlEncode(unit));
            }
        }
        return String.format(REDIRECT_TO_UNIT_DETAILS, urlEncode(unit));
    }

    @Override
    @RequestMapping(value = "/details", method = RequestMethod.GET)
    @RequireHardLogIn
    public String unitDetails(@RequestParam("unit") final String unit, final Model model) throws CMSItemNotFoundException
    {
        super.unitDetails(unit, model);
        return Lyonscgb2bacceleratoraddonControllerConstants.Views.Pages.MyCompany.MyCompanyManageUnitDetailsPage;
    }
}
