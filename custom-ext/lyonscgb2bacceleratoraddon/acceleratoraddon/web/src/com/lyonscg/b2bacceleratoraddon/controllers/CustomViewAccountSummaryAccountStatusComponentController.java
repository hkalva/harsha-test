/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.lyonscg.b2bacceleratoraddon.controllers;

import de.hybris.platform.accountsummaryaddon.model.AccountSummaryAccountStatusComponentModel;
import de.hybris.platform.accountsummaryaddon.controllers.AccountSummaryAccountStatusComponentController;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lyonscg.b2bacceleratoraddon.constants.Lyonscgb2bacceleratoraddonConstants;


/**
 * Controller to display account status information. Extended OOTB Controller to make the view method return our custom
 * JSP
 */
@Controller("CustomViewAccountSummaryAccountStatusComponentController")
@Scope("tenant")
@RequestMapping(value = "/view/CustomViewAccountSummaryAccountStatusComponentController")
public class CustomViewAccountSummaryAccountStatusComponentController extends AccountSummaryAccountStatusComponentController
{
    @Override
    protected String getView(final AccountSummaryAccountStatusComponentModel component)
    {
        return Lyonscgb2bacceleratoraddonConstants.ACCOUNT_STATUS_COMPONENTVIEW;
    }

}
