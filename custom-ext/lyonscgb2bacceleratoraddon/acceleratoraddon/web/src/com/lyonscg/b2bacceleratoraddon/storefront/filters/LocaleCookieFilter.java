/**
 *
 */
package com.lyonscg.b2bacceleratoraddon.storefront.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;

import com.lyonscg.b2bacceleratoraddon.storefront.security.cookie.CustomerInformationCookieGenerator;
import com.lyonscg.b2bacceleratoraddon.store.data.CustomerLocaleData;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;


/**
 * The LocaleCookieFilter is responsible for building and saving a cookie to the users
 * browser that holds the current locale and currency.  The purpose of this is for use
 * by 3rd party caching sites as locale and currency are not always available in the URL. 
 */
public class LocaleCookieFilter extends OncePerRequestFilter
{
    private CommonI18NService commonI18NService;
    private CommerceCommonI18NService commerceCommonI18NService;
    private CustomerInformationCookieGenerator customerInfoCookieGenerator;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.web.filter.OncePerRequestFilter#doFilterInternal(javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse, javax.servlet.FilterChain)
     */
    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
            final FilterChain filterChain) throws ServletException, IOException
    {

        final CustomerLocaleData customerLocaleData = new CustomerLocaleData();
        customerLocaleData.setLocale(getCommonI18NService().getCurrentLanguage().getIsocode());
        customerLocaleData.setCurrency(getCommerceCommonI18NService().getCurrentCurrency().getIsocode());
        getCustomerInfoCookieGenerator().addCookie(response, customerLocaleData);
        filterChain.doFilter(request, response);
    }

    public CustomerInformationCookieGenerator getCustomerInfoCookieGenerator()
    {
        return customerInfoCookieGenerator;
    }

    @Required
    public void setCustomerInfoCookieGenerator(CustomerInformationCookieGenerator customerInfoCookieGenerator)
    {
        this.customerInfoCookieGenerator = customerInfoCookieGenerator;
    }


    public CommonI18NService getCommonI18NService()
    {
        return commonI18NService;
    }

    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService)
    {
        this.commonI18NService = commonI18NService;
    }

    public CommerceCommonI18NService getCommerceCommonI18NService()
    {
        return commerceCommonI18NService;
    }

    @Required
    public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
    {
        this.commerceCommonI18NService = commerceCommonI18NService;
    }
}
