/**
 *
 */
package com.lyonscg.b2bacceleratoraddon.interceptors;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;


/**
 * Before View Handler class to override any views within LEA.
 */
public class LyonscgB2bAcceleratoraddonBeforeViewHandler implements BeforeViewHandler
{
    private static final String OVERRIDING_VIEW_WITH = "Overriding view with ";
    private static final String STORE_FINDER_DETAILS_PAGE = "pages/storeFinder/storeFinderDetailsPage";
    private static final String OVERRIDING_STORE_FINDER_DETAILS_PAGE = "addon:/lyonscgb2bacceleratoraddon/pages/storefinder/storeFinderDetailsPage";

    private static final Logger LOG = Logger.getLogger(LyonscgB2bAcceleratoraddonBeforeViewHandler.class);

    /*
     * (non-Javadoc)
     * 
     * @see
     * de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler#beforeView(javax.servlet.http.
     * HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.web.servlet.ModelAndView)
     */
    @Override
    public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView)
            throws Exception
    {
        final String viewName = modelAndView.getViewName();
        /* Overriding the Store Finder details page with our custom responsive version. */
        if (STORE_FINDER_DETAILS_PAGE.equals(viewName))
        {
            LOG.info(OVERRIDING_VIEW_WITH + OVERRIDING_STORE_FINDER_DETAILS_PAGE);
            modelAndView.setViewName(OVERRIDING_STORE_FINDER_DETAILS_PAGE);
        }

    }

}
