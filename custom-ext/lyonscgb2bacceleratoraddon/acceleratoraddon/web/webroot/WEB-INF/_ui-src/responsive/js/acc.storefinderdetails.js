ACC.storefinderdetails = {

	_autoload: [
		["initStoreDetails", $(".js-store-details").length != 0]
	],

	storeName:"",
	storeLatitude:"",
	storeLongitude:"",

	initGoogleMap:function(){
		if($(".js-store-finder-details-map").length > 0){
			ACC.global.addGoogleMapsApi("ACC.storefinderdetails.loadGoogleMap");
		}
	},

	loadGoogleMap: function(){

		var storeName = ACC.storefinderdetails.storeName;
		var storeLatitude = ACC.storefinderdetails.storeLatitude;
		var storeLongitude = ACC.storefinderdetails.storeLongitude;
		
		if($(".js-store-finder-details-map").length > 0)
		{ 
			$(".js-store-finder-details-map").attr("id","store-finder-details-map")
			var centerPoint = new google.maps.LatLng(storeLatitude, storeLongitude);
			var mapOptions = {
				zoom: 13,
				zoomControl: true,
				panControl: true,
				streetViewControl: false,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				center: centerPoint
			}
			var map = new google.maps.Map(document.getElementById("store-finder-details-map"), mapOptions);
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(storeLatitude, storeLongitude),
				map: map,
				title: storeName,
				icon: "https://maps.google.com/mapfiles/marker" + 'A' + ".png"
			});
			var infowindow = new google.maps.InfoWindow({
				content: storeName,
				disableAutoPan: true
			});
			google.maps.event.addListener(marker, 'click', function (){
				infowindow.open(map, marker);
			});
		}

	},

	initStoreDetails:function(){
			ACC.storefinderdetails.storeName =storeName;
			ACC.storefinderdetails.storeLatitude =storeLatitude;
			ACC.storefinderdetails.storeLongitude =storeLongitude;
			
			ACC.storefinderdetails.initGoogleMap();
			$(".js-store-details").show();
			$(".js-store-details-details").show();	
	}
};
