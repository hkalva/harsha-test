$("body").on("change", "#address_country", (function() {
	var selected = $("#address_country option:selected").val();
	var addressId = $("#addressId").val();
	var unit = $("#unit").val();
	$.ajax({
		type : "POST",
		url : "./region-data",
		data : {
			"countryIsoCode" : selected,
			"addressId" : addressId,
			"unit" : unit
		},
		success : function(result) {
			$("#addressForm").html(result);
		}
	});
}));
