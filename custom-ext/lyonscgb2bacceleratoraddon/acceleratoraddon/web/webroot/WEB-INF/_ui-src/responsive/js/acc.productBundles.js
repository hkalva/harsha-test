ACC.productBundles = {

	_autoload: [
		"bindSelectProduct",
		"addBundleToCart",
		"init"
	],

	// Maybe move this to the JSP render with teh status thingie
	init: function() {
		$(document).on('ready', function() {
			var bundles = $('.js-bundle');

			$.each(bundles, function(idx, i) {
				var firstProduct = $(i).find('.js-product-bundle')[0];
				$(firstProduct).addClass('selected');
			});

			ACC.productBundles.bundleUpdatePrice();
		});
	},

	bindSelectProduct: function() {

		$(document).on('click touchstart', '.js-product-bundle', function(e) {

			var el = $(this),
				parent = el.closest('.products');

			if(el.hasClass('selected')) {
				return false;
			} else {
				parent.find('.selected').removeClass('selected');
				el.addClass('selected');
			}

			ACC.productBundles.bundleUpdatePrice();

		});

	},

	addBundleToCart: function() {

		$(document).on('click touchstart', '#addBundleToCartButton', function(e) {
			e.preventDefault();
			$(productCodes).val('');

			var bundleLists = $('.js-bundle'),
				hasError = false;

			$.each(bundleLists, function(idx, i) {
				var bundle = $(i),
					inputId = '#productCodes',
					selectedProd = bundle.find('.selected'),
					inputVal = selectedProd.length > 0 ? selectedProd.data().productId : '',
					bundleVal = bundle.find('.bundleId').val(),
					bundleId = '#bundleTemplateIds';

				if(selectedProd.length > 0) {
					bundle.find('.error').hide();
				} else {
					bundle.find('.error').show();
					hasError = true;
				}

				if($(inputId).val() != ''){
					inputVal = $(inputId).val() + "," + inputVal;
					bundleVal = $(bundleId).val() + "," + bundleVal;
				}
				$(inputId).val(inputVal);
				$(bundleId).val(bundleVal);
			});

			if(!hasError) {
				$('#addToCartForm').submit();
			}

		});

	},

	bundleUpdatePrice: function() {
		var language = $('html').attr('lang'),
			priceUrl = '/' + language + '/b/price',
			bundleLists = $('.js-bundle'),
			selectedBundles = [],
			bundleData, bundle;

		$.each(bundleLists, function(idx, i) {
			bundle = $(i);

			if(bundle.find('.selected').length > 0) {
				bundleData = {
					productCode: bundle.find('.selected').attr('data-product-id'),
					bundleId: bundle.attr('id')
				}

				selectedBundles.push(bundleData);
			}
		});

		if(selectedBundles.length >= 2) {
			$.ajax({
				url: priceUrl,
				method: 'POST',
				contentType:'application/json',
				data: JSON.stringify(selectedBundles),
				success: function(data) {
					$('.js-originalPrice').text(data.priceWithoutBundle.formattedValue);
					$('.js-bundlePrice').text(data.priceWithBundle.formattedValue);
				},
				error: function(data) {
					$('.js-originalPrice').empty();
					$('.js-bundlePrice').empty();
				}
			});
		}
	}

}