<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/addons/lyonscgb2bacceleratoraddon/responsive/store" %>
<%@ taglib prefix="seo" tagdir="/WEB-INF/tags/addons/lyonscgseoaddon/responsive/common" %>

<template:page pageTitle="${pageTitle}">
			<script> var storeLatitude='${store.geoPoint.latitude}';</script>
			<script> var storeLongitude='${store.geoPoint.longitude}';</script>
			<script> var storeName='${store.name}';</script>
			<seo:seoStoreDetailsSchemaTag />
			<div id="storeFinder">
				<store:storeFinderDetails store="${store}"/>
			</div>
</template:page>