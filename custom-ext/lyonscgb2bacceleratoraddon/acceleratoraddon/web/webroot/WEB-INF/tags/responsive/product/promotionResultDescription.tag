<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="promotionResult" required="true" type="de.hybris.platform.commercefacades.product.data.PromotionResultData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
   <c:when test="${not empty promotionResult.promotionData.detailsUrl}">
      <a href="<%=request.getContextPath()%>${promotionResult.promotionData.detailsUrl}">
         ${promotionResult.description}
      </a>
   </c:when>
   <c:otherwise>
      ${promotionResult.description}
   </c:otherwise>
</c:choose>