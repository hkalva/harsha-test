<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="store" required="true" type="de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/responsive/store" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>

<c:set var="openingSchedule" value="${store.openingHours}" />

<div class="store__finder store__finder_detailsPage js-store-details" >
	<div>
		<div>
			<h2 class="headline"><spring:theme code="storeDetails.title" /></h2>

			<div class="store__finder--panel">
				<div class="store__finder--details js-store-details-details">
					<div class="store__finder--details-image"><store:storeImage store="${store}" format="cartIcon" /></div>  

					<div class="store__finder--details-info">
						<div class="info__name">${store.name}</div>
						
						<c:if test="${not empty store.address.line1 || not empty store.address.town || not empty store.address.country.name || not empty store.address.postalCode || not empty store.address.phone }">
						
						<div class="info__address">
							<div>${store.address.line1}</div>

							<c:if test="${not empty store.address.line2}" >
								<div>${store.address.line2}</div>
							</c:if>

							<div>${store.address.town}</div>
							<div>${store.address.postalCode}</div>
							<div>${store.address.country.name}</div>
						</div>

						</c:if>
					</div>

					<hr />

					<div class="store__finder--map js-store-finder-details-map"></div>

					<hr />              

					<div class="store__finder--details-openings">
						<dl class="dl-horizontal">

							<c:if test="${not empty openingSchedule}">
								
								<dl class="dl-horizontal">
								
								<c:forEach items="${openingSchedule.weekDayOpeningList}" var="weekDay" varStatus="weekDayNumber">
									<c:choose>
										<c:when test="${weekDay.closed}" >
											<dt>${weekDay.weekDay}</dt><dd><spring:theme code="storeDetails.table.opening.closed" /></dd>
										</c:when>
										<c:otherwise>
											<dt>${weekDay.weekDay}</dt><dd>${weekDay.openingTime.formattedHour} - ${weekDay.closingTime.formattedHour}<dd>
										</c:otherwise>
									</c:choose>
								</c:forEach>

								</dl>
							</c:if>
						</dl>


						<c:if test="${not empty store.features}">
							<div class="openings__title"><spring:theme code="storeDetails.table.features" /></div>
							
							<ul>
								<c:forEach items="${store.features}" var="feature">
									<li>${feature.value}</li>
								</c:forEach>
							</ul>
						</c:if>
					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
