<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="code" required="true" type="java.lang.String" %>
<%@ attribute name="variantName" required="false" type="java.lang.String" %>
<%@ attribute name="format" required="true" type="java.lang.String" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set value="${ycommerce:productCodeImage(product, code, format)}" var="productImage"/>

<c:choose>
	<c:when test="${not empty variantName}">
	   <c:set value="${fn:escapeXml(variantName)}" var="nameToShow"/>
	</c:when>
	<c:otherwise>
	   <c:set value="${fn:escapeXml(product.name)}" var="nameToShow"/>	
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${not empty productImage}">
		<img src="${productImage.url}" alt="${nameToShow}" title="${nameToShow}"/>
	</c:when>
	<c:otherwise>
		<theme:image code="img.missingProductImage.${format}" alt="${nameToShow}" title="${nameToShow}"/>
	</c:otherwise>
</c:choose>