<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="promotionData" required="true" type="de.hybris.platform.commercefacades.product.data.PromotionData" %>
<%@ attribute name="couldFireMessage" required="false" type="java.lang.String" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
   <c:when test="${not empty couldFireMessage}">
      <c:set var="message" value="${couldFireMessage}"/>
   </c:when>
   <c:otherwise>
      <c:set var ="message" value="${promotionData.description}"/>
   </c:otherwise>
</c:choose>
<c:choose>
   <c:when test="${not empty promotionData.detailsUrl}">
      <a href="<%=request.getContextPath()%>${promotionData.detailsUrl}">
         ${message}
      </a>
   </c:when>
   <c:otherwise>
      ${message}
   </c:otherwise>
</c:choose>