/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.lyonscg.b2bacceleratoraddon.constants;

/**
 * Global class for all Lyonscgb2bacceleratoraddon constants. You can add global constants for your extension into this
 * class.
 */
@SuppressWarnings("deprecation")
public final class Lyonscgb2bacceleratoraddonConstants extends GeneratedLyonscgb2bacceleratoraddonConstants
{

    public static final String EXTENSIONNAME = "lyonscgb2bacceleratoraddon";

    public static final String ACCOUNT_STATUS_COMPONENTVIEW = "addon:/" + EXTENSIONNAME
            + "/cms/accountsummaryaccountstatuscomponent";

    private Lyonscgb2bacceleratoraddonConstants()
    {
        // empty to avoid instantiating this constant class
    }
}
