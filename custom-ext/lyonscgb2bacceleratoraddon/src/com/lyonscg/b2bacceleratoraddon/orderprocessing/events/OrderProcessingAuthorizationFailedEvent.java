package com.lyonscg.b2bacceleratoraddon.orderprocessing.events;

import de.hybris.platform.b2bacceleratorservices.model.process.ReplenishmentProcessModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


/**
 * Event to notify Replenishment Order Authorization Failure.
 *
 */
public class OrderProcessingAuthorizationFailedEvent extends AbstractEvent
{
    private static final long serialVersionUID = 2L;
    private final ReplenishmentProcessModel process;

    /**
     * Constructor to create OrderProcessingAuthorizationFailedEvent.
     *
     * @param replenishmentProcessModel
     *            - replenishmentProcessModel for which Authorization failed
     */
    public OrderProcessingAuthorizationFailedEvent(final ReplenishmentProcessModel replenishmentProcessModel)
    {
        this.process = replenishmentProcessModel;
    }

    /**
     * @return the process
     */
    public ReplenishmentProcessModel getProcess()
    {
        return process;
    }

}
