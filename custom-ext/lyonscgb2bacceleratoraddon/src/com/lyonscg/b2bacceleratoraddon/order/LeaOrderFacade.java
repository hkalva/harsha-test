package com.lyonscg.b2bacceleratoraddon.order;

import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;

/** @author lyonscg
 * Interface to search order from order history with order number.
 */
public interface LeaOrderFacade extends OrderFacade
{
    SearchPageData<OrderHistoryData> getOrderHistoryForCode(final String orderNumber, final PageableData pageableData);
}
