package com.lyonscg.b2bacceleratoraddon.order.dao.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.dao.impl.DefaultCommerceCartDao;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.orderscheduling.model.CartToOrderCronJobModel;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class is to override the default cart restoration search query.
 *
 */
public class OldCartCommerceCartDao extends DefaultCommerceCartDao
{
    protected static final String FIND_OLD_CARTS_FOR_SITE = SELECTCLAUSE + "WHERE {" + CartModel.MODIFIEDTIME
            + "} <= ?modifiedBefore AND {" + CartModel.SITE + "} = ?site" + " and {" + CartModel.SAVETIME + "} IS NULL and {"
            + CartModel.PK + "} not in ({{ SELECT {" + CartToOrderCronJobModel.CART + "} FROM {"
            + CartToOrderCronJobModel._TYPECODE + "} }})" + ORDERBYCLAUSE;

    protected static final String FIND_OLD_CARTS_FOR_SITE_AND_USER = SELECTCLAUSE + "WHERE {" + CartModel.USER
            + "} = ?user AND {" + CartModel.MODIFIEDTIME + "} <= ?modifiedBefore AND {" + CartModel.SITE + "} = ?site" + " AND {"
            + CartModel.SAVETIME + "} IS NULL and {" + CartModel.PK + "} not in ({{ SELECT {" + CartToOrderCronJobModel.CART
            + "} FROM {" + CartToOrderCronJobModel._TYPECODE + "} }})" + ORDERBYCLAUSE;

    @Override
    public List<CartModel> getCartsForRemovalForSiteAndUser(final Date modifiedBefore, final BaseSiteModel site,
            final UserModel user)
            {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("site", site);
        params.put("modifiedBefore", modifiedBefore);

        if (user == null)
        {
            return doSearch(FIND_OLD_CARTS_FOR_SITE, params, CartModel.class);
        }
        else
        {
            params.put("user", user);
            return doSearch(FIND_OLD_CARTS_FOR_SITE_AND_USER, params, CartModel.class);
        }
    }
}
