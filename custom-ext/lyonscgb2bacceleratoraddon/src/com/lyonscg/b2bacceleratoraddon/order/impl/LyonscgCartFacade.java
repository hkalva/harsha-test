package com.lyonscg.b2bacceleratoraddon.order.impl;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;


/**
 * Lyonscg implementation of CartFacade to override default cart restoration logic to prevent loading of Carts
 * associated with Replenishment order.
 */
public class LyonscgCartFacade extends DefaultCartFacade
{
    /**
     * Find the most recent user cart excluding carts associated with Replenishment order.
     * 
     * @param guid
     *            - Cart's guid.
     * @return - Return {@link CartRestorationData}.
     */
    @Override
    public CartRestorationData restoreSavedCart(final String guid) throws CommerceCartRestorationException
    {
        if (StringUtils.isNotBlank(guid))
        {
            return super.restoreSavedCart(guid);
        }
        if (!hasEntries())
        {
            getCartService().setSessionCart(null);
        }
        final CommerceCartParameter parameter = new CommerceCartParameter();
        parameter.setEnableHooks(true);
        parameter.setCart(findCartBySiteAndUser(null));
        return getCartRestorationConverter().convert(getCommerceCartService().restoreCart(parameter));
    }

    /**
     * Finds most recent user cart guid excluding carts associated with Replenishment order or with guids in
     * excludedCartGuid list. If excludedCartGuid is empty or null the excludedCartGuid condition is ignored.
     * 
     * @param excludedCartGuid
     *            - cart guids to exclude.
     * @return - most recent user cart's guid.
     */
    @Override
    public String getMostRecentCartGuidForUser(final Collection<String> excludedCartGuid)
    {
        CartModel cart = findCartBySiteAndUser(excludedCartGuid);
        return null == cart ? null : cart.getGuid();
    }

    /**
     * Finds most recent user cart excluding carts associated with Replenishment order or with guids in excludedCartGuid
     * list. If excludedCartGuid is empty or null the excludedCartGuid condition is ignored.
     * 
     * @param excludedCartGuid
     *            - cart guids to exclude.
     * @return - return most recent user cart.
     */
    protected CartModel findCartBySiteAndUser(final Collection<String> excludedCartGuid)
    {
        boolean skipExcluded = CollectionUtils.isEmpty(excludedCartGuid);
        List<CartModel> cartModels = getCommerceCartService().getCartsForSiteAndUser(getBaseSiteService().getCurrentBaseSite(),
                getUserService().getCurrentUser());
        if (CollectionUtils.isNotEmpty(cartModels))
        {
            for (final CartModel cartModel : cartModels)
            {
                if (CollectionUtils.isEmpty(cartModel.getCartToOrderCronJob())
                        && (skipExcluded ? true : !excludedCartGuid.contains(cartModel.getGuid())))
                {
                    return cartModel;
                }
            }
        }
        return null;
    }
}
