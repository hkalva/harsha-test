package com.lyonscg.b2bacceleratoraddon.order.impl;

import java.util.Collections;

import com.lyonscg.b2bacceleratoraddon.order.LeaOrderFacade;

import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.order.impl.DefaultOrderFacade;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.store.BaseStoreModel;

/**@author lyonscg
 * Implementation of LeaOrderFacade interface to search orders from order history with order number.
 */
public class LeaOrderFacadeImpl extends DefaultOrderFacade implements LeaOrderFacade
{

    @Override
    public SearchPageData<OrderHistoryData> getOrderHistoryForCode(String orderNumber, PageableData pageableData)
    {
        final BaseStoreModel baseStoreModel = getBaseStoreService().getCurrentBaseStore();

        OrderModel orderModel = null;
        if (getCheckoutCustomerStrategy().isAnonymousCheckout())
        {
            orderModel = getCustomerAccountService().getOrderDetailsForGUID(orderNumber, baseStoreModel);
        }
        else
        {

            try
            {
                orderModel = getCustomerAccountService().getOrderForCode((CustomerModel) getUserService().getCurrentUser(),
                        orderNumber, baseStoreModel);
            }
            catch (final ModelNotFoundException e)
            {
            	//displays no order found page when typed order number is not found.
            }
        }
        SearchPageData<OrderModel> searchPageData = new SearchPageData<OrderModel>();
        if (orderModel == null)
        {
            PaginationData pagination = new PaginationData();
            pagination.setCurrentPage(1);
            pagination.setNumberOfPages(0);
            pagination.setPageSize(0);
            pagination.setTotalNumberOfResults(0);
            searchPageData.setPagination(pagination);
            return convertPageData(searchPageData, getOrderHistoryConverter());
        }

        searchPageData.setResults(Collections.singletonList(orderModel));
        PaginationData pagination = new PaginationData();
        pagination.setCurrentPage(1);
        pagination.setNumberOfPages(1);
        pagination.setPageSize(1);
        pagination.setTotalNumberOfResults(1);
        searchPageData.setPagination(pagination);
        return convertPageData(searchPageData, getOrderHistoryConverter());
    }

}
