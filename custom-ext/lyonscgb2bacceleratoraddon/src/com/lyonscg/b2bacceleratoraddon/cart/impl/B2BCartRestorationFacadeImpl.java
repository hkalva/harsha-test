package com.lyonscg.b2bacceleratoraddon.cart.impl;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.lyonscg.b2bacceleratoraddon.cart.B2BCartRestorationFacade;


/**
 * Implementation of B2BCartRestorationFacade.
 *
 */
public class B2BCartRestorationFacadeImpl implements B2BCartRestorationFacade
{
    /** commerceCartService bean. */
    private CommerceCartService commerceCartService;

    /** baseSiteService bean. */
    private BaseSiteService baseSiteService;

    /** userService bean. */
    private UserService userService;

    /** cartFacade bean. */
    private CartFacade cartFacade;

    /** cartService bean. */
    private CartService cartService;

    /** cartRestorationConverter bean. */
    private Converter<CommerceCartRestoration, CartRestorationData> cartRestorationConverter;

    @Override
    public String getRestorationCartGuidForUser(final Collection<String> excludedCartGuid)
    {
        final CartModel cart = getRestorationCartForUser(excludedCartGuid);
        if (null == cart)
        {
            return null;
        }
        return cart.getGuid();
    }

    /**
     * excludedCartGuid- Current card id that has to be excluded in the search.
     * returns - the most recently modified card for the user.
     */
    private CartModel getRestorationCartForUser(final Collection<String> excludedCartGuid)
    {
        final List<CartModel> cartModels = getCommerceCartService().getCartsForSiteAndUser(
                getBaseSiteService().getCurrentBaseSite(), getUserService().getCurrentUser());
        if (CollectionUtils.isNotEmpty(cartModels))
        {
            for (final CartModel cartModel : cartModels)
            {
                if (CollectionUtils.isEmpty(cartModel.getCartToOrderCronJob()))
                {
                    if (!excludedCartGuid.contains(cartModel.getGuid()))
                    {
                        return cartModel;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public CartRestorationData restoreSavedCart(final String guid) throws CommerceCartRestorationException
    {
        if (!getCartFacade().hasEntries())
        {
            getCartService().setSessionCart(null);
        }
        final CommerceCartParameter parameter = new CommerceCartParameter();
        parameter.setEnableHooks(true);
        CartModel cartForGuidAndSiteAndUser = null;
        if (null == guid)
        {
            cartForGuidAndSiteAndUser = getRestorationCartForUser(Collections.EMPTY_LIST);
        }
        else
        {
            cartForGuidAndSiteAndUser = getCommerceCartService().getCartForGuidAndSiteAndUser(guid,
                    getBaseSiteService().getCurrentBaseSite(), getUserService().getCurrentUser());
            if (cartForGuidAndSiteAndUser != null
                    && CollectionUtils.isNotEmpty(cartForGuidAndSiteAndUser.getCartToOrderCronJob()))
            {
                cartForGuidAndSiteAndUser = null;
            }
        }
        parameter.setCart(cartForGuidAndSiteAndUser);
        return getCartRestorationConverter().convert(getCommerceCartService().restoreCart(parameter));
    }

    /**
     * @return the commerceCartService
     */
    public CommerceCartService getCommerceCartService()
    {
        return commerceCartService;
    }

    /**
     * @param commerceCartService
     *            the commerceCartService to set
     */
    @Required
    public void setCommerceCartService(final CommerceCartService commerceCartService)
    {
        this.commerceCartService = commerceCartService;
    }

    /**
     * @return the baseSiteService
     */
    public BaseSiteService getBaseSiteService()
    {
        return baseSiteService;
    }

    /**
     * @param baseSiteService
     *            the baseSiteService to set
     */
    @Required
    public void setBaseSiteService(final BaseSiteService baseSiteService)
    {
        this.baseSiteService = baseSiteService;
    }

    /**
     * @return the userService
     */
    public UserService getUserService()
    {
        return userService;
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService)
    {
        this.userService = userService;
    }

    /**
     * @return the cartFacade
     */
    public CartFacade getCartFacade()
    {
        return cartFacade;
    }

    /**
     * @param cartFacade
     *            the cartFacade to set
     */
    @Required
    public void setCartFacade(final CartFacade cartFacade)
    {
        this.cartFacade = cartFacade;
    }

    /**
     * @return the cartService
     */
    public CartService getCartService()
    {
        return cartService;
    }

    /**
     * @param cartService
     *            the cartService to set
     */
    @Required
    public void setCartService(final CartService cartService)
    {
        this.cartService = cartService;
    }

    /**
     * @return the cartRestorationConverter
     */
    public Converter<CommerceCartRestoration, CartRestorationData> getCartRestorationConverter()
    {
        return cartRestorationConverter;
    }

    /**
     * @param cartRestorationConverter
     *            the cartRestorationConverter to set
     */
    @Required
    public void setCartRestorationConverter(final Converter<CommerceCartRestoration, CartRestorationData> cartRestorationConverter)
    {
        this.cartRestorationConverter = cartRestorationConverter;
    }
}
