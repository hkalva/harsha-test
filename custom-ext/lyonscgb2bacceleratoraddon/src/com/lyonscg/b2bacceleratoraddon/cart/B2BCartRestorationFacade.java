package com.lyonscg.b2bacceleratoraddon.cart;

import java.util.Collection;

import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;


/**
 * Restores cart for a user.
 *
 */
public interface B2BCartRestorationFacade
{
    /**
     * excludedCartGuid - Current card id that has to be excluded in the search.
     * returns the most recently modified card id for the user
     */
    String getRestorationCartGuidForUser(final Collection<String> excludedCartGuid);

    /**
     * @param guid
     *            - Guid of the cart to be restored
     * @return - {@link} CartRestorationData pojo object
     * @throws CommerceCartRestorationException
     */
    CartRestorationData restoreSavedCart(String guid) throws CommerceCartRestorationException;
}
