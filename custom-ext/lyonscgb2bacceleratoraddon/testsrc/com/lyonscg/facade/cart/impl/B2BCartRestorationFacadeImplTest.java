package com.lyonscg.facade.cart.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.lyonscg.b2bacceleratoraddon.cart.impl.B2BCartRestorationFacadeImpl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.order.CommerceSaveCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;

/**
 * To Test the B2BCartRestorationFacadeImpl Class.
 *
 */
@UnitTest
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(LogRunListener.class)
public class B2BCartRestorationFacadeImplTest
{

    @InjectMocks
    private B2BCartRestorationFacadeImpl b2BCartRestorationFacadeImpl;
    @Mock
    private AbstractPopulatingConverter<CartModel, CartData> cartConverter;
    @Mock
    private CommerceCartService commerceCartService;
    @Mock
    private CommerceSaveCartService commerceSaveCartService;
    @Mock
    private BaseSiteService baseSiteService;
    @Mock
    private BaseSiteModel currentBaseSite;
    @Mock
    private UserService userService;
    @Mock
    private CartFacade cartFacade;
    @Mock
    private CartService cartService;
    @Mock
    private Converter<CommerceCartRestoration, CartRestorationData> cartRestorationConverter;
    @Mock
    private UserModel currentUser;
    @Mock
    private CartModel savedCartModel;
    @Mock
    private CommerceCartRestoration savedCartRestoration;
    @Mock
    private CartRestorationData savedCardRestorationData;
    private static final String SAVED_CART_CODE = "test_saved_cart";
    private static final String SAVED_CART_GUID = "1234_abcd_5678_efghi";

    private static final Logger LOG = Logger.getLogger(B2BCartRestorationFacadeImplTest.class);

	 /** Setting up all services such as CommerceCartService,BaseSiteService,UserService,
	 * Saving all cart information into cartList and cartData and return appropriate info when is asked.
	 */
    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);

        b2BCartRestorationFacadeImpl = new B2BCartRestorationFacadeImpl();
        b2BCartRestorationFacadeImpl.setCommerceCartService(commerceCartService);
        b2BCartRestorationFacadeImpl.setBaseSiteService(baseSiteService);
        b2BCartRestorationFacadeImpl.setUserService(userService);
        b2BCartRestorationFacadeImpl.setCartFacade(cartFacade);
        b2BCartRestorationFacadeImpl.setCartService(cartService);
        b2BCartRestorationFacadeImpl.setCartRestorationConverter(cartRestorationConverter);

        final List<CartModel> cartList = new ArrayList<CartModel>();
        cartList.add(savedCartModel);
        final CartData savedCartData = new CartData();
        savedCartData.setCode(SAVED_CART_CODE);
        doReturn(currentUser).when(userService).getCurrentUser();
        doReturn(currentBaseSite).when(baseSiteService).getCurrentBaseSite();
        doReturn(SAVED_CART_CODE).when(savedCartModel).getCode();
        doReturn(SAVED_CART_GUID).when(savedCartModel).getGuid();
        doReturn(currentUser).when(savedCartModel).getUser();
        doReturn(currentBaseSite).when(savedCartModel).getSite();
        doReturn(cartList).when(commerceCartService).getCartsForSiteAndUser(currentBaseSite, currentUser);
        doReturn(savedCartModel).when(commerceCartService).getCartForGuidAndSiteAndUser(SAVED_CART_GUID, currentBaseSite, currentUser);

        try
        {
			doReturn(savedCartRestoration).when(commerceCartService).restoreCart(any(CommerceCartParameter.class));
		}
        catch (CommerceCartRestorationException e)
        {
			LOG.error(e);
			fail();
        }

        doReturn(savedCardRestorationData).when(cartRestorationConverter).convert(savedCartRestoration);

        given(cartConverter.convert(savedCartModel)).willReturn(savedCartData);
        given(userService.getCurrentUser()).willReturn(currentUser);
        given(commerceCartService.getCartForCodeAndUser(SAVED_CART_CODE, currentUser)).willReturn(savedCartModel);
        given(commerceCartService.getCartForGuidAndSite(SAVED_CART_GUID, currentBaseSite)).willReturn(savedCartModel);
        given(commerceCartService.getCartForGuidAndSiteAndUser(SAVED_CART_GUID, currentBaseSite, currentUser)).willReturn(
        		savedCartModel);
    }

    /**
     * Testing Getter to restore CartGu id for the User.
     */
    @Test
    public void testGetRestorationCartGuidForUser()
    {
        final String cartGuid = b2BCartRestorationFacadeImpl.getRestorationCartGuidForUser(Collections.emptyList());
        assertEquals(SAVED_CART_GUID, cartGuid);
    }

    /**
     * testing Restore Saved cart data.
     */
    @Test
    public void testRestoreSavedCart()
    {
    	try
    	{
			final CartRestorationData cartRestorationData = b2BCartRestorationFacadeImpl.restoreSavedCart(SAVED_CART_GUID);
			assertNotNull(cartRestorationData);
    	}
    	catch (CommerceCartRestorationException e)
    	{
			LOG.error(e);
			fail();
		}
    }
}
