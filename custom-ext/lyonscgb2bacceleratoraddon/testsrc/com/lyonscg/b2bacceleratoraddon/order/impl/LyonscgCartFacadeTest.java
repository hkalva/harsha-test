package com.lyonscg.b2bacceleratoraddon.order.impl;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.orderscheduling.model.CartToOrderCronJobModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;


/**
 * Test class for {@link LyonscgCartFacade}.
 */
@UnitTest
public class LyonscgCartFacadeTest
{
    @InjectMocks
    private LyonscgCartFacade testClass;
    @Mock
    private CartService cartService;
    @Mock
    private Converter<CommerceCartRestoration, CartRestorationData> cartRestorationConverter;
    @Mock
    private CommerceCartService commerceCartService;
    @Mock
    private BaseSiteService baseSiteService;
    @Mock
    private UserService userService;
    @Mock
    private BaseSiteModel currentBaseSite;
    @Mock
    private CustomerModel customer;
    @Mock
    private CartModel cart1;
    @Mock
    private CartModel cart2;
    @Mock
    private CartModel repleCart;
    @Mock
    private AbstractOrderEntryModel entry;
    @Mock
    private CartToOrderCronJobModel cronjob;
    @Mock
    private CommerceCartRestoration commerceCartRestoration;
    private String guid1 = "guid1";
    private String guid2 = "guid2";

    /**
     * Setup test data.
     * 
     * @throws CommerceCartRestorationException
     */
    @Before
    public void setUp() throws CommerceCartRestorationException
    {
        testClass = new LyonscgCartFacade();
        MockitoAnnotations.initMocks(this);
        testClass.setCartRestorationConverter(cartRestorationConverter);
        doReturn(currentBaseSite).when(baseSiteService).getCurrentBaseSite();
        doReturn(customer).when(userService).getCurrentUser();

        doReturn(cart1).when(cartService).getSessionCart();
        doReturn(Collections.singletonList(entry)).when(cart1).getEntries();
        doReturn(Collections.singletonList(cronjob)).when(repleCart).getCartToOrderCronJob();

        doReturn(guid2).when(cart2).getGuid();
        doReturn(guid1).when(cart1).getGuid();

        doReturn(Arrays.asList(cart1, cart2, repleCart)).when(commerceCartService).getCartsForSiteAndUser(currentBaseSite,
                customer);
        doReturn(commerceCartRestoration).when(commerceCartService).restoreCart(Mockito.any(CommerceCartParameter.class));
    }

    /**
     * Test case for {@link LyonscgCartFacade#restoreSavedCart(String)} without guid.
     */
    @Test
    public void restoreSavedCartTest() throws CommerceCartRestorationException
    {
        doReturn(true).when(cartService).hasSessionCart();
        doReturn(cart1).when(cartService).getSessionCart();
        ArgumentCaptor<CommerceCartParameter> argument = ArgumentCaptor.forClass(CommerceCartParameter.class);
        testClass.restoreSavedCart(null);
        verify(commerceCartService).restoreCart(argument.capture());
        Assert.assertEquals(cart1, argument.getValue().getCart());
    }

    /**
     * Test case for {@link LyonscgCartFacade#getMostRecentCartGuidForUser(java.util.Collection)} with guid.
     */
    @Test
    public void getMostRecentCartGuidForUserTest() throws CommerceCartRestorationException
    {
        Assert.assertEquals(guid2, testClass.getMostRecentCartGuidForUser(Arrays.asList(guid1)));
    }

    /**
     * Test case for {@link LyonscgCartFacade#getMostRecentCartGuidForUser(java.util.Collection)} with all guids to
     * return null.
     */
    @Test
    public void getMostRecentCartGuidForUserNoCartTest() throws CommerceCartRestorationException
    {
        Assert.assertNull(testClass.getMostRecentCartGuidForUser(Arrays.asList(guid1, guid2)));
    }
}
