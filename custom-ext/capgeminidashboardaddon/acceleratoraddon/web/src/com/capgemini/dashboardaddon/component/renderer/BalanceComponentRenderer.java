package com.capgemini.dashboardaddon.component.renderer;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.capgemini.dashboard.facades.DashboardFacade;
import com.capgemini.dashboard.facades.data.DashboardBalanceData;
import com.capgemini.dashboardaddon.model.BalanceComponentModel;

import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;

/**
 *
 * @author lyonscg
 *
 * @param <C>
 */
public class BalanceComponentRenderer <C extends BalanceComponentModel> extends DefaultAddOnCMSComponentRenderer<C>
{

    /**
     * the dashboard facade.
     */
    private DashboardFacade dashboardFacade;

    @Override
    public void renderComponent(final PageContext pageContext, final C component) throws ServletException, IOException
    {
        final List<DashboardBalanceData> balances = getDashboardFacade().getBalances();
        final StringBuilder moduleView = new StringBuilder();
        moduleView.append("/WEB-INF/views/addons/");
        moduleView.append(getAddonUiExtensionName(component)).append("/");
        moduleView.append(getUIExperienceFolder());
        moduleView.append("/cms/balancecomponent.jsp");
        final Map<String, Object> exposedVariables = exposeVariables(pageContext, component);
        exposedVariables.put("balances", balances);
        pageContext.setAttribute("balances", balances, getScopeForVariableName("balances"));
        pageContext.include(moduleView.toString());
        unExposeVariables(pageContext, component, exposedVariables);
    }

    /**
     * @return the dashboardFacade
     */
    public DashboardFacade getDashboardFacade()
    {
        return dashboardFacade;
    }

    /**
     * @param dashboardFacade
     *           the dashboardFacade to set
     */
    public void setDashboardFacade(final DashboardFacade dashboardFacade)
    {
        this.dashboardFacade = dashboardFacade;
    }

}
