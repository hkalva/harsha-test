/**
 *
 */
package com.capgemini.dashboardaddon.component.renderer;

import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.capgemini.dashboard.facades.DashboardFacade;
import com.capgemini.dashboard.facades.data.DashboardBudgetData;
import com.capgemini.dashboardaddon.model.BudgetComponentModel;


/**
 * @author lyonscg
 *
 */
public class BudgetComponentRenderer<C extends BudgetComponentModel> extends DefaultAddOnCMSComponentRenderer<C>
{
	private DashboardFacade dashboardFacade;

	@Override
	public void renderComponent(final PageContext pageContext, final C component) throws ServletException, IOException
	{
		final List<DashboardBudgetData> budgets = getDashboardFacade().getBudgets();
		final StringBuilder moduleView = new StringBuilder();
		moduleView.append("/WEB-INF/views/addons/");
		moduleView.append(getAddonUiExtensionName(component)).append("/");
		moduleView.append(getUIExperienceFolder());
		moduleView.append("/cms/budgetcomponent.jsp");
		final Map<String, Object> exposedVariables = exposeVariables(pageContext, component);
		exposedVariables.put("budgets", budgets);
		pageContext.setAttribute("budgets", budgets, getScopeForVariableName("budgets"));
		pageContext.include(moduleView.toString());
		unExposeVariables(pageContext, component, exposedVariables);
	}

	/**
	 * @return the dashboardFacade
	 */
	public DashboardFacade getDashboardFacade()
	{
		return dashboardFacade;
	}

	/**
	 * @param dashboardFacade
	 *           the dashboardFacade to set
	 */
	public void setDashboardFacade(final DashboardFacade dashboardFacade)
	{
		this.dashboardFacade = dashboardFacade;
	}
}
