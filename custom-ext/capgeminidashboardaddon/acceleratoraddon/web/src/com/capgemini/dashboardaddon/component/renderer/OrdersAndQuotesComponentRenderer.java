/**
 *
 */
package com.capgemini.dashboardaddon.component.renderer;

import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.capgemini.dashboard.facades.DashboardFacade;
import com.capgemini.dashboard.facades.data.DashboardOrderData;
import com.capgemini.dashboard.facades.data.DashboardQuoteData;
import com.capgemini.dashboardaddon.model.OrdersAndQuotesComponentModel;


/**
 * @author lyonscg
 *
 */
public class OrdersAndQuotesComponentRenderer<C extends OrdersAndQuotesComponentModel> extends DefaultAddOnCMSComponentRenderer<C>
{
	private DashboardFacade dashboardFacade;

	@Override
	public void renderComponent(final PageContext pageContext, final C component) throws ServletException, IOException
	{
		final List<DashboardOrderData> orders = getDashboardFacade().getOrders(component.getMaxDisplay().intValue());
		final List<DashboardQuoteData> quotes = getDashboardFacade().getQuotes(component.getMaxDisplay().intValue());
		final StringBuilder moduleView = new StringBuilder();
		moduleView.append("/WEB-INF/views/addons/");
		moduleView.append(getAddonUiExtensionName(component)).append("/");
		moduleView.append(getUIExperienceFolder());
		moduleView.append("/cms/ordersandquotescomponent.jsp");
		final Map<String, Object> exposedVariables = exposeVariables(pageContext, component);
		exposedVariables.put("orders", orders);
		exposedVariables.put("quotes", quotes);
		pageContext.setAttribute("orders", orders, getScopeForVariableName("orders"));
		pageContext.setAttribute("quotes", quotes, getScopeForVariableName("quotes"));
		pageContext.include(moduleView.toString());
		unExposeVariables(pageContext, component, exposedVariables);
	}

	/**
	 * @return the dashboardFacade
	 */
	public DashboardFacade getDashboardFacade()
	{
		return dashboardFacade;
	}

	/**
	 * @param dashboardFacade
	 *           the dashboardFacade to set
	 */
	public void setDashboardFacade(final DashboardFacade dashboardFacade)
	{
		this.dashboardFacade = dashboardFacade;
	}
}
