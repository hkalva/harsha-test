<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<spring:url value="/my-account/order/" var="orderDetailsUrl" htmlEscape="false"/>
<spring:url value="/my-account/my-quotes/" var="quoteDetailsUrl" htmlEscape="false"/>
<spring:url value="/my-account/orders" var="ordersUrl" htmlEscape="false"/>
<spring:url value="/my-account/my-quotes" var="quotesUrl" htmlEscape="false"/>
 
<div class="dashboard-orders-quotes">
    <h2><spring:theme code="dashboard.orders"/></h2>
    <c:choose>
        <c:when test="${empty orders}">
            <p><spring:theme code="dashboard.no.orders"/></p>
        </c:when>
        <c:otherwise>
            <div class="row">
                <div class="col-xs-12">
        	        <a href="${ordersUrl}" class="dashboard__view-all"><spring:theme code="dashboard.view.all.orders"/></a>
                </div>
            </div>
            <div class="row">
                <c:forEach items="${orders}" var="order">
                    <div class="dashboard__entry">
                        <div class="row">
                            <div class="col-xs-4">
                                <dl>
                                    <dd><spring:theme code="dashboard.order.number"/></dd>
                                    <dt><a href="${orderDetailsUrl}${ycommerce:encodeUrl(order.code)}">${order.code}</a></dt>
                                    <br>
                                    <dd><spring:theme code="dashboard.status"/></dd>
                                    <dt><spring:theme code="text.account.order.status.display.${order.status}" /></dt>
                                </dl>
                            </div>
                            <div class="col-xs-8">
                                <c:choose>
                                    <c:when test="${order.status eq 'completed'}">
                                        <span class="dashboard__icon glyphicon glyphicon-ok">
                                    </c:when>
                                    <c:when test="${order.status eq 'created'}">
                                        <span class="dashboard__icon glyphicon glyphicon-shopping-cart">
                                    </c:when>
                                    <c:when test="${(order.status eq 'approved')}">
                                        <span class="dashboard__icon glyphicon glyphicon-thumbs-up">
                                    </c:when>
                                    <c:when test="${(order.status eq 'cancelled') || (order.status eq 'rejected') || (order.status eq 'cancelling')}">
                                        <span class="dashboard__icon glyphicon glyphicon-thumbs-down">
                                    </c:when>
                                    <c:otherwise>
                                    	<span class="dashboard__icon glyphicon glyphicon-folder-open">
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </c:otherwise>
    </c:choose>
    <h2><spring:theme code="dashboard.quotes"/></h2>
    <c:choose>
        <c:when test="${empty quotes}">
            <p><spring:theme code="dashboard.no.quotes"/></p>
        </c:when>
        <c:otherwise>
        	<div class="row">
                <div class="col-xs-12">
        	        <a href="${quotesUrl}" class="dashboard__view-all"><spring:theme code="dashboard.view.all.quotes"/></a>
                </div>
            </div>
            <div class="row">
                <c:forEach items="${quotes}" var="quote">
                    <div class="dashboard__entry">
                            <div class="row">
                                <div class="col-xs-4">
                                    <dl>
                                        <dd><spring:theme code="dashboard.quote.number"/></dd>
                                        <dt><a href="${quoteDetailsUrl}${ycommerce:encodeUrl(quote.code)}">${quote.code}</a></dt>
                                        <br>
                                        <dd><spring:theme code="dashboard.status"/></dd>
                                        <dt><spring:theme code="text.account.quote.status.display.${fn:escapeXml(quote.status)}"/></dt>
                                    </dl>
                                </div>
                                <div class="col-xs-8">
                                    <c:choose>
                                        <c:when test="${(quote.status eq 'BUYER_ACCEPTED') || (quote.status eq 'BUYER_ORDERED')}">
                                            <span class="dashboard__icon glyphicon glyphicon-ok">
                                        </c:when>
                                        <c:when test="${(quote.status eq 'CREATED') || (quote.status eq 'SELLERAPPROVER_DRAFT') || (quote.status eq 'SELLER_DRAFT') || (quote.status eq 'BUYER_DRAFT')}">
                                            <span class="dashboard__icon glyphicon glyphicon-shopping-cart">
                                        </c:when>
                                        <c:when test="${(quote.status eq 'BUYER_APPROVED') || (quote.status eq 'SELLERAPPROVER_APPROVED')}">
                                            <span class="dashboard__icon glyphicon glyphicon-thumbs-up">
                                        </c:when>
                                        <c:when test="${(quote.status eq 'CANCELLED') || (quote.status eq 'EXPIRED') || (quote.status eq 'BUYER_REJECTED') || (quote.status eq 'SELLERAPPROVER_REJECTED')}">
                                            <span class="dashboard__icon glyphicon glyphicon-thumbs-down">
                                        </c:when>
                                        <c:otherwise>
                                        	<span class="dashboard__icon glyphicon glyphicon-folder-open">
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                </c:forEach>
            </div>
        </c:otherwise>
    </c:choose>
</div>