<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
 
<c:if test="${not empty balances}">
	<div class="dashboard dashboard-balance">
		<h2><spring:theme code="dashboard.balances"/></h2>
		<div class="dashboard__container balance-container">
			<c:forEach items="${balances}" var="balance">
			  <c:url var="balanceDetailsUrl" value="/my-company/organization-management/accountsummary-unit/details?unit=${balance.unitUid}"/>
			  <a href="${balanceDetailsUrl}">
				<c:set var="openBalanceLabel"><spring:theme code="dashboard.balance.open"/></c:set>
				<c:set var="currentBalanceLabel"><spring:theme code="dashboard.balance.current"/></c:set>
				<c:set var="pastDueBalanceLabel"><spring:theme code="dashboard.balance.pastDue"/></c:set>
				<div class="dashboard__chart-container balance">
					<canvas class="js-balance-chart"
						data-unit-name="${balance.unitName}"
						data-open-balance-value="${balance.openBalance.value}"
						data-current-balance-value="${balance.currentBalance.value}"
						data-pastdue-balance-value="${balance.pastDueBalance.value}"
						data-open-balance-label="${openBalanceLabel}"
						data-current-balance-label="${currentBalanceLabel}"
						data-pastdue-balance-label="${pastDueBalanceLabel}"
						data-open-balance-formatted="${balance.openBalance.formattedValue}"
						data-current-balance-formatted="${balance.currentBalance.formattedValue}"
						data-pastdue-balance-formatted="${balance.pastDueBalance.formattedValue}">
					</canvas>
				</div>
			  </a>
			</c:forEach>
		</div>
	</div>
</c:if>
