<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
 
<c:if test="${not empty budgets}">
	<div class="dashboard dashboard-budget">
		<h2><spring:theme code="dashboard.budgets"/></h2>
		<div class="dashboard__container cost-center-container">
		    <c:forEach items="${budgets}" var="budget">
		      <c:url var="budgetDetailsUrl" value="/my-company/organization-management/manage-costcenters/view/?costCenterCode=${budget.costCenterCode}"/>
			   <a href="${budgetDetailsUrl}">
				<div class="dashboard__chart-container cost-center">
					<div class="dashboard__cost-center-content">
						<div class="dashboard__cost-center-title"><spring:theme code="dashboard.budget.costCenter.title"/>&nbsp;${budget.costCenterName}</div>
						<div class="dashboard__cost-center-remaining"><spring:theme code="dashboard.budget.costCenter.remainingBudget"/>&nbsp;<span class="${budget.remainingBudget.value ge 0 ? 'dashboard__budget-under' : 'dashboard__budget-over' }">${budget.remainingBudget.formattedValue}</span></div>
						<c:set var="totalLabel"><spring:theme code="dashboard.budget.costCenter.budget"/></c:set>
						<c:set var="spendLabel"><spring:theme code="dashboard.budget.costCenter.spend"/></c:set>
						<canvas class="js-budget-chart" 
							data-total-value="${budget.totalBudget.value}" 
							data-spend-value="${budget.totalSpent.value}"
							data-total-label="${totalLabel}"
							data-spend-label="${spendLabel}"
							data-total-formatted="${budget.totalBudget.formattedValue}"
							data-spend-formatted="${budget.totalSpent.formattedValue}">
						</canvas>
					</div>
				</div>
			  </a>	        
		    </c:forEach>
	    </div>
	</div>
</c:if>