<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
 
<template:page pageTitle="${pageTitle}">
	<h1 class="dashboard__title">${cmsPage.title}</h1>
    <cms:pageSlot position="Section1" var="feature" element="div" class="dashboard__components">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
</template:page>