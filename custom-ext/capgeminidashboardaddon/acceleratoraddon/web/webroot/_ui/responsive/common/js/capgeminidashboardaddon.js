ACC.dashboard = {
		_autoload: [
			["init", $('.dashboard-budget').length !== 0]
		],
		
		init: function() {
			$('.js-budget-chart').each(function() {
				var $budgetChart = $(this);
				var totalValue = $budgetChart.data('total-value');
				var spendValue = $budgetChart.data('spend-value');
				var totalLabel = $budgetChart.data('total-label');
				var spendLabel = $budgetChart.data('spend-label');
				var totalFormatted = $budgetChart.data('total-formatted');
				var spendFormatted = $budgetChart.data('spend-formatted');
				var tooltipLabels = [totalFormatted, spendFormatted];
				var ctx = $budgetChart[0].getContext('2d');
				new Chart(ctx, {
					type: 'bar',
					data: {
						labels: [totalLabel, spendLabel],
						datasets: [{
							data: [totalValue, spendValue],
							backgroundColor: [
								'rgba(201, 203, 207, 0.2)',
								'rgba(255, 99, 132, 0.2)'
							],
							borderColor: [
								'rgba(201, 203, 207, 1)',
								'rgba(255, 99, 132, 1)'
							],
							borderWidth: 1
						}]
					},
					options: {
						scales: {
							yAxes: [{
								display: false,
								gridLines: {
									display: false
								}
							}],
							xAxes : [{
								gridLines: {
									display: false
								}
							}]
						},
						legend: {
							display: false
						},
						tooltips: {
							callbacks: {
								label: function(tooltipItem, data) {
									return tooltipLabels[tooltipItem.index];
								}
							}
						}
					}
				});
			});
			
			$('.js-balance-chart').each(function() {
				var $balanceChart = $(this);
				var unitName = $balanceChart.data('unit-name');
				var openBalanceValue = $balanceChart.data('open-balance-value');
				var currentBalanceValue = $balanceChart.data('current-balance-value');
				var pastDueBalanceValue = $balanceChart.data('pastdue-balance-value');
				var openBalanceLabel = $balanceChart.data('open-balance-label');
				var currentBalanceLabel = $balanceChart.data('current-balance-label');
				var pastDueBalanceLabel = $balanceChart.data('pastdue-balance-label');
				var currentBalanceFormatted = $balanceChart.data('current-balance-formatted');
				var openBalanceFormatted = $balanceChart.data('open-balance-formatted');
				var pastDueBalanceFormatted = $balanceChart.data('pastdue-balance-formatted');
				var tooltipLabels = [pastDueBalanceFormatted, currentBalanceFormatted, openBalanceFormatted];
				var tooltipTitles = [pastDueBalanceLabel, currentBalanceLabel, openBalanceLabel];
				var ctx = $balanceChart[0].getContext('2d');
				new Chart(ctx, {
					type: 'bar',
					data: {
						labels: [unitName],
						datasets: [{
							data: [pastDueBalanceValue],
							label: pastDueBalanceLabel,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)'
							],
							borderColor: [
								'rgba(255, 99, 132, 1)'
							],
							borderWidth: 1
						}, {
							data: [currentBalanceValue],
							label: currentBalanceLabel,
							backgroundColor: [
								'rgba(0, 204, 102, 0.2)'
							],
							borderColor: [
								'rgba(0, 204, 102, 1)'
							],
							borderWidth: 1
						}, {
							data: [openBalanceValue],
							label: openBalanceLabel,
							backgroundColor: [
								'rgba(201, 203, 207, 0.2)'
							],
							borderColor: [
								'rgba(201, 203, 207, 1)'
							],
							borderWidth: 1
						}]
					},
					options: {
						scales: {
							yAxes: [{
								display: false,
								gridLines: {
									display: false
								},
								stacked: true,
								ticks: {
									beginAtZero: true
								}
							}],
							xAxes : [{
								gridLines: {
									display: false
								},
								stacked: true
							}]
						},
						maintainAspectRatio : false,
						legend: {
							verticalAlign: 'top',
							horizontalAlign: 'right'
						},
						tooltips: {
							callbacks: {
								label: function(tooltipItem, data) {
									return tooltipLabels[tooltipItem.datasetIndex];
								},
								title: function(tooltipItem, data) {
									return tooltipTitles[tooltipItem[0].datasetIndex];
								}
							}
						}
					}
				});
				
			});
		}

}