/**
 *
 */
package com.lyonscg.b2bsecureportaladdon.forms;

import org.hibernate.validator.constraints.NotEmpty;


/** @author lyonscg.
 * implementation of country registration form.
 */
public class CountryRegistrationForm
{
	private String companyAddressCountryIso;

	/**
	 * @return the companyAddressCountryIso
	 */
	@NotEmpty(message = "{text.secureportal.register.field.mandatory}")
	public String getCompanyAddressCountryIso()
	{
		return companyAddressCountryIso;
	}

	/**
	 * @param companyAddressCountryIso
	 *           the companyAddressCountryIso to set
	 */
	public void setCompanyAddressCountryIso(final String companyAddressCountryIso)
	{
		this.companyAddressCountryIso = companyAddressCountryIso;
	}
}
