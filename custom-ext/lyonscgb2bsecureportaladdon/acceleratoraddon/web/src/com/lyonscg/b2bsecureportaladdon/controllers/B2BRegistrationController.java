/**
 *
 */
package com.lyonscg.b2bsecureportaladdon.controllers;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.lyonscg.b2bsecureportaladdon.constants.Lyonscgb2bsecureportaladdonWebConstants;
import com.lyonscg.b2bsecureportaladdon.facades.StateEnabledB2BRegistrationFacade;
import com.lyonscg.b2bsecureportaladdon.forms.StateEnabledRegistrationForm;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.secureportaladdon.data.B2BRegistrationData;
import de.hybris.platform.secureportaladdon.exceptions.CustomerAlreadyExistsException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.localization.Localization;


/**@author lyonscg.
 *Registering B2BController.
 */
@Controller
@RequestMapping(value = Lyonscgb2bsecureportaladdonWebConstants.RequestMappings.ACCOUNT_REGISTRATION)
public class B2BRegistrationController extends AbstractB2BRegistrationController
{

    private final static class MessageKeys
    {
        public static final String REGISTER_SUBMIT_CONFIRMATION = "text.secureportal.register.submit.confirmation";
        public static final String REGISTER_ACCOUNT_EXISTING = "text.secureportal.register.account.existing";
        public static final String SCP_LINK_CREATE_ACCOUNT = "text.secureportal.link.createAccount";
    }

    private static final String HOME_REDIRECT = REDIRECT_PREFIX + ROOT;

    @Resource(name = "stateEnabledB2BRegistrationFacade")
    private StateEnabledB2BRegistrationFacade b2bRegistrationFacade;

    @Resource(name = "modelService")
    private ModelService modelService;

    @RequestMapping(method = RequestMethod.GET)
    public String showRegistrationPage(final HttpServletRequest request, final Model model) throws CMSItemNotFoundException
    {
        if (getCmsSiteService().getCurrentSite().isEnableRegistration())
        {
            final ContentPageModel registerContent = getContentPageForLabelOrId(getRegistrationCmsPage());
            storeCmsPageInModel(model, registerContent);
            setUpMetaDataForContentPage(model, registerContent);
            //			return getViewForPage(model);
            return getDefaultRegistrationPage(model, registerContent);
        }
        return HOME_REDIRECT;
    }

    @RequestMapping(value = "/registerform", method = RequestMethod.GET)
    public String getCountryAddressForm(@RequestParam(value = "countryIsoCode", required = false) final String countryIsoCode,
            final Model model)
    {
        if (StringUtils.isEmpty(countryIsoCode))
        {
            return Lyonscgb2bsecureportaladdonWebConstants.RequestMappings.ACCOUNT_REGISTRATION;
        }
        final List<CountryData> countries = getCountries();
        if (CollectionUtils.isEmpty(countries))
        {
            return Lyonscgb2bsecureportaladdonWebConstants.RequestMappings.ACCOUNT_REGISTRATION;
        }
        model.addAttribute("countries", getCountries());
        model.addAttribute("selectedCountryIsoCode", countryIsoCode);
        model.addAttribute("states", b2bRegistrationFacade.getRegionsDataForCountryIso(countryIsoCode));
        final StateEnabledRegistrationForm form = new StateEnabledRegistrationForm();
        form.setCompanyAddressCountryIso(countryIsoCode);
        model.addAttribute("registerForm", form);
        return Lyonscgb2bsecureportaladdonWebConstants.Views.RegisterCountryAddressForm;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String submitRegistration(@Valid final StateEnabledRegistrationForm form, final BindingResult bindingResult,
            final Model model, final HttpServletRequest request, final HttpSession session, final RedirectAttributes redirectModel)
            throws CMSItemNotFoundException
    {

        populateModelCmsContent(model, getContentPageForLabelOrId(getRegistrationCmsPage()));
        model.addAttribute(form);

        if (bindingResult.hasErrors())
        {
            return getRegistrationView();
        }

        try
        {
            b2bRegistrationFacade.register(convertFormToData(form));
        }
        catch (final CustomerAlreadyExistsException e)
        {
            GlobalMessages.addErrorMessage(model, Localization.getLocalizedString(MessageKeys.REGISTER_ACCOUNT_EXISTING));
            return getRegistrationView();
        }

        GlobalMessages.addInfoMessage(model, Localization.getLocalizedString(MessageKeys.REGISTER_SUBMIT_CONFIRMATION));

        return getDefaultLoginPage(false, session, model);

    }

    /**
     * @param form
     *            Form data as submitted by user
     * @return A DTO object built from the form instance
     */
    private B2BRegistrationData convertFormToData(final StateEnabledRegistrationForm form)
    {
        final B2BRegistrationData registrationData = new B2BRegistrationData();
        BeanUtils.copyProperties(form, registrationData);
        //convert email address to be lowercase
        registrationData.setEmail(registrationData.getEmail().toLowerCase(Locale.getDefault()));
        return registrationData;
    }

    @Override
    protected String getRegistrationView()
    {
        return Lyonscgb2bsecureportaladdonWebConstants.Views.REGISTRATION_PAGE;
    }

    @Override
    protected String getRegistrationCmsPage()
    {
        return Lyonscgb2bsecureportaladdonWebConstants.CMS_REGISTER_PAGE_NAME;
    }

    @Override
    protected void populateModelCmsContent(final Model model, final ContentPageModel contentPageModel)
    {

        storeCmsPageInModel(model, contentPageModel);
        setUpMetaDataForContentPage(model, contentPageModel);

        final Breadcrumb registrationBreadcrumbEntry = new Breadcrumb("#", getMessageSource().getMessage(
                MessageKeys.SCP_LINK_CREATE_ACCOUNT, null, getI18nService().getCurrentLocale()), null);
        model.addAttribute("breadcrumbs", Collections.singletonList(registrationBreadcrumbEntry));

    }

    @Override
    protected String getView()
    {
        // According to Lyonscgb2bacceleratoraddon, we want to override this getView method
        return "pages/account/accountLoginPage";
    }

    @Override
    protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
    {
        return getContentPageForLabelOrId("login");
    }

    @Override
    protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
    {
        return HOME_REDIRECT;
    }

    //	@ModelAttribute("states")
    //	public Map<String, List<RegionData>> getCountryRegionDatamap()
    //	{
    //		return b2bRegistrationFacade.getCountryRegionDataMap();
    //	}

}
