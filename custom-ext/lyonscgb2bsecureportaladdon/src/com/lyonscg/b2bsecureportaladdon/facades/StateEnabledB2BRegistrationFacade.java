/**
 *
 */
package com.lyonscg.b2bsecureportaladdon.facades;

import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.secureportaladdon.facades.impl.DefaultB2BRegistrationFacade;

import java.util.List;
import java.util.Map;


/**@author lyonscg.
 * creating abstract class state enabled B2BRegistration Facade.
 */
public abstract class StateEnabledB2BRegistrationFacade extends DefaultB2BRegistrationFacade
{
	public abstract Map<String, List<RegionData>> getCountryRegionDataMap();

	public abstract List<RegionData> getRegionsDataForCountryIso(String countryIsoCode);
}
