/**
 *
 */
package com.lyonscg.b2bsecureportaladdon.facades.impl;

import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.RegionData;

import java.util.List;
import java.util.Map;

import com.lyonscg.b2bsecureportaladdon.facades.StateEnabledB2BRegistrationFacade;


/**@author lyonscg.
 *Implementation of Default state enabled b2b registration.
 */
public class DefaultStateEnabledB2BRegistrationFacade extends StateEnabledB2BRegistrationFacade
{
	//@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.lyonscg.b2bsecureportaladdon.facades.StateEnabledB2BRegistrationFacade#getCountryRegionDataMap()
	 */

	@Override
	public Map<String, List<RegionData>> getCountryRegionDataMap()
	{
		if(i18NFacade != null){
		return i18NFacade.getRegionsForAllCountries();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.lyonscg.b2bsecureportaladdon.facades.StateEnabledB2BRegistrationFacade#getRegionDataForCountry(java.lang.String
	 * )
	 */
	@Override
	public List<RegionData> getRegionsDataForCountryIso(final String countryIsoCode)
	{
		if(i18NFacade != null){
		return i18NFacade.getRegionsForCountryIso(countryIsoCode);
		}
		return null;
	}


	/**
	 * @return the i18NFacade
	 */
	public I18NFacade getI18NFacade()
	{
		return i18NFacade;
	}

	/**
	 * @param i18nFacade
	 *           the i18NFacade to set
	 */
	public void setI18NFacade(final I18NFacade i18nFacade)
	{
		i18NFacade = i18nFacade;
	}



}
