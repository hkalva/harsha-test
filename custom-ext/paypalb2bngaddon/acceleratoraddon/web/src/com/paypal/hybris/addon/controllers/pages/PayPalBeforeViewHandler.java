package com.paypal.hybris.addon.controllers.pages;

import de.hybris.platform.addonsupport.interceptors.BeforeViewHandlerAdaptee;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.ui.ModelMap;

import com.paypal.hybris.addon.constants.Paypalb2bngaddonConstants;
import com.paypal.hybris.addon.controllers.Paypalb2bngaddonControllerConstants;
import com.paypal.hybris.addon.controllers.utils.PayPalUserHelper;
import com.paypal.hybris.constants.PaypalConstants;


public class PayPalBeforeViewHandler implements BeforeViewHandlerAdaptee
{
	private static final String ADDON_PREFIX_B2B = "addon:/b2bacceleratoraddon/";
	private static final String B2B_CHECKOUT_CONFIRMATION_PAGE = ADDON_PREFIX_B2B + "pages/checkout/checkoutConfirmationPage";
	private static final String ADD_TO_CART_POPUP_PAGE = "fragments/cart/addToCartPopup";
	private static final String B2B_SILENT_ORDER_POST_PAGE = "pages/checkout/multi/silentOrderPostPage";
	private static final String B2B_CHECKOUT_SUMMARY_PAGE = ADDON_PREFIX_B2B + "pages/checkout/multi/checkoutSummaryPage";

	private static final String CART_PAGE = ADDON_PREFIX_B2B + "pages/cart/cartPage";

	private SessionService sessionService;
	private CMSComponentService cmsComponentService;
	private ConfigurationService configurationService;
	private PayPalUserHelper payPalUserHelper;

	@Override
	public String beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
			final String viewName) throws Exception
	{
		if (B2B_SILENT_ORDER_POST_PAGE.equals(viewName))
		{
			return Paypalb2bngaddonControllerConstants.Views.Pages.MultiStepCheckout.SilentOrderPostPage;
		}
		else if (B2B_CHECKOUT_SUMMARY_PAGE.equals(viewName))
		{
			final String repeatRedirectUrl = getSessionService()
					.getAttribute((Paypalb2bngaddonConstants.PAY_PAL_REPEAT_REDIRECT_URL));
			if (StringUtils.isNotBlank(repeatRedirectUrl))
			{
				getSessionService().removeAttribute(Paypalb2bngaddonConstants.PAY_PAL_REPEAT_REDIRECT_URL);
				return "redirect:" + repeatRedirectUrl;
			}
			else
			{
				return Paypalb2bngaddonControllerConstants.Views.Pages.MultiStepCheckout.CheckoutSummaryPage;
			}
		}
		else if (B2B_CHECKOUT_CONFIRMATION_PAGE.equals(viewName))
		{
			return Paypalb2bngaddonControllerConstants.Views.Pages.Checkout.CheckoutConfirmationPage;
		}
		else if (ADD_TO_CART_POPUP_PAGE.equals(viewName))
		{
			model.addAttribute("payPalExpressCheckoutShortcut",
					getCmsComponentService().getSimpleCMSComponent("PayPalExpressCheckoutShortcutSmall"));
			model.addAttribute("payPalCreditShortcut", getCmsComponentService().getSimpleCMSComponent("PayPalCreditShortcutSmall"));
			return Paypalb2bngaddonControllerConstants.Views.Fragments.Cart.AddToCartPopup;
		}
		else if (isCheckoutPage(viewName) || isPayPalCheckoutRedirect(viewName, request))
		{
			final String payPalHopUrl = getSessionService().getAttribute(Paypalb2bngaddonControllerConstants.PAY_PAL_HOP_URL_ATTR);
			if (StringUtils.isNotBlank(payPalHopUrl))
			{
				getSessionService().removeAttribute(Paypalb2bngaddonControllerConstants.PAY_PAL_HOP_URL_ATTR);
				return "redirect:" + payPalHopUrl;
			}
		}

		if (!StringUtils.contains(viewName, Paypalb2bngaddonControllerConstants.Views.Pages.Checkout.CheckoutLoginPage)
				&& !StringUtils.contains(viewName, CART_PAGE))
		{
			getSessionService().removeAttribute(Paypalb2bngaddonControllerConstants.PAY_PAL_HOP_URL_ATTR);

		}
		if (request.getSession().getAttribute(PaypalConstants.USE_EASY_PAYMENT) == null)
		{
			if (Boolean.TRUE.toString()
					.equalsIgnoreCase(getConfigurationService().getConfiguration().getString(PaypalConstants.USE_EASY_PAYMENT)))
			{
				request.getSession().setAttribute(PaypalConstants.USE_EASY_PAYMENT, Boolean.TRUE);
			}
			else
			{
				request.getSession().setAttribute(PaypalConstants.USE_EASY_PAYMENT, Boolean.FALSE);
			}
		}
		return viewName;
	}

	private boolean isCheckoutPage(final String viewName)
	{
		boolean result;
		result = !Paypalb2bngaddonControllerConstants.Views.Pages.Checkout.CheckoutLoginPage.equals(viewName)
				&& StringUtils.contains(viewName, Paypalb2bngaddonControllerConstants.CHECKOUT_PAGE_VIEW_NAME_FRAGMENT);

		return result;

	}

	private boolean isPayPalCheckoutRedirect(final String viewName, final HttpServletRequest request)
	{
		return StringUtils.contains(viewName, CART_PAGE);
	}



	public SessionService getSessionService()
	{
		return sessionService;
	}

	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public CMSComponentService getCmsComponentService()
	{
		return cmsComponentService;
	}

	public void setCmsComponentService(final CMSComponentService cmsComponentService)
	{
		this.cmsComponentService = cmsComponentService;
	}

	public PayPalUserHelper getPayPalUserHelper()
	{
		return payPalUserHelper;
	}

	public void setPayPalUserHelper(final PayPalUserHelper payPalUserHelper)
	{
		this.payPalUserHelper = payPalUserHelper;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
