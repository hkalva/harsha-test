package com.paypal.hybris.addon.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.util.Config;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.common.collect.Lists;
import com.paypal.hybris.addon.constants.Paypalb2bngaddonConstants;


@SystemSetup(extension = Paypalb2bngaddonConstants.EXTENSIONNAME)
public class PaypalSystemSetup extends AbstractSystemSetup
{

	private static final Logger LOG = Logger.getLogger(PaypalSystemSetup.class);

	private static final String DEFAULT_UI_EXPERIENCE = "commerceservices.default.desktop.ui.experience";
	private static final String RESPONSIVE = "responsive";
	private static final String IMPORT_PAY_PAL_PARAMETER_NAME = "Import Pay Pal content";
	private static final String IMPORT_PAY_PAL_PARAMETER_VALUE = "ImportPayPalCheckoutButton";
	private static final String IMPORT_PAY_PAL_POWERTOOLS_RESPONSIVE_FILE_PATH = "/paypalb2bngaddon/import/contentCatalogs/powertoolsContentCatalog/cms-content-responsive.impex";
	private static final String IMPORT_PAY_PAL_POWERTOOLS_DESKTOP_FILE_PATH = "/paypalb2bngaddon/import/contentCatalogs/powertoolsContentCatalog/cms-content.impex";
	private static final String IMPORT_PAY_PAL_POWERTOOLS_CATALOG_NAME = "powertools";

	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = Lists.newArrayList();
		params.add(createBooleanSystemSetupParameter(IMPORT_PAY_PAL_PARAMETER_VALUE, IMPORT_PAY_PAL_PARAMETER_NAME, false));
		return params;
	}

	@SystemSetup(type = SystemSetup.Type.ALL, process = SystemSetup.Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		final boolean importPayPalContent = getBooleanSystemSetupParameter(context, IMPORT_PAY_PAL_PARAMETER_VALUE);
		LOG.info("[Import pay pal content] set as: " + importPayPalContent);
		if (importPayPalContent)
		{
			if (isResponsive())
			{
				importImpexFile(context, IMPORT_PAY_PAL_POWERTOOLS_RESPONSIVE_FILE_PATH, true);
			}
			else
			{
				importImpexFile(context, IMPORT_PAY_PAL_POWERTOOLS_DESKTOP_FILE_PATH, true);
			}
			synchronizeContentCatalog(context, IMPORT_PAY_PAL_POWERTOOLS_CATALOG_NAME, true);
		}
	}

	private boolean synchronizeContentCatalog(final SystemSetupContext context, final String catalogName,
			final boolean syncCatalogs)
	{
		logInfo(context, String.format("Begin synchronizing Content Catalog [%s]", catalogName));
		getSetupSyncJobService().createContentCatalogSyncJob(String.format("%sContentCatalog", catalogName));
		if (syncCatalogs)
		{
			final PerformResult syncCronJobResult = getSetupSyncJobService().executeCatalogSyncJob(
					String.format("%sContentCatalog", catalogName));
			if (isSyncRerunNeeded(syncCronJobResult))
			{
				logInfo(context, String.format("Content Catalog [%s] sync has issues.", catalogName));
				return false;
			}
		}
		return true;
	}

	private boolean isResponsive()
	{
		if (StringUtils.isBlank(Config.getParameter(DEFAULT_UI_EXPERIENCE)))
		{
			return false;
		}
		return StringUtils.equalsIgnoreCase(Config.getParameter(DEFAULT_UI_EXPERIENCE), RESPONSIVE);
	}

}
