/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.capgemini.secureportaladdon.controllers;

/**
 */
public interface CapgeminisecureportaladdonControllerConstants
{
    // implement here controller constants used by this extension
    /**
     * List of any new request URIs defined and handled in this add on.
     */
    public static final class RequestMappings
    {

        public static final String ACCOUNT_REGISTRATION = "/register";
        public static final String HOME = "/";

        private RequestMappings()
        {
        }
    }
}
