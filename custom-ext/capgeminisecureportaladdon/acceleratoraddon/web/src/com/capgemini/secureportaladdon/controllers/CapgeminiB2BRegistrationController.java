/**
 *
 */
package com.capgemini.secureportaladdon.controllers;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.secureportaladdon.controllers.B2BRegistrationController;
import de.hybris.platform.secureportaladdon.exceptions.CustomerAlreadyExistsException;
import de.hybris.platform.secureportaladdon.facades.B2BRegistrationFacade;
import de.hybris.platform.secureportaladdon.forms.RegistrationForm;
import de.hybris.platform.util.localization.Localization;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * Registration page controller: Handles Get and Post request and dispatches relevant wortkflow facades and necessary
 * services.
 */
@RequestMapping(value = CapgeminisecureportaladdonControllerConstants.RequestMappings.ACCOUNT_REGISTRATION)
public class CapgeminiB2BRegistrationController extends B2BRegistrationController
{
    private static final Logger LOG = LoggerFactory.getLogger(CapgeminiB2BRegistrationController.class);

    private static final String REGISTER_SUBMIT_CONFIRMATION = "text.secureportal.register.submit.confirmation";
    private static final String REGISTER_ACCOUNT_EXISTING = "text.secureportal.register.account.existing";

    @Resource(name = "b2bRegistrationFacade")
    private B2BRegistrationFacade b2bRegistrationFacade;

    @Override
    @RequestMapping(method = RequestMethod.POST)
    public String submitRegistration(final RegistrationForm form, final BindingResult bindingResult, final Model model,
            final HttpServletRequest request, final HttpSession session, final RedirectAttributes redirectModel)
            throws CMSItemNotFoundException
    {

        populateModelCmsContent(model, getContentPageForLabelOrId(getRegistrationCmsPage()));
        model.addAttribute(form);

        getRegistrationValidator().validate(form, bindingResult);
        if (bindingResult.hasErrors())
        {
            return getRegistrationView();
        }

        try
        {
            b2bRegistrationFacade.register(convertFormToData(form));
        }
        catch (final CustomerAlreadyExistsException e) //NOSONAR
        {
            LOG.error("Failed to register account. Account already exists.");
            GlobalMessages.addErrorMessage(model, Localization.getLocalizedString(REGISTER_ACCOUNT_EXISTING));
            return getRegistrationView();
        }

        GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER,
                Localization.getLocalizedString(REGISTER_SUBMIT_CONFIRMATION));

        return REDIRECT_PREFIX + "/login";
    }
}
