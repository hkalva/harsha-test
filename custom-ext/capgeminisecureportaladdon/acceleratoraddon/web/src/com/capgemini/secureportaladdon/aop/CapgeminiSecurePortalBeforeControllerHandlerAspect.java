/**
 *
 */
package com.capgemini.secureportaladdon.aop;

import de.hybris.platform.acceleratorcms.data.CmsPageRequestContextData;
import de.hybris.platform.acceleratorcms.services.CMSPageContextService;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.PredicateUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author lyonscg
 *
 */
public class CapgeminiSecurePortalBeforeControllerHandlerAspect
{

    /**
     * the logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(CapgeminiSecurePortalBeforeControllerHandlerAspect.class);

    /**
     * the cms page context service.
     */
    private CMSPageContextService cmsPageContextService;

    /**
     * intercept calls to SecurePortalBeforeControllerHandler's beforeController method. check first if page is being
     * viewed in Smart Edit. if in preview mode, return true, otherwise call the intercepted method.
     *
     * @param pjp
     *            - the proceeding join point.
     * @return Object
     * @throws Throwable
     */
    public Object execute(final ProceedingJoinPoint pjp) throws Throwable
    {
        final List<Object> args = Arrays.asList(pjp.getArgs());
        final HttpServletRequest request = (HttpServletRequest) CollectionUtils.find(args,
                PredicateUtils.instanceofPredicate(HttpServletRequest.class));
        final CmsPageRequestContextData cmsPageRequestContextData = getCmsPageContextService()
                .initialiseCmsPageContextForRequest(request);

        if ((cmsPageRequestContextData != null) && cmsPageRequestContextData.isPreview())
        {
            LOG.debug("Page in preview mode");
            return Boolean.TRUE;
        }

        return pjp.proceed();
    }

    /**
     *
     * @param cmsPageContextService
     *            - the cms page context service.
     */
    @Required
    public void setCmsPageContextService(final CMSPageContextService cmsPageContextService)
    {
        this.cmsPageContextService = cmsPageContextService;
    }

    /**
     *
     * @return CMSPageContextService
     */
    protected CMSPageContextService getCmsPageContextService()
    {
        return cmsPageContextService;
    }
}
