package com.paypal.hybris.cscockpit.widgets.controllers.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.widgets.controllers.impl.DefaultOrderManagementActionsWidgetController;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.paypal.hybris.cscockpit.widgets.controllers.PayPalOrderManagementActionsWidgetController;
import com.paypal.hybris.reauthorization.PayPalOrderReauthorizationService;


public class PayPalOrderManagementActionsWidgetControllerImpl extends DefaultOrderManagementActionsWidgetController
		implements PayPalOrderManagementActionsWidgetController
{

	private static final Logger LOG = Logger.getLogger(PayPalOrderManagementActionsWidgetControllerImpl.class);

	private PayPalOrderReauthorizationService payPalOrderReauthorizationService;

	@Override
	public boolean isReauthorizationPossible()
	{
		final TypedObject order = getOrder();
		if ((order != null) && (order.getObject() instanceof OrderModel)
				&& (StringUtils.isBlank(((OrderModel) order.getObject()).getVersionID())))
		{
			try
			{
				final OrderModel orderModel = (OrderModel) order.getObject();
				return getPayPalOrderReauthorizationService().isReauthorizationPossible(orderModel);
			}
			catch (final Exception e)
			{
				LOG.error("failed to work out if reauthorization of order is possible", e);
			}
		}
		return false;
	}

	@Override
	public boolean isMultiCapturingPossible()
	{
		final OrderModel order = getOrderIfExist();
		if (order != null)
		{
			final boolean isCapturingPossible = OrderStatus.COMPLETED.equals(order.getStatus())
					|| OrderStatus.PAYMENT_CAPTURED.equals(order.getStatus());
			return isCapturingPossible;
		}

		return false;
	}

	@Override
	public boolean isPartialRefundPossible()
	{

		final OrderModel order = getOrderIfExist();
		if (order != null)
		{
			final boolean isRefundPossible = (OrderStatus.COMPLETED.equals(order.getStatus())
					|| OrderStatus.PAYMENT_CAPTURED.equals(order.getStatus())) && (order.getSubtotal().doubleValue() != 0);

			return !isRefundPossible;
		}

		return false;

	}

	private OrderModel getOrderIfExist()
	{
		OrderModel order = null;

		final TypedObject orderType = getOrder();
		if ((orderType != null) && (orderType.getObject() instanceof OrderModel)
				&& (StringUtils.isBlank(((OrderModel) orderType.getObject()).getVersionID())))
		{
			order = (OrderModel) orderType.getObject();
		}

		return order;
	}

	public PayPalOrderReauthorizationService getPayPalOrderReauthorizationService()
	{
		return payPalOrderReauthorizationService;
	}

	public void setPayPalOrderReauthorizationService(final PayPalOrderReauthorizationService payPalOrderReauthorizationService)
	{
		this.payPalOrderReauthorizationService = payPalOrderReauthorizationService;
	}

}
