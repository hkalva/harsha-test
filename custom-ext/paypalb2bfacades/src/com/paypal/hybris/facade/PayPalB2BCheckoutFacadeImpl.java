/**
 *
 */
package com.paypal.hybris.facade;

import de.hybris.platform.b2bacceleratorfacades.order.impl.DefaultB2BAcceleratorCheckoutFacade;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.core.model.order.CartModel;

import javax.annotation.Resource;


public class PayPalB2BCheckoutFacadeImpl extends DefaultB2BAcceleratorCheckoutFacade implements PayPalB2BCheckoutFacade
{
	@Resource(name = "payPalCheckoutFacade")
	private PayPalCheckoutFacade payPalCheckoutFacade;

	@Override
	public boolean authorizePayment(final String securityCode)
	{
		final CartModel cart = getCartService().getSessionCart();
		if (CheckoutPaymentType.ACCOUNT.equals(cart.getPaymentType()))
		{
			return super.authorizePayment(securityCode);
		}
		return payPalCheckoutFacade.authorizePayment(securityCode);
	}

	@Override
	protected CCPaymentInfoData getPaymentDetails()
	{
		return payPalCheckoutFacade.getPaymentDetails();
	}

	@Override
	public boolean setPaymentDetails(final String paymentInfoId)
	{
		return payPalCheckoutFacade.setPaymentDetails(paymentInfoId);
	}

	@Override
	public void setCardPaymentType()
	{
		final CartModel cart = getCart();
		cart.setPaymentType(CheckoutPaymentType.CARD);
		getModelService().save(cart);
	}
}
