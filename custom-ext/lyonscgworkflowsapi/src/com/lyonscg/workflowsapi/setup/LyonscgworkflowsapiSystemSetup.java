/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.lyonscg.workflowsapi.setup;

import static com.lyonscg.workflowsapi.constants.LyonscgworkflowsapiConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import com.lyonscg.workflowsapi.constants.LyonscgworkflowsapiConstants;
import com.lyonscg.workflowsapi.service.LyonscgworkflowsapiService;


@SystemSetup(extension = LyonscgworkflowsapiConstants.EXTENSIONNAME)
public class LyonscgworkflowsapiSystemSetup
{
	private final LyonscgworkflowsapiService lyonscgworkflowsapiService;

	public LyonscgworkflowsapiSystemSetup(final LyonscgworkflowsapiService lyonscgworkflowsapiService)
	{
		this.lyonscgworkflowsapiService = lyonscgworkflowsapiService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		lyonscgworkflowsapiService.createLogo(PLATFORM_LOGO_CODE);
	}

}
