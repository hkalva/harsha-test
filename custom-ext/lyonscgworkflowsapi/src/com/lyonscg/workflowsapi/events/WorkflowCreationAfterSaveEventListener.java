/**
 *
 */
package com.lyonscg.workflowsapi.events;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.AfterSaveEvent;
import de.hybris.platform.tx.AfterSaveListener;

import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import com.lyonscg.workflowsapi.service.WorkflowTriggerService;


/**
 * After Save Event Listener to capture all the UPDATE and SAVE requests matching {@link #workflowTriggerServiceMap}.
 */
public class WorkflowCreationAfterSaveEventListener implements AfterSaveListener
{
	private ModelService modelService;
	private Map<Integer, WorkflowTriggerService> workflowTriggerServiceMap;

	/**
	 * Listens for AfterSave Events. If the typeCode of the item being saved matches the key in
	 * {@link #workflowTriggerServiceMap}, then it calls the corresponding {@link WorkflowTriggerService}.
	 *
	 * @param events
	 *           - Collection of after save events.
	 */
	@Override
	public void afterSave(final Collection<AfterSaveEvent> events)
	{
		events.stream()
				.filter(e -> (null != e.getPk() && (e.getType() == AfterSaveEvent.UPDATE || e.getType() == AfterSaveEvent.CREATE)))
				.forEach(this::findAndCallWorkflowTrigger);

	}

	/**
	 * Finds and calls the corresponding Workflow trigger service based on {@link #workflowTriggerServiceMap}
	 *
	 * @param event
	 *           - The after save events.
	 */
	private void findAndCallWorkflowTrigger(final AfterSaveEvent event)
	{
		final WorkflowTriggerService workflowTriggerService = getWorkflowTriggerServiceMap().get(
				Integer.valueOf(event.getPk().getTypeCode()));
		if (null != workflowTriggerService)
		{
			workflowTriggerService.triggerWorkflow(getModelService().get(event.getPk()));
		}
	}

	protected Map<Integer, WorkflowTriggerService> getWorkflowTriggerServiceMap()
	{
		return workflowTriggerServiceMap;
	}

	@Required
	public void setWorkflowTriggerServiceMap(final Map<Integer, WorkflowTriggerService> workflowTriggerServiceMap)
	{
		this.workflowTriggerServiceMap = workflowTriggerServiceMap;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
