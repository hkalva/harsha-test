/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.lyonscg.workflowsapi.constants;

/**
 * Global class for all Lyonscgworkflowsapi constants. You can add global constants for your extension into this class.
 */
public final class LyonscgworkflowsapiConstants extends GeneratedLyonscgworkflowsapiConstants
{
	public static final String EXTENSIONNAME = "lyonscgworkflowsapi";

	private LyonscgworkflowsapiConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "lyonscgworkflowsapiPlatformLogo";
	public static final String CONFIG_STORE_ID_PREFIX = "impex.store.prefix.id";

	// implement here constants used by this extension for Workflows
	public interface Workflows
	{
		public static final String PRODUCT_WORKFLOW_DEFAULT_EMAIL = "cockpit.product.workflow.default.email";
		public static final String PRODUCT_WORKFLOW_DEFAULT_DISPLAYNAME = "cockpit.product.workflow.default.displayname";
	}

}
