/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.lyonscg.workflowsapi.service.impl;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.workflow.WorkflowProcessingService;
import de.hybris.platform.workflow.WorkflowService;
import de.hybris.platform.workflow.WorkflowTemplateService;
import de.hybris.platform.workflow.model.WorkflowModel;
import de.hybris.platform.workflow.model.WorkflowTemplateModel;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.lyonscg.workflowsapi.data.WorkflowTriggerData;
import com.lyonscg.workflowsapi.service.WorkflowTriggerService;


/**
 * ItemModel's status(HybrisEnumValue) based Workflow trigger.
 */
public class StatusBasedWrkFlwTriggerService implements WorkflowTriggerService
{
	private ModelService modelService;
	private ConfigurationService configurationService;
	private WorkflowTemplateService workflowTemplateService;
	private WorkflowService workflowService;
	private UserService userService;
	private WorkflowProcessingService workflowProcessingService;
	private WorkflowTriggerData workflowTriggerData;
	private static final Logger LOG = Logger.getLogger(StatusBasedWrkFlwTriggerService.class);

	/**
	 * Creates workflow from template and attaches the attachment to the workflow.
	 *
	 * @param attachment
	 *           - The item to check for status and workflow creation.
	 */
	@Override
	public void triggerWorkflow(final ItemModel attachment)
	{
		if (getWorkflowTriggerData().getTriggerStatus().equals(
				getModelService().getAttributeValue(attachment, getWorkflowTriggerData().getAttribute())))
		{
			determineWorkflowTemplate(attachment);
		}
	}

	/**
	 * Determines the workflow template and updates the item with the proper status as per configuration.
	 *
	 * @param attachment
	 *           - The item to set the status and workflow creation.
	 */
	protected void determineWorkflowTemplate(final ItemModel attachment)
	{
		final String workFlowCode = getConfigurationService().getConfiguration().getString(
				getWorkflowTriggerData().getWorkflowCodeKey());
		final WorkflowTemplateModel workflowTemplate = getWorkflowTemplateService().getWorkflowTemplateForCode(workFlowCode);

		if (LOG.isDebugEnabled())
		{
			LOG.debug(String.format("Created WorkflowTemplateModel using code '%s'", workFlowCode));
		}
		//Setting it to a different status, so that the Approver changes doesn't trigger another workflow process
		getModelService().setAttributeValue(attachment, getWorkflowTriggerData().getAttribute(),
				getWorkflowTriggerData().getSetStatus());
		getModelService().save(attachment);
		// Start the workflow
		launchWorkflow(workflowTemplate, attachment);
	}

	/**
	 * Creates and starts the Workflow.
	 *
	 * @param workflowTemplateModel
	 *           - The workflowtemplate model.
	 * @param itemModel
	 *           - Attachment for the workflow.
	 */
	protected void launchWorkflow(final WorkflowTemplateModel workflowTemplateModel, final ItemModel itemModel)
	{
		final WorkflowModel workflow = getWorkflowService().createWorkflow(workflowTemplateModel, itemModel,
				getUserService().getAdminUser());
		getModelService().save(workflow);

		if (LOG.isDebugEnabled())
		{
			LOG.debug(String.format("Starting workflow for Item with Id '%s' .", itemModel.getPk()));
		}
		getWorkflowProcessingService().startWorkflow(workflow);
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	@Required
	public void setWorkflowTemplateService(final WorkflowTemplateService workflowTemplateService)
	{
		this.workflowTemplateService = workflowTemplateService;
	}

	@Required
	public void setWorkflowService(final WorkflowService workflowService)
	{
		this.workflowService = workflowService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	@Required
	public void setWorkflowProcessingService(final WorkflowProcessingService workflowProcessingService)
	{
		this.workflowProcessingService = workflowProcessingService;
	}

	@Required
	public void setWorkflowTriggerData(final WorkflowTriggerData workflowTriggerData)
	{
		this.workflowTriggerData = workflowTriggerData;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	protected WorkflowTemplateService getWorkflowTemplateService()
	{
		return workflowTemplateService;
	}

	protected WorkflowService getWorkflowService()
	{
		return workflowService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	protected WorkflowProcessingService getWorkflowProcessingService()
	{
		return workflowProcessingService;
	}

	protected WorkflowTriggerData getWorkflowTriggerData()
	{
		return workflowTriggerData;
	}

}
