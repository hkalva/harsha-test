/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.lyonscg.workflowsapi.service.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Collection;
import java.util.Collections;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Specific Workflow trigger service for CMSComponent changes. Creates a workflow for all the pages associated by the
 * component when there is a Component save and changes the all associated page status to UNAPPROVED.
 */
public class ComponentWorkflowTriggerService extends StatusBasedWrkFlwTriggerService
{
	private CMSPageService cmsPageService;
	private String workFlowEnabledKey;
	private SessionService sessionService;
	private CatalogVersionService catalogVersionService;

	/**
	 * Triggers workflow for pages associated with the component if enabled.
	 * 
	 * @param attachment
	 *           -The item responsible for the worlkfow trigger.
	 */
	@Override
	public void triggerWorkflow(final ItemModel attachment)
	{
		if (getConfigurationService().getConfiguration().getBoolean(getWorkFlowEnabledKey(), false))
		{
			final Collection<AbstractPageModel> pages = getSessionService().executeInLocalView(new SessionExecutionBody()
			{
				@Override
				public Object execute()
				{
					final AbstractCMSComponentModel component = (AbstractCMSComponentModel) attachment;
					getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(component.getCatalogVersion()));
					return getCmsPageService().getPagesForComponent(component);
				}
			});
			if (CollectionUtils.isNotEmpty(pages))
			{
				pages.stream().forEach(super::determineWorkflowTemplate);
			}
		}
	}

	protected CMSPageService getCmsPageService()
	{
		return cmsPageService;
	}

	@Required
	public void setCmsPageService(final CMSPageService cmsPageService)
	{
		this.cmsPageService = cmsPageService;
	}

	protected String getWorkFlowEnabledKey()
	{
		return workFlowEnabledKey;
	}

	@Required
	public void setWorkFlowEnabledKey(final String workFlowEnabledKey)
	{
		this.workFlowEnabledKey = workFlowEnabledKey;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}
}
