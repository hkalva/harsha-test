/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.lyonscg.workflowsapi.service.impl;

import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.collect.Lists;
import com.lyonscg.workflowsapi.daos.WorkflowsDao;
import com.lyonscg.workflowsapi.service.WorkflowsService;


/**
 * Default implementation of {@link WorkflowsService}.
 */
public class DefaultWorkflowsService implements WorkflowsService
{
	private WorkflowsDao workflowsDao;
	private EmailService emailService;

	/**
	 * To retrieve the Employees in a User Group.
	 */
	@Override
	public List<EmployeeModel> getEmployeesInUserGroup(final String userGroup)
	{
		ServicesUtil.validateParameterNotNull(workflowsDao, "workflowsDao cannot be null");
		return getWorkflowsDao().getEmployeesInUserGroup(userGroup);
	}


	/**
	 * To retrieve the EmailAddress Models of a list of employees.
	 */
	@Override
	public List<EmailAddressModel> getEmailAddressesOfEmployees(final List<EmployeeModel> employees)
	{
		final List<EmailAddressModel> emails = new ArrayList<>();
		ServicesUtil.validateParameterNotNull(emailService, "emailService cannot be null");
		for (final EmployeeModel employee : employees)
		{
			for (final AddressModel address : Lists.newArrayList(employee.getAddresses()))
			{
				if (BooleanUtils.isTrue(address.getContactAddress()))
				{
					if (StringUtils.isNotBlank(address.getEmail()))
					{
						final EmailAddressModel emailAddress = getEmailService().getOrCreateEmailAddressForEmail(address.getEmail(),
								employee.getName());
						emails.add(emailAddress);
					}
					break;
				}
			}
		}
		return emails;
	}

	/**
	 * @param workflowsDao
	 *           the workflowDao to set
	 */
	public void setWorkflowsDao(final WorkflowsDao workflowsDao)
	{
		this.workflowsDao = workflowsDao;
	}

	/**
	 * @return the workflowsDao
	 */
	public WorkflowsDao getWorkflowsDao()
	{
		return workflowsDao;
	}

	/**
	 * @param emailService
	 *           the emailService to set
	 */
	@Required
	public void setEmailService(final EmailService emailService)
	{
		this.emailService = emailService;
	}

	/**
	 * @return the emailService
	 */
	public EmailService getEmailService()
	{
		return emailService;
	}

}
