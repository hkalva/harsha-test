/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.lyonscg.workflowsapi.service;

import de.hybris.platform.core.model.ItemModel;


/**
 * Generic interface to trigger workflow based on the Item.
 */
public interface WorkflowTriggerService
{
	/**
	 * Triggers workflow based on the conditions and item state.
	 * 
	 * @param item
	 *           - The item responsible for workflow trigger.
	 */
	void triggerWorkflow(ItemModel item);
}
