/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.lyonscg.workflowsapi.workflows.actions;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;

import java.util.Collections;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.lyonscg.workflowsapi.constants.LyonscgworkflowsapiConstants;
import com.lyonscg.workflowsapi.model.CockpitProcessModel;


/**
 * Automated action responsible for creating the email business process.
 */
public class SendEmailAutomatedWorkflowTemplateJob extends AbstractAutomatedWorkflowTemplateJob
{

	private static final Logger LOG = Logger.getLogger(SendEmailAutomatedWorkflowTemplateJob.class);
	/**
	 * The name of the process definition that is responsible for sending an email. Processes are configured through XML
	 * files under the resource folder.
	 */
	private String processDefinitionName;
	/**
	 * The unique attribute from the workflow attachment to create code for CockpitProcessModel.
	 */
	private String attributeForProcessId;
	/**
	 * The attribute to which the workflow attachment has to be attached to the created email business process.
	 */
	private String cockpitProcessAttachmentAttribute;
	/**
	 * Business parameter name and value for setting the email generation action.
	 */
	private String cockpitProcessEmailGeneratorAttribute, cockpitProcessEmailGeneratorValue;

	private ConfigurationService configurationService;
	private BaseSiteService baseSiteService;
	private CommerceCommonI18NService commerceCommonI18NService;

	/**
	 * Creates and starts the email business process corresponding to the attribute in {@link #processDefinitionName}
	 */
	@Override
	public WorkflowDecisionModel perform(final WorkflowActionModel workflowActionModel)
	{
		final ItemModel item = getAttachment(workflowActionModel);
		Assert.notNull(item, "Mandatory workflow attachment missing!");
		final CockpitProcessModel process = createProcessModel(item);

		if (LOG.isDebugEnabled())
		{
			LOG.debug(String.format("Starting process with code '%s'", process.getCode()));
		}

		getBusinessProcessService().startProcess(process);
		return defaultDecision(workflowActionModel);
	}

	/**
	 * Creates an instance of {@link CockpitProcessModel} using the process definition.
	 *
	 * @param itemModel
	 *           item
	 * @return An instance of {@link CockpitProcessModel}
	 */
	protected CockpitProcessModel createProcessModel(final ItemModel itemModel)
	{
		final String processCode = generateProcessCode(itemModel);
		final CockpitProcessModel process = getBusinessProcessService().createProcess(processCode, getProcessDefinitionName());
		getModelService().setAttributeValue(process, getCockpitProcessAttachmentAttribute(), itemModel);
		getBaseSiteService().setCurrentBaseSite(
				getConfigurationService().getConfiguration().getString(LyonscgworkflowsapiConstants.CONFIG_STORE_ID_PREFIX), true);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("baseSiteService.getCurrentBaseSite().getUid() :" + getBaseSiteService().getCurrentBaseSite().getUid());
		}
		process.setSite(getBaseSiteService().getCurrentBaseSite());
		process.setLanguage(getCommerceCommonI18NService().getDefaultLanguage());
		getModelService().save(process);
		process.setContextParameters(Collections.singletonList(createEmailGeneratorParam(process)));
		getModelService().save(process);
		return process;

	}

	/**
	 * Creates the business process parameter from {@link #cockpitProcessEmailGeneratorValue} which helps the process
	 * identify the next business action.
	 *
	 * @param process
	 *           - The email business process.
	 * @return - The created {@link BusinessProcessParameterModel}
	 */
	protected BusinessProcessParameterModel createEmailGeneratorParam(final CockpitProcessModel process)
	{
		final BusinessProcessParameterModel emailGeneratorParam = getModelService().create(BusinessProcessParameterModel.class);
		emailGeneratorParam.setName(getCockpitProcessEmailGeneratorAttribute());
		emailGeneratorParam.setValue(getCockpitProcessEmailGeneratorValue());
		emailGeneratorParam.setProcess(process);
		getModelService().save(emailGeneratorParam);
		return emailGeneratorParam;
	}

	/**
	 * Generates a new 'unique' code for the process to be launched.
	 *
	 * @param item
	 *           The item associated to the workflow
	 * @return A code made of the process definition name, the product's code and a timestamp
	 */
	protected String generateProcessCode(final ItemModel item)
	{
		return getProcessDefinitionName() + "-" + getModelService().getAttributeValue(item, getAttributeForProcessId()) + "-"
				+ System.currentTimeMillis();
	}

	protected CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	@Required
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	@Required
	public void setProcessDefinitionName(final String processDefinitionName)
	{
		this.processDefinitionName = processDefinitionName;
	}

	protected String getProcessDefinitionName()
	{
		return processDefinitionName;
	}

	protected String getAttributeForProcessId()
	{
		return attributeForProcessId;
	}

	@Required
	public void setAttributeForProcessId(final String attributeForProcessId)
	{
		this.attributeForProcessId = attributeForProcessId;
	}

	protected String getCockpitProcessAttachmentAttribute()
	{
		return cockpitProcessAttachmentAttribute;
	}

	@Required
	public void setCockpitProcessAttachmentAttribute(final String cockpitProcessAttachmentAttribute)
	{
		this.cockpitProcessAttachmentAttribute = cockpitProcessAttachmentAttribute;
	}

	protected String getCockpitProcessEmailGeneratorAttribute()
	{
		return cockpitProcessEmailGeneratorAttribute;
	}

	@Required
	public void setCockpitProcessEmailGeneratorAttribute(final String cockpitProcessEmailGeneratorAttribute)
	{
		this.cockpitProcessEmailGeneratorAttribute = cockpitProcessEmailGeneratorAttribute;
	}

	protected String getCockpitProcessEmailGeneratorValue()
	{
		return cockpitProcessEmailGeneratorValue;
	}

	@Required
	public void setCockpitProcessEmailGeneratorValue(final String cockpitProcessEmailGeneratorValue)
	{
		this.cockpitProcessEmailGeneratorValue = cockpitProcessEmailGeneratorValue;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}
}
