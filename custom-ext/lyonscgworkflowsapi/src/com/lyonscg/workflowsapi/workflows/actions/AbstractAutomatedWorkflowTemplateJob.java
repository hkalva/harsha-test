/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.lyonscg.workflowsapi.workflows.actions;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.workflow.WorkflowAttachmentService;
import de.hybris.platform.workflow.jobs.AutomatedWorkflowTemplateJob;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Abstract class with basic functionalities to support automated workflow actions.
 */
public abstract class AbstractAutomatedWorkflowTemplateJob implements AutomatedWorkflowTemplateJob
{

	private BusinessProcessService businessProcessService;
	private ModelService modelService;
	private WorkflowAttachmentService workflowAttachmentService;
	private Class attachmentClass;

	/**
	 * Gets the next decision and resumes the execution of the workflow UNLESS there are no more decisions in which case
	 * we return null to indicate the end of the workflow.Use this when there is only ONE decision from a given action!.
	 *
	 * @param workflowActionModel
	 *           The current workflow action.
	 * @return A {@link WorkflowDecisionModel} that indicates the default decision.
	 */
	protected WorkflowDecisionModel defaultDecision(final WorkflowActionModel workflowActionModel)
	{
		for (final WorkflowDecisionModel decision : workflowActionModel.getDecisions())
		{
			return decision;
		}
		return null;
	}

	/**
	 * Gets the {@link ItemModel} attached to the workflow.
	 *
	 * @param workflowActionModel
	 *           The workflow action to extract the attachments from.
	 * @return The {@link ItemModel} attached to workflow, or null if not found.
	 */
	protected ItemModel getAttachment(final WorkflowActionModel workflowActionModel)
	{
		return getModelOfType(workflowActionModel, getAttachmentClass());
	}

	/**
	 * Gets a model of the provided class from the list.
	 *
	 * @param workflowActionModel
	 *           The workflow action being executed
	 * @param clazz
	 *           The item to be returned needs to be of this type
	 * @return The first instance of the provided type, or null if not found
	 */
	protected <T extends ItemModel> T getModelOfType(final WorkflowActionModel workflowActionModel, final Class<T> clazz)
	{
		final List<ItemModel> models = getWorkflowAttachmentService().getAttachmentsForAction(workflowActionModel, clazz.getName());

		if (CollectionUtils.isNotEmpty(models))
		{
			return (T) models.iterator().next();
		}
		return null;
	}

	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected WorkflowAttachmentService getWorkflowAttachmentService()
	{
		return workflowAttachmentService;
	}

	@Required
	public void setWorkflowAttachmentService(final WorkflowAttachmentService workflowAttachmentService)
	{
		this.workflowAttachmentService = workflowAttachmentService;
	}

	protected Class getAttachmentClass()
	{
		return attachmentClass;
	}

	@Required
	public void setAttachmentClass(final Class attachmentClass)
	{
		this.attachmentClass = attachmentClass;
	}

}
