/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.lyonscg.workflowsapi.workflows.actions;

import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;


/**
 * Generic AutomatedWorkflowTemplateJob to do nothing and return the first decision. Generally used when only one
 * Decision exists.
 */
public class DefaultDecisionAutomatedWorkflowTemplateJob extends AbstractAutomatedWorkflowTemplateJob
{
	/**
	 * Does nothing but returns the default decision from
	 * {@link AbstractAutomatedWorkflowTemplateJob#defaultDecision(WorkflowActionModel)}.
	 * 
	 * @param workflowAction
	 *           - Current workflow action.
	 * @return - Returns the decision from
	 *         {@link AbstractAutomatedWorkflowTemplateJob#defaultDecision(WorkflowActionModel)}.
	 */
	@Override
	public WorkflowDecisionModel perform(final WorkflowActionModel workflowAction)
	{
		return defaultDecision(workflowAction);
	}
}
