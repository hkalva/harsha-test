/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.lyonscg.workflowsapi.workflows.actions;

import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * Generic Automated Action to set and save workflow attachment's based on {@link #attribute} and {@link #setStatus}
 */
public class SetStatusAutomatedWorkflowTemplateJob extends AbstractAutomatedWorkflowTemplateJob
{

	private static final Logger LOG = Logger.getLogger(SetStatusAutomatedWorkflowTemplateJob.class);
	private String attribute;
	private HybrisEnumValue setStatus;

	/**
	 * Set and save workflow attachment's based on {@link #attribute} and {@link #setStatus} and returns the default
	 * decision from the super class.
	 */
	@Override
	public WorkflowDecisionModel perform(final WorkflowActionModel workflowAction)
	{
		final ItemModel item = getAttachment(workflowAction);
		getModelService().setAttributeValue(item, getAttribute(), getSetStatus());
		getModelService().save(item);
		if (LOG.isDebugEnabled())
		{
			LOG.debug(item + "attribute '" + getAttribute() + "' set as " + getSetStatus());
		}
		return defaultDecision(workflowAction);
	}

	protected String getAttribute()
	{
		return attribute;
	}

	@Required
	public void setAttribute(final String attribute)
	{
		this.attribute = attribute;
	}


	protected HybrisEnumValue getSetStatus()
	{
		return setStatus;
	}


	@Required
	public void setSetStatus(final HybrisEnumValue setStatus)
	{
		this.setStatus = setStatus;
	}

}
