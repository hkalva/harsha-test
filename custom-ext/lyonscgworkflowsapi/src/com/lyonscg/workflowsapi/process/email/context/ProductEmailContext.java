/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.lyonscg.workflowsapi.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.util.localization.Localization;

import javax.annotation.Resource;

import com.lyonscg.workflowsapi.constants.LyonscgworkflowsapiConstants;
import com.lyonscg.workflowsapi.model.CockpitProcessModel;
import com.lyonscg.workflowsapi.model.ProductProcessModel;


/**
 * Email context used to render B2B registration specific emails.
 */
public class ProductEmailContext extends AbstractEmailContext<CockpitProcessModel>
{
	private ProductModel product;
	@Resource
	private ConfigurationService configurationService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#init(de.hybris.platform.
	 * processengine.model.BusinessProcessModel, de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel)
	 */
	@Override
	public void init(final CockpitProcessModel businessProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(businessProcessModel, emailPageModel);
		if (businessProcessModel instanceof ProductProcessModel)
		{
			final ProductProcessModel productProcessModel = (ProductProcessModel) businessProcessModel;
			setProduct(productProcessModel.getProduct());
			populateToAddress();
		}
	}

	/**
	 * Populate information for the destination email address.
	 *
	 * is generated when order is processing.
	 */
	protected void populateToAddress()
	{
		//Email has to be populated here, else validation error is thrown. ToAddresses will be later set again based on the group to be notified
		put(EMAIL,
				configurationService.getConfiguration().getString(
						LyonscgworkflowsapiConstants.Workflows.PRODUCT_WORKFLOW_DEFAULT_EMAIL));
		final String toDisplayName = Localization.getLocalizedString(configurationService.getConfiguration().getString(
				LyonscgworkflowsapiConstants.Workflows.PRODUCT_WORKFLOW_DEFAULT_DISPLAYNAME));
		put(DISPLAY_NAME, toDisplayName);
	}

	@Override
	protected LanguageModel getEmailLanguage(final CockpitProcessModel businessProcessModel)
	{

		return businessProcessModel.getLanguage();
	}

	@Override
	protected BaseSiteModel getSite(final CockpitProcessModel businessProcessModel)
	{
		return businessProcessModel.getSite();
	}

	@Override
	protected CustomerModel getCustomer(final CockpitProcessModel businessProcessModel)
	{
		return null;
	}

	/**
	 * @return the configurationService
	 */
	@Override
	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	/**
	 * @param configurationService
	 *           the configurationService to set
	 */
	@Override
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	/**
	 * @return the product
	 */
	public ProductModel getProduct()
	{
		return product;
	}

	/**
	 * @param product
	 *           the product to set
	 */
	public void setProduct(final ProductModel product)
	{
		this.product = product;
	}

}
