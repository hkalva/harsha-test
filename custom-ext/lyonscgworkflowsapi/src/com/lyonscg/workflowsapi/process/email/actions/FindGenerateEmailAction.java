/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.lyonscg.workflowsapi.process.email.actions;

import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.lyonscg.workflowsapi.model.CockpitProcessModel;


/**
 * This class helps to find the corresponding GenerateEmailAction(for the next step in business process) bean based on
 * values in {@link CockpitProcessModel#getContextParameters()}.
 */
public class FindGenerateEmailAction extends AbstractAction<CockpitProcessModel>
{
	private static final Logger LOG = Logger.getLogger(FindGenerateEmailAction.class);
	private static final String NOK = "NOK";
	private String cockpitProcessEmailGeneratorAttribute;
	private Map<String, String> emailGeneratorMapping;

	/**
	 * Returns the next action based on the values in {@link CockpitProcessModel#getContextParameters()} and mapping in
	 * {@link #emailGeneratorMapping}.
	 *
	 * @param cockpitProcess
	 *           - The email business process.
	 * @return The next action if successful else returns NOK.
	 */
	@Override
	public String execute(final CockpitProcessModel cockpitProcess)
	{
		final Collection<BusinessProcessParameterModel> businessProcessParams = cockpitProcess.getContextParameters();
		if (CollectionUtils.isNotEmpty(businessProcessParams))
		{
			final BusinessProcessParameterModel businessProcessParam = businessProcessParams.iterator().next();
			if (getCockpitProcessEmailGeneratorAttribute().equals(businessProcessParam.getName()))
			{
				final String nextAction = getEmailGeneratorMapping().get(businessProcessParam.getValue());
				if (null != nextAction)
				{
					LOG.info("Proceeding to next action. Returning " + nextAction);
					return nextAction;
				}
			}
		}
		LOG.warn("Unable to find next action. Returning NOK");
		return NOK;
	}

	/**
	 * Returns set of all possible String Transition values.
	 *
	 * @return - Set of String Transition values.
	 */
	@Override
	public Set<String> getTransitions()
	{
		final Set<String> transitions = new HashSet<String>(getEmailGeneratorMapping().values());
		transitions.add(NOK);
		return transitions;
	}

	protected String getCockpitProcessEmailGeneratorAttribute()
	{
		return cockpitProcessEmailGeneratorAttribute;
	}

	@Required
	public void setCockpitProcessEmailGeneratorAttribute(final String cockpitProcessEmailGeneratorAttribute)
	{
		this.cockpitProcessEmailGeneratorAttribute = cockpitProcessEmailGeneratorAttribute;
	}

	protected Map<String, String> getEmailGeneratorMapping()
	{
		return emailGeneratorMapping;
	}

	@Required
	public void setEmailGeneratorMapping(final Map<String, String> emailGeneratorMapping)
	{
		this.emailGeneratorMapping = emailGeneratorMapping;
	}
}
