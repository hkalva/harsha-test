/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.lyonscg.workflowsapi.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.util.localization.Localization;

import org.springframework.beans.factory.annotation.Required;

import com.lyonscg.workflowsapi.model.CockpitProcessModel;
import com.lyonscg.workflowsapi.model.PageWorkflowProcessModel;


/**
 * Email context used to CMS Page workflow specific emails.
 */
public class PageWorkflowEmailContext extends AbstractEmailContext<CockpitProcessModel>
{
	private AbstractPageModel page;
	private String defaultEmailKey, defaultDisplayNameKey;

	/**
	 * Initializes {@link PageWorkflowEmailContext}.
	 */
	@Override
	public void init(final CockpitProcessModel businessProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(businessProcessModel, emailPageModel);
		if (businessProcessModel instanceof PageWorkflowProcessModel)
		{
			final PageWorkflowProcessModel pageWorkflowProcessModel = (PageWorkflowProcessModel) businessProcessModel;
			setPage(pageWorkflowProcessModel.getPage());
			populateToAddress();
		}
	}

	/**
	 * Populate information for the destination email address. However this will be overridden by the actions in the
	 * business process.
	 */
	protected void populateToAddress()
	{
		//Email has to be populated here, else validation error is thrown. ToAddresses will be later set again based on the group to be notified
		put(EMAIL, getConfigurationService().getConfiguration().getString(getDefaultEmailKey()));
		final String toDisplayName = Localization.getLocalizedString(getDefaultDisplayNameKey());
		put(DISPLAY_NAME, toDisplayName);
	}

	@Override
	protected LanguageModel getEmailLanguage(final CockpitProcessModel businessProcessModel)
	{

		return businessProcessModel.getLanguage();
	}

	@Override
	protected BaseSiteModel getSite(final CockpitProcessModel businessProcessModel)
	{
		return businessProcessModel.getSite();
	}

	@Override
	protected CustomerModel getCustomer(final CockpitProcessModel businessProcessModel)
	{
		return null;
	}

	public AbstractPageModel getPage()
	{
		return page;
	}

	public void setPage(final AbstractPageModel page)
	{
		this.page = page;
	}

	@Required
	public void setDefaultEmailKey(final String defaultEmailKey)
	{
		this.defaultEmailKey = defaultEmailKey;
	}

	@Required
	public void setDefaultDisplayNameKey(final String defaultDisplayNameKey)
	{
		this.defaultDisplayNameKey = defaultDisplayNameKey;
	}

	protected String getDefaultEmailKey()
	{
		return defaultEmailKey;
	}

	protected String getDefaultDisplayNameKey()
	{
		return defaultDisplayNameKey;
	}
}
