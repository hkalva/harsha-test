/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.lyonscg.workflowsapi.process.email.actions;

import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.acceleratorservices.process.email.actions.GenerateEmailAction;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.task.RetryLaterException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.lyonscg.workflowsapi.service.WorkflowsService;


/**
 * Implementation of {@link GenerateEmailAction} responsible for creating an email message for all users associated to
 * the list of user groups corresponding to the field {@link #userGroupIdKeys}.
 */
public class CommonWorkflowGenerateEmailAction extends GenerateEmailAction
{
	private static final Logger LOG = Logger.getLogger(CommonWorkflowGenerateEmailAction.class);
	private WorkflowsService workflowsService;
	private ConfigurationService configurationService;
	private List<String> userGroupIdKeys;

	/**
	 * Iterates through {@link #userGroupIdKeys} and gets all email ids of all users in the group and saves it to email
	 * message.
	 *
	 * @param businessProcessModel
	 *           - The Business process for sending workflow emails.
	 * @return Returns {@link de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition#OK} if
	 *         successfully retrieves email ids else returns
	 *         {@link de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition#NOK}
	 */
	@Override
	public Transition executeAction(final BusinessProcessModel businessProcessModel) throws RetryLaterException
	{
		final Transition transition = super.executeAction(businessProcessModel);
		if (transition == Transition.OK)
		{
			final List<EmployeeModel> employees = new ArrayList<EmployeeModel>();
			for (final String userGroupIdKey : getUserGroupIdKeys())
			{
				final List<EmployeeModel> tempEmployees = getWorkflowsService().getEmployeesInUserGroup(
						getConfigurationService().getConfiguration().getString(userGroupIdKey));
				if (tempEmployees != null)
				{
					employees.addAll(tempEmployees);
				}
			}

			final List<EmailAddressModel> recipients = getWorkflowsService().getEmailAddressesOfEmployees(employees);
			if (CollectionUtils.isEmpty(recipients))
			{
				LOG.warn(String.format("There are no employees within the user group '%s'. No notification will be sent.",
						getUserGroupIdKeys()));
				return Transition.NOK;
			}
			final EmailMessageModel emailMessage = businessProcessModel.getEmails().iterator().next();
			emailMessage.setToAddresses(recipients);
			getModelService().save(emailMessage);
		}
		return transition;
	}

	protected WorkflowsService getWorkflowsService()
	{
		return workflowsService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setWorkflowsService(final WorkflowsService workflowsService)
	{
		this.workflowsService = workflowsService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected List<String> getUserGroupIdKeys()
	{
		return userGroupIdKeys;
	}

	@Required
	public void setUserGroupIdKeys(final List<String> userGroupIdKeys)
	{
		this.userGroupIdKeys = userGroupIdKeys;
	}
}
