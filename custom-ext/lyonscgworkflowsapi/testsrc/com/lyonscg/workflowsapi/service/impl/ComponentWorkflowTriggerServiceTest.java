/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.lyonscg.workflowsapi.service.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.cms2.enums.CmsApprovalStatus;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;
import de.hybris.platform.workflow.WorkflowProcessingService;
import de.hybris.platform.workflow.WorkflowService;
import de.hybris.platform.workflow.WorkflowTemplateService;
import de.hybris.platform.workflow.model.WorkflowModel;
import de.hybris.platform.workflow.model.WorkflowTemplateModel;

import java.util.Collections;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.lyonscg.workflowsapi.data.WorkflowTriggerData;


/**
 * Test class for {@link ComponentWorkflowTriggerService}.
 */
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(LogRunListener.class)
@UnitTest
public class ComponentWorkflowTriggerServiceTest
{
	@InjectMocks
	private ComponentWorkflowTriggerService testClass;
	@Mock
	private ModelService modelService;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private WorkflowTemplateService workflowTemplateService;
	@Mock
	private WorkflowService workflowService;
	@Mock
	private UserService userService;
	@Mock
	private WorkflowProcessingService workflowProcessingService;
	@Mock
	private AbstractCMSComponentModel abstractCMSComponentModel;
	@Mock
	private Configuration configuration;
	@Mock
	private WorkflowTemplateModel workflowTemplateModel;
	@Mock
	private WorkflowModel workflowModel;
	@Mock
	private EmployeeModel employee;
	@Mock
	private AbstractPageModel abstractPageModel;
	@Mock
	private CMSPageService cmsPageService;
	@Mock
	private SessionService sessionService;
	@Mock
	private CatalogVersionService catalogVersionService;

	private WorkflowTriggerData workflowTriggerData;
	private static final String PAGE_WORKFLOW_NAME = "pageCreateOrUpdate";
	private static final String APPROVAL_STATUS_ATTRIBUTE = "approvalStatus";
	private static final String PAGE_WORKFLOW_KEY = "cockpit.page.workflow.name";
	private final String workFlowEnabledKey = "workFlowEnabledKey";

	/**
	 * Setup test data.
	 */
	@Before
	public void setUp()
	{
		testClass = new ComponentWorkflowTriggerService();
		testClass.setWorkFlowEnabledKey(workFlowEnabledKey);
		workflowTriggerData = new WorkflowTriggerData();
		MockitoAnnotations.initMocks(this);
		testClass.setWorkflowTriggerData(workflowTriggerData);
		workflowTriggerData.setAttribute(APPROVAL_STATUS_ATTRIBUTE);
		workflowTriggerData.setSetStatus(CmsApprovalStatus.UNAPPROVED);
		workflowTriggerData.setTypecode(2);
		workflowTriggerData.setWorkflowCodeKey(PAGE_WORKFLOW_KEY);

		Mockito.when(configurationService.getConfiguration()).thenReturn(configuration);
		Mockito.when(configuration.getString(PAGE_WORKFLOW_KEY)).thenReturn(PAGE_WORKFLOW_NAME);
		Mockito.when(configurationService.getConfiguration().getString(PAGE_WORKFLOW_KEY)).thenReturn(PAGE_WORKFLOW_NAME);
		Mockito.when(workflowService.createWorkflow(workflowTemplateModel, abstractPageModel, employee)).thenReturn(workflowModel);
		Mockito.when(workflowTemplateService.getWorkflowTemplateForCode(PAGE_WORKFLOW_NAME)).thenReturn(workflowTemplateModel);
		when(userService.getAdminUser()).thenReturn(employee);
		when(sessionService.executeInLocalView(Mockito.any())).thenReturn(Collections.singletonList(abstractPageModel));
	}

	/**
	 * Test case for {@link ComponentWorkflowTriggerService#triggerWorkflow(de.hybris.platform.core.model.ItemModel)}
	 * with workflow enabled.
	 */
	@Test
	public void triggerWorkflowEnabledTest()
	{
		Mockito.doReturn(Boolean.TRUE).when(configuration).getBoolean(workFlowEnabledKey, false);
		testClass.triggerWorkflow(abstractCMSComponentModel);
		Mockito.verify(modelService).setAttributeValue(abstractPageModel, APPROVAL_STATUS_ATTRIBUTE, CmsApprovalStatus.UNAPPROVED);
		//Verifies that the startWorkflow was invoked at least once.
		Mockito.verify(workflowProcessingService).startWorkflow(workflowModel);
	}

	/**
	 * Test case for {@link ComponentWorkflowTriggerService#triggerWorkflow(de.hybris.platform.core.model.ItemModel)}
	 * with workflow disabled.
	 */
	@Test
	public void triggerWorkflowDisabledTest()
	{
		Mockito.doReturn(Boolean.FALSE).when(configuration).getBoolean(workFlowEnabledKey, false);
		testClass.triggerWorkflow(abstractCMSComponentModel);
		Mockito.verify(modelService, Mockito.never()).setAttributeValue(abstractPageModel, APPROVAL_STATUS_ATTRIBUTE,
				CmsApprovalStatus.UNAPPROVED);
		//Verifies that the startWorkflow was invoked at least once.
		Mockito.verify(workflowProcessingService, Mockito.never()).startWorkflow(workflowModel);
	}
}
