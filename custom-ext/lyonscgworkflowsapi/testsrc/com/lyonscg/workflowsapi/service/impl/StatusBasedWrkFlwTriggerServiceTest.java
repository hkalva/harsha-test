/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.lyonscg.workflowsapi.service.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;
import de.hybris.platform.workflow.WorkflowProcessingService;
import de.hybris.platform.workflow.WorkflowService;
import de.hybris.platform.workflow.WorkflowTemplateService;
import de.hybris.platform.workflow.model.WorkflowModel;
import de.hybris.platform.workflow.model.WorkflowTemplateModel;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.lyonscg.workflowsapi.data.WorkflowTriggerData;


/**
 * Test class for {@link StatusBasedWrkFlwTriggerService}.
 */
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(LogRunListener.class)
@UnitTest
public class StatusBasedWrkFlwTriggerServiceTest
{
	private static final String PRODUCT_WORKFLOW_KEY = "cockpit.product.workflow.name";
	private static final String APPROVAL_STATUS_ATTRIBUTE = "approvalStatus";
	@InjectMocks
	private StatusBasedWrkFlwTriggerService testClass;
	@Mock
	private ModelService modelService;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private WorkflowTemplateService workflowTemplateService;
	@Mock
	private WorkflowService workflowService;
	@Mock
	private UserService userService;
	@Mock
	private WorkflowProcessingService workflowProcessingService;
	@Mock
	private ProductModel productModel;
	@Mock
	private Configuration configuration;
	@Mock
	private WorkflowTemplateModel workflowTemplateModel;
	@Mock
	private WorkflowModel workflowModel;
	@Mock
	private EmployeeModel employee;
	private WorkflowTriggerData workflowTriggerData;
	private static final String PRODUCT_WORKFLOW_NAME = "productCreateOrUpdate";

	/**
	 * Setup test data.
	 */
	@Before
	public void setUp()
	{
		testClass = new StatusBasedWrkFlwTriggerService();
		workflowTriggerData = new WorkflowTriggerData();
		MockitoAnnotations.initMocks(this);
		testClass.setWorkflowTriggerData(workflowTriggerData);
		workflowTriggerData.setAttribute(APPROVAL_STATUS_ATTRIBUTE);
		workflowTriggerData.setSetStatus(ArticleApprovalStatus.UNAPPROVED);
		workflowTriggerData.setTriggerStatus(ArticleApprovalStatus.REQUESTAPPROVAL);
		workflowTriggerData.setTypecode(1);
		workflowTriggerData.setWorkflowCodeKey(PRODUCT_WORKFLOW_KEY);

		Mockito.when(configurationService.getConfiguration()).thenReturn(configuration);
		Mockito.when(configurationService.getConfiguration().getString(PRODUCT_WORKFLOW_KEY)).thenReturn(PRODUCT_WORKFLOW_NAME);
		Mockito.when(workflowTemplateService.getWorkflowTemplateForCode(PRODUCT_WORKFLOW_NAME)).thenReturn(workflowTemplateModel);
		Mockito.when(workflowService.createWorkflow(workflowTemplateModel, productModel, employee)).thenReturn(workflowModel);
		when(userService.getAdminUser()).thenReturn(employee);
	}

	/**
	 * Test case for {@link StatusBasedWrkFlwTriggerService#triggerWorkflow(de.hybris.platform.core.model.ItemModel)} for
	 * REQUESTAPPROVAL product.
	 */
	@Test
	public void triggerWorkflowTestRequestApproval()
	{
		Mockito.when(modelService.getAttributeValue(productModel, APPROVAL_STATUS_ATTRIBUTE)).thenReturn(
				ArticleApprovalStatus.REQUESTAPPROVAL);
		testClass.triggerWorkflow(productModel);
		Mockito.verify(modelService).setAttributeValue(productModel, APPROVAL_STATUS_ATTRIBUTE, ArticleApprovalStatus.UNAPPROVED);
		//Verifies that the startWorkflow was invoked at least once.
		Mockito.verify(workflowProcessingService).startWorkflow(workflowModel);
	}

	/**
	 * Test case for {@link StatusBasedWrkFlwTriggerService#triggerWorkflow(de.hybris.platform.core.model.ItemModel)} for
	 * APPROVED product.
	 */
	@Test
	public void triggerWorkflowTestApproved()
	{
		Mockito.when(modelService.getAttributeValue(productModel, APPROVAL_STATUS_ATTRIBUTE)).thenReturn(
				ArticleApprovalStatus.APPROVED);
		testClass.triggerWorkflow(productModel);
		Mockito.verify(modelService, Mockito.never()).setAttributeValue(productModel, APPROVAL_STATUS_ATTRIBUTE,
				ArticleApprovalStatus.UNAPPROVED);
		//Verifies that the startWorkflow was not invoked.
		Mockito.verify(workflowProcessingService, Mockito.never()).startWorkflow(workflowModel);
	}
}
