/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.lyonscg.workflowsapi.daos.impl;

import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.servicelayer.ServicelayerTest;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.lyonscg.workflowsapi.daos.WorkflowsDao;
import com.lyonscg.workflowsapi.tests.util.WorkflowsapiTestsUtil;


/**
 * To test DefaultWorkflowsDao.
 */
@IntegrationTest
public class DefaultWorkflowsDaoIntegrationTest extends ServicelayerTest
{
	@Resource
	private WorkflowsDao workflowsDao;

	private String impexContent;
	private List<String> userGroups;

	private static final String PATH = "/lyonscgworkflowsapi/test/";
	private static final String IMPEX_TEST_FILE = "testDefaultWorkflowsDaoIntegrationTest.impex";

	private static final int UID_INDEX = 1;
	private static final int USER_GROUP_INDEX = 3;
	private static final String INEXISTANT_USER_GROUP = "inexistantUserGroup";


	@Before
	public void setUp() throws Exception
	{
		importCsv(PATH + IMPEX_TEST_FILE, "UTF-8");
		impexContent = WorkflowsapiTestsUtil.impexFileToString(PATH + IMPEX_TEST_FILE);

		userGroups = WorkflowsapiTestsUtil.getUserGroupsFromImpex(impexContent, UID_INDEX);
	}


	/*
	 * Make sure no user is associated to userGroup INEXISTANT_USER_GROUP in file IMPEX_TEST_FILE.
	 */
	@Test
	public void testGetEmployeesInUserGroup()
	{

		List<EmployeeModel> employees;
		List<String> uidsFromDao;
		List<String> uidsFromImpex;

		for (final String userGroup : userGroups)
		{
			uidsFromImpex = WorkflowsapiTestsUtil.getEmployeesUidsFromImpex(impexContent, userGroup, UID_INDEX, USER_GROUP_INDEX);

			employees = workflowsDao.getEmployeesInUserGroup(userGroup);
			uidsFromDao = WorkflowsapiTestsUtil.employeesToUids(employees);

			assertTrue("Dao should return same employees as those in impex file",
					WorkflowsapiTestsUtil.compareLists(uidsFromImpex, uidsFromDao));
		}

		//Testing inexistent userGroup
		employees = workflowsDao.getEmployeesInUserGroup(INEXISTANT_USER_GROUP);
		assertTrue("Dao should not return any employee for userGroup " + INEXISTANT_USER_GROUP, employees.isEmpty());
	}

}
