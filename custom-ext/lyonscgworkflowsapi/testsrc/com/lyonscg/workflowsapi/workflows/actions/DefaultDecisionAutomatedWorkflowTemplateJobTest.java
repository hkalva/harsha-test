/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.lyonscg.workflowsapi.workflows.actions;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;


/**
 * This test file tests and demonstrates the behavior of the ProductRejectedAutomatedWorkflowTemplateJob method perform.
 */
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(LogRunListener.class)
@UnitTest
public class DefaultDecisionAutomatedWorkflowTemplateJobTest
{
	private DefaultDecisionAutomatedWorkflowTemplateJob testClass;
	private WorkflowActionModel workflowActionModel;
	private WorkflowDecisionModel workflowDecisionModel;
	private final String decisionName = "Test decision name";

	/**
	 * Setup test data.
	 */
	@Before
	public void setUp()
	{
		// We will be testing ProductRejectedAutomatedWorkflowTemplateJob
		testClass = new DefaultDecisionAutomatedWorkflowTemplateJob();
		// This instance of a WorkflowActionModel will be used by the tests
		workflowActionModel = new WorkflowActionModel();
		// This instance of a WorkflowDecisionModel will be used by the tests
		workflowDecisionModel = new WorkflowDecisionModel();
	}

	/**
	 * This test tests and demonstrates that the ProductRejectedAutomatedWorkflowTemplateJob method perform. Cause we do
	 * nothing in the ProductRejected workflow step, just return the next decision of the workflowActionModel.
	 */
	@Test
	public void testRejectedAutomatedWorkflowTemplateJob()
	{
		// We will create a test workflowDecisionModel
		final Locale locale = Locale.getDefault();
		workflowDecisionModel.setName(decisionName, locale);

		// Add workflowDecisionModel to collection
		final Collection<WorkflowDecisionModel> c = new ArrayList<>();
		c.add(workflowDecisionModel);

		// Save the workflowDecisionModel collection to the workflowActionModel
		workflowActionModel.setDecisions(c);

		// Now we test to perform the workflowActionModel
		final WorkflowDecisionModel result = testClass.perform(workflowActionModel);

		// We then verify the result from the ProductRejectedAutomatedWorkflowTemplateJob is equal to the one we just created above
		assertEquals("the next workflow decision model is expected", workflowDecisionModel, result);
	}
}
