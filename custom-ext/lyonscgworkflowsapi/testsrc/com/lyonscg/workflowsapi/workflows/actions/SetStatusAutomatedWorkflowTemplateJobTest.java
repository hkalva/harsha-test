/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.lyonscg.workflowsapi.workflows.actions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;
import de.hybris.platform.workflow.WorkflowAttachmentService;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;
import de.hybris.platform.workflow.model.WorkflowModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;


/**
 * Test class for {@link SetStatusAutomatedWorkflowTemplateJob}.
 */
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(LogRunListener.class)
@UnitTest
public class SetStatusAutomatedWorkflowTemplateJobTest
{
	@InjectMocks
	private SetStatusAutomatedWorkflowTemplateJob testClass;

	private WorkflowActionModel workflowActionModel;
	private WorkflowDecisionModel workflowDecisionModel;
	private ProductModel productModel1;
	@Mock
	private ModelService modelService;
	@Mock
	private WorkflowAttachmentService workflowAttachmentService;

	private List<ProductModel> attachments;
	private List<ProductModel> products;

	private static final String PROD_NAME = "Product 1";
	private static final String PROD_CODE = "PROD1";
	private static final String DECISION_NAME = "Default decision name";
	private Class attachmentClass;

	private String attribute;
	private HybrisEnumValue setStatus;

	/**
	 * Setup test data.
	 */
	@Before
	public void setUp()
	{
		testClass = new SetStatusAutomatedWorkflowTemplateJob();
		MockitoAnnotations.initMocks(this);
		attribute = "approvalStatus";
		setStatus = ArticleApprovalStatus.APPROVED;
		attachmentClass = ProductModel.class;
		testClass.setAttribute(attribute);
		testClass.setSetStatus(setStatus);
		testClass.setAttachmentClass(attachmentClass);

		productModel1 = createProductModel();
		products = Arrays.asList(productModel1);

		workflowActionModel = new WorkflowActionModel();
		workflowDecisionModel = new WorkflowDecisionModel();
		workflowDecisionModel.setName(DECISION_NAME, Locale.getDefault());
		workflowActionModel.setDecisions(Arrays.asList(workflowDecisionModel));
	}


	/**
	 * To test the perform method.
	 */
	@Test
	public void testPerform()
	{

		attachments = Arrays.asList(productModel1);
		Mockito.doAnswer(new Answer<Object>()
		{
			@Override
			public Object answer(final InvocationOnMock invocation)
			{
				products = new ArrayList<ProductModel>();
				products.add(productModel1);

				return null;
			}
		}).when(modelService).save(productModel1);

		Mockito.doAnswer(new Answer<Object>()
		{
			@Override
			public Object answer(final InvocationOnMock invocation)
			{
				attachments = new LinkedList<ProductModel>();
				attachments = Arrays.asList(productModel1);

				return null;
			}
		}).when(workflowAttachmentService).addItems(any(WorkflowModel.class), any(attachments.getClass()));

		workflowAttachmentService.addItems(workflowActionModel.getWorkflow(), attachments);

		final List<ItemModel> productAsList = new LinkedList<ItemModel>();
		productAsList.add(productModel1);
		when(workflowAttachmentService.getAttachmentsForAction(workflowActionModel, ProductModel.class.getName())).thenReturn(
				productAsList);

		final WorkflowModel workflowModel = new WorkflowModel();
		workflowModel.setActions(Arrays.asList(workflowActionModel));

		workflowActionModel.setWorkflow(workflowModel);

		final WorkflowDecisionModel decision = testClass.perform(workflowActionModel);

		assertEquals("The right decision shoule be returned", decision, workflowDecisionModel);

		assertTrue("Product should have been created", attachments.get(0) != null);

		Mockito.verify(modelService).setAttributeValue(productModel1, attribute, setStatus);

		assertTrue("ProductModel should still be in workflow attachment",
				attachments.get(0).getClass().getName().endsWith("ProductModel"));
	}

	/**
	 * To create a dummy productModel for testing.
	 */
	private ProductModel createProductModel()
	{
		final ProductModel product = new ProductModel();
		product.setCode(PROD_CODE);
		product.setName(PROD_NAME, Locale.getDefault());

		return product;
	}

}
