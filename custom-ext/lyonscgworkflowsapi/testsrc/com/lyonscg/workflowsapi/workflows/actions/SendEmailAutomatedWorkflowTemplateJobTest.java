/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.lyonscg.workflowsapi.workflows.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;
import de.hybris.platform.workflow.WorkflowAttachmentService;
import de.hybris.platform.workflow.model.WorkflowActionModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.lyonscg.workflowsapi.constants.LyonscgworkflowsapiConstants;
import com.lyonscg.workflowsapi.model.ProductProcessModel;


/**
 * To test SendEmailAutomatedWorkflowTemplateJob class.
 */
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(LogRunListener.class)
@UnitTest
public class SendEmailAutomatedWorkflowTemplateJobTest
{
	@InjectMocks
	private SendEmailAutomatedWorkflowTemplateJob testClass;
	@Mock
	private WorkflowAttachmentService workflowAttachmentService;
	@Mock
	private WorkflowActionModel workflowActionModel;
	@Mock
	private ProductModel productModel;
	@Mock
	private BusinessProcessService businessProcessService;
	@Mock
	private ProductProcessModel productProcessModel;
	@Mock
	private BaseSiteModel baseSiteModel;
	@Mock
	private LanguageModel languageModel;
	@Mock
	private ModelService modelService;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private BaseSiteService baseSiteService;
	@Mock
	private CommerceCommonI18NService commerceCommonI18NService;
	@Mock
	private Configuration configuration;

	private Class attachmentClass;
	@Mock
	private BusinessProcessParameterModel businessProcessParameter;
	private String cockpitProcessEmailGeneratorAttribute, cockpitProcessEmailGeneratorValue, cockpitProcessAttachmentAttribute;

	private static final String STORE_PREFIX_ID = "lea";

	/**
	 * Setup test data.
	 */
	@Before
	public void setUp()
	{
		testClass = new SendEmailAutomatedWorkflowTemplateJob();
		MockitoAnnotations.initMocks(this);
		attachmentClass = ProductModel.class;
		cockpitProcessEmailGeneratorAttribute = "cockpitProcessEmailGeneratorAttribute";
		cockpitProcessEmailGeneratorValue = "cockpitProcessEmailGeneratorValue";
		cockpitProcessAttachmentAttribute = "cockpitProcessAttachmentAttribute";
		testClass.setAttachmentClass(attachmentClass);
		testClass.setCockpitProcessEmailGeneratorAttribute(cockpitProcessEmailGeneratorAttribute);
		testClass.setCockpitProcessEmailGeneratorValue(cockpitProcessEmailGeneratorValue);
		testClass.setCockpitProcessAttachmentAttribute(cockpitProcessAttachmentAttribute);

		Mockito.when(configurationService.getConfiguration()).thenReturn(configuration);

		Mockito.when(configuration.getString(LyonscgworkflowsapiConstants.CONFIG_STORE_ID_PREFIX)).thenReturn(STORE_PREFIX_ID);

		Mockito.when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
		Mockito.when(commerceCommonI18NService.getDefaultLanguage()).thenReturn(languageModel);
		Mockito.when(modelService.create(BusinessProcessParameterModel.class)).thenReturn(businessProcessParameter);

	}

	/**
	 * IllegalArgument test for {@link SendEmailAutomatedWorkflowTemplateJob#perform(WorkflowActionModel)}. Should throw
	 * IllegalArgumentException as it is unable to find the attachment.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void callPerformWithNoProductModel()
	{
		testClass.perform(workflowActionModel);
	}

	/**
	 * Positive test case for {@link SendEmailAutomatedWorkflowTemplateJob#perform(WorkflowActionModel)}.
	 */
	@Test
	public void callPerformWithValidProductModel()
	{

		final List<ItemModel> productModelList = new ArrayList<>();
		productModelList.add(productModel);

		Mockito.when(
				workflowAttachmentService.getAttachmentsForAction(workflowActionModel,
						"de.hybris.platform.core.model.product.ProductModel")).thenReturn(productModelList);

		Mockito.when(businessProcessService.createProcess(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(productProcessModel);

		testClass.perform(workflowActionModel);

		Mockito.verify(businessProcessService).startProcess(productProcessModel);
		Mockito.verify(businessProcessParameter).setName(cockpitProcessEmailGeneratorAttribute);
		Mockito.verify(businessProcessParameter).setValue(cockpitProcessEmailGeneratorValue);
		Mockito.verify(productProcessModel).setContextParameters(Collections.singletonList(businessProcessParameter));
		Mockito.verify(modelService).setAttributeValue(productProcessModel, cockpitProcessAttachmentAttribute, productModel);
	}

}
