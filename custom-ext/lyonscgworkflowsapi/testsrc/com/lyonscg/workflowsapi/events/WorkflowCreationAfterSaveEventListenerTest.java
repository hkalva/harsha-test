/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.lyonscg.workflowsapi.events;

import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;
import de.hybris.platform.tx.AfterSaveEvent;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.lyonscg.workflowsapi.service.WorkflowTriggerService;


/**
 * Test class for {@link WorkflowCreationAfterSaveEventListener}.
 */
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(LogRunListener.class)
public class WorkflowCreationAfterSaveEventListenerTest
{
	@InjectMocks
	private WorkflowCreationAfterSaveEventListener testClass;
	@Mock
	private ModelService modelService;
	@Mock
	private WorkflowTriggerService prdWklflwTriggerService, pageWklflwTriggerService, cmpntWklflwTriggerService;
	private Map<Integer, WorkflowTriggerService> workflowTriggerServiceMap;
	private static final PK PK1_ = PK.fromLong(8796093054977l);
	private static final PK PK2_ = PK.fromLong(8796093056048l);
	private static final PK PK3_ = PK.fromLong(8796093056060l);
	@Mock
	private AfterSaveEvent saveEvent;
	@Mock
	private ProductModel product;
	@Mock
	private AbstractPageModel page;
	@Mock
	private AbstractCMSComponentModel cmsComponent;

	/**
	 * Setup test data.
	 */
	@Before
	public void setUp()
	{
		testClass = new WorkflowCreationAfterSaveEventListener();
		MockitoAnnotations.initMocks(this);
		workflowTriggerServiceMap = new HashMap<>();
		testClass.setWorkflowTriggerServiceMap(workflowTriggerServiceMap);
		workflowTriggerServiceMap.put(Integer.valueOf(PK1_.getTypeCode()), prdWklflwTriggerService);
		workflowTriggerServiceMap.put(Integer.valueOf(PK2_.getTypeCode()), pageWklflwTriggerService);
		workflowTriggerServiceMap.put(Integer.valueOf(PK3_.getTypeCode()), cmpntWklflwTriggerService);

		Mockito.when(modelService.get(PK1_)).thenReturn(product);
		Mockito.when(modelService.get(PK2_)).thenReturn(page);
		Mockito.when(modelService.get(PK3_)).thenReturn(cmsComponent);
	}

	/**
	 * Test class for {@link WorkflowCreationAfterSaveEventListener#afterSave(java.util.Collection)} for product update.
	 */
	@Test
	public void afterSaveProductUpdate()
	{
		Mockito.when(saveEvent.getPk()).thenReturn(PK1_);
		callForSave(AfterSaveEvent.UPDATE);
		Mockito.verify(prdWklflwTriggerService).triggerWorkflow(product);
	}

	/**
	 * Test class for {@link WorkflowCreationAfterSaveEventListener#afterSave(java.util.Collection)} for product create.
	 */
	@Test
	public void afterSaveProductCreate()
	{
		Mockito.when(saveEvent.getPk()).thenReturn(PK1_);
		callForSave(AfterSaveEvent.CREATE);
		Mockito.verify(prdWklflwTriggerService).triggerWorkflow(product);
	}

	/**
	 * Test class for {@link WorkflowCreationAfterSaveEventListener#afterSave(java.util.Collection)} for page update.
	 */
	@Test
	public void afterSavePageUpdate()
	{
		Mockito.when(saveEvent.getPk()).thenReturn(PK2_);
		callForSave(AfterSaveEvent.UPDATE);
		Mockito.verify(pageWklflwTriggerService).triggerWorkflow(page);
	}

	/**
	 * Test class for {@link WorkflowCreationAfterSaveEventListener#afterSave(java.util.Collection)} for page create.
	 */
	@Test
	public void afterSavePageCreate()
	{
		Mockito.when(saveEvent.getPk()).thenReturn(PK2_);
		callForSave(AfterSaveEvent.CREATE);
		Mockito.verify(pageWklflwTriggerService).triggerWorkflow(page);
	}

	/**
	 * Test class for {@link WorkflowCreationAfterSaveEventListener#afterSave(java.util.Collection)} for page update.
	 */
	@Test
	public void afterSaveComponentUpdate()
	{
		Mockito.when(saveEvent.getPk()).thenReturn(PK3_);
		callForSave(AfterSaveEvent.UPDATE);
		Mockito.verify(cmpntWklflwTriggerService).triggerWorkflow(cmsComponent);
	}

	/**
	 * Test class for {@link WorkflowCreationAfterSaveEventListener#afterSave(java.util.Collection)} for page create.
	 */
	@Test
	public void afterSaveComponentCreate()
	{
		Mockito.when(saveEvent.getPk()).thenReturn(PK3_);
		callForSave(AfterSaveEvent.CREATE);
		Mockito.verify(cmpntWklflwTriggerService).triggerWorkflow(cmsComponent);
	}

	private void callForSave(final int eventType)
	{
		Mockito.when(Integer.valueOf(saveEvent.getType())).thenReturn(Integer.valueOf(eventType));
		testClass.afterSave(Collections.singletonList(saveEvent));
	}
}
