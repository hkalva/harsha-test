/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.lyonscg.workflowsapi.process.email.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.lyonscg.workflowsapi.model.CockpitProcessModel;


/**
 * Test class for {@link FindGenerateEmailAction}.
 */
@UnitTest
public class FindGenerateEmailActionTest
{
	private static final String GENERATE_EMAIL_ACTION_KEY = "pagePendingApprovalGenerateEmailAction";
	private static final String GENERATE_EMAIL_ACTION_VALUE = "pagePendingApprovalGenerateEmailAction";
	@InjectMocks
	private FindGenerateEmailAction testClass;
	private String cockpitProcessEmailGeneratorAttribute;
	private Map<String, String> emailGeneratorMapping;
	@Mock
	private CockpitProcessModel cockpitProcess;
	@Mock
	private BusinessProcessParameterModel businessProcessParam;

	/**
	 * Setup test data.
	 */
	@Before
	public void setUp()
	{
		testClass = new FindGenerateEmailAction();
		MockitoAnnotations.initMocks(this);
		cockpitProcessEmailGeneratorAttribute = "emailGenerator";
		testClass.setCockpitProcessEmailGeneratorAttribute(cockpitProcessEmailGeneratorAttribute);
		emailGeneratorMapping = new HashMap<String, String>(1);
		testClass.setEmailGeneratorMapping(emailGeneratorMapping);
		emailGeneratorMapping.put(GENERATE_EMAIL_ACTION_KEY, GENERATE_EMAIL_ACTION_VALUE);
		Mockito.when(cockpitProcess.getContextParameters()).thenReturn(Collections.singletonList(businessProcessParam));
		Mockito.when(businessProcessParam.getName()).thenReturn(cockpitProcessEmailGeneratorAttribute);
		Mockito.when(businessProcessParam.getValue()).thenReturn(GENERATE_EMAIL_ACTION_KEY);
	}

	/**
	 * Positive test case for {@link FindGenerateEmailAction#execute(CockpitProcessModel)}
	 */
	@Test
	public void executeTest()
	{
		Assert.assertEquals(GENERATE_EMAIL_ACTION_VALUE, testClass.execute(cockpitProcess));
	}

	/**
	 * NOK test case for {@link FindGenerateEmailAction#execute(CockpitProcessModel)}
	 */
	@Test
	public void executeNOKTest() throws RetryLaterException, Exception
	{
		Mockito.when(businessProcessParam.getValue()).thenReturn("UNKNOWN");
		Assert.assertEquals("NOK", testClass.execute(cockpitProcess));

		Mockito.when(businessProcessParam.getValue()).thenReturn(null);
		Assert.assertEquals("NOK", testClass.execute(cockpitProcess));

		Mockito.when(businessProcessParam.getName()).thenReturn("UNKNOWN");
		Assert.assertEquals("NOK", testClass.execute(cockpitProcess));
	}

}
