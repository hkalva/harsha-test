/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.lyonscg.workflowsapi.process.email.actions;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.email.CMSEmailPageService;
import de.hybris.platform.acceleratorservices.email.EmailGenerationService;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.google.common.collect.Lists;
import com.lyonscg.b2btransactionalemails.process.strategies.impl.EmailProcessContextResolutionStrategy;
import com.lyonscg.workflowsapi.service.WorkflowsService;


/**
 * To test ProductPublishedGenerateEmailAction class.
 */
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(LogRunListener.class)
@UnitTest
public class CommonWorkflowGenerateEmailActionTest
{
	private static final String PRODUCT_PUBLISHER_GROUP = "cockpit.product.publisher.usergroup";
	@InjectMocks
	private CommonWorkflowGenerateEmailAction testClass;
	@Mock
	private BusinessProcessModel businessProcessModel;
	@Mock
	private EmailProcessContextResolutionStrategy b2bContextResolutionStrategy;
	@Mock
	private CMSEmailPageService cmsEmailPageService;
	@Mock
	private CatalogVersionModel contentCatalogVersion;
	@Mock
	private EmailPageModel emailPageModel;
	@Mock
	private EmailGenerationService emailGenerationService;
	@Mock
	private EmailMessageModel emailMessageModel;
	@Mock
	private ModelService modelService;
	@Mock
	private WorkflowsService workflowsService;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private Configuration configuration;
	private static final String employeeGroupName = "testEmployeeGroupName";
	private final List<String> userGroupIdKeys = new ArrayList<String>();

	/**
	 * Setup test data.
	 */
	@Before
	public void setUp()
	{
		testClass = new CommonWorkflowGenerateEmailAction();
		MockitoAnnotations.initMocks(this);
		userGroupIdKeys.add(PRODUCT_PUBLISHER_GROUP);
		testClass.setUserGroupIdKeys(userGroupIdKeys);
	}

	/**
	 * To test executeAction method when the recipients list is empty.
	 */
	@Test
	public void testExecuteActionWhenThereIsEmptyRecipients()
	{

		Mockito.when(b2bContextResolutionStrategy.getContentCatalogVersion(businessProcessModel)).thenReturn(contentCatalogVersion);

		Mockito.when(cmsEmailPageService.getEmailPageForFrontendTemplate(Mockito.anyString(), Mockito.eq(contentCatalogVersion)))
				.thenReturn(emailPageModel);

		Mockito.when(emailGenerationService.generate(businessProcessModel, emailPageModel)).thenReturn(emailMessageModel);

		Mockito.when(configurationService.getConfiguration()).thenReturn(configuration);
		Mockito.when(configuration.getString(PRODUCT_PUBLISHER_GROUP)).thenReturn(employeeGroupName);
		final List<EmployeeModel> employees = new ArrayList<>();
		Mockito.when(workflowsService.getEmployeesInUserGroup(employeeGroupName)).thenReturn(employees);

		final List<EmailAddressModel> recipients = new ArrayList<>();
		Mockito.when(workflowsService.getEmailAddressesOfEmployees(Mockito.anyList())).thenReturn(recipients);

		assertEquals("Transaction must be NOK", Transition.NOK, testClass.executeAction(businessProcessModel));

	}

	/**
	 * To test executeAction method when the recipients list is not empty.
	 */
	@Test
	public void testExecuteActionWhenEmailMessageContainsRecipients()
	{

		final int NUM_OF_EMAILS = 100;

		businessProcessModel = new BusinessProcessModel();
		Mockito.when(b2bContextResolutionStrategy.getContentCatalogVersion(businessProcessModel)).thenReturn(contentCatalogVersion);

		Mockito.when(cmsEmailPageService.getEmailPageForFrontendTemplate(Mockito.anyString(), Mockito.eq(contentCatalogVersion)))
				.thenReturn(emailPageModel);

		emailMessageModel = new EmailMessageModel();
		Mockito.when(emailGenerationService.generate(businessProcessModel, emailPageModel)).thenReturn(emailMessageModel);

		Mockito.when(configurationService.getConfiguration()).thenReturn(configuration);
		Mockito.when(configuration.getString(PRODUCT_PUBLISHER_GROUP)).thenReturn(employeeGroupName);
		final List<EmployeeModel> employees = new ArrayList<>();
		Mockito.when(workflowsService.getEmployeesInUserGroup(employeeGroupName)).thenReturn(employees);

		//create a list of NUM_OF_EMAILS recipients
		final List<EmailAddressModel> recipients = new ArrayList<>();
		for (int i = 0; i < NUM_OF_EMAILS; i++)
		{
			recipients.add(Mockito.mock(EmailAddressModel.class));
		}

		Mockito.when(workflowsService.getEmailAddressesOfEmployees(Mockito.anyList())).thenReturn(recipients);

		final List<EmailMessageModel> emailMessageModelList = Lists.newArrayList(emailMessageModel);
		businessProcessModel.setEmails(emailMessageModelList);

		assertEquals("Transaction must be OK", Transition.OK, testClass.executeAction(businessProcessModel));

		assertEquals("Number of email recipients must match the number of recipients to the email message ", NUM_OF_EMAILS,
				emailMessageModel.getToAddresses().size());

	}

	/**
	 * Verifies that if the Transition.NOK is the output of super.executeAction(), then this method also returns
	 * Transition.NOK.
	 */
	@Test
	public void testTransitionNotOk() throws RetryLaterException
	{
		businessProcessModel = Mockito.mock(BusinessProcessModel.class);
		Mockito.when(b2bContextResolutionStrategy.getContentCatalogVersion(businessProcessModel)).thenReturn(null);
		assertEquals("Transaction must be NOK", Transition.NOK, testClass.executeAction(businessProcessModel));
	}

}
