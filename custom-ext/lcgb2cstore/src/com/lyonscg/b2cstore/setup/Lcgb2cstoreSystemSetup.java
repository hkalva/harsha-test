/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.lyonscg.b2cstore.setup;


import com.lyonscg.rapidsetup.setup.AbstractRapidSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;


import com.lyonscg.b2cstore.constants.Lcgb2cstoreConstants;

import java.util.Arrays;
import java.util.List;


@SystemSetup(extension = Lcgb2cstoreConstants.EXTENSIONNAME)
public class Lcgb2cstoreSystemSetup extends AbstractRapidSystemSetup
{

	@Override
	protected String getProductCatalogName() {
		return Lcgb2cstoreConstants.LCGB2C;
	}

	@Override
	protected List<String> getContentCatalogNames() {
		return Arrays.asList(Lcgb2cstoreConstants.LCGB2C);
	}

	@Override
	protected List<String> getStoreNames() {
		return Arrays.asList(Lcgb2cstoreConstants.LCGB2C);
	}
}
