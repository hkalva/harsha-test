/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.lyonscg.b2cstore.constants;

/**
 * Global class for all Lcgb2cstore constants. You can add global constants for your extension into this class.
 */
public final class Lcgb2cstoreConstants extends GeneratedLcgb2cstoreConstants
{
	public static final String EXTENSIONNAME = "lcgb2cstore";

	private Lcgb2cstoreConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String LCGB2C = "lcgb2c";
}
