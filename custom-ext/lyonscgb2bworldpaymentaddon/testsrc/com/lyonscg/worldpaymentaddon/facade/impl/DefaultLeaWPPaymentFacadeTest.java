package com.lyonscg.worldpaymentaddon.facade.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.worldpay.order.data.WorldpayAdditionalInfoData;
import com.worldpay.strategy.WorldpayAuthenticatedShopperIdStrategy;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.services.B2BOrderService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.dto.CardInfo;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.model.ModelService;


/**
 *
 * Test class for DefaultLeaWPPaymentFacade.
 */
@UnitTest
public class DefaultLeaWPPaymentFacadeTest
{
    @InjectMocks
    private DefaultLeaWPPaymentFacade defaultLeaWPPaymentFacade;
    private static final String ACCEPTED_STATUS = "ACCEPTED";
    @Mock
    private B2BOrderService b2bOrderService;
    @Mock
    private PaymentService paymentService;
    @Mock
    private WorldpayAuthenticatedShopperIdStrategy worldpayAuthenticatedShopperIdStrategy;
    @Mock
    private ModelService modelService;
    @Mock
    private OrderModel order;
    @Mock
    private B2BCustomerModel customer;
    @Mock
    private PaymentTransactionEntryModel paymentTransEntry;
    @Mock
    private PaymentTransactionModel paymentTrans;
    private String abstractOrderCode = "abstractOrderCode";
    private String customerId = "customerId";
    private WorldpayAdditionalInfoData worldPayAdditionalInfoData;

    @Before
    public void setup() throws InvalidCartException
    {
        defaultLeaWPPaymentFacade = new DefaultLeaWPPaymentFacade();
        MockitoAnnotations.initMocks(this);
        worldPayAdditionalInfoData = new WorldpayAdditionalInfoData();
        Mockito.doReturn(order).when(b2bOrderService).getOrderForCode(abstractOrderCode);
        Mockito.doReturn(customer).when(order).getUser();
        Mockito.doReturn(customerId).when(worldpayAuthenticatedShopperIdStrategy).getAuthenticatedShopperId(customer);
    }

    @Test
    public void authorizeSuccessTest()
    {
        Mockito.doReturn(paymentTransEntry).when(paymentService)
                .authorize(Mockito.any(OrderModel.class), Mockito.any(CardInfo.class));
        Mockito.doReturn(paymentTrans).when(paymentTransEntry).getPaymentTransaction();
        Mockito.doReturn(ACCEPTED_STATUS).when(paymentTransEntry).getTransactionStatus();
        Assert.assertTrue(defaultLeaWPPaymentFacade.authorize(abstractOrderCode, worldPayAdditionalInfoData));
        Mockito.verify(paymentTrans).setOrder(order);
    }

    @Test
    public void authorizeFailTest()
    {
        Mockito.doReturn(paymentTransEntry).when(paymentService)
                .authorize(Mockito.any(OrderModel.class), Mockito.any(CardInfo.class));
        Mockito.doReturn(paymentTrans).when(paymentTransEntry).getPaymentTransaction();
        Mockito.doReturn(null).when(paymentTransEntry).getTransactionStatus();
        Assert.assertFalse(defaultLeaWPPaymentFacade.authorize(abstractOrderCode, worldPayAdditionalInfoData));
        Mockito.verify(paymentTrans).setOrder(order);
    }

    @Test
    public void authorizeNullTest()
    {
        Mockito.doReturn(null).when(paymentService).authorize(Mockito.any(OrderModel.class), Mockito.any(CardInfo.class));
        Assert.assertFalse(defaultLeaWPPaymentFacade.authorize(abstractOrderCode, worldPayAdditionalInfoData));
    }
}
