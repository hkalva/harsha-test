package com.lyonscg.worldpaymentaddon.strategy.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.worldpay.exception.WorldpayException;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;


/**
 * Test class for LeaWPAuthenticatedShopperIdStrategy.
 *
 */
@UnitTest
public class LeaWPAuthenticatedShopperIdStrategyTest
{
    @InjectMocks
    private LeaWPAuthenticatedShopperIdStrategy leaWPAuthenticatedShopperIdStrategy;
    @Mock
    private B2BCustomerModel user;
    @Mock
    private EmployeeModel employee;

    @Before
    public void setup() throws WorldpayException
    {
        leaWPAuthenticatedShopperIdStrategy = new LeaWPAuthenticatedShopperIdStrategy();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAuthenticatedShopperIdTest()
    {
        String userId = "userId";
        Mockito.doReturn(userId).when(user).getCustomerID();
        Assert.assertEquals(userId, leaWPAuthenticatedShopperIdStrategy.getAuthenticatedShopperId(user));
    }

    @Test
    public void getAuthenticatedShopperIdBlankCustIdTest()
    {
        Mockito.doReturn(null).when(user).getCustomerID();
        String userId = "userId";
        Mockito.doReturn(userId).when(user).getUid();
        Assert.assertEquals(userId, leaWPAuthenticatedShopperIdStrategy.getAuthenticatedShopperId(user));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getAuthenticatedShopperIdEmployeeTest()
    {
        leaWPAuthenticatedShopperIdStrategy.getAuthenticatedShopperId(employee);
    }
}
