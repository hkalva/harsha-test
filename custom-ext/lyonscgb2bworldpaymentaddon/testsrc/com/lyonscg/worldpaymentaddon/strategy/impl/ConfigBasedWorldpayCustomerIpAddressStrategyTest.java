/**
 *
 */
package com.lyonscg.worldpaymentaddon.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.worldpay.exception.WorldpayException;


/**
 * Test class for ConfigBasedWorldpayCustomerIpAddressStrategy.
 *
 */
@UnitTest
public class ConfigBasedWorldpayCustomerIpAddressStrategyTest
{
	@InjectMocks
	private ConfigBasedWorldpayCustomerIpAddressStrategy configBasedWorldpayCustomerIpAddressStrategy;
	@Mock
	private ConfigurationService configurationService;
	private final String headerName = "headerName", headerConfigProperty = "headerConfigProperty", configHeader = "configHeader",
			headerValue = "headerValue", configHeaderValue = "configHeaderValue", remoteAdd = "remoteAdd", defaultIpSeparator = ",",
			ipIndexConfig = "ipIndexConfig", ipSeparatorConfig = "ipSeparatorConfig";
	@Mock
	private HttpServletRequest request;
	@Mock
	private Configuration configuration;
	private final int defaultIpIndex = 0;

	@Before
	public void setup() throws WorldpayException
	{
		configBasedWorldpayCustomerIpAddressStrategy = new ConfigBasedWorldpayCustomerIpAddressStrategy();
		MockitoAnnotations.initMocks(this);
		configBasedWorldpayCustomerIpAddressStrategy.setHeaderConfigProperty(headerConfigProperty);
		configBasedWorldpayCustomerIpAddressStrategy.setHeaderName(headerName);
		configBasedWorldpayCustomerIpAddressStrategy.setDefaultIpIndex(defaultIpIndex);
		configBasedWorldpayCustomerIpAddressStrategy.setDefaultIpSeparator(defaultIpSeparator);
		configBasedWorldpayCustomerIpAddressStrategy.setIpIndexConfig(ipIndexConfig);
		configBasedWorldpayCustomerIpAddressStrategy.setIpSeparatorConfig(ipSeparatorConfig);

		Mockito.doReturn(configuration).when(configurationService).getConfiguration();
		Mockito.doReturn(configHeader).when(configuration).getString(headerConfigProperty, headerName);
		Mockito.doReturn(defaultIpSeparator).when(configuration).getString(ipSeparatorConfig, defaultIpSeparator);
		Mockito.doReturn(Integer.valueOf(defaultIpIndex)).when(configuration).getInt(ipIndexConfig, defaultIpIndex);
		Mockito.doReturn(headerValue).when(request).getHeader(headerName);
		Mockito.doReturn(remoteAdd).when(request).getRemoteAddr();
		Mockito.doReturn(configHeaderValue).when(request).getHeader(configHeader);
	}

	@Test
	public void getCustomerIpConfigTest()
	{
		final String ipAddress = configBasedWorldpayCustomerIpAddressStrategy.getCustomerIp(request);
		Assert.assertEquals(configHeaderValue, ipAddress);
	}

	@Test
	public void getCustomerIpConfigWithCommaTest()
	{
		final String firstIpAddress = "20.200.2.254";
		Mockito.doReturn(firstIpAddress + ", 20.0.90.5, 127.0.0.1").when(request).getHeader(configHeader);
		final String ipAddress = configBasedWorldpayCustomerIpAddressStrategy.getCustomerIp(request);
		Assert.assertEquals(firstIpAddress, ipAddress);
	}

	@Test
	public void getCustomerIpConfigDefTest()
	{
		Mockito.doReturn(headerName).when(configuration).getString(headerConfigProperty, headerName);
		final String ipAddress = configBasedWorldpayCustomerIpAddressStrategy.getCustomerIp(request);
		Assert.assertEquals(headerValue, ipAddress);
	}

	@Test
	public void getCustomerIpNoHeaderTest()
	{
		Mockito.doReturn(null).when(request).getHeader(Mockito.anyString());
		final String ipAddress = configBasedWorldpayCustomerIpAddressStrategy.getCustomerIp(request);
		Assert.assertEquals(remoteAdd, ipAddress);
	}
}
