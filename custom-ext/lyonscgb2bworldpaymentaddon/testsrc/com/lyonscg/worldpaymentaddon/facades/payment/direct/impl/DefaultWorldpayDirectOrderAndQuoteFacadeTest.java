package com.lyonscg.worldpaymentaddon.facades.payment.direct.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.lyonscg.worldpaymentaddon.service.payment.WorldpayDirectOrderAndQuoteService;
import com.worldpay.data.CSEAdditionalAuthInfo;
import com.worldpay.exception.WorldpayConfigurationException;
import com.worldpay.exception.WorldpayException;
import com.worldpay.merchant.WorldpayMerchantInfoService;
import com.worldpay.order.data.WorldpayAdditionalInfoData;
import com.worldpay.service.model.MerchantInfo;
import com.worldpay.strategy.WorldpayAuthenticatedShopperIdStrategy;

import de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade;
import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.commerceservices.enums.UiExperienceLevel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;

/**@author lyonscg.
 *Test class for default worldpay for direct order and quote facade.
 */
public class DefaultWorldpayDirectOrderAndQuoteFacadeTest
{
    @InjectMocks
    private DefaultWorldpayDirectOrderAndQuoteFacade defaultWorldpayDirectOrderAndQuoteFacade;
    @Mock
    private CartService cartService;
    @Mock
    private WorldpayDirectOrderAndQuoteService worldpayDirectOrderService;
    @Mock
    private WorldpayMerchantInfoService worldpayMerchantInfoService;
    @Mock
    private WorldpayAuthenticatedShopperIdStrategy worldpayAuthenticatedShopperIdStrategy;
    @Mock
    private UiExperienceService uiExperienceService;
    @Mock
    private CheckoutFlowFacade checkoutFlowFacade;
    @Mock
    private CartModel cart;
    @Mock
    private MerchantInfo merchantInfo;
    private CSEAdditionalAuthInfo cseAdditionalAuthInfo;
    private WorldpayAdditionalInfoData worldPayAdditionalInfoData;

    /**Setting up mock objects for testing.
     * @throws InvalidCartException
     * @throws WorldpayConfigurationException
     */
    @Before
    public void setup() throws InvalidCartException, WorldpayConfigurationException
    {
        defaultWorldpayDirectOrderAndQuoteFacade = new DefaultWorldpayDirectOrderAndQuoteFacade();
        MockitoAnnotations.initMocks(this);
        cseAdditionalAuthInfo = new CSEAdditionalAuthInfo();
        worldPayAdditionalInfoData = new WorldpayAdditionalInfoData();

        Mockito.doReturn(Boolean.TRUE).when(cartService).hasSessionCart();
        Mockito.doReturn(cart).when(cartService).getSessionCart();
        Mockito.doReturn(UiExperienceLevel.RESPONSIVE).when(uiExperienceService).getUiExperienceLevel();
        Mockito.doReturn(merchantInfo).when(worldpayMerchantInfoService).getCurrentSiteMerchant(UiExperienceLevel.RESPONSIVE);
    }

    @Test
    public void tokenizeTest() throws WorldpayException
    {
        Assert.assertTrue(defaultWorldpayDirectOrderAndQuoteFacade.tokenize(cseAdditionalAuthInfo, worldPayAdditionalInfoData));
        Mockito.verify(worldpayDirectOrderService).createToken(merchantInfo, cart, cseAdditionalAuthInfo,
                worldPayAdditionalInfoData);
        Mockito.verify(checkoutFlowFacade).prepareCartForCheckout();
    }

    @Test(expected = WorldpayException.class)
    public void tokenizeTestException() throws WorldpayException
    {
        Mockito.doThrow(WorldpayException.class).when(worldpayDirectOrderService)
                .createToken(merchantInfo, cart, cseAdditionalAuthInfo, worldPayAdditionalInfoData);
        defaultWorldpayDirectOrderAndQuoteFacade.tokenize(cseAdditionalAuthInfo, worldPayAdditionalInfoData);
    }

    @Test
    public void tokenizeTestFailure() throws WorldpayException
    {
        Mockito.doThrow(WorldpayConfigurationException.class).when(worldpayMerchantInfoService)
                .getCurrentSiteMerchant(UiExperienceLevel.RESPONSIVE);
        Assert.assertFalse(defaultWorldpayDirectOrderAndQuoteFacade.tokenize(cseAdditionalAuthInfo, worldPayAdditionalInfoData));
    }

    @Test
    public void processQuoteTest()
    {
        defaultWorldpayDirectOrderAndQuoteFacade.processQuote();
        Mockito.verify(worldpayDirectOrderService).processQuote(cart);
    }

}
