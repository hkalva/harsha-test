package com.lyonscg.worldpaymentaddon.facades.order.impl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.worldpay.core.checkout.WorldpayCheckoutService;

import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;

/**@author lyonscg.
 * Test class for lea worldpay payment checkout Facade.
 */
public class DefaultLeaWPPaymentCheckoutFacadeTest
{
    @InjectMocks
    private DefaultLeaWPPaymentCheckoutFacade defaultLeaWPPaymentCheckoutFacade;

    @Mock
    private CheckoutFacade checkoutFacade;
    @Mock
    private CartService cartService;
    @Mock
    private CartModel cart;
    @Mock
    private PaymentInfoModel paymentInfo;
    @Mock
    private AddressModel billingAddress;
    @Mock
    private WorldpayCheckoutService worldpayCheckoutService;

  /**setting all mock values.
   * @throws InvalidCartException - for testing.
   */
    @Before
    public void setup() throws InvalidCartException
    {
        defaultLeaWPPaymentCheckoutFacade = new DefaultLeaWPPaymentCheckoutFacade();
        MockitoAnnotations.initMocks(this);
        Mockito.doReturn(Boolean.TRUE).when(checkoutFacade).hasCheckoutCart();
        Mockito.doReturn(cart).when(cartService).getSessionCart();
    }


    @Test
    public void setAddressInPaymentAsCartBillingAddressTest()
    {
        Mockito.doReturn(paymentInfo).when(cart).getPaymentInfo();
        Mockito.doReturn(billingAddress).when(paymentInfo).getBillingAddress();
        defaultLeaWPPaymentCheckoutFacade.setAddressInPaymentAsCartBillingAddress();
        Mockito.verify(worldpayCheckoutService).setPaymentAddress(cart, billingAddress);
    }

    @Test
    public void setAddressInPaymentAsCartBillingAddressNoAddressTest()
    {
        Mockito.doReturn(paymentInfo).when(cart).getPaymentInfo();
        Mockito.doReturn(null).when(paymentInfo).getBillingAddress();
        defaultLeaWPPaymentCheckoutFacade.setAddressInPaymentAsCartBillingAddress();
        Mockito.verify(worldpayCheckoutService, Mockito.never()).setPaymentAddress(cart, billingAddress);
    }

    @Test
    public void setAddressInPaymentAsCartBillingAddressNoPaymentTest()
    {
        Mockito.doReturn(null).when(cart).getPaymentInfo();
        defaultLeaWPPaymentCheckoutFacade.setAddressInPaymentAsCartBillingAddress();
        Mockito.verify(worldpayCheckoutService, Mockito.never()).setPaymentAddress(cart, billingAddress);
    }
}
