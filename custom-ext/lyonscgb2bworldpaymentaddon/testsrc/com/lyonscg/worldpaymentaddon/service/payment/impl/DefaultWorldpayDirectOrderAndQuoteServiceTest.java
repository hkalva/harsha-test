/**
 *
 */
package com.lyonscg.worldpaymentaddon.service.payment.impl;

import static com.worldpay.enums.token.TokenEvent.CONFLICT;
import static com.worldpay.enums.token.TokenEvent.NEW;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.worldpay.core.services.WorldpayPaymentInfoService;
import com.worldpay.data.CSEAdditionalAuthInfo;
import com.worldpay.exception.WorldpayException;
import com.worldpay.order.data.WorldpayAdditionalInfoData;
import com.worldpay.service.WorldpayServiceGateway;
import com.worldpay.service.model.ErrorDetail;
import com.worldpay.service.model.MerchantInfo;
import com.worldpay.service.model.token.TokenDetails;
import com.worldpay.service.model.token.TokenReply;
import com.worldpay.service.payment.WorldpayOrderService;
import com.worldpay.service.payment.request.WorldpayRequestFactory;
import com.worldpay.service.request.CreateTokenServiceRequest;
import com.worldpay.service.request.UpdateTokenServiceRequest;
import com.worldpay.service.response.CreateTokenResponse;
import com.worldpay.service.response.UpdateTokenResponse;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.model.ModelService;


/**
 * Test class for LyonscgWorldpayDirectOrderService.
 *
 */
@UnitTest
public class DefaultWorldpayDirectOrderAndQuoteServiceTest
{
    @InjectMocks
    private DefaultWorldpayDirectOrderAndQuoteService lyonscgWorldpayDirectOrderService;
    @Mock
    private WorldpayRequestFactory worldpayRequestFactory;
    @Mock
    private WorldpayOrderService worldpayOrderService;
    @Mock
    private WorldpayServiceGateway worldpayServiceGateway;
    @Mock
    private CreateTokenServiceRequest createTokenRequest;
    @Mock
    private CreateTokenResponse createTokenResponse;
    @Mock
    private MerchantInfo merchantInfo;
    @Mock
    private CartModel cartModel;
    @Mock
    private CSEAdditionalAuthInfo cseAdditionalAuthInfo;
    @Mock
    private WorldpayAdditionalInfoData worldpayAdditionalInfoData;
    @Mock
    private TokenReply tokenReply;
    @Mock
    private TokenDetails tokenDetails;
    @Mock
    private UpdateTokenServiceRequest updateTokenServiceRequest;
    @Mock
    private UpdateTokenResponse updateTokenResponse;
    @Mock
    private WorldpayPaymentInfoService worldpayPaymentInfoService;
    @Mock
    private CreditCardPaymentInfoModel creditCardPaymentInfoModel;
    @Mock
    private ModelService modelService;
    @Mock
    private ErrorDetail errorDetail;
    @Mock
    private PaymentInfoModel paymentInfoModel;
    @Mock
    private AddressModel paymentAddress, clonedPaymentAddress;
    @Mock
    private CommerceCheckoutService commerceCheckoutService;
    @Mock
    private EnumerationService enumerationService;

    /**
     * Setup for testing.
     */
    @Before
    public void setup() throws WorldpayException
    {
        lyonscgWorldpayDirectOrderService = new DefaultWorldpayDirectOrderAndQuoteService();
        MockitoAnnotations.initMocks(this);

        Mockito.doReturn(worldpayServiceGateway).when(worldpayOrderService).getWorldpayServiceGateway();
        Mockito.doReturn(createTokenRequest).when(worldpayRequestFactory)
                .buildTokenRequest(merchantInfo, cartModel, cseAdditionalAuthInfo, worldpayAdditionalInfoData);
        Mockito.doReturn(createTokenResponse).when(worldpayServiceGateway).createToken(createTokenRequest);
        Mockito.doReturn(tokenReply).when(createTokenResponse).getToken();
        Mockito.doReturn(tokenDetails).when(tokenReply).getTokenDetails();

        Mockito.doReturn(updateTokenServiceRequest).when(worldpayRequestFactory)
                .buildTokenUpdateRequest(merchantInfo, cseAdditionalAuthInfo, worldpayAdditionalInfoData, createTokenResponse);
        Mockito.doReturn(updateTokenResponse).when(worldpayServiceGateway).updateToken(updateTokenServiceRequest);
        Mockito.doReturn(null).when(worldpayPaymentInfoService).updateCreditCardPaymentInfo(cartModel, updateTokenServiceRequest);
        Mockito.doReturn(creditCardPaymentInfoModel).when(worldpayPaymentInfoService)
                .createCreditCardPaymentInfo(cartModel, createTokenResponse, false);

        Mockito.doReturn(paymentInfoModel).when(cartModel).getPaymentInfo();
        Mockito.doReturn(paymentAddress).when(cartModel).getPaymentAddress();
        Mockito.doReturn(clonedPaymentAddress).when(modelService).clone(paymentAddress);
    }

    /**
     * Test for lyonscgWorldpayDirectOrderService.createToken CONFLICT.
     */
    @Test
    public void createTokenConflictUpdateTest() throws WorldpayException
    {
        Mockito.doReturn(CONFLICT.name()).when(tokenDetails).getTokenEvent();
        lyonscgWorldpayDirectOrderService.createToken(merchantInfo, cartModel, cseAdditionalAuthInfo, worldpayAdditionalInfoData);
        Mockito.verify(worldpayPaymentInfoService).createCreditCardPaymentInfo(cartModel, createTokenResponse,
                BooleanUtils.isTrue(cseAdditionalAuthInfo.getSaveCard()));
        Mockito.verify(cartModel).setPaymentInfo(creditCardPaymentInfoModel);
        Mockito.verify(modelService).save(cartModel);
    }

    /**
     * Test for lyonscgWorldpayDirectOrderService.createToken to throw WorldpayException.
     */
    @Test(expected = WorldpayException.class)
    public void createTokenConflictUpdateErrorTest() throws WorldpayException
    {
        Mockito.doReturn(CONFLICT.name()).when(tokenDetails).getTokenEvent();
        Mockito.doReturn(Boolean.TRUE).when(updateTokenResponse).isError();
        Mockito.doReturn(errorDetail).when(updateTokenResponse).getErrorDetail();
        Mockito.doReturn("errorMessage").when(errorDetail).getMessage();
        lyonscgWorldpayDirectOrderService.createToken(merchantInfo, cartModel, cseAdditionalAuthInfo, worldpayAdditionalInfoData);
    }

    /**
     * Test for lyonscgWorldpayDirectOrderService.createToken NEW.
     */
    @Test
    public void createTokenNewTokenTest() throws WorldpayException
    {
        Mockito.doReturn(NEW.name()).when(tokenDetails).getTokenEvent();
        lyonscgWorldpayDirectOrderService.createToken(merchantInfo, cartModel, cseAdditionalAuthInfo, worldpayAdditionalInfoData);
        Mockito.verify(worldpayPaymentInfoService).createCreditCardPaymentInfo(cartModel, createTokenResponse,
                BooleanUtils.isTrue(cseAdditionalAuthInfo.getSaveCard()));
        Mockito.verify(worldpayPaymentInfoService, Mockito.never()).updateCreditCardPaymentInfo(cartModel,
                updateTokenServiceRequest);
        Mockito.verify(cartModel).setPaymentInfo(creditCardPaymentInfoModel);
        Mockito.verify(modelService).save(cartModel);
    }

    /**
     * Test for lyonscgWorldpayDirectOrderService.createToken to throw WorldpayException.
     */
    @Test(expected = WorldpayException.class)
    public void createTokenErrorTest() throws WorldpayException
    {
        Mockito.doReturn(Boolean.TRUE).when(createTokenResponse).isError();
        Mockito.doReturn(errorDetail).when(createTokenResponse).getErrorDetail();
        Mockito.doReturn("errorMessage").when(errorDetail).getMessage();
        lyonscgWorldpayDirectOrderService.createToken(merchantInfo, cartModel, cseAdditionalAuthInfo, worldpayAdditionalInfoData);
    }

    /**
     * Test for lyonscgWorldpayDirectOrderService.processQuote.
     */
    @Test
    public void processQuoteTest()
    {
        lyonscgWorldpayDirectOrderService.processQuote(cartModel);
        Mockito.verify(commerceCheckoutService).setPaymentInfo(Mockito.any());
    }

    /**
     * Test for lyonscgWorldpayDirectOrderService.populateCardType.
     */
    @Test
    public void populateCardTypeTest()
    {
        String cardType = "visa";
        CreditCardType cardTypeEnum = CreditCardType.VISA;
        Mockito.doReturn(cardType).when(cseAdditionalAuthInfo).getCardType();
        Mockito.doReturn(cardTypeEnum).when(enumerationService)
                .getEnumerationValue(CreditCardType.class.getSimpleName(), cardType);
        lyonscgWorldpayDirectOrderService.populateCardType(cseAdditionalAuthInfo, creditCardPaymentInfoModel);
        Mockito.verify(creditCardPaymentInfoModel).setType(cardTypeEnum);
    }

    /**
     * Test for lyonscgWorldpayDirectOrderService.populateCardType with card type empty.
     */
    @Test
    public void populateCardTypeEmptyTest()
    {
        Mockito.doReturn(StringUtils.EMPTY).when(cseAdditionalAuthInfo).getCardType();
        lyonscgWorldpayDirectOrderService.populateCardType(cseAdditionalAuthInfo, creditCardPaymentInfoModel);
        Mockito.verify(creditCardPaymentInfoModel, Mockito.never()).setType(Mockito.any());
    }
}
