package com.lyonscg.worldpaymentaddon.service.payment.impl;

import java.math.BigDecimal;
import java.util.Currency;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.lyonscg.worldpaymentaddon.helpers.LeaWPPaymentHelper;
import com.worldpay.exception.WorldpayException;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.dto.CardInfo;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;


/**
 * Test class for LeaWPPaymentServiceImpl.
 *
 */
@UnitTest
public class LeaWPPaymentServiceImplTest
{
    private static final String ACCEPTED_STATUS = "ACCEPTED";
    @InjectMocks
    private LeaWPPaymentServiceImpl leaWPPaymentServiceImpl;
    private LeaWPPaymentServiceImpl spyLeaWPPaymentServiceImpl;
    @Mock
    private OrderModel order;
    @Mock
    private CardInfo card;
    @Mock
    private CreditCardPaymentInfoModel paymentInfo;
    @Mock
    private BaseStoreModel store;
    @Mock
    private PaymentTransactionEntryModel paymentTransactionEntry;
    @Mock
    private I18NService i18nService;
    @Mock
    private CurrencyModel currency;
    @Mock
    private CommerceCheckoutService commerceCheckoutService;
    @Mock
    private ModelService modelService;
    @Mock
    private LeaWPPaymentHelper leaWPPaymentHelper;
    private String subscriptionID = "subscriptionID";
    private String orderCode = "orderCode";

    /**
     * Setup for testing.
     */
    @Before
    public void setup() throws WorldpayException
    {
        leaWPPaymentServiceImpl = new LeaWPPaymentServiceImpl();
        MockitoAnnotations.initMocks(this);
        spyLeaWPPaymentServiceImpl = Mockito.spy(leaWPPaymentServiceImpl);
        Mockito.doReturn(subscriptionID).when(paymentInfo).getSubscriptionId();
        Mockito.doReturn(orderCode).when(order).getCode();
        Mockito.doReturn(currency).when(order).getCurrency();
        Mockito.doReturn("USD").when(currency).getIsocode();
        Mockito.doReturn(store).when(order).getStore();
    }

    /**
     * Test for lyonscgWorldpayDirectOrderService.authorize.
     */
    @Test
    public void authorizeTest() throws WorldpayException
    {
        Mockito.doReturn(paymentInfo).when(order).getPaymentInfo();
        Mockito.doReturn(paymentTransactionEntry)
                .when(spyLeaWPPaymentServiceImpl)
                .authorize(Mockito.anyString(), Mockito.any(BigDecimal.class), Mockito.any(Currency.class),
                        Mockito.any(AddressModel.class), Mockito.anyString(), Mockito.anyString(), Mockito.anyString());
        Assert.assertEquals(paymentTransactionEntry, spyLeaWPPaymentServiceImpl.authorize(order, card));
    }

    /**
     * Test for lyonscgWorldpayDirectOrderService.authorize with null paymentinfo.
     */
    @Test
    public void authorizeTestNull() throws WorldpayException
    {
        Mockito.doReturn(null).when(order).getPaymentInfo();
        Assert.assertNull(leaWPPaymentServiceImpl.authorize(order, card));
    }

    /**
     * Test for lyonscgWorldpayDirectOrderService.setPaymentEntryAsNonPending.
     */
    @Test
    public void setPaymentEntryAsNonPendingTest() throws WorldpayException
    {
        Mockito.doReturn(PaymentTransactionType.AUTHORIZATION).when(paymentTransactionEntry).getType();
        Mockito.doReturn(ACCEPTED_STATUS).when(paymentTransactionEntry).getTransactionStatus();
        Assert.assertEquals(paymentTransactionEntry, leaWPPaymentServiceImpl.setPaymentEntryAsNonPending(paymentTransactionEntry));
        Mockito.verify(paymentTransactionEntry).setPending(Boolean.FALSE);
        Mockito.verify(modelService).save(paymentTransactionEntry);
    }

    /**
     * Test for lyonscgWorldpayDirectOrderService.setPaymentEntryAsNonPending.
     */
    @Test
    public void setPaymentEntryAsNonPendingNullTest() throws WorldpayException
    {
        Mockito.doReturn(null).when(paymentTransactionEntry).getType();
        Assert.assertEquals(paymentTransactionEntry, leaWPPaymentServiceImpl.setPaymentEntryAsNonPending(paymentTransactionEntry));
        Mockito.verify(paymentTransactionEntry, Mockito.never()).setPending(Boolean.FALSE);
        Mockito.verify(modelService, Mockito.never()).save(paymentTransactionEntry);
    }

    /**
     * Test for lyonscgWorldpayDirectOrderService.setPaymentEntryAsNonPending with no AUTH.
     */
    @Test
    public void setPaymentEntryAsNonPendingTestNoAuth() throws WorldpayException
    {
        Mockito.doReturn(PaymentTransactionType.CAPTURE).when(paymentTransactionEntry).getType();
        Mockito.doReturn(ACCEPTED_STATUS).when(paymentTransactionEntry).getTransactionStatus();
        Assert.assertEquals(paymentTransactionEntry, leaWPPaymentServiceImpl.setPaymentEntryAsNonPending(paymentTransactionEntry));
        Mockito.verify(paymentTransactionEntry, Mockito.never()).setPending(Boolean.FALSE);
        Mockito.verify(modelService, Mockito.never()).save(paymentTransactionEntry);
    }

    /**
     * Test for lyonscgWorldpayDirectOrderService.setPaymentEntryAsNonPending with null TransactionStatus.
     */
    @Test
    public void setPaymentEntryAsNonPendingTestNotAccepted() throws WorldpayException
    {
        Mockito.doReturn(PaymentTransactionType.AUTHORIZATION).when(paymentTransactionEntry).getType();
        Mockito.doReturn(null).when(paymentTransactionEntry).getTransactionStatus();
        Assert.assertEquals(paymentTransactionEntry, leaWPPaymentServiceImpl.setPaymentEntryAsNonPending(paymentTransactionEntry));
        Mockito.verify(paymentTransactionEntry, Mockito.never()).setPending(Boolean.FALSE);
        Mockito.verify(modelService, Mockito.never()).save(paymentTransactionEntry);
    }

}
