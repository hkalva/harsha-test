package com.lyonscg.worldpaymentaddon.services.impl;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.lyonscg.worldpaymentaddon.helpers.LeaWPPaymentHelper;
import com.worldpay.enums.token.TokenEvent;
import com.worldpay.exception.WorldpayException;
import com.worldpay.service.model.Address;
import com.worldpay.service.model.Date;
import com.worldpay.service.model.PaymentReply;
import com.worldpay.service.model.payment.Card;
import com.worldpay.service.model.payment.PaymentType;
import com.worldpay.service.model.token.TokenDetails;
import com.worldpay.service.model.token.TokenReply;
import com.worldpay.service.response.CreateTokenResponse;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;


/**
 * Test class for LeaWPPaymentInfoService.
 *
 */
@UnitTest
public class LeaWPPaymentInfoServiceTest
{
    @InjectMocks
    private LeaWPPaymentInfoService leaWPPaymentInfoService;
    @Mock
    private ModelService modelService;
    @Mock
    private EnumerationService enumerationService;
    private LeaWPPaymentHelper leaWPPaymentHelper = new LeaWPPaymentHelper();
    @Mock
    private TokenReply tokenReply;
    @Mock
    private CartModel cartModel;
    @Mock
    private B2BCustomerModel customer;
    @Mock
    private CreateTokenResponse createTokenResponse;
    @Mock
    private TokenDetails tokenDetails;
    @Mock
    private CreditCardPaymentInfoModel creditCardPaymentInfoModel;
    @Mock
    private Date expiryDate;
    @Mock
    private PaymentTransactionModel paymentTransactionModel;
    @Mock
    private PaymentReply paymentReply;
    @Mock
    private ConfigurationService configurationService;
    @Mock
    private Configuration configuration;

    private String tokenId = "tokenId";
    private String cardNumberMaskChar = "*";
    private String year = "2016";
    private String month = "09";
    private String day = "06";
    private String cardNumber = "4111111111111111";
    private String maskCardNumber = "************1111";
    private String cvc = "123";
    private String cardHolderName = "cardHolderName";
    private Address cardAddress = null;
    private Date birthDate = null;
    private Date startDate = null;
    private String issueNumber = "issueNumber";
    private Card card;

    /**
     * Setup for testing.
     */
    @Before
    public void setup() throws WorldpayException
    {
        leaWPPaymentInfoService = new LeaWPPaymentInfoService();
        leaWPPaymentInfoService.setLeaWPPaymentHelper(leaWPPaymentHelper);
        MockitoAnnotations.initMocks(this);
        leaWPPaymentInfoService.setCardNumberMaskChar(cardNumberMaskChar);
        card = new Card(PaymentType.CARD_SSL, cardNumber, cvc, expiryDate, cardHolderName, cardAddress, birthDate, startDate,
                issueNumber);

        Mockito.doReturn(tokenReply).when(createTokenResponse).getToken();
        Mockito.doReturn(tokenDetails).when(tokenReply).getTokenDetails();
        Mockito.doReturn(customer).when(cartModel).getUser();
        Mockito.doReturn(tokenId).when(tokenDetails).getPaymentTokenID();
        Mockito.doReturn(creditCardPaymentInfoModel).when(modelService).create(CreditCardPaymentInfoModel.class);
        Mockito.doReturn(creditCardPaymentInfoModel).when(paymentTransactionModel).getInfo();
        Mockito.doReturn(creditCardPaymentInfoModel).when(modelService)
                .clone(creditCardPaymentInfoModel, CreditCardPaymentInfoModel.class);
        Mockito.doReturn(card).when(tokenReply).getPaymentInstrument();
        Mockito.doReturn(card).when(paymentReply).getCardDetails();
        Mockito.doReturn(expiryDate).when(tokenDetails).getPaymentTokenExpiry();
        Mockito.doReturn(year).when(expiryDate).getYear();
        Mockito.doReturn(month).when(expiryDate).getMonth();
        Mockito.doReturn(day).when(expiryDate).getDayOfMonth();
        Mockito.doReturn(configuration).when(configurationService).getConfiguration();

    }

    /**
     * Test for leaWPPaymentInfoService.createCreditCardPaymentInfo for existing card.
     */
    @Test
    public void createCreditCardPaymentInfoMatchTest()
    {
        Mockito.doReturn(TokenEvent.MATCH.name()).when(tokenDetails).getTokenEvent();
        leaWPPaymentInfoService.createCreditCardPaymentInfo(cartModel, createTokenResponse, true);
        Mockito.verify(creditCardPaymentInfoModel).setNumber(maskCardNumber);
    }

    /**
     * Test for leaWPPaymentInfoService.createCreditCardPaymentInfo for NEW card.
     */
    @Test
    public void createCreditCardPaymentInfoNewTest()
    {
        Mockito.doReturn(TokenEvent.NEW.name()).when(tokenDetails).getTokenEvent();
        leaWPPaymentInfoService.createCreditCardPaymentInfo(cartModel, createTokenResponse, true);
        Mockito.verify(creditCardPaymentInfoModel).setNumber(maskCardNumber);
    }

    /**
     * Test for leaWPPaymentInfoService.cloneAndSaveCreditCardWithPaymentInformation.
     */
    @Test
    public void cloneAndSaveCreditCardWithPaymentInformationTest()
    {
        leaWPPaymentInfoService.cloneAndSaveCreditCardWithPaymentInformation(paymentTransactionModel, paymentReply);
        Mockito.verify(creditCardPaymentInfoModel).setNumber(maskCardNumber);
    }

    /**
     * Test for leaWPPaymentInfoService.cloneAndSaveCreditCardWithTokenInformation.
     */
    @Test
    public void cloneAndSaveCreditCardWithTokenInformationTest()
    {
        leaWPPaymentInfoService.cloneAndSaveCreditCardWithTokenInformation(paymentTransactionModel, tokenReply, paymentReply);
        Mockito.verify(creditCardPaymentInfoModel).setNumber(maskCardNumber);
    }

}
