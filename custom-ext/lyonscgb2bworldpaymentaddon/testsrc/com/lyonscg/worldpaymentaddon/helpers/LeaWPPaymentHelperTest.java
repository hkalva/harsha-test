package com.lyonscg.worldpaymentaddon.helpers;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.store.BaseStoreModel;


/**
 * Test class for LeaWPPaymentHelper.
 */
public class LeaWPPaymentHelperTest
{
    private static final String MASKING_CHAR = "*";
    private static final String CREDIT_CARD_NUMBER = "4111111111111111";
    private LeaWPPaymentHelper leaWPPaymentHelper;
    private final Double totalPrice = Double.parseDouble("100");
    private final Double totalTax = Double.parseDouble("7.25");
    private final BigDecimal totalPriceWithoutTaxBD = new BigDecimal(totalPrice == null ? 0d : totalPrice.doubleValue())
            .setScale(2, RoundingMode.HALF_EVEN);
    @Mock
    private OrderModel order;
    @Mock
    private BaseStoreModel store;

    /**
     * Setup for testing.
     */
    @Before
    public void setup()
    {
        leaWPPaymentHelper = new LeaWPPaymentHelper();
        MockitoAnnotations.initMocks(this);
        Mockito.doReturn(store).when(order).getStore();
        Mockito.doReturn(totalPrice).when(order).getTotalPrice();
        Mockito.doReturn(totalTax).when(order).getTotalTax();
    }

    /**
     * Test for leaWPPaymentHelper.calcTotalWithTax.
     */
    @Test
    public void calcTotalWithTaxTest()
    {
        BigDecimal totalPriceWithTax = new BigDecimal(totalTax == null ? 0d : totalTax.doubleValue()).setScale(2,
                RoundingMode.HALF_EVEN).add(totalPriceWithoutTaxBD);
        Mockito.doReturn(Boolean.TRUE).when(order).getNet();
        Mockito.doReturn(Boolean.TRUE).when(store).getExternalTaxEnabled();
        Assert.assertEquals(totalPriceWithTax, leaWPPaymentHelper.calcTotalWithTax(order));
    }

    /**
     * Test for leaWPPaymentHelper.calcTotalWithTax with net tax false.
     */
    @Test
    public void calcTotalWithTaxNetFalseTest()
    {
        Mockito.doReturn(Boolean.FALSE).when(order).getNet();
        Mockito.doReturn(Boolean.TRUE).when(store).getExternalTaxEnabled();
        Assert.assertEquals(totalPriceWithoutTaxBD, leaWPPaymentHelper.calcTotalWithTax(order));
    }

    /**
     * Test for leaWPPaymentHelper.calcTotalWithTax with not external tax.
     */
    @Test
    public void calcTotalWithTaxNetNoExtTaxTest()
    {
        Mockito.doReturn(Boolean.TRUE).when(order).getNet();
        Mockito.doReturn(Boolean.FALSE).when(store).getExternalTaxEnabled();
        Assert.assertEquals(totalPriceWithoutTaxBD, leaWPPaymentHelper.calcTotalWithTax(order));
    }

    /**
     * Test for leaWPPaymentHelper.maskCardNumber.
     */
    @Test
    public void maskCardNumberTest()
    {
        int lengthOfCard = CREDIT_CARD_NUMBER.length();
        StringBuilder cardNumber = new StringBuilder();
        for (int i = 0; i < lengthOfCard; i++)
        {
            if (i > (lengthOfCard - 4 - 1))
            {
                cardNumber.append(CREDIT_CARD_NUMBER.charAt(i));
            }
            else
            {
                cardNumber.append(MASKING_CHAR);
            }
        }
        String maskedNumber = leaWPPaymentHelper.maskCardNumber(CREDIT_CARD_NUMBER, MASKING_CHAR);
        Assert.assertEquals(cardNumber.length(), maskedNumber.length());
        Assert.assertEquals(cardNumber.toString(), maskedNumber);
    }

    /**
     * Test for leaWPPaymentHelper.maskCardNumber with card number as null, Empty and mask char as null.
     */
    @Test
    public void maskCardNumberBlankTest()
    {
        Assert.assertEquals(StringUtils.EMPTY, leaWPPaymentHelper.maskCardNumber(StringUtils.EMPTY, MASKING_CHAR));
        Assert.assertEquals(null, leaWPPaymentHelper.maskCardNumber(null, MASKING_CHAR));
        Assert.assertEquals(CREDIT_CARD_NUMBER, leaWPPaymentHelper.maskCardNumber(CREDIT_CARD_NUMBER, null));
    }
}
