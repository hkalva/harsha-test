/**
 *
 */
package com.lyonscg.worldpaymentaddon.order.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Test class for WorldpayAbstractOrderPopulator.
 *
 */
@UnitTest
public class WorldpayAbstractOrderPopulatorTest
{
	private WorldpayAbstractOrderPopulator worldpayAbstractOrderPopulator;
	@Mock
	private Converter<AddressModel, AddressData> addressConverter;
	private AbstractOrderModel source;
	private AbstractOrderData target;
	@Mock
	private PaymentInfoModel paymentInfoModel;
	@Mock
	private CreditCardPaymentInfoModel creditCardPaymentInfoModel;
	private String worldpayOrderCode;
	private Integer worldpayDeclineCode;
	@Mock
	private AddressModel addressModel;
	private AddressData addressData;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		source = new AbstractOrderModel();
		target = new AbstractOrderData();
		addressData = new AddressData();
		worldpayAbstractOrderPopulator = new WorldpayAbstractOrderPopulator();
		worldpayAbstractOrderPopulator.setAddressConverter(addressConverter);

		source.setWorldpayOrderCode(worldpayOrderCode);
		source.setWorldpayDeclineCode(worldpayDeclineCode);
		source.setPaymentInfo(paymentInfoModel);

		Mockito.doReturn(addressData).when(addressConverter).convert(addressModel);
	}

	@Test
	public void populateTestAddressConverterNtCalledNonCredit()
	{
		worldpayAbstractOrderPopulator.populate(source, target);
		Assert.assertEquals(source.getWorldpayOrderCode(), target.getWorldpayOrderCode());
		Assert.assertEquals(source.getWorldpayDeclineCode(), target.getWorldpayDeclineCode());
		Mockito.verify(addressConverter, Mockito.never()).convert(Mockito.any(AddressModel.class));
	}

	@Test
	public void populateTestAddressConverterCalledNonCredit()
	{
		Mockito.doReturn(addressModel).when(paymentInfoModel).getBillingAddress();
		worldpayAbstractOrderPopulator.populate(source, target);
		Assert.assertEquals(source.getWorldpayOrderCode(), target.getWorldpayOrderCode());
		Assert.assertEquals(source.getWorldpayDeclineCode(), target.getWorldpayDeclineCode());
		Mockito.verify(addressConverter).convert(addressModel);
		Assert.assertEquals(addressData, target.getPaymentInfo().getBillingAddress());
	}

	@Test
	public void populateTestAddressConverterNtCalledCredit()
	{
		source.setPaymentInfo(creditCardPaymentInfoModel);
		worldpayAbstractOrderPopulator.populate(source, target);
		Assert.assertEquals(source.getWorldpayOrderCode(), target.getWorldpayOrderCode());
		Assert.assertEquals(source.getWorldpayDeclineCode(), target.getWorldpayDeclineCode());
		Mockito.verify(addressConverter, Mockito.never()).convert(Mockito.any(AddressModel.class));
		Assert.assertTrue(null == target.getPaymentInfo());
	}
}
