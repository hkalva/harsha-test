package com.lyonscg.worldpaymentaddon.controllers.pages;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.lyonscg.worldpaymentaddon.facade.LeaWPPaymentFacade;
import com.worldpay.facades.payment.WorldpayAdditionalInfoFacade;
import com.worldpay.order.data.WorldpayAdditionalInfoData;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.b2bacceleratoraddon.controllers.pages.MyQuotesController;
import de.hybris.platform.b2bacceleratoraddon.forms.QuoteOrderForm;
import de.hybris.platform.b2bacceleratorfacades.order.B2BOrderFacade;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;


/**
 * Worldpay Quote controller.
 */
public class LeaWPMyQuotesController extends MyQuotesController
{
    private static final String REDIRECT_MY_ACCOUNT = REDIRECT_PREFIX + "/my-account";
    private static final String REDIRECT_TO_QUOTES_DETAILS = REDIRECT_PREFIX + "/my-account/my-quote/%s";
    private static final String MY_QUOTES_CMS_PAGE = "my-quotes";
    private static final String NEGOTIATEQUOTE = "NEGOTIATEQUOTE";
    private static final String ACCEPTQUOTE = "ACCEPTQUOTE";
    private static final String CANCELQUOTE = "CANCELQUOTE";
    private static final String ADDADDITIONALCOMMENT = "ADDADDITIONALCOMMENT";
    private static final Logger LOG = Logger.getLogger(LeaWPMyQuotesController.class);

    private B2BOrderFacade orderFacade;
    private WorldpayAdditionalInfoFacade worldpayAdditionalInfoFacade;
    private LeaWPPaymentFacade leaWPPaymentFacade;
    @Autowired
    private HttpServletRequest request;

    private String cardPaymentType;

    /**
     * Makes/process quote acceptance decision.
     * 
     * @param quoteOrderForm
     *            - Submitted quote order form.
     * @param model
     *            - Model to add attributes.
     * @param redirectModel
     *            - Attributes for a redirect scenario.
     * @throws CMSItemNotFoundException
     *             - if unable to find page by id.
     */
    @RequestMapping(value = "/quote/quoteOrderDecision")
    @RequireHardLogIn
    public String quoteOrderDecision(@ModelAttribute("quoteOrderDecisionForm") final QuoteOrderForm quoteOrderForm,
            final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
    {
        storeCmsPageInModel(model, getContentPageForLabelOrId(MY_QUOTES_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MY_QUOTES_CMS_PAGE));
        String orderCode = null;
        try
        {
            orderCode = quoteOrderForm.getOrderCode();

            final String comment = XSSFilterUtil.filter(quoteOrderForm.getComments());

            if (NEGOTIATEQUOTE.equals(quoteOrderForm.getSelectedQuoteDecision()))
            {
                if (StringUtils.isBlank(comment))
                {
                    setUpCommentIsEmptyError(quoteOrderForm, model);
                    return quotesDetails(orderCode, model);
                }
                getOrderFacade().createAndSetNewOrderFromNegotiateQuote(orderCode, comment);
            }

            if (ACCEPTQUOTE.equals(quoteOrderForm.getSelectedQuoteDecision()))
            {
                final OrderData orderDetails = getOrderFacade().getOrderDetailsForCode(orderCode);
                final Date quoteExpirationDate = orderDetails.getQuoteExpirationDate();
                if (quoteExpirationDate != null && quoteExpirationDate.before(new Date()))
                {
                    GlobalMessages.addErrorMessage(model, "text.quote.expired");
                    return quotesDetails(orderCode, model);
                }
                if (!authorizePayment(orderDetails))
                {
                    GlobalMessages.addErrorMessage(model, "checkout.error.authorization.failed");
                    return quotesDetails(orderCode, model);
                }
                getOrderFacade().createAndSetNewOrderFromApprovedQuote(orderCode, comment);
                return REDIRECT_PREFIX + "/checkout/orderConfirmation/" + orderCode;
            }

            if (CANCELQUOTE.equals(quoteOrderForm.getSelectedQuoteDecision()))
            {
                getOrderFacade().cancelOrder(orderCode, comment);
            }

            if (ADDADDITIONALCOMMENT.equals(quoteOrderForm.getSelectedQuoteDecision()))
            {
                if (StringUtils.isBlank(comment))
                {
                    setUpCommentIsEmptyError(quoteOrderForm, model);
                    return quotesDetails(orderCode, model);
                }
                getOrderFacade().addAdditionalComment(orderCode, comment);
                GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
                        "text.confirmation.quote.comment.added");
                return String.format(REDIRECT_TO_QUOTES_DETAILS, orderCode);
            }
        }
        catch (final UnknownIdentifierException e)
        {
            LOG.warn("Attempted to load a order that does not exist or is not visible", e);
            return REDIRECT_MY_ACCOUNT;
        }

        return REDIRECT_PREFIX + "/checkout/quote/orderConfirmation/" + orderCode;
    }

    /**
     * Authorize Card payment.
     * 
     * @param orderData
     *            - Order to process payment for.
     * @return - true if successfully authorized.
     */
    protected boolean authorizePayment(final OrderData orderData)
    {
        if (getCardPaymentType().equals(orderData.getPaymentType().getCode()))
        {
            LOG.info("Authorizing Quote using Worldpay....");
            return getLeaWPPaymentFacade().authorize(orderData.getCode(), getWorldPayAdditionalInfo());
        }
        return true;
    }

    /**
     * Creates and populates {@link WorldpayAdditionalInfoData}.
     * 
     * @return - populated {@link WorldpayAdditionalInfoData}.
     */
    protected WorldpayAdditionalInfoData getWorldPayAdditionalInfo()
    {
        return getWorldpayAdditionalInfoFacade().createWorldpayAdditionalInfoData(request);
    }

    protected B2BOrderFacade getOrderFacade()
    {
        return orderFacade;
    }

    @Required
    public void setOrderFacade(B2BOrderFacade orderFacade)
    {
        this.orderFacade = orderFacade;
    }

    protected WorldpayAdditionalInfoFacade getWorldpayAdditionalInfoFacade()
    {
        return worldpayAdditionalInfoFacade;
    }

    @Required
    public void setWorldpayAdditionalInfoFacade(WorldpayAdditionalInfoFacade worldpayAdditionalInfoFacade)
    {
        this.worldpayAdditionalInfoFacade = worldpayAdditionalInfoFacade;
    }

    protected LeaWPPaymentFacade getLeaWPPaymentFacade()
    {
        return leaWPPaymentFacade;
    }

    @Required
    public void setLeaWPPaymentFacade(LeaWPPaymentFacade leaWPPaymentFacade)
    {
        this.leaWPPaymentFacade = leaWPPaymentFacade;
    }

    protected String getCardPaymentType()
    {
        return cardPaymentType;
    }

    @Required
    public void setCardPaymentType(String cardPaymentType)
    {
        this.cardPaymentType = cardPaymentType;
    }
}
