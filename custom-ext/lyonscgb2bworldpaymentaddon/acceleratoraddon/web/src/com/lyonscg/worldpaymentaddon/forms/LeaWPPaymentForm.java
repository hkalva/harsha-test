package com.lyonscg.worldpaymentaddon.forms;

import com.worldpay.forms.CSEPaymentForm;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;


/**
 * Lea-Worldpay payment form.
 */
public class LeaWPPaymentForm extends CSEPaymentForm
{
    private Boolean newBillingAddress;
    private AddressForm billingAddress;
    private boolean useDeliveryAddress;

    private String billTo_city;
    private String billTo_country;
    private String billTo_customerID;
    private String billTo_email;
    private String billTo_firstName;
    private String billTo_lastName;
    private String billTo_phoneNumber;
    private String billTo_postalCode;
    private String billTo_titleCode;
    private String billTo_state;
    private String billTo_street1;
    private String billTo_street2;
    private String comments;
    private String currency;
    private String shipTo_city;
    private String shipTo_country;
    private String shipTo_firstName;
    private String shipTo_lastName;
    private String shipTo_phoneNumber;
    private String shipTo_postalCode;
    private String shipTo_shippingMethod;
    private String shipTo_state;
    private String shipTo_street1;
    private String shipTo_street2;
    private String taxAmount;
    private boolean savePaymentInfo;
    private String cardType;

    public Boolean getNewBillingAddress()
    {
        return newBillingAddress;
    }

    public void setNewBillingAddress(Boolean newBillingAddress)
    {
        this.newBillingAddress = newBillingAddress;
    }

    public AddressForm getBillingAddress()
    {
        return billingAddress;
    }

    public void setBillingAddress(AddressForm billingAddress)
    {
        this.billingAddress = billingAddress;
    }

    public boolean isUseDeliveryAddress()
    {
        return useDeliveryAddress;
    }

    public void setUseDeliveryAddress(boolean useDeliveryAddress)
    {
        this.useDeliveryAddress = useDeliveryAddress;
    }

    public String getBillTo_city()
    {
        return billTo_city;
    }

    public void setBillTo_city(String billTo_city)
    {
        this.billTo_city = billTo_city;
    }

    public String getBillTo_country()
    {
        return billTo_country;
    }

    public void setBillTo_country(String billTo_country)
    {
        this.billTo_country = billTo_country;
    }

    public String getBillTo_customerID()
    {
        return billTo_customerID;
    }

    public void setBillTo_customerID(String billTo_customerID)
    {
        this.billTo_customerID = billTo_customerID;
    }

    public String getBillTo_email()
    {
        return billTo_email;
    }

    public void setBillTo_email(String billTo_email)
    {
        this.billTo_email = billTo_email;
    }

    public String getBillTo_firstName()
    {
        return billTo_firstName;
    }

    public void setBillTo_firstName(String billTo_firstName)
    {
        this.billTo_firstName = billTo_firstName;
    }

    public String getBillTo_lastName()
    {
        return billTo_lastName;
    }

    public void setBillTo_lastName(String billTo_lastName)
    {
        this.billTo_lastName = billTo_lastName;
    }

    public String getBillTo_phoneNumber()
    {
        return billTo_phoneNumber;
    }

    public void setBillTo_phoneNumber(String billTo_phoneNumber)
    {
        this.billTo_phoneNumber = billTo_phoneNumber;
    }

    public String getBillTo_postalCode()
    {
        return billTo_postalCode;
    }

    public void setBillTo_postalCode(String billTo_postalCode)
    {
        this.billTo_postalCode = billTo_postalCode;
    }

    public String getBillTo_titleCode()
    {
        return billTo_titleCode;
    }

    public void setBillTo_titleCode(String billTo_titleCode)
    {
        this.billTo_titleCode = billTo_titleCode;
    }

    public String getBillTo_state()
    {
        return billTo_state;
    }

    public void setBillTo_state(String billTo_state)
    {
        this.billTo_state = billTo_state;
    }

    public String getBillTo_street1()
    {
        return billTo_street1;
    }

    public void setBillTo_street1(String billTo_street1)
    {
        this.billTo_street1 = billTo_street1;
    }

    public String getBillTo_street2()
    {
        return billTo_street2;
    }

    public void setBillTo_street2(String billTo_street2)
    {
        this.billTo_street2 = billTo_street2;
    }

    public String getComments()
    {
        return comments;
    }

    public void setComments(String comments)
    {
        this.comments = comments;
    }

    public String getCurrency()
    {
        return currency;
    }

    public void setCurrency(String currency)
    {
        this.currency = currency;
    }

    public String getShipTo_city()
    {
        return shipTo_city;
    }

    public void setShipTo_city(String shipTo_city)
    {
        this.shipTo_city = shipTo_city;
    }

    public String getShipTo_country()
    {
        return shipTo_country;
    }

    public void setShipTo_country(String shipTo_country)
    {
        this.shipTo_country = shipTo_country;
    }

    public String getShipTo_firstName()
    {
        return shipTo_firstName;
    }

    public void setShipTo_firstName(String shipTo_firstName)
    {
        this.shipTo_firstName = shipTo_firstName;
    }

    public String getShipTo_lastName()
    {
        return shipTo_lastName;
    }

    public void setShipTo_lastName(String shipTo_lastName)
    {
        this.shipTo_lastName = shipTo_lastName;
    }

    public String getShipTo_phoneNumber()
    {
        return shipTo_phoneNumber;
    }

    public void setShipTo_phoneNumber(String shipTo_phoneNumber)
    {
        this.shipTo_phoneNumber = shipTo_phoneNumber;
    }

    public String getShipTo_postalCode()
    {
        return shipTo_postalCode;
    }

    public void setShipTo_postalCode(String shipTo_postalCode)
    {
        this.shipTo_postalCode = shipTo_postalCode;
    }

    public String getShipTo_shippingMethod()
    {
        return shipTo_shippingMethod;
    }

    public void setShipTo_shippingMethod(String shipTo_shippingMethod)
    {
        this.shipTo_shippingMethod = shipTo_shippingMethod;
    }

    public String getShipTo_state()
    {
        return shipTo_state;
    }

    public void setShipTo_state(String shipTo_state)
    {
        this.shipTo_state = shipTo_state;
    }

    public String getShipTo_street1()
    {
        return shipTo_street1;
    }

    public void setShipTo_street1(String shipTo_street1)
    {
        this.shipTo_street1 = shipTo_street1;
    }

    public String getShipTo_street2()
    {
        return shipTo_street2;
    }

    public void setShipTo_street2(String shipTo_street2)
    {
        this.shipTo_street2 = shipTo_street2;
    }

    public String getTaxAmount()
    {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount)
    {
        this.taxAmount = taxAmount;
    }

    public boolean isSavePaymentInfo()
    {
        return savePaymentInfo;
    }

    public void setSavePaymentInfo(boolean savePaymentInfo)
    {
        this.savePaymentInfo = savePaymentInfo;
    }

    public String getCardType()
    {
        return cardType;
    }

    public void setCardType(String cardType)
    {
        this.cardType = cardType;
    }
}
