/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.lyonscg.worldpaymentaddon.controllers;

/**
 */
public interface Lyonscgb2bworldpaymentaddonControllerConstants
{
    /**
     * Class with view name constants
     */
    static final String ADDON_PREFIX = "addon:/lyonscgb2bworldpaymentaddon/";
    static final String STOREFRONT_PREFIX = "/";

    interface Views
    {
        interface Pages
        {
            interface MultiStepCheckout
            {
                String LeaWPCSEPaymentDetailsPage = ADDON_PREFIX + "pages/checkout/multi/paymentDetailsPage";
                String CheckoutSummaryPage = "pages/checkout/multi/checkoutSummaryPage";
            }
        }
    }
}
