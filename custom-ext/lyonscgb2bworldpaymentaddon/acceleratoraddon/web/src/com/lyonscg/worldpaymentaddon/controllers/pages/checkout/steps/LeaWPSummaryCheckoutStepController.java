package com.lyonscg.worldpaymentaddon.controllers.pages.checkout.steps;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.lyonscg.worldpaymentaddon.facades.payment.direct.WorldpayDirectOrderAndQuoteFacade;
import com.worldpay.facades.payment.WorldpayAdditionalInfoFacade;
import com.worldpay.order.data.WorldpayAdditionalInfoData;
import com.worldpay.util.WorldpayUtil;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.b2bacceleratoraddon.controllers.pages.checkout.steps.SummaryCheckoutStepController;
import de.hybris.platform.b2bacceleratoraddon.forms.PlaceOrderForm;
import de.hybris.platform.b2bacceleratorfacades.checkout.data.PlaceOrderData;
import de.hybris.platform.b2bacceleratorfacades.exception.EntityValidationException;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.AdapterException;


/**
 * Worldpay summary page controller.
 */
public class LeaWPSummaryCheckoutStepController extends SummaryCheckoutStepController
{
    private static final Logger LOG = Logger.getLogger(LeaWPSummaryCheckoutStepController.class);
    private WorldpayAdditionalInfoFacade worldpayAdditionalInfoFacade;
    private WorldpayDirectOrderAndQuoteFacade worldpayDirectOrderFacade;
    @Autowired
    private HttpServletRequest request;

    /**
     * Saves card details and redirects to the next page in checkout flow.
     * 
     * @param placeOrderForm
     *            -Submitted order form.
     * @param model
     *            -Model to add attributes.
     * @param redirectModel
     *            - Attributes for a redirect scenario.
     * @return - Redirects to next step if successful else send back to the same page.
     * 
     * @throws CMSItemNotFoundException
     * @throws InvalidCartException
     * @throws CommerceCartModificationException
     */
    @RequestMapping(value = "/placeOrder")
    @RequireHardLogIn
    public String placeOrder(@ModelAttribute("placeOrderForm") final PlaceOrderForm placeOrderForm, final Model model,
            final RedirectAttributes redirectModel) throws CMSItemNotFoundException, InvalidCartException,
            CommerceCartModificationException
    {
        if (validateOrderForm(placeOrderForm, model))
        {
            return enterStep(model, redirectModel);
        }
        String paymentTypeCode = getCheckoutFacade().getCheckoutCart().getPaymentType().getCode();
        boolean cardPayment = CheckoutPaymentType.CARD.getCode().equalsIgnoreCase(paymentTypeCode);
        boolean isQuote = placeOrderForm.isNegotiateQuote();
        boolean isAuthNeeded = !(isQuote && cardPayment);
        // authorize, if failure occurs don't allow to place the order
        boolean isPaymentAuthorized = false;
        if (isAuthNeeded)
        {
            try
            {
                isPaymentAuthorized = getCheckoutFacade().authorizePayment(
                        cardPayment ? serializeCvv(request, placeOrderForm.getSecurityCode()) : placeOrderForm.getSecurityCode());
            }
            catch (final AdapterException ae)
            {
                // handle a case where a wrong paymentProvider configurations on the store see getCommerceCheckoutService().getPaymentProvider()
                LOG.error(ae.getMessage(), ae);
            }
        }
        else if (isQuote)
        {
            getWorldpayDirectOrderFacade().processQuote();
        }
        if (isAuthNeeded && (!isPaymentAuthorized))
        {
            GlobalMessages.addErrorMessage(model, "checkout.error.authorization.failed");
            return enterStep(model, redirectModel);
        }

        final PlaceOrderData placeOrderData = new PlaceOrderData();
        placeOrderData.setNDays(placeOrderForm.getnDays());
        placeOrderData.setNDaysOfWeek(placeOrderForm.getnDaysOfWeek());
        placeOrderData.setNegotiateQuote(isQuote);
        placeOrderData.setNthDayOfMonth(placeOrderForm.getNthDayOfMonth());
        placeOrderData.setNWeeks(placeOrderForm.getnWeeks());
        placeOrderData.setQuoteRequestDescription(placeOrderForm.getQuoteRequestDescription());
        placeOrderData.setReplenishmentOrder(placeOrderForm.isReplenishmentOrder());
        placeOrderData.setReplenishmentRecurrence(placeOrderForm.getReplenishmentRecurrence());
        placeOrderData.setReplenishmentStartDate(placeOrderForm.getReplenishmentStartDate());
        placeOrderData.setSecurityCode(placeOrderForm.getSecurityCode());
        placeOrderData.setTermsCheck(placeOrderForm.isTermsCheck());

        final AbstractOrderData orderData;
        try
        {
            orderData = getB2BCheckoutFacade().placeOrder(placeOrderData);
        }
        catch (final EntityValidationException e)
        {
            LOG.error("Failed to place Order", e);
            GlobalMessages.addErrorMessage(model, e.getLocalizedMessage());

            placeOrderForm.setTermsCheck(false);
            placeOrderForm.setNegotiateQuote(false);
            model.addAttribute(placeOrderForm);

            return enterStep(model, redirectModel);
        }
        catch (final Exception e)
        {
            LOG.error("Failed to place Order", e);
            GlobalMessages.addErrorMessage(model, "checkout.placeOrder.failed");
            return enterStep(model, redirectModel);
        }

        return redirectToOrderConfirmationPage(placeOrderData, orderData);
    }

    /**
     * Serialize security code with additional details.
     * 
     * @param request
     *            - HttpServletRequest for the current request.
     * @param securityCode
     *            - cvv number
     * @return - Serialized String with additional information.
     */
    protected String serializeCvv(final HttpServletRequest request, final String securityCode)
    {
        return WorldpayUtil.serializeWorldpayAdditionalInfo(getWorldpayAdditionalInfo(request, securityCode));
    }

    /**
     * Create and populate {@link WorldpayAdditionalInfoData}
     * 
     * @param request
     *            - HttpServletRequest for the current request.
     * @param securityCode
     *            - cvv number.
     * @return - {@link WorldpayAdditionalInfoData}
     */
    protected WorldpayAdditionalInfoData getWorldpayAdditionalInfo(final HttpServletRequest request, final String securityCode)
    {
        final WorldpayAdditionalInfoData info = getWorldpayAdditionalInfoFacade().createWorldpayAdditionalInfoData(request);
        info.setSecurityCode(securityCode);
        return info;
    }

    protected WorldpayAdditionalInfoFacade getWorldpayAdditionalInfoFacade()
    {
        return worldpayAdditionalInfoFacade;
    }

    @Required
    public void setWorldpayAdditionalInfoFacade(WorldpayAdditionalInfoFacade worldpayAdditionalInfoFacade)
    {
        this.worldpayAdditionalInfoFacade = worldpayAdditionalInfoFacade;
    }

    protected WorldpayDirectOrderAndQuoteFacade getWorldpayDirectOrderFacade()
    {
        return worldpayDirectOrderFacade;
    }

    @Required
    public void setWorldpayDirectOrderFacade(WorldpayDirectOrderAndQuoteFacade worldpayDirectOrderFacade)
    {
        this.worldpayDirectOrderFacade = worldpayDirectOrderFacade;
    }

}
