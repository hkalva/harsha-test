package com.lyonscg.worldpaymentaddon.controllers.pages.checkout.steps;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.annotation.Scope;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.lyonscg.worldpaymentaddon.controllers.Lyonscgb2bworldpaymentaddonControllerConstants;
import com.lyonscg.worldpaymentaddon.facades.order.LeaWPPaymentCheckoutFacade;
import com.lyonscg.worldpaymentaddon.facades.payment.direct.WorldpayDirectOrderAndQuoteFacade;
import com.lyonscg.worldpaymentaddon.forms.LeaWPPaymentForm;
import com.worldpay.controllers.pages.checkout.steps.WorldpayCseCheckoutStepController;
import com.worldpay.data.CSEAdditionalAuthInfo;
import com.worldpay.exception.WorldpayException;
import com.worldpay.forms.CSEPaymentForm;
import com.worldpay.order.data.WorldpayAdditionalInfoData;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;


/**
 * Worldpay payment page controller.
 */
@Scope("tenant")
@RequestMapping(value = "/checkout/multi/payment-method")
public class LeaWPPaymentMethodCheckoutStepController extends WorldpayCseCheckoutStepController
{
    private static final String PAYMENT_METHOD = "payment-method";
    private static final String CART_DATA_ATTR = "cartData";

    private Validator leaWPPaymentFormValidator;
    private LeaWPPaymentCheckoutFacade worldpayPaymentCheckoutFacade;
    private WorldpayDirectOrderAndQuoteFacade worldpayDirectOrderFacade;

    private static final Logger LOGGER = Logger.getLogger(LeaWPPaymentMethodCheckoutStepController.class);

    /**
     * Displays worldpay payment page.
     * 
     * @param model
     *            -Model to add attributes.
     * @param redirectAttributes
     *            -Redirect attributes if the page is/was redirected.
     * @return -Returns the jsp view for rendering a page.
     */
    @Override
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    @RequireHardLogIn
    @PreValidateCheckoutStep(checkoutStep = PAYMENT_METHOD)
    public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
    {
        getCheckoutFacade().setDeliveryModeIfAvailable();
        setupCSEPaymentDetailsPage(new LeaWPPaymentForm(), model);
        return Lyonscgb2bworldpaymentaddonControllerConstants.Views.Pages.MultiStepCheckout.LeaWPCSEPaymentDetailsPage;
    }

    /**
     * Saves card details and redirects to the next page in checkout flow.
     * 
     * @param request
     *            - HttpServletRequest used to post the form.
     * @param model
     *            -Model to add attributes.
     * @param paymentDetailsForm
     *            -Submitted payment details form.
     * @param bindingResult
     *            - Interface that represents binding results.
     * @return - Redirects to next step if successful else send back to the same page.
     */
    @RequestMapping(value =
    { "/add" }, method = RequestMethod.POST)
    @RequireHardLogIn
    public String add(final HttpServletRequest request, final Model model, @Valid final LeaWPPaymentForm paymentDetailsForm,
            final BindingResult bindingResult) throws CMSItemNotFoundException
    {
        getLeaWPPaymentFormValidator().validate(paymentDetailsForm, bindingResult);
        model.addAttribute(CART_DATA_ATTR, getCheckoutFacade().getCheckoutCart());
        if (bindingResult.hasErrors())
        {
            return errorRedirectToPaymentPage(model, paymentDetailsForm, "checkout.error.paymentethod.formentry.invalid");
        }
        handleAndSaveAddresses(paymentDetailsForm);
        final CSEAdditionalAuthInfo cseAdditionalAuthInfo = createCSEAdditionalAuthInfo(paymentDetailsForm);
        final WorldpayAdditionalInfoData worldpayAdditionalInfoData = createWorldpayAdditionalInfo(request,
                paymentDetailsForm.getCvc());
        try
        {
            if (getWorldpayDirectOrderFacade().tokenize(cseAdditionalAuthInfo, worldpayAdditionalInfoData))
            {
                setCheckoutStepLinksForModel(model, getCheckoutStep());
                return getCheckoutStep().nextStep();
            }
        }
        catch (WorldpayException e)
        {
            LOGGER.error("There was an error authorising the transaction", e);
        }
        return errorRedirectToPaymentPage(model, paymentDetailsForm, CHECKOUT_ERROR_PAYMENTETHOD_FORMENTRY_INVALID);
    }

    /**
     * Sets the saved paymentMethod selected and proceeds to next step
     * 
     * @param selectedPaymentMethodId
     *            The id of the CreditCardPaymentInfo to use
     * @return - Redirects to next step if successful
     */
    @RequestMapping(value = "/choose", method = GET)
    @RequireHardLogIn
    public String doSelectPaymentMethod(@RequestParam("selectedPaymentMethodId") final String selectedPaymentMethodId)
    {
        if (StringUtils.isNotBlank(selectedPaymentMethodId))
        {
            getCheckoutFacade().setPaymentDetails(selectedPaymentMethodId);
            getWorldpayPaymentCheckoutFacade().setAddressInPaymentAsCartBillingAddress();
            getCheckoutFacade().prepareCartForCheckout();
        }
        return getCheckoutStep().nextStep();
    }

    /**
     * Method to populate payment page details in case of a validation failure/error redirect.
     * 
     * @param model
     *            - holder for model attributes.
     * @param paymentDetailsForm
     *            - payment details form.
     * @param errorMsgKey
     *            - Property key for the error message to be displayed.
     * @return - Returns the jsp view for rendering a page.
     */
    protected String errorRedirectToPaymentPage(final Model model, final LeaWPPaymentForm paymentDetailsForm, String errorMsgKey)
            throws CMSItemNotFoundException
    {
        GlobalMessages.addErrorMessage(model, errorMsgKey);
        setupCSEPaymentDetailsPage(paymentDetailsForm, model);
        return Lyonscgb2bworldpaymentaddonControllerConstants.Views.Pages.MultiStepCheckout.LeaWPCSEPaymentDetailsPage;
    }

    /**
     * Saves payment address.
     * 
     * @param leaWPPaymentForm
     *            - payment details form.
     */
    protected void handleAndSaveAddresses(final @Valid LeaWPPaymentForm leaWPPaymentForm)
    {
        final AddressData addressData = getAddressData(leaWPPaymentForm);
        addressData.setEmail(getCheckoutCustomerStrategy().getCurrentUserForCheckout().getContactEmail());
        if (shouldSaveAddressInProfile(leaWPPaymentForm.isUseDeliveryAddress()))
        {
            getUserFacade().addAddress(addressData);
        }
        getWorldpayPaymentCheckoutFacade().setBillingDetails(addressData);
    }

    /**
     * Populated AddressData from payment form.
     * 
     * @param leaWPPaymentForm
     *            - payment details form.
     * @return - populated AddressData
     */
    protected AddressData getAddressData(LeaWPPaymentForm leaWPPaymentForm)
    {
        AddressData addressData = new AddressData();
        if (Boolean.TRUE.equals(leaWPPaymentForm.isUseDeliveryAddress()))
        {
            addressData = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
            addressData.setBillingAddress(true);
        }
        else
        {
            addressData.setFirstName(leaWPPaymentForm.getBillTo_firstName());
            addressData.setLastName(leaWPPaymentForm.getBillTo_lastName());
            addressData.setLine1(leaWPPaymentForm.getBillTo_street1());
            addressData.setLine2(leaWPPaymentForm.getBillTo_street2());
            addressData.setTown(leaWPPaymentForm.getBillTo_city());
            addressData.setPostalCode(leaWPPaymentForm.getBillTo_postalCode());
            addressData.setCountry(getI18NFacade().getCountryForIsocode(leaWPPaymentForm.getBillTo_country()));
            if (leaWPPaymentForm.getBillTo_state() != null)
            {
                addressData.setRegion(getI18NFacade().getRegion(leaWPPaymentForm.getBillTo_country(),
                        leaWPPaymentForm.getBillTo_country() + "-" + leaWPPaymentForm.getBillTo_state()));
            }
            addressData.setPhone(leaWPPaymentForm.getBillTo_phoneNumber());
            addressData.setTitleCode(leaWPPaymentForm.getBillTo_titleCode());
        }
        return addressData;
    }

    /**
     * To decide if the address has to be saved in user profile.
     * 
     * @param useDeliveryAddress
     *            - true to use delivery address is used as payment address.
     * @return - true if user is Anonymous or useDeliveryAddress is false.
     */
    private boolean shouldSaveAddressInProfile(final Boolean useDeliveryAddress)
    {
        return Boolean.FALSE.equals(useDeliveryAddress) || getUserFacade().isAnonymousUser();
    }

    /**
     * Adds attributes to Model to render payment details page.
     * 
     * @param model
     *            - holder for model attributes.
     * @param leaWPPaymentForm
     *            - payment details form.
     * @throws CMSItemNotFoundException
     */
    protected void setupAddPaymentPage(final Model model, LeaWPPaymentForm leaWPPaymentForm) throws CMSItemNotFoundException
    {
        model.addAttribute("metaRobots", "noindex,nofollow");
        model.addAttribute("hasNoPaymentInfo", Boolean.valueOf(getCheckoutFlowFacade().hasNoPaymentInfo()));
        prepareDataForPage(model);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.paymentMethod.breadcrumb"));
        final ContentPageModel contentPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
        storeCmsPageInModel(model, contentPage);
        setUpMetaDataForContentPage(model, contentPage);
        setCheckoutStepLinksForModel(model, getCheckoutStep());
        super.setupAddPaymentPage(model);
        model.addAttribute(CSE_PAYMENT_FORM, leaWPPaymentForm);
    }

    /**
     * Adds attributes to Model to render CSE payment details page.
     * 
     * @param leaWPPaymentForm
     *            - payment details form.
     * @param model
     *            - holder for model attributes.
     * @throws CMSItemNotFoundException
     */
    protected void setupCSEPaymentDetailsPage(final LeaWPPaymentForm leaWPPaymentForm, final Model model)
            throws CMSItemNotFoundException
    {
        final CartData cartData = getCheckoutFacade().getCheckoutCart();
        model.addAttribute("csePaymentForm", leaWPPaymentForm);
        model.addAttribute(CART_DATA_ATTR, cartData);
        model.addAttribute("deliveryAddress", cartData.getDeliveryAddress());
        model.addAttribute("paymentInfos", getUserFacade().getCCPaymentInfos(true));
        if (StringUtils.isNotBlank(leaWPPaymentForm.getBillTo_country()))
        {
            model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(leaWPPaymentForm.getBillTo_country()));
            model.addAttribute("country", leaWPPaymentForm.getBillTo_country());
        }
        setupAddPaymentPage(model, leaWPPaymentForm);
    }

    /**
     * Populates and creates {@link CSEAdditionalAuthInfo}
     * 
     * @param csePaymentForm
     *            - Submitted payment form
     * @return - Populated {@link CSEAdditionalAuthInfo}
     */
    @Override
    protected CSEAdditionalAuthInfo createCSEAdditionalAuthInfo(final CSEPaymentForm csePaymentForm)
    {
        CSEAdditionalAuthInfo cseAdditionalAuthInfo = super.createCSEAdditionalAuthInfo(csePaymentForm);
        if (csePaymentForm instanceof LeaWPPaymentForm)
        {
            cseAdditionalAuthInfo.setCardType(((LeaWPPaymentForm) csePaymentForm).getCardType());
        }
        return cseAdditionalAuthInfo;
    }

    /**
     * Gets current {@link CheckoutStep}.
     * 
     * @return - Current {@link CheckoutStep}.
     */
    @Override
    protected CheckoutStep getCheckoutStep()
    {
        return getCheckoutStep(PAYMENT_METHOD);
    }

    protected Validator getLeaWPPaymentFormValidator()
    {
        return leaWPPaymentFormValidator;
    }

    @Required
    public void setLeaWPPaymentFormValidator(Validator leaWPPaymentFormValidator)
    {
        this.leaWPPaymentFormValidator = leaWPPaymentFormValidator;
    }

    @Override
    public LeaWPPaymentCheckoutFacade getWorldpayPaymentCheckoutFacade()
    {
        return worldpayPaymentCheckoutFacade;
    }

    @Required
    public void setWorldpayPaymentCheckoutFacade(LeaWPPaymentCheckoutFacade worldpayPaymentCheckoutFacade)
    {
        this.worldpayPaymentCheckoutFacade = worldpayPaymentCheckoutFacade;
    }

    protected WorldpayDirectOrderAndQuoteFacade getWorldpayDirectOrderFacade()
    {
        return worldpayDirectOrderFacade;
    }

    @Required
    public void setWorldpayDirectOrderFacade(WorldpayDirectOrderAndQuoteFacade worldpayDirectOrderFacade)
    {
        this.worldpayDirectOrderFacade = worldpayDirectOrderFacade;
    }

}
