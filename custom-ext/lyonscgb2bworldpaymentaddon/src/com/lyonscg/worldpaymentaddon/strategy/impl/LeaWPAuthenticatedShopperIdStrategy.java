package com.lyonscg.worldpaymentaddon.strategy.impl;

import static java.text.MessageFormat.format;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Preconditions;
import com.worldpay.strategy.WorldpayAuthenticatedShopperIdStrategy;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;


/**
 * {@inheritDoc} Has additional logic to return Customer.uid if Customer.CustomerID is empty.
 */
public class LeaWPAuthenticatedShopperIdStrategy implements WorldpayAuthenticatedShopperIdStrategy
{

    /**
     * {@inheritDoc} If Customer.customerID is null the Customer.uid is returned.
     */
    @Override
    public String getAuthenticatedShopperId(final UserModel userModel)
    {
        Preconditions.checkNotNull(userModel, "The user is null");
        if (userModel instanceof CustomerModel)
        {

            final String custId = ((CustomerModel) userModel).getCustomerID();
            if (StringUtils.isBlank(custId))
            {
                return userModel.getUid();
            }
            return custId;
        }
        throw new IllegalArgumentException(format("The user {0} is not of type Customer.", userModel));
    }
}
