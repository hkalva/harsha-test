/**
 *
 */
package com.lyonscg.worldpaymentaddon.strategy.impl;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.worldpay.strategy.WorldpayCustomerIpAddressStrategy;


/**
 * This class provides implementation to retrieve the ip address of the user from configurable header name.
 */
public class ConfigBasedWorldpayCustomerIpAddressStrategy implements WorldpayCustomerIpAddressStrategy
{
	private ConfigurationService configurationService;
	private String headerName, headerConfigProperty, ipSeparatorConfig, ipIndexConfig, defaultIpSeparator;
	private int defaultIpIndex;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCustomerIp(final HttpServletRequest request)
	{
		String headerValue = request.getHeader(getConfigurationService().getConfiguration().getString(getHeaderConfigProperty(),
				getHeaderName()));
		String ipSeparator = null;
		ipSeparator = getConfigurationService().getConfiguration().getString(getIpSeparatorConfig(), getDefaultIpSeparator());
		if (null != headerValue
				&& headerValue.contains(ipSeparator))
		{
			headerValue = headerValue.split(ipSeparator)[getConfigurationService().getConfiguration().getInt(getIpIndexConfig(),
					getDefaultIpIndex())];
		}
		return containsValidIp(headerValue) ? request.getRemoteAddr() : headerValue;
	}

	/** checks if entered url has valid ip address.
	 * @param headerValue is the URL.
	 * @return true or false if IP address is valid or not.
	 */
	private boolean containsValidIp(final String headerValue)
	{
		return StringUtils.isBlank(headerValue) || "unknown".equalsIgnoreCase(headerValue);
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected String getHeaderName()
	{
		return headerName;
	}

	@Required
	public void setHeaderName(final String headerName)
	{
		this.headerName = headerName;
	}

	protected String getHeaderConfigProperty()
	{
		return headerConfigProperty;
	}

	@Required
	public void setHeaderConfigProperty(final String headerConfigProperty)
	{
		this.headerConfigProperty = headerConfigProperty;
	}


	protected String getIpIndexConfig()
	{
		return ipIndexConfig;
	}

	@Required
	public void setIpIndexConfig(final String ipIndexConfig)
	{
		this.ipIndexConfig = ipIndexConfig;
	}

	protected String getIpSeparatorConfig()
	{
		return ipSeparatorConfig;
	}

	@Required
	public void setIpSeparatorConfig(final String ipSeparatorConfig)
	{
		this.ipSeparatorConfig = ipSeparatorConfig;
	}

	protected String getDefaultIpSeparator()
	{
		return defaultIpSeparator;
	}

	@Required
	public void setDefaultIpSeparator(final String defaultIpSeparator)
	{
		this.defaultIpSeparator = defaultIpSeparator;
	}

	protected int getDefaultIpIndex()
	{
		return defaultIpIndex;
	}

	@Required
	public void setDefaultIpIndex(final int defaultIpIndex)
	{
		this.defaultIpIndex = defaultIpIndex;
	}
}
