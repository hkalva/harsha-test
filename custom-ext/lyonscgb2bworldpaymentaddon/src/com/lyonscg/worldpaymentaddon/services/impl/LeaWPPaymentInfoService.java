package com.lyonscg.worldpaymentaddon.services.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import java.util.Locale;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Required;

import com.lyonscg.worldpaymentaddon.helpers.LeaWPPaymentHelper;
import com.worldpay.core.services.impl.DefaultWorldpayPaymentInfoService;
import com.worldpay.enums.token.TokenEvent;
import com.worldpay.service.model.Date;
import com.worldpay.service.model.PaymentReply;
import com.worldpay.service.model.payment.Card;
import com.worldpay.service.model.token.TokenReply;
import com.worldpay.service.response.CreateTokenResponse;

import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.model.ModelService;


/**
 * Lea-Worldpay implementation to mask card number.
 */
public class LeaWPPaymentInfoService extends DefaultWorldpayPaymentInfoService
{
    private ModelService modelService;
    private EnumerationService enumerationService;
    private LeaWPPaymentHelper leaWPPaymentHelper;
    private String cardNumberMaskChar;

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public CreditCardPaymentInfoModel createCreditCardPaymentInfo(final CartModel cartModel,
            final CreateTokenResponse createTokenResponse, final boolean saveCard)
    {
        validateParameterNotNull(cartModel, "CartModel cannot be null");
        validateParameterNotNull(createTokenResponse, "Token response cannot be null");

        final TokenReply tokenReply = createTokenResponse.getToken();
        maskCardNumberInToken(tokenReply);
        final TokenEvent tokenEvent = TokenEvent.valueOf(tokenReply.getTokenDetails().getTokenEvent().toUpperCase(Locale.getDefault()));
        if (tokenEvent.equals(TokenEvent.MATCH))
        {
            final CreditCardPaymentInfoModel customerSavedCard = findMatchingTokenisedCard(cartModel.getUser(), tokenReply
                    .getTokenDetails().getPaymentTokenID());
            if (customerSavedCard != null)
            {
                if (!customerSavedCard.isSaved())
                {
                    customerSavedCard.setSaved(saveCard);
                }
                cartModel.setPaymentInfo(customerSavedCard);
                getModelService().save(cartModel);
                return customerSavedCard;
            }
        }

        final CreditCardPaymentInfoModel creditCardPaymentInfoModel = getModelService().create(CreditCardPaymentInfoModel.class);
        creditCardPaymentInfoModel.setCode(generateCcPaymentInfoCode(cartModel));
        creditCardPaymentInfoModel.setWorldpayOrderCode(cartModel.getWorldpayOrderCode());
        creditCardPaymentInfoModel.setUser(cartModel.getUser());
        creditCardPaymentInfoModel.setType(CreditCardType.TOKEN);
        updateCreditCardModel(tokenReply, creditCardPaymentInfoModel, saveCard);
        return creditCardPaymentInfoModel;
    }

    /**
     * Masks Card number
     * 
     * @param tokenReply
     *            - Token response from Worldpay.
     */
    protected void maskCardNumberInToken(final TokenReply tokenReply)
    {
        if (null != tokenReply)
        {
            maskCardNumber(tokenReply.getPaymentInstrument());
        }
    }

    /**
     * Masks Card number
     * 
     * @param card
     *            - Card used for payment.
     */
    protected void maskCardNumber(Card card)
    {
        if (null != card)
        {
            card.setCardNumber(getLeaWPPaymentHelper().maskCardNumber(card.getCardNumber(), getCardNumberMaskChar()));
        }
    }

    /**
     * Clones and saves CreditCardPaymentInfo with payment information.
     * 
     * @param paymentTransactionModel
     *            - Payment transaction created by Worldpay api.
     * @param paymentReply
     *            - Payment reply from Worldpay.
     * 
     */
    protected CreditCardPaymentInfoModel cloneAndSaveCreditCardWithPaymentInformation(
            final PaymentTransactionModel paymentTransactionModel, final PaymentReply paymentReply)
    {
        final CreditCardPaymentInfoModel creditCardPaymentInfoModel = getModelService().clone(paymentTransactionModel.getInfo(),
                CreditCardPaymentInfoModel.class);
        creditCardPaymentInfoModel.setOriginal(null);
        creditCardPaymentInfoModel.setDuplicate(false);
        final Card card = paymentReply.getCardDetails();
        final Date expiryDate = card.getExpiryDate();
        maskCardNumber(card);
        setCardInformation(creditCardPaymentInfoModel, card.getCardNumber(), expiryDate.getMonth(), expiryDate.getYear(),
                card.getCardHolderName());

        updateCreditCardType(creditCardPaymentInfoModel, paymentReply);

        getModelService().save(creditCardPaymentInfoModel);
        return creditCardPaymentInfoModel;
    }

    /**
     * Clones and saves CreditCardPaymentInfo with token information.
     * 
     * @param paymentTransactionModel
     *            - Payment transaction created by Worldpay api.
     * @param tokenReply
     *            - Token reply from Worldpay.
     * @param paymentReply
     *            - Payment reply from Worldpay.
     * 
     */
    protected CreditCardPaymentInfoModel cloneAndSaveCreditCardWithTokenInformation(
            final PaymentTransactionModel paymentTransactionModel, final TokenReply tokenReply, final PaymentReply paymentReply)
    {
        final CreditCardPaymentInfoModel creditCardPaymentInfoModel = getModelService().clone(paymentTransactionModel.getInfo(),
                CreditCardPaymentInfoModel.class);
        updateCreditCardType(creditCardPaymentInfoModel, paymentReply);
        return updateCreditCardModel(tokenReply, creditCardPaymentInfoModel, true);
    }

    /**
     * Update credit card type(VISA,AMEX etc)
     * 
     * @param creditCardPaymentInfoModel
     *            - {@link CreditCardPaymentInfoModel} to set the card type.
     * @param paymentReply
     *            - Payment reply from Worldpay.
     */
    private void updateCreditCardType(final CreditCardPaymentInfoModel creditCardPaymentInfoModel, final PaymentReply paymentReply)
    {
        final String methodCode = paymentReply.getMethodCode();
        final String creditCardTypeValue = getHybrisCCTypeForWPCCType(methodCode);
        final CreditCardType cardType = getEnumerationService().getEnumerationValue(CreditCardType.class.getSimpleName(),
                creditCardTypeValue);
        creditCardPaymentInfoModel.setPaymentType(methodCode);
        creditCardPaymentInfoModel.setType(cardType);
    }

    /**
     * Populates Card details in {@link CreditCardPaymentInfoModel}
     * 
     * @param creditCardPaymentInfoModel
     *            - {@link CreditCardPaymentInfoModel} to populate.
     * @param cardNumber
     *            - Card number.
     * @param month
     *            - Expire month.
     * @param year
     *            - Expire year.
     * @param cardHolderName
     *            - Card holder name.
     */
    private void setCardInformation(final CreditCardPaymentInfoModel creditCardPaymentInfoModel, final String cardNumber,
            final String month, final String year, final String cardHolderName)
    {
        creditCardPaymentInfoModel.setNumber(cardNumber);
        creditCardPaymentInfoModel.setValidToMonth(month);
        creditCardPaymentInfoModel.setValidToYear(year);
        creditCardPaymentInfoModel.setCcOwner(cardHolderName);
    }

    /**
     * Updates and saves {@link CreditCardPaymentInfoModel}.
     * 
     * @param tokenReply
     *            - Token reply from Worldpay.
     * @param creditCardPaymentInfoModel
     *            - {@link CreditCardPaymentInfoModel} to populate and save.
     * @param saveCard
     *            - true to save card to user profile.
     * @return - Populated and saved Credit card.
     */
    private CreditCardPaymentInfoModel updateCreditCardModel(final TokenReply tokenReply,
            final CreditCardPaymentInfoModel creditCardPaymentInfoModel, final boolean saveCard)
    {
        creditCardPaymentInfoModel.setSubscriptionId(tokenReply.getTokenDetails().getPaymentTokenID());

        final Card card = tokenReply.getPaymentInstrument();
        final Date date = card.getExpiryDate();
        maskCardNumberInToken(tokenReply);
        setCardInformation(creditCardPaymentInfoModel, card.getCardNumber(), date.getMonth(), date.getYear(),
                card.getCardHolderName());

        creditCardPaymentInfoModel.setSaved(saveCard);
        creditCardPaymentInfoModel.setAuthenticatedShopperID(tokenReply.getAuthenticatedShopperID());
        creditCardPaymentInfoModel.setEventReference(tokenReply.getTokenDetails().getTokenEventReference());
        final DateTime dt = getDateTime(tokenReply.getTokenDetails().getPaymentTokenExpiry());
        creditCardPaymentInfoModel.setExpiryDate(dt.toDate());
        getModelService().save(creditCardPaymentInfoModel);
        return creditCardPaymentInfoModel;
    }

    protected ModelService getModelService()
    {
        return modelService;
    }

    /**
     * Sets modelService for this class and calls the super class's setter.
     * 
     * @param modelService
     *            - injected {@link ModelService}
     */
    @Required
    @Override
    public void setModelService(ModelService modelService)
    {
        super.setModelService(modelService);
        this.modelService = modelService;
    }

    protected EnumerationService getEnumerationService()
    {
        return enumerationService;
    }

    /**
     * Sets enumerationService for this class and calls the super class's setter.
     * 
     * @param enumerationService
     *            - injected {@link EnumerationService}
     */
    @Required
    @Override
    public void setEnumerationService(EnumerationService enumerationService)
    {
        super.setEnumerationService(enumerationService);
        this.enumerationService = enumerationService;
    }

    protected LeaWPPaymentHelper getLeaWPPaymentHelper()
    {
        return leaWPPaymentHelper;
    }

    @Required
    public void setLeaWPPaymentHelper(LeaWPPaymentHelper leaWPPaymentHelper)
    {
        this.leaWPPaymentHelper = leaWPPaymentHelper;
    }

    protected String getCardNumberMaskChar()
    {
        return cardNumberMaskChar;
    }

    @Required
    public void setCardNumberMaskChar(String cardNumberMaskChar)
    {
        this.cardNumberMaskChar = cardNumberMaskChar;
    }

}
