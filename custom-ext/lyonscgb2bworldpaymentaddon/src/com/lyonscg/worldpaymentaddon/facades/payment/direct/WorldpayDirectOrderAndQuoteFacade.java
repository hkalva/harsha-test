package com.lyonscg.worldpaymentaddon.facades.payment.direct;

import com.worldpay.data.CSEAdditionalAuthInfo;
import com.worldpay.exception.WorldpayException;
import com.worldpay.facades.payment.direct.WorldpayDirectOrderFacade;
import com.worldpay.order.data.WorldpayAdditionalInfoData;


/**
 * Interface that exposes the tokenize operations that enables the Client Side Encryption with Worldpay.
 *
 */
public interface WorldpayDirectOrderAndQuoteFacade extends WorldpayDirectOrderFacade
{
    /**
     * Tokenize performs a direct authorisation using Client Side Encryption with Worldpay.
     *
     * @param cseAdditionalAuthInfo
     *            Object that contains additional authorisation information and the cseToken
     * @param worldPayAdditionalInfoData
     *            Object that contains information about the current session, browser used, and cookies.
     * @throws WorldPayException
     */
    boolean tokenize(final CSEAdditionalAuthInfo cseAdditionalAuthInfo,
            final WorldpayAdditionalInfoData worldPayAdditionalInfoData) throws WorldpayException;

    /**
     * Processes payment info for Quote orders.
     * 
     */
    void processQuote();

}
