package com.lyonscg.worldpaymentaddon.facades.payment.direct.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Preconditions;
import com.lyonscg.worldpaymentaddon.facades.payment.direct.WorldpayDirectOrderAndQuoteFacade;
import com.lyonscg.worldpaymentaddon.service.payment.WorldpayDirectOrderAndQuoteService;
import com.worldpay.data.CSEAdditionalAuthInfo;
import com.worldpay.exception.WorldpayConfigurationException;
import com.worldpay.exception.WorldpayException;
import com.worldpay.facades.payment.direct.impl.DefaultWorldpayDirectOrderFacade;
import com.worldpay.merchant.WorldpayMerchantInfoService;
import com.worldpay.order.data.WorldpayAdditionalInfoData;
import com.worldpay.payment.DirectResponseData;
import com.worldpay.payment.TransactionStatus;
import com.worldpay.service.model.MerchantInfo;
import com.worldpay.strategy.WorldpayAuthenticatedShopperIdStrategy;

import de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade;
import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;


/**
 * Default implementaion for tokenization related operations.
 *
 */
public class DefaultWorldpayDirectOrderAndQuoteFacade extends DefaultWorldpayDirectOrderFacade implements
        WorldpayDirectOrderAndQuoteFacade
{

    private static final Logger LOG = Logger.getLogger(DefaultWorldpayDirectOrderAndQuoteFacade.class);
    private CartService cartService;
    private WorldpayDirectOrderAndQuoteService worldpayDirectOrderService;
    private WorldpayMerchantInfoService worldpayMerchantInfoService;
    private WorldpayAuthenticatedShopperIdStrategy worldpayAuthenticatedShopperIdStrategy;
    private UiExperienceService uiExperienceService;
    private CheckoutFlowFacade checkoutFlowFacade;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean tokenize(final CSEAdditionalAuthInfo cseAdditionalAuthInfo,
            final WorldpayAdditionalInfoData worldPayAdditionalInfoData) throws WorldpayException
    {
        Preconditions.checkState(getCartService().hasSessionCart(), "Cannot tokenize there is no cart");
        final CartModel cart = getCartService().getSessionCart();
        try
        {
            final MerchantInfo merchantInfo = getCurrentMerchantInfo();
            populateShopperId(worldPayAdditionalInfoData, cart);
            getWorldpayDirectOrderService().createToken(merchantInfo, cart, cseAdditionalAuthInfo, worldPayAdditionalInfoData);
            getCheckoutFlowFacade().prepareCartForCheckout();
            return true;
        }
        catch (final WorldpayConfigurationException e)
        {
            LOG.error("There is no configuration for the requested merchant. Please review your settings.");
            //throw e;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void processQuote()
    {
        Preconditions.checkState(getCartService().hasSessionCart(), "Cannot process quote, where there is no cart");
        getWorldpayDirectOrderService().processQuote(getCartService().getSessionCart());
    }

    /**
     * Populates user id from cart user in WorldpayAdditionalInfoData.
     * 
     * @param worldPayAdditionalInfoData
     * @param cart
     */
    private void populateShopperId(final WorldpayAdditionalInfoData worldPayAdditionalInfoData, final CartModel cart)
    {
        worldPayAdditionalInfoData.setAuthenticatedShopperId(getWorldpayAuthenticatedShopperIdStrategy()
                .getAuthenticatedShopperId(cart.getUser()));
    }

    /**
     * Overrides and removes Worldpay's order submit logic.
     * 
     * @param response
     *            - Worldpay's response.
     */
    @Override
    protected void handleAuthorisedResponse(final DirectResponseData response) throws InvalidCartException
    {
        response.setTransactionStatus(TransactionStatus.AUTHORISED);
    }

    private MerchantInfo getCurrentMerchantInfo() throws WorldpayConfigurationException
    {
        return getWorldpayMerchantInfoService().getCurrentSiteMerchant(getUiExperienceService().getUiExperienceLevel());
    }

    protected CheckoutFlowFacade getCheckoutFlowFacade()
    {
        return checkoutFlowFacade;
    }

    @Required
    public void setCheckoutFlowFacade(CheckoutFlowFacade checkoutFlowFacade)
    {
        this.checkoutFlowFacade = checkoutFlowFacade;
    }

    protected CartService getCartService()
    {
        return cartService;
    }

    /**
     * Sets cartService for this class and calls the super class's setter.
     * 
     * @param cartService
     *            - injected {@link CartService}
     */
    @Required
    @Override
    public void setCartService(CartService cartService)
    {
        super.setCartService(cartService);
        this.cartService = cartService;
    }

    protected WorldpayDirectOrderAndQuoteService getWorldpayDirectOrderService()
    {
        return worldpayDirectOrderService;
    }

    /**
     * Sets worldpayDirectOrderService for this class and calls the super class's setter.
     * 
     * @param worldpayDirectOrderService
     *            - injected {@link WorldpayDirectOrderAndQuoteService}
     */
    @Required
    public void setWorldpayDirectOrderService(WorldpayDirectOrderAndQuoteService worldpayDirectOrderService)
    {
        super.setWorldpayDirectOrderService(worldpayDirectOrderService);
        this.worldpayDirectOrderService = worldpayDirectOrderService;
    }

    protected WorldpayMerchantInfoService getWorldpayMerchantInfoService()
    {
        return worldpayMerchantInfoService;
    }

    /**
     * Sets worldpayMerchantInfoService for this class and calls the super class's setter.
     * 
     * @param worldpayMerchantInfoService
     *            - injected {@link WorldpayMerchantInfoService}
     */
    @Required
    @Override
    public void setWorldpayMerchantInfoService(WorldpayMerchantInfoService worldpayMerchantInfoService)
    {
        super.setWorldpayMerchantInfoService(worldpayMerchantInfoService);
        this.worldpayMerchantInfoService = worldpayMerchantInfoService;
    }

    protected WorldpayAuthenticatedShopperIdStrategy getWorldpayAuthenticatedShopperIdStrategy()
    {
        return worldpayAuthenticatedShopperIdStrategy;
    }

    /**
     * Sets worldpayAuthenticatedShopperIdStrategy for this class and calls the super class's setter.
     * 
     * @param worldpayAuthenticatedShopperIdStrategy
     *            - injected {@link WorldpayAuthenticatedShopperIdStrategy}
     */
    @Override
    @Required
    public void setWorldpayAuthenticatedShopperIdStrategy(
            WorldpayAuthenticatedShopperIdStrategy worldpayAuthenticatedShopperIdStrategy)
    {
        super.setWorldpayAuthenticatedShopperIdStrategy(worldpayAuthenticatedShopperIdStrategy);
        this.worldpayAuthenticatedShopperIdStrategy = worldpayAuthenticatedShopperIdStrategy;
    }

    protected UiExperienceService getUiExperienceService()
    {
        return uiExperienceService;
    }

    /**
     * Sets uiExperienceService for this class and calls the super class's setter.
     * 
     * @param uiExperienceService
     *            - injected {@link UiExperienceService}
     */
    @Override
    @Required
    public void setUiExperienceService(UiExperienceService uiExperienceService)
    {
        super.setUiExperienceService(uiExperienceService);
        this.uiExperienceService = uiExperienceService;
    }

}
