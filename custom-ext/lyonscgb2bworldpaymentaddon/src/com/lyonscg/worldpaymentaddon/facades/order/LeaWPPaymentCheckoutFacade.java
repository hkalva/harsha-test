package com.lyonscg.worldpaymentaddon.facades.order;

import com.worldpay.facades.order.WorldpayPaymentCheckoutFacade;


/**
 * Added a method to set billing address in payment as billing address in cart.
 *
 */
public interface LeaWPPaymentCheckoutFacade extends WorldpayPaymentCheckoutFacade
{
    /**
     * Sets payment address in PaymentInfo as cart's billing address
     *
     */
    void setAddressInPaymentAsCartBillingAddress();
}
