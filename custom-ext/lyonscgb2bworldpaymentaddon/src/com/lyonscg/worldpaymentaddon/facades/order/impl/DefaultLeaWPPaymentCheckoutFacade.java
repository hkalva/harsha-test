package com.lyonscg.worldpaymentaddon.facades.order.impl;

import com.lyonscg.worldpaymentaddon.facades.order.LeaWPPaymentCheckoutFacade;
import com.worldpay.core.checkout.WorldpayCheckoutService;
import com.worldpay.facades.order.impl.DefaultWorldpayPaymentCheckoutFacade;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;


/**
 * Lea-Worldpay implementation of LeaWPPaymentCheckoutFacade.
 */
public class DefaultLeaWPPaymentCheckoutFacade extends DefaultWorldpayPaymentCheckoutFacade implements LeaWPPaymentCheckoutFacade
{
    private WorldpayCheckoutService worldpayCheckoutService;

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAddressInPaymentAsCartBillingAddress()
    {
        final CartModel cart = getCart();
        final PaymentInfoModel paymentInfo = cart.getPaymentInfo();
        if (null != paymentInfo)
        {
            final AddressModel billingAddress = paymentInfo.getBillingAddress();
            if (null != billingAddress)
            {
                getWorldpayCheckoutService().setPaymentAddress(cart, billingAddress);
            }
        }
    }

    protected WorldpayCheckoutService getWorldpayCheckoutService()
    {
        return worldpayCheckoutService;
    }

    /**
     * Setter for worldpayCheckoutService. Also calls the super class's setter.
     * 
     * @param worldpayCheckoutService
     *            - worldpayCheckoutService instance.
     */
    @Override
    public void setWorldpayCheckoutService(WorldpayCheckoutService worldpayCheckoutService)
    {
        super.setWorldpayCheckoutService(worldpayCheckoutService);
        this.worldpayCheckoutService = worldpayCheckoutService;
    }

}
