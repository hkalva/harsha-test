package com.lyonscg.worldpaymentaddon.order.converters.populator;

import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;


/**
 * Custom Order populator with null check for billing address. This overrides Worldpay's populator by spring bean
 * definition.
 */
public class WorldpayAbstractOrderPopulator implements Populator<AbstractOrderModel, AbstractOrderData>
{
    private Converter<AddressModel, AddressData> addressConverter;

    /**
     * {@inheritDoc}
     */
    @Override
    public void populate(final AbstractOrderModel source, final AbstractOrderData target) throws ConversionException
    {
        target.setWorldpayOrderCode(source.getWorldpayOrderCode());
        target.setWorldpayDeclineCode(source.getWorldpayDeclineCode());
        final PaymentInfoModel paymentInfo = source.getPaymentInfo();
        if (paymentInfo != null && isNotCreditCard(paymentInfo) && null == target.getPaymentInfo())
        {
            // In the OrderConfirmationPage, orderData.PaymentInfo is "required" to be a CCPaymentInfoData, and cannot be null.
            final CCPaymentInfoData ccPaymentInfoData = new CCPaymentInfoData();
            final AddressModel billingAddress = paymentInfo.getBillingAddress();
            if (billingAddress != null)
            {
                ccPaymentInfoData.setBillingAddress(getAddressConverter().convert(billingAddress));
            }
            target.setPaymentInfo(ccPaymentInfoData);
        }
    }

    protected boolean isNotCreditCard(final PaymentInfoModel paymentInfo)
    {
        return !(paymentInfo instanceof CreditCardPaymentInfoModel);
    }

    @Required
    public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter)
    {
        this.addressConverter = addressConverter;
    }

    protected Converter<AddressModel, AddressData> getAddressConverter()
    {
        return addressConverter;
    }
}
