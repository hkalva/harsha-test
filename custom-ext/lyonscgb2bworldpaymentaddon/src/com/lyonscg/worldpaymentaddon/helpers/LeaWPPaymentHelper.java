package com.lyonscg.worldpaymentaddon.helpers;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.commons.lang.StringUtils;


/**
 * Helper class for payment related functions.
 *
 */
public class LeaWPPaymentHelper
{
    /**
     * Calculates order total.
     *
     * @param abstractOrderModel
     *            - AbstractOrderModel to calculate order total.
     * @return - Order total.
     */
    public BigDecimal calcTotalWithTax(final AbstractOrderModel abstractOrderModel)
    {
        if (abstractOrderModel == null)
        {
            throw new IllegalArgumentException("CartModel must not be null");
        }
        final Double totalPrice = abstractOrderModel.getTotalPrice();
        final Double totalTax = (abstractOrderModel.getNet().booleanValue() && abstractOrderModel.getStore() != null && abstractOrderModel
                .getStore().getExternalTaxEnabled().booleanValue()) ? abstractOrderModel.getTotalTax() : Double.valueOf(0d);
                final BigDecimal totalPriceWithoutTaxBD = new BigDecimal(totalPrice == null ? 0d : totalPrice.doubleValue()).setScale(2,
                        RoundingMode.HALF_EVEN);
                final BigDecimal totalPriceBD = new BigDecimal(totalTax == null ? 0d : totalTax.doubleValue()).setScale(2,
                        RoundingMode.HALF_EVEN).add(totalPriceWithoutTaxBD);
                return totalPriceBD;
    }

    /**
     * Masks all characters except last 4 characters.
     *
     * @param cardNumber
     *            - Card number for masking.
     * @param maskingChar
     *            - Character to mask.
     * @return - Masked card number.
     */
    public String maskCardNumber(final String cardNumber, final String maskingChar)
    {
        if (StringUtils.isBlank(cardNumber) || (null == maskingChar))
        {
            return cardNumber;
        }
        return cardNumber.replaceAll("\\d", maskingChar).substring(0, cardNumber.length() - 4)
                + cardNumber.substring(cardNumber.length() - 4);
    }
}
