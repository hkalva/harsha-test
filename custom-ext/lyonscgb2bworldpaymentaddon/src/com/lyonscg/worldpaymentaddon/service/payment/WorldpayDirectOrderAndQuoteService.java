package com.lyonscg.worldpaymentaddon.service.payment;

import de.hybris.platform.core.model.order.CartModel;

import com.worldpay.service.payment.WorldpayDirectOrderService;


/**
 * Extending WorldpayDirectOrderService to implement a new method processQuote for Quote processing Interface that
 * expose methods to authorise payments encrypted with the Worldpay CSE-library using the direct xml integration.
 * <p>
 * In order to successfully complete a direct xml integration with CSE, the request must be authorised first, and with
 * the response, update the transaction and order accordingly.
 */
public interface WorldpayDirectOrderAndQuoteService extends WorldpayDirectOrderService
{

    /**
     * Processes payment info for Quote orders.
     *
     * @param cartModel
     *            -Cart to be processed for quote payment.
     */
    void processQuote(final CartModel cartModel);

}
