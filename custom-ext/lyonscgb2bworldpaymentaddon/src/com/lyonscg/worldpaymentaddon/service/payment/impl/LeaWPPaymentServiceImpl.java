package com.lyonscg.worldpaymentaddon.service.payment.impl;

import java.math.BigDecimal;
import java.util.Currency;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.lyonscg.worldpaymentaddon.helpers.LeaWPPaymentHelper;

import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.CardInfo;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.impl.DefaultPaymentServiceImpl;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;


/**
 * Lea-Worldpay implementation of PaymentService.
 *
 */
public class LeaWPPaymentServiceImpl extends DefaultPaymentServiceImpl
{
    /** commerceCheckoutService bean. */
    private CommerceCheckoutService commerceCheckoutService;
    /** i18nService bean. */
    private I18NService i18nService;
    /** modelService bean. */
    private ModelService modelService;
    private LeaWPPaymentHelper leaWPPaymentHelper;
    private static final String ACCEPTED_STATUS = "ACCEPTED";
    private static final Logger LOG = Logger.getLogger(LeaWPPaymentServiceImpl.class);

    /**
     * Authorizes orders using Worldpay payment gateway.
     * 
     * @param order
     *            - The order to be sent for payment authorization.
     * @param card
     *            - Card information used for authorization.
     */
    @Override
    public PaymentTransactionEntryModel authorize(final OrderModel order, final CardInfo card) throws AdapterException
    {
        final PaymentInfoModel paymentInfo = order.getPaymentInfo();
        if (paymentInfo instanceof CreditCardPaymentInfoModel)
        {
            final CreditCardPaymentInfoModel creditCardPaymentInfo = (CreditCardPaymentInfoModel) paymentInfo;
            final String subscriptionID = creditCardPaymentInfo.getSubscriptionId();
            final String merchantTransactionCode = order.getCode() + "-" + getTime();
            return authorize(merchantTransactionCode, getLeaWPPaymentHelper().calcTotalWithTax(order), getI18nService()
                    .getBestMatchingJavaCurrency(order.getCurrency().getIsocode()), order.getDeliveryAddress(), subscriptionID,
                    card.getCv2Number(), getCommerceCheckoutService().getPaymentProvider());
        }
        return null;
    }

    /**
     * Authorizes payment using subscriptionID and other card details.
     * 
     * @param merchantTransactionCode
     *            - TransactionCode for payment authorization.
     * @param amount
     *            - Amount(Order Total/price) for authorization.
     * @param currency
     *            - Currency to be used for authorization.
     * @param deliveryAddress
     *            - Delivery address for the payment transaction.
     * @param subscriptionID
     *            - subscriptionID for the card.
     * @param cv2
     *            - Security code.
     * @param paymentProvider
     *            - Payment provider for authorization.
     */
    @Override
    public PaymentTransactionEntryModel authorize(final String merchantTransactionCode, final BigDecimal amount,
            final Currency currency, final AddressModel deliveryAddress, final String subscriptionID, final String cv2,
            final String paymentProvider) throws AdapterException
    {
        return setPaymentEntryAsNonPending(super.authorize(merchantTransactionCode, amount, currency, deliveryAddress,
                subscriptionID, cv2, paymentProvider));
    }

    /**
     * Sets PaymentTransactionEntryModel as not pending.
     * 
     * @param paymentTransactionEntry
     *            - Payment transaction entry.
     * @return paymentTransactionEntry with pending attribute set as false.
     */
    protected PaymentTransactionEntryModel setPaymentEntryAsNonPending(final PaymentTransactionEntryModel paymentTransactionEntry)
    {
        if (null != paymentTransactionEntry && PaymentTransactionType.AUTHORIZATION == paymentTransactionEntry.getType()
                && ACCEPTED_STATUS.equals(paymentTransactionEntry.getTransactionStatus()))
        {
            paymentTransactionEntry.setPending(Boolean.FALSE);
            getModelService().save(paymentTransactionEntry);
            LOG.info("Worldpay payment Authorised...");
        }
        return paymentTransactionEntry;
    }

    /** Returns time in milliseconds. */
    protected long getTime()
    {
        return System.currentTimeMillis();
    }

    /**
     * @return the commerceCheckoutService
     */
    public CommerceCheckoutService getCommerceCheckoutService()
    {
        return commerceCheckoutService;
    }

    /**
     * @param commerceCheckoutService
     *            the commerceCheckoutService to set
     */
    @Required
    public void setCommerceCheckoutService(final CommerceCheckoutService commerceCheckoutService)
    {
        this.commerceCheckoutService = commerceCheckoutService;
    }

    /**
     * @return the i18nService
     */
    public I18NService getI18nService()
    {
        return i18nService;
    }

    /**
     * @param i18nService
     *            the i18nService to set
     */
    @Required
    public void setI18nService(final I18NService i18nService)
    {
        this.i18nService = i18nService;
    }

    protected ModelService getModelService()
    {
        return modelService;
    }

    @Required
    public void setModelService(ModelService modelService)
    {
        super.setModelService(modelService);
        this.modelService = modelService;
    }

    protected LeaWPPaymentHelper getLeaWPPaymentHelper()
    {
        return leaWPPaymentHelper;
    }

    @Required
    public void setLeaWPPaymentHelper(LeaWPPaymentHelper leaWPPaymentHelper)
    {
        this.leaWPPaymentHelper = leaWPPaymentHelper;
    }

}
