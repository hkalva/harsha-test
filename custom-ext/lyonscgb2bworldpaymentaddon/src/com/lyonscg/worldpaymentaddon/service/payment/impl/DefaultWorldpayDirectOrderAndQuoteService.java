package com.lyonscg.worldpaymentaddon.service.payment.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.lyonscg.worldpaymentaddon.service.payment.WorldpayDirectOrderAndQuoteService;
import com.worldpay.data.CSEAdditionalAuthInfo;
import com.worldpay.exception.WorldpayException;
import com.worldpay.order.data.WorldpayAdditionalInfoData;
import com.worldpay.service.model.MerchantInfo;
import com.worldpay.service.payment.impl.DefaultWorldpayDirectOrderService;
import com.worldpay.service.payment.request.WorldpayRequestFactory;
import com.worldpay.service.request.CreateTokenServiceRequest;
import com.worldpay.service.request.UpdateTokenServiceRequest;
import com.worldpay.service.response.CreateTokenResponse;
import com.worldpay.service.response.UpdateTokenResponse;

import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;


/**
 * Lea-Worldpay's impelmentation of WorldpayDirectOrderAndQuoteService with methods for processing quote payements.
 *
 */
public class DefaultWorldpayDirectOrderAndQuoteService extends DefaultWorldpayDirectOrderService implements
        WorldpayDirectOrderAndQuoteService
{
    private static final Logger LOG = Logger.getLogger(DefaultWorldpayDirectOrderAndQuoteService.class);
    private WorldpayRequestFactory worldpayRequestFactory;
    private EnumerationService enumerationService;

    /***
     * {@inheritDoc}
     */
    @Override
    public void processQuote(final CartModel cartModel)
    {
        LOG.debug("Processing Quote order...");
        final PaymentInfoModel paymentInfoModel = cartModel.getPaymentInfo();
        cloneAndSetBillingAddressFromCart(cartModel, paymentInfoModel);
        final CommerceCheckoutParameter commerceCheckoutParameter = createCommerceCheckoutParameter(cartModel, paymentInfoModel,
                getTotalAmount(cartModel));
        getCommerceCheckoutService().setPaymentInfo(commerceCheckoutParameter);
    }

    /**
     * Fixes the conflict credit card token issue. Creates a new CreditCardModel if updateCreditCardPaymentInfo returns
     * null. {@inheritDoc}
     *
     */
    @Override
    public void createToken(final MerchantInfo merchantInfo, final CartModel cartModel,
            final CSEAdditionalAuthInfo cseAdditionalAuthInfo, final WorldpayAdditionalInfoData worldpayAdditionalInfoData)
            throws WorldpayException
    {
        final CreateTokenServiceRequest createTokenRequest = getWorldpayRequestFactory().buildTokenRequest(merchantInfo,
                cartModel, cseAdditionalAuthInfo, worldpayAdditionalInfoData);
        final CreateTokenResponse createTokenResponse = getWorldpayOrderService().getWorldpayServiceGateway().createToken(
                createTokenRequest);
        CreditCardPaymentInfoModel creditCardPaymentInfoModel;
        if (createTokenResponse.isError())
        {
            throw new WorldpayException(createTokenResponse.getErrorDetail().getMessage());
        }
        if (createTokenRepliesWithConflict(createTokenResponse))
        {
            final UpdateTokenServiceRequest updateTokenServiceRequest = getWorldpayRequestFactory().buildTokenUpdateRequest(
                    merchantInfo, cseAdditionalAuthInfo, worldpayAdditionalInfoData, createTokenResponse);
            final UpdateTokenResponse updateTokenResponse = getWorldpayOrderService().getWorldpayServiceGateway().updateToken(
                    updateTokenServiceRequest);
            if (updateTokenResponse.isError())
            {
                throw new WorldpayException(updateTokenResponse.getErrorDetail().getMessage());
            }
            creditCardPaymentInfoModel = getWorldpayPaymentInfoService().updateCreditCardPaymentInfo(cartModel,
                    updateTokenServiceRequest);
            if (null == creditCardPaymentInfoModel)
            {
                creditCardPaymentInfoModel = getWorldpayPaymentInfoService().createCreditCardPaymentInfo(cartModel,
                        createTokenResponse, BooleanUtils.isTrue(cseAdditionalAuthInfo.getSaveCard()));
            }
        }
        else
        {
            creditCardPaymentInfoModel = getWorldpayPaymentInfoService().createCreditCardPaymentInfo(cartModel,
                    createTokenResponse, BooleanUtils.isTrue(cseAdditionalAuthInfo.getSaveCard()));
        }
        if (creditCardPaymentInfoModel != null)
        {
            cartModel.setPaymentInfo(creditCardPaymentInfoModel);
            getModelService().save(cartModel);
            cloneAndSetBillingAddressFromCart(cartModel, creditCardPaymentInfoModel);
            if (BooleanUtils.isTrue(cseAdditionalAuthInfo.getSaveCard()))
            {
                creditCardPaymentInfoModel.setSaved(true);
            }
            populateCardType(cseAdditionalAuthInfo, creditCardPaymentInfoModel);
            getModelService().save(creditCardPaymentInfoModel);
        }
    }

    /**
     * Populates card type.
     * 
     * @param cseAdditionalAuthInfo
     *            - CSEAdditionalAuthInfo containing Card type
     * @param creditCardPaymentInfoModel
     *            - CreditCardPaymentInfoModel in which the card type has to be set.
     */
    protected void populateCardType(final CSEAdditionalAuthInfo cseAdditionalAuthInfo,
            CreditCardPaymentInfoModel creditCardPaymentInfoModel)
    {
        String creditCardTypeValue = cseAdditionalAuthInfo.getCardType();
        if (StringUtils.isNotBlank(creditCardTypeValue))
        {
            CreditCardType cardType = null;
            try
            {
                cardType = getEnumerationService().getEnumerationValue(CreditCardType.class.getSimpleName(), creditCardTypeValue);
            }
            catch (UnknownIdentifierException e)
            {
                LOG.warn("Unable to find card type", e);
            }
            if (null != cardType)
            {
                creditCardPaymentInfoModel.setType(cardType);
            }
        }
    }

    /***
     * Calculate order total for Authorization.
     * 
     * @param order
     *            - Order to be calculated.
     * @return - order total.
     */
    protected BigDecimal getTotalAmount(final AbstractOrderModel order)
    {
        final Double totalPrice = order.getTotalPrice();
        final Double totalTax = (order.getNet().booleanValue() && order.getStore() != null && order.getStore()
                .getExternalTaxEnabled().booleanValue()) ? order.getTotalTax() : Double.valueOf(0d);
        final BigDecimal totalPriceWithoutTaxBD = new BigDecimal(totalPrice == null ? 0d : totalPrice.doubleValue()).setScale(2,
                RoundingMode.HALF_EVEN);
        final BigDecimal totalPriceBD = new BigDecimal(totalTax == null ? 0d : totalTax.doubleValue()).setScale(2,
                RoundingMode.HALF_EVEN).add(totalPriceWithoutTaxBD);
        return totalPriceBD;
    }

    protected WorldpayRequestFactory getWorldpayRequestFactory()
    {
        return worldpayRequestFactory;
    }

    /**
     * Sets worldpayRequestFactory for this class and calls the super class's setter.
     * 
     * @param worldpayRequestFactory
     *            - injected {@link WorldpayRequestFactory}
     */
    @Override
    @Required
    public void setWorldpayRequestFactory(WorldpayRequestFactory worldpayRequestFactory)
    {
        super.setWorldpayRequestFactory(worldpayRequestFactory);
        this.worldpayRequestFactory = worldpayRequestFactory;
    }

    public EnumerationService getEnumerationService()
    {
        return enumerationService;
    }

    @Required
    public void setEnumerationService(EnumerationService enumerationService)
    {
        this.enumerationService = enumerationService;
    }
}
