package com.lyonscg.worldpaymentaddon.facade;

import com.worldpay.order.data.WorldpayAdditionalInfoData;


/**
 * Lea Payment facade to handle payment transactions.
 *
 */
public interface LeaWPPaymentFacade
{
    /**
     * Authorize Quote orders.
     * 
     * @param-abstractOrderCode order code to authorize order.
     * @param-worldPayAdditionalInfoData additional information to authorize order.
     * @return-true if successfully authorized.
     * */
    boolean authorize(final String abstractOrderCode, final WorldpayAdditionalInfoData worldPayAdditionalInfoData);
}
