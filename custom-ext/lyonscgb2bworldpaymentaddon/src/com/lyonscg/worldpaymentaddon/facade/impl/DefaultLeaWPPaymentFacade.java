package com.lyonscg.worldpaymentaddon.facade.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.lyonscg.worldpaymentaddon.facade.LeaWPPaymentFacade;
import com.worldpay.order.data.WorldpayAdditionalInfoData;
import com.worldpay.strategy.WorldpayAuthenticatedShopperIdStrategy;
import com.worldpay.util.WorldpayUtil;

import de.hybris.platform.b2b.services.B2BOrderService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.dto.CardInfo;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.model.ModelService;


/**
 * Lea Payment facade implementation to handle payment transactions.
 *
 */
public class DefaultLeaWPPaymentFacade implements LeaWPPaymentFacade
{
    protected static final Logger LOG = Logger.getLogger(DefaultLeaWPPaymentFacade.class);
    /** ACCEPTED TransactionStatus. */
    private static final String ACCEPTED_STATUS = "ACCEPTED";
    /** b2bOrderService bean. */
    private B2BOrderService b2bOrderService;
    /** paymentService bean. */
    private PaymentService paymentService;
    /** worldpayAuthenticatedShopperIdStrategy bean. */
    private WorldpayAuthenticatedShopperIdStrategy worldpayAuthenticatedShopperIdStrategy;
    /** modelService bean. */
    private ModelService modelService;

    /** {@inheritDoc} */
    @Override
    public boolean authorize(final String abstractOrderCode, final WorldpayAdditionalInfoData worldPayAdditionalInfoData)
    {
        final OrderModel order = getB2bOrderService().getOrderForCode(abstractOrderCode);
        if (null != order)
        {
            populateShopperId(worldPayAdditionalInfoData, order);
            final String cvv = WorldpayUtil.serializeWorldpayAdditionalInfo(worldPayAdditionalInfoData);
            final CardInfo paramCardInfo = new CardInfo();
            paramCardInfo.setCv2Number(cvv);
            final PaymentTransactionEntryModel paymentTransactionEntry = getPaymentService().authorize(order, paramCardInfo);
            if (null != paymentTransactionEntry)
            {
                paymentTransactionEntry.setPending(Boolean.FALSE);
                final PaymentTransactionModel paymentTransaction = paymentTransactionEntry.getPaymentTransaction();
                paymentTransaction.setOrder(order);
                getModelService().saveAll(paymentTransactionEntry, paymentTransaction);
                if (ACCEPTED_STATUS.equals(paymentTransactionEntry.getTransactionStatus()))
                {
                    LOG.debug("Payment authorization Successful for order " + order.getCode());
                    return true;
                }
                LOG.warn("Payment Transaction Status is " + paymentTransactionEntry.getTransactionStatus() + " for order "
                        + order.getCode() + ". Order not authorized.");
                return false;
            }
            LOG.error("Unable to create PaymentTransactionEntryModel for order " + order.getCode());
        }
        return false;
    }

    /** Populates user id from order. */
    private void populateShopperId(final WorldpayAdditionalInfoData worldPayAdditionalInfoData, final OrderModel order)
    {
        worldPayAdditionalInfoData.setAuthenticatedShopperId(getWorldpayAuthenticatedShopperIdStrategy()
                .getAuthenticatedShopperId(order.getUser()));
    }

    /**
     * @return the paymentService
     */
    public PaymentService getPaymentService()
    {
        return paymentService;
    }

    /**
     * @param paymentService
     *            the paymentService to set
     */
    @Required
    public void setPaymentService(final PaymentService paymentService)
    {
        this.paymentService = paymentService;
    }

    /**
     * @return the worldpayAuthenticatedShopperIdStrategy
     */
    public WorldpayAuthenticatedShopperIdStrategy getWorldpayAuthenticatedShopperIdStrategy()
    {
        return worldpayAuthenticatedShopperIdStrategy;
    }

    /**
     * @param worldpayAuthenticatedShopperIdStrategy
     *            the worldpayAuthenticatedShopperIdStrategy to set
     */
    @Required
    public void setWorldpayAuthenticatedShopperIdStrategy(
            final WorldpayAuthenticatedShopperIdStrategy worldpayAuthenticatedShopperIdStrategy)
    {
        this.worldpayAuthenticatedShopperIdStrategy = worldpayAuthenticatedShopperIdStrategy;
    }

    /**
     * @return the b2bOrderService
     */
    public B2BOrderService getB2bOrderService()
    {
        return b2bOrderService;
    }

    /**
     * @param b2bOrderService
     *            the b2bOrderService to set
     */
    @Required
    public void setB2bOrderService(final B2BOrderService b2bOrderService)
    {
        this.b2bOrderService = b2bOrderService;
    }

    /**
     * @return the modelService
     */
    public ModelService getModelService()
    {
        return modelService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService)
    {
        this.modelService = modelService;
    }

}
