package com.capgemini.dataxfer.product.export.cronjob;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.productexportservices.config.ProductExportConfig;
import com.capgemini.productexportservices.cronjob.ProductExportCronJobPerformable;
import com.capgemini.productexportservices.enums.ProductExportType;
import com.capgemini.productexportservices.facade.ProductDataExporterFacade;
import com.capgemini.productexportservices.model.ProductExportCronJobModel;


/**
 * Unit test for {@link ProductExportCronJobPerformable}.
 */
@UnitTest
public class ProductExportCronJobPerformableTest
{
	/**
	 * ProductExportCronJobPerformable to be tested.
	 */
	private ProductExportCronJobPerformable productExportCronJobPerformable;
	/**
	 * productDataExporterFacade for ProductExportCronJobPerformable.
	 */
	@Mock
	private ProductDataExporterFacade productDataExporterFacade;
	/**
	 * cronJob for ProductExportCronJobPerformable.
	 */
	private ProductExportCronJobModel cronJob;

	/**
	 * Setup initial data.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		productExportCronJobPerformable = new ProductExportCronJobPerformable();
		productExportCronJobPerformable.setProductDataExporter(productDataExporterFacade);
		cronJob = new ProductExportCronJobModel();
		cronJob.setQueryEnabled(Boolean.FALSE);
		cronJob.setQuery("");
		cronJob.setQuerySearchParameter(new HashMap<String, String>());
		cronJob.setCatalogVersion(new CatalogVersionModel());
		cronJob.setApprovalStatuses(Collections.singletonList(ArticleApprovalStatus.APPROVED));
		cronJob.setProductOfflineDate(new Date());
		cronJob.setProductOnlineDate(new Date());
		cronJob.setRendererTemplate(new RendererTemplateModel());
		cronJob.setExportFilePath("");
		cronJob.setFileNameExt("");
		cronJob.setFileEncoding("");
		cronJob.setExportStrategy(ProductExportType.JAXB);
	}

	/**
	 * Test perform success.
	 */
	@Test
	public void testPerformForSuccess()
	{
		given(Boolean.valueOf(productDataExporterFacade.exportProducts(Mockito.any(ProductExportConfig.class)))).willReturn(
				Boolean.TRUE);
		final PerformResult result = productExportCronJobPerformable.perform(cronJob);
		verify(productDataExporterFacade).exportProducts(Mockito.any(ProductExportConfig.class));
		Assert.assertEquals("Cronjob should be successful", CronJobResult.SUCCESS, result.getResult());
		Assert.assertEquals("Cronjob should have finished", CronJobStatus.FINISHED, result.getStatus());
	}

	/**
	 * Test perform failure.
	 */
	@Test
	public void testPerformForFailure()
	{
		given(Boolean.valueOf(productDataExporterFacade.exportProducts(Mockito.any(ProductExportConfig.class)))).willReturn(
				Boolean.FALSE);
		final PerformResult result = productExportCronJobPerformable.perform(cronJob);
		verify(productDataExporterFacade).exportProducts(Mockito.any(ProductExportConfig.class));
		Assert.assertEquals("Cronjob should be successful", CronJobResult.FAILURE, result.getResult());
		Assert.assertEquals("Cronjob should have finished", CronJobStatus.FINISHED, result.getStatus());
	}

}
