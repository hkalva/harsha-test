package com.capgemini.productexportservices.writer.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doReturn;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.velocity.exception.VelocityException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.productexportservices.context.ProductDetailsContext;
import com.capgemini.productexportservices.context.ProductsDetailsContext;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;


/** Unit test for {@link JAXBProductDetailsFileWriterImpl}. */

@UnitTest
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(LogRunListener.class)
public class JAXBProductDetailsFileWriterImplTest
{
    /**
     * JAXBProductDetailsFileWriterImpl to be tested.
     */
    private JAXBProductDetailsFileWriterImpl jaxbProductDetailsFileWriterImpl;

    @Mock
    private ConfigurationService configurationService;
    @Mock
    private Configuration configuration;
    @Mock
    private ProductDetailsContext singleProductContext;
    @Mock
    private ProductsDetailsContext multipleProductContext;

    private ProductData productData;

    private static final String PRODUCT_CODE = "productCode";
    private static final Logger LOG = LogManager.getLogger(JAXBProductDetailsFileWriterImplTest.class);

    /**
     * Setup initial data.
     */
    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);

        jaxbProductDetailsFileWriterImpl = new JAXBProductDetailsFileWriterImpl();

        productData = new ProductData();
        productData.setCode(PRODUCT_CODE);

        doReturn(productData).when(singleProductContext).getProductData();
        doReturn(Collections.singletonList(productData)).when(multipleProductContext).getProductDatas();
    }

    /**
     * Test write a single product to file.
     */
    @Test
    public void testWriteSingleProductToFile()
    {
        try
        {
            final File singleProductFile = File.createTempFile("single", ".xml");
            assertTrue(jaxbProductDetailsFileWriterImpl.writeProductToFile(null, singleProductFile, null, singleProductContext));
            final String singleProductXml = FileUtils.readFileToString(singleProductFile);
            assertTrue(singleProductXml.contains("<code>" + PRODUCT_CODE + "</code>"));
            assertTrue(singleProductXml.contains("<ProductDetails>"));
            assertTrue(singleProductXml.contains("</ProductDetails>"));
            assertTrue(true);
        }
        catch (IOException e)
        {
            LOG.error(e);
            fail();
        }
    }

    /**
     * Test write multiple products to file.
     */
    @Test
    public void testWriteMultipleProductsToFile()
    {
        try
        {
            final File multipleProductFile = File.createTempFile("multiple", ".xml");
            assertTrue(jaxbProductDetailsFileWriterImpl.writeProductToFile(null, multipleProductFile, null,
                    multipleProductContext));
            final String xmlString = FileUtils.readFileToString(multipleProductFile);
            assertTrue(xmlString.contains("<code>" + PRODUCT_CODE + "</code>"));
            assertTrue(xmlString.contains("<productDatas>"));
            assertTrue(xmlString.contains("</productDatas>"));
            assertTrue(xmlString.contains("<ProductsDetails>"));
            assertTrue(xmlString.contains("</ProductsDetails>"));
        }
        catch (IOException e)
        {
            LOG.error(e);
            fail();
        }
    }

    /**
     * Test File NULL Error with a single product.
     */
    @Test
    public void testWriteSingleProductToFileFileNullError()
    {
        try
        {
            jaxbProductDetailsFileWriterImpl.writeProductToFile(null, null, null, singleProductContext);
        }
        catch (IOException e)
        {
            assertEquals(IOException.class, e.getClass());
        }
    }

    /**
     * Test File NULL Error with multiple products.
     */
    @Test
    public void testWriteMultipleProductsToFileFileNullError()
    {
        try
        {
            jaxbProductDetailsFileWriterImpl.writeProductToFile(null, null, null, multipleProductContext);
        }
        catch (IOException e)
        {
            assertEquals(IOException.class, e.getClass());
        }
    }

    /**
     * Test Context NULL Error.
     */
    @Test
    public void testWriteProductToFileContextNullError()
    {
        try
        {
            final File productFile = File.createTempFile("test", ".xml");
            jaxbProductDetailsFileWriterImpl.writeProductToFile(null, productFile, null, null);
        }
        catch (VelocityException | IOException e)
        {
            assertEquals(VelocityException.class, e);
        }
    }
}
