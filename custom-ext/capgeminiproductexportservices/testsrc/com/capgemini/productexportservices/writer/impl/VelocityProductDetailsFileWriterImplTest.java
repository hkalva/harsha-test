package com.capgemini.productexportservices.writer.impl;

import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.commons.renderer.RendererService;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;

import org.apache.velocity.VelocityContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.productexportservices.context.ProductDetailsContext;
import com.capgemini.productexportservices.context.ProductsDetailsContext;



/** Unit test for {@link VelocityProductDetailsFileWriterImpl} */

@UnitTest
public class VelocityProductDetailsFileWriterImplTest
{
	private VelocityProductDetailsFileWriterImpl velocityProductDetailsFileWriterImpl;
	@Mock
	private RendererService rendererService;
	private VelocityContext productDetailsContext;
	private VelocityContext productsDetailsContext;
	private String encoding;
	private RendererTemplateModel template;

	/**
	 * Setup initial data
	 */
	@Before
	public void setUp()
	{
		velocityProductDetailsFileWriterImpl = new VelocityProductDetailsFileWriterImpl();
		MockitoAnnotations.initMocks(this);
		velocityProductDetailsFileWriterImpl.setRendererService(rendererService);
		productDetailsContext = new ProductDetailsContext();
		productsDetailsContext = new ProductsDetailsContext();
		template = new RendererTemplateModel();
		final ProductData productData = new ProductData();
		encoding = "UTF-8";
		((ProductDetailsContext) productDetailsContext).setProductData(productData);
		((ProductsDetailsContext) productsDetailsContext).setProductDatas(Collections.singletonList(productData));
		Mockito
				.doNothing()
				.when(rendererService)
				.render(Mockito.any(RendererTemplateModel.class), Mockito.any(ProductDetailsContext.class),
						Mockito.any(StringWriter.class));
		Mockito
				.doNothing()
				.when(rendererService)
				.render(Mockito.any(RendererTemplateModel.class), Mockito.any(ProductsDetailsContext.class),
						Mockito.any(StringWriter.class));
	}

	/**
	 * Test Write Product To File
	 *
	 * @throws IOException
	 */
	@Test
	public void testWriteProductToFile() throws IOException
	{
		final File file = File.createTempFile("test", ".xml");
		final String encoding = "UTF-8";
		Assert.assertTrue(velocityProductDetailsFileWriterImpl.writeProductToFile(template, file, encoding, productDetailsContext));
		verify(rendererService).render(Mockito.eq(template), Mockito.eq(productDetailsContext), Mockito.any(StringWriter.class));
	}

	/**
	 * Test Write Product To Single File
	 *
	 * @throws IOException
	 */
	@Test
	public void testWriteProductToSingleFile() throws IOException
	{
		final File file = File.createTempFile("test", ".xml");
		final String encoding = "UTF-8";
		Assert.assertTrue(velocityProductDetailsFileWriterImpl.writeProductToFile(template, file, encoding, productsDetailsContext));
		verify(rendererService).render(Mockito.eq(template), Mockito.eq(productsDetailsContext), Mockito.any(StringWriter.class));
	}

	/**
	 * Test Assert File null Error
	 *
	 * @throws IOException
	 */
	@Test(expected = AssertionError.class)
	public void testWriteProductToFileFileNullError() throws IOException
	{
		velocityProductDetailsFileWriterImpl.writeProductToFile(null, null, null, productDetailsContext);
	}

	/**
	 * Test Assert Context null Error
	 *
	 * @throws IOException
	 */
	@Test(expected = AssertionError.class)
	public void testWriteProductToFileContextNullError() throws IOException
	{
		final File file = File.createTempFile("test", ".xml");
		velocityProductDetailsFileWriterImpl.writeProductToFile(null, file, null, null);
	}

	/**
	 * Test Assert Encoding null Error
	 *
	 * @throws IOException
	 */
	@Test(expected = AssertionError.class)
	public void testWriteProductToFileEncodingNullError() throws IOException
	{
		final File file = File.createTempFile("test", ".xml");
		velocityProductDetailsFileWriterImpl.writeProductToFile(template, file, null, productDetailsContext);
	}

	/**
	 * Test Assert Template null Error
	 *
	 * @throws IOException
	 */
	@Test(expected = AssertionError.class)
	public void testWriteProductToFileTemplateNullError() throws IOException
	{
		final File file = File.createTempFile("test", ".xml");
		velocityProductDetailsFileWriterImpl.writeProductToFile(null, file, encoding, productDetailsContext);
	}
}
