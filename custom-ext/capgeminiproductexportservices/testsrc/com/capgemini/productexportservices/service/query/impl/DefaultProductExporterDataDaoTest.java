package com.capgemini.productexportservices.service.query.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Test class for DefaultProductExporterDataDao.
 */
public class DefaultProductExporterDataDaoTest
{
    /**
     * Class to be tested.
     */
    private DefaultProductExporterDataDao defaultProductExporterDataDao;
    /**
     * Mock FlexibleSearchService for testing.
     */
    @Mock
    private FlexibleSearchService flexibleSearchService;
    /**
     * Mock searchResults for testing.
     */
    @Mock
    private SearchResult<ProductModel> searchResults;
    /**
     * Mock ProductModel results for testing.
     */
    @Mock
    private List<ProductModel> products;
    /**
     * catalogVersion for Testing.
     */
    private CatalogVersionModel catalogVersion;
    /**
     * productStatuses for Testing.
     */
    private List<ArticleApprovalStatus> productApprovalStatuses;
    /**
     * customQuery for Testing.
     */
    private String customQuery;
    /**
     * customQueryParams for Testing.
     */
    private Map<String, String> customQueryParams;
    /**
     * productNumber for Testing.
     */
    private String productNumber;
    /**
     * Product online offline dates for Testing.
     */
    private Date productOnlineDate, productOfflineDate;

    /**
     * Setup initial data.
     */
    @SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);

        productNumber = "000000001";
        customQuery = "SELECT {pk} FROM {Product} WHERE {code} =?code";
        customQueryParams = new HashMap<String, String>(1);
        customQueryParams.put("code", productNumber);
        productOnlineDate = new Date();
        productOfflineDate = new Date();
        catalogVersion = new CatalogVersionModel();

        defaultProductExporterDataDao = new DefaultProductExporterDataDao();
        defaultProductExporterDataDao.setFlexibleSearchService(flexibleSearchService);

        productApprovalStatuses = Collections.singletonList(ArticleApprovalStatus.APPROVED);

        given(flexibleSearchService.search(anyString(), anyMap())).willReturn(searchResults);
        given(searchResults.getResult()).willReturn(products);
    }

    /**
     * Test default query.
     */
    @Test
    public void testGetProductsDefaultQuery()
    {
        products = defaultProductExporterDataDao.getProducts(Boolean.FALSE, null, null, catalogVersion, productApprovalStatuses,
                null, null);
        Assert.assertTrue(CollectionUtils.isNotEmpty(products));
    }

    /**
     * Test default query with dates.
     */
    @Test
    public void testGetProductsDefaultQueryWithDates()
    {
        products = defaultProductExporterDataDao.getProducts(Boolean.FALSE, null, null, catalogVersion, productApprovalStatuses,
                productOnlineDate, productOfflineDate);
        Assert.assertTrue(CollectionUtils.isNotEmpty(products));
    }

    /**
     * Test custom query.
     */
    @Test
    public void testGetProductsCustomQuery()
    {
        products = defaultProductExporterDataDao.getProducts(true, customQuery, customQueryParams, null, null, null, null);
        Assert.assertTrue(CollectionUtils.isNotEmpty(products));
    }
}
