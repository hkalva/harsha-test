package com.capgemini.productexportservices.facade.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.productexportservices.config.ProductExportConfig;
import com.capgemini.productexportservices.context.ProductDetailsContext;
import com.capgemini.productexportservices.context.ProductsDetailsContext;
import com.capgemini.productexportservices.enums.ProductExportType;
import com.capgemini.productexportservices.service.query.impl.DefaultProductExporterDataDao;
import com.capgemini.productexportservices.writer.ProductDetailsFileWriter;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.converters.ConfigurablePopulator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import net.sf.saxon.s9api.SaxonApiException;


/**
 * Unit test for {@link DefaultProductDataExporterFacade}.
 */
@UnitTest
public class DefaultProductDataExporterFacadeTest
{
    /**
     * Product tax code field name.
     */
    private static final String PRODUCT_TAX_CODE = "PRODUCT_TAX_CODE";
    /**
     * DefaultProductDataExporterFacade to be tested.
     */
    private DefaultProductDataExporterFacade defaultProductExportFacade;
    /**
     * productConverter for DefaultProductDataExporterFacade.
     */
    @Mock
    private Converter<ProductModel, ProductData> productConverter;
    /**
     * flexibleSearchService for DefaultProductDataExporterFacade.
     */
    @Mock
    private FlexibleSearchService flexibleSearchService;
    /**
     * productDetailsFileWriterMap for DefaultProductDataExporterFacade.
     */
    @Mock
    private Map<ProductExportType, ProductDetailsFileWriter> productDetailsFileWriterMap;
    /**
     * productDetailsFileWriter for DefaultProductDataExporterFacade.
     */
    @Mock
    private ProductDetailsFileWriter productDetailsFileWriter;
    /**
     * Product options populator for DefaultProductDataExporterFacade.
     */
    @Mock
    private ConfigurablePopulator<ProductModel, ProductData, ProductOption> productExportConfiguredPopulator;
    /**
     * productExportConfig for Testing.
     */
    private ProductExportConfig productExportConfig;
    /**
     * catalogVersion for Testing.
     */
    private CatalogVersionModel catalogVersion;

    /**
     * fileEncoding for Testing.
     */
    private String fileEncoding;
    /**
     * template for Testing.
     */
    private RendererTemplateModel template;
    /**
     * productStatuses for Testing.
     */
    private List<ArticleApprovalStatus> productApprovalStatuses;

    /**
     * defQueryParams for Testing.
     */
    private Map<String, Object> defQueryParams;
    /**
     * defQueryParamsWithDates for Testing.
     */
    private Map<String, Object> defQueryParamsWithDates;
    /**
     * customQuery for Testing.
     */
    private String customQuery;

    /**
     * customQueryParams for Testing.
     */
    private Map<String, String> customQueryParams;

    /**
     * searchResults for Testing.
     */
    @Mock
    private SearchResult searchResults;
    /**
     * productModel for Testing.
     */
    private ProductModel productModel;

    /**
     * productData for Testing.
     */
    private ProductData productData;

    /**
     * productNumber for Testing.
     */
    private String productNumber;

    /**
     * defQuery for Testing.
     */
    private String defQuery;
    /**
     * defQueryWithDates for Testing.
     */
    private String defQueryWithDates;

    /**
     * Product online offline dates for Testing.
     */
    private Date productOnlineDate, productOfflineDate;
    /**
     * Mock ConfigurationService for Testing.
     */
    @Mock
    private ConfigurationService configurationService;
    /**
     * Mock Configuration for Testing.
     */
    @Mock
    private Configuration configuration;
    /**
     * ProductOptions for Testing.
     */
    private List<ProductOption> productOptions;
    /**
     * Tax code for testing
     */
    private String defaultTaxCode;
    /**
     * Product search service
     */
    private DefaultProductExporterDataDao defaultProductExporterDataDao;
    /**
     * ProductModels for Testing.
     */
    private List<ProductModel> productModels;

    /**
     * Setup initial data.
     */
    @Before
    public void setUp()
    {
        productNumber = "000000001";
        defQuery = "SELECT {pk} FROM {Product} WHERE {catalogVersion} = ?catalogVersion AND {approvalStatus} IN (?approvalStatuses)";
        defQueryWithDates = "SELECT {pk} FROM {Product} WHERE {catalogVersion} = ?catalogVersion AND {approvalStatus} IN (?approvalStatuses) AND {onlineDate}>=?productOnlineDate AND {offlineDate}<=?productOfflineDate";
        customQuery = "SELECT {pk} FROM {Product} WHERE {code} =?code";
        customQueryParams = new HashMap<String, String>(1);
        customQueryParams.put("code", productNumber);
        defaultProductExportFacade = new DefaultProductDataExporterFacade();
        defaultProductExporterDataDao = new DefaultProductExporterDataDao();
        defaultProductExportFacade.setProductExporterDataDao(defaultProductExporterDataDao);

        productOnlineDate = new Date();
        productOfflineDate = new Date();
        defaultTaxCode = "DEFAULT_TAX_CODE";
        productOptions = Arrays.asList(ProductOption.values());
        MockitoAnnotations.initMocks(this);
        defaultProductExportFacade.setDateFormat("yyyy_MM_dd_HH_mm_ss_SSS");
        defaultProductExportFacade.setProductConverter(productConverter);
        defaultProductExportFacade.setProductDetailsFileWriterMap(productDetailsFileWriterMap);
        defaultProductExportFacade.setDateTimeFormatter(DateTimeFormatter.ofPattern(defaultProductExportFacade.getDateFormat()));
        defaultProductExportFacade.setProductExportConfiguredPopulator(productExportConfiguredPopulator);
        defaultProductExportFacade.setConfigurationService(configurationService);
        defaultProductExportFacade.setProductOptions(productOptions);
        productData = new ProductData();
        productData.setCode(productNumber);

        productModel = new ProductModel();
        productModel.setCode(productNumber);

        productModels = new ArrayList<ProductModel>();
        productModels.add(productModel);
        productExportConfig = new ProductExportConfig();
        fileEncoding = "UTF-8";
        template = new RendererTemplateModel();
        catalogVersion = new CatalogVersionModel();
        productApprovalStatuses = Collections.singletonList(ArticleApprovalStatus.APPROVED);
        productExportConfig.setRendererTemplate(template);
        productExportConfig.setFileExportPath("/tmp/");
        productExportConfig.setFileNameExt(".xml");
        productExportConfig.setFileEncoding(fileEncoding);

        defQueryParams = new HashMap<String, Object>(2);
        defQueryParams.put("catalogVersion", catalogVersion);
        defQueryParams.put("approvalStatuses", productApprovalStatuses);

        defQueryParamsWithDates = new HashMap<String, Object>(4);
        defQueryParamsWithDates.put("catalogVersion", catalogVersion);
        defQueryParamsWithDates.put("approvalStatuses", productApprovalStatuses);
        defQueryParamsWithDates.put("productOnlineDate", productOnlineDate);
        defQueryParamsWithDates.put("productOfflineDate", productOfflineDate);

        defaultProductExportFacade.getProductExporterDataDao().setFlexibleSearchService(flexibleSearchService);

        given(flexibleSearchService.search(anyString(), anyMap())).willReturn(searchResults);
        given(productConverter.convert(Mockito.any(ProductModel.class))).willReturn(productData);
        given(searchResults.getResult()).willReturn(Collections.singletonList(productModel));
        given(productDetailsFileWriterMap.get(Mockito.any(ProductExportType.class))).willReturn(productDetailsFileWriter);
        given(configurationService.getConfiguration()).willReturn(configuration);
        given(configuration.getString(anyString())).willReturn(defaultTaxCode);
    }

    /**
     * Test default product export for success.
     *
     * @throws IOException
     *             When writing to file
     * @throws SaxonApiException 
     */
    @Test
    public void testDefExportProductsForSuccess() throws IOException, SaxonApiException
    {
        populateForDefaultQuery();
        given(
                Boolean.valueOf(productDetailsFileWriter.writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                        Mockito.eq(fileEncoding), Mockito.any(ProductDetailsContext.class)))).willReturn(Boolean.TRUE);

        Assert.assertTrue(defaultProductExportFacade.exportProducts(productExportConfig));
        verify(flexibleSearchService).search(defQuery, defQueryParams);
        verify(productConverter).convert(productModel);
        verify(productDetailsFileWriter).writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                Mockito.eq(fileEncoding), Mockito.any(ProductDetailsContext.class));
    }

    /**
     * Test default product export for success for Avalara.
     *
     * @throws IOException
     *             When writing to file
     * @throws SaxonApiException 
     */
    @Test
    public void testDefExportProductsWithOptionsForSuccess() throws IOException, SaxonApiException
    {
        populateForDefaultQuery();
        productExportConfig.setProductOptions(ProductOption.BASIC.name() + "," + ProductOption.TAX_CODE.name());
        productExportConfig.setTaxCountryIso(Locale.US.getCountry());
        final Map<String, String> areaTaxCode = new HashMap<String, String>();
        areaTaxCode.put(Locale.US.getCountry(), PRODUCT_TAX_CODE);
        productData.setAreaTaxCode(areaTaxCode);
        given(
                Boolean.valueOf(productDetailsFileWriter.writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                        Mockito.eq(fileEncoding), Mockito.any(ProductDetailsContext.class)))).willReturn(Boolean.TRUE);
        Assert.assertTrue(defaultProductExportFacade.exportProducts(productExportConfig));
        Assert.assertTrue(PRODUCT_TAX_CODE.equals(productData.getTaxCode()));
        verify(flexibleSearchService).search(defQuery, defQueryParams);
        verify(productConverter).convert(productModel);
        verify(productDetailsFileWriter).writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                Mockito.eq(fileEncoding), Mockito.any(ProductDetailsContext.class));
    }

    /**
     * Test default product export for success for Avalara default tax code.
     *
     * @throws IOException
     *             When writing to file
     * @throws SaxonApiException 
     */
    @Test
    public void testDefExportProductsWithOptionsForSuccessForDefaultTax() throws IOException, SaxonApiException
    {
        populateForDefaultQuery();
        productExportConfig.setProductOptions(ProductOption.BASIC.name() + "," + ProductOption.TAX_CODE.name());
        productExportConfig.setTaxCountryIso(Locale.US.getCountry());

        final Map<String, String> areaTaxCode = new HashMap<String, String>();
        areaTaxCode.put(Locale.US.getCountry(), null);
        productData.setAreaTaxCode(areaTaxCode);
        given(
                Boolean.valueOf(productDetailsFileWriter.writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                        Mockito.eq(fileEncoding), Mockito.any(ProductDetailsContext.class)))).willReturn(Boolean.TRUE);
        Assert.assertTrue(defaultProductExportFacade.exportProducts(productExportConfig));
        Assert.assertTrue(defaultTaxCode.equals(productData.getTaxCode()));
        verify(flexibleSearchService).search(defQuery, defQueryParams);
        verify(productConverter).convert(productModel);
        verify(productDetailsFileWriter).writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                Mockito.eq(fileEncoding), Mockito.any(ProductDetailsContext.class));
    }

    /**
     * Test default product export for success for Avalara default tax code for null map.
     *
     * @throws IOException
     *             When writing to file
     * @throws SaxonApiException 
     */
    @Test
    public void testDefExportProductsWithOptionsNullMapForSuccessForDefaultTax() throws IOException, SaxonApiException
    {
        populateForDefaultQuery();
        productExportConfig.setProductOptions(ProductOption.BASIC.name() + "," + ProductOption.TAX_CODE.name());
        productExportConfig.setTaxCountryIso(Locale.US.getCountry());
        productData.setAreaTaxCode(null);
        given(
                Boolean.valueOf(productDetailsFileWriter.writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                        Mockito.eq(fileEncoding), Mockito.any(ProductDetailsContext.class)))).willReturn(Boolean.TRUE);
        Assert.assertTrue(defaultProductExportFacade.exportProducts(productExportConfig));
        Assert.assertTrue(defaultTaxCode.equals(productData.getTaxCode()));
        verify(flexibleSearchService).search(defQuery, defQueryParams);
        verify(productConverter).convert(productModel);
        verify(productDetailsFileWriter).writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                Mockito.eq(fileEncoding), Mockito.any(ProductDetailsContext.class));
    }

    /**
     * Test default product export for success for Avalara default tax code for no country iso.
     *
     * @throws IOException
     *             When writing to file
     * @throws SaxonApiException 
     */
    @Test
    public void testDefExportProductsWithOptionsNoCountryForSuccessForNoTax() throws IOException, SaxonApiException
    {
        populateForDefaultQuery();
        productExportConfig.setTaxCountryIso(null);
        given(
                Boolean.valueOf(productDetailsFileWriter.writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                        Mockito.eq(fileEncoding), Mockito.any(ProductDetailsContext.class)))).willReturn(Boolean.TRUE);
        Assert.assertTrue(defaultProductExportFacade.exportProducts(productExportConfig));
        Assert.assertTrue(null == productData.getTaxCode());
        verify(flexibleSearchService).search(defQuery, defQueryParams);
        verify(productConverter).convert(productModel);
        verify(productDetailsFileWriter).writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                Mockito.eq(fileEncoding), Mockito.any(ProductDetailsContext.class));
    }

    /**
     * Test default product export with dates for success.
     *
     * @throws IOException
     *             When writing to file
     * @throws SaxonApiException 
     */
    @Test
    public void testDefExportProductsWithDatesForSuccess() throws IOException, SaxonApiException
    {
        populateForDefaultQuery();
        productExportConfig.setProductOnlineDate(productOnlineDate);
        productExportConfig.setProductOfflineDate(productOfflineDate);

        given(
                Boolean.valueOf(productDetailsFileWriter.writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                        Mockito.eq(fileEncoding), Mockito.any(ProductDetailsContext.class)))).willReturn(Boolean.TRUE);

        Assert.assertTrue(defaultProductExportFacade.exportProducts(productExportConfig));
        verify(flexibleSearchService).search(defQueryWithDates, defQueryParamsWithDates);
        verify(productConverter).convert(productModel);
        verify(productDetailsFileWriter).writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                Mockito.eq(fileEncoding), Mockito.any(ProductDetailsContext.class));
    }

    /**
     * Test single file default product export for success.
     *
     * @throws IOException
     *             When writing to file
     * @throws SaxonApiException 
     */
    @Test
    public void testSingleFileDefExportProductsForSuccess() throws IOException, SaxonApiException
    {
        populateForDefaultQuery();
        given(
                Boolean.valueOf(productDetailsFileWriter.writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                        Mockito.eq(fileEncoding), Mockito.any(ProductsDetailsContext.class)))).willReturn(Boolean.TRUE);
        productExportConfig.setIsSingleFile(true);
        Assert.assertTrue(defaultProductExportFacade.exportProducts(productExportConfig));
        verify(flexibleSearchService).search(defQuery, defQueryParams);
        verify(productConverter).convert(productModel);
        verify(productDetailsFileWriter).writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                Mockito.eq(fileEncoding), Mockito.any(ProductsDetailsContext.class));
    }

    /**
     * Test single file default product export for success.
     *
     * @throws IOException
     *             When writing to file
     * @throws SaxonApiException 
     */
    @Test
    public void testSingleFileDefExportProductsWithDateForSuccess() throws IOException, SaxonApiException
    {
        populateForDefaultQuery();
        productExportConfig.setProductOnlineDate(productOnlineDate);
        productExportConfig.setProductOfflineDate(productOfflineDate);
        given(
                Boolean.valueOf(productDetailsFileWriter.writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                        Mockito.eq(fileEncoding), Mockito.any(ProductsDetailsContext.class)))).willReturn(Boolean.TRUE);
        productExportConfig.setIsSingleFile(true);
        Assert.assertTrue(defaultProductExportFacade.exportProducts(productExportConfig));
        verify(flexibleSearchService).search(defQueryWithDates, defQueryParamsWithDates);
        verify(productConverter).convert(productModel);
        verify(productDetailsFileWriter).writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                Mockito.eq(fileEncoding), Mockito.any(ProductsDetailsContext.class));
    }

    /**
     * Test default product export for failure.
     *
     * @throws IOException
     *             When writing to file
     * @throws SaxonApiException 
     */
    @Test
    public void testDefExportProductsForFailure() throws IOException, SaxonApiException
    {
        populateForDefaultQuery();
        given(
                Boolean.valueOf(productDetailsFileWriter.writeProductToFile(Mockito.any(RendererTemplateModel.class),
                        Mockito.any(File.class), Mockito.any(String.class), Mockito.any(ProductDetailsContext.class))))
                .willThrow(new IOException());
        Assert.assertFalse(defaultProductExportFacade.exportProducts(productExportConfig));
        verify(flexibleSearchService).search(defQuery, defQueryParams);
        verify(productConverter).convert(productModel);
        verify(productDetailsFileWriter).writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                Mockito.eq(fileEncoding), Mockito.any(ProductDetailsContext.class));
    }

    /**
     * Test single file default product export for failure.
     *
     * @throws IOException
     *             When writing to file
     * @throws SaxonApiException 
     */
    @Test
    public void testSingleFileDefExportProductsForFailure() throws IOException, SaxonApiException
    {
        populateForDefaultQuery();
        productExportConfig.setIsSingleFile(true);
        given(
                Boolean.valueOf(productDetailsFileWriter.writeProductToFile(Mockito.any(RendererTemplateModel.class),
                        Mockito.any(File.class), Mockito.any(String.class), Mockito.any(ProductsDetailsContext.class))))
                .willThrow(new IOException());
        Assert.assertFalse(defaultProductExportFacade.exportProducts(productExportConfig));
        verify(flexibleSearchService).search(defQuery, defQueryParams);
        verify(productConverter).convert(productModel);
        verify(productDetailsFileWriter).writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                Mockito.eq(fileEncoding), Mockito.any(ProductsDetailsContext.class));
    }

    /**
     * Test default product export for null store.
     *
     */
    @Test(expected = AssertionError.class)
    public void testDefExportProductsForNullStore()
    {
        populateForDefaultQuery();
        productExportConfig.setCatalogVersion(null);
        defaultProductExportFacade.exportProducts(productExportConfig);
    }

    /**
     * Test default product export for null product status.
     *
     */
    @Test(expected = AssertionError.class)
    public void testDefExportProductsForNullProductStatus()
    {
        populateForDefaultQuery();
        productExportConfig.setApprovalStatuses(null);
        defaultProductExportFacade.exportProducts(productExportConfig);
    }

    /**
     * Test custom product export for success.
     *
     * @throws IOException
     *             When writing to file
     * @throws SaxonApiException 
     */
    @Test
    public void testCustomExportProductsForSuccess() throws IOException, SaxonApiException
    {
        populateForCustomQuery();
        given(
                Boolean.valueOf(productDetailsFileWriter.writeProductToFile(Mockito.any(RendererTemplateModel.class),
                        Mockito.any(File.class), Mockito.any(String.class), Mockito.any(ProductDetailsContext.class))))
                .willReturn(Boolean.TRUE);
        Assert.assertTrue(defaultProductExportFacade.exportProducts(productExportConfig));
        verify(flexibleSearchService).search(customQuery, customQueryParams);
        verify(productConverter).convert(productModel);
        verify(productDetailsFileWriter).writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                Mockito.eq(fileEncoding), Mockito.any(ProductDetailsContext.class));
    }

    /**
     * Test Single file custom product export for success.
     *
     * @throws IOException
     *             When writing to file
     * @throws SaxonApiException 
     */
    @Test
    public void testSingleFileCustomExportProductsForSuccess() throws IOException, SaxonApiException
    {
        populateForCustomQuery();
        productExportConfig.setIsSingleFile(true);
        given(
                Boolean.valueOf(productDetailsFileWriter.writeProductToFile(Mockito.any(RendererTemplateModel.class),
                        Mockito.any(File.class), Mockito.any(String.class), Mockito.any(ProductsDetailsContext.class))))
                .willReturn(Boolean.TRUE);
        Assert.assertTrue(defaultProductExportFacade.exportProducts(productExportConfig));
        verify(flexibleSearchService).search(customQuery, customQueryParams);
        verify(productConverter).convert(productModel);
        verify(productDetailsFileWriter).writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                Mockito.eq(fileEncoding), Mockito.any(ProductsDetailsContext.class));
    }

    /**
     * Test custom product export for failure.
     *
     * @throws IOException
     *             When writing to file
     * @throws SaxonApiException 
     */
    @Test
    public void testCustomExportProductsForFailure() throws IOException, SaxonApiException
    {
        populateForCustomQuery();
        given(
                Boolean.valueOf(productDetailsFileWriter.writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                        Mockito.eq(fileEncoding), Mockito.any(ProductDetailsContext.class)))).willThrow(new IOException());
        Assert.assertFalse(defaultProductExportFacade.exportProducts(productExportConfig));
        verify(flexibleSearchService).search(customQuery, customQueryParams);
        verify(productConverter).convert(productModel);
        verify(productDetailsFileWriter).writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                Mockito.eq(fileEncoding), Mockito.any(ProductDetailsContext.class));
    }

    /**
     * Test Single file custom product export for failure.
     *
     * @throws IOException
     *             When writing to file
     * @throws SaxonApiException 
     */
    @Test
    public void testSingleFileCustomExportProductsForFailure() throws IOException, SaxonApiException
    {
        populateForCustomQuery();
        productExportConfig.setIsSingleFile(true);
        given(
                Boolean.valueOf(productDetailsFileWriter.writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                        Mockito.eq(fileEncoding), Mockito.any(ProductsDetailsContext.class)))).willThrow(new IOException());

        Assert.assertFalse(defaultProductExportFacade.exportProducts(productExportConfig));
        verify(flexibleSearchService).search(customQuery, customQueryParams);
        verify(productConverter).convert(productModel);
        verify(productDetailsFileWriter).writeProductToFile(Mockito.eq(template), Mockito.any(File.class),
                Mockito.eq(fileEncoding), Mockito.any(ProductsDetailsContext.class));
    }

    /**
     * Test custom product export for null query.
     *
     */
    @Test(expected = AssertionError.class)
    public void testCustomExportProductsForNullQuery()
    {
        populateForCustomQuery();
        productExportConfig.setQuery(null);
        defaultProductExportFacade.exportProducts(productExportConfig);
    }

    /**
     * Test custom product export for null query
     *
     */
    @Test(expected = AssertionError.class)
    public void testCustomExportProductsForFilePathQuery()
    {
        populateForCustomQuery();
        productExportConfig.setFileExportPath(null);
        defaultProductExportFacade.exportProducts(productExportConfig);
    }

    /**
     * Test custom product export for null query
     *
     */
    @Test(expected = AssertionError.class)
    public void testCustomExportProductsForFileExtQuery()
    {
        populateForCustomQuery();
        productExportConfig.setFileNameExt(null);
        defaultProductExportFacade.exportProducts(productExportConfig);
    }

    /**
     * Populate config object with values for testing default queries
     */
    private void populateForDefaultQuery()
    {
        productExportConfig.setIsQueryEnabled(Boolean.FALSE);
        productExportConfig.setProductExportStrategy(ProductExportType.VELOCITY);
        productExportConfig.setApprovalStatuses(productApprovalStatuses);
        productExportConfig.setCatalogVersion(catalogVersion);
        productExportConfig.setQuery(null);
        productExportConfig.setQueryParams(null);
    }

    /**
     * Populate config object with values for testing custom queries
     */
    private void populateForCustomQuery()
    {
        productExportConfig.setIsQueryEnabled(Boolean.TRUE);
        productExportConfig.setProductExportStrategy(ProductExportType.VELOCITY);
        productExportConfig.setApprovalStatuses(null);
        productExportConfig.setCatalogVersion(null);
        productExportConfig.setQuery(customQuery);
        productExportConfig.setQueryParams(customQueryParams);
    }
}
