/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.productexportservices.product.converters.populator;

import de.hybris.platform.basecommerce.model.externaltax.ProductTaxCodeModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.externaltax.ProductTaxCodeService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;


/**
 * Populates Map<AreaCode,TaxCode> for the product.
 */
public class ProductAreaTaxCodePopulator implements Populator<ProductModel, ProductData>
{
    /** productTaxCodeService bean. */
	private ProductTaxCodeService productTaxCodeService;

	@Override
	public void populate(final ProductModel product, final ProductData productData) throws ConversionException
	{
		if (isVariantProduct(product))
		{
			productData.setAreaTaxCode(getTaxCodeForVariantProduct((VariantProductModel) product));
		}
		else
		{
			productData.setAreaTaxCode(getTaxCodeForBaseProduct(product.getCode()));
		}
	}


	/**Getting tax code for base product.
	 * @param productCode - base product.
	 * @return - area tax code for the base product.
	 */
	protected Map<String, String> getTaxCodeForBaseProduct(final String productCode)
	{
		final Collection<ProductTaxCodeModel> productTaxCodeModels = getProductTaxCodeService().getTaxCodesForProduct(productCode);
		if (CollectionUtils.isEmpty(productTaxCodeModels))
		{
			return null;
		}
		final Map<String, String> areaTaxCode = new HashMap<String, String>();
		for (final ProductTaxCodeModel productTaxCodeModel : productTaxCodeModels)
		{
			areaTaxCode.put(productTaxCodeModel.getTaxArea(), productTaxCodeModel.getTaxCode());
		}
		return areaTaxCode;
	}


	/**Getting tax code for variant products.
	 * @param variantProduct - given variant product to get tax for.
	 * @return - area tax code for the entered product.
	 */
	protected Map<String, String> getTaxCodeForVariantProduct(final VariantProductModel variantProduct)
	{
		final Collection<String> productCodes = getProductHierarchyCodes(variantProduct);
		for (final String productCode : productCodes)
		{
			final Map<String, String> areaTaxCode = getTaxCodeForBaseProduct(productCode);
			if (MapUtils.isNotEmpty(areaTaxCode))
			{
				return areaTaxCode;
			}
		}
		return null;
	}

	/** Getting product hierarchy codes.
	 * @param variantProduct - entered variant product for which we want hierarchy.
	 * @return - returns hierarchy product list.
	 */
	protected Collection<String> getProductHierarchyCodes(final VariantProductModel variantProduct)
	{
		final Collection<String> productCodeList = new ArrayList<String>();
		productCodeList.add(variantProduct.getCode());
		ProductModel baseProduct = variantProduct.getBaseProduct();
		while (baseProduct != null)
		{
			productCodeList.add(baseProduct.getCode());
			if (baseProduct instanceof VariantProductModel)
			{
				baseProduct = ((VariantProductModel) baseProduct).getBaseProduct();
			}
			else
			{
				baseProduct = null;
			}
		}
		return productCodeList;
	}

	/**
	 * Checking if entered product is variant product or not.
	 * @param product - variant product.
	 * @return - true or false if entered products variant or not.
	 */
	protected boolean isVariantProduct(final ProductModel product)
	{
		return (product instanceof VariantProductModel);
	}

	protected ProductTaxCodeService getProductTaxCodeService()
	{
		return productTaxCodeService;
	}

	public void setProductTaxCodeService(final ProductTaxCodeService productTaxCodeService)
	{
		this.productTaxCodeService = productTaxCodeService;
	}
}
