/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.productexportservices.service.query.impl;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Required;

import com.capgemini.productexportservices.service.query.ProductExporterDataDao;


/**
 * DefaultProductExporterDataDao class to perform Product search service.
 *
 * @author lyonscg
 *
 */
public class DefaultProductExporterDataDao implements ProductExporterDataDao
{
    /**
     * LOGGER for DefaultProductExportFacade.
     */
    private static final Logger LOG = LogManager.getLogger(DefaultProductExporterDataDao.class);
    /**
     * Default query.
     */
    private static final String REF_QUERY_DEFAULT = "SELECT {pk} FROM {Product} WHERE {catalogVersion} = ?catalogVersion AND {approvalStatus} IN (?approvalStatuses)";
    /**
     * Default query with dates.
     */
    private static final String REF_QUERY_DEFAULT_WITH_DATES = "SELECT {pk} FROM {Product} WHERE {catalogVersion} = ?catalogVersion AND {approvalStatus} IN (?approvalStatuses) AND {onlineDate}>=?productOnlineDate AND {offlineDate}<=?productOfflineDate";

    private FlexibleSearchService flexibleSearchService;

    /**
     * Get Products based on ExportConfig or Params.
     *
     * @param isQueryEnabled
     *            boolean: query has been passed by cronjob
     * @param query
     *            query passed by cronjob
     * @param params
     *            passed by cronjob
     * @param catalogVersion
     *            catalog version for product search
     * @param productStatuses
     *            valid product statuses for query
     * @param productOnlineDate
     *            product catalog online date
     * @param productOfflineDate
     *            product catalog offline date
     * @return List of product objects that match criteria
     */
    public List<ProductModel> getProducts(final Boolean isQueryEnabled, final String query, final Map<String, String> params,
            final CatalogVersionModel catalogVersion, final List<ArticleApprovalStatus> productStatuses,
            final Date productOnlineDate, final Date productOfflineDate)
    {
        SearchResult<ProductModel> results = null;
        if (BooleanUtils.isTrue(isQueryEnabled))
        {
            Assert.assertNotNull("Query cannot be null", query);
            LOG.info("Querying for products using input query....");
            results = getFlexibleSearchService().search(query, params);
        }
        else
        {
            Assert.assertNotNull("CatalogVersion cannot be null", catalogVersion);
            Assert.assertFalse("productStatuses cannot be Empty", CollectionUtils.isEmpty(productStatuses));
            final Map<String, Object> queryParams = new HashMap<String, Object>(2);
            queryParams.put("catalogVersion", catalogVersion);
            queryParams.put("approvalStatuses", productStatuses);
            if (null == productOnlineDate || null == productOfflineDate)
            {
                LOG.info("Querying for products using default query....");
                results = getFlexibleSearchService().search(REF_QUERY_DEFAULT, queryParams);
            }
            else
            {
                queryParams.put("productOnlineDate", productOnlineDate);
                queryParams.put("productOfflineDate", productOfflineDate);
                LOG.info("Querying for products using default query with dates....");
                results = getFlexibleSearchService().search(REF_QUERY_DEFAULT_WITH_DATES, queryParams);
            }
        }
        return results.getResult();
    }

    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearch)
    {
        this.flexibleSearchService = flexibleSearch;
    }

    public FlexibleSearchService getFlexibleSearchService()
    {
        return this.flexibleSearchService;
    }

}
