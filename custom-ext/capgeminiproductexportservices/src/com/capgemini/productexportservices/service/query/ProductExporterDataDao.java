/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.productexportservices.service.query;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;


/**
 * Interface to product search service.
 *
 * @author lyonscg
 *
 */
public interface ProductExporterDataDao
{
    /**
     * Get Products based on ExportConfig or Params.
     *
     * @param isQueryEnabled
     *            boolean: query has been passed by cronjob
     * @param query
     *            query passed by cronjob
     * @param params
     *            passed by cronjob
     * @param catalogVersion
     *            catalog version for product search
     * @param productStatuses
     *            valid product statuses for query
     * @param productOnlineDate
     *            product catalog online date
     * @param productOfflineDate
     *            product catalog offline date
     * @return List of product objects that match criteria
     */
    public List<ProductModel> getProducts(Boolean isQueryEnabled, String query, Map<String, String> params,
            CatalogVersionModel catalogVersion, List<ArticleApprovalStatus> productStatuses, Date productOnlineDate,
            Date productOfflineDate);

    /**
     * FlexibleSearchService setter used by junit tests.
     *
     * @param flexibleSearch
     */
    @Required
    public void setFlexibleSearchService(FlexibleSearchService flexibleSearch);

    /**
     * FlexibleSearchService getter used by junit tests.
     *
     * @return FlexibleSearchService
     */
    public FlexibleSearchService getFlexibleSearchService();
}
