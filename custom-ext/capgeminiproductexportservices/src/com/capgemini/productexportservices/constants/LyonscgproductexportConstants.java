package com.capgemini.productexportservices.constants;

/**
 * Global class for all LyonscgproductexportConstants constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings({"deprecation","PMD","squid:CallToDeprecatedMethod"})
public class LyonscgproductexportConstants extends GeneratedLyonscgproductexportConstants
{
	public static final String EXTENSIONNAME = "capgeminiproductexportservices";
	
	private LyonscgproductexportConstants()
	{
		//empty
	}
}
