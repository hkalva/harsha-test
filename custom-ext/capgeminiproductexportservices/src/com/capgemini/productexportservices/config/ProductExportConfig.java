/*
 * [y] hybris Platform
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.productexportservices.config;

import java.util.Date;
import java.util.List;

import com.capgemini.productexportservices.enums.ProductExportType;
import com.capgemini.services.AbstractExportConfig;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;


/**
 * Product export configuration.
 */
public class ProductExportConfig extends AbstractExportConfig
{
    /**
     * CatalogVersion parameter when using default query.
     */
    private CatalogVersionModel catalogVersion;
    

    /**
     * Product ApprovalStatus parameter when using default query.
     */
    private List<ArticleApprovalStatus> approvalStatuses;
 
    /**
     * Product Export Strategy for exporting orders.
     */
    private ProductExportType productExportStrategy;
    /**
     * Single or multifile. true for single file.
     */
    private boolean isSingleFile;
    /**
     * Product online date.
     */
    private Date productOnlineDate;
    /**
     * Product offline date.
     */
    private Date productOfflineDate;
    /**
     * Comma separated ProductOption.
     */
    private String productOptions;
    /**
     * Tax area country region.
     */
    private String taxCountryIso;

    
    public void setCatalogVersion(final CatalogVersionModel catalogVersion)
    {
        this.catalogVersion = catalogVersion;
    }

    public CatalogVersionModel getCatalogVersion()
    {
        return catalogVersion;
    }
    
    public ProductExportType getProductExportStrategy()
    {
        return productExportStrategy;
    }

    public void setProductExportStrategy(final ProductExportType productExportStrategy)
    {
        this.productExportStrategy = productExportStrategy;
    }

    public List<ArticleApprovalStatus> getApprovalStatuses()
    {
        return approvalStatuses;
    }

    public void setApprovalStatuses(final List<ArticleApprovalStatus> approvalStatuses)
    {
        this.approvalStatuses = approvalStatuses;
    }

    public boolean getIsSingleFile()
    {
        return isSingleFile;
    }

    public void setIsSingleFile(final boolean isSingleFile)
    {
        this.isSingleFile = isSingleFile;
    }

    public Date getProductOnlineDate()
    {
        return null == productOnlineDate ? null : (Date) productOnlineDate.clone();
    }

    public void setProductOnlineDate(final Date productOnlineDate)
    {
        this.productOnlineDate = productOnlineDate == null ? null : (Date) productOnlineDate.clone();
    }

    public Date getProductOfflineDate()
    {
        return null == productOfflineDate ? null : (Date) productOfflineDate.clone();
    }

    public void setProductOfflineDate(final Date productOfflineDate)
    {
        this.productOfflineDate = productOfflineDate == null ? null : (Date) productOfflineDate.clone();
    }

    public String getProductOptions()
    {
        return productOptions;
    }

    public void setProductOptions(final String productOptions)
    {
        this.productOptions = productOptions;
    }

    public String getTaxCountryIso()
    {
        return taxCountryIso;
    }

    public void setTaxCountryIso(final String taxCountryIso)
    {
        this.taxCountryIso = taxCountryIso;
    }

}
