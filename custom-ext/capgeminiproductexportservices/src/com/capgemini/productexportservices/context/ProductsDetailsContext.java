/*
 * [y] hybris Platform
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.productexportservices.context;

import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.Collection;

import org.apache.velocity.VelocityContext;


/**
 * Wrapper for {@link ProductData} to assist jaxb xml marshalling to a single file.
 */
public class ProductsDetailsContext extends VelocityContext
{
    /** Collection of product data. */
	private Collection<ProductData> productDatas;

	/**
     * @return the productDatas
     */
	public Collection<ProductData> getProductDatas()
	{
		return productDatas;
	}

	 /**
     * @param productDatas
     *            the productDatas to set
     */
	public void setProductDatas(final Collection<ProductData> productDatas)
	{
		this.productDatas = productDatas;
	}

}
