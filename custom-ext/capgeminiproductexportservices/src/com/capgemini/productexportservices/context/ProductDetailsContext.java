/*
 * [y] hybris Platform
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.productexportservices.context;

import de.hybris.platform.commercefacades.product.data.ProductData;

import org.apache.velocity.VelocityContext;


/**
 * Wrapper for {@link ProductData} to assist jaxb xml marshalling.
 */
public class ProductDetailsContext extends VelocityContext
{
    /** product data. */
	private ProductData productData;

	/**
     * @return the productData
     */
	public ProductData getProductData()
	{
		return productData;
	}

	 /**
     * @param productData
     *            the productData to set
     */
	public void setProductData(final ProductData productData)
	{
		this.productData = productData;
	}
}
