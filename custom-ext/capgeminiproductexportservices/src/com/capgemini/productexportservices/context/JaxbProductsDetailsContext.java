/*
 * [y] hybris Platform
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.productexportservices.context;

/**
 * Wrapper for {@link ProductsDetailsContext} to assist jaxb xml marshalling.
 */
public class JaxbProductsDetailsContext
{
    /** productsDetailsContext bean. */
	private ProductsDetailsContext productsDetailsContext;

	/**
     * @return the productsDetailsContext
     */
	public ProductsDetailsContext getProductsDetailsContext()
	{
		return productsDetailsContext;
	}

	 /**
     * @param productsDetailsContext
     *            the productsDetailsContext to set
     */
	public void setProductsDetailsContext(final ProductsDetailsContext productsDetailsContext)
	{
		this.productsDetailsContext = productsDetailsContext;
	}
}
