/*
 * [y] hybris Platform
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.productexportservices.cronjob;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.apache.commons.lang.BooleanUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Required;

import com.capgemini.productexportservices.config.ProductExportConfig;
import com.capgemini.productexportservices.facade.ProductDataExporterFacade;
import com.capgemini.productexportservices.model.ProductExportCronJobModel;


/** Implements product export cronjob abstract class.
 * @spring.bean productExportCronJob
 */
public class ProductExportCronJobPerformable extends AbstractJobPerformable<ProductExportCronJobModel>
{
	/**
	 * LOGGER for ProductExportCronJobPerformable.
	 */
	private static final Logger LOG = LogManager.getLogger(ProductExportCronJobPerformable.class.getName());
	/**
	 * ProductExportFacade for product export.
	 */
	private ProductDataExporterFacade productDataExporter;

	@Override
	public PerformResult perform(final ProductExportCronJobModel cronJob)
	{
		LOG.info("Starting ProductExportCronJob...");
		final ProductExportConfig exportConfig = new ProductExportConfig();
		exportConfig.setIsQueryEnabled(cronJob.getQueryEnabled());
		exportConfig.setQuery(cronJob.getQuery());
		exportConfig.setQueryParams(cronJob.getQuerySearchParameter());
		exportConfig.setCatalogVersion(cronJob.getCatalogVersion());
		exportConfig.setApprovalStatuses(cronJob.getApprovalStatuses());
		exportConfig.setRendererTemplate(cronJob.getRendererTemplate());
		exportConfig.setFileExportPath(cronJob.getExportFilePath());
		exportConfig.setFileNameExt(cronJob.getFileNameExt());
		exportConfig.setFileEncoding(cronJob.getFileEncoding());
		exportConfig.setProductExportStrategy(cronJob.getExportStrategy());
		exportConfig.setIsSingleFile(BooleanUtils.isTrue(cronJob.getSingleFile()));
		exportConfig.setProductOnlineDate(cronJob.getProductOnlineDate());
		exportConfig.setProductOfflineDate(cronJob.getProductOfflineDate());
		exportConfig.setProductOptions(cronJob.getProductOptions());
		final CountryModel taxCountry = cronJob.getTaxCountry();
		if (null != taxCountry)
		{
			exportConfig.setTaxCountryIso(taxCountry.getIsocode());
		}

		if (getProductDataExporter().exportProducts(exportConfig))
		{
			LOG.info("ProductExportCronJob completed successfully...");
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		LOG.info("ProductExportCronJob completed unsuccessfully...");
		return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
	}
	/**
     * @return the productDataExporter
     */
	protected ProductDataExporterFacade getProductDataExporter()
	{
		return productDataExporter;
	}
    /**
    * @param productDataExporter
    *            the productDataExporter to set
    */
	@Required
	public void setProductDataExporter(final ProductDataExporterFacade productDataExporter)
	{
		this.productDataExporter = productDataExporter;
	}
}
