/**
 *
 */
package com.capgemini.productexportservices.writer;

import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import net.sf.saxon.s9api.SaxonApiException;

import java.io.File;
import java.io.IOException;

import org.apache.velocity.VelocityContext;


/**@author lyonscg.
 *product detail file writer interface.
 */
public interface ProductDetailsFileWriter
{
	boolean writeProductToFile(final RendererTemplateModel template, final File file, final String fileEncoding,
			final VelocityContext productDetailsContext) throws IOException, SaxonApiException;
}
