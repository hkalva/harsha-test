/**
 * Copyright (c) 2019 Capgemini. All rights reserved.
 *
 * This software is the confidential and proprietary information of Capgemini
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Capgemini.
 */
package com.capgemini.productexportservices.writer.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import javax.annotation.Resource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamWriter;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.exception.VelocityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.productexportservices.context.JaxbProductsDetailsContext;
import com.capgemini.productexportservices.context.ProductDetailsContext;
import com.capgemini.productexportservices.context.ProductsDetailsContext;
import com.capgemini.productexportservices.writer.ProductDetailsFileWriter;
import com.capgemini.services.util.CharsetUtil;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import net.sf.saxon.Configuration;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.Serializer.Property;


/**
 * @author lyonscg. implementation of JAXB product details file writer/
 */
public class JAXBProductDetailsFileWriterImpl implements ProductDetailsFileWriter
{
    private static final Logger LOG = LoggerFactory.getLogger(JAXBProductDetailsFileWriterImpl.class.getName());

    @Resource
    private ConfigurationService configurationService;

    @Override
    public boolean writeProductToFile(final RendererTemplateModel template, final File file, final String fileEncoding,
            final VelocityContext context) throws IOException
    {
        boolean succeed = false;
        try
        {
            if (context instanceof ProductDetailsContext)
            {
                succeed = jaxbWriteProductToFile(file, context);
            }
            else if (context instanceof ProductsDetailsContext)
            {
                succeed = jaxbWriteProductsToFile(file, context);
            }
            else
            {
                throw new VelocityException("Invalid Context");
            }
        }
        catch (final JAXBException | VelocityException | SaxonApiException e)
        {
            LOG.error("Exception writing product details to file", e);
        }
        return succeed;
    }

    protected boolean jaxbWriteProductsToFile(final File file, final VelocityContext context)
            throws JAXBException, IOException, SaxonApiException
    {
        final JaxbProductsDetailsContext jaxbProductsDetailsContext = new JaxbProductsDetailsContext();
        if (!(context instanceof ProductsDetailsContext))
        {
            LOG.error("Context is not instance of productsdetailscontext, cannot cast to ProductsDetailsContext");
            return false;
        }
        jaxbProductsDetailsContext.setProductsDetailsContext((ProductsDetailsContext) context);
        final Marshaller marshaller = JAXBContext.newInstance(ProductsDetailsContext.class).createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        final JAXBElement<ProductsDetailsContext> rootElement = new JAXBElement<>(
                new QName("ProductsDetails"), ProductsDetailsContext.class,
                jaxbProductsDetailsContext.getProductsDetailsContext());
        writeToFile(file,marshaller,rootElement);
        return true;
    }

    protected boolean jaxbWriteProductToFile(final File file, final VelocityContext context)
            throws JAXBException, IOException, SaxonApiException
    {
        final Marshaller marshaller = JAXBContext.newInstance(ProductData.class).createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        if (!(context instanceof ProductDetailsContext))
        {
            LOG.error("Context is not instance of productdetailscontext, cannot cast to ProductDetailsContext");
            return false;
        }
        final JAXBElement<ProductData> rootElement = new JAXBElement<>(new QName("ProductDetails"), ProductData.class,
                ((ProductDetailsContext) context).getProductData());
        writeToFile(file,marshaller,rootElement);
        return true;
    }
  /**
   * 
   * @param file
   * @param marshaller
   * @param rootElement
   * @throws JAXBException
   * @throws IOException
   * @throws SaxonApiException
   */
    private void writeToFile(File file, Marshaller marshaller, JAXBElement<?> rootElement) throws JAXBException,
            IOException, SaxonApiException
    {
        Serializer serializer = null;
        LOG.debug("Writing to file {}", file.getAbsolutePath());
        XMLStreamWriter writer = null;
        
        try (FileOutputStream fos = new FileOutputStream(file);
                OutputStreamWriter out = new OutputStreamWriter(fos, CharsetUtil.getCharset());
                )
        {
            Processor processor = new Processor(Configuration.newConfiguration());
            serializer = processor.newSerializer();
            serializer.setOutputProperty(Property.METHOD, "xml");
            serializer.setOutputProperty(Property.INDENT, "yes");
            serializer.setOutputWriter(out);
            writer = serializer.getXMLStreamWriter();
            marshaller.marshal(rootElement, writer);
        } 
        catch (SaxonApiException | IOException | JAXBException e)
        {
            LOG.error(e.getMessage(), e);
            throw e;
        }
        finally
        {
            if (serializer != null)
            {
                try
                {
                    serializer.close();
                }
                catch (SaxonApiException e)
                {
                    LOG.warn("Error closing serializer", e);
                }
            }
        }
    }
}
