/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.productexportservices.writer.impl;

import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.commons.renderer.RendererService;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.velocity.VelocityContext;
import org.junit.Assert;

import com.capgemini.productexportservices.writer.ProductDetailsFileWriter;


/**
 * Velocity template based ProductDetailsFileWriter implementation.
 */
public class VelocityProductDetailsFileWriterImpl implements ProductDetailsFileWriter
{
	private static final Logger LOG = LogManager.getLogger(VelocityProductDetailsFileWriterImpl.class.getName());
	private RendererService rendererService;

	@Override
	public boolean writeProductToFile(final RendererTemplateModel template, final File file, final String fileEncoding,
			final VelocityContext context) throws IOException
	{
		Assert.assertNotNull("Template cannot be null", template);
		Assert.assertNotNull("File cannot be null", file);
		Assert.assertNotNull("Context cannot be null", context);
		Assert.assertTrue("File Encoding cannot be empty", StringUtils.isNotBlank(fileEncoding));
		return velocityRenderTemplateToFile(template, file, fileEncoding, context);
	}

	protected boolean velocityRenderTemplateToFile(final RendererTemplateModel template, final File file,
			final String fileEncoding, final VelocityContext context) throws IOException
	{
		final StringWriter productDetailsString = new StringWriter();
		getRendererService().render(template, context, productDetailsString);
		LOG.debug("Writing to file " + file.getAbsolutePath());
		FileUtils.writeStringToFile(file, productDetailsString.getBuffer().toString(), fileEncoding);
		return true;
	}

	public RendererService getRendererService()
	{
		return rendererService;
	}

	public void setRendererService(final RendererService rendererService)
	{
		this.rendererService = rendererService;
	}

}
