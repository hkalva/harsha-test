/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.productexportservices.facade;

import com.capgemini.productexportservices.config.ProductExportConfig;


/**
 * Exports product data.
 */
public interface ProductDataExporterFacade
{
	/**
	 * Export Products based on ProductExportConfig.
	 *
	 * @param exportConfig
	 *           Product export config.
	 * @return returns true if successful.
	 */
	boolean exportProducts(ProductExportConfig exportConfig);
}
