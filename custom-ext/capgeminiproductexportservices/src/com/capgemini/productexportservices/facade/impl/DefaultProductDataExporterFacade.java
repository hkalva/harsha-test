/**
 * Copyright (c) 2019 Capgemini. All rights reserved.
 *
 * This software is the confidential and proprietary information of Capgemini
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Capgemini.
 * 
 */
package com.capgemini.productexportservices.facade.impl;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.capgemini.productexportservices.config.ProductExportConfig;
import com.capgemini.productexportservices.context.ProductDetailsContext;
import com.capgemini.productexportservices.context.ProductsDetailsContext;
import com.capgemini.productexportservices.enums.ProductExportType;
import com.capgemini.productexportservices.facade.ProductDataExporterFacade;
import com.capgemini.productexportservices.service.query.ProductExporterDataDao;
import com.capgemini.productexportservices.writer.ProductDetailsFileWriter;

import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.ConfigurablePopulator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import net.sf.saxon.s9api.SaxonApiException;


/**
 * Default implementation of {@link ProductDataExporterFacade}.
 */
public class DefaultProductDataExporterFacade implements ProductDataExporterFacade
{
    /**
     * LOGGER for DefaultProductDataExporterFacade.
     */
    private static final Logger LOG = LoggerFactory.getLogger(DefaultProductDataExporterFacade.class);

    /**
     * ProductModel to ProductData converter.
     */
    private Converter<ProductModel, ProductData> productConverter;

    /**
     * Map of different ProductDetailsFileWriter implementations.
     */
    private Map<ProductExportType, ProductDetailsFileWriter> productDetailsFileWriterMap;

    /**
     * Product export file name date format.
     */
    private String dateFormat;

    /**
     * Date time formatter for product export file names.
     */
    private DateTimeFormatter dateTimeFormatter;

    /**
     * List of product options to populate.
     */
    private List<ProductOption> productOptions;

    /**
     * Product options to populator.
     */
    private ConfigurablePopulator<ProductModel, ProductData, ProductOption> productExportConfiguredPopulator;

    /**
     * ConfigurationService bean.
     */
    private ConfigurationService configurationService;

    /**
     * Fall back tax code property config.
     */
    private String fallbackTaxCodeConfig;

    /**
     * Product search service
     */
    private ProductExporterDataDao productExporterDataDao;

    /**
     * Export products to a file based on options in exportConfig object
     * 
     * @param exportConfig
     *            Object containing all product query options
     */
    @Override
    public boolean exportProducts(final ProductExportConfig exportConfig)
    {
        Assert.assertNotNull("ProductExportConfig cannot be null", exportConfig);

        final List<ProductModel> products = getProductExporterDataDao().getProducts(exportConfig.getIsQueryEnabled(),
                exportConfig.getQuery(), exportConfig.getQueryParams(), exportConfig.getCatalogVersion(),
                exportConfig.getApprovalStatuses(), exportConfig.getProductOnlineDate(), exportConfig.getProductOfflineDate());
        if (CollectionUtils.isEmpty(products))
        {
            LOG.warn("No Products to export");
            return false;
        }

        boolean isSuccessful = true;
        Assert.assertNotNull("FilePath cannot be null", exportConfig.getFileExportPath());
        Assert.assertNotNull("FileExt cannot be null", exportConfig.getFileNameExt());
        final ProductDetailsFileWriter productDetailsFileWriter = getProductDetailsFileWriterMap()
                .get(exportConfig.getProductExportStrategy());
        Assert.assertNotNull("No corresponding ProductDetailsFileWriter implementations found", productDetailsFileWriter);
        final MessageFormat fileNameFormat = new MessageFormat(
                generateFileNamePattern("{0}", exportConfig.getFileExportPath(), exportConfig.getFileNameExt()));
        ProductsDetailsContext productsDetailsContext = null;
        final List<String> errorList = new ArrayList<String>();
        errorList.add("Failed product codes");
        final String defaultTaxCode = getConfigurationService().getConfiguration().getString(getFallbackTaxCodeConfig());
        final List<ProductOption> productOptionsFromCronjob = populateProductOptions(exportConfig.getProductOptions());

        for (final ProductModel product : products)
        {
            final String productCode = product.getCode();
            
            try
            {
                final ProductData productData = getProductConverter().convert(product);
                final List<ProductOption> productOptionsToPopulate = CollectionUtils.isEmpty(productOptionsFromCronjob)
                        ? getProductOptions()
                        : productOptionsFromCronjob;
                getProductExportConfiguredPopulator().populate(product, productData, productOptionsToPopulate);
                verifyAndpopulateTaxCodeForCountry(productOptionsToPopulate, productData, exportConfig.getTaxCountryIso(),
                        defaultTaxCode);
                Map<Boolean, ProductsDetailsContext> objectMap = getProductsDetailsContext(isSuccessful, productData,
                        exportConfig, productsDetailsContext, productDetailsFileWriter, fileNameFormat);

                for (Entry<Boolean, ProductsDetailsContext> mapEntry : objectMap.entrySet())
                {
                    isSuccessful = mapEntry.getKey();
                    productsDetailsContext = mapEntry.getValue();
                }
            }
            catch (final IOException | SaxonApiException e)
            {
                isSuccessful = false;
                errorList.add(productCode);
                LOG.error("Error writing product {} to file...Proceeding to next product...", productCode, e);
            }
        }
        
        if (null != productsDetailsContext)
        {
            isSuccessful = writeProductsToSingleFile(exportConfig, isSuccessful, productDetailsFileWriter, fileNameFormat,
                    productsDetailsContext);
        }
        
        logFailedProducts(exportConfig, errorList);
        return isSuccessful;
    }

    /**
     * 
     * @param productOptionsToPopulate
     * @param productData
     * @param taxCountryIso
     * @param defaultTaxCode
     * @param exportConfig
     */
    private void verifyAndpopulateTaxCodeForCountry(List<ProductOption> productOptionsToPopulate, ProductData productData,
            String taxCountryIso, String defaultTaxCode)
    {
        if (productOptionsToPopulate.contains(ProductOption.TAX_CODE))
        {
            populateTaxCodeForCountry(productData, taxCountryIso, defaultTaxCode);
        }
    }

    /**
     * 
     * @param isSuccessful
     * @param productData
     * @param exportConfig
     * @param productsDetailsContext
     * @param fileNameFormat
     * @param productDetailsFileWriter
     * @throws IOException
     * @throws SaxonApiException
     */
    private Map<Boolean, ProductsDetailsContext> getProductsDetailsContext(boolean isSuccessful, ProductData productData,
            ProductExportConfig exportConfig, ProductsDetailsContext productsDetailsContext,
            ProductDetailsFileWriter productDetailsFileWriter, MessageFormat fileNameFormat) throws IOException, SaxonApiException
    {
        Map<Boolean, ProductsDetailsContext> objectMap = new HashedMap<Boolean, ProductsDetailsContext>();
        if (null == productData)
        {
            isSuccessful = false;
            LOG.error("ProductData is null. Unable to write product...");
        }
        if (exportConfig.getIsSingleFile())
        {
            if (null == productsDetailsContext)
            {
                productsDetailsContext = createProductDetailsContext();
            }
            productsDetailsContext.getProductDatas().add(productData);
        }
        else
        {
            isSuccessful = writeProductToFile(exportConfig, isSuccessful, productDetailsFileWriter, fileNameFormat, productData);
        }
        objectMap.put(isSuccessful, productsDetailsContext);
        return objectMap;
    }

    /**
     * Set tax code for country
     * 
     * @param productData
     * @param taxCountryIso
     * @param defaultTaxCode
     */
    protected void populateTaxCodeForCountry(final ProductData productData, final String taxCountryIso,
            final String defaultTaxCode)
    {
        if (null == taxCountryIso)
        {
            return;
        }
        final Map<String, String> areaTaxCodeMap = productData.getAreaTaxCode();
        if (null == areaTaxCodeMap)
        {
            productData.setTaxCode(defaultTaxCode);
            return;
        }
        final String taxCode = productData.getAreaTaxCode().get(taxCountryIso);
        productData.setTaxCode(null == taxCode ? defaultTaxCode : taxCode);
    }

    /**
     * Create list of product options
     * 
     * @param productOptions
     * @return list of product options
     */
    protected List<ProductOption> populateProductOptions(final String productOptions)
    {
        List<ProductOption> productOptionsList = null;
        if (StringUtils.isBlank(productOptions))
        {
            return productOptionsList;
        }
        final String[] productOptionsArray = productOptions.split(",");
        productOptionsList = new ArrayList<ProductOption>(productOptionsArray.length);
        for (final String productOptionString : productOptionsArray)
        {
            if (StringUtils.isNotBlank(productOptionString))
            {
                productOptionsList.add(ProductOption.valueOf(productOptionString));
            }
        }
        return productOptionsList;
    }

    /**
     * Write failed product data to error file
     * 
     * @param exportConfig
     * @param errorList
     */
    protected void logFailedProducts(final ProductExportConfig exportConfig, final List<String> errorList)
    {
        if (errorList.size() > 1)
        {
            try
            {
                FileUtils.writeLines(new File(generateFileNamePattern("errorList", exportConfig.getFileExportPath(), ".txt")),
                        errorList);
            }
            catch (final IOException e)
            {
                LOG.error("Unable to write failed product files", e);
            }
        }
    }

    /**
     * Create product details context
     * 
     * @return productsDetailsContext
     */
    protected ProductsDetailsContext createProductDetailsContext()
    {
        final ProductsDetailsContext productsDetailsContext = new ProductsDetailsContext();
        productsDetailsContext.setProductDatas(new ArrayList<ProductData>());
        return productsDetailsContext;
    }

    /**
     * Write one product to a file
     * 
     * @param exportConfig
     * @param isSuccessful
     * @param productDetailsFileWriter
     * @param fileNameFormat
     * @param productData
     * @return success or failure boolean
     * @throws IOException
     * @throws SaxonApiException
     */
    protected boolean writeProductToFile(final ProductExportConfig exportConfig, boolean isSuccessful,
            final ProductDetailsFileWriter productDetailsFileWriter, final MessageFormat fileNameFormat,
            final ProductData productData) throws IOException, SaxonApiException
    {
        final ProductDetailsContext productDetailsContext = new ProductDetailsContext();
        productDetailsContext.setProductData(productData);
        final boolean writeToFileSuccess = productDetailsFileWriter.writeProductToFile(exportConfig.getRendererTemplate(),
                new File(fileNameFormat.format(new Object[]
                { productData.getCode() })), exportConfig.getFileEncoding(), productDetailsContext);
        isSuccessful = isSuccessful && writeToFileSuccess;
        return isSuccessful;
    }

    /**
     * Write product data to single file
     * 
     * @param exportConfig
     * @param isSuccessful
     * @param productDetailsFileWriter
     * @param fileNameFormat
     * @param productsDetailsContext
     * @return success or failure boolean
     * @throws SaxonApiException
     */
    protected boolean writeProductsToSingleFile(final ProductExportConfig exportConfig, boolean isSuccessful,
            final ProductDetailsFileWriter productDetailsFileWriter, final MessageFormat fileNameFormat,
            final ProductsDetailsContext productsDetailsContext)
    {
        try
        {
            final boolean writeToFileSuccess = productDetailsFileWriter.writeProductToFile(exportConfig.getRendererTemplate(),
                    new File(fileNameFormat.format(new Object[]
                    { "productsDetails" })), exportConfig.getFileEncoding(), productsDetailsContext);
            isSuccessful = isSuccessful && writeToFileSuccess;
        }
        catch (final IOException | SaxonApiException e)
        {
            isSuccessful = false;
            LOG.error("Error writing product to file...", e);
        }
        return isSuccessful;
    }

    /**
     * Generate file name pattern for string + datetime + extension
     * 
     * @param productNumber
     * @param filePath
     * @param fileExt
     * @return
     */
    protected String generateFileNamePattern(final String productNumber, final String filePath, final String fileExt)
    {
        final String fileName = filePath + productNumber;
        if (getDateTimeFormatter() != null)
        {
            return fileName + "_" + ZonedDateTime.now().format(getDateTimeFormatter()) + fileExt;
        }
        return fileName + fileExt;
    }

    /**
     * 
     */
    @PostConstruct
    private void initDateTimeFormatter()
    {
        final String dateFormatString = getDateFormat();
        if (StringUtils.isNotEmpty(dateFormatString))
        {
            setDateTimeFormatter(DateTimeFormatter.ofPattern(dateFormatString));
        }
    }

    /**
     * 
     * @param productDetailsFileWriterMap
     */
    @Required
    public void setProductDetailsFileWriterMap(final Map<ProductExportType, ProductDetailsFileWriter> productDetailsFileWriterMap)
    {
        this.productDetailsFileWriterMap = productDetailsFileWriterMap;
    }

    /**
     * 
     * @return
     */
    protected Converter<ProductModel, ProductData> getProductConverter()
    {
        return productConverter;
    }

    @Required
    public void setProductConverter(final Converter<ProductModel, ProductData> productConverter)
    {
        this.productConverter = productConverter;
    }

    /**
     * 
     * @return
     */
    protected String getDateFormat()
    {
        return dateFormat;
    }

    /**
     * 
     * @param dateFormat
     */
    public void setDateFormat(final String dateFormat)
    {
        this.dateFormat = dateFormat;
    }

    /**
     * 
     * @return
     */
    protected DateTimeFormatter getDateTimeFormatter()
    {
        return dateTimeFormatter;
    }

    /**
     * 
     * @param dateTimeFormatter
     */
    public void setDateTimeFormatter(final DateTimeFormatter dateTimeFormatter)
    {
        this.dateTimeFormatter = dateTimeFormatter;
    }

    /**
     * 
     * @return
     */
    protected Map<ProductExportType, ProductDetailsFileWriter> getProductDetailsFileWriterMap()
    {
        return productDetailsFileWriterMap;
    }

    /**
     * 
     * @return
     */
    protected List<ProductOption> getProductOptions()
    {
        return productOptions;
    }

    /**
     * 
     * @param productOptions
     */
    public void setProductOptions(final List<ProductOption> productOptions)
    {
        this.productOptions = productOptions;
    }

    /**
     * 
     * @return
     */
    protected ConfigurablePopulator<ProductModel, ProductData, ProductOption> getProductExportConfiguredPopulator()
    {
        return productExportConfiguredPopulator;
    }

    /**
     * 
     * @param productExportConfiguredPopulator
     */
    @Required
    public void setProductExportConfiguredPopulator(
            final ConfigurablePopulator<ProductModel, ProductData, ProductOption> productExportConfiguredPopulator)
    {
        this.productExportConfiguredPopulator = productExportConfiguredPopulator;
    }

    /**
     * 
     * @return
     */
    protected ConfigurationService getConfigurationService()
    {
        return configurationService;
    }

    /**
     * 
     * @param productExporterDataDao
     */
    @Required
    public void setProductExporterDataDao(final ProductExporterDataDao productExporterDataDao)
    {
        this.productExporterDataDao = productExporterDataDao;
    }

    /**
     * 
     * @return
     */
    public ProductExporterDataDao getProductExporterDataDao()
    {
        return productExporterDataDao;
    }

    /**
     * 
     * @param configurationService
     */
    @Required
    public void setConfigurationService(final ConfigurationService configurationService)
    {
        this.configurationService = configurationService;
    }

    /**
     * 
     * @return
     */
    protected String getFallbackTaxCodeConfig()
    {
        return fallbackTaxCodeConfig;
    }

    /**
     * 
     * @param fallbackTaxCodeConfig
     */
    @Required
    public void setFallbackTaxCodeConfig(final String fallbackTaxCodeConfig)
    {
        this.fallbackTaxCodeConfig = fallbackTaxCodeConfig;
    }

}
