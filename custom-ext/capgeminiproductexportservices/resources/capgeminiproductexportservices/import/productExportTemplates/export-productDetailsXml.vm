#macro ( variantOptionData $option $optionName)
#if ($option)
<${optionName}>
   <code>${option.code}</code>
   <url>${option.url}</url>
   #fullPriceBlock( $option.priceData "priceData" )
   #if (${option.stock})
   <stock>
      <stockLevelStatus>${option.stock.stockLevelStatus}</stockLevelStatus>
      <stockLevel>${option.stock.stockLevel}</stockLevel>
      #if(${option.stock.stockThreshold})
      <stockThreshold>${option.stock.stockThreshold}</stockThreshold>
      #end
   </stock>
   #end
</${optionName}>
#end
#end

#macro( fullPriceBlock $priceData $priceName)
#if ($priceData)
<${priceName}>
	<currencyIso>${priceData.currencyIso}</currencyIso>
	<formattedValue>${priceData.formattedValue}</formattedValue>
	<priceType>${priceData.priceType}</priceType>
	<value>${priceData.value}</value>
</${priceName}>
#end
#end

#macro( escapeSpecial $text )#if($text)$text.replace("&", "&amp;").replace("<","&lt;").replace(">","&gt;")#end#end


<ProductDetails>
  <code>${ctx.productData.code}</code>
  <name>#escapeSpecial(${ctx.productData.name})</name>
  <description>#escapeSpecial(${ctx.productData.description})</description>
  #if(${ctx.productData.baseProduct})
  <baseProduct>${ctx.productData.baseProduct}</baseProduct>
  #end
  <manufacturer>#escapeSpecial(${ctx.productData.manufacturer})</manufacturer>
  <purchasable>${ctx.productData.purchasable}</purchasable>
  <summary>#escapeSpecial(${ctx.productData.summary})</summary>
  <url>${ctx.productData.url}</url>
  <categories>
    #foreach( $category in ${ctx.productData.categories} )
    <category>
      <code>${category.code}</code>
      <name>#escapeSpecial(${category.name})</name>
      <sequence>${category.sequence}</sequence>
      <url>${category.url}</url>
    </category>
  #end
  </categories>
  #if(${ctx.productData.classifications})
  <classifications>
    #foreach( $classification in ${ctx.productData.classifications} )
    <classification>
      <code>${classification.code}</code>
      #if(${classification.features})
      <features>
        #foreach( $feature in ${classification.features} )
        <feature>
          <code>${feature.code}</code>
          <comparable>${feature.comparable}</comparable>
          #if(${feature.featureUnit})
          <featureUnit>
            <name>#escapeSpecial(${feature.featureUnit.name})</name>
            <symbol>${feature.featureUnit.symbol}</symbol>
            <unitType>${feature.featureUnit.unitType}</unitType>
          </featureUnit>
          #end
          <featureValues>
            #foreach( $feature in ${feature.featureValues} )
            <featureValue>
              <value>#escapeSpecial(${feature.value})</value>
            </featureValue>
            #end
          </featureValues>
          <name>#escapeSpecial(${feature.name})</name>
          <range>${feature.range}</range>      
        </feature>
        #end
      </features>
      #end
    </classification>
    #end
  </classifications>
  #end
  #if(${ctx.productData.images})
  <images>
    #foreach( $image in ${ctx.productData.images} )
    <image>
      <altText>#escapeSpecial(${image.altText})</altText>
      <format>${image.format}</format>
      <imageType>${image.imageType}</imageType>
      <url>${image.url}</url>
    </image>
    #end
  </images>
  #end
  <baseOptions>
    #foreach( $baseOption in ${ctx.productData.baseOptions} )
	<baseOption>
	   <variantType>${ctx.baseOption.variantType}</variantType>
	   <options>
	      #foreach( $option in ${ctx.baseOption.options} )
		     #variantOptionData( ${option} "option" )
		  #end
	   </options>
	   #variantOptionData( ${ctx.baseOption.selected} "selected" )
	</baseOption>
	#end
  </baseOptions>
  #fullPriceBlock( ${ctx.productData.price} "price" )
  <priceRange>
    #fullPriceBlock( ${ctx.productData.priceRange.maxPrice} "maxPrice" )
    #fullPriceBlock( ${ctx.productData.priceRange.minPrice} "minPrice" )	
  </priceRange>
  <potentialPromotions>
     #foreach( $potentialPromotion in ${ctx.productData.potentialPromotions} )
	 #if($potentialPromotion)
	    <potentialPromotion>
		   <code>${potentialPromotion.code}</code>
		   <title>${potentialPromotion.title}</title>
		   <promotionType>${potentialPromotion.promotionType}</promotionType>
		   <startDate>${potentialPromotion.startDate}</startDate>
		   <endDate>${potentialPromotion.endDate}</endDate>
		   <description>${potentialPromotion.description}</description>
		   <enabled>${potentialPromotion.enabled}</enabled>
		   <priority>${potentialPromotion.priority}</priority>
		   <promotionGroup>${potentialPromotion.promotionGroup}</promotionGroup>
		   <detailsUrl>${potentialPromotion.detailsUrl}</detailsUrl>
		   <restrictions>
		      #foreach( $restriction in ${potentialPromotion.restrictions} )
			  #if($restriction)
			     <restriction>
				    <restrictionType>${restriction.restrictionType}</restrictionType>
					<description>${restriction.description}</description>
				 </restriction>
			  #end
			  #end
		   </restrictions>
		</potentialPromotion>
	 #end
	 #end
  </potentialPromotions>
  <volumePrices>
     #foreach($volumePrice in ${ctx.productData.volumePrices})
     #fullPriceBlock( ${$volumePrice} "$volumePrice" )
     #end
  </volumePrices>
  <keywords>
     #foreach($keyword in ${ctx.productData.keywords})
	 #if(${keyword})
	 <keyword>${keyword}</keyword>
	 #end
	 #end
  </keywords>
  <variantOptions>
     #foreach( $variantOption in ${ctx.productData.variantOptions} )
     #variantOptionData( ${variantOption} "variantOption" )
     #end
  </variantOptions>
</ProductDetails>