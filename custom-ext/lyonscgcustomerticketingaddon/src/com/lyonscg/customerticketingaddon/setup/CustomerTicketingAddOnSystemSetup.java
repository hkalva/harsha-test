/**
 *
 */
package com.lyonscg.customerticketingaddon.setup;


import de.hybris.platform.addonsupport.setup.AddOnConfigDataImportService;
import de.hybris.platform.addonsupport.setup.AddOnSampleDataImportService;
import de.hybris.platform.addonsupport.setup.impl.DefaultAddOnSystemSetupSupport;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.lyonscg.customerticketingaddon.constants.LyonscgcustomerticketingaddonConstants;


/**
 * Lyonscg Custom Customer Ticketing Add-on Data Setup Class.
 *
 * This is to override the Project data folder to load based on the boolean flag Import Sample Data - Default value
 * false, so that the it doesn't load during Init. Defaulting to false, due to dependency on Initial Data.
 *
 * During Update, if leaContentCatalog data needs to be loaded again, explicitly selecting the extension and Import
 * Sample data as Yes will load the leaContentCatalog.
 *
 * OOTB AddOnCoreDataImportedEventListener & GenericAddOnSampleDataEventListener will take care of loading the all the
 * data during Init process.
 */
@SystemSetup(extension = LyonscgcustomerticketingaddonConstants.EXTENSIONNAME)
public class CustomerTicketingAddOnSystemSetup extends DefaultAddOnSystemSetupSupport
{
	Logger LOG = Logger.getLogger(CustomerTicketingAddOnSystemSetup.class);
	@Resource
	private AddOnConfigDataImportService addonConfigDataImportService;

	/**
	 * @return the addonConfigDataImportService
	 */
	public AddOnConfigDataImportService getAddonConfigDataImportService()
	{
		return addonConfigDataImportService;
	}

	/**
	 * @param addonConfigDataImportService
	 *           the addonConfigDataImportService to set
	 */
	public void setAddonConfigDataImportService(final AddOnConfigDataImportService addonConfigDataImportService)
	{
		this.addonConfigDataImportService = addonConfigDataImportService;
	}

	@Resource
	private AddOnSampleDataImportService addonSampleDataImportService;
	@Resource
	private ConfigurationService configurationService;

	private static final String CONFIG_STORE_ID_PREFIX = "impex.store.prefix.id";
	private static final String IMPORT_SAMPLE_DATA = "importSampleData";

	/**
	 * @return the configurationService
	 */
	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	/**
	 * @param configurationService
	 *           the configurationService to set
	 */
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	/**
	 * @return the addOnSampleDataImportService
	 */
	public AddOnSampleDataImportService getAddOnSampleDataImportService()
	{
		return addonSampleDataImportService;
	}

	/**
	 * @param addOnSampleDataImportService
	 *           the addOnSampleDataImportService to set
	 */
	public void setAddOnSampleDataImportService(final AddOnSampleDataImportService addOnSampleDataImportService)
	{
		this.addonSampleDataImportService = addOnSampleDataImportService;
	}

	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<SystemSetupParameter>();

		params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_DATA, "Import Sample Data", false));
		params.add(createBooleanSystemSetupParameter(ACTIVATE_SOLR_CRON_JOBS, "Activate Solr Cron Jobs", false));

		return params;
	}

	/**
	 * createProject Data method utilizes the AddOnSampleDataImportService.importSampleData() to loads the sample data
	 * specific to a site - All folders except the template folders will be read and processed
	 *
	 * Import of template folder - Core data is not implemented here, as it is not needed.
	 *
	 * @param context
	 */
	@SystemSetup(type = Type.PROJECT, process = Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		LOG.info("Lyonscgcustomerticketingaddon : createProjectData() :"
				+ getBooleanSystemSetupParameter(context, IMPORT_SAMPLE_DATA));
		if (getBooleanSystemSetupParameter(context, IMPORT_SAMPLE_DATA))
		{
			//Retrieve the StoreId from the local.properties for determining the Store and Catalog names
			final String storeUId = getConfigurationService().getConfiguration().getString(CONFIG_STORE_ID_PREFIX);
			LOG.info("Lyonscgcustomerticketingaddon : Sample Data to be loaded for store : " + storeUId);
			final String catalogId = storeUId;
			/*
			 * Add import data for each site you have configured
			 */
			final List<ImportData> importData = new ArrayList<ImportData>();

			final ImportData leaImportData = new ImportData();
			leaImportData.setProductCatalogName(catalogId);
			leaImportData.setContentCatalogNames(Arrays.asList(catalogId));
			leaImportData.setStoreNames(Arrays.asList(storeUId));
			importData.add(leaImportData);

			//To invoke the Sample data import for the Add-on
			getAddOnSampleDataImportService().importSampleData(LyonscgcustomerticketingaddonConstants.EXTENSIONNAME, context,
					importData, false);

		}
	}
}
