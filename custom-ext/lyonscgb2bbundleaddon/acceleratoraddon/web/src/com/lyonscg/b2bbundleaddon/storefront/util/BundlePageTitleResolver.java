package com.lyonscg.b2bbundleaddon.storefront.util;

import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;

import org.apache.commons.lang.StringEscapeUtils;


/**
 * PageTile resolver for Bundle Pages.
 */
public class BundlePageTitleResolver extends PageTitleResolver
{
	/**
	 * Resolve page title for Bundle Pages.
	 * @param bundleTemplateData - all the data related to bundle page.
	 * @return it returns title of the bundle page.
	 */
	public String resolveBundlePageTitle(final BundleTemplateData bundleTemplateData)
	{
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(bundleTemplateData.getName()).append(TITLE_WORD_SEPARATOR);
		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();
		if (currentSite != null)
		{
			stringBuilder.append(currentSite.getName());
		}
		return StringEscapeUtils.escapeHtml(stringBuilder.toString());
	}
}
