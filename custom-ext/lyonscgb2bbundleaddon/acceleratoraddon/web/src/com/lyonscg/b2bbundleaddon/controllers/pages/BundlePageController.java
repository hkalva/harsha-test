/**
 *
 */
package com.lyonscg.b2bbundleaddon.controllers.pages;

import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ReviewForm;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.acceleratorstorefrontcommons.variants.VariantSortStrategy;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.util.Config;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lyonscg.b2bbundleaddon.storefront.util.BundlePageTitleResolver;
import com.lyonscg.bundleapi.data.BundleProductInfo;
import com.lyonscg.bundleapi.data.BundledProductPrice;
import com.lyonscg.bundleapi.product.BundleProductFacade;


/** @author lyonscg.
 * Sending the bundle id as a part of URL. By doing this all the methods and sub-bundles get all products,
 * associated with it and it displays all of the above details. 
 */
@Controller
@RequestMapping(value = "/**/b")
public class BundlePageController extends AbstractPageController
{
	private static final String BUNDLE_PRODUCT_PAGE_ID = "productDetailsForBundles";
	private static final String PRODUCT_CODE_PATH_VARIABLE_PATTERN = "/{productCode:.*}";
	private static final String FUTURE_STOCK_ENABLED = "storefront.products.futurestock.enabled";
	private static final String PRODUCT_BUNDLE_PRICE_PATH = "/price";

	@Resource(name = "bundleProductfacade")
	private BundleProductFacade bundleProductfacade;

	@Resource(name = "bundlePageTitleResolver")
	private BundlePageTitleResolver bundlePageTitleResolver;

	@Resource(name = "bundleTemplateDataUrlResolver")
	private UrlResolver<BundleTemplateData> bundleTemplateDataUrlResolver;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "productBreadcrumbBuilder")
	private ProductBreadcrumbBuilder productBreadcrumbBuilder;

	@Resource(name = "variantSortStrategy")
	private VariantSortStrategy variantSortStrategy;

	@Resource(name = "cmsPageService")
	private CMSPageService cmsPageService;

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String bundleDetail(@PathVariable("productCode") final String productCode, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException,
			UnsupportedEncodingException
	{
		final BundleTemplateData bundleData = bundleProductfacade.getCompleteBundleDetails(productCode);
		model.addAttribute("bundleData", bundleData);
		model.addAttribute("galleryImagesMap", getGalleryImageMap(bundleData));
		final String redirection = checkRequestUrl(request, response, bundleTemplateDataUrlResolver.resolve(bundleData));
		if (StringUtils.isNotEmpty(redirection))
		{
			return redirection;
		}
		updatePageTitle(bundleData, model);
		storeContentPageTitleInModel(model, bundleData.getDescription());
		model.addAttribute(new ReviewForm());
		model.addAttribute("pageType", PageType.PRODUCT.name());
		model.addAttribute("futureStockEnabled", Boolean.valueOf(Config.getBoolean(FUTURE_STOCK_ENABLED, false)));

		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(getKeyWords(bundleData));
		final String metaDescription = MetaSanitizerUtil.sanitizeDescription(bundleData.getDescription());
		setUpMetaData(model, metaKeywords, metaDescription);
		storeCmsPageInModel(model, cmsPageService.getPageForId(BUNDLE_PRODUCT_PAGE_ID));
		return getViewForPage(model);
	}
    
	@RequestMapping(value = PRODUCT_BUNDLE_PRICE_PATH, method = RequestMethod.POST, consumes = "application/json",
            produces = "application/json")
    public @ResponseBody BundledProductPrice getBundlePrice(@RequestBody BundleProductInfo[] bundleProductInfos)
    {
        return bundleProductfacade.getBundlePriceForProductsAndTemplates(Arrays.asList(bundleProductInfos));
    }
	
	private Set<String> getKeyWords(final BundleTemplateData bundleData)
	{
		final Set<String> keyWords = new HashSet<String>();
		final List<ProductData> products = bundleData.getProducts();
		if (CollectionUtils.isNotEmpty(products))
		{
			for (final ProductData productData : products)
			{
				final Set<String> keyWordsSet = productData.getKeywords();
				if (CollectionUtils.isNotEmpty(keyWordsSet))
				{
					keyWords.addAll(keyWordsSet);
				}
			}
		}
		if (CollectionUtils.isNotEmpty(bundleData.getBundleTemplates()))
		{
			for (final BundleTemplateData childBundleData : bundleData.getBundleTemplates())
			{
				final Set<String> childKeyWords = getKeyWords(childBundleData);
				if (!childKeyWords.isEmpty())
				{
					keyWords.addAll(childKeyWords);
				}
			}
		}
		return keyWords;
	}


	private Map<String, List<Map<String, ImageData>>> getGalleryImageMap(final BundleTemplateData bundleData)
	{
		final Map<String, List<Map<String, ImageData>>> galleryImageMap = new HashMap<String, List<Map<String, ImageData>>>();
		final List<ProductData> products = bundleData.getProducts();
		if (CollectionUtils.isNotEmpty(products))
		{
			for (final ProductData productData : products)
			{
				galleryImageMap.put(productData.getCode(), getGalleryImages(productData));
			}
		}
		if (CollectionUtils.isNotEmpty(bundleData.getBundleTemplates()))
		{
			for (final BundleTemplateData childBundleData : bundleData.getBundleTemplates())
			{
				final Map<String, List<Map<String, ImageData>>> childGalleryImageMap = getGalleryImageMap(childBundleData);
				if (!childGalleryImageMap.isEmpty())
				{
					galleryImageMap.putAll(childGalleryImageMap);
				}
			}
		}
		return galleryImageMap;
	}

	protected void updatePageTitle(final BundleTemplateData bundleData, final Model model)
	{
		storeContentPageTitleInModel(model, getBundlePageTitleResolver().resolveBundlePageTitle(bundleData));
	}


	/**Implementing comparator for ImageData.
	 */
	private static class ImageDataComparator implements Comparator<ImageData>, Serializable {

		private static final long serialVersionUID = 1L;
		
		
		@Override
		public int compare(final ImageData image1, final ImageData image2)
		{
			return image1.getGalleryIndex().compareTo(image2.getGalleryIndex());
		}
	}

	protected List<Map<String, ImageData>> getGalleryImages(final ProductData productData)
	{
		final List<Map<String, ImageData>> galleryImages = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(productData.getImages()))
		{
			final List<ImageData> images = new ArrayList<>();
			for (final ImageData image : productData.getImages())
			{
				if (ImageDataType.GALLERY.equals(image.getImageType()))
				{
					images.add(image);
				}
			}
			Collections.sort(images,new ImageDataComparator());

			if (CollectionUtils.isNotEmpty(images))
			{
				addFormatsToGalleryImages(galleryImages, images);
			}
		}
		return galleryImages;
	}

	protected void addFormatsToGalleryImages(final List<Map<String, ImageData>> galleryImages, final List<ImageData> images)
	{
		int currentIndex = images.get(0).getGalleryIndex().intValue();
		Map<String, ImageData> formats = new HashMap<String, ImageData>();
		for (final ImageData image : images)
		{
			if (currentIndex != image.getGalleryIndex().intValue())
			{
				galleryImages.add(formats);
				formats = new HashMap<>();
				currentIndex = image.getGalleryIndex().intValue();
			}
			formats.put(image.getFormat(), image);
		}
		if (!formats.isEmpty())
		{
			galleryImages.add(formats);
		}
	}

	public BundlePageTitleResolver getBundlePageTitleResolver()
	{
		return bundlePageTitleResolver;
	}
}
