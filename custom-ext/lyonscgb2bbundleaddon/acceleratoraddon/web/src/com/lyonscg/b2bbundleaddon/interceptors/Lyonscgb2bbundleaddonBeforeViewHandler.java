/**
 *
 */
package com.lyonscg.b2bbundleaddon.interceptors;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import com.lyonscg.b2bbundleaddon.constants.Lyonscgb2bbundleaddonConstants;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;

/**@author lyonscg.
 *Implementation of bundle addon before view handler for lyonscg.
 */
public class Lyonscgb2bbundleaddonBeforeViewHandler implements
		BeforeViewHandler {

	public static final String VIEW_NAME_MAP_KEY = "viewName";
	private Map<String, Map<String, String>> viewMap;

	@Override
	public void beforeView(final HttpServletRequest request,
			final HttpServletResponse response, final ModelAndView modelAndView)
			throws Exception {
		final String viewName = modelAndView.getViewName();

		if (viewMap != null && viewMap.containsKey(viewName)) {
			modelAndView
					.setViewName(Lyonscgb2bbundleaddonConstants.ADDON_PREFIX
							+ viewMap.get(viewName).get(VIEW_NAME_MAP_KEY));
		}
	}

	public Map<String, Map<String, String>> getViewMap() {
		return viewMap;
	}

	public void setViewMap(final Map<String, Map<String, String>> viewMap) {
		this.viewMap = viewMap;
	}

}
