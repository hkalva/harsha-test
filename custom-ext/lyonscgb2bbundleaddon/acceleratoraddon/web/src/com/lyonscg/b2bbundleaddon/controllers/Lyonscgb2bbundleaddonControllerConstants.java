/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.lyonscg.b2bbundleaddon.controllers;

/**
 */
public interface Lyonscgb2bbundleaddonControllerConstants
{
	String ADDON_PREFIX = "addon:/lyonscgb2bbundleaddon/";

	interface Views
	{
		interface Fragments
		{
			interface Cart
			{
				String AddToCartPopup = "fragments/cart/addToCartPopup";
				String MiniCartPanel = "fragments/cart/miniCartPanel";
				String MiniCartErrorPanel = "fragments/cart/miniCartErrorPanel";
				String CartPopup = ADDON_PREFIX + "fragments/cart/cartPopup";
			}

		}
	}
}
