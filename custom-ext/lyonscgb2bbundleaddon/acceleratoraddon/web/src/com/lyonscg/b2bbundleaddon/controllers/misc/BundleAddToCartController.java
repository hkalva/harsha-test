/**
 *
 */
package com.lyonscg.b2bbundleaddon.controllers.misc;

import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCartPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.configurablebundlefacades.order.BundleCartFacade;
import de.hybris.platform.jalo.JaloObjectNoLongerValidException;
import de.hybris.platform.order.InvalidCartException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.lyonscg.b2bbundleaddon.controllers.Lyonscgb2bbundleaddonControllerConstants;


/**
 * Add to cart controller for Bundles.
 *
 * @author lyonscg
 *
 */
@Controller
public class BundleAddToCartController extends AbstractCartPageController
{
    private static final String TYPE_MISMATCH_ERROR_CODE = "typeMismatch";
    private static final String ERROR_MSG_TYPE = "errorMsg";
    private static final String CART_PAGE = "pages/cart/cartPage";
    private static final String REDIRECT_CART_URL = REDIRECT_PREFIX + "/cart";

    private static final Logger LOG = LoggerFactory.getLogger(BundleAddToCartController.class);

    @Resource(name = "cartFacade")
    private BundleCartFacade cartFacade;

    @Resource(name = "bundleProductfacade")
    private ProductFacade bundleProductfacade;

    @Resource(name = "simpleBreadcrumbBuilder")
    private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

    protected void addProduct(final String code, final Model model, final long qty, final Integer bundleNo,
            final String bundleTemplateId)
    {
        try
        {
            final List<CartModificationData> cartModifications = getCartFacade().addToCart(code, qty, bundleNo.intValue(),
                    bundleTemplateId, false);
            model.addAttribute("modifiedCartData", cartModifications);

            long quantity = qty;

            for (final CartModificationData cartModification : cartModifications)
            {

                if (cartModification.getQuantityAdded() == 0L)
                {
                    GlobalMessages.addErrorMessage(model,
                            "basket.information.quantity.noItemsAdded." + cartModification.getStatusCode());
                    model.addAttribute(ERROR_MSG_TYPE,
                            "basket.information.quantity.noItemsAdded." + cartModification.getStatusCode());
                    quantity = 0;
                }
                else if (cartModification.getQuantityAdded() < qty)
                {
                    GlobalMessages.addErrorMessage(model, "basket.information.quantity.reducedNumberOfItemsAdded."
                            + cartModification.getStatusCode());
                    model.addAttribute(ERROR_MSG_TYPE, "basket.information.quantity.reducedNumberOfItemsAdded."
                            + cartModification.getStatusCode());

                    quantity = cartModification.getQuantityAdded();
                }

                model.addAttribute("entry", cartModification.getEntry());
                model.addAttribute("cartCode", cartModification.getCartCode());
            }

            model.addAttribute("quantity", Long.valueOf(quantity));

        }
        catch (final CommerceCartModificationException ex)
        {
            model.addAttribute("errorMsg", "basket.error.occurred");
            LOG.warn("Couldn't add product of code " + code + " to cart.", ex);
        }

        final CartData cartData = getCartFacade().getSessionCart();
        model.addAttribute("cartData", cartData);
    }

    /**
     * Original method written to invoke add to cart with 2 products. Retaining this method, as the back end also has
     * methods with 2 product codes explicitly stated. Changed the request mapping to be /cart/addBundleWith2Products,
     * so this is not invoked.
     *
     * @param productCode1
     *            Product code of the first product selected within the bundle.
     * @param productCode2
     *            Product code of the second product selected within the bundle.
     * @param model
     *            Model Object
     * @param bundleTemplateId1
     *            bundleTemplateId1 of the first product selected within the bundle.
     * @param bundleTemplateId2
     *            bundleTemplateId2 of the first product selected within the bundle.
     * @return JSON String object.
     */
    @RequestMapping(value = "/cart/addBundleWith2Products", method = RequestMethod.POST, produces = "application/json")
    public String addToCartBundleWith2Products(@RequestParam("productCode1") final String productCode1,
            @RequestParam("productCode2") final String productCode2, final Model model, @RequestParam(
                    value = "bundleTemplateId1", required = false) final String bundleTemplateId1, @RequestParam(
                            value = "bundleTemplateId2", required = false) final String bundleTemplateId2)
    {
        try
        {
            final List<CartModificationData> cartModifications = getCartFacade().addToCart(productCode1, -1, bundleTemplateId1,
                    productCode2, bundleTemplateId2);
            model.addAttribute("modifications", cartModifications);

            for (final CartModificationData cartModification : cartModifications)
            {
                if (cartModification.getEntry() == null)
                {
                    GlobalMessages.addErrorMessage(model, "basket.information.quantity.noItemsAdded");
                    model.addAttribute("errorMsg", "basket.information.quantity.noItemsAdded");
                    throw new CommerceCartModificationException("Cart entry was not created. Reason: "
                            + cartModification.getStatusCode());
                }
            }
        }
        catch (final CommerceCartModificationException ex)
        {
            model.addAttribute("errorMsg", "basket.error.occurred");
            LOG.error("Couldn't add products of code " + productCode1 + " and " + productCode2 + " to cart.", ex);
            // we have no bundleId here, so we cannot redirect to extras-page; go to cart page instead.
            return REDIRECT_PREFIX + "/cart";
        }
        return Lyonscgb2bbundleaddonControllerConstants.Views.Fragments.Cart.AddToCartPopup;
    }

    /**
     * To add a list of products of a Bundle to Cart. Each product is individually added to cart, and the bundle number
     * generated with the first add to cart function call is used for the subsequent add to cart function calls, so that
     * all the products are listed under a single bundle.
     *
     * @param productCodes
     *            List of selected product codes from a bundle, which is in the format of comma separated string.
     * @param model
     *            Model Object.
     * @param bundleTemplateIds
     *            List of bundle template Ids for the selected product codes from a bundle, which is in the format of
     *            comma separated string.
     * @return JSON Object String
     */
    @RequestMapping(value = "/cart/addBundle", method = RequestMethod.POST, produces = "application/json")
    public String addToCartBundle(@RequestParam("productCodes") final String productCodes, final Model model, @RequestParam(
            value = "bundleTemplateIds", required = false) final String bundleTemplateIds)
    {
        try
        {
            final String[] productCodesLst = StringUtils.split(productCodes, ",");
            final String[] bundleTemplateIdsLst = StringUtils.split(bundleTemplateIds, ",");
            final List<CartModificationData> cartModifications = new ArrayList<CartModificationData>();
            int idx = 0;
            int newBundleNo = -1;
            for (final String productCode : productCodesLst)
            {
                final CartModificationData modification1 = (getCartFacade().addToCart(productCode, 1L, newBundleNo,
                        bundleTemplateIdsLst[idx], false)).get(0);

                if (modification1.getEntry() != null && modification1.getEntry().getBundleNo() != -1)
                {
                    newBundleNo = modification1.getEntry().getBundleNo();
                }
                cartModifications.add(modification1);
                idx++;
            }
            model.addAttribute("modifications", cartModifications);

            for (final CartModificationData cartModification : cartModifications)
            {
                if (cartModification.getEntry() == null)
                {
                    GlobalMessages.addErrorMessage(model, "basket.information.quantity.noItemsAdded");
                    model.addAttribute("errorMsg", "basket.information.quantity.noItemsAdded");
                    throw new CommerceCartModificationException("Cart entry was not created. Reason: "
                            + cartModification.getStatusCode());
                }
            }
        }
        catch (final CommerceCartModificationException ex)
        {
            model.addAttribute("errorMsg", "basket.error.occurred");
            LOG.error("Couldn't add products of code " + productCodes + " to cart.", ex);
            // we have no bundleId here, so we cannot redirect to extras-page; go to cart page instead.
            return REDIRECT_PREFIX + "/cart";
        }
        return Lyonscgb2bbundleaddonControllerConstants.Views.Fragments.Cart.AddToCartPopup;
    }

    /**
     * Add product to cart.
     *
     * @param code
     * @return
     * @throws CommerceCartModificationException
     * @throws InvalidCartException
     */
    @RequestMapping(value = "/sbgaddtocart", method = RequestMethod.POST, produces = "application/json")
    public String postAddToCart(@RequestParam("productCodePost") final String code, final Model model)
            throws CommerceCartModificationException, InvalidCartException
    {
        Long quantityAdded = null;
        try
        {
            final CartModificationData cartModification = getCartFacade().addToCart(code, 1);
            quantityAdded = Long.valueOf(cartModification.getQuantityAdded());
        }
        catch (final JaloObjectNoLongerValidException ex)
        {
            LOG.warn(String.format("Couldn't update product with the code: %s, quantity: %d.", code, Integer.valueOf(1)), ex);
            quantityAdded = Long.valueOf(0);
        }
        model.addAttribute("quantity", quantityAdded);
        model.addAttribute("product", bundleProductfacade.getProductForCodeAndOptions(code, Arrays.asList(ProductOption.BASIC)));
        return Lyonscgb2bbundleaddonControllerConstants.Views.Fragments.Cart.AddToCartPopup;
    }

    /**
     * Remove bundle from cart.
     *
     * @param bundleNumber
     *            - bundle number from cart entry.
     * @param model
     *            - Model map for rendering page.
     * @param request
     *            - Http request.
     * @param redirectModel
     *            - Model map for redirecting page
     * @return - The page to be rendered.
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = "/cart/removebundle", method = RequestMethod.POST)
    public String updateCartQuantities(@RequestParam("bundleNumber") final int bundleNumber, final Model model,
            final HttpServletRequest request, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
    {
        if (getCartFacade().hasEntries())
        {
            try
            {
                getCartFacade().deleteCartBundle(bundleNumber);
                addFlashMessage(request, redirectModel);
                // Redirect to the cart page on update success so that the browser doesn't re-post again
                return REDIRECT_CART_URL;
            }
            catch (final CommerceCartModificationException ex)
            {
                LOG.warn("Couldn't remove bundle " + bundleNumber + ".", ex);
            }
        }

        prepareDataForPage(model);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.cart"));
        model.addAttribute("pageType", PageType.CART.name());
        GlobalMessages.addErrorMessage(model, "bundle.page.message.remove.error");
        return CART_PAGE;
    }

    protected void addFlashMessage(final HttpServletRequest request, final RedirectAttributes redirectModel)
    {
        GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "bundle.page.message.remove");
    }

    protected boolean isTypeMismatchError(final ObjectError error)
    {
        return error.getCode().equals(TYPE_MISMATCH_ERROR_CODE);
    }

    @Override
    protected BundleCartFacade getCartFacade()
    {
        return cartFacade;
    }

    protected void setCartFacade(final BundleCartFacade cartFacade)
    {
        this.cartFacade = cartFacade;
    }
}
