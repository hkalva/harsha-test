/**
 *
 */
package com.lyonscg.b2bbundleaddon.storefront.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**Unit Tests for BundlePageTitleResolver.
 */
@UnitTest
public class BundlePageTitleResolverTest
{
	private static final String TITLE_WORD_SEPARATOR = " | ";
	@InjectMocks
	private BundlePageTitleResolver bundlePageTitleResolver;
	private BundleTemplateData bundleTemplateData;
	@Mock
	private CMSSiteService cmsSiteService;
	private final String bundleName = "bundleName";
	private final String siteName = "siteName";
	@Mock
	private CMSSiteModel currentSite;

	/** Setting up bundle template data.
	 */
	@Before
	public void setup()
	{
		bundlePageTitleResolver = new BundlePageTitleResolver();
		MockitoAnnotations.initMocks(this);
		bundleTemplateData = new BundleTemplateData();
		bundleTemplateData.setName(bundleName);
		Mockito.doReturn(currentSite).when(cmsSiteService).getCurrentSite();
		Mockito.doReturn(siteName).when(currentSite).getName();
	}

	/** Testing the bundlePagetitleresolver functionality.
	 */
	@Test
	public void resolveBundlePageTitleTest()
	{
		final String expectedName = bundleTemplateData.getName() + TITLE_WORD_SEPARATOR + currentSite.getName();
		Assert.assertEquals(expectedName, bundlePageTitleResolver.resolveBundlePageTitle(bundleTemplateData));
	}
}
