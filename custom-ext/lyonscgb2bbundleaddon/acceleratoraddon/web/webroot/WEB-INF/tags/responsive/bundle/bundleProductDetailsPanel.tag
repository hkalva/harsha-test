<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>


<div class="product js-product-bundle" data-product-id="${product.code}">

	<div class="image">
		<c:set var="productCode" scope="request" value="${product.code}"/>
		<c:set var="container" value="${galleryImagesMap[productCode][0]}"/>
		<img src="${container.product.url}" alt="${container.thumbnail.altText}" />
	</div>

	<div class="info">
	<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">

		<c:url value="${product.url}" var="productUrl"/>
		<div class="name">
			<a href="${productUrl}">${product.name}</a> <span class="sku">ID</span> <span class="code">${product.code}</span>
		</div>
	</ycommerce:testId>

		<div class="review">
			<product:productReviewSummary product="${product}" showLinks="false"/>
		</div>
		
		<div class="details">
			<product:productPromotionSection product="${product}"/>

			<ycommerce:testId
				code="productDetails_productNamePrice_label_${product.code}">
				<product:productPricePanel product="${product}" />
			</ycommerce:testId>

			<div class="description">${product.summary}</div>
		</div>

	
	</div>

</div>



