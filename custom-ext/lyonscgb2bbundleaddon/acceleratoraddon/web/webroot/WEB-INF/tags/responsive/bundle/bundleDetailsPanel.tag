<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="bundle" tagdir="/WEB-INF/tags/addons/lyonscgb2bbundleaddon/responsive/bundle"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ attribute name="bundleData" required="true" type="de.hybris.platform.configurablebundlefacades.data.BundleTemplateData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="${not empty bundleData.products ? 'js-bundle ' : ''}bundle" id="${bundleData.id}">

	<c:if test="${not empty bundleData.products}">

		<h2><spring:theme code="text.bundle.selectProduct"/></h2>

		<div class="error" style="display: none;"><spring:theme code="text.bundle.selectProduct.error"/></div>

		<div class="products">

			<c:forEach items="${bundleData.products}" var="productData">

				<c:set var="product" scope="request" value="${productData}"/>

				<bundle:bundleProductDetailsPanel />
				
				<bundle:bundleProductTabs />

			</c:forEach>

			<input type="hidden" class="maxQuantity" value="${bundleData.maxItemsAllowed}"/>
			<input type="hidden" class="minQuantity" value="${bundleData.minItemsAllowed}"/>
			<input type="hidden" class="bundleId" value="${bundleData.id}"/>

		</div>
	</c:if>
	
	<div class="childBundles">
		<c:forEach items="${bundleData.bundleTemplates}" var="childBundleData">
			<bundle:bundleDetailsPanel bundleData="${childBundleData}"/>
		</c:forEach>
	</div>
</div>