<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="bundle" tagdir="/WEB-INF/tags/addons/lyonscgb2bbundleaddon/responsive/bundle"%>
<%@ attribute name="bundleData" required="true" type="de.hybris.platform.configurablebundlefacades.data.BundleTemplateData" %>


3333333333333333333333333333333333333333333333333333333333

<div class="bundle">
Bundle id:${bundleData.id}
	<div class="products">
	<c:forEach items="${bundleData.products}" var="product">
		<div class="product">
		Product id:${product.code}
		Product Name:${product.name}
		</div>
	</c:forEach>
	</div>
	<div class="childBundles">
	<c:forEach items="${bundleData.bundleTemplates}" var="childBundleData">
		<div class="childBundle">
			<bundle:bundle bundleData="${childBundleData}"/>
		</div>
	</c:forEach>
	</div>
</div>