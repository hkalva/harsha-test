<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<br />
<%--Bundle Component JSP to render the bundle package related restoration data on top of the Merge Cart page --%>
<c:forEach items="${restorationData.bundleModificationsMap}" var="bundleModificationsEntry" >
	<c:set var="bundleDetails" value="${fn:split(bundleModificationsEntry.key,':')}" />
	<c:set var="bundleNo" value="${bundleDetails[0]}" />
	<c:set var="bundleName" value="${bundleDetails[1]}" />
			<b>${bundleName}</b>
			<ul>
			<c:forEach items="${bundleModificationsEntry.value}"	var="modification">
				<li>
				<c:url value="${modification.entry.product.url}" var="entryUrl" />
				<c:choose>
					<c:when
						test="${modification.deliveryModeChanged and not empty modification.entry}">
						<spring:theme code="basket.restoration.delivery.changed"
							arguments="${modification.entry.product.name},${entryUrl},${modification.entry.quantity},${modification.quantityAdded}" />
					</c:when>
					<c:when
						test="${modification.deliveryModeChanged and empty modification.entry}">
						<spring:theme code="basket.restoration.delivery.changed"
							arguments="${modification.entry.product.name},${entryUrl},${modification.quantity},${modification.quantityAdded}" />
					</c:when>
					<c:when test="${not modification.deliveryModeChanged and not empty modification.entry}">
						<spring:theme
							code="basket.restoration.${modification.statusCode}"
							arguments="${modification.entry.product.name},${entryUrl},${modification.entry.quantity},${modification.quantityAdded}" />
					</c:when>
					<c:when test="${not modification.deliveryModeChanged and empty modification.entry}">
						<spring:theme
							code="basket.restoration.${modification.statusCode}"
							arguments="${modification.entry.product.name},${entryUrl},${modification.quantity},${modification.quantityAdded}" />
					</c:when>
				</c:choose>
				</li>
			</c:forEach>
			</ul>
</c:forEach>
