<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:url value="/cart/removebundle" var="cartRemoveBundle" />

<c:forEach var="entry" items="${cartData.bundleEntriesMap}">
	<c:set var="bundleDetails" value="${fn:split(entry.key,':')}" />
	<c:set var="bundleNo" value="${bundleDetails[0]}" />
	<c:set var="bundleName" value="${bundleDetails[1]}" />

	<li class="bundleEntry">
	 	<div id="bundle${entry.key}" class="bundleEntry_details">
	 		<div class="bundleEntry_name">
				${bundleName}
			</div>
			<div class="item-remove bundleEntry_remove">
	            <form:form id="cartRemoveBundle_${bundleNo}" action="${cartRemoveBundle}" method="post">
					<input type="hidden" name="bundleNumber" value="${bundleNo}"/>
					<button class="btn js-remove-bundle-button" id="removeBundle_${bundleNo}" type="submit">
						<span class="glyphicon glyphicon-remove"></span>
					</button>
				</form:form>
			</div>
	  	</div>
	  	<ul class="bundleEntry_products">
  		<c:forEach items="${entry.value}" var="entry" varStatus="loop">
  			<cart:cartEntry entry="${entry}" />
  		</c:forEach>
  		</ul>
	</li>

</c:forEach>