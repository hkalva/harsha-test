<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="bundle" tagdir="/WEB-INF/tags/addons/lyonscgb2bbundleaddon/responsive/bundle"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">

  <jsp:body>

	<div class="bundleInfo">
		<h1>${bundleData.name}</h1>

		<div class="bundleDetails">

			<p class="bundleDescription">${bundleData.description}</p>

			<div class="bundlePricing">
				<div class="bundlePrice js-bundlePrice">
				</div>

				<div class="originalPrice js-originalPrice">
				</div>
			</div>
		</div>

		<bundle:bundleDetailsPanel bundleData="${bundleData}"/>
		<c:url var="bundleAddToCart" value="/cart/addBundle"/>
		<div class="bundleAddToCart">
		  <form:form method="post" id="addToCartForm" class="add_to_cart_form" action="${bundleAddToCart}">
		   <input type="hidden" id="productCodes" name="productCodes" value=""/>
		   <input type="hidden" id="bundleTemplateIds" name="bundleTemplateIds" value=""/>
		  </form:form>

		  <button id="addBundleToCartButton" class="btn btn-primary btn-block js-add-to-cart js-enable-btn btn-icon glyphicon-shopping-cart">
			<spring:theme code="text.bundle.addToCart"/>
		  </button>

	</div>

	<div id="addToCartTitle" class="display-none">
		<div class="add-to-cart-header">
			<div class="headline">
				<span class="headline-text"><spring:theme code="basket.added.to.basket"/></span>
			</div>
		</div>
	</div>
	
  </jsp:body>

</template:page>