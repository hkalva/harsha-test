/**
 *
 */
package com.capgemini.seoservices.url.impl;

import java.util.Locale;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.services.url.UrlHelper;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.DefaultProductModelUrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;


/**
 * Implementation of Seo product model Url resolver abstract class.
 *
 * @author lyonscg.
 */
public class SeoProductModelUrlResolver extends DefaultProductModelUrlResolver
{
	private static final Logger LOG = LoggerFactory.getLogger(SeoProductModelUrlResolver.class);
	private SeoCategoryModelUrlResolver categoryModelUrlResolver;
	protected static final String BRAND_STRING = "brand_";


	@Override
	protected String resolveInternal(final ProductModel source)
	{
		final ProductModel baseProduct = getBaseProduct(source);
		
		if (LOG.isDebugEnabled())
		{
    		LOG.debug("Source Product Name: {}", source.getName(Locale.ENGLISH));
    		LOG.debug("Base Product Name: {}", baseProduct.getName(Locale.ENGLISH));
		}

		final BaseSiteModel currentBaseSite = getBaseSiteService().getCurrentBaseSite();

		String url = getPattern();

		if (currentBaseSite != null && url.contains("{baseSite-uid}"))
		{
			url = url.replace("{baseSite-uid}", currentBaseSite.getUid());
		}
		if (url.contains("{category-path}"))
		{
			url = url.replace("{category-path}",
					UrlHelper.urlSafe(getCategoryModelUrlResolver().getSeoCategoryPath(getPrimaryCategoryForProduct(baseProduct))));
		}
		if (url.contains("{product-name}"))
		{
			url = url.replace("{product-name}", UrlHelper.urlSafe(StringUtils.lowerCase(baseProduct.getName(Locale.ENGLISH))));
		}
		if (url.contains("{product-code}"))
		{
			url = url.replace("{product-code}", source.getCode());
		}

        return UrlHelper.removeDoubleSlashes(url);
	}

	@Override
	public CategoryModel getPrimaryCategoryForProduct(final ProductModel product)
	{
		// Get the first super-category from the product that isn't a classification category or a brand category
		CategoryModel firstCategory = null;
		
		for (final CategoryModel category : product.getSupercategories())
		{
		    if ((category instanceof ClassificationClassModel) || StringUtils.startsWithIgnoreCase(category.getCode(), BRAND_STRING))
		    {
		        continue;
		    }
		    
			if (firstCategory == null)
			{
				firstCategory = category;
			}
			
			// Pick the first lowest category
			if (CollectionUtils.isEmpty(category.getAllSubcategories()))
			{
			    if (LOG.isDebugEnabled())
			    {
			        LOG.debug("Returning Category Code: {}", category.getCode());
			    }
			    
				return category;
			}
		}
		
		if (LOG.isDebugEnabled())
		{
		    LOG.debug("Returning first category Code: {}", (firstCategory == null ? "null" : firstCategory.getCode()));
		}
		
		return firstCategory;
	}

	/**
	 * @return {@link SeoCategoryModelUrlResolver}
	 */
	public SeoCategoryModelUrlResolver getCategoryModelUrlResolver()
	{
		return categoryModelUrlResolver;
	}

	/**
	 * @param categoryModelUrlResolver
	 */
	public void setCategoryModelUrlResolver(final SeoCategoryModelUrlResolver categoryModelUrlResolver)
	{
		this.categoryModelUrlResolver = categoryModelUrlResolver;
	}

	protected ProductModel getBaseProduct(final ProductModel product)
	{
		ProductModel current = product;

		while (current instanceof VariantProductModel)
		{
			final ProductModel baseProduct = ((VariantProductModel) current).getBaseProduct();
			if (baseProduct == null)
			{
				break;
			}
			else
			{
				current = baseProduct;
			}
		}
		return current;
	}
	
}
