/**
 *
 */
package com.capgemini.seoservices.url.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.seoservices.dao.NavigationDaoImpl;
import com.capgemini.services.url.UrlHelper;

import de.hybris.platform.acceleratorcms.model.components.NavigationBarComponentModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.commerceservices.url.impl.DefaultCategoryModelUrlResolver;
import de.hybris.platform.util.Config;


/**
 * Implementation of seo category model url resolver abstract class.
 * 
 * @author lyonscg.
 */
public class SeoCategoryModelUrlResolver extends DefaultCategoryModelUrlResolver
{
    /**
     * the logger.
     */
	private static final Logger LOG = LoggerFactory.getLogger(SeoCategoryModelUrlResolver.class);

	/**
	 * constant for use navigation node pathing flag.
	 */
    private static final String USE_NAVIGATION_NODE_PATHING = "use.navigation.node.based.path";

    /**
     * the navigation dao.
     */
	private NavigationDaoImpl navigationDao;

	/**
	 * Returns a SEO category path. The path may be based on the navigation node structure or the catalog category
	 * hierarchy. Which data is used is based on the "use.navigation.node.based.path" property value. This string is
	 * typically used to replace the "{category-path}" defined in the Pattern attribute. The returned value will be
	 * converted to lower case and have any spaces replaced with hyphens. For example /Hand Tools/Nut Driver/ would
	 * become: /hand-tools/nut-driver/
	 *
	 * @param source
	 *           {@link CategoryModel} category to get path for
	 * @return {@link String} path
	 */
	public String getSeoCategoryPath(final CategoryModel source)
	{
		String categoryPath = null;
		final boolean useNavNodePath = BooleanUtils.toBoolean(Config.getParameter(USE_NAVIGATION_NODE_PATHING));
		/*
		 * HLP1-515 - Setting the locale to English in all cases, so that the category navigation URLs are rendered
		 * correctly for all languages
		 */
		final Locale locale = Locale.ENGLISH;

		if (useNavNodePath)
		{
			categoryPath = getCategoryNavigationPath(source);
		}
		if (StringUtils.isEmpty(categoryPath) || !useNavNodePath)
		{
			categoryPath = buildPathString(getCategoryPath(source));
		}
		LOG.debug("Base categoryPath: " + categoryPath);
		categoryPath = StringUtils.lowerCase(categoryPath, locale).replaceAll("\\s+", "-");
		LOG.debug("Returned categoryPath: " + categoryPath);
		return categoryPath;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.commerceservices.url.impl.DefaultCategoryModelUrlResolver#buildPathString(java.util.List)
	 */
	@Override
	protected String buildPathString(final List<CategoryModel> categories)
	{
		final Locale locale = Locale.ENGLISH;

		final StringBuilder result = new StringBuilder();
		boolean appendSlash = false;
		for (final CategoryModel category : categories)
		{
			if (appendSlash)
			{
				result.append('/');
			}
			if (BooleanUtils.isFalse(category.getUseInCategoryPathUrl()))
			{
				appendSlash = false;
			}
			else
			{
                String name = category.getName(locale);
                result.append(UrlHelper.urlSafe(UrlHelper.replaceSlash(name == null ? "" : name)));
				appendSlash = true;
			}
		}
		return result.toString();
	}

	@Override
	protected String resolveInternal(final CategoryModel source)
	{
		// Work out values

		// Replace pattern values
		String url = getPattern();
		if (url.contains("{baseSite-uid}"))
		{
			url = url.replace("{baseSite-uid}", getBaseSiteUid());
		}
		if (url.contains("{category-path}"))
		{
			final String categoryPath = UrlHelper.urlSafe(getSeoCategoryPath(source));

			if (StringUtils.isNotEmpty(categoryPath))
			{
				url = url.replace("{category-path}", categoryPath);
			}
			else
			{
				url = url.replace("{category-path}/", StringUtils.EMPTY);
			}
		}
		if (url.contains("{category-code}"))
		{
			final String categoryCode = urlEncode(source.getCode()).replaceAll("\\+", "%20");
			url = url.replace("{category-code}", categoryCode);
		}
		if (url.contains("{catalog-id}"))
		{
			url = url.replace("{catalog-id}", source.getCatalogVersion().getCatalog().getId());
		}
		if (url.contains("{catalogVersion}"))
		{
			url = url.replace("{catalogVersion}", source.getCatalogVersion().getVersion());
		}

        return UrlHelper.removeDoubleSlashes(url);

	}

	/**
	 * Returns the navigation path to the requested category as configured via navigation nodes
	 *
	 * @param category
	 *           {@link CategoryModel} the category you want the path for
	 * @return {@link String} navigation node path
	 */
	protected String getCategoryNavigationPath(final CategoryModel category)
	{

		// find any CMS Link Components where the category code is the code for this category
		final List<CMSLinkComponentModel> links = category.getLinkComponents();
		final List<NavigationBarComponentModel> navBarLinks = getNavigationDao().findAllNavigationBarLinksForGivingCategory(
				category);
		LOG.debug("Here are the following links for category: " + category.getCode());
		/*
		 * HLP1-515 - Setting the locale to English in all cases, so that the category navigation URLs are rendered
		 * correctly for all languages
		 */
		final Locale locale = Locale.ENGLISH;
		String toReturn = null;

		if (CollectionUtils.isNotEmpty(navBarLinks))
		{
			// we are just going to take the last one because if we find more than one here
			// that means that there are several navigation nodes that do not have drop downs
			// and all point to the same category
			for (final NavigationBarComponentModel model : navBarLinks)
			{
				toReturn = model.getNavigationNode().getTitle(locale);
			}
			LOG.debug("Direct link path: " + toReturn);
		}
		else if (CollectionUtils.isNotEmpty(links))
		{
		    toReturn= getCMSNavigationNodeTitlesLink(links,locale,category);
		}
		LOG.debug("Returning: " + toReturn);
		return toReturn;
	}

	/**
	 * 
	 * @param links
	 * @param locale
	 * @param category
	 * @return
	 */
	private String getCMSNavigationNodeTitlesLink(List<CMSLinkComponentModel> links, Locale locale, CategoryModel category)
    {
	    String toReturn = null;
        for (final CMSLinkComponentModel link : links)
        {
            final List<CMSNavigationNodeModel> navigationNodes = link.getNavigationNodes();
            
            if (!CollectionUtils.isEmpty(navigationNodes))
            {
                final String baseLinkName = link.getLinkName(locale);
                LOG.debug("Base link name: " + baseLinkName);
                
                List<CMSNavigationNodeModel> navigationList =getCMSNavigationNodeList(navigationNodes);
               
                StringBuilder sb =appendCMSNavigationTitleFromList(navigationList,locale); 
                
                sb.append(baseLinkName);
                LOG.debug(sb.toString());
                toReturn = sb.toString();
            }
            else
            {
                String name = category.getName(locale);
                toReturn = UrlHelper.replaceSlash(name == null ? "" : name);
                LOG.debug(toReturn);
            }
        }
        return toReturn;
    }
	
	/**
	 * 
	 * @param navigationList
	 * @param locale 
	 * @return
	 */
	private StringBuilder appendCMSNavigationTitleFromList(List<CMSNavigationNodeModel> navigationList, Locale locale)
    {
	    Collections.reverse(navigationList);
        final StringBuilder sb = new StringBuilder();
        for (final CMSNavigationNodeModel item : navigationList)
        {
            final String title = item.getTitle(locale);
            LOG.debug("Navigation Node: " + item.getName() + " has title of: " + title
                    + " and UseInCateogryPathUrl value of: " + BooleanUtils.toBoolean(item.getUseInCategoryPathUrl()));
            if (StringUtils.isNotEmpty(title) && BooleanUtils.toBoolean(item.getUseInCategoryPathUrl()))
            {
                sb.append(title).append("/");
            }
        }
        return sb;
    }

/**
 * 
 * @param navigationNodes
 * @return
 */
    private List<CMSNavigationNodeModel> getCMSNavigationNodeList(List<CMSNavigationNodeModel> navigationNodes)
    {
        final List<CMSNavigationNodeModel> navigationList = new ArrayList<CMSNavigationNodeModel>();
        for (final CMSNavigationNodeModel nav : navigationNodes)
        {
            CMSNavigationNodeModel parent = nav.getParent();
            navigationList.add(nav);
            while (parent != null)
            {
                navigationList.add(parent);
                parent = parent.getParent();
            }
        }
        return navigationList;
    }

    /**
	 * @return {@link NavigationDaoImpl}
	 */
	public NavigationDaoImpl getNavigationDao()
	{
		return navigationDao;
	}

	/**
	 * @param navigationDao
	 */
	public void setNavigationDao(final NavigationDaoImpl navigationDao)
	{
		this.navigationDao = navigationDao;
	}

}
