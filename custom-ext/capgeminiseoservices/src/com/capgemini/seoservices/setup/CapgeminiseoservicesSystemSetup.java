package com.capgemini.seoservices.setup;

import java.util.Collections;
import java.util.List;

import com.capgemini.seoservices.constants.CapgeminiseoservicesConstants;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;

/**
 *
 * @author lyonscg
 *
 */
@SystemSetup(extension = CapgeminiseoservicesConstants.EXTENSIONNAME)
public class CapgeminiseoservicesSystemSetup extends AbstractSystemSetup
{

    @Override
    public List<SystemSetupParameter> getInitializationOptions()
    {
        return Collections.emptyList();
    }

    @SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
    public void createEssentialData(final SystemSetupContext context)
    {
        importImpexFile(context, "/capgeminiseoservices/import/essentialData.impex", true);
    }
    
}
