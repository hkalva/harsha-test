/*
* Copyright (c) 2019 Capgemini. All rights reserved.
*
* This software is the confidential and proprietary information of Capgemini
* ("Confidential Information"). You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the
* license agreement you entered into with Capgemini.
*/
package com.capgemini.seoservices.sitemap.populators;

import java.text.SimpleDateFormat;

import com.capgemini.seoservices.constants.CapgeminiseoservicesConstants;
import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.acceleratorservices.sitemap.populators.ContentPageModelToSiteMapUrlDataPopulator;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

/**
 * 
 * @author lyonscg
 *
 */
public class SeoContentPageModelToSiteMapUrlDataPopulator extends ContentPageModelToSiteMapUrlDataPopulator
{
    @Override
    public void populate(final ContentPageModel contentPageModel, final SiteMapUrlData siteMapUrlData) throws ConversionException
    {
        super.populate(contentPageModel, siteMapUrlData);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CapgeminiseoservicesConstants.SITEMAP_DATE_FORMAT);
        siteMapUrlData.setLastmod(simpleDateFormat.format(contentPageModel.getModifiedtime()));
    }
}
