/*
* Copyright (c) 2019 Capgemini. All rights reserved.
*
* This software is the confidential and proprietary information of Capgemini
* ("Confidential Information"). You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the
* license agreement you entered into with Capgemini.
*/
package com.capgemini.seoservices.sitemap.populators;

import java.text.SimpleDateFormat;

import com.capgemini.seoservices.constants.CapgeminiseoservicesConstants;

import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.acceleratorservices.sitemap.populators.ProductModelToSiteMapUrlDataPopulator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
/**
 * 
 * @author lyonscg
 *
 */
public class SeoProductModelToSiteMapUrlDataPopulator extends ProductModelToSiteMapUrlDataPopulator
{
    @Override
    public void populate(final ProductModel productModel, final SiteMapUrlData siteMapUrlData) throws ConversionException
    {
        super.populate(productModel, siteMapUrlData);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CapgeminiseoservicesConstants.SITEMAP_DATE_FORMAT);
        siteMapUrlData.setLastmod(simpleDateFormat.format(productModel.getModifiedtime()));
    }
}
