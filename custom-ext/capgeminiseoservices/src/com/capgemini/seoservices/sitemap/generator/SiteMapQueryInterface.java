/*
* Copyright (c) 2019 Capgemini. All rights reserved.
*
* This software is the confidential and proprietary information of Capgemini
* ("Confidential Information"). You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the
* license agreement you entered into with Capgemini.
*/
package com.capgemini.seoservices.sitemap.generator;

import java.util.Collections;
import java.util.List;

import com.capgemini.seoservices.model.SiteMapQueryModel;

import de.hybris.platform.acceleratorservices.enums.SiteMapPageEnum;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

/**
 * This interface used to find the query for all sitemaps.
 */
public interface SiteMapQueryInterface
{
    default List<SiteMapQueryModel> findSiteMapQuery(SiteMapPageEnum siteMapPageEnum, GenericDao<SiteMapQueryModel> siteMapQueryGenericDao)
    {
        return siteMapQueryGenericDao.find(Collections.singletonMap(SiteMapQueryModel.PAGETYPE, siteMapPageEnum));
    }
}
