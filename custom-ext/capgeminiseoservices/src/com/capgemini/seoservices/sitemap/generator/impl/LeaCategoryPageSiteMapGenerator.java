/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.capgemini.seoservices.sitemap.generator.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.capgemini.seoservices.model.SiteMapQueryModel;
import com.capgemini.seoservices.sitemap.generator.SiteMapQueryInterface;
import com.capgemini.services.category.LeaCategoryService;

import de.hybris.platform.acceleratorservices.sitemap.generator.impl.CategoryPageSiteMapGenerator;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;


/**
 * To extend the OOTB CategoryPageSiteMapGenerator to restrict the categories to the ones that have at least one
 * approved product
 */
public class LeaCategoryPageSiteMapGenerator extends CategoryPageSiteMapGenerator implements SiteMapQueryInterface
{
    private static final String DEFAULT_QUERY = "SELECT {c.pk} FROM {Category! AS c JOIN CatalogVersion AS cv ON {c.catalogVersion}={cv.pk} "
            + " JOIN Catalog AS cat ON {cv.pk}={cat.activeCatalogVersion} "
            + " JOIN CMSSite AS site ON {cat.pk}={site.defaultCatalog}}  WHERE {site.pk} = ?site "
            + " AND NOT exists ({{select {cr.pk} from {CategoriesForRestriction as cr} where {cr.target} = {c.pk} }})";
    private static final Logger LOG = Logger.getLogger(LeaCategoryPageSiteMapGenerator.class);
    private LeaCategoryService categoryService;
    private GenericDao<SiteMapQueryModel> siteMapQueryGenericDao;

    /**
     * @param categoryService
     */
    public void setCategoryService(final LeaCategoryService categoryService)
    {
        this.categoryService = categoryService;
    }

    @Override
    protected List<CategoryModel> getDataInternal(final CMSSiteModel siteModel)
    {
        String query = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("site", siteModel);
        List<SiteMapQueryModel> siteMapQueryList = findSiteMapQuery(getSiteMapPageEnum(), getSiteMapQueryGenericDao());
        if (CollectionUtils.isEmpty(siteMapQueryList))
        {
            query = DEFAULT_QUERY;
        }
        else
        {
            SiteMapQueryModel siteMapQueryModel = siteMapQueryList.iterator().next();
            query = siteMapQueryModel.getQuery();
            LOG.debug("Query from persistence - " + query);
            Map<String, String> searchParams = siteMapQueryModel.getQuerySearchParameter();
            if (MapUtils.isNotEmpty(searchParams))
            {
                params.putAll(searchParams);
                LOG.debug("Query params from persistence - " + params);
            }
        }
        List<CategoryModel> lstCategoryModels = doSearch(query, params, CategoryModel.class);

        /*
         * Calling this method to modify the category list to remove the categories that do not have any approved
         * product
         */
        if (categoryService != null)
        {
            lstCategoryModels = categoryService.findAllCategoriesWithApprovedProducts(lstCategoryModels);
        }

        return lstCategoryModels;
    }

    protected GenericDao<SiteMapQueryModel> getSiteMapQueryGenericDao()
    {
        return siteMapQueryGenericDao;
    }

    @Required
    public void setSiteMapQueryGenericDao(GenericDao<SiteMapQueryModel> siteMapQueryGenericDao)
    {
        this.siteMapQueryGenericDao = siteMapQueryGenericDao;
    }

}
