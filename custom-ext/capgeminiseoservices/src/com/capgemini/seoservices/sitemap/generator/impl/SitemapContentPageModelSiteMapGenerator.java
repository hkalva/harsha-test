/*
l•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.seoservices.sitemap.generator.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.capgemini.seoservices.model.SiteMapQueryModel;
import com.capgemini.seoservices.sitemap.generator.SiteMapQueryInterface;

import de.hybris.platform.acceleratorservices.sitemap.generator.impl.ContentPageModelSiteMapGenerator;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import de.hybris.platform.util.FlexibleSearchUtils;


/**
 * This class extends contentPageModelSiteMapGenerator method to include, pages based on includeInSiteMap boolean field.
 */
public class SitemapContentPageModelSiteMapGenerator extends ContentPageModelSiteMapGenerator implements SiteMapQueryInterface
{
    /**
     * the logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(SitemapContentPageModelSiteMapGenerator.class);

    /**
     * the dao.
     */
    private GenericDao<SiteMapQueryModel> siteMapQueryGenericDao;

    /*
     * To get sitemap content of ContentPage with additional attribute(includeInSiteMap), to provide more flexibility on
     * deciding which elements to include.
     */
    @Override
    protected List<ContentPageModel> getDataInternal(final CMSSiteModel siteModel)
    {
        String query = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("site", siteModel);
        params.put("catalogVersions", getCatalogVersionService().getSessionCatalogVersions());
        List<SiteMapQueryModel> siteMapQueryList = findSiteMapQuery(getSiteMapPageEnum(), getSiteMapQueryGenericDao());
        if (CollectionUtils.isEmpty(siteMapQueryList))
        {
            params.put(ContentPageModel.DEFAULTPAGE, Boolean.TRUE);
            params.put(ContentPageModel.INCLUDEINSITEMAP, Boolean.TRUE);
            final StringBuilder queryBuilder = new StringBuilder();
            queryBuilder
                    .append("SELECT {cp." + ContentPageModel.PK + "} FROM {" + ContentPageModel._TYPECODE + " AS cp } WHERE ");
            queryBuilder.append(FlexibleSearchUtils.buildOracleCompatibleCollectionStatement("{cp."
                    + ContentPageModel.CATALOGVERSION + "} in (?catalogVersions)", "catalogVersions", "OR",
                    getCatalogVersionService().getSessionCatalogVersions(), params));
            queryBuilder.append(" AND {cp." + ContentPageModel.DEFAULTPAGE + "}  = ?defaultPage");
            queryBuilder.append(" AND {cp." + ContentPageModel.INCLUDEINSITEMAP + "}  = ?includeInSiteMap");
            query = queryBuilder.toString();
        }
        else
        {
            SiteMapQueryModel siteMapQueryModel = siteMapQueryList.iterator().next();
            query = siteMapQueryModel.getQuery();
            LOG.debug("Query from persistence - " + query);
            Map<String, String> searchParams = siteMapQueryModel.getQuerySearchParameter();
            if (MapUtils.isNotEmpty(searchParams))
            {
                params.putAll(searchParams);
                LOG.debug("Query params from persistence - " + params);
            }
        }

        return doSearch(query, params, ContentPageModel.class);

    }

    protected GenericDao<SiteMapQueryModel> getSiteMapQueryGenericDao()
    {
        return siteMapQueryGenericDao;
    }

    @Required
    public void setSiteMapQueryGenericDao(GenericDao<SiteMapQueryModel> siteMapQueryGenericDao)
    {
        this.siteMapQueryGenericDao = siteMapQueryGenericDao;
    }

}
