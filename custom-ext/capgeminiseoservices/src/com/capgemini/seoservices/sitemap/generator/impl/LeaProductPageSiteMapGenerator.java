/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.capgemini.seoservices.sitemap.generator.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.capgemini.seoservices.model.SiteMapQueryModel;
import com.capgemini.seoservices.sitemap.generator.SiteMapQueryInterface;

import de.hybris.platform.acceleratorservices.sitemap.generator.impl.ProductPageSiteMapGenerator;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;


/**
 * Extending the OOTB class to override the query to restrict product restrictions
 */
public class LeaProductPageSiteMapGenerator extends ProductPageSiteMapGenerator implements SiteMapQueryInterface
{
    private static final Logger LOG = LoggerFactory.getLogger(LeaProductPageSiteMapGenerator.class);
    private static final String DEFAULT_QUERY = "SELECT {p.pk} FROM {Product AS p JOIN CatalogVersion AS cv ON {p.catalogVersion}={cv.pk} "
            + " JOIN Catalog AS cat ON {cv.pk}={cat.activeCatalogVersion} "
            + " JOIN CMSSite AS site ON {cat.pk}={site.defaultCatalog}}  WHERE {site.pk} = ?site"
            + " AND {p.approvalStatus} = ?approvalStatus"
            + " AND NOT exists ({{select {pr.pk} from {ProductsForRestriction as pr} where {pr.target} = {p.pk} }})";

    private GenericDao<SiteMapQueryModel> siteMapQueryGenericDao;

    @Override
    protected List<ProductModel> getDataInternal(final CMSSiteModel siteModel)
    {

        String query = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("site", siteModel);
        List<SiteMapQueryModel> siteMapQueryList = findSiteMapQuery(getSiteMapPageEnum(), getSiteMapQueryGenericDao());
        if (CollectionUtils.isEmpty(siteMapQueryList))
        {
            query = DEFAULT_QUERY;
            params.put("approvalStatus", ArticleApprovalStatus.APPROVED);
        }
        else
        {
            SiteMapQueryModel siteMapQueryModel = siteMapQueryList.iterator().next();
            query = siteMapQueryModel.getQuery();
            LOG.debug("Query from persistence - " + query);
            Map<String, String> searchParams = siteMapQueryModel.getQuerySearchParameter();
            if (MapUtils.isNotEmpty(searchParams))
            {
                params.putAll(searchParams);
                LOG.debug("Query params from persistence - " + params);
            }
        }
        return doSearch(query, params, ProductModel.class);
    }

    protected GenericDao<SiteMapQueryModel> getSiteMapQueryGenericDao()
    {
        return siteMapQueryGenericDao;
    }

    @Required
    public void setSiteMapQueryGenericDao(GenericDao<SiteMapQueryModel> siteMapQueryGenericDao)
    {
        this.siteMapQueryGenericDao = siteMapQueryGenericDao;
    }
}
