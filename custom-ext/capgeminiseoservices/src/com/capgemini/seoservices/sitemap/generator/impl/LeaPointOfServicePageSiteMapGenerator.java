/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.seoservices.sitemap.generator.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.capgemini.seoservices.model.SiteMapQueryModel;
import com.capgemini.seoservices.sitemap.generator.SiteMapQueryInterface;

import de.hybris.platform.acceleratorservices.sitemap.generator.impl.PointOfServicePageSiteMapGenerator;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import de.hybris.platform.storelocator.model.PointOfServiceModel;


/**
 * Class to override getDataInternal method to, only display list of stores, and not to include warehouses.
 */
public class LeaPointOfServicePageSiteMapGenerator extends PointOfServicePageSiteMapGenerator implements SiteMapQueryInterface
{
    private static final Logger LOG = LoggerFactory.getLogger(LeaPointOfServicePageSiteMapGenerator.class);
    private static final String DEFAULT_QUERY = "SELECT {ps.pk} FROM {PointOfService as ps} WHERE {ps.BaseStore} in (?stores) AND {ps.type}= (?storeType)";
    private GenericDao<SiteMapQueryModel> siteMapQueryGenericDao;

    @Override
    protected List<PointOfServiceModel> getDataInternal(final CMSSiteModel siteModel)
    {
        String query = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("site", siteModel);
        List<SiteMapQueryModel> siteMapQueryList = findSiteMapQuery(getSiteMapPageEnum(), getSiteMapQueryGenericDao());
        if (CollectionUtils.isEmpty(siteMapQueryList))
        {
            query = DEFAULT_QUERY;
            params.put("storeType", PointOfServiceTypeEnum.STORE);
        }
        else
        {
            SiteMapQueryModel siteMapQueryModel = siteMapQueryList.iterator().next();
            query = siteMapQueryModel.getQuery();
            LOG.debug("Query from persistence - " + query);
            Map<String, String> searchParams = siteMapQueryModel.getQuerySearchParameter();
            if (MapUtils.isNotEmpty(searchParams))
            {
                params.putAll(searchParams);
                LOG.debug("Query params from persistence - " + params);
            }
        }
        return doSearch(query, params, PointOfServiceModel.class);
    }

    protected GenericDao<SiteMapQueryModel> getSiteMapQueryGenericDao()
    {
        return siteMapQueryGenericDao;
    }

    @Required
    public void setSiteMapQueryGenericDao(GenericDao<SiteMapQueryModel> siteMapQueryGenericDao)
    {
        this.siteMapQueryGenericDao = siteMapQueryGenericDao;
    }
}
