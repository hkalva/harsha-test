/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.seoservices.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.hybris.platform.acceleratorcms.model.components.NavigationBarComponentModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;


/**
 * Dao for NavigationBarComponent.
 */
public class NavigationDaoImpl extends AbstractItemDao
{
    /** Logger for the class. */
    private static final Logger LOG = LoggerFactory.getLogger(NavigationDaoImpl.class);

    /**It finds all navigation bar links for category.
     * @param category - it is getting category from category model.
     * @return - it returns list of links.
     */
    public List<NavigationBarComponentModel> findAllNavigationBarLinksForGivingCategory(final CategoryModel category)
    {
        List<NavigationBarComponentModel> result = null;
        final Map<String, Object> params = new HashMap<String, Object>();

        final StringBuilder sb = new StringBuilder();
        sb.append(" SELECT {n.pk} ").append(" FROM { NavigationBarComponent as n ")
                .append(" join CmsLinkComponent as l on {n.link} = {l.pk} ")
                .append(" join Category as c on {l.category}={c.pk} ")
                .append(" join CatalogVersion as cv on {l.catalogVersion} = {cv.pk} ")
                .append(" join Catalog as cl on {cl.pk} = {cv.catalog} ").append("} ").append(" where {c.code}=?categoryCode ")
                .append(" and {cv.version}='Online' ");

        params.put("categoryCode", category.getCode());

        final SearchResult<NavigationBarComponentModel> searchResult = search(new FlexibleSearchQuery(sb.toString(), params));
        if (!(searchResult.getResult().isEmpty()))
        {
            result = searchResult.getResult();
        }
        else
        {
            if (LOG.isDebugEnabled())
            {
                LOG.debug("No Links for category " + category.getCode() + " was found");
            }
            result = new ArrayList<NavigationBarComponentModel>();
        }
        return result;
    }

    /**
     * To retrieve the CMSLinkComponentModel for the given navigation node. Find the
     *
     * @param navNode
     * @return CMSLinkComponentModel
     */
    public CMSLinkComponentModel findCMSLinkComponentForNavigationNode(final CMSNavigationNodeModel navNode)
    {
        CMSLinkComponentModel result = null;

        final Map<String, Object> params = new HashMap<String, Object>();

        final StringBuilder query = new StringBuilder();
        query.append("SELECT {l.pk} FROM { CmsLinkComponent as l ");
        query.append(" JOIN NavigationBarComponent as n on {n.link} = {l.pk} ");
        query.append(" JOIN CMSNavigationNode as node on {node.pk} = {n.navigationNode} ");
        query.append(" JOIN CatalogVersion as cv on {l.catalogVersion} = {cv.pk} ");
        query.append(" JOIN Catalog as cl on {cl.pk} = {cv.catalog} }");
        query.append(" WHERE {node.uid} = ?navNode and {cv.version}='Online' ");

        params.put("navNode", navNode.getUid());

        final SearchResult<CMSLinkComponentModel> searchResult = search(new FlexibleSearchQuery(query.toString(), params));

        if (!(searchResult.getResult().isEmpty()))
        {
            result = searchResult.getResult().get(0);
        }
        else
        {
            if (LOG.isDebugEnabled())
            {
                LOG.debug("No Links for Navigation node : " + navNode.getUid() + " was found");
            }
        }

        return result;
    }

}
