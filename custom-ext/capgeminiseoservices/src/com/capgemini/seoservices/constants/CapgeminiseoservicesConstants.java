package com.capgemini.seoservices.constants;

/**
 *
 * @author lyonscg
 *
 */
public class CapgeminiseoservicesConstants extends GeneratedCapgeminiseoservicesConstants
{
    /**
     * constant for extension name.
     */
	public static final String EXTENSIONNAME = "capgeminiseoservices";
	 /**
     * constant for sitemap date format.
     */
	public static final String SITEMAP_DATE_FORMAT = "YYY-MM-dd'T'hh:mmZ";
	
	/**
	 * the default private constructor.
	 */
	private CapgeminiseoservicesConstants()
	{
		//empty
	}
	
	
}
