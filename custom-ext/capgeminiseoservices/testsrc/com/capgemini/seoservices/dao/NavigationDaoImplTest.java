/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.seoservices.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.LoggerFactory;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorcms.model.components.NavigationBarComponentModel;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.user.UserManager;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;



/**
 * To Test the NavigationDaoImpl Class.
 */
@UnitTest
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(LogRunListener.class)
public class NavigationDaoImplTest extends ServicelayerTransactionalTest
{
	private final Logger LOG = LoggerFactory.getLogger(NavigationDaoImplTest.class);
	@InjectMocks
	private final NavigationDaoImpl navigationDaoImpl = new NavigationDaoImpl();
	@Mock
	private CategoryModel categoryModel1, categoryModel2;
	@Mock
	private CMSNavigationNodeModel navNode1, navNode2;
	@Resource
	private FlexibleSearchService flexibleSearchService;
	@Resource
	private ModelService modelService;

	private static final String CAT_CODE_1 = "1360";
	private static final String CAT_CODE_2 = "1806";

	private static final String NAV_NODE_UID_1 = "SafetyNavNode";
	private static final String NAV_NODE_UID_2 = "FootwearLinksNavNode";

	/**
	 * Load SAmple DAta with CMSLinkComponents and NavigationBarComponent.
	 */
	@Before
	public void setUp() throws Exception
	{
		//To load the Core data such as Languages before importing Catalog
		createCoreData();

		LOG.info("Creating test catalog ..");
		JaloSession.getCurrentSession().setUser(UserManager.getInstance().getAdminEmployee());
		final long startTime = System.currentTimeMillis();

		flexibleSearchService = (FlexibleSearchService) Registry.getApplicationContext().getBean("flexibleSearchService");
		assertNotNull(flexibleSearchService);
		modelService = (ModelService) Registry.getApplicationContext().getBean("modelService");
		assertNotNull(modelService);

		importCsv("/capgeminiseoservices/test/seoTestContentCatalog.csv", "UTF-8");

		final CatalogModel catalog = (CatalogModel) flexibleSearchService
				.search("SELECT {PK} FROM {Catalog} WHERE {id}='testContentCatalog'").getResult().get(0);
		assertNotNull(catalog);
		final CatalogVersionModel version = (CatalogVersionModel) flexibleSearchService
				.search("SELECT {PK} FROM {CatalogVersion} WHERE {version}='Online' AND {catalog}=?catalog",
						Collections.singletonMap("catalog", catalog)).getResult().get(0);
		assertNotNull(version);

		JaloSession.getCurrentSession().getSessionContext()
				.setAttribute("catalogversions", modelService.toPersistenceLayer(Collections.singletonList(version)));

		final CategoryModel category = (CategoryModel) flexibleSearchService
				.search("SELECT {PK} FROM {Category} WHERE {code}='1360'").getResult().get(0);
		assertNotNull(category);

		LOG.info("Finished creating test catalog in " + (System.currentTimeMillis() - startTime) + "ms");

		MockitoAnnotations.initMocks(this);

		navigationDaoImpl.setFlexibleSearchService(flexibleSearchService);
		navigationDaoImpl.setModelService(modelService);

		given(categoryModel1.getCode()).willReturn(CAT_CODE_1);
		given(categoryModel2.getCode()).willReturn(CAT_CODE_2);
		given(navNode1.getUid()).willReturn(NAV_NODE_UID_1);
		given(navNode2.getUid()).willReturn(NAV_NODE_UID_2);

	}

	/**
	 * TO test that the method returns the NavigationBarComponents for the category that is passed. And expecting 1.
	 * component
	 */
	@Test
	public void testFindAllNavigationBarLinksForGivingCategory()
	{
		final List<NavigationBarComponentModel> actualLst = navigationDaoImpl
				.findAllNavigationBarLinksForGivingCategory(categoryModel1);
		assertNotNull(actualLst);
		assertEquals(1, actualLst.size());

	}

	/**
	 * To test that the method returns empty list when the Category is not associated with NavigationBarComponent.
	 */
	@Test
	public void testFindAllNavigationBarLinksForGivingCategoryWithNoBarComponent()
	{
		final List<NavigationBarComponentModel> actualLst = navigationDaoImpl
				.findAllNavigationBarLinksForGivingCategory(categoryModel2);
		assertNotNull(actualLst);
		assertEquals(0, actualLst.size());
	}

	/**
	 * To test if the method returns the CMSLinkComponent associated to a NavigationNode from its NavigationBarComponent.
	 */
	@Test
	public void testFindCMSLinkComponentForNavigationNode()
	{
		LOG.info("Nav Node :" + navNode1.getUid());
		final CMSLinkComponentModel actualCMSComponent = navigationDaoImpl.findCMSLinkComponentForNavigationNode(navNode1);
		LOG.info("actualCMSComponent :" + actualCMSComponent);
		assertNotNull(actualCMSComponent);
	}

	/**
	 * To test if the method returns null if the NavigationNode passed is not associated with NavigationBarComponent.
	 */
	@Test
	public void testFindCMSLinkComponentForNavigationNodeWithNoBarComponent()
	{
		LOG.info("Nav Node :" + navNode2.getUid());
		final CMSLinkComponentModel actualCMSComponent = navigationDaoImpl.findCMSLinkComponentForNavigationNode(navNode2);
		LOG.info("actualCMSComponent :" + actualCMSComponent);
		assertNull(actualCMSComponent);
	}


}
