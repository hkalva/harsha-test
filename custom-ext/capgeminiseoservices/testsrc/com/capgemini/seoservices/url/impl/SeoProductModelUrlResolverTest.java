/**
 *
 */
package com.capgemini.seoservices.url.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.threadcontext.impl.DefaultThreadContextService;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.Config;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.capgemini.seoservices.url.impl.SeoCategoryModelUrlResolver;
import com.capgemini.seoservices.url.impl.SeoProductModelUrlResolver;


/**@author lyonscg.
 *unit test for Seo Product model Url resolver class.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(Config.class)
@UnitTest
public class SeoProductModelUrlResolverTest
{
	private final SeoProductModelUrlResolver lyonsB2BProductModelUrlResolver = new SeoProductModelUrlResolver();

	private static final String CAT_PATH_PATTERN = "/{category-path}";
	private static final String PRD_NAME_PATTERN = "/{product-name}";
	private static final String PRD_CODE_PATTERN = "/{product-code}";
	private static final String URL_PATTERN = CAT_PATH_PATTERN + PRD_NAME_PATTERN + "/p" + PRD_CODE_PATTERN;

	private static final String LEAF_CAT_NAME = "leafcategoryName";
	private static final String PRODUCT_NAME = "productName";
	private static final String PRODUCT_NAME_WITH_SPACE = "product Name";
	private static final String PRODUCT_NAME_RETURNED = "productname";
	private static final String PRODUCT_NAME_WITH_SPACE_RETURNED = "product-name";
	private static final String PRODUCT_CODE = "productCode";

	private static final String BRAND_CAT_NAME = "brand1";
	private static final String BRAND_CAT_CODE = "brand_123";

	private static final String ROOT_CAT_PATH = "rootPath";
	private static final String MID_CAT_PATH = ROOT_CAT_PATH + "/middlePath";
	private static final String LEAF_CAT_PATH = MID_CAT_PATH + "/" + LEAF_CAT_NAME;

	private BaseSiteModel baseSite;

	@Mock
	private CategoryModel middleCategory, rootCategory, leafCategory, rootCategoryBrandFirst, rootCategoryBrandSecond;
	@Mock
	private ProductModel product, productWithSpaceInName, productBrandFirst, productBrandSecond;
	@Mock
	private BaseSiteService baseSiteService;
	@Mock
	private CommerceCategoryService commerceCategoryService;
	@Mock
	private SeoCategoryModelUrlResolver lyonsB2BCategoryModelUrlResolver;

	private List<CategoryModel> leafCategoryPath;
	private List<CategoryModel> midCategoryPath;
	private List<CategoryModel> rootCategoryPath;

	private List<CategoryModel> categoryPathBrandFirst;
	private List<CategoryModel> categoryPathBrandSecond;

	@Mock
	private CategoryModel brandCategory;

	@SuppressWarnings("javadoc")
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		baseSite = new BaseSiteModel();
		baseSite.setUid("baseSiteUid");

		lyonsB2BProductModelUrlResolver.setDefaultPattern(URL_PATTERN);
		lyonsB2BProductModelUrlResolver.setBaseSiteService(baseSiteService);
		lyonsB2BProductModelUrlResolver.setCommerceCategoryService(commerceCategoryService);

		given(baseSiteService.getCurrentBaseSite()).willReturn(baseSite);

		given(rootCategory.getUseInCategoryPathUrl()).willReturn(Boolean.FALSE);
		given(rootCategory.getCode()).willReturn("root_123");
		given(rootCategory.getSupercategories()).willReturn(Collections.<CategoryModel> emptyList());
		given(rootCategory.getAllSubcategories()).willReturn(Collections.singletonList(middleCategory));

		given(middleCategory.getUseInCategoryPathUrl()).willReturn(Boolean.FALSE);
		given(middleCategory.getCode()).willReturn("middle_123");
		given(middleCategory.getSupercategories()).willReturn(Collections.singletonList(rootCategory));
		given(middleCategory.getAllSubcategories()).willReturn(Collections.singletonList(leafCategory));

		given(leafCategory.getName()).willReturn(LEAF_CAT_NAME);
		given(leafCategory.getCode()).willReturn("leaf_123");
		given(leafCategory.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);
		given(leafCategory.getSupercategories()).willReturn(leafCategoryPath);
		given(leafCategory.getAllSubcategories()).willReturn(Collections.<CategoryModel> emptyList());

		given(brandCategory.getName()).willReturn(BRAND_CAT_NAME);
		given(brandCategory.getCode()).willReturn(BRAND_CAT_CODE);
		given(brandCategory.getUseInCategoryPathUrl()).willReturn(Boolean.FALSE);
		given(brandCategory.getSupercategories()).willReturn(Collections.<CategoryModel> emptyList());
		given(brandCategory.getAllSubcategories()).willReturn(Collections.<CategoryModel> emptyList());

		final CategoryModel[] subCategories =
		{ rootCategory, middleCategory, leafCategory };
		leafCategoryPath = Arrays.asList(subCategories);

		final CategoryModel[] categories =
		{ rootCategory, middleCategory };
		midCategoryPath = Arrays.asList(categories);

		final CategoryModel[] superCategories =
		{ rootCategory };
		rootCategoryPath = Arrays.asList(superCategories);

		final CategoryModel[] brandFirstCategories =
		{ brandCategory, rootCategory };
		categoryPathBrandFirst = Arrays.asList(brandFirstCategories);

		final CategoryModel[] brandSecondCategories =
		{ rootCategory, brandCategory };
		categoryPathBrandSecond = Arrays.asList(brandSecondCategories);

		given(commerceCategoryService.getPathsForCategory(leafCategory)).willReturn(Collections.singletonList(leafCategoryPath));
		given(commerceCategoryService.getPathsForCategory(middleCategory)).willReturn(Collections.singletonList(midCategoryPath));
		given(commerceCategoryService.getPathsForCategory(rootCategory)).willReturn(Collections.singletonList(rootCategoryPath));

		given(product.getSupercategories()).willReturn(leafCategoryPath);
		given(product.getName()).willReturn(PRODUCT_NAME);
		given(product.getName(Locale.ENGLISH)).willReturn(PRODUCT_NAME);
		given(product.getCode()).willReturn(PRODUCT_CODE);
		given(product.getPk()).willReturn(PK.BIG_PK);

		given(productWithSpaceInName.getSupercategories()).willReturn(leafCategoryPath);
		given(productWithSpaceInName.getName()).willReturn(PRODUCT_NAME_WITH_SPACE);
		given(productWithSpaceInName.getName(Locale.ENGLISH)).willReturn(PRODUCT_NAME_WITH_SPACE);
		given(productWithSpaceInName.getCode()).willReturn(PRODUCT_CODE);
		given(productWithSpaceInName.getPk()).willReturn(PK.BIG_PK);

		given(productBrandFirst.getSupercategories()).willReturn(categoryPathBrandFirst);
		given(productBrandFirst.getName()).willReturn(PRODUCT_NAME);
		given(productBrandFirst.getCode()).willReturn(PRODUCT_CODE);
		given(productBrandFirst.getPk()).willReturn(PK.BIG_PK);

		given(productBrandSecond.getSupercategories()).willReturn(categoryPathBrandSecond);
		given(productBrandSecond.getName()).willReturn(PRODUCT_NAME);
		given(productBrandSecond.getCode()).willReturn(PRODUCT_CODE);
		given(productBrandSecond.getPk()).willReturn(PK.BIG_PK);

		given(lyonsB2BCategoryModelUrlResolver.getSeoCategoryPath(rootCategory)).willReturn(ROOT_CAT_PATH);
		given(lyonsB2BCategoryModelUrlResolver.getSeoCategoryPath(middleCategory)).willReturn(MID_CAT_PATH);
		given(lyonsB2BCategoryModelUrlResolver.getSeoCategoryPath(leafCategory)).willReturn(LEAF_CAT_PATH);
	}

	@SuppressWarnings("javadoc")
	@Test
	public void testResolve()
	{
		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("false");
		lyonsB2BProductModelUrlResolver.setThreadContextService(new DefaultThreadContextService());
		lyonsB2BProductModelUrlResolver.setCategoryModelUrlResolver(lyonsB2BCategoryModelUrlResolver);
		Assert.assertEquals("/" + LEAF_CAT_PATH + "/" + PRODUCT_NAME_RETURNED + "/p/" + PRODUCT_CODE,
				lyonsB2BProductModelUrlResolver.resolve(product));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testProductNameWithSpace()
	{
		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("false");
		lyonsB2BProductModelUrlResolver.setThreadContextService(new DefaultThreadContextService());
		lyonsB2BProductModelUrlResolver.setCategoryModelUrlResolver(lyonsB2BCategoryModelUrlResolver);
		Assert.assertEquals("/" + LEAF_CAT_PATH + "/" + PRODUCT_NAME_WITH_SPACE_RETURNED + "/p/" + PRODUCT_CODE,
				lyonsB2BProductModelUrlResolver.resolve(productWithSpaceInName));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testGetPrimaryCategoryForProduct()
	{
		lyonsB2BProductModelUrlResolver.setCategoryModelUrlResolver(lyonsB2BCategoryModelUrlResolver);
		Assert.assertEquals(leafCategory, lyonsB2BProductModelUrlResolver.getPrimaryCategoryForProduct(product));
	}

	@SuppressWarnings("javadoc")
	@Test
	public void testGetPrimaryCategoryForProductBrandFirst()
	{
		lyonsB2BProductModelUrlResolver.setCategoryModelUrlResolver(lyonsB2BCategoryModelUrlResolver);
		Assert.assertEquals(rootCategory, lyonsB2BProductModelUrlResolver.getPrimaryCategoryForProduct(productBrandFirst));
	}

	@SuppressWarnings("javadoc")
	@Test
	public void testGetPrimaryCategoryForProductBrandSecond()
	{
		lyonsB2BProductModelUrlResolver.setCategoryModelUrlResolver(lyonsB2BCategoryModelUrlResolver);
		Assert.assertEquals(rootCategory, lyonsB2BProductModelUrlResolver.getPrimaryCategoryForProduct(productBrandSecond));
	}

	@SuppressWarnings("javadoc")
	@Test
	public void testResolveInternal()
	{
		lyonsB2BProductModelUrlResolver.setCategoryModelUrlResolver(lyonsB2BCategoryModelUrlResolver);
		Assert.assertEquals("/" + LEAF_CAT_PATH + "/" + PRODUCT_NAME_RETURNED + "/p/" + PRODUCT_CODE,
				lyonsB2BProductModelUrlResolver.resolveInternal(product));
	}
}
