/**
 *
 */
package com.capgemini.seoservices.url.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorcms.model.components.NavigationBarComponentModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.servicelayer.model.ItemModelContextImpl;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.capgemini.seoservices.dao.NavigationDaoImpl;
import com.capgemini.seoservices.url.impl.SeoCategoryModelUrlResolver;


/**@author lyonscg.
 *Test class for SEO category model Url resolver class.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(Config.class)
@UnitTest
public class SeoCategoryModelUrlResolverTest
{
	private static final String LEAF_CAT_NAME = "leafcategoryName";
	private static final String LEAF_CAT_NAME_GERMAN = "leafcategoryNameGerman";

	private static final String LEAF_CAT_CODE = "leafCategoryCode";
	private static final String MID_CAT_CODE = "midCategoryCode";
	private static final String ROOT_CAT_CODE = "rootCategoryCode";

	private static final String CAT_PATH_PATTERN = "{category-path}";
	private static final String CAT_CODE_PATTERN = "{category-code}";
	private static final String URL_PATTERN = "/" + CAT_PATH_PATTERN + "/c/" + CAT_CODE_PATTERN;
	private static final String URL_PATTERN_NO_CAT_PATH = "/c/" + CAT_CODE_PATTERN;

	private static final String ENGLISH_TITLE = "englishTitle";
	private static final String ENGLISH_TITLE_RETURNED = "englishtitle";
	private static final String ENGLISH_TITLE_WITH_TAB = "english  Title";
	private static final String ENGLISH_TITLE_WITH_TAB_RETURNED = "english-title";
	private static final String ENGLISH_TITLE_WITH_2_SPACES = "english  Title";
	private static final String ENGLISH_TITLE_WITH_2_SPACES_RETURNED = "english-title";

	private static final String ENGLISH_TITLE_WITH_AMP = "english&Title";
	private static final String ENGLISH_TITLE_WITH_AMP_RETURNED = "englishtitle";
	private static final String ENGLISH_TITLE_WITH_PLUS = "english+Title";
	private static final String ENGLISH_TITLE_WITH_PLUS_RETURNED = "englishtitle";
	private static final String ENGLISH_TITLE_WITH_SINGLE_QUOTE = "english'Title";
	private static final String ENGLISH_TITLE_WITH_SINGLE_QUOTE_RETURNED = "englishtitle";
	private static final String ENGLISH_TITLE_WITH_1_DOUBLE_QUOTE = "english\"Title";
	private static final String ENGLISH_TITLE_WITH_1_DOUBLE_QUOTE_RETURNED = "englishtitle";
	private static final String ENGLISH_TITLE_WITH_2_DOUBLE_QUOTE = "english\"Title\"";
	private static final String ENGLISH_TITLE_WITH_2_DOUBLE_QUOTE_RETURNED = "englishtitle";

	private static final String GERMAN_TITLE = "germanTitle";
	private static final String NAV_NODE_LVL_1_ENGLISH_TITLE = "Level1 english Title";
	private static final String NAV_NODE_LVL_1_ENGLISH_TITLE_1_DASHDASH = "Level1--english Title";
	private static final String NAV_NODE_LVL_1_ENGLISH_TITLE_2_DASHDASH = "Level1--english--Title";
	private static final String NAV_NODE_LVL_1_ENGLISH_TITLE_3_DASHES = "Level1---english Title";
	private static final String NAV_NODE_LVL_2_ENGLISH_TITLE = "Level2 english Title";
	private static final String NAV_NODE_LVL_3_ENGLISH_TITLE = "Level3 english Title";

	private static final String NAV_NODE_LVL_1_ENGLISH_TITLE_RETURNED = "level1-english-title";
	private static final String NAV_NODE_LVL_2_ENGLISH_TITLE_RETURNED = "level2-english-title";
	private static final String NAV_NODE_LVL_3_ENGLISH_TITLE_RETURNED = "level3-english-title";

	private static final String NAV_NODE_LVL_1_GERMAN_TITLE = "werkzeuge";
	private static final String NAV_NODE_LVL_2_GERMAN_TITLE = "Level2 German Title";
	private static final String NAV_NODE_LVL_3_GERMAN_TITLE = "Level3 German Title";

	private static final String CATEGORY_LINK_NAME_ENGLISH = "EnglishLinkName";
	private static final String CATEGORY_LINK_NAME_ENGLISH_RETURNED = "englishlinkname";
	private static final String CATEGORY_LINK_NAME_GERMAN = "GermanLinkName";
	private final SeoCategoryModelUrlResolver lyonsB2BCategoryModelUrlResolver = new SeoCategoryModelUrlResolver();
	@Mock
	private BaseSiteService baseSiteService;
	@Mock
	private CommerceCategoryService commerceCategoryService;
	private CategoryModel middleCategory, rootCategory;
	@Mock
	private CategoryModel leafCategory;
	@Mock
	private NavigationDaoImpl navigationDaoResults;
	@Mock
	private NavigationDaoImpl navigationDaoNoResults;
	@Mock
	private ItemModelContextImpl itemModelContextImpl;
	@Mock
	NavigationBarComponentModel navBarModel;
	@Mock
	CMSNavigationNodeModel navNodeModel;
	@Mock
	CMSNavigationNodeModel navNodeModelLvl3;
	@Mock
	CMSNavigationNodeModel navNodeModelLvl2;
	@Mock
	CMSNavigationNodeModel navNodeModelLvl1;
	@Mock
	CMSLinkComponentModel categoryLink;

	private CatalogVersionModel catalogVersion;
	private CatalogModel catalog;
	private BaseSiteModel baseSite;
	private List<CategoryModel> leafCategoryPath;
	private List<CategoryModel> midCategoryPath;
	private List<CategoryModel> rootCategoryPath;
	private final Locale localeEnglish = new Locale("en");
	private final Locale localeGerman = new Locale("de");

	@SuppressWarnings("javadoc")
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		baseSite = new BaseSiteModel();
		baseSite.setUid("baseSiteUid");
		lyonsB2BCategoryModelUrlResolver.setPattern(URL_PATTERN);
		lyonsB2BCategoryModelUrlResolver.setBaseSiteService(baseSiteService);
		lyonsB2BCategoryModelUrlResolver.setCommerceCategoryService(commerceCategoryService);
		given(baseSiteService.getCurrentBaseSite()).willReturn(baseSite);

		catalogVersion = new CatalogVersionModel();
		catalog = new CatalogModel();
		catalog.setId("catalogId");
		catalogVersion.setCatalog(catalog);
		catalogVersion.setVersion("Online");

		rootCategory = new CategoryModel();
		rootCategory.setCode(ROOT_CAT_CODE);
		rootCategory.setCatalogVersion(catalogVersion);
		rootCategory.setUseInCategoryPathUrl(Boolean.FALSE);

		middleCategory = new CategoryModel();
		middleCategory.setCode(MID_CAT_CODE);
		middleCategory.setCatalogVersion(catalogVersion);
		middleCategory.setUseInCategoryPathUrl(Boolean.FALSE);
		middleCategory.setSupercategories(Collections.singletonList(rootCategory));

		given(leafCategory.getName()).willReturn(LEAF_CAT_NAME);
		given(leafCategory.getName(Locale.ENGLISH)).willReturn(LEAF_CAT_NAME);
		given(leafCategory.getName(Locale.GERMAN)).willReturn(LEAF_CAT_NAME_GERMAN);
		given(leafCategory.getCode()).willReturn(LEAF_CAT_CODE);
		given(leafCategory.getCatalogVersion()).willReturn(catalogVersion);
		given(leafCategory.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);
		given(leafCategory.getSupercategories()).willReturn(Collections.singletonList(middleCategory));

		final CategoryModel[] subCategories =
		{ rootCategory, middleCategory, leafCategory };
		leafCategoryPath = Arrays.asList(subCategories);
		given(commerceCategoryService.getPathsForCategory(leafCategory)).willReturn(Collections.singletonList(leafCategoryPath));

		final CategoryModel[] categories =
		{ rootCategory, middleCategory };
		midCategoryPath = Arrays.asList(categories);
		given(commerceCategoryService.getPathsForCategory(middleCategory)).willReturn(Collections.singletonList(midCategoryPath));

		final CategoryModel[] superCategories =
		{ rootCategory };
		rootCategoryPath = Arrays.asList(superCategories);
		given(commerceCategoryService.getPathsForCategory(rootCategory)).willReturn(Collections.singletonList(rootCategoryPath));

		PowerMockito.when(navigationDaoNoResults.findAllNavigationBarLinksForGivingCategory(rootCategory)).thenReturn(null);
		PowerMockito.when(navigationDaoNoResults.findAllNavigationBarLinksForGivingCategory(middleCategory)).thenReturn(null);
		PowerMockito.when(navigationDaoNoResults.findAllNavigationBarLinksForGivingCategory(leafCategory)).thenReturn(null);

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseCategoryStructure()
	{
		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("false");

		Assert.assertEquals(
				URL_PATTERN_NO_CAT_PATH.replace(CAT_PATH_PATTERN, StringUtils.EMPTY).replace(CAT_CODE_PATTERN, ROOT_CAT_CODE),
				lyonsB2BCategoryModelUrlResolver.resolveInternal(rootCategory));
		Assert.assertEquals(
				URL_PATTERN_NO_CAT_PATH.replace(CAT_PATH_PATTERN, StringUtils.EMPTY).replace(CAT_CODE_PATTERN, MID_CAT_CODE),
				lyonsB2BCategoryModelUrlResolver.resolveInternal(middleCategory));
		Assert.assertEquals(
				URL_PATTERN.replace(CAT_PATH_PATTERN, StringUtils.lowerCase(LEAF_CAT_NAME)).replace(CAT_CODE_PATTERN, LEAF_CAT_CODE),
				lyonsB2BCategoryModelUrlResolver.resolveInternal(leafCategory));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseCategoryStructureNonEnglish()
	{
		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("false");

		Assert.assertEquals(
				URL_PATTERN_NO_CAT_PATH.replace(CAT_PATH_PATTERN, StringUtils.EMPTY).replace(CAT_CODE_PATTERN, ROOT_CAT_CODE),
				lyonsB2BCategoryModelUrlResolver.resolveInternal(rootCategory));
		Assert.assertEquals(
				URL_PATTERN_NO_CAT_PATH.replace(CAT_PATH_PATTERN, StringUtils.EMPTY).replace(CAT_CODE_PATTERN, MID_CAT_CODE),
				lyonsB2BCategoryModelUrlResolver.resolveInternal(middleCategory));
		Assert.assertEquals(
				URL_PATTERN.replace(CAT_PATH_PATTERN, StringUtils.lowerCase(LEAF_CAT_NAME)).replace(CAT_CODE_PATTERN, LEAF_CAT_CODE),
				lyonsB2BCategoryModelUrlResolver.resolveInternal(leafCategory));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseNavigaionNodesWithNoneSet()
	{
		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("true");
		lyonsB2BCategoryModelUrlResolver.setNavigationDao(navigationDaoNoResults);

		Assert.assertEquals(
				URL_PATTERN_NO_CAT_PATH.replace(CAT_PATH_PATTERN, StringUtils.EMPTY).replace(CAT_CODE_PATTERN, ROOT_CAT_CODE),
				lyonsB2BCategoryModelUrlResolver.resolveInternal(rootCategory));
		Assert.assertEquals(
				URL_PATTERN_NO_CAT_PATH.replace(CAT_PATH_PATTERN, StringUtils.EMPTY).replace(CAT_CODE_PATTERN, MID_CAT_CODE),
				lyonsB2BCategoryModelUrlResolver.resolveInternal(middleCategory));
		Assert.assertEquals(
				URL_PATTERN.replace(CAT_PATH_PATTERN, StringUtils.lowerCase(LEAF_CAT_NAME)).replace(CAT_CODE_PATTERN, LEAF_CAT_CODE),
				lyonsB2BCategoryModelUrlResolver.resolveInternal(leafCategory));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseNavigaionNodesWithDirectLinkEnglish()
	{
		final List<NavigationBarComponentModel> navBarLinksRoot = new ArrayList<NavigationBarComponentModel>();

		given(navNodeModel.getTitle(localeEnglish)).willReturn(ENGLISH_TITLE);
		given(navBarModel.getNavigationNode()).willReturn(navNodeModel);

		navBarLinksRoot.add(navBarModel);

		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("true");
		PowerMockito.when(navigationDaoResults.findAllNavigationBarLinksForGivingCategory(rootCategory))
				.thenReturn(navBarLinksRoot);

		lyonsB2BCategoryModelUrlResolver.setNavigationDao(navigationDaoResults);

		Assert.assertEquals(URL_PATTERN.replace(CAT_PATH_PATTERN, ENGLISH_TITLE_RETURNED).replace(CAT_CODE_PATTERN, ROOT_CAT_CODE),
				lyonsB2BCategoryModelUrlResolver.resolveInternal(rootCategory));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseNavigaionNodesWithDirectLinkEnglishWithTab()
	{
		final List<NavigationBarComponentModel> navBarLinksRoot = new ArrayList<NavigationBarComponentModel>();

		given(navNodeModel.getTitle(localeEnglish)).willReturn(ENGLISH_TITLE_WITH_TAB);
		given(navBarModel.getNavigationNode()).willReturn(navNodeModel);

		navBarLinksRoot.add(navBarModel);

		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("true");
		PowerMockito.when(navigationDaoResults.findAllNavigationBarLinksForGivingCategory(rootCategory))
				.thenReturn(navBarLinksRoot);

		lyonsB2BCategoryModelUrlResolver.setNavigationDao(navigationDaoResults);

		Assert.assertEquals(
				URL_PATTERN.replace(CAT_PATH_PATTERN, ENGLISH_TITLE_WITH_TAB_RETURNED).replace(CAT_CODE_PATTERN, ROOT_CAT_CODE),
				lyonsB2BCategoryModelUrlResolver.resolveInternal(rootCategory));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseNavigaionNodesWithDirectLinkEnglishWith2Spaces()
	{
		final List<NavigationBarComponentModel> navBarLinksRoot = new ArrayList<NavigationBarComponentModel>();

		given(navNodeModel.getTitle(localeEnglish)).willReturn(ENGLISH_TITLE_WITH_2_SPACES);
		given(navBarModel.getNavigationNode()).willReturn(navNodeModel);

		navBarLinksRoot.add(navBarModel);

		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("true");
		PowerMockito.when(navigationDaoResults.findAllNavigationBarLinksForGivingCategory(rootCategory))
				.thenReturn(navBarLinksRoot);

		lyonsB2BCategoryModelUrlResolver.setNavigationDao(navigationDaoResults);

		Assert.assertEquals(
				URL_PATTERN.replace(CAT_PATH_PATTERN, ENGLISH_TITLE_WITH_2_SPACES_RETURNED).replace(CAT_CODE_PATTERN, ROOT_CAT_CODE),
				lyonsB2BCategoryModelUrlResolver.resolveInternal(rootCategory));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseNavigaionNodesWithDirectLinkGerman()
	{
		final List<NavigationBarComponentModel> navBarLinksRoot = new ArrayList<NavigationBarComponentModel>();

		given(navNodeModel.getTitle(localeGerman)).willReturn(GERMAN_TITLE);
		given(navNodeModel.getTitle(localeEnglish)).willReturn(ENGLISH_TITLE);
		given(navBarModel.getNavigationNode()).willReturn(navNodeModel);
		navBarLinksRoot.add(navBarModel);

		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("true");
		PowerMockito.when(navigationDaoResults.findAllNavigationBarLinksForGivingCategory(rootCategory))
				.thenReturn(navBarLinksRoot);

		lyonsB2BCategoryModelUrlResolver.setNavigationDao(navigationDaoResults);

		Assert.assertEquals(URL_PATTERN.replace(CAT_PATH_PATTERN, ENGLISH_TITLE_RETURNED).replace(CAT_CODE_PATTERN, ROOT_CAT_CODE),
				lyonsB2BCategoryModelUrlResolver.resolveInternal(rootCategory));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseNavigaionNodesWithHierarchyUseAllEnglish()
	{
		final List<CMSLinkComponentModel> navNodeModelList = new ArrayList<CMSLinkComponentModel>();
		final List<CMSNavigationNodeModel> navigationNodes = new ArrayList<CMSNavigationNodeModel>();

		navNodeModelList.add(categoryLink);
		navigationNodes.add(navNodeModelLvl3);

		given(leafCategory.getLinkComponents()).willReturn(navNodeModelList);
		given(categoryLink.getNavigationNodes()).willReturn(navigationNodes);
		given(categoryLink.getLinkName(localeEnglish)).willReturn(CATEGORY_LINK_NAME_ENGLISH);
		given(navNodeModelLvl3.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_3_ENGLISH_TITLE);
		given(navNodeModelLvl2.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_2_ENGLISH_TITLE);
		given(navNodeModelLvl1.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_1_ENGLISH_TITLE);

		given(navNodeModelLvl3.getParent()).willReturn(navNodeModelLvl2);
		given(navNodeModelLvl2.getParent()).willReturn(navNodeModelLvl1);
		given(navNodeModelLvl1.getParent()).willReturn(null);

		given(navNodeModelLvl3.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);
		given(navNodeModelLvl2.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);
		given(navNodeModelLvl1.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);

		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("true");
		PowerMockito.when(navigationDaoResults.findAllNavigationBarLinksForGivingCategory(leafCategory)).thenReturn(null);

		lyonsB2BCategoryModelUrlResolver.setNavigationDao(navigationDaoResults);

		Assert.assertEquals(
				URL_PATTERN.replace(
						CAT_PATH_PATTERN,
						NAV_NODE_LVL_1_ENGLISH_TITLE_RETURNED + "/" + NAV_NODE_LVL_2_ENGLISH_TITLE_RETURNED + "/"
								+ NAV_NODE_LVL_3_ENGLISH_TITLE_RETURNED + "/" + CATEGORY_LINK_NAME_ENGLISH_RETURNED).replace(
						CAT_CODE_PATTERN, LEAF_CAT_CODE), lyonsB2BCategoryModelUrlResolver.resolveInternal(leafCategory));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseNavigaionNodesWithHierarchyUseAllEnglish1Dash()
	{
		final List<CMSLinkComponentModel> navNodeModelList = new ArrayList<CMSLinkComponentModel>();
		final List<CMSNavigationNodeModel> navigationNodes = new ArrayList<CMSNavigationNodeModel>();

		navNodeModelList.add(categoryLink);
		navigationNodes.add(navNodeModelLvl3);

		given(leafCategory.getLinkComponents()).willReturn(navNodeModelList);
		given(categoryLink.getNavigationNodes()).willReturn(navigationNodes);
		given(categoryLink.getLinkName(localeEnglish)).willReturn(CATEGORY_LINK_NAME_ENGLISH);
		given(navNodeModelLvl3.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_3_ENGLISH_TITLE);
		given(navNodeModelLvl2.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_2_ENGLISH_TITLE);
		given(navNodeModelLvl1.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_1_ENGLISH_TITLE_1_DASHDASH);

		given(navNodeModelLvl3.getParent()).willReturn(navNodeModelLvl2);
		given(navNodeModelLvl2.getParent()).willReturn(navNodeModelLvl1);
		given(navNodeModelLvl1.getParent()).willReturn(null);

		given(navNodeModelLvl3.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);
		given(navNodeModelLvl2.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);
		given(navNodeModelLvl1.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);

		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("true");
		PowerMockito.when(navigationDaoResults.findAllNavigationBarLinksForGivingCategory(leafCategory)).thenReturn(null);

		lyonsB2BCategoryModelUrlResolver.setNavigationDao(navigationDaoResults);

		Assert.assertEquals(
				URL_PATTERN.replace(
						CAT_PATH_PATTERN,
						NAV_NODE_LVL_1_ENGLISH_TITLE_RETURNED + "/" + NAV_NODE_LVL_2_ENGLISH_TITLE_RETURNED + "/"
								+ NAV_NODE_LVL_3_ENGLISH_TITLE_RETURNED + "/" + CATEGORY_LINK_NAME_ENGLISH_RETURNED).replace(
						CAT_CODE_PATTERN, LEAF_CAT_CODE), lyonsB2BCategoryModelUrlResolver.resolveInternal(leafCategory));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseNavigaionNodesWithHierarchyUseAllEnglish2Dashes()
	{
		final List<CMSLinkComponentModel> navNodeModelList = new ArrayList<CMSLinkComponentModel>();
		final List<CMSNavigationNodeModel> navigationNodes = new ArrayList<CMSNavigationNodeModel>();

		navNodeModelList.add(categoryLink);
		navigationNodes.add(navNodeModelLvl3);

		given(leafCategory.getLinkComponents()).willReturn(navNodeModelList);
		given(categoryLink.getNavigationNodes()).willReturn(navigationNodes);
		given(categoryLink.getLinkName(localeEnglish)).willReturn(CATEGORY_LINK_NAME_ENGLISH);
		given(navNodeModelLvl3.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_3_ENGLISH_TITLE);
		given(navNodeModelLvl2.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_2_ENGLISH_TITLE);
		given(navNodeModelLvl1.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_1_ENGLISH_TITLE_2_DASHDASH);

		given(navNodeModelLvl3.getParent()).willReturn(navNodeModelLvl2);
		given(navNodeModelLvl2.getParent()).willReturn(navNodeModelLvl1);
		given(navNodeModelLvl1.getParent()).willReturn(null);

		given(navNodeModelLvl3.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);
		given(navNodeModelLvl2.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);
		given(navNodeModelLvl1.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);

		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("true");
		PowerMockito.when(navigationDaoResults.findAllNavigationBarLinksForGivingCategory(leafCategory)).thenReturn(null);

		lyonsB2BCategoryModelUrlResolver.setNavigationDao(navigationDaoResults);

		Assert.assertEquals(
				URL_PATTERN.replace(
						CAT_PATH_PATTERN,
						NAV_NODE_LVL_1_ENGLISH_TITLE_RETURNED + "/" + NAV_NODE_LVL_2_ENGLISH_TITLE_RETURNED + "/"
								+ NAV_NODE_LVL_3_ENGLISH_TITLE_RETURNED + "/" + CATEGORY_LINK_NAME_ENGLISH_RETURNED).replace(
						CAT_CODE_PATTERN, LEAF_CAT_CODE), lyonsB2BCategoryModelUrlResolver.resolveInternal(leafCategory));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseNavigaionNodesWithHierarchyUseAllEnglish3Dashes()
	{
		final List<CMSLinkComponentModel> navNodeModelList = new ArrayList<CMSLinkComponentModel>();
		final List<CMSNavigationNodeModel> navigationNodes = new ArrayList<CMSNavigationNodeModel>();

		navNodeModelList.add(categoryLink);
		navigationNodes.add(navNodeModelLvl3);

		given(leafCategory.getLinkComponents()).willReturn(navNodeModelList);
		given(categoryLink.getNavigationNodes()).willReturn(navigationNodes);
		given(categoryLink.getLinkName(localeEnglish)).willReturn(CATEGORY_LINK_NAME_ENGLISH);
		given(navNodeModelLvl3.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_3_ENGLISH_TITLE);
		given(navNodeModelLvl2.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_2_ENGLISH_TITLE);
		given(navNodeModelLvl1.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_1_ENGLISH_TITLE_3_DASHES);

		given(navNodeModelLvl3.getParent()).willReturn(navNodeModelLvl2);
		given(navNodeModelLvl2.getParent()).willReturn(navNodeModelLvl1);
		given(navNodeModelLvl1.getParent()).willReturn(null);

		given(navNodeModelLvl3.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);
		given(navNodeModelLvl2.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);
		given(navNodeModelLvl1.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);

		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("true");
		PowerMockito.when(navigationDaoResults.findAllNavigationBarLinksForGivingCategory(leafCategory)).thenReturn(null);

		lyonsB2BCategoryModelUrlResolver.setNavigationDao(navigationDaoResults);

		Assert.assertEquals(
				URL_PATTERN.replace(
						CAT_PATH_PATTERN,
						NAV_NODE_LVL_1_ENGLISH_TITLE_RETURNED + "/" + NAV_NODE_LVL_2_ENGLISH_TITLE_RETURNED + "/"
								+ NAV_NODE_LVL_3_ENGLISH_TITLE_RETURNED + "/" + CATEGORY_LINK_NAME_ENGLISH_RETURNED).replace(
						CAT_CODE_PATTERN, LEAF_CAT_CODE), lyonsB2BCategoryModelUrlResolver.resolveInternal(leafCategory));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseNavigaionNodesWithHierarchyNotLevel1English()
	{
		final List<CMSLinkComponentModel> navNodeModelList = new ArrayList<CMSLinkComponentModel>();
		final List<CMSNavigationNodeModel> navigationNodes = new ArrayList<CMSNavigationNodeModel>();

		navNodeModelList.add(categoryLink);
		navigationNodes.add(navNodeModelLvl3);

		given(leafCategory.getLinkComponents()).willReturn(navNodeModelList);
		given(categoryLink.getNavigationNodes()).willReturn(navigationNodes);
		given(categoryLink.getLinkName(localeEnglish)).willReturn(CATEGORY_LINK_NAME_ENGLISH);
		given(navNodeModelLvl3.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_3_ENGLISH_TITLE);
		given(navNodeModelLvl2.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_2_ENGLISH_TITLE);
		given(navNodeModelLvl1.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_1_ENGLISH_TITLE);

		given(navNodeModelLvl3.getParent()).willReturn(navNodeModelLvl2);
		given(navNodeModelLvl2.getParent()).willReturn(navNodeModelLvl1);
		given(navNodeModelLvl1.getParent()).willReturn(null);

		given(navNodeModelLvl3.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);
		given(navNodeModelLvl2.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);
		given(navNodeModelLvl1.getUseInCategoryPathUrl()).willReturn(Boolean.FALSE);

		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("true");
		PowerMockito.when(navigationDaoResults.findAllNavigationBarLinksForGivingCategory(leafCategory)).thenReturn(null);

		lyonsB2BCategoryModelUrlResolver.setNavigationDao(navigationDaoResults);

		Assert.assertEquals(
				URL_PATTERN.replace(
						CAT_PATH_PATTERN,
						NAV_NODE_LVL_2_ENGLISH_TITLE_RETURNED + "/" + NAV_NODE_LVL_3_ENGLISH_TITLE_RETURNED + "/"
								+ CATEGORY_LINK_NAME_ENGLISH_RETURNED).replace(CAT_CODE_PATTERN, LEAF_CAT_CODE),
				lyonsB2BCategoryModelUrlResolver.resolveInternal(leafCategory));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseNavigaionNodesWithHierarchyUseAllGerman()
	{
		final List<CMSLinkComponentModel> navNodeModelList = new ArrayList<CMSLinkComponentModel>();
		final List<CMSNavigationNodeModel> navigationNodes = new ArrayList<CMSNavigationNodeModel>();

		navNodeModelList.add(categoryLink);
		navigationNodes.add(navNodeModelLvl3);

		given(leafCategory.getLinkComponents()).willReturn(navNodeModelList);
		given(categoryLink.getNavigationNodes()).willReturn(navigationNodes);
		given(categoryLink.getLinkName(localeGerman)).willReturn(CATEGORY_LINK_NAME_GERMAN);
		given(navNodeModelLvl3.getTitle(localeGerman)).willReturn(NAV_NODE_LVL_3_GERMAN_TITLE);
		given(navNodeModelLvl2.getTitle(localeGerman)).willReturn(NAV_NODE_LVL_2_GERMAN_TITLE);
		given(navNodeModelLvl1.getTitle(localeGerman)).willReturn(NAV_NODE_LVL_1_GERMAN_TITLE);
		given(categoryLink.getLinkName(localeEnglish)).willReturn(CATEGORY_LINK_NAME_ENGLISH);
		given(navNodeModelLvl3.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_3_ENGLISH_TITLE);
		given(navNodeModelLvl2.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_2_ENGLISH_TITLE);
		given(navNodeModelLvl1.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_1_ENGLISH_TITLE);

		given(navNodeModelLvl3.getParent()).willReturn(navNodeModelLvl2);
		given(navNodeModelLvl2.getParent()).willReturn(navNodeModelLvl1);
		given(navNodeModelLvl1.getParent()).willReturn(null);

		given(navNodeModelLvl3.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);
		given(navNodeModelLvl2.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);
		given(navNodeModelLvl1.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);

		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("true");
		PowerMockito.when(navigationDaoResults.findAllNavigationBarLinksForGivingCategory(leafCategory)).thenReturn(null);

		lyonsB2BCategoryModelUrlResolver.setNavigationDao(navigationDaoResults);

		Assert.assertEquals(
				URL_PATTERN.replace(
						CAT_PATH_PATTERN,
						NAV_NODE_LVL_1_ENGLISH_TITLE_RETURNED + "/" + NAV_NODE_LVL_2_ENGLISH_TITLE_RETURNED + "/"
								+ NAV_NODE_LVL_3_ENGLISH_TITLE_RETURNED + "/" + CATEGORY_LINK_NAME_ENGLISH_RETURNED).replace(
						CAT_CODE_PATTERN, LEAF_CAT_CODE), lyonsB2BCategoryModelUrlResolver.resolveInternal(leafCategory));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseNavigaionNodesWithHierarchyNotLevel1German()
	{
		final List<CMSLinkComponentModel> navNodeModelList = new ArrayList<CMSLinkComponentModel>();
		final List<CMSNavigationNodeModel> navigationNodes = new ArrayList<CMSNavigationNodeModel>();

		navNodeModelList.add(categoryLink);
		navigationNodes.add(navNodeModelLvl3);

		given(leafCategory.getLinkComponents()).willReturn(navNodeModelList);
		given(categoryLink.getNavigationNodes()).willReturn(navigationNodes);
		given(categoryLink.getLinkName(localeGerman)).willReturn(CATEGORY_LINK_NAME_GERMAN);
		given(navNodeModelLvl3.getTitle(localeGerman)).willReturn(NAV_NODE_LVL_3_GERMAN_TITLE);
		given(navNodeModelLvl2.getTitle(localeGerman)).willReturn(NAV_NODE_LVL_2_GERMAN_TITLE);
		given(navNodeModelLvl1.getTitle(localeGerman)).willReturn(NAV_NODE_LVL_1_GERMAN_TITLE);
		given(categoryLink.getLinkName(localeEnglish)).willReturn(CATEGORY_LINK_NAME_ENGLISH);
		given(navNodeModelLvl3.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_3_ENGLISH_TITLE);
		given(navNodeModelLvl2.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_2_ENGLISH_TITLE);
		given(navNodeModelLvl1.getTitle(localeEnglish)).willReturn(NAV_NODE_LVL_1_ENGLISH_TITLE);

		given(navNodeModelLvl3.getParent()).willReturn(navNodeModelLvl2);
		given(navNodeModelLvl2.getParent()).willReturn(navNodeModelLvl1);
		given(navNodeModelLvl1.getParent()).willReturn(null);

		given(navNodeModelLvl3.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);
		given(navNodeModelLvl2.getUseInCategoryPathUrl()).willReturn(Boolean.TRUE);
		given(navNodeModelLvl1.getUseInCategoryPathUrl()).willReturn(Boolean.FALSE);

		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("true");
		PowerMockito.when(navigationDaoResults.findAllNavigationBarLinksForGivingCategory(leafCategory)).thenReturn(null);

		lyonsB2BCategoryModelUrlResolver.setNavigationDao(navigationDaoResults);

		Assert.assertEquals(
				URL_PATTERN.replace(
						CAT_PATH_PATTERN,
						NAV_NODE_LVL_2_ENGLISH_TITLE_RETURNED + "/" + NAV_NODE_LVL_3_ENGLISH_TITLE_RETURNED + "/"
								+ CATEGORY_LINK_NAME_ENGLISH_RETURNED).replace(CAT_CODE_PATTERN, LEAF_CAT_CODE),
				lyonsB2BCategoryModelUrlResolver.resolveInternal(leafCategory));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testBuildPathString()
	{
		Assert.assertEquals(StringUtils.EMPTY, lyonsB2BCategoryModelUrlResolver.buildPathString(rootCategoryPath));
		Assert.assertEquals(StringUtils.EMPTY, lyonsB2BCategoryModelUrlResolver.buildPathString(midCategoryPath));
		Assert.assertEquals(LEAF_CAT_NAME, lyonsB2BCategoryModelUrlResolver.buildPathString(leafCategoryPath));
	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseNavigaionNodesWithDirectLinkEnglishWithAmp()
	{
		final List<NavigationBarComponentModel> navBarLinksRoot = new ArrayList<NavigationBarComponentModel>();

		given(navNodeModel.getTitle(localeEnglish)).willReturn(ENGLISH_TITLE_WITH_AMP);
		given(navBarModel.getNavigationNode()).willReturn(navNodeModel);

		navBarLinksRoot.add(navBarModel);

		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("true");
		PowerMockito.when(navigationDaoResults.findAllNavigationBarLinksForGivingCategory(rootCategory))
				.thenReturn(navBarLinksRoot);

		lyonsB2BCategoryModelUrlResolver.setNavigationDao(navigationDaoResults);

		Assert.assertEquals(
				URL_PATTERN.replace(CAT_PATH_PATTERN, ENGLISH_TITLE_WITH_AMP_RETURNED).replace(CAT_CODE_PATTERN, ROOT_CAT_CODE),
				lyonsB2BCategoryModelUrlResolver.resolveInternal(rootCategory));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseNavigaionNodesWithDirectLinkEnglishWithPlus()
	{
		final List<NavigationBarComponentModel> navBarLinksRoot = new ArrayList<NavigationBarComponentModel>();

		given(navNodeModel.getTitle(localeEnglish)).willReturn(ENGLISH_TITLE_WITH_PLUS);
		given(navBarModel.getNavigationNode()).willReturn(navNodeModel);

		navBarLinksRoot.add(navBarModel);

		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("true");
		PowerMockito.when(navigationDaoResults.findAllNavigationBarLinksForGivingCategory(rootCategory))
				.thenReturn(navBarLinksRoot);

		lyonsB2BCategoryModelUrlResolver.setNavigationDao(navigationDaoResults);

		Assert.assertEquals(
				URL_PATTERN.replace(CAT_PATH_PATTERN, ENGLISH_TITLE_WITH_PLUS_RETURNED).replace(CAT_CODE_PATTERN, ROOT_CAT_CODE),
				lyonsB2BCategoryModelUrlResolver.resolveInternal(rootCategory));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseNavigaionNodesWithDirectLinkEnglishWithSingleQuote()
	{
		final List<NavigationBarComponentModel> navBarLinksRoot = new ArrayList<NavigationBarComponentModel>();

		given(navNodeModel.getTitle(localeEnglish)).willReturn(ENGLISH_TITLE_WITH_SINGLE_QUOTE);
		given(navBarModel.getNavigationNode()).willReturn(navNodeModel);

		navBarLinksRoot.add(navBarModel);

		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("true");
		PowerMockito.when(navigationDaoResults.findAllNavigationBarLinksForGivingCategory(rootCategory))
				.thenReturn(navBarLinksRoot);

		lyonsB2BCategoryModelUrlResolver.setNavigationDao(navigationDaoResults);

		Assert.assertEquals(
				URL_PATTERN.replace(CAT_PATH_PATTERN, ENGLISH_TITLE_WITH_SINGLE_QUOTE_RETURNED).replace(CAT_CODE_PATTERN,
						ROOT_CAT_CODE), lyonsB2BCategoryModelUrlResolver.resolveInternal(rootCategory));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseNavigaionNodesWithDirectLinkEnglishWith1DoubleQuote()
	{
		final List<NavigationBarComponentModel> navBarLinksRoot = new ArrayList<NavigationBarComponentModel>();

		given(navNodeModel.getTitle(localeEnglish)).willReturn(ENGLISH_TITLE_WITH_1_DOUBLE_QUOTE);
		given(navBarModel.getNavigationNode()).willReturn(navNodeModel);

		navBarLinksRoot.add(navBarModel);

		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("true");
		PowerMockito.when(navigationDaoResults.findAllNavigationBarLinksForGivingCategory(rootCategory))
				.thenReturn(navBarLinksRoot);

		lyonsB2BCategoryModelUrlResolver.setNavigationDao(navigationDaoResults);

		Assert.assertEquals(
				URL_PATTERN.replace(CAT_PATH_PATTERN, ENGLISH_TITLE_WITH_1_DOUBLE_QUOTE_RETURNED).replace(CAT_CODE_PATTERN,
						ROOT_CAT_CODE), lyonsB2BCategoryModelUrlResolver.resolveInternal(rootCategory));

	}

	@SuppressWarnings("javadoc")
	@Test
	public void testCategoryUrlUseNavigaionNodesWithDirectLinkEnglishWith2DoubleQuotes()
	{
		final List<NavigationBarComponentModel> navBarLinksRoot = new ArrayList<NavigationBarComponentModel>();

		given(navNodeModel.getTitle(localeEnglish)).willReturn(ENGLISH_TITLE_WITH_2_DOUBLE_QUOTE);
		given(navBarModel.getNavigationNode()).willReturn(navNodeModel);

		navBarLinksRoot.add(navBarModel);

		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getParameter("use.navigation.node.based.path")).thenReturn("true");
		PowerMockito.when(navigationDaoResults.findAllNavigationBarLinksForGivingCategory(rootCategory))
				.thenReturn(navBarLinksRoot);

		lyonsB2BCategoryModelUrlResolver.setNavigationDao(navigationDaoResults);

		Assert.assertEquals(
				URL_PATTERN.replace(CAT_PATH_PATTERN, ENGLISH_TITLE_WITH_2_DOUBLE_QUOTE_RETURNED).replace(CAT_CODE_PATTERN,
						ROOT_CAT_CODE), lyonsB2BCategoryModelUrlResolver.resolveInternal(rootCategory));

	}
}
