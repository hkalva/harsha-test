package com.lyonscg.rapidsetup.dataimport.impl;

import com.lyonscg.rapidsetup.service.RapidSetupResourceLoader;
import com.lyonscg.rapidsetup.setup.RapidSetupImpexService;
import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.core.initialization.SystemSetupContext;
import org.springframework.beans.factory.annotation.Required;

public class RapidSetupSampleDataImportService extends SampleDataImportService 
{
    private final static String FORCE_IMPORT = "forceImport";

    private RapidSetupImpexService rapidSetupImpexService;
    private RapidSetupResourceLoader rapidSetupResourceLoader;

    protected void importContentCatalog(final String extensionName, final String contentCatalogName, final boolean forceImport)
    {
        if(!getConfigurationService().getConfiguration().getBoolean(extensionName + ".rapidsetup", false))
        {
            super.importContentCatalog(extensionName, contentCatalogName);
        }
        else
        {

            getRapidSetupResourceLoader().findResourcePaths(String.format("/%s/import/sampledata/contentCatalogs/%sContentCatalog/pre", extensionName,
                    contentCatalogName)).forEach(file -> {
                getRapidSetupImpexService().importImpexFile(file, false, forceImport, extensionName);
            });

            getRapidSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/contentCatalogs/%sContentCatalog/templates.impex", extensionName,
                    contentCatalogName), false, forceImport, extensionName);

            getRapidSetupResourceLoader().findResourcePaths(String.format("/%s/import/sampledata/contentCatalogs/%sContentCatalog/pages", extensionName,
                    contentCatalogName)).forEach(file -> {
                getRapidSetupImpexService().importImpexFile(file, false, forceImport, extensionName);
            });

            getRapidSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/contentCatalogs/%sContentCatalog/header.impex", extensionName,
                    contentCatalogName), false, forceImport, extensionName);

            getRapidSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/contentCatalogs/%sContentCatalog/footer.impex", extensionName,
                    contentCatalogName), false, forceImport, extensionName);

            getRapidSetupResourceLoader().findResourcePaths(String.format("/%s/import/sampledata/contentCatalogs/%sContentCatalog/post", extensionName,
                    contentCatalogName)).forEach(file -> {
                getRapidSetupImpexService().importImpexFile(file, false, forceImport, extensionName);
            });

            getRapidSetupImpexService().importImpexFile(String.format("/%s/import/coredata/contentCatalogs/%sContentCatalog/email-content.impex", extensionName,
                    contentCatalogName), false, forceImport, extensionName);
        }

    }

    protected void importStore(final String extensionName, final String storeName, final String productCatalogName, final boolean forceImport)
    {
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/stores/%s/points-of-service-media.impex", extensionName, storeName), false, forceImport, extensionName);
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/stores/%s/points-of-service.impex", extensionName, storeName), false, forceImport, extensionName);

        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/stores/%s/warehouses.impex", extensionName, storeName), false, forceImport, extensionName);

        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/reviews.impex", extensionName,
                        productCatalogName), false, forceImport, extensionName);

        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/stores/%s/promotions.impex", extensionName, storeName), false, forceImport, extensionName);

        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/stores/%s/consents.impex", extensionName, storeName), false, forceImport, extensionName);
    }

    protected void importJobs(final String extensionName, final String storeName, final boolean forceImport)
    {
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/stores/%s/jobs.impex", extensionName, storeName), false, forceImport, extensionName);
    }

    protected void importSolrIndex(final String extensionName, final String storeName, final boolean forceImport)
    {
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/stores/%s/solr.impex", extensionName, storeName), false, forceImport, extensionName);

        getSetupSolrIndexerService().createSolrIndexerCronJobs(String.format("%sIndex", storeName));
    }

    protected void importProductCatalog(final String extensionName, final String productCatalogName, final boolean forceImport)
    {
        // Load Units
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/classifications-units.impex", extensionName,
                        productCatalogName), false, forceImport, extensionName);

        // Load Categories
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/categories.impex", extensionName,
                        productCatalogName), false, forceImport, extensionName);

        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/categories-classifications.impex",
                        extensionName, productCatalogName), false, forceImport, extensionName);

        // Load Suppliers
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/suppliers.impex", extensionName,
                        productCatalogName), false, forceImport, extensionName);
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/suppliers-media.impex", extensionName,
                        productCatalogName), false, forceImport, extensionName);

        // Load medias for Categories as Suppliers loads some new Categories
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/categories-media.impex", extensionName,
                        productCatalogName), false, forceImport, extensionName);

        // Load Products
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/products.impex", extensionName,
                        productCatalogName), false, forceImport, extensionName);
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/products-media.impex", extensionName,
                        productCatalogName), false, forceImport, extensionName);
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/products-classifications.impex", extensionName,
                        productCatalogName), false, forceImport, extensionName);

        // Load Products Relations
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/products-relations.impex", extensionName,
                        productCatalogName), false, forceImport, extensionName);

        // Load Products Fixes
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/products-fixup.impex", extensionName,
                        productCatalogName), false, forceImport, extensionName);

        // Load Prices
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/products-prices.impex", extensionName,
                        productCatalogName), false, forceImport, extensionName);

        // Load Stock Levels
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/products-stocklevels.impex", extensionName,
                        productCatalogName), false, forceImport, extensionName);
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/products-pos-stocklevels.impex", extensionName,
                        productCatalogName), false, forceImport, extensionName);

        // Load Taxes
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/products-tax.impex", extensionName,
                        productCatalogName), false, forceImport, extensionName);

        // Load Multi-Dimensial Products
        importMultiDProductCatalog(extensionName, productCatalogName, forceImport);

    }

    protected void importMultiDProductCatalog(final String extensionName, final String productCatalogName, final boolean forceImport)
    {
        // Load Multi-Dimension Categories
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/multi-d/dimension-categories.impex",
                        extensionName, productCatalogName), false, forceImport, extensionName);
        // Load Multi-Dimension Products
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/multi-d/dimension-products.impex",
                        extensionName, productCatalogName), false, forceImport, extensionName);
        // Load Multi-Dimension Products-Media
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/multi-d/dimension-products-media.impex",
                        extensionName, productCatalogName), false, forceImport, extensionName);
        // Load Multi-Dimension Products-Prices
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/multi-d/dimension-products-prices.impex",
                        extensionName, productCatalogName), false, forceImport, extensionName);
        // Load Multi-Dimension Products-Stocklevels
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/multi-d/dimension-products-stock-levels.impex",
                        extensionName, productCatalogName), false, forceImport, extensionName);
        // Load Multi-Dimension Products-Stocklevels
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/multi-d/dimension-products-tax.impex",
                        extensionName, productCatalogName), false, forceImport, extensionName);
        // Load Multi-Dimension Products-pos-stocklevels
        getRapidSetupImpexService().importImpexFile(
                String.format(
                        "/%s/import/sampledata/productCatalogs/%sProductCatalog/multi-d/dimension-products-pos-stocklevels.impex",
                        extensionName, productCatalogName), false, forceImport, extensionName);
        // Load Multi-Dimension Products-classifications
        getRapidSetupImpexService().importImpexFile(
                String.format(
                        "/%s/import/sampledata/productCatalogs/%sProductCatalog/multi-d/dimension-products-classifications.impex",
                        extensionName, productCatalogName), false, forceImport, extensionName);
        // Load future stock for multi -D products
        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/products-futurestock.impex", extensionName,
                        productCatalogName), false, forceImport, extensionName);
    }

    protected void importCommonData(final String extensionName, final boolean forceImport)
    {

        if (isExtensionLoaded(CUSTOMER_SUPPORT_BACKOFFICE_EXTENSION_NAME)
                || isExtensionLoaded(ORDER_MANAGEMENT_BACKOFFICE_EXTENSION_NAME))
        {
            getRapidSetupImpexService().importImpexFile(
                    String.format("/%s/import/sampledata/backoffice/customersupport/customersupport-groups.impex", extensionName),
                    false, forceImport, extensionName);

            getRapidSetupImpexService().importImpexFile(
                    String.format("/%s/import/sampledata/backoffice/customersupport/customersupport-users.impex", extensionName),
                    false, forceImport, extensionName);
            getRapidSetupImpexService()
                    .importImpexFile(
                            String.format("/%s/import/sampledata/backoffice/customersupport/customersupport-savedqueries.impex",
                                    extensionName), false, forceImport, extensionName);
            getRapidSetupImpexService()
                    .importImpexFile(
                            String.format("/%s/import/sampledata/backoffice/customersupport/customersupport-accessrights.impex",
                                    extensionName), false, forceImport, extensionName);
            getRapidSetupImpexService()
                    .importImpexFile(
                            String.format("/%s/import/sampledata/backoffice/customersupport/customersupport-restrictions.impex",
                                    extensionName), false, forceImport, extensionName);
        }

        if (isExtensionLoaded(CUSTOMER_SUPPORT_BACKOFFICE_EXTENSION_NAME) && isExtensionLoaded(ASSISTED_SERVICE_EXTENSION_NAME))
        {
            getRapidSetupImpexService().importImpexFile(
                    String.format("/%s/import/sampledata/backoffice/customersupport/customersupport-assistedservice-groups.impex",
                            extensionName), false, forceImport, extensionName);
        }
    }

    protected void importAllData(final AbstractSystemSetup systemSetup, final SystemSetupContext context,
                                 final ImportData importData, final boolean syncCatalogs)
    {
        if(!getConfigurationService().getConfiguration().getBoolean(context.getExtensionName() + ".rapidsetup", false))
        {
            super.importAllData(systemSetup, context, importData, syncCatalogs);
            return;
        }

        final boolean forceImport = systemSetup.getBooleanSystemSetupParameter(context, FORCE_IMPORT);

        systemSetup.logInfo(context, String.format("Begin importing common data for [%s]", context.getExtensionName()));
        importCommonData(context.getExtensionName(), forceImport);

        systemSetup.logInfo(context,
                String.format("Begin importing product catalog data for [%s]", importData.getProductCatalogName()));
        importProductCatalog(context.getExtensionName(), importData.getProductCatalogName(), forceImport);

        for (final String contentCatalogName : importData.getContentCatalogNames())
        {
            systemSetup.logInfo(context, String.format("Begin importing content catalog data for [%s]", contentCatalogName));
            importContentCatalog(context.getExtensionName(), contentCatalogName, forceImport);
        }

        synchronizeProductCatalog(systemSetup, context, importData.getProductCatalogName(), false);
        for (final String contentCatalog : importData.getContentCatalogNames())
        {
            synchronizeContentCatalog(systemSetup, context, contentCatalog, false);
        }
        assignDependent(importData.getProductCatalogName(), importData.getContentCatalogNames());

        if (syncCatalogs)
        {
            systemSetup
                    .logInfo(context, String.format("Synchronizing product catalog for [%s]", importData.getProductCatalogName()));
            final boolean productSyncSuccess = synchronizeProductCatalog(systemSetup, context, importData.getProductCatalogName(),
                    true);

            for (final String contentCatalogName : importData.getContentCatalogNames())
            {
                systemSetup.logInfo(context, String.format("Synchronizing content catalog for [%s]", contentCatalogName));
                synchronizeContentCatalog(systemSetup, context, contentCatalogName, true);
            }

            if (!productSyncSuccess)
            {
                // Rerun the product sync if required
                systemSetup.logInfo(context,
                        String.format("Rerunning product catalog synchronization for [%s]", importData.getProductCatalogName()));
                if (!synchronizeProductCatalog(systemSetup, context, importData.getProductCatalogName(), true))
                {
                    systemSetup.logInfo(context, String.format(
                            "Rerunning product catalog synchronization for [%s], failed. Please consult logs for more details.",
                            importData.getProductCatalogName()));
                }
            }
        }

        for (final String storeName : importData.getStoreNames())
        {
            systemSetup.logInfo(context, String.format("Begin importing store data for [%s]", storeName));
            importStore(context.getExtensionName(), storeName, importData.getProductCatalogName(), forceImport);

            systemSetup.logInfo(context, String.format("Begin importing job data for [%s]", storeName));
            importJobs(context.getExtensionName(), storeName, forceImport);

            systemSetup.logInfo(context, String.format("Begin importing solr index data for [%s]", storeName));
            importSolrIndex(context.getExtensionName(), storeName, forceImport);

            if (systemSetup.getBooleanSystemSetupParameter(context, ACTIVATE_SOLR_CRON_JOBS))
            {
                systemSetup.logInfo(context, String.format("Activating solr index for [%s]", storeName));
                runSolrIndex(context.getExtensionName(), storeName);
            }
        }
    }

    public RapidSetupImpexService getRapidSetupImpexService()
    {
        return rapidSetupImpexService;
    }

    @Required
    public void setRapidSetupImpexService(final RapidSetupImpexService rapidSetupImpexService)
    {
        this.rapidSetupImpexService = rapidSetupImpexService;
    }

    public RapidSetupResourceLoader getRapidSetupResourceLoader()
    {
        return rapidSetupResourceLoader;
    }

    @Required
    public void setRapidSetupResourceLoader(final RapidSetupResourceLoader rapidSetupResourceLoader)
    {
        this.rapidSetupResourceLoader = rapidSetupResourceLoader;
    }
}
