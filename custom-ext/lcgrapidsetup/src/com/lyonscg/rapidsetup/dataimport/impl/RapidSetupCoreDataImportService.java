package com.lyonscg.rapidsetup.dataimport.impl;

import com.lyonscg.rapidsetup.service.RapidSetupResourceLoader;
import com.lyonscg.rapidsetup.setup.RapidSetupImpexService;
import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.core.initialization.SystemSetupContext;
import org.springframework.beans.factory.annotation.Required;

/**
 * Extends CoreDataImportService. Overrides importAllData method so it calls custom importContentCatalog method. This
 * custom method optionally uses the RapidSetupImpexService if rapidsetup is enabled for the extension.
 *
 * @author lyonscg
 */
public class RapidSetupCoreDataImportService extends CoreDataImportService
{
    private final static String FORCE_IMPORT = "forceImport";

    private RapidSetupImpexService rapidSetupImpexService;
    private RapidSetupResourceLoader rapidSetupResourceLoader;

    protected void importContentCatalog(final String extensionName, final String contentCatalogName, final boolean forceImport)
    {
        if(!getConfigurationService().getConfiguration().getBoolean(extensionName + ".rapidsetup", false))
        {
            super.importContentCatalog(extensionName, contentCatalogName);
        }
        else
        {
            getRapidSetupImpexService().importImpexFile(String.format("/%s/import/coredata/contentCatalogs/%sContentCatalog/catalog.impex", extensionName,
                    contentCatalogName), false, forceImport, extensionName);

            getRapidSetupResourceLoader().findResourcePaths(String.format("/%s/import/coredata/contentCatalogs/%sContentCatalog/pre", extensionName,
                    contentCatalogName)).forEach(file -> {
                getRapidSetupImpexService().importImpexFile(file, false, forceImport, extensionName);
            });

            getRapidSetupImpexService().importImpexFile(String.format("/%s/import/coredata/contentCatalogs/%sContentCatalog/templates.impex", extensionName,
                    contentCatalogName), false, forceImport, extensionName);

            getRapidSetupResourceLoader().findResourcePaths(String.format("/%s/import/coredata/contentCatalogs/%sContentCatalog/pages", extensionName,
                    contentCatalogName)).forEach(file -> {
                getRapidSetupImpexService().importImpexFile(file, false, forceImport, extensionName);
            });

            getRapidSetupImpexService().importImpexFile(String.format("/%s/import/coredata/contentCatalogs/%sContentCatalog/header.impex", extensionName,
                    contentCatalogName), false, forceImport, extensionName);

            getRapidSetupImpexService().importImpexFile(String.format("/%s/import/coredata/contentCatalogs/%sContentCatalog/footer.impex", extensionName,
                    contentCatalogName), false, forceImport, extensionName);

            getRapidSetupResourceLoader().findResourcePaths(String.format("/%s/import/coredata/contentCatalogs/%sContentCatalog/post", extensionName,
                    contentCatalogName)).forEach(file -> {
                getRapidSetupImpexService().importImpexFile(file, false, forceImport, extensionName);
            });

            getRapidSetupImpexService().importImpexFile(String.format("/%s/import/coredata/contentCatalogs/%sContentCatalog/email-content.impex", extensionName,
                    contentCatalogName), false, forceImport, extensionName);
        }

    }

    protected void importCommonData(final String extensionName, final boolean forceImport)
    {
        if(!getConfigurationService().getConfiguration().getBoolean(extensionName + ".rapidsetup", false))
        {
            super.importCommonData(extensionName);
            return;
        }

        getRapidSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/essential-data.impex", extensionName),
                true, forceImport, extensionName);
        getRapidSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/countries.impex", extensionName), false, forceImport, extensionName);
        getRapidSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/user-groups.impex", extensionName), false, forceImport, extensionName);
        getRapidSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/delivery-modes.impex", extensionName),
                false, forceImport, extensionName);
        getRapidSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/themes.impex", extensionName), false, forceImport, extensionName);
        getRapidSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/promotions.impex", extensionName), false, forceImport, extensionName);
    }

    protected void importProductCatalog(final String extensionName, final String productCatalogName, final boolean forceImport)
    {
        if(!getConfigurationService().getConfiguration().getBoolean(extensionName + ".rapidsetup", false))
        {
            super.importProductCatalog(extensionName, productCatalogName);
            return;
        }

        getRapidSetupImpexService()
                .importImpexFile(
                        String.format("/%s/import/coredata/productCatalogs/%sProductCatalog/catalog.impex", extensionName,
                                productCatalogName), false, forceImport, extensionName);
    }


    protected void importSolrIndex(final String extensionName, final String storeName, final boolean forceImport)
    {
        if(!getConfigurationService().getConfiguration().getBoolean(extensionName + ".rapidsetup", false))
        {
            super.importContentCatalog(extensionName, storeName);
            return;
        }

        getRapidSetupImpexService().importImpexFile(String.format("/%s/import/coredata/stores/%s/solr.impex", extensionName, storeName),
                false, forceImport, extensionName);

        getSetupSolrIndexerService().createSolrIndexerCronJobs(String.format("%sIndex", storeName));

        getRapidSetupImpexService().importImpexFile(
                String.format("/%s/import/coredata/stores/%s/solrtrigger.impex", extensionName, storeName), false, forceImport, extensionName);
    }

    protected void importJobs(final String extensionName, final String storeName, final boolean forceImport)
    {
        if(!getConfigurationService().getConfiguration().getBoolean(extensionName + ".rapidsetup", false))
        {
            super.importJobs(extensionName, storeName);
            return;
        }

        getRapidSetupImpexService().importImpexFile(String.format("/%s/import/coredata/stores/%s/jobs.impex", extensionName, storeName),
                false, forceImport, extensionName);
    }

    protected void importStore(final String extensionName, final String storeName, final String productCatalogName, final boolean forceImport)
    {
        if(!getConfigurationService().getConfiguration().getBoolean(extensionName + ".rapidsetup", false))
        {
            super.importStore(extensionName, storeName, productCatalogName);
            return;
        }

        final String responsiveStoreFile = String.format("/%s/import/coredata/stores/%s/store-responsive.impex", extensionName,
                storeName);
        final String responsiveSiteFile = String.format("/%s/import/coredata/stores/%s/site-responsive.impex", extensionName,
                storeName);

        if (isResponsive() && getInputStream(responsiveStoreFile) != null)
        {
            getRapidSetupImpexService().importImpexFile(responsiveStoreFile, false, forceImport, extensionName);
        }
        else
        {
            getRapidSetupImpexService().importImpexFile(
                    String.format("/%s/import/coredata/stores/%s/store.impex", extensionName, storeName), false, forceImport, extensionName);
        }

        if (isResponsive() && getInputStream(responsiveSiteFile) != null)
        {
            getRapidSetupImpexService().importImpexFile(responsiveSiteFile, false, forceImport, extensionName);
        }
        else
        {
            getRapidSetupImpexService().importImpexFile(
                    String.format("/%s/import/coredata/stores/%s/site.impex", extensionName, storeName), false, forceImport, extensionName);
        }
    }

    protected void importAllData(final AbstractSystemSetup systemSetup, final SystemSetupContext context,
                                 final ImportData importData, final boolean syncCatalogs)
    {
        final boolean forceImport = systemSetup.getBooleanSystemSetupParameter(context, FORCE_IMPORT);

        systemSetup.logInfo(context, String.format("Begin importing common data for [%s]", context.getExtensionName()));
        importCommonData(context.getExtensionName(), forceImport);

        systemSetup.logInfo(context,
                String.format("Begin importing product catalog data for [%s]", importData.getProductCatalogName()));
        importProductCatalog(context.getExtensionName(), importData.getProductCatalogName(), forceImport);


        for (final String contentCatalogName : importData.getContentCatalogNames())
        {
            systemSetup.logInfo(context, String.format("Begin importing content catalog data for [%s]", contentCatalogName));
            importContentCatalog(context.getExtensionName(), contentCatalogName, forceImport);
        }

        synchronizeProductCatalog(systemSetup, context, importData.getProductCatalogName(), false);
        for (final String contentCatalog : importData.getContentCatalogNames())
        {
            synchronizeContentCatalog(systemSetup, context, contentCatalog, false);
        }
        assignDependent(importData.getProductCatalogName(), importData.getContentCatalogNames());

        if (syncCatalogs)
        {
            systemSetup
                    .logInfo(context, String.format("Synchronizing product catalog for [%s]", importData.getProductCatalogName()));
            final boolean productSyncSuccess = synchronizeProductCatalog(systemSetup, context, importData.getProductCatalogName(),
                    true);

            for (final String contentCatalogName : importData.getContentCatalogNames())
            {
                systemSetup.logInfo(context, String.format("Synchronizing content catalog for [%s]", contentCatalogName));
                synchronizeContentCatalog(systemSetup, context, contentCatalogName, true);
            }

            if (!productSyncSuccess)
            {
                // Rerun the product sync if required
                systemSetup.logInfo(context,
                        String.format("Rerunning product catalog synchronization for [%s]", importData.getProductCatalogName()));
                if (!synchronizeProductCatalog(systemSetup, context, importData.getProductCatalogName(), true))
                {
                    systemSetup.logInfo(context, String.format(
                            "Rerunning product catalog synchronization for [%s], failed. Please consult logs for more details.",
                            importData.getProductCatalogName()));
                }
            }
        }

        for (final String storeName : importData.getStoreNames())
        {
            systemSetup.logInfo(context, String.format("Begin importing store data for [%s]", storeName));
            importStore(context.getExtensionName(), storeName, importData.getProductCatalogName(), forceImport);

            systemSetup.logInfo(context, String.format("Begin importing job data for [%s]", storeName));
            importJobs(context.getExtensionName(), storeName, forceImport);

            systemSetup.logInfo(context, String.format("Begin importing solr index data for [%s]", storeName));
            importSolrIndex(context.getExtensionName(), storeName, forceImport);

            if (systemSetup.getBooleanSystemSetupParameter(context, ACTIVATE_SOLR_CRON_JOBS))
            {
                systemSetup.logInfo(context, String.format("Activating solr index for [%s]", storeName));
                runSolrIndex(context.getExtensionName(), storeName);
            }
        }
    }

    public RapidSetupImpexService getRapidSetupImpexService()
    {
        return rapidSetupImpexService;
    }

    @Required
    public void setRapidSetupImpexService(final RapidSetupImpexService rapidSetupImpexService)
    {
        this.rapidSetupImpexService = rapidSetupImpexService;
    }

    public RapidSetupResourceLoader getRapidSetupResourceLoader()
    {
        return rapidSetupResourceLoader;
    }

    @Required
    public void setRapidSetupResourceLoader(final RapidSetupResourceLoader rapidSetupResourceLoader)
    {
        this.rapidSetupResourceLoader = rapidSetupResourceLoader;
    }
}
