package com.lyonscg.rapidsetup.setup;

import com.lyonscg.rapidsetup.dao.FileResourceDao;
import com.lyonscg.rapidsetup.model.FileResourceModel;
import com.lyonscg.rapidsetup.service.RapidSetupResourceLoader;
import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.commerceservices.setup.events.CoreDataImportedEvent;
import de.hybris.platform.commerceservices.setup.events.SampleDataImportedEvent;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.*;

/**
 * Abstract system setup class providing support for enhanced system setup.
 *
 * @author lyonscg
 */
public abstract class AbstractRapidSystemSetup extends AbstractSystemSetup
{
    private static final Logger LOG = Logger.getLogger(AbstractRapidSystemSetup.class);

    private static final String IMPORT_CORE_DATA = "importCoreData";
    private static final String IMPORT_SAMPLE_DATA = "importSampleData";
    private static final String FORCE_IMPORT = "forceImport";
    private static final String ACTIVATE_SOLR_CRON_JOBS = "activateSolrCronJobs";

    private CoreDataImportService coreDataImportService;
    private SampleDataImportService sampleDataImportService;
    private RapidSetupResourceLoader rapidSetupResourceLoader;
    private ConfigurationService configurationService;
    private FileResourceDao fileResourceDao;

    @SystemSetupParameterMethod
    @Override
    public List<SystemSetupParameter> getInitializationOptions()
    {
        final List<SystemSetupParameter> params = new ArrayList<SystemSetupParameter>();

        params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", true));
        params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_DATA, "Import Sample Data", true));
        params.add(createBooleanSystemSetupParameter(FORCE_IMPORT, "Force Import of Rapid Setup files", false));
        params.add(createBooleanSystemSetupParameter(ACTIVATE_SOLR_CRON_JOBS, "Activate Solr Cron Jobs", true));

        return params;
    }


    /**
     * RapidSetup introduces a toggable autoload feature. When running standard system updates, modified files
     * will be imported. This is intended to help streamline the development process.
     *
     * @param context
     */
    @SystemSetup(type = SystemSetup.Type.ESSENTIAL, process = SystemSetup.Process.UPDATE)
    public void createEssentialData(final SystemSetupContext context)
    {
        if(!hasImportedFilesForExtension(context.getExtensionName()))
        {
            return;
        }

        final Map<String, String[]> params = new HashMap<>(context.getParameterMap());
        if(getConfigurationService().getConfiguration().getBoolean(context.getExtensionName() + ".coredata.autoload", false))
        {

            params.put(context.getExtensionName() + "_" + IMPORT_CORE_DATA, new String[] {"yes"});
        }
        else
        {
            params.put(context.getExtensionName() + "_" + IMPORT_CORE_DATA, new String[] {"no"});
        }

        if(getConfigurationService().getConfiguration().getBoolean(context.getExtensionName() + ".sampledata.autoload", false))
        {
            params.put(context.getExtensionName() + "_" + IMPORT_SAMPLE_DATA, new String[] {"yes"});
        }
        else
        {
            params.put(context.getExtensionName() + "_" + IMPORT_SAMPLE_DATA, new String[] {"no"});
        }

        params.put(context.getExtensionName() + "_" + FORCE_IMPORT, new String[] {"no"});
        params.put(context.getExtensionName() + "_" + ACTIVATE_SOLR_CRON_JOBS, new String[] {"no"});

        context.setParameterMap(Collections.unmodifiableMap(params));
        createProjectData(context);
    }



    @SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.ALL)
    public void createProjectData(final SystemSetupContext context)
    {
        final List<ImportData> importData = new ArrayList<ImportData>();

        final ImportData extImportData = new ImportData();
        extImportData.setProductCatalogName(getProductCatalogName());
        extImportData.setContentCatalogNames(getContentCatalogNames());
        extImportData.setStoreNames(getStoreNames());
        importData.add(extImportData);

        getCoreDataImportService().execute(this, context, importData);
        getSampleDataImportService().execute(this, context, importData);

    }

    protected abstract String getProductCatalogName();

    protected abstract List<String> getContentCatalogNames();

    protected abstract List<String> getStoreNames();

    public RapidSetupResourceLoader getRapidSetupResourceLoader()
    {
        return rapidSetupResourceLoader;
    }

    @Required
    public void setRapidSetupResourceLoader(final RapidSetupResourceLoader rapidSetupResourceLoader)
    {
        this.rapidSetupResourceLoader = rapidSetupResourceLoader;
    }

    public ConfigurationService getConfigurationService()
    {
        return configurationService;
    }

    @Required
    public void setConfigurationService(final ConfigurationService configurationService)
    {
        this.configurationService = configurationService;
    }

    public FileResourceDao getFileResourceDao()
    {
        return fileResourceDao;
    }

    @Required
    public void setFileResourceDao(final FileResourceDao fileResourceDao)
    {
        this.fileResourceDao = fileResourceDao;
    }


    public CoreDataImportService getCoreDataImportService()
    {
        return coreDataImportService;
    }

    @Required
    public void setCoreDataImportService(final CoreDataImportService coreDataImportService)
    {
        this.coreDataImportService = coreDataImportService;
    }

    public SampleDataImportService getSampleDataImportService()
    {
        return sampleDataImportService;
    }

    @Required
    public void setSampleDataImportService(final SampleDataImportService sampleDataImportService)
    {
        this.sampleDataImportService = sampleDataImportService;
    }

    boolean hasImportedFilesForExtension(final String extensionname)
    {
        final Map<String, Object> params = new HashMap<>();
        params.put(FileResourceModel.EXTENSION, extensionname);
        return !CollectionUtils.isEmpty(getFileResourceDao().find(params));
    }

}
