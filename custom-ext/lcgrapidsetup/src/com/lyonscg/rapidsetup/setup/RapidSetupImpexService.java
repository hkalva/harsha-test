package com.lyonscg.rapidsetup.setup;

public interface RapidSetupImpexService
{
    /**
     * RapidSetup impex import method.
     * @param file
     * @param errorIfMissing
     * @param forceImport
     * @param extensionName
     */
    void importImpexFile(final String file, final boolean errorIfMissing, final boolean forceImport, final String extensionName);
}
