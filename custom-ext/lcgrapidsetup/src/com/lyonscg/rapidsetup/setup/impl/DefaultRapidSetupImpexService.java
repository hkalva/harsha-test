package com.lyonscg.rapidsetup.setup.impl;

import com.lyonscg.rapidsetup.dao.FileResourceDao;
import com.lyonscg.rapidsetup.service.ChecksumComputer;
import com.lyonscg.rapidsetup.setup.RapidSetupImpexService;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.impex.ImportConfig;
import de.hybris.platform.servicelayer.impex.ImportResult;
import de.hybris.platform.servicelayer.impex.ImportService;
import de.hybris.platform.servicelayer.impex.impl.StreamBasedImpExResource;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Default implementation of RapidSetupImpexService. Impex files are imported only if they have not yet been imported
 * or forceImport is set to true.
 *
 * @author lyonscg
 */
public class DefaultRapidSetupImpexService implements RapidSetupImpexService
{
    private final static Logger LOG = Logger.getLogger(DefaultRapidSetupImpexService.class);
    private final static String IMPEX_EXT = ".impex";
    private final static String FILE_ENCODING = "UTF-8";

    private FileResourceDao fileResourceDao;
    private ChecksumComputer checksumComputer;
    private ImportService importService;
    private CommonI18NService commonI18NService;


    @Override
    public void importImpexFile(final String file, final boolean errorIfMissing, final boolean forceImport, final String extensionName)
    {
        try(final InputStream resourceAsStream = getClass().getResourceAsStream(file)) {
            if (resourceAsStream == null) {
                if (errorIfMissing) {
                    LOG.error("Importing [" + file + "]... ERROR (MISSING FILE)", null);
                } else {
                    LOG.info("Importing [" + file + "]... SKIPPED (Optional File Not Found)");
                }
            } else {
                importImpexFile(file, resourceAsStream, forceImport, extensionName);

                // Try to import language specific impex files
                if (file.endsWith(IMPEX_EXT)) {
                    final String filePath = file.substring(0, file.length() - IMPEX_EXT.length());

                    final List<LanguageModel> languages = getCommonI18NService().getAllLanguages();
                    for (final LanguageModel language : languages) {
                        final String languageFilePath = filePath + "_" + language.getIsocode() + IMPEX_EXT;
                        try(final InputStream languageResourceAsStream = getClass().getResourceAsStream(languageFilePath)) {
                            if (languageResourceAsStream != null) {
                                importImpexFile(languageFilePath, languageResourceAsStream, forceImport, extensionName);
                            }
                        }
                    }
                }
            }
        } catch(IOException e){
            LOG.error("FAILED", e);
        }
    }

    protected void importImpexFile(final String file, final InputStream stream, final boolean forceImport, final String extensionName)
    {
        String checksum = null;
        try {
            checksum = getChecksumComputer().compute(file);

        } catch (IOException e) {
            LOG.error("IOException computing checksum of [" + file + "]. Cannot import.");
            IOUtils.closeQuietly(stream);
            return;
        }

        if(!forceImport && !needsImporting(file, checksum))
        {
            LOG.info("Impex file [" + file + "] has unchanged checksum [" + checksum + "]. Skipping import.");
            IOUtils.closeQuietly(stream);
            return;
        }

        final String message = "Importing [" + file + "]...";

        try
        {
            LOG.info(message);

            final ImportConfig importConfig = new ImportConfig();
            importConfig.setScript(new StreamBasedImpExResource(stream, FILE_ENCODING));
            importConfig.setLegacyMode(Boolean.FALSE);

            final ImportResult importResult = getImportService().importData(importConfig);
            if (importResult.isError())
            {
                LOG.error(message + " FAILED");
            }
            getFileResourceDao().updateOrCreateFileResource(file, checksum, extensionName);
        }
        catch (final Exception e)
        {
            LOG.error(message + " FAILED", e);
        }
    }

    protected boolean needsImporting(final String file, final String checksum)
    {
        return getFileResourceDao().findFileResource(file, checksum) == null;
    }

    public FileResourceDao getFileResourceDao()
    {
        return fileResourceDao;
    }

    @Required
    public void setFileResourceDao(final FileResourceDao fileResourceDao)
    {
        this.fileResourceDao = fileResourceDao;
    }

    public ChecksumComputer getChecksumComputer()
    {
        return checksumComputer;
    }

    @Required
    public void setChecksumComputer(final ChecksumComputer checksumComputer)
    {
        this.checksumComputer = checksumComputer;
    }

    public ImportService getImportService()
    {
        return importService;
    }

    @Required
    public void setImportService(final ImportService importService)
    {
        this.importService = importService;
    }

    public CommonI18NService getCommonI18NService()
    {
        return commonI18NService;
    }

    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService)
    {
        this.commonI18NService = commonI18NService;
    }
}
