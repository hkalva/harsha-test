package com.lyonscg.rapidsetup;

import org.apache.commons.lang3.StringUtils;

public class RapidSetupUtils
{
    private RapidSetupUtils()
    {
        //Do nothing
    }

    public static String normalizeFileResourcePath(final String filePath, final String basePath)
    {
        return StringUtils.removeStart(filePath, StringUtils.substringBefore(filePath, basePath));
    }
}
