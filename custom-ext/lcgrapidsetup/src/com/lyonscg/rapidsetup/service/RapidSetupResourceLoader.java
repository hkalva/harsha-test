package com.lyonscg.rapidsetup.service;

import java.io.File;
import java.util.List;

/**
 * @author lyonscg
 */
public interface RapidSetupResourceLoader
{
    /**
     * Returns a list of file paths nested under the specified directory. The paths are relative to the /resources directory.
     *
     * @param dirPath
     * @return
     */
    List<String> findResourcePaths(final String dirPath);

    /**
     * Returns all files.
     *
     * @param basePath
     * @return file collection
     */
    List<File> findFiles(final String basePath);

    /**
     * Returns only files that have either not been processed or have changed since they were last processed.
     *
     * @param basePath
     * @return file collection
     */
    List<File> findModifiedFiles(final String basePath);
}
