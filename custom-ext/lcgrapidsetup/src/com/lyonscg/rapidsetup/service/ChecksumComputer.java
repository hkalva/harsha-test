package com.lyonscg.rapidsetup.service;

import java.io.File;
import java.io.IOException;

public interface ChecksumComputer
{
    String compute(final File file) throws IOException;
    String compute(final String file) throws IOException;
}
