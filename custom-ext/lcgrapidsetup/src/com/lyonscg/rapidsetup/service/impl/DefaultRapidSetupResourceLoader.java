package com.lyonscg.rapidsetup.service.impl;

import com.lyonscg.rapidsetup.RapidSetupUtils;
import com.lyonscg.rapidsetup.dao.FileResourceDao;
import com.lyonscg.rapidsetup.model.FileResourceModel;
import com.lyonscg.rapidsetup.service.ChecksumComputer;
import com.lyonscg.rapidsetup.service.RapidSetupResourceLoader;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author lyonscg
 */
public class DefaultRapidSetupResourceLoader implements RapidSetupResourceLoader
{
    private final static Logger LOG = Logger.getLogger(DefaultRapidSetupResourceLoader.class);

    private Pattern I18N_FILE_PATTERN = Pattern.compile("^[a-zA-Z0-9-_]+_[a-z]{2}.impex$");
    private FileResourceDao fileResourceDao;
    private ChecksumComputer checksumComputer;

    @Override
    public List<String> findResourcePaths(final String dirPath) {
        return findFiles(dirPath).stream()
                .filter(file -> !I18N_FILE_PATTERN.matcher(file.getName()).matches())
                .map(file -> RapidSetupUtils.normalizeFileResourcePath(file.getPath(), dirPath))
                .collect(Collectors.toList());
    }

    @Override
    public List<File> findFiles(final String basePath) {
        try {
            final File dir = new File(DefaultRapidSetupResourceLoader.class.getResource(basePath).getFile());
            return new ArrayList<>(FileUtils.listFiles(dir, new String[]{"impex"}, true));
        } catch(Exception e) {
            LOG.warn("Could not access directory path " + basePath + ". Does the directory resource exist?");
            return new ArrayList<>();
        }
    }

    @Override
    public List<File> findModifiedFiles(final String basePath) {

        final List<File> files = findFiles(basePath);
        final List<File> modifiedFiles = new ArrayList<>();

        for(File file : files)
        {
            try {
                final String checksum = getChecksumComputer().compute(file);
                final String resourcePath = RapidSetupUtils.normalizeFileResourcePath(file.getPath(), basePath);

                if(needsImporting(resourcePath, checksum))
                {
                    modifiedFiles.add(file);
                }


            } catch(Exception e) {
                //Do nothing
            }
        }

        return modifiedFiles;

    }

    private boolean needsImporting(final String path, final String checksum)
    {
        final Map<String, Object> params = new HashMap<>();
        params.put(FileResourceModel.PATH, path);
        params.put(FileResourceModel.CHECKSUM, checksum);
        return CollectionUtils.isEmpty(getFileResourceDao().find(params));
    }

    public FileResourceDao getFileResourceDao()
    {
        return fileResourceDao;
    }

    @Required
    public void setFileResourceDao(final FileResourceDao fileResourceDao)
    {
        this.fileResourceDao = fileResourceDao;
    }

    public ChecksumComputer getChecksumComputer()
    {
        return checksumComputer;
    }

    @Required
    public void setChecksumComputer(final ChecksumComputer checksumComputer)
    {
        this.checksumComputer = checksumComputer;
    }
}
