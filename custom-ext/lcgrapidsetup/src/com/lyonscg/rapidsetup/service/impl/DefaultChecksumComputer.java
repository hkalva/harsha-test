package com.lyonscg.rapidsetup.service.impl;

import com.lyonscg.rapidsetup.service.ChecksumComputer;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class DefaultChecksumComputer implements ChecksumComputer
{
    @Override
    public String compute(final File file) throws IOException {
        return String.valueOf(FileUtils.checksumCRC32(file));
    }

    @Override
    public String compute(final String file) throws IOException
    {
        return compute(new File(getClass().getResource(file).getPath()));
    }
}
