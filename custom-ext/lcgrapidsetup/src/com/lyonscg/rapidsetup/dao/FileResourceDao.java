package com.lyonscg.rapidsetup.dao;

import com.lyonscg.rapidsetup.model.FileResourceModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

import java.io.File;

public interface FileResourceDao extends GenericDao<FileResourceModel>
{
    void updateOrCreateFileResource(final String path, final File file, final String extensionName);

    void updateOrCreateFileResource(final String path, final String checksum, final String extensionName);

    FileResourceModel findFileResource(final String path, final String checksum);
}
