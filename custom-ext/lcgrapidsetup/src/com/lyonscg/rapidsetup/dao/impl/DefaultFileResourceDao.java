package com.lyonscg.rapidsetup.dao.impl;

import com.lyonscg.rapidsetup.dao.FileResourceDao;
import com.lyonscg.rapidsetup.model.FileResourceModel;
import com.lyonscg.rapidsetup.service.ChecksumComputer;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * DefaultFileResourceDao.
 *
 * @author lyonscg
 */
public class DefaultFileResourceDao extends DefaultGenericDao<FileResourceModel> implements FileResourceDao
{
    private static final Logger LOG = Logger.getLogger(DefaultFileResourceDao.class);

    private ModelService modelService;
    private ChecksumComputer checksumComputer;

    public DefaultFileResourceDao()
    {
        super(FileResourceModel._TYPECODE);
    }

    @Override
    public void updateOrCreateFileResource(final String path, final File file, final String extensionName)
    {
        try {
            updateOrCreateFileResource(path, getChecksumComputer().compute(file), extensionName);
        } catch (Exception e) {
            LOG.error("Error saving file " + path + " to resource table.", e);
        }

    }

    @Override
    public void updateOrCreateFileResource(final String path, final String checksum, final String extensionName) {
        try {
            final Map<String, Object> params = new HashMap<>();
            params.put(FileResourceModel.PATH, path);
            final List<FileResourceModel> result = find(params);

            final FileResourceModel resourceModel = CollectionUtils.isNotEmpty(result)
                    ? result.get(0) : getModelService().create(FileResourceModel.class);
            resourceModel.setChecksum(checksum);
            resourceModel.setPath(path);
            resourceModel.setExtension(extensionName);
            getModelService().save(resourceModel);

            LOG.info("Saved file [" + path + "] to resource table with checksum [" + checksum + "].");
        } catch (Exception e) {
            LOG.error("Error saving file " + path + " to resource table.", e);
        }
    }

    @Override
    public FileResourceModel findFileResource(final String path, final String checksum)
    {
        final Map<String, Object> params = new HashMap<>();
        params.put(FileResourceModel.PATH, path);
        params.put(FileResourceModel.CHECKSUM, checksum);
        final List<FileResourceModel> results = find(params);
        return CollectionUtils.isEmpty(results) ? null : results.get(0);
    }

    public ChecksumComputer getChecksumComputer()
    {
        return checksumComputer;
    }

    @Required
    public void setChecksumComputer(final ChecksumComputer checksumComputer)
    {
        this.checksumComputer = checksumComputer;
    }

    public ModelService getModelService() {
        return modelService;
    }

    @Required
    public void setModelService(final ModelService modelService)
    {
        this.modelService = modelService;
    }
}
