# LCG RapidSetup

This extension provides an enhanced means of importing project data. Project data files are imported once, unless modified, reducing the time required to run project data loads for your extension.

## The Setup class

Your extension should extend AbstractRapidSystemSetup.

```java
@SystemSetup(extension = Lcgb2cstoreConstants.EXTENSIONNAME)
public class Lcgb2cstoreSystemSetup extends AbstractRapidSystemSetup
{

	@Override
	protected String getProductCatalogName() {
		return Lcgb2cstoreConstants.LCGB2C;
	}

	@Override
	protected List<String> getContentCatalogNames() {
		return Arrays.asList(Lcgb2cstoreConstants.LCGB2C);
	}

	@Override
	protected List<String> getStoreNames() {
		return Arrays.asList(Lcgb2cstoreConstants.LCGB2C);
	}
}
```



## Properties

Add the following properties to your extension.

```properties
lcgb2cstore.rapidsetup=true
lcgb2cstore.coredata.autoload=true
lcgb2cstore.sampledata.autoload=true
```

Replace 'lcgb2cstore' with the name of your extension. Setting *.rapidsetup to true enables the custom "rapid" behavior for your data extension. This must be true for Rapid Setup features to work.

Optionally, you may set core and/or sample data autoload flags to true. Doing so will cause new or modified impex files to be loaded during system updates without the need to check your extension's project data options on the HAC update page. The autoload flags, especially the sample autoload flag, should be disabled in production environments.

## ant storegen

This extension provides a custom storegen ant task that can be run from the hybris platform directory. This task copies the OOTB extgen logic and adds additional YSTORE and YSTORE_NAME tokens to support copying store catalog impex files from *store template extensions. An example store template extension is the lcgb2cstore extension. To generate a store data extension using this template, perform the following steps:

1. Add lcgrapidsetup to localextensions.xml
2. Add lcgb2cstore to localextensions.xml
3. In the hybris platform directory, run 'ant storegen'
4. For the first three options, proceed as you would when running 'ant extgen'.
5. For the fourth and final store name option, select the name of your store. This should be a simple name, as this will be used as the store uid and directory prefixes. For example "pets" or "hardware" are appropriate choices.
6. Remove the lcgb2cstore template extension from localextensions.xml
7. Add your new store extension to localextensions.xml
