package com.lyonscg.rapidsetup.service.impl;

import com.lyonscg.rapidsetup.dao.FileResourceDao;
import com.lyonscg.rapidsetup.model.FileResourceModel;
import com.lyonscg.rapidsetup.service.ChecksumComputer;
import com.lyonscg.rapidsetup.service.RapidSetupResourceLoader;
import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@UnitTest
public class DefaultRapidSetupResourceLoaderTest
{
    @InjectMocks
    private RapidSetupResourceLoader loader;

    @Mock
    private FileResourceDao fileResourceDao;

    @Mock
    private ChecksumComputer checksumComputer;

    @Before
    public void setup()
    {
        loader = new DefaultRapidSetupResourceLoader();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAllFiles()
    {
        final List<File> files = loader.findFiles("/test/mystore/import/coredata/contentCatalogs");

        Assert.assertEquals(11, files.size());
    }

    @Test
    public void testGetModifiedFilesReturnsNewFiles()
    {
        Mockito.doReturn(new ArrayList<FileResourceModel>()).when(fileResourceDao).find(Mockito.anyMap());
        final List<File> files = loader.findModifiedFiles("/test/mystore/import/coredata/contentCatalogs");
        Assert.assertEquals(11, files.size());
    }


    @Test
    public void testGetModifiedFilesDoesNotReturnUnModifiedFiles() throws Exception
    {
        Mockito.doReturn(new ArrayList<FileResourceModel>()).when(fileResourceDao).find(Mockito.anyMap());
        Mockito.doAnswer(iom -> {
            final Map<String, Object> params = (Map<String, Object>)iom.getArguments()[0];
            final List<FileResourceModel> frs = new ArrayList<>();
            if(StringUtils.endsWith((String)params.get(FileResourceModel.PATH), "footer.impex"))
            {
                frs.add(Mockito.mock(FileResourceModel.class));
            }
            return frs;
        }).when(fileResourceDao).find(Mockito.anyMapOf(String.class, Object.class));
        final List<File> files = loader.findModifiedFiles("/test/mystore/import/coredata/contentCatalogs");
        Assert.assertEquals(10, files.size());
        Assert.assertTrue(files.stream().noneMatch(f -> StringUtils.endsWith(f.getPath(), "footer.impex")));
    }

    @Test
    public void testFindResourcePaths() throws Exception
    {
        final List<String> files = loader.findResourcePaths("/test/mystore/import/coredata/contentCatalogs");

        Assert.assertEquals(9, files.size());
        Assert.assertFalse(files.contains("/test/mystore/import/coredata/contentCatalogs/myContentCatalog/header_en.impex"));
        Assert.assertFalse(files.contains("/test/mystore/import/coredata/contentCatalogs/myContentCatalog/footer_en.impex"));
    }
}
