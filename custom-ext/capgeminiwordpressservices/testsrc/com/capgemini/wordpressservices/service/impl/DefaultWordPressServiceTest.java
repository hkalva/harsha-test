package com.capgemini.wordpressservices.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.capgemini.wordpressservices.data.WordPressPage;
import com.capgemini.wordpressservices.data.WordPressPostsRequest;
import com.capgemini.wordpressservices.data.WordPressPostsResponse;
import com.capgemini.wordpressservices.exception.WordPressException;


@UnitTest
public class DefaultWordPressServiceTest
{

    private static final String PROPERTY_WORD_PRESS_POSTS_URL = "wordpress.posts.url";
    private static final String PROPERTY_WORD_PRESS_RETRIEVE_HTML_FROM_GUID = "wordpress.retrieve.html.from.guid";
    private static final String PROPERTY_WORD_PRESS_TIMEOUT_CONNECT = "wordpress.timeout.connect";
    private static final String PROPERTY_WORD_PRESS_TIMEOUT_READ = "wordpress.timeout.read";
    private static final String SLUG = "[{\"id\":1,\"date\":\"2017-05-23T06:25:50\",\"date_gmt\":\"2017-05-23T06:25:50\",\"guid\":{\"rendered\":\"http:\\/\\/demo.wp-api.org\\/2017\\/05\\/23\\/hello-world\\/\"},\"modified\":\"2017-05-23T06:25:50\",\"modified_gmt\":\"2017-05-23T06:25:50\",\"slug\":\"hello-world\",\"status\":\"publish\",\"type\":\"post\",\"link\":\"https:\\/\\/demo.wp-api.org\\/2017\\/05\\/23\\/hello-world\\/\",\"title\":{\"rendered\":\"Hello world!\"},\"content\":{\"rendered\":\"<p>Welcome to <a href=\\\"http:\\/\\/wp-api.org\\/\\\">WP API Demo Sites<\\/a>. This is your first post. Edit or delete it, then start blogging!<\\/p>\\n\",\"protected\":false},\"excerpt\":{\"rendered\":\"<p>Welcome to WP API Demo Sites. This is your first post. Edit or delete it, then start blogging!<\\/p>\\n\",\"protected\":false},\"author\":1,\"featured_media\":0,\"comment_status\":\"open\",\"ping_status\":\"open\",\"sticky\":false,\"template\":\"\",\"format\":\"standard\",\"meta\":[],\"categories\":[1],\"tags\":[],\"_links\":{\"self\":[{\"href\":\"https:\\/\\/demo.wp-api.org\\/wp-json\\/wp\\/v2\\/posts\\/1\"}],\"collection\":[{\"href\":\"https:\\/\\/demo.wp-api.org\\/wp-json\\/wp\\/v2\\/posts\"}],\"about\":[{\"href\":\"https:\\/\\/demo.wp-api.org\\/wp-json\\/wp\\/v2\\/types\\/post\"}],\"author\":[{\"embeddable\":true,\"href\":\"https:\\/\\/demo.wp-api.org\\/wp-json\\/wp\\/v2\\/users\\/1\"}],\"replies\":[{\"embeddable\":true,\"href\":\"https:\\/\\/demo.wp-api.org\\/wp-json\\/wp\\/v2\\/comments?post=1\"}],\"version-history\":[{\"count\":0,\"href\":\"https:\\/\\/demo.wp-api.org\\/wp-json\\/wp\\/v2\\/posts\\/1\\/revisions\"}],\"wp:attachment\":[{\"href\":\"https:\\/\\/demo.wp-api.org\\/wp-json\\/wp\\/v2\\/media?parent=1\"}],\"wp:term\":[{\"taxonomy\":\"category\",\"embeddable\":true,\"href\":\"https:\\/\\/demo.wp-api.org\\/wp-json\\/wp\\/v2\\/categories?post=1\"},{\"taxonomy\":\"post_tag\",\"embeddable\":true,\"href\":\"https:\\/\\/demo.wp-api.org\\/wp-json\\/wp\\/v2\\/tags?post=1\"}],\"curies\":[{\"name\":\"wp\",\"href\":\"https:\\/\\/api.w.org\\/{rel}\",\"templated\":true}]}}]";
    private static final String SLUG1 = "[{\"id\":1,\"date\":\"2017-05-23T06:25:50\",\"date_gmt\":\"2017-05-23T06:25:50\",\"guid\":{\"rendered\":\"http:\\/\\/demo.wp-api.org\\/2017\\/05\\/23\\/hello-world\\/\"},\"modified\":\"2017-05-23T06:25:50\",\"modified_gmt\":\"2017-05-23T06:25:50\",\"slug\":\"hello-world\",\"status\":\"publish\",\"type\":\"post\",\"link\":\"https:\\/\\/demo.wp-api.org\\/2017\\/05\\/23\\/hello-world\\/\",\"title\":{\"rendered\":\"Hello world!\"},\"content\":{\"rendered\":\"\",\"protected\":false},\"excerpt\":{\"rendered\":\"<p>Welcome to WP API Demo Sites. This is your first post. Edit or delete it, then start blogging!<\\/p>\\n\",\"protected\":false},\"author\":1,\"featured_media\":0,\"comment_status\":\"open\",\"ping_status\":\"open\",\"sticky\":false,\"template\":\"\",\"format\":\"standard\",\"meta\":[],\"categories\":[1],\"tags\":[],\"_links\":{\"self\":[{\"href\":\"https:\\/\\/demo.wp-api.org\\/wp-json\\/wp\\/v2\\/posts\\/1\"}],\"collection\":[{\"href\":\"https:\\/\\/demo.wp-api.org\\/wp-json\\/wp\\/v2\\/posts\"}],\"about\":[{\"href\":\"https:\\/\\/demo.wp-api.org\\/wp-json\\/wp\\/v2\\/types\\/post\"}],\"author\":[{\"embeddable\":true,\"href\":\"https:\\/\\/demo.wp-api.org\\/wp-json\\/wp\\/v2\\/users\\/1\"}],\"replies\":[{\"embeddable\":true,\"href\":\"https:\\/\\/demo.wp-api.org\\/wp-json\\/wp\\/v2\\/comments?post=1\"}],\"version-history\":[{\"count\":0,\"href\":\"https:\\/\\/demo.wp-api.org\\/wp-json\\/wp\\/v2\\/posts\\/1\\/revisions\"}],\"wp:attachment\":[{\"href\":\"https:\\/\\/demo.wp-api.org\\/wp-json\\/wp\\/v2\\/media?parent=1\"}],\"wp:term\":[{\"taxonomy\":\"category\",\"embeddable\":true,\"href\":\"https:\\/\\/demo.wp-api.org\\/wp-json\\/wp\\/v2\\/categories?post=1\"},{\"taxonomy\":\"post_tag\",\"embeddable\":true,\"href\":\"https:\\/\\/demo.wp-api.org\\/wp-json\\/wp\\/v2\\/tags?post=1\"}],\"curies\":[{\"name\":\"wp\",\"href\":\"https:\\/\\/api.w.org\\/{rel}\",\"templated\":true}]}}]";

    private WordPressPostsRequest request;

    @Mock
    private SiteConfigService siteConfigService;

    @Mock
    private RestTemplate restTemplate;

    @Spy
    private DefaultWordPressService service;

    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        request = new WordPressPostsRequest();
        request.setSlug("hello-world");
        service.setSiteConfigService(siteConfigService);
        Mockito.when(siteConfigService.getProperty(PROPERTY_WORD_PRESS_TIMEOUT_CONNECT)).thenReturn("20000");
        Mockito.when(siteConfigService.getProperty(PROPERTY_WORD_PRESS_TIMEOUT_READ)).thenReturn("10000");
        Mockito.when(siteConfigService.getProperty(PROPERTY_WORD_PRESS_RETRIEVE_HTML_FROM_GUID)).thenReturn("true");
        Mockito.when(siteConfigService.getProperty(PROPERTY_WORD_PRESS_POSTS_URL)).thenReturn("http://test.com/?slug=");
    }

    @Test
    public void getReadTimeoutInMillis()
    {
        final int result = service.getReadTimeoutInMillis();
        Assert.assertTrue(result == 10000);
    }

    @Test
    public void getConnectTimeoutInMillis()
    {
        final int result = service.getConnectTimeoutInMillis();
        Assert.assertTrue(result == 20000);
    }

    @Test
    public void isRetrieveHtmlFromGuid()
    {
        boolean result = service.isRetrieveHtmlFromGuid(false);
        Assert.assertFalse(result);
        result = service.isRetrieveHtmlFromGuid(true);
        Assert.assertTrue(result);
    }

    @Test
    public void getRestTemplate()
    {
        final RestTemplate result = service.getRestTemplate();
        Assert.assertTrue(result != null);
    }

    @Test
    public void getPosts()
    {
        Mockito.doReturn(restTemplate).when(service).getRestTemplate();
        Mockito.when(restTemplate.getForObject("http://test.com/?slug=hello-world", String.class)).thenReturn(SLUG);
        final WordPressPostsResponse response = service.getPosts(request);
        Assert.assertTrue(response != null);
        final String guid = service.getGuid(response);
        Assert.assertTrue(StringUtils.isNotBlank(guid));
    }

    @Test(expected = WordPressException.class)
    public void getPostsInvalidResponse()
    {
        Mockito.doReturn(restTemplate).when(service).getRestTemplate();
        Mockito.when(restTemplate.getForObject("http://test.com/?slug=hello-world", String.class))
                .thenThrow(RestClientException.class);
        service.getPosts(request);
    }

    @Test
    public void getPostsNoResponse()
    {
        Mockito.doReturn(restTemplate).when(service).getRestTemplate();
        Mockito.when(restTemplate.getForObject("http://test.com/?slug=hello-world", String.class)).thenReturn(null);
        final WordPressPostsResponse response = service.getPosts(request);
        Assert.assertTrue(response == null);
    }

    @Test
    public void getHtml()
    {
        final String result = service.getHtml("http://demo.wp-api.org/2017/05/23/hello-world/");
        Assert.assertTrue(StringUtils.isNotBlank(result));
    }

    @Test(expected = WordPressException.class)
    public void getPageNoResponseError()
    {
        Mockito.doReturn(restTemplate).when(service).getRestTemplate();
        Mockito.when(restTemplate.getForObject("http://test.com/?slug=hello-world", String.class)).thenReturn(null);
        service.getPage(request, false, true);
    }

    @Test
    public void getPageNoResponse()
    {
        Mockito.doReturn(restTemplate).when(service).getRestTemplate();
        Mockito.when(restTemplate.getForObject("http://test.com/?slug=hello-world", String.class)).thenReturn(SLUG1);
        final WordPressPage result = service.getPage(request, false, false);
        Assert.assertTrue(result != null);
        Assert.assertTrue(StringUtils.isBlank(result.getContent()));
    }

    @Test
    public void getPageFromGuid()
    {
        Mockito.doReturn(restTemplate).when(service).getRestTemplate();
        Mockito.when(restTemplate.getForObject("http://test.com/?slug=hello-world", String.class)).thenReturn(SLUG1);
        final WordPressPage result = service.getPage(request, true, false);
        Assert.assertTrue(result != null);
        Assert.assertTrue(StringUtils.isNotBlank(result.getContent()));
    }

    @Test
    public void getPage()
    {
        Mockito.doReturn(restTemplate).when(service).getRestTemplate();
        Mockito.when(restTemplate.getForObject("http://test.com/?slug=hello-world", String.class)).thenReturn(SLUG);
        final WordPressPage result = service.getPage(request, false, false);
        Assert.assertTrue(result != null);
        Assert.assertTrue(StringUtils.isNotBlank(result.getContent()));
    }

    @Test
    public void getContent()
    {
        Mockito.doReturn(restTemplate).when(service).getRestTemplate();
        Mockito.when(restTemplate.getForObject("http://test.com/?slug=hello-world", String.class)).thenReturn(SLUG);
        final String result = service.getContent(request, false);
        Assert.assertTrue(StringUtils.isNotBlank(result));
    }

}
