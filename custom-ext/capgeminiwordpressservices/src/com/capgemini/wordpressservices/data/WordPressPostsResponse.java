package com.capgemini.wordpressservices.data;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class WordPressPostsResponse
{

    /**
     * the id.
     */
    @SerializedName("id")
    @Expose
    private Integer id;

    /**
     * the date.
     */
    @SerializedName("date")
    @Expose
    private String date;

    /**
     * the date in GMT.
     */
    @SerializedName("date_gmt")
    @Expose
    private String dateGmt;

    /**
     * the guid.
     */
    @SerializedName("guid")
    @Expose
    private Guid guid;

    /**
     * the modified date.
     */
    @SerializedName("modified")
    @Expose
    private String modified;

    /**
     * the modified date in GMT.
     */
    @SerializedName("modified_gmt")
    @Expose
    private String modifiedGmt;

    /**
     * the slug.
     */
    @SerializedName("slug")
    @Expose
    private String slug;

    /**
     * the status.
     */
    @SerializedName("status")
    @Expose
    private String status;

    /**
     * the type.
     */
    @SerializedName("type")
    @Expose
    private String type;

    /**
     * the link.
     */
    @SerializedName("link")
    @Expose
    private String link;

    /**
     * the title.
     */
    @SerializedName("title")
    @Expose
    private Title title;

    /**
     * the content.
     */
    @SerializedName("content")
    @Expose
    private Content content;

    /**
     * the excerpt.
     */
    @SerializedName("excerpt")
    @Expose
    private Excerpt excerpt;

    /**
     * the author.
     */
    @SerializedName("author")
    @Expose
    private Integer author;

    /**
     * the featured media.
     */
    @SerializedName("featured_media")
    @Expose
    private Integer featuredMedia;

    /**
     * the comment status.
     */
    @SerializedName("comment_status")
    @Expose
    private String commentStatus;

    /**
     * the ping status
     */
    @SerializedName("ping_status")
    @Expose
    private String pingStatus;

    /**
     * the sticky.
     */
    @SerializedName("sticky")
    @Expose
    private Boolean sticky;

    /**
     * the template.
     */
    @SerializedName("template")
    @Expose
    private String template;

    /**
     * the format.
     */
    @SerializedName("format")
    @Expose
    private String format;

    /**
     * the meta.
     */
    @SerializedName("meta")
    @Expose
    private List<Object> meta = null;

    /**
     * the categories.
     */
    @SerializedName("categories")
    @Expose
    private List<Integer> categories = null;

    /**
     * the tags.
     */
    @SerializedName("tags")
    @Expose
    private List<Object> tags = null;

    /**
     * the links.
     */
    @SerializedName("_links")
    @Expose
    private Links links;

    /**
     * @return Integer
     */
    public Integer getId()
    {
        return id;
    }

    /**
     * @param id
     *            - the id.
     */
    public void setId(final Integer id)
    {
        this.id = id;
    }

    /**
     * @return String
     */
    public String getDate()
    {
        return date;
    }

    /**
     * @param date
     *            - the date.
     */
    public void setDate(final String date)
    {
        this.date = date;
    }

    /**
     * @return String
     */
    public String getDateGmt()
    {
        return dateGmt;
    }

    /**
     * @param dateGmt
     *            - the date in GMT.
     */
    public void setDateGmt(final String dateGmt)
    {
        this.dateGmt = dateGmt;
    }

    /**
     * @return Guid
     */
    public Guid getGuid()
    {
        return guid;
    }

    /**
     * @param guid
     *            - the guid.
     */
    public void setGuid(final Guid guid)
    {
        this.guid = guid;
    }

    /**
     * @return String
     */
    public String getModified()
    {
        return modified;
    }

    /**
     * @param modified
     *            - the modified date.
     */
    public void setModified(final String modified)
    {
        this.modified = modified;
    }

    /**
     * @return String
     */
    public String getModifiedGmt()
    {
        return modifiedGmt;
    }

    /**
     * @param modifiedGmt
     *            - the modified date in GMT.
     */
    public void setModifiedGmt(final String modifiedGmt)
    {
        this.modifiedGmt = modifiedGmt;
    }

    /**
     * @return String
     */
    public String getSlug()
    {
        return slug;
    }

    /**
     * @param slug
     *            - the slug.
     */
    public void setSlug(final String slug)
    {
        this.slug = slug;
    }

    /**
     * @return String
     */
    public String getStatus()
    {
        return status;
    }

    /**
     * @param status
     *            - the status.
     */
    public void setStatus(final String status)
    {
        this.status = status;
    }

    /**
     * @return String
     */
    public String getType()
    {
        return type;
    }

    /**
     * @param type
     *            - the type.
     */
    public void setType(final String type)
    {
        this.type = type;
    }

    /**
     * @return String
     */
    public String getLink()
    {
        return link;
    }

    /**
     * @param link
     *            - the link.
     */
    public void setLink(final String link)
    {
        this.link = link;
    }

    /**
     * @return Title
     */
    public Title getTitle()
    {
        return title;
    }

    /**
     * @param title
     *            - the title.
     */
    public void setTitle(final Title title)
    {
        this.title = title;
    }

    /**
     * @return Content
     */
    public Content getContent()
    {
        return content;
    }

    /**
     * @param content
     *            - the content.
     */
    public void setContent(final Content content)
    {
        this.content = content;
    }

    /**
     * @return Excerpt
     */
    public Excerpt getExcerpt()
    {
        return excerpt;
    }

    /**
     * @param excerpt
     *            - the excerpt.
     */
    public void setExcerpt(final Excerpt excerpt)
    {
        this.excerpt = excerpt;
    }

    /**
     * @return Integer
     */
    public Integer getAuthor()
    {
        return author;
    }

    /**
     * @param author
     *            - the author.
     */
    public void setAuthor(final Integer author)
    {
        this.author = author;
    }

    /**
     * @return Integer
     */
    public Integer getFeaturedMedia()
    {
        return featuredMedia;
    }

    /**
     * @param featuredMedia
     *            - the featured media.
     */
    public void setFeaturedMedia(final Integer featuredMedia)
    {
        this.featuredMedia = featuredMedia;
    }

    /**
     * @return String
     */
    public String getCommentStatus()
    {
        return commentStatus;
    }

    /**
     * @param commentStatus
     *            - the comment status.
     */
    public void setCommentStatus(final String commentStatus)
    {
        this.commentStatus = commentStatus;
    }

    /**
     * @return String
     */
    public String getPingStatus()
    {
        return pingStatus;
    }

    /**
     * @param pingStatus
     *            - the ping status.
     */
    public void setPingStatus(final String pingStatus)
    {
        this.pingStatus = pingStatus;
    }

    /**
     * @return Boolean
     */
    public Boolean getSticky()
    {
        return sticky;
    }

    /**
     * @param sticky
     *            - the sticky flag.
     */
    public void setSticky(final Boolean sticky)
    {
        this.sticky = sticky;
    }

    /**
     * @return String
     */
    public String getTemplate()
    {
        return template;
    }

    /**
     * @param template
     *            - the template.
     */
    public void setTemplate(final String template)
    {
        this.template = template;
    }

    /**
     * @return String
     */
    public String getFormat()
    {
        return format;
    }

    /**
     * @param format
     *            - the format.
     */
    public void setFormat(final String format)
    {
        this.format = format;
    }

    /**
     * @return List<Object>
     */
    public List<Object> getMeta()
    {
        return meta;
    }

    /**
     * @param meta
     *            - the meta objects.
     */
    public void setMeta(final List<Object> meta)
    {
        this.meta = meta;
    }

    /**
     * @return List<Integer>
     */
    public List<Integer> getCategories()
    {
        return categories;
    }

    /**
     * @param categories
     *            - the categories.
     */
    public void setCategories(final List<Integer> categories)
    {
        this.categories = categories;
    }

    /**
     * @return List<Object>
     */
    public List<Object> getTags()
    {
        return tags;
    }

    /**
     * @param tags
     *            - the tags.
     */
    public void setTags(final List<Object> tags)
    {
        this.tags = tags;
    }

    /**
     * @return Links
     */
    public Links getLinks()
    {
        return links;
    }

    /**
     * @param links
     *            - the links
     */
    public void setLinks(final Links links)
    {
        this.links = links;
    }

}
