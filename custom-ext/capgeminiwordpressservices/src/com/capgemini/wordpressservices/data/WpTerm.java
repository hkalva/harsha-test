package com.capgemini.wordpressservices.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class WpTerm
{

    /**
     * the taxonomy.
     */
    @SerializedName("taxonomy")
    @Expose
    private String taxonomy;

    /**
     * the embeddable flag.
     */
    @SerializedName("embeddable")
    @Expose
    private Boolean embeddable;

    /**
     * the href.
     */
    @SerializedName("href")
    @Expose
    private String href;

    /**
     * @return String
     */
    public String getTaxonomy()
    {
        return taxonomy;
    }

    /**
     * @param taxonomy
     *            - the taxonomy
     */
    public void setTaxonomy(final String taxonomy)
    {
        this.taxonomy = taxonomy;
    }

    /**
     * @return Boolean
     */
    public Boolean getEmbeddable()
    {
        return embeddable;
    }

    /**
     * @param embeddable
     *            - the embeddable flag.
     */
    public void setEmbeddable(final Boolean embeddable)
    {
        this.embeddable = embeddable;
    }

    /**
     * @return String
     */
    public String getHref()
    {
        return href;
    }

    /**
     * @param href
     *            - the href.
     */
    public void setHref(final String href)
    {
        this.href = href;
    }

}
