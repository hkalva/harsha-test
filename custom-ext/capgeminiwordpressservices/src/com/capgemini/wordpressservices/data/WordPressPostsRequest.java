/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.wordpressservices.data;

/**
 * the word press retrieve posts api supports a set of arguments. right now we only need "slug" but if additional
 * arguments are needed in the future, they can be added here.
 */
public class WordPressPostsRequest
{

    /**
     * the slug.
     */
    private String slug;

    /**
     * @return String
     */
    public String getSlug()
    {
        return slug;
    }

    /**
     * @param slug
     *            - the slug.
     */
    public void setSlug(final String slug)
    {
        this.slug = slug;
    }

}
