package com.capgemini.wordpressservices.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class Cury
{

    /**
     * the name.
     */
    @SerializedName("name")
    @Expose
    private String name;

    /**
     * the href.
     */
    @SerializedName("href")
    @Expose
    private String href;

    /**
     * the templated flag.
     */
    @SerializedName("templated")
    @Expose
    private Boolean templated;

    /**
     * @return String
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name
     *            - the name.
     */
    public void setName(final String name)
    {
        this.name = name;
    }

    /**
     * @return String
     */
    public String getHref()
    {
        return href;
    }

    /**
     * @param href
     *            - the href.
     */
    public void setHref(final String href)
    {
        this.href = href;
    }

    /**
     * @return Boolean
     */
    public Boolean getTemplated()
    {
        return templated;
    }

    /**
     * @param templated
     *            - the templated flag.
     */
    public void setTemplated(final Boolean templated)
    {
        this.templated = templated;
    }

}
