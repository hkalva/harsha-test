/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.wordpressservices.data;

/**
 *
 */
public class WordPressPage
{

    private String content;

    private String title;

    public String getContent()
    {
        return content;
    }

    public String getTitle()
    {
        return title;
    }

    public void setContent(final String content)
    {
        this.content = content;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

}
