package com.capgemini.wordpressservices.data;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class Links
{

    /**
     * a list of Self objects.
     */
    @SerializedName("self")
    @Expose
    private List<Self> self = null;

    /**
     * a list of Collection objects.
     */
    @SerializedName("collection")
    @Expose
    private List<Collection> collection = null;

    /**
     * a list of About objects.
     */
    @SerializedName("about")
    @Expose
    private List<About> about = null;

    /**
     * a list of Author objects.
     */
    @SerializedName("author")
    @Expose
    private List<Author> author = null;

    /**
     * a list of Reply objects.
     */
    @SerializedName("replies")
    @Expose
    private List<Reply> replies = null;

    /**
     * a list of VersionHistory objects.
     */
    @SerializedName("version-history")
    @Expose
    private List<VersionHistory> versionHistory = null;

    /**
     * a list of WpAttachment objects.
     */
    @SerializedName("wp:attachment")
    @Expose
    private List<WpAttachment> wpAttachment = null;

    /**
     * a list WpTerm objects.
     */
    @SerializedName("wp:term")
    @Expose
    private List<WpTerm> wpTerm = null;

    /**
     * a list of Cury objects.
     */
    @SerializedName("curies")
    @Expose
    private List<Cury> curies = null;

    /**
     * @return List<Self>
     */
    public List<Self> getSelf()
    {
        return self;
    }

    /**
     * @param self
     *            - a list of Self objects.
     */
    public void setSelf(final List<Self> self)
    {
        this.self = self;
    }

    /**
     * @return List<Collection>
     */
    public List<Collection> getCollection()
    {
        return collection;
    }

    /**
     * @param collection
     *            - a list of Collection objects.
     */
    public void setCollection(final List<Collection> collection)
    {
        this.collection = collection;
    }

    /**
     * @return List<About>
     */
    public List<About> getAbout()
    {
        return about;
    }

    /**
     * @param about
     *            - a list of About objects.
     */
    public void setAbout(final List<About> about)
    {
        this.about = about;
    }

    /**
     * @return List<Author>
     */
    public List<Author> getAuthor()
    {
        return author;
    }

    /**
     * @param author
     *            - a list of Author objects.
     */
    public void setAuthor(final List<Author> author)
    {
        this.author = author;
    }

    /**
     * @return List<Reply>
     */
    public List<Reply> getReplies()
    {
        return replies;
    }

    /**
     * @param replies
     *            - a list of Reply objects
     */
    public void setReplies(final List<Reply> replies)
    {
        this.replies = replies;
    }

    /**
     * @return List<VersionHistory>
     */
    public List<VersionHistory> getVersionHistory()
    {
        return versionHistory;
    }

    /**
     * @param versionHistory
     *            - a list of VersionHistory objects.
     */
    public void setVersionHistory(final List<VersionHistory> versionHistory)
    {
        this.versionHistory = versionHistory;
    }

    /**
     * @return List<WpAttachment>
     */
    public List<WpAttachment> getWpAttachment()
    {
        return wpAttachment;
    }

    /**
     * @param wpAttachment
     *            - a list of WpAttachment objects.
     */
    public void setWpAttachment(final List<WpAttachment> wpAttachment)
    {
        this.wpAttachment = wpAttachment;
    }

    /**
     * @return List<WpTerm>
     */
    public List<WpTerm> getWpTerm()
    {
        return wpTerm;
    }

    /**
     * @param wpTerm
     *            - a list of WpTerm objects.
     */
    public void setWpTerm(final List<WpTerm> wpTerm)
    {
        this.wpTerm = wpTerm;
    }

    /**
     * @return List<Cury>
     */
    public List<Cury> getCuries()
    {
        return curies;
    }

    /**
     * @param curies
     *            - a list of Cury objects.
     */
    public void setCuries(final List<Cury> curies)
    {
        this.curies = curies;
    }

}
