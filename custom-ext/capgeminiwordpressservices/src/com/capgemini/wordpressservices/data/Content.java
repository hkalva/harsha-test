package com.capgemini.wordpressservices.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class Content
{

    /**
     * the rendered attribute.
     */
    @SerializedName("rendered")
    @Expose
    private String rendered;

    /**
     * the protected flag.
     */
    @SerializedName("protected")
    @Expose
    private Boolean _protected;

    /**
     * @return String
     */
    public String getRendered()
    {
        return rendered;
    }

    /**
     * @param rendered
     *            - the rendered attribute.
     */
    public void setRendered(final String rendered)
    {
        this.rendered = rendered;
    }

    /**
     * @return Boolean
     */
    public Boolean getProtected()
    {
        return _protected;
    }

    /**
     * @param _protected
     *            - the protected flag.
     */
    public void setProtected(final Boolean _protected)
    {
        this._protected = _protected;
    }

}
