package com.capgemini.wordpressservices.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class Title
{

    /**
     * the rendered attribute.
     */
    @SerializedName("rendered")
    @Expose
    private String rendered;

    /**
     * @return String
     */
    public String getRendered()
    {
        return rendered;
    }

    /**
     * @param rendered
     *            - the rendered attributed.
     */
    public void setRendered(final String rendered)
    {
        this.rendered = rendered;
    }

}
