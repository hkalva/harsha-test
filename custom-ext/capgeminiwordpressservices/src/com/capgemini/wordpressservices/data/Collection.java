package com.capgemini.wordpressservices.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class Collection
{

    /**
     * the href.
     */
    @SerializedName("href")
    @Expose
    private String href;

    /**
     * @return String
     */
    public String getHref()
    {
        return href;
    }

    /**
     * @param href
     *            - the href.
     */
    public void setHref(final String href)
    {
        this.href = href;
    }

}
