package com.capgemini.wordpressservices.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class Author
{

	/**
	 * flag for embeddable.
	 */
	@SerializedName("embeddable")
	@Expose
	private Boolean embeddable;

	/**
	 * the href.
	 */
	@SerializedName("href")
	@Expose
	private String href;

	/**
	 * @return Boolean
	 */
	public Boolean getEmbeddable()
	{
		return embeddable;
	}

	/**
	 * @param embeddable
	 *           - flag for embeddable.
	 */
	public void setEmbeddable(final Boolean embeddable)
	{
		this.embeddable = embeddable;
	}

	/**
	 * @return String
	 */
	public String getHref()
	{
		return href;
	}

	/**
	 * @param href
	 *           - the href.
	 */
	public void setHref(final String href)
	{
		this.href = href;
	}

}
