/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.wordpressservices.exception;

/**
 *
 */
public class WordPressException extends RuntimeException
{

    public WordPressException()
    {
        super();
    }

    public WordPressException(final String message)
    {
        super(message);
    }

    public WordPressException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public WordPressException(final Throwable cause)
    {
        super(cause);
    }
}
