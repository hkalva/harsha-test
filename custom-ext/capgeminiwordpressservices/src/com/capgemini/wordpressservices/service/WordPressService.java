/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.wordpressservices.service;

import com.capgemini.wordpressservices.data.WordPressPage;
import com.capgemini.wordpressservices.data.WordPressPostsRequest;
import com.capgemini.wordpressservices.data.WordPressPostsResponse;


/**
 * services to retrieve content from Word Press.
 */
public interface WordPressService
{
    /**
     * Get the posts based on the word press request
     *
     * @param request
     *            - The post request for word press
     * @return WordPressPostsResponse returns the posts
     */
    WordPressPostsResponse getPosts(WordPressPostsRequest request);

    /**
     * Get the content based on the word press request.
     *
     * @param request
     *            the posts
     * @param retrieveHtmlFromGuid
     *            - true or false based on what to retrieve
     * @return String - the content based on the request
     */
    String getContent(WordPressPostsRequest request, boolean retrieveHtmlFromGuid);

    /**
     * Get the content based on the word press request.
     *
     * @param request
     *            the posts
     * @param retrieveHtmlFromGuid
     *            - true or false based on what to retrieve
     * @param errorIfEmpty
     *            - throw an exception if content is empty.
     * @return String - the content based on the request
     */
    String getContent(WordPressPostsRequest request, boolean retrieveHtmlFromGuid, boolean errorIfEmpty);

    /**
     * Get the word press page.
     *
     * @param request
     *            the posts
     * @param retrieveHtmlFromGuid
     *            - true or false based on what to retrieve
     * @param errorIfEmpty
     *            - throw an exception if content is empty.
     * @return WordPressPage
     */
    WordPressPage getPage(WordPressPostsRequest request, boolean retrieveHtmlFromGuid, boolean errorIfEmpty);

}
