/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.wordpressservices.service.impl;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.capgemini.wordpressservices.data.WordPressPage;
import com.capgemini.wordpressservices.data.WordPressPostsRequest;
import com.capgemini.wordpressservices.data.WordPressPostsResponse;
import com.capgemini.wordpressservices.exception.WordPressException;
import com.capgemini.wordpressservices.service.WordPressService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


/**
 * implementing class for word press service interface.
 */
public class DefaultWordPressService implements WordPressService
{

    /**
     * the logger.
     */
    private static final Logger LOG = Logger.getLogger(DefaultWordPressService.class);

    /**
     * constant for wordpress posts url property.
     */
    private static final String PROPERTY_WORD_PRESS_POSTS_URL = "wordpress.posts.url";

    /**
     * constant for retrieve from guid property.
     */
    private static final String PROPERTY_WORD_PRESS_RETRIEVE_HTML_FROM_GUID = "wordpress.retrieve.html.from.guid";

    /**
     * constant for connect timeout property.
     */
    private static final String PROPERTY_WORD_PRESS_TIMEOUT_CONNECT = "wordpress.timeout.connect";

    /**
     * constant for read timeout property.
     */
    private static final String PROPERTY_WORD_PRESS_TIMEOUT_READ = "wordpress.timeout.read";

    /**
     * the site config service.
     */
    private SiteConfigService siteConfigService;

    @Override
    public WordPressPostsResponse getPosts(final WordPressPostsRequest request)
    {
        WordPressPostsResponse result = null;

        try
        {
            ServicesUtil.validateParameterNotNullStandardMessage("request", request);

            //build the url for the service passing in the slug id
            final RestTemplate restTemplate = getRestTemplate();
            final StringBuilder url = new StringBuilder();
            url.append(getSiteConfigService().getProperty(PROPERTY_WORD_PRESS_POSTS_URL));
            url.append(request.getSlug());

            if (LOG.isDebugEnabled())
            {
                LOG.debug("Word Press posts url: " + url.toString());
            }

            final String jsonString = restTemplate.getForObject(url.toString(), String.class);

            if (LOG.isDebugEnabled())
            {
                LOG.debug("Word Press posts JSON response: " + jsonString);
            }

            //retrieve the JSON object returned by the service
            final Type responsesType = (new WordPressTypeToken()).getType();
            final Gson gson = new Gson();
            final Collection<WordPressPostsResponse> responses = gson.fromJson(jsonString, responsesType);

            if (CollectionUtils.isNotEmpty(responses))
            {
                result = responses.iterator().next();
            }
        }
        catch (final Exception e)
        {
            throw new WordPressException(e);
        }

        return result;
    }

    @Override
    public String getContent(final WordPressPostsRequest request, final boolean retrieveHtmlFromGuid)
    {
        return getContent(request, retrieveHtmlFromGuid, false);
    }

    @Override
    public String getContent(final WordPressPostsRequest request, final boolean retrieveHtmlFromGuid, final boolean errorIfEmpty)
    {
        return getPage(request, retrieveHtmlFromGuid, errorIfEmpty).getContent();
    }

    protected boolean isRetrieveHtmlFromGuid(final boolean retrieveHtmlFromGuid)
    {
        return retrieveHtmlFromGuid && Boolean.TRUE
                .equals(Boolean.valueOf(getSiteConfigService().getProperty(PROPERTY_WORD_PRESS_RETRIEVE_HTML_FROM_GUID)));
    }

    protected String getGuid(final WordPressPostsResponse response)
    {
        String result = null;

        if ((response != null) && (response.getGuid() != null) && StringUtils.isNotBlank(response.getGuid().getRendered()))
        {
            result = response.getGuid().getRendered();
        }

        return result;
    }

    @Override
    public WordPressPage getPage(final WordPressPostsRequest request, final boolean retrieveHtmlFromGuid,
            final boolean errorIfEmpty)
    {
        final WordPressPage result = new WordPressPage();
        String content = null;
        String title = null;
        final WordPressPostsResponse response = getPosts(request);

        //retrieve the HTML snippet from the JSON object
        if ((response == null) || (response.getContent() == null))
        {
            LOG.warn("Word Press response or content is null");
        }
        else
        {
            content = response.getContent().getRendered();

            if (response.getTitle() != null)
            {
                title = response.getTitle().getRendered();
            }
        }

        //if there's no HTML snippet from the JSON object,
        //retrieve the HTML from a URL provided in the JSON
        //object
        if (StringUtils.isBlank(content) && isRetrieveHtmlFromGuid(retrieveHtmlFromGuid))
        {
            final String guid = getGuid(response);

            if (StringUtils.isNotBlank(guid))
            {
                content = getHtml(guid);
            }
        }

        if (StringUtils.isBlank(content) && errorIfEmpty)
        {
            throw new WordPressException(String.format("No content found for slug '%s'", request.getSlug()));
        }

        result.setContent(content);
        result.setTitle(title);
        return result;
    }

    @Required
    public void setSiteConfigService(final SiteConfigService siteConfigService)
    {
        this.siteConfigService = siteConfigService;
    }

    protected SiteConfigService getSiteConfigService()
    {
        return siteConfigService;
    }

    protected RestTemplate getRestTemplate()
    {
        final SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setConnectTimeout(getConnectTimeoutInMillis());
        requestFactory.setReadTimeout(getReadTimeoutInMillis());
        return new RestTemplate(requestFactory);
    }

    protected int getConnectTimeoutInMillis()
    {
        final String timeout = getSiteConfigService().getProperty(PROPERTY_WORD_PRESS_TIMEOUT_CONNECT);
        return Integer.parseInt(timeout);
    }

    protected int getReadTimeoutInMillis()
    {
        final String timeout = getSiteConfigService().getProperty(PROPERTY_WORD_PRESS_TIMEOUT_READ);
        return Integer.parseInt(timeout);
    }

    protected String getHtml(final String url)
    {
        final StringBuilder result = new StringBuilder();
        BufferedReader bufferedReader = null;

        //capture the HTML from a URL
        try
        {
            final URL website = new URL(url);
            final URLConnection connection = website.openConnection();
            connection.setConnectTimeout(getConnectTimeoutInMillis());
            connection.setReadTimeout(getReadTimeoutInMillis());
            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));
            String inputLine;

            while ((inputLine = bufferedReader.readLine()) != null)
            {
                result.append(inputLine);
            }
        }
        catch (final IOException e)
        {
            throw new WordPressException(String.format("Error readcurrentCatalogVersioning content from '%s'", url), e);
        }
        finally
        {
            if (bufferedReader != null)
            {
                try
                {
                    bufferedReader.close();
                }
                catch (final IOException ioe)
                {
                    LOG.warn(ioe);
                }
            }

        }

        return result.toString();
    }

    private static final class WordPressTypeToken extends TypeToken<Collection<WordPressPostsResponse>>
    {
        // no implementation.
    }

}
