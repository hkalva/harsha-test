/**
 *
 */
package com.capgemini.quickview.interceptors;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;


/**
 * @author lyonscg
 *
 */
public class QuickViewOverrideBeforeViewHandler implements BeforeViewHandler
{

	@Override
	public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView)
			throws Exception
	{
		if (("fragments/product/quickViewPopup").equals(modelAndView.getViewName()))
		{
			modelAndView.setViewName("addon:/capgeminiquickviewaddon/fragments/product/quickViewPopup");
		}

	}

}
