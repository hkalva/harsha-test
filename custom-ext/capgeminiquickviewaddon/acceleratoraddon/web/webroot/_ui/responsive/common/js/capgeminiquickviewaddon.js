var CapgeminiQuickViewAddon = (function() {
    var $document = $(document);

    $document.on('mouseenter', '.product-item', function(e) {
        e.preventDefault();
        var $self = $(this);
        $quickviewButton = $('.quickview-button', $self);
        if($quickviewButton.length > 0) {
            $quickviewButton.show();
        } else {
            $self.append('<div class="quickview-button btn btn-primary">' + ACC.addons.capgeminiquickviewaddon['button.quickView'] + '</div>');
        }
    }).on('mouseleave', '.product-item', function(e) {
        e.preventDefault();
        $('.quickview-button', $(this)).hide();
    }).on('click', '.quickview-button', function(e) {
        e.preventDefault();
        $('.quick-view-popup-container').remove();
        if($('.quick-view-popup-container').length < 1 ){
            $('body').append('<div class="quick-view-popup-container"></div>');
        }
        $('.quick-view-popup-container').load($(this).siblings('a.thumb').attr('href') + '/quickView', '', initBootstrapQuickview);
        initBootstrapQuickview();
    
    });

    function initBootstrapQuickview(){
        ACC.quickview.initQuickviewLightbox();
        $('#quick-view-popup-modal').modal('show');
    }
});

$(document).ready(function() {
    CapgeminiQuickViewAddon();
}); 