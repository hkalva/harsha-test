/**
 *
 */
package com.lyonscg.contentsearch.search.impl;

import de.hybris.platform.solrfacetsearch.search.QueryField;
import de.hybris.platform.solrfacetsearch.search.impl.DefaultSolrQueryConverter;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;


/**
 * Overrides the DefaultSolrQueryConvert to allow a configurable list of query fields
 * 
 *
 */
public class ContentSearchSolrQueryConverter extends DefaultSolrQueryConverter
{
	private List<String> filterQueryFields;

	/**
	 * Method overriden to include other fields than catalogId or catalogVersion in the list of filter fields.
	 */
	@Override
	protected boolean isFilterQueryField(final QueryField queryField, final Map<String, IndexedFacetInfo> facetInfoMap)
	{
		final String field = queryField.getField();
		final boolean parentReturn = super.isFilterQueryField(queryField, facetInfoMap);

		return parentReturn || this.filterQueryFields.contains(field);
	}

	@Required
	public void setFilterQueryFields(final List<String> filterQueryFields)
	{
		this.filterQueryFields = filterQueryFields;
	}
}
