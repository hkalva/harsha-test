/**
 *
 */
package com.lyonscg.contentsearch.solrfacetsearch.facade;

import de.hybris.platform.commercefacades.search.data.SearchStateData;

import java.util.Collection;

import com.lyonscg.contentsearch.data.ContentData;
import com.lyonscg.contentsearch.data.SearchStoreData;




/**
 * Facade to provide the content search functionality to the front end.
 *
 *
 */
public interface ContentSearchFacade
{
	/**
	 * Invokes the solr search based on the information passed using the SearchStateData object.
	 *
	 * @param searchState
	 * @return A collection on ContentData object with the relevant content search results.
	 */
	Collection<ContentData> contentTextSearch(String text);

	/**
	 * Invokes the solr search based on the information passed using the SearchStateData object.
	 *
	 * @param searchState
	 * @return A collection on ContentData object with the relevant content search results.
	 */
	Collection<ContentData> contentTextSearch(SearchStateData searchState);

	/**
	 * Invokes the solr search based on the information passed using the SearchStateData object.
	 *
	 * @param searchState
	 * @return A collection on ContentData object with the relevant content search results.
	 */
	Collection<SearchStoreData> storeTextSearch(String text);

	/**
	 * Invokes the solr search based on the information passed using the SearchStateData object.
	 *
	 * @param searchState
	 * @return A collection on ContentData object with the relevant content search results.
	 */
	Collection<SearchStoreData> storeTextSearch(SearchStateData searchState);

}
