/**
 *
 */
package com.lyonscg.contentsearch.solrfacetsearch.facade.impl;

import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.threadcontext.ThreadContextService;
import de.hybris.platform.commerceservices.threadcontext.ThreadContextService.Nothing;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.lyonscg.contentsearch.constants.LyonscgcontentsearchaddonConstants;
import com.lyonscg.contentsearch.data.ContentData;
import com.lyonscg.contentsearch.data.SearchStoreData;
import com.lyonscg.contentsearch.solrfacetsearch.facade.ContentSearchFacade;
import com.lyonscg.contentsearch.solrfacetsearch.service.ContentSearchService;


/**
 * Facade to provide the content search functionality to the front end.
 *
 *
 */
public class ContentSearchFacadeImpl implements ContentSearchFacade
{
	private Converter<SearchResultValueData, ContentData> contentSearchResultConverter;
	private Converter<SearchResultValueData, SearchStoreData> storeSearchResultConverter;
	private Converter<SearchQueryData, SolrSearchQueryData> searchQueryDecoder;
	private ContentSearchService contentSearchService;
	private ThreadContextService threadContextService;


	@Override
	public Collection<ContentData> contentTextSearch(final String text)
	{
		return this.getThreadContextService().executeInContext(
				new ThreadContextService.Executor<Collection<ContentData>, ThreadContextService.Nothing>()
				{

					@Override
					public Collection<ContentData> execute() throws Nothing
					{
						final SearchPageData<SearchResultValueData> contentSearch = contentSearchService.searchText(text,
								LyonscgcontentsearchaddonConstants.CONTENT_SEARCH);
						return Converters.convertAll(contentSearch.getResults(), contentSearchResultConverter);
					}
				});
	}

	@Override
	public Collection<ContentData> contentTextSearch(final SearchStateData searchState)
	{
		Assert.notNull(searchState, "SearchStateData must not be null.");

		return this.getThreadContextService().executeInContext(
				new ThreadContextService.Executor<Collection<ContentData>, ThreadContextService.Nothing>()
				{

					@Override
					public Collection<ContentData> execute() throws Nothing
					{
						final SearchPageData<SearchResultValueData> contentSearch = contentSearchService.search(
								decodeState(searchState), LyonscgcontentsearchaddonConstants.CONTENT_SEARCH);
						return Converters.convertAll(contentSearch.getResults(), contentSearchResultConverter);
					}
				});
	}


	@Override
	public Collection<SearchStoreData> storeTextSearch(final String text)
	{
		return this.getThreadContextService().executeInContext(
				new ThreadContextService.Executor<Collection<SearchStoreData>, ThreadContextService.Nothing>()
				{

					@Override
					public Collection<SearchStoreData> execute() throws Nothing
					{
						final SearchPageData<SearchResultValueData> storeSearch = contentSearchService.searchText(text,
								LyonscgcontentsearchaddonConstants.STORE_SEARCH);
						return Converters.convertAll(storeSearch.getResults(), storeSearchResultConverter);
					}
				});
	}


	@Override
	public Collection<SearchStoreData> storeTextSearch(final SearchStateData searchState)
	{
		Assert.notNull(searchState, "SearchStateData must not be null.");

		return this.getThreadContextService().executeInContext(
				new ThreadContextService.Executor<Collection<SearchStoreData>, ThreadContextService.Nothing>()
				{

					@Override
					public Collection<SearchStoreData> execute() throws Nothing
					{
						final SearchPageData<SearchResultValueData> contentSearch = contentSearchService.search(
								decodeState(searchState), LyonscgcontentsearchaddonConstants.STORE_SEARCH);
						return Converters.convertAll(contentSearch.getResults(), storeSearchResultConverter);
					}
				});
	}

	/**
	 * Decodes the searchState to convert it into a SolrSearchQueryData. It could be easier to use a simple String, but
	 * trying to maintain the hybris-solr structure as much as possible for future improvements on the extension.
	 *
	 * @param searchState
	 * @return A SolrSearchQueryData object.
	 */
	protected SolrSearchQueryData decodeState(final SearchStateData searchState)
	{
		return this.searchQueryDecoder.convert(searchState.getQuery());
	}

	protected ThreadContextService getThreadContextService()
	{
		return threadContextService;
	}

	@Required
	public void setThreadContextService(final ThreadContextService threadContextService)
	{
		this.threadContextService = threadContextService;
	}

	@Required
	public void setContentSearchService(final ContentSearchService contentSearchService)
	{
		this.contentSearchService = contentSearchService;
	}

	@Required
	public void setContentSearchResultConverter(final Converter<SearchResultValueData, ContentData> contentSearchResultConverter)
	{
		this.contentSearchResultConverter = contentSearchResultConverter;
	}

	@Required
	public void setStoreSearchResultConverter(final Converter<SearchResultValueData, SearchStoreData> storeSearchResultConverter)
	{
		this.storeSearchResultConverter = storeSearchResultConverter;
	}

	@Required
	public void setSearchQueryDecoder(final Converter<SearchQueryData, SolrSearchQueryData> searchQueryDecoder)
	{
		this.searchQueryDecoder = searchQueryDecoder;
	}


}