/**
 *
 */
package com.lyonscg.contentsearch.solrfacetsearch.populators;

import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.lyonscg.contentsearch.data.SearchStoreData;




/**
 * Populates the information retrieve from the store content search into a SearchStoreData object to be sent to the
 * front end.
 *
 *
 */
public class StoreSearchResultPopulator implements Populator<SearchResultValueData, SearchStoreData>
{

	@Override
	public void populate(final SearchResultValueData source, final SearchStoreData target) throws ConversionException
	{
		//populate page id, page name and label
		target.setName(this.<String> getValue(source, "name"));
		target.setDisplayName(this.<String> getValue(source, "displayName"));
		target.setUrl(this.<String> getValue(source, "url"));

	}

	/**
	 * Gets the value of the property with propertyName from the list of results from solr.
	 *
	 * @param source
	 * @param propertyName
	 * @return the value of the property with propertyName from the list of results from solr.
	 */
	protected <T> T getValue(final SearchResultValueData source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}

		// DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
		return (T) source.getValues().get(propertyName);
	}
}
