/**
 *
 */
package com.lyonscg.contentsearch.solrfacetsearch.populators;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.exceptions.NoValidSolrConfigException;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Collection;
import java.util.List;


/**
 * Extends the ContentSearchSolrQueryPopulator to add the base store as a query field for the stores. Product and
 * content search add catalogId and catalogVersion as query fields for all the solr searchs. Stores are not catalog
 * aware but they coult be considered store aware. This populator guarantes that stores retrieved in the search are from
 * the current base store.
 *
 *
 */
public class StoreSearchSolrQueryPopulator<INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE> extends
		ContentSearchSolrQueryPopulator<INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE>
{

	/**
	 * Extends the ContentSearchSolrQueryPopulator to add the base store as a query field for the stores.
	 */
	@Override
	public void populate(
			final SearchQueryPageableData<SolrSearchQueryData> source,
			final SolrSearchRequest<FacetSearchConfig, IndexedType, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE> target)
			throws ConversionException
	{
		super.populate(source, target);

		//add a query field for each currentSite base store
		if (null != getBaseSiteService().getCurrentBaseSite())
		{
			final List<BaseStoreModel> baseStores = getBaseSiteService().getCurrentBaseSite().getStores();
			for (final BaseStoreModel baseStore : baseStores)
			{
				target.getSearchQuery().searchInField("baseStore", baseStore.getPk().toString(), SearchQuery.Operator.OR);
			}
		}
	}

	/**
	 * Method overwritten as stores are not catalog aware, therefore list of catalogs not required for the SolrQuery.
	 */
	@Override
	protected Collection<CatalogVersionModel> getSessionCatalogVersions()
	{
		return null;
	}

	/**
	 * Method overwritten to return the store search configuration instead of the content search.
	 */
	@Override
	protected FacetSearchConfig getFacetSearchConfig() throws NoValidSolrConfigException
	{
		FacetSearchConfig facetSearchConfig = null;
		if (null != getBaseSiteService().getCurrentBaseSite()
				&& null != getBaseSiteService().getCurrentBaseSite().getSolrStoreFacetSearchConfiguration())
		{
			facetSearchConfig = getFacetSearchConfigConverter().convert(
					getBaseSiteService().getCurrentBaseSite().getSolrStoreFacetSearchConfiguration());
		}
		else
		{
			final String currentBaseSite = null != getBaseSiteService().getCurrentBaseSite() ? getBaseSiteService()
					.getCurrentBaseSite().getName() : null;
			throw new NoValidSolrConfigException("Solr Content facet search configuration not found for current base site "
					+ currentBaseSite);
		}
		return facetSearchConfig;
	}
}
