/**
 *
 */
package com.lyonscg.contentsearch.solrfacetsearch.provider.impl;

import de.hybris.platform.commerceservices.model.storelocator.StoreLocatorFeatureModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Value provider to index the localised Point of Service feature names.
 *
 *
 */
public class StoreFeatureValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider, Serializable
{
	private static final Logger LOG = LoggerFactory.getLogger(StoreFeatureValueProvider.class);

	private FieldNameProvider fieldNameProvider;

	private final Map<String, Locale> locales = new HashMap<String, Locale>();

	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		final Collection<FieldValue> fieldValues = new ArrayList<FieldValue>();

		//check that the object is of type Point of Service
		if (model instanceof PointOfServiceModel)
		{
			final PointOfServiceModel pointOfService = (PointOfServiceModel) model;
			if (indexedProperty.isLocalized())
			{
				final Collection<LanguageModel> languages = indexConfig.getLanguages();
				if (null != languages)
				{
					for (final LanguageModel language : languages)
					{
						fieldValues.addAll(createFieldValue(pointOfService, language, indexedProperty));
					}
				}
				else
				{
					LOG.error("No language configured for the current indexConfig.");
				}
			}
			else
			{
				LOG.error("Trying to use a localised value provider (StoreFeatureValueProvider) for a non localised property {}",
						indexedProperty.getName());
			}
		}
		else
		{
			LOG.error("Not possible to index store features for the object {}, it is not a Point of Service", model.toString());
		}

		return fieldValues;
	}

	/**
	 * Creates a field value per feature and language that is added to the global list of field values.
	 *
	 * @param pos
	 * @param language
	 * @param indexedProperty
	 * @return
	 */
	protected List<FieldValue> createFieldValue(final PointOfServiceModel pos, final LanguageModel language,
			final IndexedProperty indexedProperty)
	{
		final List<FieldValue> fieldValues = new ArrayList<FieldValue>();
		final Collection<StoreLocatorFeatureModel> storeFeatures = pos.getFeatures();

		//index store feature names if storeFeatures available for the store
		if (null != storeFeatures)
		{
			for (final StoreLocatorFeatureModel storeFeature : storeFeatures)
			{
				final String propertyLanguage = language == null ? null : language.getIsocode();
				final Collection<String> fieldNames = this.fieldNameProvider.getFieldNames(indexedProperty, propertyLanguage);
				for (final String fieldName : fieldNames)
				{
					fieldValues.add(new FieldValue(fieldName, storeFeature.getName(this.getLocale(propertyLanguage))));
				}
			}
		}

		return fieldValues;
	}

	/**
	 * Gets the Locale object based on the language iso code. This method guarantees that only one Locale per language is
	 * created.
	 *
	 * @param language
	 * @return the Locale object based on the language iso code
	 */
	private Locale getLocale(final String language)
	{
		Locale locale = null;
		if (this.locales.containsKey(language))
		{
			locale = this.locales.get(language);
		}
		else
		{
			locale = new Locale(language);
			this.locales.put(language, locale);
		}

		return locale;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}
}
