/**
 *
 */
package com.lyonscg.contentsearch.solrfacetsearch.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Value provider to index the store url so the processing time in front end is reduced.
 *
 *
 */
public class UrlValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider, Serializable
{
	private static Logger LOG = LoggerFactory.getLogger(UrlValueProvider.class);
	private static final String URL_PREFIX = "/store/";
	private static final String SPACE = " ";
	private static final String UNDERSCORE = "_";
	private static final String NON_SPECIAL_CHARACTER_REGEX = "[^a-zA-Z0-9_]+";

	private FieldNameProvider fieldNameProvider;


	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		final Collection<FieldValue> fieldValues = new ArrayList<FieldValue>();
		//check that the object is a point of service, if not throw an exception
		if (model instanceof PointOfServiceModel)
		{
			final PointOfServiceModel pointOfService = (PointOfServiceModel) model;
			fieldValues.addAll(this.createFieldValue(pointOfService, indexedProperty));
		}
		else
		{
			LOG.error("Not possible to index store url for the object {} as it is not a PointOfService", model.toString());
		}

		return fieldValues;
	}

	/**
	 * Creates the store url and adds it to the indexed properties for the store.
	 *
	 * @param pos
	 * @param indexedProperty
	 * @return
	 */
	protected List<FieldValue> createFieldValue(final PointOfServiceModel pos, final IndexedProperty indexedProperty)
	{
		final List<FieldValue> fieldValues = new ArrayList<FieldValue>();
		final Collection<String> fieldNames = this.fieldNameProvider.getFieldNames(indexedProperty, null);

		for (final String fieldName : fieldNames)
		{
			//The following piece of code has been copied from SearchPagePointOfServiceDistancePopulator to use the same logic while building the store url
			String storeName = StringUtils.EMPTY;
			if (null != pos.getDisplayName())
			{
				storeName = pos.getDisplayName().replaceAll(UrlValueProvider.SPACE, UrlValueProvider.UNDERSCORE).toLowerCase();
				storeName = storeName.replaceAll(UrlValueProvider.NON_SPECIAL_CHARACTER_REGEX, StringUtils.EMPTY);
			}
			final StringBuilder urlSb = new StringBuilder(UrlValueProvider.URL_PREFIX);

			//if storeName is not empty, add store name + / to the url
			if (StringUtils.isNotEmpty(storeName))
			{
				urlSb.append(storeName).append("/");
			}

			//in any case, include the point of service name.
			urlSb.append(pos.getName());
			fieldValues.add(new FieldValue(fieldName, urlSb.toString()));
		}

		return fieldValues;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}
}
