/**
 *
 */
package com.lyonscg.contentsearch.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * Value provider to index all the address details for a Point of Service.
 *
 *
 */
public class AddressValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider, Serializable
{
	private static final Logger LOG = LoggerFactory.getLogger(AddressValueProvider.class);

	private FieldNameProvider fieldNameProvider;

	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		final Collection<FieldValue> fieldValues = new ArrayList<FieldValue>();
		//check that the object is a point of service, if not throw an exception
		if (model instanceof PointOfServiceModel)
		{
			final PointOfServiceModel pointOfService = (PointOfServiceModel) model;
			if (null != pointOfService.getAddress())
			{
				fieldValues.addAll(this.createFieldValue(pointOfService.getAddress(), indexedProperty));
			}
		}
		else
		{
			LOG.error("Not possible to index and address for the object {} as it is not a PointOfService", model.toString());
		}
		return fieldValues;
	}

	/**
	 * Creates a FieldValue for each of the fields in the point of service address.
	 *
	 * @param address
	 * @param indexedProperty
	 * @return The list of FieldValues for each of the fields in the point of service address.
	 */
	protected List<FieldValue> createFieldValue(final AddressModel address, final IndexedProperty indexedProperty)
	{
		final List<FieldValue> fieldValues = new ArrayList<FieldValue>();
		final Collection<String> fieldNames = this.fieldNameProvider.getFieldNames(indexedProperty, null);

		for (final String fieldName : fieldNames)
		{
			//street name
			if (null != address.getStreetname())
			{
				fieldValues.add(new FieldValue(fieldName, address.getStreetname()));
			}
			//street number
			if (null != address.getStreetnumber())
			{
				fieldValues.add(new FieldValue(fieldName, address.getStreetnumber()));
			}
			//postal code
			if (null != address.getPostalcode())
			{
				fieldValues.add(new FieldValue(fieldName, address.getPostalcode()));
			}
			//town
			if (null != address.getTown())
			{
				fieldValues.add(new FieldValue(fieldName, address.getTown()));
			}
			//country name
			if (null != address.getCountry())
			{
				fieldValues.add(new FieldValue(fieldName, address.getCountry().getName()));
			}
		}

		return fieldValues;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}
}
