/**
 *
 */
package com.lyonscg.contentsearch.solrfacetsearch.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.cms2.model.contents.components.CMSParagraphComponentModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryTermData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchResponse;

import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.convert.converter.Converter;

import com.lyonscg.contentsearch.constants.LyonscgcontentsearchaddonConstants;
import com.lyonscg.contentsearch.solrfacetsearch.dao.ContentSearchDao;
import com.lyonscg.contentsearch.solrfacetsearch.service.ContentSearchService;


/**
 * Provides all the service methods required for the content search functionality.
 *
 *
 */
public class ContentSearchServiceImpl implements ContentSearchService
{
	private ContentSearchDao contentSearchDao;

	private Converter<SearchQueryPageableData<SolrSearchQueryData>, SolrSearchRequest> contentSearchQueryPageableConverter;
	private Converter<SearchQueryPageableData<SolrSearchQueryData>, SolrSearchRequest> storeSearchQueryPageableConverter;
	private Converter<SolrSearchRequest, SolrSearchResponse> searchRequestConverter;
	private Converter<SolrSearchResponse, SearchPageData<SearchResultValueData>> contentSearchResponseConverter;

	@Override
	public Collection<CMSParagraphComponentModel> findParagraphComponentsForContentPage(final ContentPageModel page)
	{
		return contentSearchDao.findParagraphComponentsForContentPage(page);
	}

	@Override
	public SearchPageData<SearchResultValueData> searchText(final String text, final String searchType)
	{
		final SolrSearchQueryData searchQueryData = new SolrSearchQueryData();
		searchQueryData.setFreeTextSearch(text);
		searchQueryData.setFilterTerms(Collections.<SolrSearchQueryTermData> emptyList());

		return doSearch(searchQueryData, searchType);
	}

	@Override
	public SearchPageData<SearchResultValueData> search(final SolrSearchQueryData searchQueryData, final String searchType)
	{
		return this.doSearch(searchQueryData, searchType);
	}

	/**
	 * Performs the solr search query and returns the results.
	 *
	 * @param searchQueryData
	 * @return
	 */
	protected SearchPageData<SearchResultValueData> doSearch(final SolrSearchQueryData searchQueryData, final String searchType)
	{
		validateParameterNotNull(searchQueryData, "SearchQueryData cannot be null");

		// Create the SearchQueryPageableData that contains our parameters
		final SearchQueryPageableData<SolrSearchQueryData> searchQueryPageableData = this.buildSearchQueryPageableData(
				searchQueryData, null);

		//Build up the search request based on the type of search
		final SolrSearchRequest solrSearchRequest = searchType.equals(LyonscgcontentsearchaddonConstants.CONTENT_SEARCH) ? this.contentSearchQueryPageableConverter
				.convert(searchQueryPageableData) : this.storeSearchQueryPageableConverter.convert(searchQueryPageableData);

		// Execute the search
		final SolrSearchResponse solrSearchResponse = this.searchRequestConverter.convert(solrSearchRequest);

		//Convert the response
		return this.contentSearchResponseConverter.convert(solrSearchResponse);
	}

	/**
	 * Creates a SearchQueryPageableData object to be passed into the solr logic. Content search not pageable for now.
	 *
	 * @param searchQueryData
	 * @param pageableData
	 * @return The SearchQueryPageableData created from SolrSearchQueryData.
	 */
	protected SearchQueryPageableData<SolrSearchQueryData> buildSearchQueryPageableData(final SolrSearchQueryData searchQueryData,
			final PageableData pageableData)
	{
		final SearchQueryPageableData<SolrSearchQueryData> searchQueryPageableData = new SearchQueryPageableData<SolrSearchQueryData>();
		searchQueryPageableData.setSearchQueryData(searchQueryData);
		searchQueryPageableData.setPageableData(pageableData);
		return searchQueryPageableData;
	}

	@Required
	public void setContentSearchDao(final ContentSearchDao contentSearchDao)
	{
		this.contentSearchDao = contentSearchDao;
	}

	@Required
	public void setContentSearchQueryPageableConverter(
			final Converter<SearchQueryPageableData<SolrSearchQueryData>, SolrSearchRequest> contentSearchQueryPageableConverter)
	{
		this.contentSearchQueryPageableConverter = contentSearchQueryPageableConverter;
	}

	@Required
	public void setSearchRequestConverter(final Converter<SolrSearchRequest, SolrSearchResponse> searchRequestConverter)
	{
		this.searchRequestConverter = searchRequestConverter;
	}

	@Required
	public void setContentSearchResponseConverter(
			final Converter<SolrSearchResponse, SearchPageData<SearchResultValueData>> contentSearchResponseConverter)
	{
		this.contentSearchResponseConverter = contentSearchResponseConverter;
	}

	@Required
	public void setStoreSearchQueryPageableConverter(
			final Converter<SearchQueryPageableData<SolrSearchQueryData>, SolrSearchRequest> storeSearchQueryPageableConverter)
	{
		this.storeSearchQueryPageableConverter = storeSearchQueryPageableConverter;
	}
}
