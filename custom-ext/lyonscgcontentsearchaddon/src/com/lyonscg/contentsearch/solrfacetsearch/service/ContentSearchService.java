/**
 *
 */
package com.lyonscg.contentsearch.solrfacetsearch.service;

import de.hybris.platform.cms2.model.contents.components.CMSParagraphComponentModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;

import java.util.Collection;


/**
 * Service class for the solrfacetsearch in lyonscgcontentsearch extension.
 *
 *
 */
public interface ContentSearchService
{
	/**
	 * Looks for CMSParagraphComponents associated with the content page. The CMSParagraphComponets can be associated
	 * with the page by the page or by the page template.
	 *
	 * @param page
	 * @return The list of CMSParagraphComponents associated with the content page.
	 */
	Collection<CMSParagraphComponentModel> findParagraphComponentsForContentPage(ContentPageModel page);

	/**
	 * Looks for Content Data indexed in solr using the list of converters and populators associated to the service.
	 * SolrSearchQueryData is used to create SolrSearchRequest and SolrSearchResult objects and return the search
	 * results.
	 *
	 * @param searchQueryData
	 * @param searchType
	 * @return the Search Results.
	 */
	SearchPageData<SearchResultValueData> search(SolrSearchQueryData searchQueryData, String searchType);

	/**
	 * Looks for Content Data indexed in solr using the list of converters and populators associated to the service.
	 * SolrSearchQueryData is used to create SolrSearchRequest and SolrSearchResult objects and return the search
	 * results.
	 *
	 * @param text
	 * @param searchType
	 * @return
	 */
	SearchPageData<SearchResultValueData> searchText(String text, String searchType);
}
