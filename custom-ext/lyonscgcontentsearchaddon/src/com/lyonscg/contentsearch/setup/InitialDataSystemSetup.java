/**
 *
 */
package com.lyonscg.contentsearch.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.SetupImpexService;
import de.hybris.platform.commerceservices.setup.SetupSolrIndexerService;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;

import java.util.ArrayList;
import java.util.List;

import com.lyonscg.contentsearch.constants.LyonscgcontentsearchaddonConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 *
 *
 */
@SystemSetup(extension = LyonscgcontentsearchaddonConstants.EXTENSIONNAME)
public class InitialDataSystemSetup extends AbstractSystemSetup
{
	private static final String ACTIVATE_CONTENT_SEARCH_SOLR_CRON_JOBS = "activateContentSearchSolrCronJobs";
	private static final String ACTIVATE_STORE_SEARCH_SOLR_CRON_JOBS = "activateStoreSearchSolrCronJobs";
	private static final String IMPORT_DEFAULT_SOLR_CONTENT = "importDefaultSolrContent";

	private SetupImpexService setupImpexService;
	private SetupSolrIndexerService setupSolrIndexerService;

	/**
	 * Defines the properties that can be configured from hac during system initialisation/update.
	 */
	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<SystemSetupParameter>();

		params.add(createBooleanSystemSetupParameter(InitialDataSystemSetup.IMPORT_DEFAULT_SOLR_CONTENT,
				"Import Default Content Search Config", true));
		params.add(createBooleanSystemSetupParameter(InitialDataSystemSetup.ACTIVATE_CONTENT_SEARCH_SOLR_CRON_JOBS,
				"Activate Content Search Solr Cron Jobs", true));
		params.add(createBooleanSystemSetupParameter(InitialDataSystemSetup.ACTIVATE_STORE_SEARCH_SOLR_CRON_JOBS,
				"Activate Store Search Solr Cron Jobs", true));

		return params;
	}

	/**
	 * Implement this method to create data that is used in your project. This method will be called during the system
	 * initialization.
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.PROJECT, process = Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		if (this.getBooleanSystemSetupParameter(context, InitialDataSystemSetup.IMPORT_DEFAULT_SOLR_CONTENT))
		{
			this.logInfo(context, "Importing solr configuration for Content Search");

			//import solr file for content search, create solr jobs and import trigger
			getSetupImpexService().importImpexFile("/lyonscgcontentsearch/import/solr/template/solr.impex", true);
			getSetupSolrIndexerService().createSolrIndexerCronJobs("content-search");
			getSetupSolrIndexerService().createSolrIndexerCronJobs("store-search");
			getSetupImpexService().importImpexFile("/lyonscgcontentsearch/import/solr/template/solrtrigger.impex", true);

			//if selected the option to activate content search solr cron jobs
			if (this.getBooleanSystemSetupParameter(context, ACTIVATE_CONTENT_SEARCH_SOLR_CRON_JOBS))
			{
				this.logInfo(context, "Activating solr index job for Content Search");
				getSetupSolrIndexerService().executeSolrIndexerCronJob("content-search", true);
				getSetupSolrIndexerService().activateSolrIndexerCronJobs("content-search");
			}
			//if selected the option to activate store search solr cron jobs
			if (this.getBooleanSystemSetupParameter(context, ACTIVATE_STORE_SEARCH_SOLR_CRON_JOBS))
			{
				this.logInfo(context, "Activating solr index job for Store Search");
				getSetupSolrIndexerService().executeSolrIndexerCronJob("store-search", true);
				getSetupSolrIndexerService().activateSolrIndexerCronJobs("store-search");
			}
		}
	}
}
