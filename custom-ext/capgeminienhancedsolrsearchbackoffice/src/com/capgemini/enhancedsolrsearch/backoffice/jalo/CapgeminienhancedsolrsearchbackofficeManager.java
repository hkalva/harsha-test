package com.capgemini.enhancedsolrsearch.backoffice.jalo;

import com.capgemini.enhancedsolrsearch.backoffice.constants.CapgeminienhancedsolrsearchbackofficeConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("PMD")
public class CapgeminienhancedsolrsearchbackofficeManager extends GeneratedCapgeminienhancedsolrsearchbackofficeManager
{
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger( CapgeminienhancedsolrsearchbackofficeManager.class.getName() );
	
	public static final CapgeminienhancedsolrsearchbackofficeManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (CapgeminienhancedsolrsearchbackofficeManager) em.getExtension(CapgeminienhancedsolrsearchbackofficeConstants.EXTENSIONNAME);
	}
	
}
