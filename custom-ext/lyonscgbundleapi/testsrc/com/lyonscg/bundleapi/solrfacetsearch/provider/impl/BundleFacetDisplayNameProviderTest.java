package com.lyonscg.bundleapi.solrfacetsearch.provider.impl;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;


/**
 *
 * Test class for BundleFacetDisplayNameProvider.
 */
@UnitTest
public class BundleFacetDisplayNameProviderTest
{
    @InjectMocks
    private BundleFacetDisplayNameProvider bundleFacetDisplayNameProvider;
    @Mock
    private BundleTemplateService bundleTemplateService;
    @Mock
    private SessionService sessionService;
    @Mock
    private CatalogVersionService catalogVersionService;
    @Mock
    private SearchQuery query;
    @Mock
    private CatalogVersionModel catVersion;
    @Mock
    private BundleTemplateModel bundle;
    private String bundleId = "bundleId", separator = "|", version = "1.0", isoCode = "en", bundleName = "bundleName";
    private String code = bundleId + separator + version;

    @Before
    public void setup()
    {
        bundleFacetDisplayNameProvider = new BundleFacetDisplayNameProvider();
        MockitoAnnotations.initMocks(this);
        Mockito.doReturn(isoCode).when(query).getLanguage();
        Mockito.doReturn(Collections.singletonList(catVersion)).when(query).getCatalogVersions();
        Mockito.doReturn(bundle).when(sessionService).executeInLocalView(Mockito.any());
        Mockito.doReturn(bundleName).when(bundle).getName(Mockito.any());
    }

    @Test
    public void getDisplayNameTestWithVersion()
    {
        String facetName = bundleFacetDisplayNameProvider.getDisplayName(query, code);
        Assert.assertEquals(bundleName + " " + version, facetName);
    }

    @Test
    public void getDisplayNameTestWithOutVersion()
    {
        String facetName = bundleFacetDisplayNameProvider.getDisplayName(query, bundleId);
        Assert.assertEquals(bundleName, facetName);
    }
}
