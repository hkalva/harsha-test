package com.lyonscg.bundleapi.solrfacetsearch.provider.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;


/**
 *
 * Test class for BundleUrlValueProvider.
 */
@UnitTest
public class BundleUrlValueProviderTest
{
    @InjectMocks
    private BundleUrlValueProvider bundleUrlValueProvider;
    @Mock
    private UrlResolver<BundleTemplateModel> bundleTemplateModelUrlResolver;
    @Mock
    private LanguageModel language;
    @Mock
    private BundleTemplateModel bundleTemplate;
    @Mock
    private CommonI18NService commonI18NService;
    @Mock
    private I18NService i18nService;
    private String bundleURL = "bundleURL";

    /**Setting up the bundle url information.
     */
    @Before
    public void setup()
    {
        bundleUrlValueProvider = new BundleUrlValueProvider();
        bundleUrlValueProvider.setCommonI18NService(commonI18NService);
        bundleUrlValueProvider.setI18nService(i18nService);
        MockitoAnnotations.initMocks(this);
        Mockito.doReturn(bundleURL).when(bundleTemplateModelUrlResolver).resolve(bundleTemplate);
    }

    /** Getting bundle property language.
     */
    @Test
    public void getPropertyTest()
    {
        String url = bundleUrlValueProvider.getProperty(language, bundleTemplate);
        Assert.assertEquals(url, bundleURL);
    }
}
