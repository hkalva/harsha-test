package com.lyonscg.bundleapi.solrfacetsearch.provider.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;


/**
 *
 * Test class for ProductBundleTemplatesPropertyValueProvider.
 */
@UnitTest
public class ProductBundleTemplatesPropertyValueProviderTest
{
    @InjectMocks
    private ProductBundleTemplatesPropertyValueProvider productBundleTemplatesPropertyValueProvider;
    @Mock
    private CommonI18NService commonI18NService;
    private String bundlePropertyName = BundleTemplateModel.NAME;
    @Mock
    private IndexConfig indexConfig;
    @Mock
    private IndexedProperty indexedProperty;
    @Mock
    private ProductModel productModel;
    @Mock
    private LanguageModel enLanguageModel;
    @Mock
    private BundleTemplateModel bundle;
    @Mock
    private ModelService modelService;
    @Mock
    private FieldNameProvider fieldNameProvider;
    private String bundleName = "bundleName", fieldName = "fieldName";

    @Before
    public void setup()
    {
        productBundleTemplatesPropertyValueProvider = new ProductBundleTemplatesPropertyValueProvider();
        MockitoAnnotations.initMocks(this);
        productBundleTemplatesPropertyValueProvider.setBundlePropertyName(bundlePropertyName);
        Mockito.doReturn(Collections.singletonList(bundle)).when(productModel).getBundleTemplates();
    }

    @Test
    public void getFieldValuesLocalizedTest() throws FieldValueProviderException
    {
        Mockito.doReturn(true).when(indexedProperty).isLocalized();
        Mockito.doReturn(Collections.singletonList(enLanguageModel)).when(indexConfig).getLanguages();
        Mockito.doReturn(Locale.US).when(commonI18NService).getLocaleForLanguage(enLanguageModel);
        Mockito.doReturn(Locale.US.getDisplayLanguage()).when(enLanguageModel).getIsocode();
        Mockito.doReturn(bundleName).when(modelService).getAttributeValue(bundle, bundlePropertyName, Locale.US);
        Mockito.doReturn(Collections.singletonList(fieldName)).when(fieldNameProvider)
                .getFieldNames(indexedProperty, Locale.US.getDisplayLanguage());
        Collection<FieldValue> fieldValues = productBundleTemplatesPropertyValueProvider.getFieldValues(indexConfig,
                indexedProperty, productModel);
        Assert.assertEquals(fieldValues.iterator().next().getFieldName(), fieldName);
        Assert.assertEquals(fieldValues.iterator().next().getValue(), bundleName);
    }

    @Test
    public void getFieldValuesTest() throws FieldValueProviderException
    {
        Mockito.doReturn(false).when(indexedProperty).isLocalized();
        Mockito.doReturn(bundleName).when(modelService).getAttributeValue(bundle, bundlePropertyName);
        Mockito.doReturn(Collections.singletonList(fieldName)).when(fieldNameProvider).getFieldNames(indexedProperty, null);
        Collection<FieldValue> fieldValues = productBundleTemplatesPropertyValueProvider.getFieldValues(indexConfig,
                indexedProperty, productModel);
        Assert.assertEquals(fieldValues.iterator().next().getFieldName(), fieldName);
        Assert.assertEquals(fieldValues.iterator().next().getValue(), bundleName);
    }
}
