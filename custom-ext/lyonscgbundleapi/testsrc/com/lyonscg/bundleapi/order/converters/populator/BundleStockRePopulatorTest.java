package com.lyonscg.bundleapi.order.converters.populator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;


/**
 * Unit Tests for {@link BundleStockRePopulator}.
 */
@UnitTest
public class BundleStockRePopulatorTest
{
    @InjectMocks
    private BundleStockRePopulator testClass;
    @Mock
    private Populator<AbstractOrderEntryModel, OrderEntryData> subscriptionOrderEntryPopulator;
    @Mock
    private AbstractOrderEntryModel orderEntryModel;
    private OrderEntryData entryData;
    private StockData stockData;
    private ProductData productData;
    private int bundleNo = 1;

    /**
     * Setup test data.
     */
    @Before
    public void setup()
    {
        testClass = new BundleStockRePopulator();
        MockitoAnnotations.initMocks(this);

        entryData = new OrderEntryData();
        productData = new ProductData();
        stockData = new StockData();

        entryData.setProduct(productData);
        productData.setStock(stockData);
        Mockito.doAnswer(new Answer<Object>() {
            public Object answer(InvocationOnMock invocation)
            {
                //Setting stock data to null similiar to de.hybris.platform.subscriptionfacades.order.converters.populator.SubscriptionOrderEntryPopulator
                productData.setStock(null);
                return null;
            }
        }).when(subscriptionOrderEntryPopulator).populate(orderEntryModel, entryData);
        Mockito.doReturn(bundleNo).when(orderEntryModel).getBundleNo();
    }

    /**
     * Unit test for {@link BundleStockRePopulator#populate(AbstractOrderEntryModel, OrderEntryData)}.
     */
    @Test
    public void populateTest()
    {
        testClass.populate(orderEntryModel, entryData);
        Assert.assertEquals(stockData, entryData.getProduct().getStock());
    }

    /**
     * Unit test to throw {@link IllegalArgumentException} when {@link AbstractOrderEntryModel} is null.
     */
    @Test(expected = IllegalArgumentException.class)
    public void populateCartModelNullTest()
    {
        testClass.populate(null, entryData);
    }

    /**
     * Unit test to throw {@link IllegalArgumentException} when {@link OrderEntryData} is null.
     */
    @Test(expected = IllegalArgumentException.class)
    public void populateCartDataNullTest()
    {
        testClass.populate(orderEntryModel, null);
    }
}
