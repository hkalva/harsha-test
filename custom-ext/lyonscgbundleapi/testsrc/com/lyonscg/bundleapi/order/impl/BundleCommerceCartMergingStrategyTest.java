/* */
package com.lyonscg.bundleapi.order.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.CommerceCartMergingException;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Test class for BundleCommerceCartMergingStrategyTest.
 *
 */
@UnitTest
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(
{ LogRunListener.class })
public class BundleCommerceCartMergingStrategyTest
{
    private static final Logger LOG = Logger.getLogger(BundleCommerceCartMergingStrategyTest.class);

    @InjectMocks
    private final BundleCommerceCartMergingStrategy mergingStrategy = new BundleCommerceCartMergingStrategy();

    @Mock
    private ProductModel commonProductMock, commonBundleProductMock1, commonBundleProductMock2;
    @Mock
    private ProductModel productMock, bundleProductMock1, bundleProductMock2;
    @Mock
    private BundleTemplateModel bundleTemplateMock;
    @Mock
    private UserService userService;
    @Mock
    private BaseSiteService baseSiteService;
    @Mock
    private ModelService modelService;
    @Mock
    private DefaultCommerceAddToCartStrategy addToCartStrategy;
    @Autowired
    private DefaultCommerceAddToCartStrategy addToCartStrategy1;
    @Mock
    private CartModel toCart, toBundleCart1, toBundleCart2;

    private CartModel fromCart, fromBundleCart;
    private CommerceCartModification commonCartModificationSuccess, commonBundleCartModificationSuccess1,
            commonBundleCartModificationSuccess2;
    private CommerceCartModification commonCartModificationPartialSuccess;
    private CommerceCartModification commonCartModificationNoSuccess;
    private CommerceCartModification notCommonCartModification, notCommonCartModification1;
    private final PK pk = PK.BIG_PK;

    private CartEntryModel cartEntryModel1;
    private CartEntryModel cartEntryModel3;
    private CartEntryModel bundleCartEntryModel1, bundleCartEntryModel2, bundleCartEntryModel3, bundleCartEntryModel4,
    bundleCartEntryModel5, bundleCartEntryModel6;
    private final PK commonBundleProductMock1PK = PK.createFixedUUIDPK(0, 1072224000001L), commonBundleProductMock2PK = PK
            .createFixedUUIDPK(0, 1072224000002L), bundleProductMock1PK = PK.createFixedUUIDPK(0, 1072224000003L),
            bundleProductMock2PK = PK.createFixedUUIDPK(0, 1072224000004L);

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        mergingStrategy.setBaseSiteService(baseSiteService);
        mergingStrategy.setCommerceAddToCartStrategy(addToCartStrategy);
        mergingStrategy.setModelService(modelService);
        mergingStrategy.setUserService(userService);

        final UserModel userModel = new UserModel();
        given(userService.getCurrentUser()).willReturn(userModel);
        given(Boolean.valueOf(userService.isAnonymousUser(userModel))).willReturn(Boolean.FALSE);

        fromCart = new CartModel();
        fromCart.setGuid("guidFromCart");

        final BaseSiteModel baseSiteModel = new BaseSiteModel();
        fromCart.setSite(baseSiteModel);

        cartEntryModel1 = spy(new CartEntryModel());
        cartEntryModel1.setEntryNumber(Integer.valueOf(0));
        cartEntryModel1.setQuantity(Long.valueOf(6));
        cartEntryModel1.setProduct(commonProductMock);

        final CartEntryModel cartEntryModel2 = new CartEntryModel();
        cartEntryModel2.setEntryNumber(Integer.valueOf(1));
        cartEntryModel2.setQuantity(Long.valueOf(9));
        cartEntryModel2.setProduct(productMock);

        final List<AbstractOrderEntryModel> entryList = new ArrayList<>();
        entryList.add(cartEntryModel1);
        entryList.add(cartEntryModel2);
        fromCart.setEntries(entryList);

        when(toCart.getSite()).thenReturn(baseSiteModel);

        cartEntryModel3 = spy(new CartEntryModel());
        cartEntryModel3.setEntryNumber(Integer.valueOf(0));
        cartEntryModel3.setQuantity(Long.valueOf(6));
        cartEntryModel3.setProduct(commonProductMock);

        final List<AbstractOrderEntryModel> entryList2 = new ArrayList<>();
        entryList2.add(cartEntryModel3);

        when(toCart.getEntries()).thenReturn(entryList2);
        given(baseSiteService.getCurrentBaseSite()).willReturn(fromCart.getSite());

        commonCartModificationSuccess = new CommerceCartModification();
        commonCartModificationSuccess.setStatusCode(CommerceCartModificationStatus.SUCCESS);
        commonCartModificationSuccess.setQuantityAdded(2);
        commonCartModificationSuccess.setQuantity(2);
        commonCartModificationSuccess.setEntry(fromCart.getEntries().get(0));

        commonCartModificationPartialSuccess = new CommerceCartModification();
        commonCartModificationPartialSuccess.setStatusCode(CommerceCartModificationStatus.NO_STOCK);
        commonCartModificationPartialSuccess.setQuantityAdded(1);
        commonCartModificationPartialSuccess.setQuantity(2);
        commonCartModificationPartialSuccess.setEntry(fromCart.getEntries().get(0));

        commonCartModificationNoSuccess = new CommerceCartModification();
        commonCartModificationNoSuccess.setStatusCode(CommerceCartModificationStatus.NO_STOCK);
        commonCartModificationNoSuccess.setQuantityAdded(0);
        commonCartModificationNoSuccess.setQuantity(2);
        commonCartModificationNoSuccess.setEntry(fromCart.getEntries().get(0));

        notCommonCartModification = new CommerceCartModification();
        notCommonCartModification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
        notCommonCartModification.setQuantityAdded(3);
        notCommonCartModification.setQuantity(3);
        notCommonCartModification.setEntry(fromCart.getEntries().get(1));

        //Bundle Data
        fromBundleCart = new CartModel();
        fromBundleCart.setGuid("guidFromCart1");
        fromBundleCart.setSite(baseSiteModel);

        bundleCartEntryModel1 = spy(new CartEntryModel());
        bundleCartEntryModel1.setEntryNumber(Integer.valueOf(0));
        bundleCartEntryModel1.setQuantity(Long.valueOf(1));
        bundleCartEntryModel1.setProduct(commonBundleProductMock1);
        bundleCartEntryModel1.setBundleNo(1);
        bundleCartEntryModel1.setBundleTemplate(bundleTemplateMock);
        when(bundleCartEntryModel1.getPk()).thenReturn(commonBundleProductMock1PK);

        bundleCartEntryModel2 = spy(new CartEntryModel());
        bundleCartEntryModel2.setEntryNumber(Integer.valueOf(1));
        bundleCartEntryModel2.setQuantity(Long.valueOf(1));
        bundleCartEntryModel2.setProduct(commonBundleProductMock2);
        bundleCartEntryModel2.setBundleNo(1);
        bundleCartEntryModel2.setBundleTemplate(bundleTemplateMock);
        when(bundleCartEntryModel2.getPk()).thenReturn(commonBundleProductMock2PK);

        final CartEntryModel cartEntryModel4 = spy(new CartEntryModel());
        cartEntryModel4.setEntryNumber(Integer.valueOf(2));
        cartEntryModel4.setQuantity(Long.valueOf(9));
        cartEntryModel4.setProduct(productMock);
        cartEntryModel4.setBundleNo(0);
        when(cartEntryModel4.getPk()).thenReturn(pk);

        final List<AbstractOrderEntryModel> bundleCartEntryList1 = new ArrayList<>();
        bundleCartEntryList1.add(bundleCartEntryModel1);
        bundleCartEntryList1.add(bundleCartEntryModel2);
        bundleCartEntryList1.add(cartEntryModel4);
        fromBundleCart.setEntries(bundleCartEntryList1);

        when(toBundleCart1.getSite()).thenReturn(baseSiteModel);
        when(toBundleCart2.getSite()).thenReturn(baseSiteModel);

        bundleCartEntryModel3 = spy(new CartEntryModel());
        bundleCartEntryModel3.setEntryNumber(Integer.valueOf(0));
        bundleCartEntryModel3.setQuantity(Long.valueOf(1));
        bundleCartEntryModel3.setProduct(commonBundleProductMock1);
        bundleCartEntryModel3.setBundleNo(1);
        bundleCartEntryModel3.setBundleTemplate(bundleTemplateMock);
        when(bundleCartEntryModel3.getPk()).thenReturn(commonBundleProductMock1PK);

        bundleCartEntryModel4 = spy(new CartEntryModel());
        bundleCartEntryModel4.setEntryNumber(Integer.valueOf(1));
        bundleCartEntryModel4.setQuantity(Long.valueOf(1));
        bundleCartEntryModel4.setProduct(commonBundleProductMock2);
        bundleCartEntryModel4.setBundleNo(1);
        bundleCartEntryModel4.setBundleTemplate(bundleTemplateMock);
        when(bundleCartEntryModel4.getPk()).thenReturn(commonBundleProductMock2PK);

        final List<AbstractOrderEntryModel> toBundleCartEntryList1 = new ArrayList<>();
        toBundleCartEntryList1.add(bundleCartEntryModel3);
        toBundleCartEntryList1.add(bundleCartEntryModel4);

        when(toBundleCart1.getEntries()).thenReturn(toBundleCartEntryList1);
        given(baseSiteService.getCurrentBaseSite()).willReturn(fromBundleCart.getSite());

        commonBundleCartModificationSuccess1 = new CommerceCartModification();
        commonBundleCartModificationSuccess1.setStatusCode(CommerceCartModificationStatus.SUCCESS);
        commonBundleCartModificationSuccess1.setQuantityAdded(1);
        commonBundleCartModificationSuccess1.setQuantity(1);
        commonBundleCartModificationSuccess1.setEntry(fromBundleCart.getEntries().get(0));

        commonBundleCartModificationSuccess2 = new CommerceCartModification();
        commonBundleCartModificationSuccess2.setStatusCode(CommerceCartModificationStatus.SUCCESS);
        commonBundleCartModificationSuccess2.setQuantityAdded(1);
        commonBundleCartModificationSuccess2.setQuantity(1);
        commonBundleCartModificationSuccess2.setEntry(fromBundleCart.getEntries().get(1));

        notCommonCartModification1 = new CommerceCartModification();
        notCommonCartModification1.setStatusCode(CommerceCartModificationStatus.SUCCESS);
        notCommonCartModification1.setQuantityAdded(9);
        notCommonCartModification1.setQuantity(9);
        notCommonCartModification1.setEntry(fromBundleCart.getEntries().get(2));

        bundleCartEntryModel5 = spy(new CartEntryModel());
        bundleCartEntryModel5.setEntryNumber(Integer.valueOf(0));
        bundleCartEntryModel5.setQuantity(Long.valueOf(1));
        bundleCartEntryModel5.setProduct(bundleProductMock1);
        bundleCartEntryModel5.setBundleNo(1);
        bundleCartEntryModel5.setBundleTemplate(bundleTemplateMock);
        when(bundleCartEntryModel5.getPk()).thenReturn(bundleProductMock1PK);

        bundleCartEntryModel6 = spy(new CartEntryModel());
        bundleCartEntryModel6.setEntryNumber(Integer.valueOf(1));
        bundleCartEntryModel6.setQuantity(Long.valueOf(1));
        bundleCartEntryModel6.setProduct(bundleProductMock2);
        bundleCartEntryModel6.setBundleNo(1);
        bundleCartEntryModel6.setBundleTemplate(bundleTemplateMock);
        when(bundleCartEntryModel6.getPk()).thenReturn(bundleProductMock2PK);

        //To List with a bundle packages that is not there in the from list.
        final List<AbstractOrderEntryModel> toBundleCartEntryList2 = new ArrayList<>();
        toBundleCartEntryList2.add(bundleCartEntryModel5);
        toBundleCartEntryList2.add(bundleCartEntryModel6);

        when(toBundleCart2.getEntries()).thenReturn(toBundleCartEntryList2);

    }

    @Test
    public void testMergeInStockModifications() throws CommerceCartMergingException
    {
        when(cartEntryModel1.getPk()).thenReturn(pk);
        when(cartEntryModel3.getPk()).thenReturn(pk);
        final CommerceCartModification cartModification = new CommerceCartModification();
        cartModification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
        cartModification.setQuantityAdded(6);
        cartModification.setQuantity(6);
        cartModification.setEntry(toCart.getEntries().get(0));

        final List<CommerceCartModification> list = new ArrayList<>();
        list.add(cartModification);

        final List<CommerceCartParameter> parameterList = new ArrayList<>();
        parameterList.add(any(CommerceCartParameter.class));

        final List<CommerceCartModification> cartModificationList = new ArrayList<>();
        cartModificationList.add(commonCartModificationSuccess);
        cartModificationList.add(notCommonCartModification);

        when(addToCartStrategy.addToCart(parameterList)).thenReturn(cartModificationList);

        mergingStrategy.mergeCarts(fromCart, toCart, list);
        Assert.assertEquals(list.get(0).getQuantity(), 8);
        Assert.assertEquals(list.get(0).getQuantityAdded(), 8);
        Assert.assertEquals(list.get(0).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
        Assert.assertEquals(list.get(1).getQuantity(), 3);
        Assert.assertEquals(list.get(1).getQuantityAdded(), 3);
        Assert.assertEquals(list.get(1).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
    }

    /**
     * To test the mergeCart method with a common bundle package present in the Anonymous From Cart and in the Saved
     * Cart.
     *
     * @throws CommerceCartMergingException
     */
    @Test
    public void testMergeInStockModificationsWithBundle() throws CommerceCartMergingException
    {
        LOG.info("testMergeInStockModificationsWithBundle method");
        final CommerceCartModification cartModification1 = new CommerceCartModification();
        cartModification1.setStatusCode(CommerceCartModificationStatus.SUCCESS);
        cartModification1.setQuantityAdded(1);
        cartModification1.setQuantity(1);
        cartModification1.setEntry(toBundleCart1.getEntries().get(0));

        final CommerceCartModification cartModification2 = new CommerceCartModification();
        cartModification2.setStatusCode(CommerceCartModificationStatus.SUCCESS);
        cartModification2.setQuantityAdded(1);
        cartModification2.setQuantity(1);
        cartModification2.setEntry(toBundleCart1.getEntries().get(1));

        final List<CommerceCartModification> toList = new ArrayList<>();
        toList.add(cartModification1);
        toList.add(cartModification2);

        final List<CommerceCartParameter> parameterList = new ArrayList<>();
        parameterList.add(any(CommerceCartParameter.class));

        final List<CommerceCartModification> cartModificationList = new ArrayList<>();
        cartModificationList.add(commonBundleCartModificationSuccess1);
        cartModificationList.add(commonBundleCartModificationSuccess2);
        cartModificationList.add(notCommonCartModification1);

        when(addToCartStrategy.addToCart(parameterList)).thenReturn(cartModificationList);

        mergingStrategy.mergeCarts(fromBundleCart, toBundleCart1, toList);
        LOG.info("testMergeInStockModificationsWithBundle method List Size" + toList.size());
        Assert.assertEquals(2, toList.get(0).getQuantity());
        Assert.assertEquals(2, toList.get(0).getQuantityAdded());
        Assert.assertEquals(CommerceCartModificationStatus.SUCCESS, toList.get(0).getStatusCode());
        Assert.assertEquals(1, toList.get(0).getEntry().getBundleNo().intValue());
        Assert.assertNotNull(toList.get(0).getEntry().getBundleTemplate());
        Assert.assertEquals(2, toList.get(1).getQuantity());
        Assert.assertEquals(2, toList.get(1).getQuantityAdded());
        Assert.assertEquals(CommerceCartModificationStatus.SUCCESS, toList.get(1).getStatusCode());
        Assert.assertEquals(1, toList.get(1).getEntry().getBundleNo().intValue());
        Assert.assertNotNull(toList.get(1).getEntry().getBundleTemplate());
        Assert.assertEquals(9, toList.get(2).getQuantity());
        Assert.assertEquals(9, toList.get(2).getQuantityAdded());
        Assert.assertEquals(CommerceCartModificationStatus.SUCCESS, toList.get(2).getStatusCode());
        Assert.assertEquals(0, toList.get(2).getEntry().getBundleNo().intValue());
        Assert.assertNull(toList.get(2).getEntry().getBundleTemplate());
    }

    /**
     * To test the mergeCart method with no common bundle package present in the Anonymous From Cart and in the Saved
     * Cart.
     *
     * @throws CommerceCartMergingException
     */
    @Test
    public void testMergeInStockModificationsWithNoCommonBundle() throws CommerceCartMergingException
    {
        LOG.info("testMergeInStockModificationsWithNoCommonBundle method");
        /* Calculate the Max bundle number present as of now in the ToCart - Anonymous Cart. */
        final int maxBundleNoInCart = mergingStrategy.calcMaxBundleNoInCart(toBundleCart2);
        Assert.assertEquals(1, maxBundleNoInCart);

        /* Create the Cart modification list for the To List with the Entries in the ToCart - Anonymous cart. */
        final CommerceCartModification cartModification1 = new CommerceCartModification();
        cartModification1.setStatusCode(CommerceCartModificationStatus.SUCCESS);
        cartModification1.setQuantityAdded(1);
        cartModification1.setQuantity(1);
        cartModification1.setEntry(toBundleCart2.getEntries().get(0));

        final CommerceCartModification cartModification2 = new CommerceCartModification();
        cartModification2.setStatusCode(CommerceCartModificationStatus.SUCCESS);
        cartModification2.setQuantityAdded(1);
        cartModification2.setQuantity(1);
        cartModification2.setEntry(toBundleCart2.getEntries().get(1));

        final List<CommerceCartModification> toList = new ArrayList<>();
        toList.add(cartModification1);
        toList.add(cartModification2);

        /*
         * Increment the Bundle No. of the bundle entries in the From Cart(Saved Card) by the ToCart's max bundle
         * number.
         */
        bundleCartEntryModel1.setBundleNo(1 + maxBundleNoInCart);
        bundleCartEntryModel2.setBundleNo(1 + maxBundleNoInCart);

        final CartEntryModel cartEntryModel4 = spy(new CartEntryModel());
        cartEntryModel4.setEntryNumber(Integer.valueOf(2));
        cartEntryModel4.setQuantity(Long.valueOf(9));
        cartEntryModel4.setProduct(productMock);
        cartEntryModel4.setBundleNo(0);
        when(cartEntryModel4.getPk()).thenReturn(pk);

        final List<AbstractOrderEntryModel> fromBundleCartEntryList2 = new ArrayList<>();
        fromBundleCartEntryList2.add(bundleCartEntryModel1);
        fromBundleCartEntryList2.add(bundleCartEntryModel2);
        fromBundleCartEntryList2.add(cartEntryModel4);
        fromBundleCart.setEntries(fromBundleCartEntryList2);

        /* Update the Cart modification objects with the updated Entries with the bundle number increased. */
        commonBundleCartModificationSuccess1.setEntry(fromBundleCart.getEntries().get(0));
        commonBundleCartModificationSuccess2.setEntry(fromBundleCart.getEntries().get(1));
        notCommonCartModification1.setEntry(fromBundleCart.getEntries().get(2));

        final List<CommerceCartModification> cartModificationList = new ArrayList<>();
        cartModificationList.add(commonBundleCartModificationSuccess1);
        cartModificationList.add(commonBundleCartModificationSuccess2);
        cartModificationList.add(notCommonCartModification1);

        final List<CommerceCartParameter> parameterList = new ArrayList<>();
        parameterList.add(any(CommerceCartParameter.class));

        when(addToCartStrategy.addToCart(parameterList)).thenReturn(cartModificationList);

        LOG.info("From Bundle Cart Size : " + fromBundleCart.getEntries().size());
        LOG.info("To Bundle Cart Size : " + toBundleCart2.getEntries().size());
        LOG.info("To List Size : " + toList.size());

        mergingStrategy.mergeCarts(fromBundleCart, toBundleCart2, toList);
        LOG.info("testMergeInStockModificationsWithNotCommonBundle method List Size" + toList.size());

        Assert.assertEquals(1, toList.get(0).getQuantity());
        Assert.assertEquals(1, toList.get(0).getQuantityAdded());
        Assert.assertEquals(CommerceCartModificationStatus.SUCCESS, toList.get(0).getStatusCode());
        Assert.assertEquals(1, toList.get(0).getEntry().getBundleNo().intValue());
        Assert.assertNotNull(toList.get(0).getEntry().getBundleTemplate());
        Assert.assertEquals(1, toList.get(1).getQuantity());
        Assert.assertEquals(1, toList.get(1).getQuantityAdded());
        Assert.assertEquals(CommerceCartModificationStatus.SUCCESS, toList.get(1).getStatusCode());
        Assert.assertEquals(1, toList.get(1).getEntry().getBundleNo().intValue());
        Assert.assertNotNull(toList.get(1).getEntry().getBundleTemplate());
        //Verify that the bundle number is incremented for the From Cart bundle entries after the mergeCarts() method call.
        Assert.assertEquals(1, toList.get(2).getQuantity());
        Assert.assertEquals(1, toList.get(2).getQuantityAdded());
        Assert.assertEquals(CommerceCartModificationStatus.SUCCESS, toList.get(2).getStatusCode());
        Assert.assertEquals(2, toList.get(2).getEntry().getBundleNo().intValue());
        Assert.assertNotNull(toList.get(2).getEntry().getBundleTemplate());
        Assert.assertEquals(1, toList.get(3).getQuantity());
        Assert.assertEquals(1, toList.get(3).getQuantityAdded());
        Assert.assertEquals(CommerceCartModificationStatus.SUCCESS, toList.get(3).getStatusCode());
        Assert.assertEquals(2, toList.get(3).getEntry().getBundleNo().intValue());
        Assert.assertNotNull(toList.get(3).getEntry().getBundleTemplate());
        Assert.assertEquals(9, toList.get(4).getQuantity());
        Assert.assertEquals(9, toList.get(4).getQuantityAdded());
        Assert.assertEquals(CommerceCartModificationStatus.SUCCESS, toList.get(4).getStatusCode());
        Assert.assertEquals(0, toList.get(4).getEntry().getBundleNo().intValue());
        Assert.assertNull(toList.get(4).getEntry().getBundleTemplate());

    }

    @Test
    public void testMergePartiallyOutOfStockModifications() throws CommerceCartMergingException
    {
        when(cartEntryModel1.getPk()).thenReturn(pk);
        when(cartEntryModel3.getPk()).thenReturn(pk);
        final CommerceCartModification cartModification = new CommerceCartModification();
        cartModification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
        cartModification.setQuantityAdded(6);
        cartModification.setQuantity(6);
        cartModification.setEntry(toCart.getEntries().get(0));

        final List<CommerceCartModification> list = new ArrayList<>();
        list.add(cartModification);

        final List<CommerceCartParameter> parameterList = new ArrayList<>();
        parameterList.add(any(CommerceCartParameter.class));

        final List<CommerceCartModification> cartModificationList = new ArrayList<>();
        cartModificationList.add(commonCartModificationPartialSuccess);
        cartModificationList.add(notCommonCartModification);

        when(addToCartStrategy.addToCart(parameterList)).thenReturn(cartModificationList);

        mergingStrategy.mergeCarts(fromCart, toCart, list);
        Assert.assertEquals(list.get(0).getQuantity(), 8);
        Assert.assertEquals(list.get(0).getQuantityAdded(), 7);
        Assert.assertEquals(list.get(0).getStatusCode(), CommerceCartModificationStatus.NO_STOCK);
        Assert.assertEquals(list.get(1).getQuantity(), 3);
        Assert.assertEquals(list.get(1).getQuantityAdded(), 3);
        Assert.assertEquals(list.get(1).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
    }

    @Test
    public void testMergeOutOfStockModifications() throws CommerceCartMergingException
    {
        when(cartEntryModel1.getPk()).thenReturn(pk);
        when(cartEntryModel3.getPk()).thenReturn(pk);
        final CommerceCartModification cartModification = new CommerceCartModification();
        cartModification.setStatusCode(CommerceCartModificationStatus.NO_STOCK);
        cartModification.setQuantityAdded(0);
        cartModification.setQuantity(6);
        cartModification.setEntry(toCart.getEntries().get(0));

        final List<CommerceCartModification> list = new ArrayList<>();
        list.add(cartModification);

        final List<CommerceCartParameter> parameterList = new ArrayList<>();
        parameterList.add(any(CommerceCartParameter.class));

        final List<CommerceCartModification> cartModificationList = new ArrayList<>();
        cartModificationList.add(commonCartModificationNoSuccess);
        cartModificationList.add(notCommonCartModification);

        when(addToCartStrategy.addToCart(parameterList)).thenReturn(cartModificationList);

        mergingStrategy.mergeCarts(fromCart, toCart, list);
        Assert.assertEquals(list.get(0).getQuantity(), 8);
        Assert.assertEquals(list.get(0).getQuantityAdded(), 0);
        Assert.assertEquals(list.get(0).getStatusCode(), CommerceCartModificationStatus.NO_STOCK);
        Assert.assertEquals(list.get(1).getQuantity(), 3);
        Assert.assertEquals(list.get(1).getQuantityAdded(), 3);
        Assert.assertEquals(list.get(1).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
    }

    @Test
    public void testMergeInStockModificationsSeparateEntries() throws CommerceCartMergingException
    {
        final CommerceCartModification cartModification = new CommerceCartModification();
        cartModification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
        cartModification.setQuantityAdded(6);
        cartModification.setQuantity(6);
        cartModification.setEntry(toCart.getEntries().get(0));

        final List<CommerceCartModification> list = new ArrayList<>();
        list.add(cartModification);

        final List<CommerceCartParameter> parameterList = new ArrayList<>();
        parameterList.add(any(CommerceCartParameter.class));

        final List<CommerceCartModification> cartModificationList = new ArrayList<>();
        cartModificationList.add(commonCartModificationSuccess);
        cartModificationList.add(notCommonCartModification);

        when(addToCartStrategy.addToCart(parameterList)).thenReturn(cartModificationList);

        mergingStrategy.mergeCarts(fromCart, toCart, list);
        Assert.assertEquals(list.get(0).getQuantity(), 6);
        Assert.assertEquals(list.get(0).getQuantityAdded(), 6);
        Assert.assertEquals(list.get(0).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
        Assert.assertEquals(list.get(1).getQuantity(), 2);
        Assert.assertEquals(list.get(1).getQuantityAdded(), 2);
        Assert.assertEquals(list.get(1).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
        Assert.assertEquals(list.get(2).getQuantity(), 3);
        Assert.assertEquals(list.get(2).getQuantityAdded(), 3);
        Assert.assertEquals(list.get(2).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
    }

    @Test
    public void testMergePartiallyOutOfStockModificationsSeparateEntries() throws CommerceCartMergingException
    {
        final CommerceCartModification cartModification = new CommerceCartModification();
        cartModification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
        cartModification.setQuantityAdded(6);
        cartModification.setQuantity(6);
        cartModification.setEntry(toCart.getEntries().get(0));

        final List<CommerceCartModification> list = new ArrayList<>();
        list.add(cartModification);

        final List<CommerceCartParameter> parameterList = new ArrayList<>();
        parameterList.add(any(CommerceCartParameter.class));

        final List<CommerceCartModification> cartModificationList = new ArrayList<>();
        cartModificationList.add(commonCartModificationPartialSuccess);
        cartModificationList.add(notCommonCartModification);

        when(addToCartStrategy.addToCart(parameterList)).thenReturn(cartModificationList);

        mergingStrategy.mergeCarts(fromCart, toCart, list);
        Assert.assertEquals(list.get(0).getQuantity(), 6);
        Assert.assertEquals(list.get(0).getQuantityAdded(), 6);
        Assert.assertEquals(list.get(0).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
        Assert.assertEquals(list.get(1).getQuantity(), 2);
        Assert.assertEquals(list.get(1).getQuantityAdded(), 1);
        Assert.assertEquals(list.get(1).getStatusCode(), CommerceCartModificationStatus.NO_STOCK);
        Assert.assertEquals(list.get(2).getQuantity(), 3);
        Assert.assertEquals(list.get(2).getQuantityAdded(), 3);
        Assert.assertEquals(list.get(2).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
    }

    @Test
    public void testMergeOutOfStockModificationsSeparateEntries() throws CommerceCartMergingException
    {
        final CommerceCartModification cartModification = new CommerceCartModification();
        cartModification.setStatusCode(CommerceCartModificationStatus.NO_STOCK);
        cartModification.setQuantityAdded(0);
        cartModification.setQuantity(6);
        cartModification.setEntry(toCart.getEntries().get(0));

        final List<CommerceCartModification> list = new ArrayList<>();
        list.add(cartModification);

        final List<CommerceCartParameter> parameterList = new ArrayList<>();
        parameterList.add(any(CommerceCartParameter.class));

        final List<CommerceCartModification> cartModificationList = new ArrayList<>();
        cartModificationList.add(commonCartModificationNoSuccess);
        cartModificationList.add(notCommonCartModification);

        when(addToCartStrategy.addToCart(parameterList)).thenReturn(cartModificationList);

        mergingStrategy.mergeCarts(fromCart, toCart, list);
        Assert.assertEquals(list.get(0).getQuantity(), 6);
        Assert.assertEquals(list.get(0).getQuantityAdded(), 0);
        Assert.assertEquals(list.get(0).getStatusCode(), CommerceCartModificationStatus.NO_STOCK);
        Assert.assertEquals(list.get(1).getQuantity(), 2);
        Assert.assertEquals(list.get(1).getQuantityAdded(), 0);
        Assert.assertEquals(list.get(1).getStatusCode(), CommerceCartModificationStatus.NO_STOCK);
        Assert.assertEquals(list.get(2).getQuantity(), 3);
        Assert.assertEquals(list.get(2).getQuantityAdded(), 3);
        Assert.assertEquals(list.get(2).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
    }
}
