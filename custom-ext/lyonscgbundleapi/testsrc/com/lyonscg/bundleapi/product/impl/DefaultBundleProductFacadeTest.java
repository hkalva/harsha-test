package com.lyonscg.bundleapi.product.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.lyonscg.bundleapi.bundle.LeaBundleRuleService;
import com.lyonscg.bundleapi.data.BundleProductInfo;
import com.lyonscg.bundleapi.data.BundledProductPrice;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ReviewData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.configurablebundleservices.model.ChangeProductPriceBundleRuleModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.PriceValue;


/**
 * Unit Tests for DefaultBundleProductFacade.
 */
@UnitTest
public class DefaultBundleProductFacadeTest
{
    @InjectMocks
    private DefaultBundleProductFacade defaultBundleProductFacade;
    @Mock
    private ProductFacade productFacade;
    @Mock
    private BundleTemplateService bundleTemplateService;
    @Mock
    private Converter<BundleTemplateModel, BundleTemplateData> bundleTemplateConverter;
    @Mock
    private BundleTemplateModel parentBundleTemplateModel;
    @Mock
    private BundleTemplateModel childBundleTemplateModel;
    @Mock
    private BundleTemplateModel childBundleTemplateModel2;
    @Mock
    private BundleTemplateModel gParentBundleTemplateModel;

    @Mock
    private CommerceCommonI18NService commerceCommonI18NService;
    @Mock
    private ProductService productService;
    @Mock
    private CommercePriceService commercePriceService;
    @Mock
    private LeaBundleRuleService leaBundleRuleService;
    @Mock
    private PriceDataFactory priceDataFactory;
    @Mock
    private ProductModel productModel1, productModel2;
    @Mock
    private BundleTemplateModel bundleModel1, bundleModel2;
    @Mock
    private PriceInformation priceInformation1, priceInformation2;
    @Mock
    private PriceValue priceValue1, priceValue2;
    @Mock
    private CurrencyModel currency;
    @Mock
    private ChangeProductPriceBundleRuleModel changeProductPriceBundleRuleModel1, changeProductPriceBundleRuleModel2;

    private BundleTemplateData parentBundleData;
    private BundleTemplateData childBundleData;
    private BundleTemplateData gParentBundleData;
    private BundleTemplateData childBundleData2;
    private final String parentBundleCode = "parentBundleCode";
    private final String childBundleCode = "childBundleCode";
    private final String gParentBundleCode = "gParentBundleCode";

    private List<BundleProductInfo> bundleProductsInfos;
    private BundleProductInfo bundleProductInfo1, bundleProductInfo2;
    private String productCode1 = "productCode1", productCode2 = "productCode2", bundleId1 = "bundleId1", bundleId2 = "bundleId2";
    private double price1, price2;
    private BigDecimal bundlePPrice1, bundlePPrice2;
    private PriceData priceData1, priceData2;
    private static final double FIFTY = 50, HUNDRED = 100, TWENTYFIVE = 25;

    /**
     * Setting mock initialization. declaring parent child gparent bundle data.
     */
    @Before
    public void setup()
    {
        defaultBundleProductFacade = new DefaultBundleProductFacade();
        MockitoAnnotations.initMocks(this);
        parentBundleData = new BundleTemplateData();
        childBundleData = new BundleTemplateData();
        gParentBundleData = new BundleTemplateData();
        childBundleData2 = new BundleTemplateData();

        bundleProductsInfos = new ArrayList<>();
        bundleProductInfo1 = new BundleProductInfo();
        bundleProductInfo2 = new BundleProductInfo();
        bundleProductInfo1.setProductCode(productCode1);
        bundleProductInfo1.setBundleId(bundleId1);
        bundleProductInfo2.setProductCode(productCode2);
        bundleProductInfo2.setBundleId(bundleId2);
        bundleProductsInfos.add(bundleProductInfo1);
        bundleProductsInfos.add(bundleProductInfo2);
        price1 = FIFTY;
        price2 = HUNDRED;
        bundlePPrice1 = BigDecimal.valueOf(TWENTYFIVE);
        bundlePPrice2 = BigDecimal.valueOf(FIFTY);
        priceData1 = new PriceData();
        priceData2 = new PriceData();

        priceData1.setValue(bundlePPrice1.add(bundlePPrice2));
        priceData2.setValue(BigDecimal.valueOf(price1).add(BigDecimal.valueOf(price2)));

        Mockito.doReturn(parentBundleTemplateModel).when(bundleTemplateService).getBundleTemplateForCode(parentBundleCode);
        Mockito.doReturn(parentBundleData).when(bundleTemplateConverter).convert(parentBundleTemplateModel);
        Mockito.doReturn(Collections.singletonList(childBundleTemplateModel)).when(parentBundleTemplateModel).getChildTemplates();

        Mockito.doReturn(childBundleTemplateModel).when(bundleTemplateService).getBundleTemplateForCode(childBundleCode);
        Mockito.doReturn(childBundleData).when(bundleTemplateConverter).convert(childBundleTemplateModel);

        Mockito.doReturn(gParentBundleTemplateModel).when(bundleTemplateService).getBundleTemplateForCode(gParentBundleCode);
        Mockito.doReturn(gParentBundleData).when(bundleTemplateConverter).convert(gParentBundleTemplateModel);
        Mockito.doReturn(Collections.singletonList(parentBundleTemplateModel)).when(gParentBundleTemplateModel)
                .getChildTemplates();

        Mockito.doReturn(productModel1).when(productService).getProductForCode(productCode1);
        Mockito.doReturn(productModel2).when(productService).getProductForCode(productCode2);

        Mockito.doReturn(bundleModel1).when(bundleTemplateService).getBundleTemplateForCode(bundleId1);
        Mockito.doReturn(bundleModel2).when(bundleTemplateService).getBundleTemplateForCode(bundleId2);

        Mockito.doReturn(priceInformation1).when(commercePriceService).getWebPriceForProduct(productModel1);
        Mockito.doReturn(priceInformation2).when(commercePriceService).getWebPriceForProduct(productModel2);

        Mockito.doReturn(priceValue1).when(priceInformation1).getPriceValue();
        Mockito.doReturn(priceValue2).when(priceInformation2).getPriceValue();

        Mockito.doReturn(price1).when(priceValue1).getValue();
        Mockito.doReturn(price2).when(priceValue2).getValue();

        Mockito.doReturn(currency).when(commerceCommonI18NService).getCurrentCurrency();

        Mockito.doReturn(changeProductPriceBundleRuleModel1).when(leaBundleRuleService).getLowestPriceForTargetProductAndTemplate(
                bundleModel1, productModel1, currency, Collections.singleton(productModel2));
        Mockito.doReturn(changeProductPriceBundleRuleModel2).when(leaBundleRuleService).getLowestPriceForTargetProductAndTemplate(
                bundleModel2, productModel2, currency, Collections.singleton(productModel1));

        Mockito.doReturn(bundlePPrice1).when(changeProductPriceBundleRuleModel1).getPrice();
        Mockito.doReturn(bundlePPrice2).when(changeProductPriceBundleRuleModel2).getPrice();

        Mockito.doReturn(priceData1).when(priceDataFactory).create(PriceDataType.BUY, bundlePPrice1.add(bundlePPrice2), currency);
        Mockito.doReturn(priceData2).when(priceDataFactory).create(PriceDataType.BUY,
                BigDecimal.valueOf(price1).add(BigDecimal.valueOf(price2)), currency);
    }

    /**
     * Getting complete detail of bundle with child.
     */
    @Test
    public void getCompleteBundleDetailsWithChildTest()
    {
        final BundleTemplateData actualBundleData = defaultBundleProductFacade.getCompleteBundleDetails(parentBundleCode);
        Assert.assertEquals(parentBundleData, actualBundleData);
        Assert.assertEquals(childBundleData, actualBundleData.getBundleTemplates().get(0));
    }

    /**
     * Getting complete detail of bundle with parent.
     */
    @Test
    public void getCompleteBundleDetailsWithParentTest()
    {
        Mockito.doReturn(gParentBundleTemplateModel).when(parentBundleTemplateModel).getParentTemplate();
        final BundleTemplateData actualBundleData = defaultBundleProductFacade.getCompleteBundleDetails(parentBundleCode);
        Assert.assertEquals(gParentBundleData, actualBundleData);
        Assert.assertEquals(parentBundleData, actualBundleData.getBundleTemplates().get(0));
        Assert.assertEquals(childBundleData, actualBundleData.getBundleTemplates().get(0).getBundleTemplates().get(0));
    }

    /**
     * Getting complete detail of bundle from all chindren.
     */
    @Test
    public void getCompleteBundleDetailsWithChildrenTest()
    {
        BundleTemplateModel[] bundleTemplateModels =
        { childBundleTemplateModel, childBundleTemplateModel2 };
        Mockito.doReturn(Arrays.asList(bundleTemplateModels)).when(parentBundleTemplateModel).getChildTemplates();
        Mockito.doReturn(childBundleData2).when(bundleTemplateConverter).convert(childBundleTemplateModel2);
        childBundleData.setNoOfRequiredBundles(1);
        final BundleTemplateData actualBundleData = defaultBundleProductFacade.getCompleteBundleDetails(parentBundleCode);
        Assert.assertEquals(parentBundleData, actualBundleData);
        Assert.assertEquals(childBundleData2, actualBundleData.getBundleTemplates().get(0));
        Assert.assertEquals(childBundleData, actualBundleData.getBundleTemplates().get(1));
    }

    //Just to verify delegation.
    @Test
    public void getProductForOptionsTest()
    {
        final ProductModel productModel = null;
        final Collection<ProductOption> options = null;
        defaultBundleProductFacade.getProductForOptions(productModel, options);
        Mockito.verify(productFacade).getProductForOptions(productModel, options);
    }

    @Test
    //Just to verify delegation
    public void getProductForCodeAndOptionsTest()
    {
        final String code = null;
        final Collection<ProductOption> options = null;
        defaultBundleProductFacade.getProductForCodeAndOptions(code, options);
        Mockito.verify(productFacade).getProductForCodeAndOptions(code, options);
    }

    @Test
    //Just to verify delegation
    public void postReviewTest()
    {
        final String productCode = null;
        final ReviewData reviewData = null;
        defaultBundleProductFacade.postReview(productCode, reviewData);
        Mockito.verify(productFacade).postReview(productCode, reviewData);
    }

    @Test
    //Just to verify delegation
    public void getReviewsTest()
    {
        final String productCode = null;
        defaultBundleProductFacade.getReviews(productCode);
        Mockito.verify(productFacade).getReviews(productCode);
    }

    @Test
    //Just to verify delegation
    public void getReviewsByNumberTest()
    {
        final String productCode = null;
        final Integer numberOfReviews = null;
        defaultBundleProductFacade.getReviews(productCode, numberOfReviews);
        Mockito.verify(productFacade).getReviews(productCode, numberOfReviews);
    }

    @Test
    //Just to verify delegation
    public void getProductReferencesForCodeTest()
    {
        final String code = null;
        final ProductReferenceTypeEnum referenceType = null;
        final List<ProductOption> options = null;
        final Integer limit = null;
        defaultBundleProductFacade.getProductReferencesForCode(code, referenceType, options, limit);
        Mockito.verify(productFacade).getProductReferencesForCode(code, referenceType, options, limit);
    }

    @Test
    //Just to verify delegation
    public void getProductReferencesForCodeEnumListTest()
    {
        final String code = null;
        final List<ProductReferenceTypeEnum> referenceTypes = null;
        final List<ProductOption> options = null;
        final Integer limit = null;
        defaultBundleProductFacade.getProductReferencesForCode(code, referenceTypes, options, limit);
        Mockito.verify(productFacade).getProductReferencesForCode(code, referenceTypes, options, limit);
    }

    /**
     * Unit test for {@link DefaultBundleProductFacade#getBundlePriceForProductsAndTemplates(List)}.
     */
    @Test
    public void getBundlePriceForProductsAndTemplatesTest()
    {
        BundledProductPrice bundledProductPrice = defaultBundleProductFacade
                .getBundlePriceForProductsAndTemplates(bundleProductsInfos);
        Assert.assertEquals(priceData1.getValue(), bundledProductPrice.getPriceWithBundle().getValue());
        Assert.assertEquals(priceData2.getValue(), bundledProductPrice.getPriceWithoutBundle().getValue());
    }

    /**
     * Unit test for {@link DefaultBundleProductFacade#getBundlePriceForProductsAndTemplates(List)}. When the input is
     * null.
     */
    @Test
    public void getBundlePriceForProductsAndTemplatesNullTest()
    {
        BundledProductPrice bundledProductPrice = defaultBundleProductFacade.getBundlePriceForProductsAndTemplates(null);
        Assert.assertNull(bundledProductPrice);
    }

    /**
     * Unit test for {@link DefaultBundleProductFacade#getBundlePriceForProductsAndTemplates(List)}. When the input is
     * empty.
     */
    @Test
    public void getBundlePriceForProductsAndTemplatesEmptyTest()
    {
        BundledProductPrice bundledProductPrice = defaultBundleProductFacade
                .getBundlePriceForProductsAndTemplates(Collections.EMPTY_LIST);
        Assert.assertNull(bundledProductPrice);
    }
}
