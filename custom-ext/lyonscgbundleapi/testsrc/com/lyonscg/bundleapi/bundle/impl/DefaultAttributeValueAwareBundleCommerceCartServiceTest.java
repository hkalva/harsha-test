package com.lyonscg.bundleapi.bundle.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.product.ProductModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 *
 * Test class for DefaultAttributeValueAwareBundleCommerceCartService.
 */
@UnitTest
public class DefaultAttributeValueAwareBundleCommerceCartServiceTest
{
	private DefaultAttributeValueAwareBundleCommerceCartService defaultAttributeValueAwareBundleCommerceCartService;
	@Mock
	private ProductModel productModel;

	/**Setting up mock initialization.
	 * Declaring cart service.
	 */
	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		defaultAttributeValueAwareBundleCommerceCartService = new DefaultAttributeValueAwareBundleCommerceCartService();
	}

	/**Checking if it is sold individually.
	 */
	@Test
	public void checkIsSoldIndividuallyNullTest() throws CommerceCartModificationException
	{
		Mockito.doReturn(null).when(productModel).getSoldIndividually();
		defaultAttributeValueAwareBundleCommerceCartService.checkIsSoldIndividually(productModel, 0);
	}

	/**making sure is it possible to sold individually.
	 * @throws CommerceCartModificationException.
	 */
	@Test
	public void checkIsSoldIndividuallyTrueTest() throws CommerceCartModificationException
	{
		Mockito.doReturn(Boolean.TRUE).when(productModel).getSoldIndividually();
		defaultAttributeValueAwareBundleCommerceCartService.checkIsSoldIndividually(productModel, 0);
	}

	/**making sure it is not possible to be sold individually.
	 * @throws CommerceCartModificationException. 
	 */
	@Test(expected = CommerceCartModificationException.class)
	public void checkIsSoldIndividuallyFalseTest() throws CommerceCartModificationException
	{
		Mockito.doReturn(Boolean.FALSE).when(productModel).getSoldIndividually();
		defaultAttributeValueAwareBundleCommerceCartService.checkIsSoldIndividually(productModel, 0);
	}
}
