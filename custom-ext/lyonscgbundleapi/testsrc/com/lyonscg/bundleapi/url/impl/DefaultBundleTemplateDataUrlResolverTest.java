package com.lyonscg.bundleapi.url.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Test class for DefaultBundleTemplateDataUrlResolver.
 */
@UnitTest
public class DefaultBundleTemplateDataUrlResolverTest
{
	@InjectMocks
	private DefaultBundleTemplateDataUrlResolver defaultBundleTemplateDataUrlResolver;
	@Mock
	private BundleTemplateService bundleTemplateService;
	@Mock
	private UrlResolver<BundleTemplateModel> bundleTemplateModelUrlResolver;
	@Mock
	private BundleTemplateModel bundleTemplate;

	private BundleTemplateData source;
	private final String bundleId = "bundleId";
	private final String url = "/testUrl";

	/**Setting up mock initialization.
	 */
	@Before
	public void setup()
	{
		defaultBundleTemplateDataUrlResolver = new DefaultBundleTemplateDataUrlResolver();
		MockitoAnnotations.initMocks(this);
		source = new BundleTemplateData();
		source.setId(bundleId);
		Mockito.doReturn(bundleTemplate).when(bundleTemplateService).getBundleTemplateForCode(bundleId);
		Mockito.doReturn(url).when(bundleTemplateModelUrlResolver).resolve(bundleTemplate);
	}

	/** resolving URL internally.
	 */
	@Test
	public void resolveInternalTest()
	{
		final String actualUrl = defaultBundleTemplateDataUrlResolver.resolveInternal(source);
		Mockito.verify(bundleTemplateModelUrlResolver).resolve(bundleTemplate);
		Assert.assertEquals(url, actualUrl);
	}
}
