package com.lyonscg.bundleapi.url.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.site.BaseSiteService;

import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Unit Tests for DefaultBundleTemplateModelUrlResolver.
 */
@UnitTest
public class DefaultBundleTemplateModelUrlResolverTest
{
	@InjectMocks
	private DefaultBundleTemplateModelUrlResolver defaultBundleTemplateModelUrlResolver;
	@Mock
	private BaseSiteService baseSiteService;
	private final String bundleNamePattern = "{bundle-name}";
	private final String bundleIdPattern = "{bundle-code}";
	private final String defaultPattern = "/" + bundleNamePattern + "/b/" + bundleIdPattern;
	@Mock
	private BundleTemplateModel source;
	@Mock
	private BaseSiteModel currentBaseSite;
	private final String bundleId = "bundleId";
	private final String bundleName = "bundleName";
	private final String baseSiteUid = "baseSiteUid";
	private final long pkLong = 12345;
	private PK bundlePK;

	/**Setting up mock initialization.
	 */
	@Before
	public void setup()
	{
		defaultBundleTemplateModelUrlResolver = new DefaultBundleTemplateModelUrlResolver();
		MockitoAnnotations.initMocks(this);
		defaultBundleTemplateModelUrlResolver.setDefaultPattern(defaultPattern);
		bundlePK = PK.fromLong(pkLong);

		Mockito.doReturn(currentBaseSite).when(baseSiteService).getCurrentBaseSite();
		Mockito.doReturn(bundleId).when(source).getId();
		Mockito.doReturn(bundlePK).when(source).getPk();
		Mockito.doReturn(bundleName).when(source).getName(Locale.ENGLISH);
		Mockito.doReturn(baseSiteUid).when(currentBaseSite).getUid();
	}

	/**resolving given URL internally.
	 */
	@Test
	public void resolveInternalTest()
	{
		final String actualURL = defaultBundleTemplateModelUrlResolver.resolveInternal(source);
		Assert.assertEquals(defaultPattern.replace(bundleNamePattern, StringUtils.lowerCase(source.getName(Locale.ENGLISH)))
				.replace(bundleIdPattern, source.getId()), actualURL);
	}

	/**getting key of internally resolved url.
	 */
	@Test
	public void getKeyTest()
	{
		Assert.assertEquals(defaultBundleTemplateModelUrlResolver.getKey(source),
				DefaultBundleTemplateModelUrlResolver.class.getName() + "." + source.getPk().toString());
	}
}
