/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.lyonscg.bundleapi.converters.populator;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.configurablebundleservices.model.PickExactlyNBundleSelectionCriteriaModel;
import de.hybris.platform.configurablebundleservices.model.PickNToMBundleSelectionCriteriaModel;
import de.hybris.platform.converters.ConfigurablePopulator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 *
 * Test class for DetailedBundleTemplatePopulator.
 */
@UnitTest
public class DetailedBundleTemplatePopulatorTest
{
	@InjectMocks
	private DetailedBundleTemplatePopulator<BundleTemplateModel, BundleTemplateData> detailedBundleTemplatePopulatorTest;
	@Mock
	private Converter<ProductModel, ProductData> productConverter;
	@Mock
	private ConfigurablePopulator<ProductModel, ProductData, ProductOption> productConfiguredPopulator;
	@Mock
	private Collection<ProductOption> productOptions;
	@Mock
	private BundleTemplateModel bundleTemplateModel;
	@Mock
	private BundleTemplateModel requiredBundleTemplateModel;
	@Mock
	private PickNToMBundleSelectionCriteriaModel pickNToMBundleSelectionCriteriaModel;
	@Mock
	private PickExactlyNBundleSelectionCriteriaModel pickExactlyNBundleSelectionCriteriaModel;
	@Mock
	private ProductModel productModel;
	@Mock
	private ProductData productData;
	private List<ProductModel> products;

	private BundleTemplateData bundleTemplateData;

	private final Integer minNInteger = Integer.valueOf(2);
	private final Integer minExactInteger = Integer.valueOf(1);
	private final String description = "description";

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		bundleTemplateData = new BundleTemplateData();
		products = Collections.singletonList(productModel);
		Mockito.doReturn(minNInteger).when(pickNToMBundleSelectionCriteriaModel).getN();
		Mockito.doReturn(minExactInteger).when(pickExactlyNBundleSelectionCriteriaModel).getN();
		Mockito.doReturn(description).when(bundleTemplateModel).getDescription();
		Mockito.doReturn(products).when(bundleTemplateModel).getProducts();
		Mockito.doReturn(productData).when(productConverter).convert(productModel);
		Mockito.doReturn(Collections.singletonList(requiredBundleTemplateModel)).when(bundleTemplateModel).getRequiredBundleTemplates();
	}

	@Test
	public void populateNtoMTest()
	{
		Mockito.doReturn(pickNToMBundleSelectionCriteriaModel).when(bundleTemplateModel).getBundleSelectionCriteria();
		detailedBundleTemplatePopulatorTest.populate(bundleTemplateModel, bundleTemplateData);
		Assert.assertEquals(minNInteger.intValue(), bundleTemplateData.getMinItemsAllowed());
		Assert.assertEquals(description, bundleTemplateData.getDescription());
		Assert.assertEquals(products.size(), bundleTemplateData.getProducts().size());
		Assert.assertEquals(productData, bundleTemplateData.getProducts().get(0));
		Assert.assertEquals(bundleTemplateData.getNoOfRequiredBundles(), 1);
	}

	@Test
	public void populateExactlyTest()
	{
		Mockito.doReturn(pickExactlyNBundleSelectionCriteriaModel).when(bundleTemplateModel).getBundleSelectionCriteria();
		detailedBundleTemplatePopulatorTest.populate(bundleTemplateModel, bundleTemplateData);
		Assert.assertEquals(minExactInteger.intValue(), bundleTemplateData.getMinItemsAllowed());
		Assert.assertEquals(description, bundleTemplateData.getDescription());
		Assert.assertEquals(products.size(), bundleTemplateData.getProducts().size());
		Assert.assertEquals(productData, bundleTemplateData.getProducts().get(0));
		Assert.assertEquals(bundleTemplateData.getNoOfRequiredBundles(), 1);
	}

	@Test
	public void populateNoProductsTest()
	{
		Mockito.doReturn(pickNToMBundleSelectionCriteriaModel).when(bundleTemplateModel).getBundleSelectionCriteria();
		Mockito.doReturn(null).when(bundleTemplateModel).getProducts();
		detailedBundleTemplatePopulatorTest.populate(bundleTemplateModel, bundleTemplateData);
		Assert.assertEquals(minNInteger.intValue(), bundleTemplateData.getMinItemsAllowed());
		Assert.assertEquals(description, bundleTemplateData.getDescription());
		Assert.assertEquals(null, bundleTemplateData.getProducts());
		Assert.assertEquals(bundleTemplateData.getNoOfRequiredBundles(), 1);
	}

}
