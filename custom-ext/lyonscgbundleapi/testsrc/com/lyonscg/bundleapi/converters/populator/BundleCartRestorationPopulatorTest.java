package com.lyonscg.bundleapi.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Test class for {@link BundleCartRestorationPopulator}.
 */
@UnitTest
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(
{ LogRunListener.class })
public class BundleCartRestorationPopulatorTest
{
    private static final Logger LOG = Logger.getLogger(BundleCartRestorationPopulatorTest.class);

    private BundleCartRestorationPopulator testClass;
    @Mock
    private CommerceCartRestoration commerceCartRestoration;
    private CartRestorationData cartRestorationData;
    private CartModificationData normalModification, bundleModification1, bundleModification2;
    private Integer entryBundleNo, bundleEntryNo;
    private String entryBundleName, bundleEntryName;

    /**
     * Setup test data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        testClass = new BundleCartRestorationPopulator();
        cartRestorationData = new CartRestorationData();
        entryBundleNo = Integer.valueOf(0);
        bundleEntryNo = Integer.valueOf(1);
        entryBundleName = "Entry Bundle Name";
        bundleEntryName = "Bundle Entry Name";

        normalModification = createCartModificationData(entryBundleNo, entryBundleName);
        bundleModification1 = createCartModificationData(bundleEntryNo, bundleEntryName);
        bundleModification2 = createCartModificationData(bundleEntryNo, bundleEntryName);

        final List<CartModificationData> modifications = new ArrayList<>();
        modifications.add(bundleModification1);
        modifications.add(bundleModification2);
        modifications.add(normalModification);
        cartRestorationData.setModifications(modifications);

    }

    /**
     * Unit test for
     * {@link BundleCartRestorationPopulator#populate(de.hybris.platform.commerceservices.order.CommerceCartRestoration, de.hybris.platform.commercefacades.order.data.CartRestorationData)}
     * .
     */
    @Test
    public void populateTest()
    {
        testClass.populate(commerceCartRestoration, cartRestorationData);
        final Map<String, List<CartModificationData>> bundleMap = cartRestorationData.getBundleModificationsMap();
        final List<CartModificationData> bundleModifications = bundleMap.get(bundleEntryNo + ":" + bundleEntryName);
        Assert.assertTrue(bundleModifications.contains(bundleModification1));
        Assert.assertTrue(bundleModifications.contains(bundleModification2));
    }

    /**
     * Unit test to throw {@link IllegalArgumentException} when {@link CommerceCartRestoration} is null.
     */
    @Test(expected = IllegalArgumentException.class)
    public void populateCommerceCartRestorationNullTest()
    {
        testClass.populate(null, cartRestorationData);
    }

    /**
     * Unit test to throw {@link IllegalArgumentException} when {@link CartRestorationData} is null.
     */
    @Test(expected = IllegalArgumentException.class)
    public void populateCartRestorationDataNullTest()
    {
        testClass.populate(commerceCartRestoration, null);
    }

    /**
     * Create {@link CartModificationData} for bundle number.
     *
     * @param bundleNo
     *            - Bundle number to set.
     * @return - The newly created {@link CartModificationData}.
     */
    private CartModificationData createCartModificationData(final Integer bundleNo, final String bundleName)
    {
        final CartModificationData modification = new CartModificationData();

        final OrderEntryData entry = new OrderEntryData();
        final BundleTemplateData bundleTemplate = new BundleTemplateData();
        entry.setBundleNo(bundleNo);
        bundleTemplate.setName(bundleName);
        entry.setRootBundleTemplate(bundleTemplate);

        modification.setEntry(entry);
        return modification;
    }
}
