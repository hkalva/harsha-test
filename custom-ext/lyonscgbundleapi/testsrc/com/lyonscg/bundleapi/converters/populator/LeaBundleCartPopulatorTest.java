package com.lyonscg.bundleapi.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.core.model.order.CartModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Test class for {@link LeaBundleCartPopulator}.
 */
@UnitTest
public class LeaBundleCartPopulatorTest
{
    private LeaBundleCartPopulator<CartModel, CartData> testClass;
    @Mock
    private CartModel cartModel;
    private CartData cartData;
    private OrderEntryData normalEntry, bundleEntry1, bundleEntry2;
    private Integer entryBundleNo, bundleEntryNo;
    private String entryBundleName, bundleEntryName;

    /**
     * Setup test data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        testClass = new LeaBundleCartPopulator<>();
        cartData = new CartData();
        entryBundleNo = Integer.valueOf(0);
        bundleEntryNo = Integer.valueOf(1);
        entryBundleName = "Entry Bundle Name";
        bundleEntryName = "Bundle Entry Name";

        normalEntry = createOrderEntryData(entryBundleNo, entryBundleName);
        bundleEntry1 = createOrderEntryData(bundleEntryNo, bundleEntryName);
        bundleEntry2 = createOrderEntryData(bundleEntryNo, bundleEntryName);
        final List<OrderEntryData> entries = new ArrayList<>();
        entries.add(bundleEntry1);
        entries.add(bundleEntry2);
        entries.add(normalEntry);
        cartData.setEntries(entries);

    }

    /**
     * Unit test for {@link LeaBundleCartPopulator#populate(CartModel, CartData)}.
     */
    @Test
    public void populateTest()
    {
        testClass.populate(cartModel, cartData);
        final Map<String, List<OrderEntryData>> bundleMap = cartData.getBundleEntriesMap();
        final List<OrderEntryData> bundleEntries = bundleMap.get(bundleEntryNo + ":" + bundleEntryName);
        Assert.assertTrue(bundleEntries.contains(bundleEntry1));
        Assert.assertTrue(bundleEntries.contains(bundleEntry2));
    }

    /**
     * Unit test to throw {@link IllegalArgumentException} when {@link CartModel} is null.
     */
    @Test(expected = IllegalArgumentException.class)
    public void populateCartModelNullTest()
    {
        testClass.populate(null, cartData);
    }

    /**
     * Unit test to throw {@link IllegalArgumentException} when {@link CartData} is null.
     */
    @Test(expected = IllegalArgumentException.class)
    public void populateCartDataNullTest()
    {
        testClass.populate(cartModel, null);
    }

    /**
     * Create {@link OrderEntryData} for bundle number.
     *
     * @param bundleNo
     *            - Bundle number to set.
     * @return - The newly created {@link OrderEntryData}.
     */
    private OrderEntryData createOrderEntryData(final Integer bundleNo, final String bundleName)
    {
        final OrderEntryData entry = new OrderEntryData();
        entry.setBundleNo(bundleNo);
        final BundleTemplateData bundleTemplate = new BundleTemplateData();
        bundleTemplate.setName(bundleName);
        entry.setRootBundleTemplate(bundleTemplate);
        return entry;
    }
}
