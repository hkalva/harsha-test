package com.lyonscg.bundleapi.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.product.ProductModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 *
 * Test class for DefaultAttributeValueAwareProductBundlePopulator.
 */
@UnitTest
public class DefaultAttributeValueAwareProductBundlePopulatorTest
{
	private DefaultAttributeValueAwareProductBundlePopulator<ProductModel, ProductData> defaultAttributeValueAwareProductBundlePopulator;
	@Mock
	private ProductModel productModel;
	private ProductData productData;

	/**Setting up mock initialization.
	 * Declaring product bundle populator.
	 */
	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		defaultAttributeValueAwareProductBundlePopulator = new DefaultAttributeValueAwareProductBundlePopulator<ProductModel, ProductData>();
		productData = new ProductData();
	}

	/**Testing is individually  sold.
	 * @throws CommerceCartModificationException
	 */
	@Test
	public void populateSoldIndividuallyNullTest() throws CommerceCartModificationException
	{
		Mockito.doReturn(null).when(productModel).getSoldIndividually();
		defaultAttributeValueAwareProductBundlePopulator.populate(productModel, productData);
		Assert.assertTrue(productData.isSoldIndividually());
	}

	/** Testing is we can populate sold individually.
	 * @throws CommerceCartModificationException
	 */
	@Test
	public void populateSoldIndividuallyTrueTest() throws CommerceCartModificationException
	{
		Mockito.doReturn(Boolean.TRUE).when(productModel).getSoldIndividually();
		defaultAttributeValueAwareProductBundlePopulator.populate(productModel, productData);
		Assert.assertTrue(productData.isSoldIndividually());
	}

	/**Testing that it is not possible to populate sold individually.
	 * @throws CommerceCartModificationException
	 */
	@Test
	public void populateSoldIndividuallyFalseTest() throws CommerceCartModificationException
	{
		Mockito.doReturn(Boolean.FALSE).when(productModel).getSoldIndividually();
		defaultAttributeValueAwareProductBundlePopulator.populate(productModel, productData);
		Assert.assertFalse(productData.isSoldIndividually());
	}
}
