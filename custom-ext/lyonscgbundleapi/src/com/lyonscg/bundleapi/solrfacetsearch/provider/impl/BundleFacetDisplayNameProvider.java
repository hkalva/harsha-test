package com.lyonscg.bundleapi.solrfacetsearch.provider.impl;

import java.io.Serializable;
import java.util.Collections;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.solrfacetsearch.provider.FacetDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;


/**
 * Provides facet names to display in the storefront based on bundle name and version.
 */
public class BundleFacetDisplayNameProvider implements FacetDisplayNameProvider, Serializable
{
    private static final long serialVersionUID = -8415948613964179649L;
    private BundleTemplateService bundleTemplateService;
    private SessionService sessionService;
    private CatalogVersionService catalogVersionService;

    /**
     * Gets the locale based on two or one part ISO code.
     * 
     * @param isoCode
     *            the iso code.
     * 
     * @return the locale.
     */
    private Locale getLocale(final String isoCode)
    {
        final String[] splitted_code = isoCode.split("_");
        Locale result;
        if (splitted_code.length == 1)
        {
            result = new Locale(splitted_code[0]);
        }
        else
        {
            result = new Locale(splitted_code[0], splitted_code[1]);
        }
        return result;
    }

    /**
     * Return's the BundleTemplate's localized name along with the version id.
     * 
     * @param query
     *            - Solr search query.
     * @param code
     *            - {@link BundleTemplateModel}'s id.
     * @return the BundleTemplate.name + " " + BundleTemplate.version
     */
    @Override
    public String getDisplayName(final SearchQuery query, final String code)
    {
        final Locale locale = getLocale(query.getLanguage());
        String bundleName = null;
        BundleTemplateModel bundle = null;

        if (query.getCatalogVersions() != null)
        {
            for (final CatalogVersionModel catVersion : query.getCatalogVersions())
            {
                try
                {
                    if (catVersion != null)
                    {
                        bundle = getSessionService().executeInLocalView(new SessionExecutionBody() {
                            @Override
                            public Object execute()
                            {
                                getCatalogVersionService().setSessionCatalogVersions(Collections.singletonList(catVersion));
                                String[] codeVersion = code.split("\\|");
                                if (codeVersion.length == 2)
                                {
                                    return getBundleTemplateService().getBundleTemplateForCode(codeVersion[0], codeVersion[1]);
                                }
                                else if (codeVersion.length == 1)
                                {
                                    return getBundleTemplateService().getBundleTemplateForCode(codeVersion[0]);
                                }
                                return null;
                            }
                        });
                        if (bundle != null)
                            break;
                    }
                }
                catch (final ModelNotFoundException | AmbiguousIdentifierException uie)
                {
                    //do nothing, because we can still search in active session catalog versions
                    continue;
                }
            }
        }

        if (bundle != null)
        {
            String[] codeVersion = code.split("\\|");
            if (codeVersion.length == 2)
            {
                bundleName = bundle.getName(locale) + " " + code.split("\\|")[1];
            }
            else
            {
                bundleName = bundle.getName(locale);
            }
        }
        return bundleName;
    }

    protected BundleTemplateService getBundleTemplateService()
    {
        return bundleTemplateService;
    }

    @Required
    public void setBundleTemplateService(BundleTemplateService bundleTemplateService)
    {
        this.bundleTemplateService = bundleTemplateService;
    }

    protected SessionService getSessionService()
    {
        return sessionService;
    }

    @Required
    public void setSessionService(SessionService sessionService)
    {
        this.sessionService = sessionService;
    }

    protected CatalogVersionService getCatalogVersionService()
    {
        return catalogVersionService;
    }

    @Required
    public void setCatalogVersionService(CatalogVersionService catalogVersionService)
    {
        this.catalogVersionService = catalogVersionService;
    }

}
