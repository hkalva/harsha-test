package com.lyonscg.bundleapi.solrfacetsearch.provider.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.configurablebundleservices.search.solrfacetsearch.provider.impl.ProductBundleTemplatesValueProvider;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;


/**
 * Generic solr index value provider for BundleTemplates. Retrieves the value of spring bean configured
 * "bundlePropertyName" from BundleTemplateModel.
 */
public class ProductBundleTemplatesPropertyValueProvider extends ProductBundleTemplatesValueProvider
{
    private static final long serialVersionUID = -7576733961052081411L;
    private CommonI18NService commonI18NService;
    private String bundlePropertyName;

    /**
     * Creates a collection of field values for {@link BundleTemplateModel} based on configured "bundlePropertyName".
     * 
     * @param indexConfig
     *            -Solr index configuration.
     * @param indexedProperty
     *            -Current property that is indexed.
     * @param model
     *            -{@link ProductModel} that is currently indexed.
     * @return Collection of {@link FieldValue}s
     */
    @Override
    public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
            final Object model) throws FieldValueProviderException
    {
        validateParameterNotNullStandardMessage("model", model);
        final List<FieldValue> fieldValues = new ArrayList<>();
        if (model instanceof ProductModel)
        {
            final ProductModel productModel = (ProductModel) model;
            if (indexedProperty.isLocalized())
            {
                final Collection<LanguageModel> languages = indexConfig.getLanguages();
                for (final LanguageModel language : languages)
                {
                    fieldValues.addAll(createFieldValue(productModel, indexedProperty, language));
                }
            }
            else
            {
                fieldValues.addAll(createFieldValue(productModel, indexedProperty, null));
            }
        }
        return fieldValues;
    }

    /**
     * Creates a List of field values for BundleTemplate.
     * 
     * @param productModel
     *            -{@link ProductModel} that is currently indexed.
     * @param indexedProperty
     *            -Current property that is indexed.
     * @param language
     *            -{@link LanguageModel} that is currently indexed for.
     * @return List of {@link FieldValue}s
     */
    protected List<FieldValue> createFieldValue(final ProductModel productModel, final IndexedProperty indexedProperty,
            LanguageModel language)
    {
        final List<FieldValue> fieldValues = new ArrayList<>();
        final Collection<BundleTemplateModel> bundleTemplates = productModel.getBundleTemplates();
        for (final BundleTemplateModel bundleTemplate : bundleTemplates)
        {
            addFieldValues(fieldValues, indexedProperty, language, getProperty(language, bundleTemplate));
        }
        return fieldValues;
    }

    /**
     * Retrieves the value of spring bean configured "bundlePropertyName" from BundleTemplateModel.
     * 
     * @param language
     *            - {@link LanguageModel} that is currently indexed for.
     * @param bundleTemplate
     *            - {@link ProductModel}'s {@link BundleTemplateModel} that is currently indexed.
     * @return {@link BundleTemplateModel}'s configured 'bundlePropertyName'.
     */
    protected String getProperty(LanguageModel language, final BundleTemplateModel bundleTemplate)
    {
        if (null == language)
        {
            return modelService.getAttributeValue(bundleTemplate, getBundlePropertyName());
        }
        return modelService.getAttributeValue(bundleTemplate, getBundlePropertyName(), getCommonI18NService()
                .getLocaleForLanguage(language));
    }

    protected CommonI18NService getCommonI18NService()
    {
        return commonI18NService;
    }

    @Required
    public void setCommonI18NService(CommonI18NService commonI18NService)
    {
        this.commonI18NService = commonI18NService;
    }

    protected String getBundlePropertyName()
    {
        return bundlePropertyName;
    }

    public void setBundlePropertyName(String bundlePropertyName)
    {
        this.bundlePropertyName = bundlePropertyName;
    }

}
