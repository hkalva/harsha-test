package com.lyonscg.bundleapi.solrfacetsearch.provider.impl;

import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.c2l.LanguageModel;


/**
 * Provides bundle URL for products.
 */
public class BundleUrlValueProvider extends ProductBundleTemplatesPropertyValueProvider
{
    private static final long serialVersionUID = 2145376068164384369L;
    private UrlResolver<BundleTemplateModel> bundleTemplateModelUrlResolver;

    /**
     * Provides bundle URL for products using bundleTemplateModelUrlResolver.
     * @param language
     *            -The {@link LanguageModel} that is currently indexed for.
     * @param bundleTemplate
     *            -{@link ProductModel}'s {@link BundleTemplateModel} that is currently indexed.
     * @return -The URL for the {@link BundleTemplateModel}
     */
    @Override
    protected String getProperty(LanguageModel language, final BundleTemplateModel bundleTemplate)
    {
        i18nService.setCurrentLocale(getCommonI18NService().getLocaleForLanguage(language));
        return getBundleTemplateModelUrlResolver().resolve(bundleTemplate);
    }

    protected UrlResolver<BundleTemplateModel> getBundleTemplateModelUrlResolver()
    {
        return bundleTemplateModelUrlResolver;
    }

    @Required
    public void setBundleTemplateModelUrlResolver(UrlResolver<BundleTemplateModel> bundleTemplateModelUrlResolver)
    {
        this.bundleTemplateModelUrlResolver = bundleTemplateModelUrlResolver;
    }
}
