package com.lyonscg.bundleapi.bundle;

import java.util.Set;

import de.hybris.platform.configurablebundleservices.bundle.BundleRuleService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.configurablebundleservices.model.ChangeProductPriceBundleRuleModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;


/**
 * Commerce service that exposes methods to find a matching rule for the product price.
 */
public interface LeaBundleRuleService extends BundleRuleService
{
    /**
     * Finds the price rule for the combinations of targetProduct, BundleTemplate and other products to be considered in
     * the bundle.
     *
     * @param bundleTemplate
     *            - Bundle template associated with the target product.
     * @param targetProduct
     *            - Target product to find the discounted price rule.
     * @param currency
     *            - Currency used.
     * @param otherProductsInSameBundle
     *            - Other possible conditional products to be considered for triggering the price rule.
     * @return The price rule for the combination.
     */
    ChangeProductPriceBundleRuleModel getLowestPriceForTargetProductAndTemplate(BundleTemplateModel bundleTemplate,
            ProductModel targetProduct, CurrencyModel currency, Set<ProductModel> otherProductsInSameBundle);
}
