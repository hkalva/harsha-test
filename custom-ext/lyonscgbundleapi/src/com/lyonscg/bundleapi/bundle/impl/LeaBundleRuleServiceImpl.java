package com.lyonscg.bundleapi.bundle.impl;

import java.util.Set;

import com.lyonscg.bundleapi.bundle.LeaBundleRuleService;

import de.hybris.platform.configurablebundleservices.bundle.impl.DefaultBundleRuleService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.configurablebundleservices.model.ChangeProductPriceBundleRuleModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;


/**
 * Lyonscg implementation of {@link LeaBundleRuleService} just to expose the protected method in
 * {@link DefaultBundleRuleService}.
 */
public class LeaBundleRuleServiceImpl extends DefaultBundleRuleService implements LeaBundleRuleService
{
    /**
     * Finds the price rule for the combinations of targetProduct, BundleTemplate and other products to be considered in
     * the bundle. This method delegates the call to its super implementation.
     * 
     * @param bundleTemplate
     *            - Bundle template associated with the target product.
     * @param targetProduct
     *            - Target product to find the discounted price rule.
     * @param currency
     *            - Currency used.
     * @param otherProductsInSameBundle
     *            - Other possible conditional products to be considered for triggering the price rule.
     * @return The price rule for the combination.
     */
    @Override
    public ChangeProductPriceBundleRuleModel getLowestPriceForTargetProductAndTemplate(BundleTemplateModel bundleTemplate,
            ProductModel targetProduct, CurrencyModel currency, Set<ProductModel> otherProductsInSameBundle)
    {
        return super.getLowestPriceForTargetProductAndTemplate(bundleTemplate, targetProduct, currency,
                otherProductsInSameBundle);
    }
}
