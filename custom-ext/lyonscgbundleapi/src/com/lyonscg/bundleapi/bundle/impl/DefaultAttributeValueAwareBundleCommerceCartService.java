package com.lyonscg.bundleapi.bundle.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.configurablebundleservices.bundle.impl.DefaultBundleCommerceCartService;
import de.hybris.platform.core.model.product.ProductModel;

import org.apache.commons.lang.BooleanUtils;


/**
 * Attribute Product.soldIndividually has a default value true defined in configurablebundleservices-items.xml. But as
 * the bundlee exts are added after init the default value is not set for products. To overcome that
 * BooleanUtils.isNotFalse is used to check SoldIndividually attribute of the product.
 */
public class DefaultAttributeValueAwareBundleCommerceCartService extends DefaultBundleCommerceCartService
{
	@Override
	protected void checkIsSoldIndividually(final ProductModel productModel, final int bundleNo)
			throws CommerceCartModificationException
	{
		if ((bundleNo != 0) || (BooleanUtils.isNotFalse(productModel.getSoldIndividually())))
		{
			return;
		}
		throw new CommerceCartModificationException("The given product '" + productModel.getCode()
				+ "' must not be sold individually.");
	}
}
