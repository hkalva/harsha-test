package com.lyonscg.bundleapi.url.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.site.BaseSiteService;

import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.lyonscg.services.url.UrlHelper;


/**
 * This class is to resolve URLs for BundleTemplateModel.
 */
public class DefaultBundleTemplateModelUrlResolver extends AbstractUrlResolver<BundleTemplateModel>
{
    private final String CACHE_KEY = DefaultBundleTemplateModelUrlResolver.class.getName();
    private BaseSiteService baseSiteService;
    private String defaultPattern;

    protected String getDefaultPattern()
    {
        return defaultPattern;
    }

    @Required
    public void setDefaultPattern(final String defaultPattern)
    {
        this.defaultPattern = defaultPattern;
    }

    protected BaseSiteService getBaseSiteService()
    {
        return baseSiteService;
    }

    @Required
    public void setBaseSiteService(final BaseSiteService baseSiteService)
    {
        this.baseSiteService = baseSiteService;
    }

    protected String getPattern()
    {
        return getDefaultPattern();
    }

    @Override
    protected String getKey(final BundleTemplateModel source)
    {
        return CACHE_KEY + "." + source.getPk().toString();
    }

    @Override
    protected String resolveInternal(final BundleTemplateModel source)
    {
        final BaseSiteModel currentBaseSite = getBaseSiteService().getCurrentBaseSite();
        String url = getPattern();
        if (currentBaseSite != null && url.contains("{baseSite-uid}"))
        {
            url = url.replace("{baseSite-uid}", currentBaseSite.getUid());
        }
        if (url.contains("{bundle-name}"))
        {
            url = url.replace("{bundle-name}", UrlHelper.urlSafe(StringUtils.lowerCase(source.getName(Locale.ENGLISH))));
        }
        if (url.contains("{bundle-code}"))
        {
            url = url.replace("{bundle-code}", source.getId());
        }
        return url.replace("//", "/");
    }

}
