/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.lyonscg.bundleapi.url.impl;

import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;

import org.springframework.beans.factory.annotation.Required;


/**
 * This class is to resolve URLs for BundleTemplateData.
 */
public class DefaultBundleTemplateDataUrlResolver extends AbstractUrlResolver<BundleTemplateData>
{
	private final String CACHE_KEY = DefaultBundleTemplateDataUrlResolver.class.getName();

	private BundleTemplateService bundleTemplateService;
	private UrlResolver<BundleTemplateModel> bundleTemplateModelUrlResolver;

	protected BundleTemplateService getBundleTemplateService()
	{
		return bundleTemplateService;
	}

	@Required
	public void setBundleTemplateService(final BundleTemplateService bundleTemplateService)
	{
		this.bundleTemplateService = bundleTemplateService;
	}

	protected UrlResolver<BundleTemplateModel> getBundleTemplateModelUrlResolver()
	{
		return bundleTemplateModelUrlResolver;
	}

	@Required
	public void setBundleTemplateModelUrlResolver(final UrlResolver<BundleTemplateModel> bundleTemplateModelUrlResolver)
	{
		this.bundleTemplateModelUrlResolver = bundleTemplateModelUrlResolver;
	}

	@Override
	protected String getKey(final BundleTemplateData source)
	{
		return CACHE_KEY + "." + source.getId();
	}

	@Override
	protected String resolveInternal(final BundleTemplateData source)
	{
		// Lookup the BundleTemplate
		final BundleTemplateModel bundleTemplate = getBundleTemplateService().getBundleTemplateForCode(source.getId());
		return getBundleTemplateModelUrlResolver().resolve(bundleTemplate);
	}
}
