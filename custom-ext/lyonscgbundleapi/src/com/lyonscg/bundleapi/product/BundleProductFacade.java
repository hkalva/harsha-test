package com.lyonscg.bundleapi.product;

import java.util.List;

import com.lyonscg.bundleapi.data.BundleProductInfo;
import com.lyonscg.bundleapi.data.BundledProductPrice;

import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;


/**
 * Bundle product facade that provides functionality to find BundleTemplate and populate it on to BundleTemplateData.
 */
public interface BundleProductFacade extends ProductFacade
{

    /**
     * Get complete bundle template details along with product details.
     *
     * @param bundleCode
     * @return {@link BundleTemplateData} of associated product
     */
    BundleTemplateData getCompleteBundleDetails(String bundleCode);

    /**
     * Get prices with and without bundle discounts.
     *
     * @param bundleProductsInfos
     *            - List of pojo objects with product code and bundle template id.
     * @return {@link BundledProductPrice} - price with and without bundle price rules applied.
     */
    BundledProductPrice getBundlePriceForProductsAndTemplates(List<BundleProductInfo> bundleProductsInfos);

}
