package com.lyonscg.bundleapi.product.impl;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.commercefacades.product.data.ReviewData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.configurablebundleservices.model.ChangeProductPriceBundleRuleModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.lyonscg.bundleapi.bundle.LeaBundleRuleService;
import com.lyonscg.bundleapi.data.BundleProductInfo;
import com.lyonscg.bundleapi.data.BundledProductPrice;
import com.lyonscg.bundleapi.product.BundleProductFacade;


/**
 * Default implementation of Bundle Product Facade {@link BundleProductFacade}.
 */
public class DefaultBundleProductFacade implements BundleProductFacade
{
	private ProductFacade productFacade;
	private BundleTemplateService bundleTemplateService;
	private Converter<BundleTemplateModel, BundleTemplateData> bundleTemplateConverter;
    private CommerceCommonI18NService commerceCommonI18NService;
    private ProductService productService;
    private CommercePriceService commercePriceService;
    private LeaBundleRuleService leaBundleRuleService;
    private PriceDataFactory priceDataFactory;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BundleTemplateData getCompleteBundleDetails(final String bundleCode)
	{
		return convertToBundleData(getRootParentBundle(getBundleTemplateService().getBundleTemplateForCode(bundleCode)));
	}

    /**
     * Calculates prices with and without bundle discounts.
     *
     * @param bundleProductsInfos
     *            - List of pojo objects with product code and bundle template id.
     * @return {@link BundledProductPrice} - priceData with and without bundle price discount applied.
     */
    @Override
    public BundledProductPrice getBundlePriceForProductsAndTemplates(List<BundleProductInfo> bundleProductsInfos)
    {
        if (CollectionUtils.isEmpty(bundleProductsInfos))
        {
            return null;
        }
        List<ProductModel> products = new ArrayList<>(bundleProductsInfos.size());
        List<BundleTemplateModel> bundleTemplates = new ArrayList<>(bundleProductsInfos.size());
        List<BigDecimal> productPrice = new ArrayList<>(bundleProductsInfos.size());
        BigDecimal priceWithoutBundleDiscount = BigDecimal.ZERO;
        for (int i = 0; i < bundleProductsInfos.size(); i++)
        {
            ProductModel product = getProductService().getProductForCode(bundleProductsInfos.get(i).getProductCode());
            products.add(product);
            bundleTemplates.add(getBundleTemplateService().getBundleTemplateForCode(bundleProductsInfos.get(i).getBundleId()));
            PriceInformation priceInfo = getCommercePriceService().getWebPriceForProduct(product);
            BigDecimal price = BigDecimal.valueOf(priceInfo.getPriceValue().getValue());
            productPrice.add(price);
            priceWithoutBundleDiscount = priceWithoutBundleDiscount.add(price);
        }
        CurrencyModel currency = getCommerceCommonI18NService().getCurrentCurrency();
        BundledProductPrice bundledProductPrice = new BundledProductPrice();
        bundledProductPrice.setPriceWithBundle(getPriceDataFactory().create(PriceDataType.BUY,
                calculateDiscountedPrice(bundleProductsInfos, products, bundleTemplates, productPrice, currency), currency));
        bundledProductPrice
                .setPriceWithoutBundle(getPriceDataFactory().create(PriceDataType.BUY, priceWithoutBundleDiscount, currency));
        return bundledProductPrice;
    }

    /**
     * Calculates discounted bundle price.
     *
     * @param bundleProductsInfos
     *            - Input bundle product infos.
     * @param products
     *            - List of ProductModels from bundle product infos.
     * @param bundleTemplates
     *            - List of BundleTemplateModels from bundle product infos.
     * @param productPrice
     *            - List of productPrices for the products.
     * @param currency
     *            - Currency used.
     * @return - Total price including bundle discounts.
     */
    protected BigDecimal calculateDiscountedPrice(List<BundleProductInfo> bundleProductsInfos, List<ProductModel> products,
            List<BundleTemplateModel> bundleTemplates, List<BigDecimal> productPrice, CurrencyModel currency)
    {
        BigDecimal priceWithBundleDiscount = BigDecimal.ZERO;
        List<BigDecimal> lowestBundleProductPrices = populateBundleProductPrices(bundleProductsInfos, currency, products,
                bundleTemplates, productPrice);
        for (BigDecimal lowestBundleProductPrice : lowestBundleProductPrices)
        {
            priceWithBundleDiscount = priceWithBundleDiscount.add(lowestBundleProductPrice);
        }
        return priceWithBundleDiscount;
    }

    /**
     * Finds lowest bundle discounted prices for products.
     * 
     * @param bundleProductsInfos
     *            - Input bundle product infos.
     * @param currency
     *            - Currency used.
     * @param products
     *            - List of ProductModels from bundle product infos.
     * @param bundleTemplates
     *            - List of BundleTemplateModels from bundle product infos.
     * @param productPrice-
     *            List of productPrices for the products.
     * @return - List of bundle discount prices for products.
     */
    protected List<BigDecimal> populateBundleProductPrices(List<BundleProductInfo> bundleProductsInfos,
            final CurrencyModel currency, List<ProductModel> products, List<BundleTemplateModel> bundleTemplates,
            List<BigDecimal> productPrice)
    {
        List<BigDecimal> lowestBundleProductPrices = new ArrayList<>(productPrice);
        for (int i = 0; i < bundleProductsInfos.size(); i++)
        {
            final int j = i;
            ChangeProductPriceBundleRuleModel productPriceRule = getLeaBundleRuleService()
                    .getLowestPriceForTargetProductAndTemplate(bundleTemplates.get(i), products.get(i), currency,
                            products.stream().filter(product -> product != products.get(j)).collect(Collectors.toSet()));

            if (null != productPriceRule)
            {
                BigDecimal productPriceRulePrice = productPriceRule.getPrice();
                if (null != productPriceRulePrice)
                {
                    lowestBundleProductPrices.set(i, productPriceRulePrice);
                }
            }

        }
        return lowestBundleProductPrices;
    }
	/**
	 * Converts and populates attributes by using corresponding converters.
	 */
	protected BundleTemplateData convertToBundleData(final BundleTemplateModel parentBundle)
	{
		final BundleTemplateData parentBundleData = getBundleTemplateConverter().convert(parentBundle);
		final List<BundleTemplateModel> childBundles = parentBundle.getChildTemplates();
		if (CollectionUtils.isNotEmpty(childBundles))
		{
			final List<BundleTemplateData> chileBundleTemplateDatas = new ArrayList<BundleTemplateData>(childBundles.size());
			for (final BundleTemplateModel childBundle : childBundles)
			{
				chileBundleTemplateDatas.add(convertToBundleData(childBundle));
			}
			chileBundleTemplateDatas.sort(Comparator.comparing(BundleTemplateData::getNoOfRequiredBundles));
			parentBundleData.setBundleTemplates(chileBundleTemplateDatas);
		}
		return parentBundleData;
	}

	/**
	 * Recursively iterates to identify the root parent bundle.
	 */
	protected BundleTemplateModel getRootParentBundle(final BundleTemplateModel bundle)
	{
		final BundleTemplateModel parentBundle = bundle.getParentTemplate();
		if (null == parentBundle)
		{
			return bundle;
		}
		return getRootParentBundle(parentBundle);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ProductData getProductForOptions(final ProductModel productModel, final Collection<ProductOption> options)
	{

		return getProductFacade().getProductForOptions(productModel, options);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ProductData getProductForCodeAndOptions(final String code, final Collection<ProductOption> options)
			throws UnknownIdentifierException, IllegalArgumentException
	{
		return getProductFacade().getProductForCodeAndOptions(code, options);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ReviewData postReview(final String productCode, final ReviewData reviewData) throws UnknownIdentifierException,
			IllegalArgumentException
	{

		return getProductFacade().postReview(productCode, reviewData);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ReviewData> getReviews(final String productCode) throws UnknownIdentifierException
	{

		return getProductFacade().getReviews(productCode);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ReviewData> getReviews(final String productCode, final Integer numberOfReviews) throws UnknownIdentifierException,
			IllegalArgumentException
	{
		return getProductFacade().getReviews(productCode, numberOfReviews);
	}

	/**
	 * {@inheritDoc}
	 */
	@Deprecated
	@Override
	public List<ProductReferenceData> getProductReferencesForCode(final String code, final ProductReferenceTypeEnum referenceType,
			final List<ProductOption> options, final Integer limit)
	{
		return getProductFacade().getProductReferencesForCode(code, referenceType, options, limit);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductReferenceData> getProductReferencesForCode(final String code,
			final List<ProductReferenceTypeEnum> referenceTypes, final List<ProductOption> options, final Integer limit)
	{
		return getProductFacade().getProductReferencesForCode(code, referenceTypes, options, limit);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ConfigurationInfoData> getConfiguratorSettingsForCode(final String code)
	{
		return getProductFacade().getConfiguratorSettingsForCode(code);
	}

	protected BundleTemplateService getBundleTemplateService()
	{
		return bundleTemplateService;
	}

	@Required
	public void setBundleTemplateService(final BundleTemplateService bundleTemplateService)
	{
		this.bundleTemplateService = bundleTemplateService;
	}

	protected Converter<BundleTemplateModel, BundleTemplateData> getBundleTemplateConverter()
	{
		return bundleTemplateConverter;
	}

	@Required
	public void setBundleTemplateConverter(final Converter<BundleTemplateModel, BundleTemplateData> bundleTemplateConverter)
	{
		this.bundleTemplateConverter = bundleTemplateConverter;
	}

	public ProductFacade getProductFacade()
	{
		return productFacade;
	}

	@Required
	public void setProductFacade(final ProductFacade productFacade)
	{
		this.productFacade = productFacade;
	}
	
    protected CommerceCommonI18NService getCommerceCommonI18NService()
    {
        return commerceCommonI18NService;
    }

    @Required
    public void setCommerceCommonI18NService(CommerceCommonI18NService commerceCommonI18NService)
    {
        this.commerceCommonI18NService = commerceCommonI18NService;
    }

    protected ProductService getProductService()
    {
        return productService;
    }

    @Required
    public void setProductService(ProductService productService)
    {
        this.productService = productService;
    }

    protected CommercePriceService getCommercePriceService()
    {
        return commercePriceService;
    }

    @Required
    public void setCommercePriceService(CommercePriceService commercePriceService)
    {
        this.commercePriceService = commercePriceService;
    }

    protected LeaBundleRuleService getLeaBundleRuleService()
    {
        return leaBundleRuleService;
    }

    @Required
    public void setLeaBundleRuleService(LeaBundleRuleService leaBundleRuleService)
    {
        this.leaBundleRuleService = leaBundleRuleService;
    }

    protected PriceDataFactory getPriceDataFactory()
    {
        return priceDataFactory;
    }

    @Required
    public void setPriceDataFactory(PriceDataFactory priceDataFactory)
    {
        this.priceDataFactory = priceDataFactory;
    }
}
