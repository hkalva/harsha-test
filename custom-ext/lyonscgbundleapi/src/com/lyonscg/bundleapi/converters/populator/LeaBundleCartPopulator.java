package com.lyonscg.bundleapi.converters.populator;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.configurablebundlefacades.order.converters.populator.AbstractBundleOrderPopulator;
import de.hybris.platform.core.model.order.CartModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Populates bundle products map to group bundles in cart.
 *
 * @param <S>
 *            - Source {@link CartModel}.
 * @param <T>
 *            - Target {@link CartData}.
 */
public class LeaBundleCartPopulator<S extends CartModel, T extends CartData> extends AbstractBundleOrderPopulator<S, T>
{
    /**
     * Populate method to populate bundle map in the cart data {@link CartData}.
     *
     * @param source
     *            - Source cart model.
     * @param target
     *            - Target cart data.
     */
    @Override
    public void populate(final S source, final T target)
    {
        validateParameterNotNullStandardMessage("source", source);
        validateParameterNotNullStandardMessage("target", target);

        addBundleEntriesMap(target);
    }

    /**
     * Adds bundle cart entries to a separate map.
     *
     * @param cartData
     *            - Target cart data.
     */
    protected void addBundleEntriesMap(final CartData cartData)
    {
        final Collection<OrderEntryData> entries = cartData.getEntries();
        final Map<String, List<OrderEntryData>> bundleEntriesMap = new HashMap<>();
        for (final OrderEntryData orderEntryData : entries)
        {
            final Integer bundleNo = orderEntryData.getBundleNo();
            if (bundleNo.intValue() > 0)
            {
                final String bundleKey = bundleNo + ":" + orderEntryData.getRootBundleTemplate().getName();
                List<OrderEntryData> tempList = bundleEntriesMap.get(bundleKey);
                if (tempList == null)
                {
                    tempList = new ArrayList<>();
                }
                tempList.add(orderEntryData);
                bundleEntriesMap.put(bundleKey, tempList);
            }
        }
        cartData.setBundleEntriesMap(bundleEntriesMap);
    }

}
