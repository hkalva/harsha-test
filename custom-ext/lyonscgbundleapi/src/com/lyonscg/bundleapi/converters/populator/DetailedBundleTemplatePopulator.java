package com.lyonscg.bundleapi.converters.populator;

import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.configurablebundlefacades.data.BundleTemplateData;
import de.hybris.platform.configurablebundleservices.model.BundleSelectionCriteriaModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.configurablebundleservices.model.PickExactlyNBundleSelectionCriteriaModel;
import de.hybris.platform.configurablebundleservices.model.PickNToMBundleSelectionCriteriaModel;
import de.hybris.platform.converters.ConfigurablePopulator;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 *
 * Populator implementation for {@link BundleTemplateModel} as source and {@link BundleTemplateData} as target type.
 * Populates additional fields from BundleTemplateModel.
 */

public class DetailedBundleTemplatePopulator<SOURCE extends BundleTemplateModel, TARGET extends BundleTemplateData> implements
		Populator<SOURCE, TARGET>
{
	private Converter<ProductModel, ProductData> productConverter;
	private ConfigurablePopulator<ProductModel, ProductData, ProductOption> productConfiguredPopulator;
	private Collection<ProductOption> productOptions;

	@Override
	public void populate(final SOURCE source, final TARGET target)
	{
		target.setMinItemsAllowed(getMinNoOfProductsAllowed(source));
		target.setProducts(getProductsForBundles(source));
		target.setDescription(source.getDescription());
		target.setNoOfRequiredBundles(source.getRequiredBundleTemplates().size());
	}

	/**
	 * Helper method to populate Productdata in BundleTemplateData. criteria.
	 *
	 * @param source
	 * @return Products associated to the BundleTemplate.
	 */
	protected List<ProductData> getProductsForBundles(final SOURCE source)
	{
		final List<ProductModel> products = source.getProducts();
		if (CollectionUtils.isEmpty(products))
		{
			return null;
		}
		final List<ProductData> productDatas = new ArrayList<ProductData>(products.size());
		for (final ProductModel productModel : products)
		{
			final ProductData productData = getProductConverter().convert(productModel);
			getProductConfiguredPopulator().populate(productModel, productData, getProductOptions());
			productDatas.add(productData);
		}
		return productDatas;
	}

	/**
	 * Helper method to find minimum possible product selections for the given BundleTemplate based on Selection
	 * criteria.
	 *
	 * @param bundleTemplate
	 * @return Number of products allowed to the BundleTemplate
	 */
	protected int getMinNoOfProductsAllowed(final BundleTemplateModel bundleTemplate)
	{
		int minItemsAllowed = 0;

		final BundleSelectionCriteriaModel selectionCriteria = bundleTemplate.getBundleSelectionCriteria();

		if (selectionCriteria instanceof PickNToMBundleSelectionCriteriaModel)
		{
			minItemsAllowed = ((PickNToMBundleSelectionCriteriaModel) selectionCriteria).getN().intValue();
		}
		else if (selectionCriteria instanceof PickExactlyNBundleSelectionCriteriaModel)
		{
			minItemsAllowed = ((PickExactlyNBundleSelectionCriteriaModel) selectionCriteria).getN().intValue();
		}
		return minItemsAllowed;
	}

	protected ConfigurablePopulator<ProductModel, ProductData, ProductOption> getProductConfiguredPopulator()
	{
		return productConfiguredPopulator;
	}

	@Required
	public void setProductConfiguredPopulator(
			final ConfigurablePopulator<ProductModel, ProductData, ProductOption> productConfiguredPopulator)
	{
		this.productConfiguredPopulator = productConfiguredPopulator;
	}

	protected Collection<ProductOption> getProductOptions()
	{
		return productOptions;
	}

	@Required
	public void setProductOptions(final Collection<ProductOption> productOptions)
	{
		this.productOptions = productOptions;
	}

	protected Converter<ProductModel, ProductData> getProductConverter()
	{
		return productConverter;
	}

	@Required
	public void setProductConverter(final Converter<ProductModel, ProductData> productConverter)
	{
		this.productConverter = productConverter;
	}

}
