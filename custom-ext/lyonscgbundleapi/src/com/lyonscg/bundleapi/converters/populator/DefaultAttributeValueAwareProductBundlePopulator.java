package com.lyonscg.bundleapi.converters.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.configurablebundlefacades.converters.populator.ProductBundlePopulator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang.BooleanUtils;


/**
 * Attribute Product.soldIndividually has a default value true defined in configurablebundleservices-items.xml. But as
 * the bundlee exts are added after init the default value is not set for products. To overcome that
 * BooleanUtils.isNotFalse is used to populate SoldIndividually attribute of ProductData.
 */
public class DefaultAttributeValueAwareProductBundlePopulator<SOURCE extends ProductModel, TARGET extends ProductData> extends
		ProductBundlePopulator<SOURCE, TARGET>
{
	@Override
	public void populate(final SOURCE productModel, final TARGET productData) throws ConversionException
	{
		super.populate(productModel, productData);
		productData.setSoldIndividually(BooleanUtils.isNotFalse(productModel.getSoldIndividually()));
	}
}
