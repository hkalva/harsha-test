/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 *
 */
package com.lyonscg.bundleapi.converters.populator;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.converters.Populator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * New Bundle specific Cart Restoration populator to populate the bundle modifications Map which will hold a map of
 * bundleKey with its corresponding list of modifications.
 */
public class BundleCartRestorationPopulator implements Populator<CommerceCartRestoration, CartRestorationData>
{
    @Override
    public void populate(final CommerceCartRestoration source, final CartRestorationData target)
    {
        validateParameterNotNullStandardMessage("source", source);
        validateParameterNotNullStandardMessage("target", target);

        addBundleModificationsMap(target);
    }

    /**
     * Adds bundle cart modifications to a separate map.
     *
     * @param CartRestorationData
     *            - Target CartRestorationData data.
     */
    protected void addBundleModificationsMap(final CartRestorationData cartRestorationData)
    {
        final Collection<CartModificationData> modifcations = cartRestorationData.getModifications();
        final Map<String, List<CartModificationData>> bundleModificationsMap = new HashMap<>();
        for (final CartModificationData cartModificationData : modifcations)
        {

            final Integer bundleNo = cartModificationData.getEntry().getBundleNo();
            if (bundleNo.intValue() > 0)
            {
                final String bundleKey = bundleNo + ":" + cartModificationData.getEntry().getRootBundleTemplate().getName();
                List<CartModificationData> tempList = bundleModificationsMap.get(bundleKey);
                if (tempList == null)
                {
                    tempList = new ArrayList<>();
                }
                tempList.add(cartModificationData);
                bundleModificationsMap.put(bundleKey, tempList);
            }
        }
        cartRestorationData.setBundleModificationsMap(bundleModificationsMap);
    }
}
