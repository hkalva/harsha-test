/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.lyonscg.bundleapi.order.hook.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.hook.CommerceAddToCartMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.servicelayer.model.ModelService;

import org.apache.log4j.Logger;


/**
 * Overriding the afterAddToCart method to set the OrderEntry's bundle number and bundle Template from the
 * CommerceCartModification object.
 */
public class BundleCommerceAddToCartMethodHook implements CommerceAddToCartMethodHook
{
    private static final Logger LOG = Logger.getLogger(BundleCommerceAddToCartMethodHook.class);

    private ModelService modelService;

    /*
     * (non-Javadoc)
     * 
     * @see
     * de.hybris.platform.commerceservices.order.hook.CommerceAddToCartMethodHook#afterAddToCart(de.hybris.platform.
     * commerceservices.service.data.CommerceCartParameter,
     * de.hybris.platform.commerceservices.order.CommerceCartModification)
     */
    @Override
    public void afterAddToCart(final CommerceCartParameter parameters, final CommerceCartModification result)
            throws CommerceCartModificationException
    {
        LOG.debug("BundleCommerceAddToCartMethodHook : afterAddToCart : Parameter Object :" + parameters.getProduct().getCode()
                + ":" + parameters.getBundleNumber());
        LOG.debug("BundleCommerceAddToCartMethodHook : afterAddToCart : Entry Object : " + result.getEntry().getProduct());
        if (parameters.getBundleNumber() != 0)
        {
            result.getEntry().setBundleNo(parameters.getBundleNumber());
            result.getEntry().setBundleTemplate(parameters.getBundleTemplate());
        }
        getModelService().save(result.getEntry());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * de.hybris.platform.commerceservices.order.hook.CommerceAddToCartMethodHook#beforeAddToCart(de.hybris.platform
     * .commerceservices.service.data.CommerceCartParameter)
     */
    @Override
    public void beforeAddToCart(final CommerceCartParameter parameters) throws CommerceCartModificationException
    {
        // TODO Auto-generated method stub

    }

    /**
     * @return the modelService
     */
    public ModelService getModelService()
    {
        return modelService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    public void setModelService(final ModelService modelService)
    {
        this.modelService = modelService;
    }

}
