package com.lyonscg.bundleapi.order.converters.populator;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * Populator to set lost stock data values back on {@link OrderEntryData} object.
 */
public class BundleStockRePopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{
    private Populator<AbstractOrderEntryModel, OrderEntryData> subscriptionOrderEntryPopulator;

    /**
     * Populates lost stock data values back on {@link OrderEntryData} object.
     *
     * @param source
     *            - Source {@link AbstractOrderEntryModel}.
     * @param target
     *            - Target {@link OrderEntryData}.
     */
    @Override
    public void populate(AbstractOrderEntryModel source, OrderEntryData target) throws ConversionException
    {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");
        StockData stockData = getStockData(target);
        getSubscriptionOrderEntryPopulator().populate(source, target);
        populateStockData(source, target, stockData);
    }

    /**
     * Populates retrieved Stock data on {@link OrderEntryData}.
     *
     * @param source
     *            - Source {@link AbstractOrderEntryModel}.
     * @param target
     *            - Target {@link OrderEntryData}.
     * @param stockData
     *            - Retrieved Stock data {@link StockData}.
     */
    private void populateStockData(AbstractOrderEntryModel source, OrderEntryData target, StockData stockData)
    {
        if (null != stockData && ObjectUtils.compare(source.getBundleNo(), NumberUtils.INTEGER_ZERO, false) > 0)
        {
            ProductData productData = target.getProduct();
            if (null != productData && null == productData.getStock())
            {
                productData.setStock(stockData);
            }
        }
    }

    /**
     * Retrieves {@link StockData} from {@link OrderEntryData}.
     *
     * @param target
     *            - Target {@link OrderEntryData}.
     * @return - Retrieved Stock data {@link StockData}.
     */
    private StockData getStockData(OrderEntryData target)
    {
        ProductData initialProductData = target.getProduct();
        if (null != initialProductData)
        {
            return initialProductData.getStock();
        }
        return null;
    }

    protected Populator<AbstractOrderEntryModel, OrderEntryData> getSubscriptionOrderEntryPopulator()
    {
        return subscriptionOrderEntryPopulator;
    }

    @Required
    public void setSubscriptionOrderEntryPopulator(
            Populator<AbstractOrderEntryModel, OrderEntryData> subscriptionOrderEntryPopulator)
    {
        this.subscriptionOrderEntryPopulator = subscriptionOrderEntryPopulator;
    }

}
