/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 *
 */
package com.lyonscg.bundleapi.order.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.order.CommerceCartMergingException;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.impl.CommerceCartMergingStrategy;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartMergingStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.security.access.AccessDeniedException;


/**
 * Overriding DefaultCommerceCartMergingStrategy class to set the Bundle number and Bundle Template object in the
 * CommerceCartParameter Object, which will be used in Add to Cart function that creates the new OrderEntry Object.
 */
public class BundleCommerceCartMergingStrategy extends DefaultCommerceCartMergingStrategy implements CommerceCartMergingStrategy
{

    private UserService userService;

    /*
     * (non-Javadoc)
     * 
     * @see
     * de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartMergingStrategy#mergeCarts(de.hybris.platform
     * .core.model.order.CartModel, de.hybris.platform.core.model.order.CartModel, java.util.List)
     */
    @Override
    public void mergeCarts(final CartModel fromCart, final CartModel toCart, final List<CommerceCartModification> modifications)
            throws CommerceCartMergingException
    {
        UserModel currentUser = null;
        if (userService != null)
        {
            currentUser = getUserService().getCurrentUser();
        }

        if (currentUser == null || (userService != null && userService.isAnonymousUser(currentUser)))
        {
            throw new AccessDeniedException("Only logged user can merge carts!");
        }

        validateParameterNotNull(fromCart, "fromCart can not be null");
        validateParameterNotNull(toCart, "toCart can not be null");

        if (!getBaseSiteService().getCurrentBaseSite().equals(fromCart.getSite()))
        {
            throw new CommerceCartMergingException(String.format("Current site %s is not equal to cart %s site %s",
                    getBaseSiteService().getCurrentBaseSite(), fromCart, fromCart.getSite()));
        }

        if (!getBaseSiteService().getCurrentBaseSite().equals(toCart.getSite()))
        {
            throw new CommerceCartMergingException(String.format("Current site %s is not equal to cart %s site %s",
                    getBaseSiteService().getCurrentBaseSite(), toCart, toCart.getSite()));
        }

        if (fromCart.getGuid().equals(toCart.getGuid()))
        {
            throw new CommerceCartMergingException("Cannot merge cart to itself!");
        }

        /* Calculate the maximum bundle no within the entries in the To Cart. */
        final int toCartMaxBundleNo = calcMaxBundleNoInCart(toCart);

        final List<CommerceCartParameter> parameterList = new ArrayList<>();
        for (final AbstractOrderEntryModel entry : fromCart.getEntries())
        {
            final CommerceCartParameter newCartParameter = new CommerceCartParameter();
            newCartParameter.setEnableHooks(true);
            newCartParameter.setCart(toCart);
            newCartParameter.setProduct(entry.getProduct());
            newCartParameter.setPointOfService(entry.getDeliveryPointOfService());
            newCartParameter.setQuantity(entry.getQuantity() == null ? 0l : entry.getQuantity().longValue());
            newCartParameter.setUnit(entry.getUnit());
            newCartParameter.setCreateNewEntry(false);
            /*
             * Lea Customization to set the Entry's Bundle number into the CommerceCartParameter that is going to be
             * used in creation of the new EntryModel / CommerceCartModification. Increment the bundle No by the max
             * bundle number from the ToCart, so that the new bundle entries will have unique numbers.
             */
            if (entry.getBundleNo() != null && entry.getBundleNo() > 0)
            {
                newCartParameter.setBundleNumber(toCartMaxBundleNo + entry.getBundleNo().intValue());
                newCartParameter.setBundleTemplate(entry.getBundleTemplate());
            }

            parameterList.add(newCartParameter);
        }
        mergeModificationsToList(getCommerceAddToCartStrategy().addToCart(parameterList), modifications);

        toCart.setCalculated(Boolean.FALSE);
        //TODO payment transactions - to clear or not to clear...

        getModelService().save(toCart);
        getModelService().remove(fromCart);
    }

    /**
     * Method to calculate the maximum bundle no out of all the entries present in the Cart.
     *
     * @param cart
     *            CartModel Objet.
     * @return max bundle number.
     */
    protected int calcMaxBundleNoInCart(final CartModel cart)
    {

        int ret = 0;
        final Iterator<AbstractOrderEntryModel> entries = cart.getEntries().iterator();

        while (entries.hasNext())
        {
            final AbstractOrderEntryModel abstractOrderEntryModel = entries.next();
            final int currentBundleNo = abstractOrderEntryModel.getBundleNo() == null ? 0 : abstractOrderEntryModel.getBundleNo()
                    .intValue();
            if (currentBundleNo > ret)
            {
                ret = currentBundleNo;
            }
        }
        return ret;
    }

    /**
     * @return the userService
     */
    @Override
    public UserService getUserService()
    {
        return userService;
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Override
    public void setUserService(final UserService userService)
    {
        this.userService = userService;
    }

}
