/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.capgemini.staticversioning.constants;

/**
 * Global class for all Lyonscgstaticversioning constants. You can add global constants for your extension into this class.
 */
public final class LyonscgstaticversioningConstants extends GeneratedLyonscgstaticversioningConstants
{
	public static final String EXTENSIONNAME = "capgeministaticversioningaddon";

	private LyonscgstaticversioningConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
