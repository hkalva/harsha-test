/**
 *
 */
package com.capgemini.staticversioning.config.impl;

import de.hybris.platform.servicelayer.config.impl.DefaultConfigurationService;

import java.util.Map.Entry;
import java.util.Properties;


/**
 * Configuration service setting.
 *
 * @author lyonscg
 *
 */
public class BuildAwareConfigurationService extends DefaultConfigurationService
{
    /**
     * the build properties.
     */
    private Properties buildProperties;

    /**
     * include additional properties.
     */
    @Override
    public void afterPropertiesSet() throws Exception
    {
        super.afterPropertiesSet();
        
        if (buildProperties != null)
        {
            // loop through the build properties and add to the configuration
            for (final Entry<Object, Object> entry : buildProperties.entrySet())
            {
                getConfiguration().addProperty(entry.getKey().toString(), entry.getValue());
            }
        }
    }

    /**
     * 
     * @param buildProperties
     *            - the build properties.
     */
    public void setBuildProperties(final Properties buildProperties)
    {
        this.buildProperties = buildProperties;
    }
}
