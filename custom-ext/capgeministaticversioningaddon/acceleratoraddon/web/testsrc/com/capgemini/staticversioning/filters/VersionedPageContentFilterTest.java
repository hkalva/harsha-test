/**
 *
 */
package com.capgemini.staticversioning.filters;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.regex.Pattern;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.web.util.ContentCachingResponseWrapper;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;


/**
 * Unit test for VersionedPageContentFilter.
 */
@UnitTest
public class VersionedPageContentFilterTest
{
    /**
     * The class to be tested.
     */
    private VersionedPageContentFilter versionedPageContentFilter;
    private VersionedPageContentFilter spyVersionedPageContentFilter;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private FilterChain filterChain;
    
    private ContentCachingResponseWrapper wrappedResponse;
    @Mock
    private ServletOutputStream servletOutputStream;
    @Mock
    private PrintWriter printWriter;
    @Mock
    private ConfigurationService configurationService;
    @Mock
    private Configuration configuration;

    private final String buildNumber = "123", replacementString = "/_ui/" + buildNumber + "/$2",
            regexPattern = "(/_ui/)(.+\\.(css|js))";
    private final String staticURL = "/_ui/addons/googletagmanager/desktop/common/css/googletagmanager.css";
    private final String alteredStaticURL = "/_ui/123/addons/googletagmanager/desktop/common/css/googletagmanager.css";
    private final String noMatchURL = "/addons/googletagmanager/desktop/common/css/googletagmanager.css";
    private final String staticVersionEnabledConfig = "lea.static.versioning.enabled";

    /**
     * Setting up.
     * 
     * @throws IOException
     *             - for testing.
     */
    @Before
    public void setUp() throws IOException
    {
        MockitoAnnotations.initMocks(this);
        versionedPageContentFilter = new VersionedPageContentFilter();
        wrappedResponse = new ContentCachingResponseWrapper(response);
        spyVersionedPageContentFilter = Mockito.spy(versionedPageContentFilter);
        //Mockito.doReturn(wrappedResponse).when(spyVersionedPageContentFilter).createResponseWrapper(response);
        Mockito.doReturn(printWriter).when(wrappedResponse).getWriter();
        Mockito.doReturn(MediaType.TEXT_HTML_VALUE).when(wrappedResponse).getContentType();
        Mockito.doReturn(servletOutputStream).when(response).getOutputStream();
        Mockito.doReturn(printWriter).when(response).getWriter();
        Mockito.doReturn("UTF-8").when(response).getCharacterEncoding();
        Mockito.doReturn(configuration).when(configurationService).getConfiguration();
        Mockito.doReturn(Boolean.TRUE).when(configuration).getBoolean(staticVersionEnabledConfig);
        Mockito.doReturn("UTF-8").when(configuration).getString("encoding.charset", Charset.defaultCharset().name());

        spyVersionedPageContentFilter.setReplacementString(replacementString);
        spyVersionedPageContentFilter.setStaticAssetRegexPatternString(regexPattern);
        spyVersionedPageContentFilter.setStaticAssertRegexPattern(Pattern.compile(regexPattern));
        spyVersionedPageContentFilter.setEnabled(StringUtils.isNotBlank(buildNumber));
        spyVersionedPageContentFilter.setConfigurationService(configurationService);
        spyVersionedPageContentFilter.setStaticVersionEnabledConfig(staticVersionEnabledConfig);
    }

    /**
     * Unit test for internal filter writer.
     * 
     * @throws IOException
     *             - for testing.
     * @throws ServletException
     *             - for testing.
     */
    @Test
    public void testDoFilterInternalWriter() throws IOException, ServletException
    {
        spyVersionedPageContentFilter.setBuildNumber(buildNumber);
        Mockito.doReturn(staticURL.getBytes()).when(wrappedResponse).getContentAsByteArray();
        spyVersionedPageContentFilter.doFilterInternal(request, response, filterChain);
        Mockito.verify(printWriter).write(alteredStaticURL);
    }

    /**
     * Unit test for internal filter output stream.
     * 
     * @throws IOException
     *             - for testing.
     * @throws ServletException
     *             - for testing.
     */
    @Test
    public void testDoFilterInternalOutputStream() throws IOException, ServletException
    {
        spyVersionedPageContentFilter.setBuildNumber(buildNumber);
        Mockito.doReturn(staticURL.getBytes()).when(wrappedResponse).getContentAsByteArray();
        Mockito.doThrow(IllegalStateException.class).when(printWriter).write(alteredStaticURL);
        spyVersionedPageContentFilter.doFilterInternal(request, response, filterChain);
        Mockito.verify(servletOutputStream).write(alteredStaticURL.getBytes());
    }

    /**
     * Unit test for internal filter if it there is no match.
     * 
     * @throws IOException
     *             - for testing.
     * @throws ServletException
     *             - for testing.
     */
    @Test
    public void testDoFilterInternalNoMatch() throws IOException, ServletException
    {
        spyVersionedPageContentFilter.setBuildNumber(buildNumber);
        Mockito.doReturn(noMatchURL.getBytes()).when(wrappedResponse).getContentAsByteArray();
        spyVersionedPageContentFilter.doFilterInternal(request, response, filterChain);
        Mockito.verify(printWriter).write(noMatchURL);
    }

    /**
     * Unit test for internal filter ig there is no build number.
     * 
     * @throws IOException
     *             - for testing.
     * @throws ServletException
     *             - for testing.
     */
    @Test
    public void testDoFilterInternalNoBuildNumber() throws IOException, ServletException
    {
        spyVersionedPageContentFilter.setEnabled(false);
        spyVersionedPageContentFilter.doFilterInternal(request, response, filterChain);
        Mockito.verify(filterChain).doFilter(request, response);
    }

    /**
     * Unit test for internal filter disabled config.
     * 
     * @throws IOException
     *             - for testing.
     * @throws ServletException
     *             - for testing.
     */
    @Test
    public void testDoFilterInternalDisabledConfig() throws IOException, ServletException
    {
        spyVersionedPageContentFilter.setEnabled(true);
        Mockito.doReturn(Boolean.FALSE).when(configuration).getBoolean(staticVersionEnabledConfig);
        spyVersionedPageContentFilter.doFilterInternal(request, response, filterChain);
        Mockito.verify(filterChain).doFilter(request, response);
    }

    /**
     * Unit test for internal filter non text/html response.
     * 
     * @throws IOException
     *             - for testing.
     * @throws ServletException
     *             - for testing.
     */
    @Test
    public void testDoFilterInternalNonTextResponse() throws IOException, ServletException
    {
        Mockito.doReturn(MediaType.ALL_VALUE).when(wrappedResponse).getContentType();
        spyVersionedPageContentFilter.setBuildNumber(buildNumber);
        spyVersionedPageContentFilter.doFilterInternal(request, response, filterChain);
        Mockito.verify(wrappedResponse).copyBodyToResponse();
    }
}
