/**
 *
 */
package com.capgemini.staticversioning.filters;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.MediaType;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingResponseWrapper;

import de.hybris.platform.servicelayer.config.ConfigurationService;


/**
 * Implementing version page content filter.
 *
 * @author lyonscg.
 */
public class VersionedPageContentFilter extends OncePerRequestFilter
{
    private static final Logger LOG = LogManager.getLogger(VersionedPageContentFilter.class);
    private Pattern staticAssertRegexPattern;
    private String buildNumber;
    private String replacementString;
    private String staticAssetRegexPatternString;
    private boolean isEnabled;
    private ConfigurationService configurationService;
    private String staticVersionEnabledConfig;

    @Override
    protected void initFilterBean() throws ServletException
    {
        super.initFilterBean();
        setStaticAssertRegexPattern(Pattern.compile(getStaticAssetRegexPatternString()));
        setEnabled(StringUtils.isNotBlank(getBuildNumber()));
    }

    @Override
    public void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
            final FilterChain filterChain) throws IOException, ServletException
    {
        final boolean isDebugEnabled = LOG.isDebugEnabled();

        if (isEnabled() && getConfigurationService().getConfiguration().getBoolean(getStaticVersionEnabledConfig()))
        {
            //use Spring's ContentCachingResponseWrapper to allow us to return the original response
            //final LeaContentCachingResponseWrapper wrappedResponse = createResponseWrapper(response);
            final ContentCachingResponseWrapper wrappedResponse = new ContentCachingResponseWrapper(response);
            filterChain.doFilter(request, wrappedResponse);

            if (StringUtils.containsIgnoreCase(wrappedResponse.getContentType(), MediaType.TEXT_HTML_VALUE))
            {
                //if content is html, check if content needs to be modified
                long startTime = 0;

                if (isDebugEnabled)
                {
                    startTime = System.currentTimeMillis();
                }

                final byte[] bytes = wrappedResponse.getContentAsByteArray();
                final Charset charset = getCharset(wrappedResponse);
                final String actualResponse = new String(bytes, charset);
                final Matcher matcher = getStaticAssertRegexPattern().matcher(actualResponse);

                if (matcher.find())
                {
                    final String modifiedResponse = matcher.replaceAll(getReplacementString());

                    if (isDebugEnabled)
                    {
                        LOG.debug("Match(es) found. Content modified.");
                    }

                    writeToActualResponse(response, modifiedResponse, charset);
                }
                else
                {
                    if (isDebugEnabled)
                    {
                        LOG.debug("No match found. Content served as is.");
                    }

                    writeToActualResponse(response, actualResponse, charset);
                }

                if (isDebugEnabled)
                {
                    LOG.debug("Processing time: " + (System.currentTimeMillis() - startTime) + " msecs");
                }
            }
            else
            {
                // if content is non-html, just return the response as is
                if (isDebugEnabled)
                {
                    LOG.debug("Non-html content. Content served as is.");
                }

                wrappedResponse.copyBodyToResponse();
            }
        }
        else
        {
            if (isDebugEnabled)
            {
                LOG.debug("Static versioning is either disabled or there's no build number. Content served as is.");
            }

            filterChain.doFilter(request, response);
        }
    }

    protected void writeToActualResponse(final HttpServletResponse response, final String content, final Charset charset)
            throws IOException
    {
        response.setContentLength(content.getBytes(charset).length);

        try
        {
            response.getWriter().write(content);
        }
        catch (final IllegalStateException e)
        {
            LOG.warn("Writer already used. Trying to write with outputStream", e);
            response.getOutputStream().write(content.getBytes(charset));
        }
    }

    /**
     * @return the staticAssetRegexPatternString
     */
    protected String getStaticAssetRegexPatternString()
    {
        return staticAssetRegexPatternString;
    }

    /**
     * @param staticAssetRegexPatternString
     *            the staticAssetRegexPatternString to set
     */
    @Required
    public void setStaticAssetRegexPatternString(final String staticAssetRegexPatternString)
    {
        this.staticAssetRegexPatternString = staticAssetRegexPatternString;
    }

    /**
     * @param staticAssertRegexPattern
     *            the staticAssertRegexPattern to set
     */
    public void setStaticAssertRegexPattern(final Pattern staticAssertRegexPattern)
    {
        this.staticAssertRegexPattern = staticAssertRegexPattern;
    }

    /**
     * @param buildNumber
     *            the buildNumber to set
     */
    public void setBuildNumber(final String buildNumber)
    {
        this.buildNumber = buildNumber;
    }

    /**
     * @param replacementString
     *            the replacementString to set
     */
    public void setReplacementString(final String replacementString)
    {
        this.replacementString = replacementString;
    }

    /**
     * @return the staticAssertRegexPattern
     */
    protected Pattern getStaticAssertRegexPattern()
    {
        return staticAssertRegexPattern;
    }

    /**
     * @return the buildNumber
     */
    protected String getBuildNumber()
    {
        return buildNumber;
    }

    /**
     * @return the replacementString
     */
    protected String getReplacementString()
    {
        return replacementString;
    }

    /**
     * @param isEnabled
     *            the isEnabled to set
     */
    public void setEnabled(final boolean isEnabled)
    {
        this.isEnabled = isEnabled;
    }

    /**
     * @return the isEnabled
     */
    protected boolean isEnabled()
    {
        return isEnabled;
    }

    /**
     * @return the configurationService
     */
    protected ConfigurationService getConfigurationService()
    {
        return configurationService;
    }

    /**
     * @param configurationService
     *            the configurationService to set
     */
    @Required
    public void setConfigurationService(final ConfigurationService configurationService)
    {
        this.configurationService = configurationService;
    }

    /**
     * @return the staticVersionEnabledConfig
     */
    protected String getStaticVersionEnabledConfig()
    {
        return staticVersionEnabledConfig;
    }

    /**
     * @param staticVersionEnabledConfig
     *            the staticVersionEnabledConfig to set
     */
    @Required
    public void setStaticVersionEnabledConfig(final String staticVersionEnabledConfig)
    {
        this.staticVersionEnabledConfig = staticVersionEnabledConfig;
    }

    private Charset getCharset(final HttpServletResponse response)
    {
        //return Charset based on the response's character encoding.
        //if null, use the character set from the properties file.
        //if still null, use the default
        String characterEncoding = response.getCharacterEncoding();

        if (StringUtils.isBlank(characterEncoding) && (getConfigurationService() != null)
                && (getConfigurationService().getConfiguration() != null))
        {
            characterEncoding = getConfigurationService().getConfiguration().getString("encoding.charset",
                    Charset.defaultCharset().name());
        }

        return Charset.forName(characterEncoding);
    }

}
