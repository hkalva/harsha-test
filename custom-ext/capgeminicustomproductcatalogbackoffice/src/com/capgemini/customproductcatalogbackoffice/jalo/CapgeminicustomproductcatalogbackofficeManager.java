package com.capgemini.customproductcatalogbackoffice.jalo;

import com.capgemini.customproductcatalogbackoffice.constants.CapgeminicustomproductcatalogbackofficeConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

@SuppressWarnings("PMD")
public class CapgeminicustomproductcatalogbackofficeManager extends GeneratedCapgeminicustomproductcatalogbackofficeManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( CapgeminicustomproductcatalogbackofficeManager.class.getName() );
	
	public static final CapgeminicustomproductcatalogbackofficeManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (CapgeminicustomproductcatalogbackofficeManager) em.getExtension(CapgeminicustomproductcatalogbackofficeConstants.EXTENSIONNAME);
	}
	
}
