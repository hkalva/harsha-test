/*
* [y] hybris Platform
*
* Copyright (c) 2000-2016 SAP SE
* All rights reserved.
*
* This software is the confidential and proprietary information of SAP
* Hybris ("Confidential Information"). You shall not disclose such
* Confidential Information and shall use it only in accordance with the
* terms of the license agreement you entered into with SAP Hybris.
*/
package com.capgemini.services.promotionengine.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.customersupport.CommerceCustomerSupportService;
import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.promotionengineservices.model.RuleBasedPromotionModel;
import de.hybris.platform.promotionengineservices.promotionengine.impl.DefaultPromotionEngineService;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.ruleengine.dao.RulesModuleDao;
import de.hybris.platform.ruleengine.enums.RuleType;
import de.hybris.platform.ruleengine.model.AbstractRuleEngineRuleModel;
import de.hybris.platform.ruleengine.model.AbstractRulesModuleModel;
import de.hybris.platform.ruleengineservices.RuleEngineServiceException;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerContextFactory;
import de.hybris.platform.ruleengineservices.compiler.RuleIrVariablesGeneratorFactory;
import de.hybris.platform.ruleengineservices.compiler.impl.DefaultRuleCompilerContext;
import de.hybris.platform.ruleengineservices.compiler.impl.DefaultRuleCompilerContextFactory;
import de.hybris.platform.ruleengineservices.maintenance.RuleCompilationContextProvider;
import de.hybris.platform.ruleengineservices.rule.dao.RuleDao;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleParameterData;
import de.hybris.platform.ruleengineservices.rule.services.RuleConditionsService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
* Promotion Engine Service to ensure that ASM related Promotions are included in the list of Promotions for a product,
* only if an active ASM session is present.
*/
public class LyonscgPromotionEngineService extends DefaultPromotionEngineService
{
    private static final Logger LOG = LoggerFactory.getLogger(LyonscgPromotionEngineService.class);

    private static final String ASM_CONDITION_ID = "y_customer_support";

    @Resource
    private RuleCompilerContextFactory<DefaultRuleCompilerContext> ruleCompilerContextFactory;
    @Resource
    private RuleConditionsService ruleConditionsService;
    @Resource
    private CommerceCustomerSupportService commerceCustomerSupportService;
    @Resource
    private RuleDao ruleDao;
    @Resource
    private RuleIrVariablesGeneratorFactory ruleIrVariablesGeneratorFactory;
    @Resource
    private RuleCompilationContextProvider ruleCompilationContextProvider;
    @Resource
    private RulesModuleDao rulesModuleDao;

    /**
     * Override the method to ensure that ASM related Promotions are included in the list of Promotions for a product,
     * only if an active ASM session is present.
     */
    @Override
    protected List<RuleBasedPromotionModel> getPromotionsForProduct(final Collection<PromotionGroupModel> promotionGroups,
            final ProductModel product)
    {
        if (this.getConfigurationService().getConfiguration()
                .getBoolean("promotionengineservices.getpromotionsforproduct.disable"))
        {
            LOG.info("Promotions for product are disabled. If you want to enable them, please change the property {} to false."
                    + "promotionengineservices.getpromotionsforproduct.disable");
            return Collections.emptyList();
        }
        else
        {
            final Set<String> allCategoryCodes = this.getCategoryCodesForProduct(product);
            final List<RuleBasedPromotionModel> promotions = this.getPromotionSourceRuleDao().findPromotions(promotionGroups,
                    product.getCode(), allCategoryCodes);

            LOG.debug("Promotion List Size:b4 : {}", promotions.size());
            if (!getCommerceCustomerSupportService().isCustomerSupportAgentActive())
            {
                final List<RuleBasedPromotionModel> promoListWithoutASM = removeASMPromotions(promotions);
                return sortPromotions(promoListWithoutASM);
            }
            LOG.debug("Promotion List Size: A4 : {}", promotions.size());
            return sortPromotions(promotions);
        }
    }

    /**
     * Iterate through the list of promotions, check if it has a ASM condition which is set to true. If yes, ignore it
     * and If not, create a new list of promotions.
     *
     */
    private List<RuleBasedPromotionModel> removeASMPromotions(final List<RuleBasedPromotionModel> promotions)
    {
        final List<RuleBasedPromotionModel> promoListWithoutASM = new ArrayList<>();
        //Filter and create a new list of promotions with out ASM related promotions
        for (final RuleBasedPromotionModel promotionRule : promotions)
        {
            final AbstractRuleEngineRuleModel rule = promotionRule.getRule();
            LOG.debug("rule.getUuid() : {}", rule.getUuid());
            final PromotionSourceRuleModel promotionSourceRule = (PromotionSourceRuleModel) rule.getSourceRule();
            LOG.debug("promotionRule1.getConditions() : {}", promotionSourceRule.getConditions());
            final List<AbstractRulesModuleModel> rulesModules = rulesModuleDao
                    .findActiveRulesModulesByRuleType(RuleType.PROMOTION);

            if (CollectionUtils.isEmpty(rulesModules))
            {
                continue;
            }

            if (!isASMPromotion(promotionSourceRule))
            {
                promoListWithoutASM.add(promotionRule);
            }
        }
        LOG.debug("Promotion List Size: {}", promoListWithoutASM.size());
        return promoListWithoutASM;
    }

    private boolean isASMPromotion(final PromotionSourceRuleModel promotionSourceRule)
    {
        boolean result = false;
        ruleCompilerContextFactory.createContext(ruleCompilationContextProvider.getRuleCompilationContext(), promotionSourceRule,
                null, getRuleIrVariablesGeneratorFactory().createVariablesGenerator());
        try
        {
            final List<RuleConditionData> conditionData = ruleConditionsService.convertConditionsFromString(
                    promotionSourceRule.getConditions(), ((DefaultRuleCompilerContextFactory) ruleCompilerContextFactory)
                            .getRuleConditionsRegistry().getConditionDefinitionsForRuleTypeAsMap(promotionSourceRule.getClass()));
            for (final RuleConditionData condition : conditionData)
            {
                LOG.debug("condition.getDefinitionId() : {}", condition.getDefinitionId());
                LOG.debug("condition.getParameters() : {}", condition.getParameters());

                RuleParameterData assistedServiceSessionActiveParameter = null;

                if (ASM_CONDITION_ID.equalsIgnoreCase(condition.getDefinitionId()))
                {
                    assistedServiceSessionActiveParameter = condition.getParameters().get("value");
                }

                if (assistedServiceSessionActiveParameter != null
                        && assistedServiceSessionActiveParameter.getValue().equals(Boolean.TRUE))
                {
                    LOG.debug("assistedServiceSessionActiveParameter :" + assistedServiceSessionActiveParameter.getValue() + " :"
                            + assistedServiceSessionActiveParameter.getType());
                    LOG.debug("REMOVE THIS PROMO SINCE THERE IS NO ASM active session");
                    result = true;
                    break;
                }
            }
        }
        catch (final RuleEngineServiceException e)
        {
            LOG.error("Error on isASMPromotion", e);
        }

        return result;
    }

    /**
     * Sort the promotions for rendering.
     */
    protected List<RuleBasedPromotionModel> sortPromotions(final List<RuleBasedPromotionModel> promotions)
    {
        final List<RuleBasedPromotionModel> sortedPromotions = new ArrayList<>(promotions);
        Collections.sort(sortedPromotions, new RuleBasedPromotionModelComparator());
        return sortedPromotions;
    }

    /**
     * @return the commerceCustomerSupportService
     */
    private CommerceCustomerSupportService getCommerceCustomerSupportService()
    {
        return commerceCustomerSupportService;
    }

    /**
     * @param commerceCustomerSupportService
     *            the commerceCustomerSupportService to set
     */
    public void setCommerceCustomerSupportService(final CommerceCustomerSupportService commerceCustomerSupportService)
    {
        this.commerceCustomerSupportService = commerceCustomerSupportService;
    }

    /**
     * @return the ruleIrVariablesGeneratorFactory
     */
    private RuleIrVariablesGeneratorFactory getRuleIrVariablesGeneratorFactory()
    {
        return ruleIrVariablesGeneratorFactory;
    }

    /**
     * @param ruleIrVariablesGeneratorFactory
     *            the ruleIrVariablesGeneratorFactory to set
     */
    public void setRuleIrVariablesGeneratorFactory(final RuleIrVariablesGeneratorFactory ruleIrVariablesGeneratorFactory)
    {
        this.ruleIrVariablesGeneratorFactory = ruleIrVariablesGeneratorFactory;
    }

    /**
     * @param ruleDao
     *            the ruleDao to set
     */
    public void setRuleDao(final RuleDao ruleDao)
    {
        this.ruleDao = ruleDao;
    }

    /**
     * @param ruleCompilerContextFactory
     *            the ruleCompilerContextFactory to set
     */
    public void setRuleCompilerContextFactory(
            final RuleCompilerContextFactory<DefaultRuleCompilerContext> ruleCompilerContextFactory)
    {
        this.ruleCompilerContextFactory = ruleCompilerContextFactory;
    }

    /**
     * @param ruleConditionsService
     *            the ruleConditionsService to set
     */
    public void setRuleConditionsService(final RuleConditionsService ruleConditionsService)
    {
        this.ruleConditionsService = ruleConditionsService;
    }

    /**
     *
     * @param ruleCompilationContextProvider
     *            - the rule compilation context provider.
     */
    public void setRuleCompilationContextProvider(final RuleCompilationContextProvider ruleCompilationContextProvider)
    {
        this.ruleCompilationContextProvider = ruleCompilationContextProvider;
    }

    /**
     *
     * @param rulesModuleDao
     *            - the rules module dao.
     */
    public void setRulesModuleDao(final RulesModuleDao rulesModuleDao)
    {
        this.rulesModuleDao = rulesModuleDao;
    }

    private static final class RuleBasedPromotionModelComparator implements Comparator<RuleBasedPromotionModel>, Serializable
    {

        /**
         * default serial version uid.
         */
        private static final long serialVersionUID = 1L;

        @Override
        public int compare(final RuleBasedPromotionModel o1, final RuleBasedPromotionModel o2)
        {
            return o2.getPriority().compareTo(o1.getPriority());
        }

    }
}

