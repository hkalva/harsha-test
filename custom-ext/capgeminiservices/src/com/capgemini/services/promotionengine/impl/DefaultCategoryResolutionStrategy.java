/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.services.promotionengine.impl;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.promotionengineservices.promotionengine.PromotionMessageParameterResolutionStrategy;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.ruleengineservices.rule.data.RuleParameterData;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

/**
 * DefaultCategoryResolutionStrategy resolves the given {@link RuleParameterData#getValue()},
 * into a category code, looks up the category via and,
 *  invokes {@link #getCategoryRepresentation(CategoryModel)} to display the category.
 */
public class DefaultCategoryResolutionStrategy implements
		PromotionMessageParameterResolutionStrategy {

	private CategoryService categoryService;

    private static final Logger LOG = LoggerFactory.getLogger(DefaultCategoryResolutionStrategy.class);


    @Override
    public String getValue(final RuleParameterData data, final PromotionResultModel promotionResult, final Locale locale)
    {
        final List<String> categoryList = (List<String>) data.getValue();
        StringBuilder categoryNames = new StringBuilder();
        for (String categoryCode : categoryList)
        {
            final CategoryModel category = getCategory(categoryCode);
            if (category == null)
            {
                // if not resolved, just return the input
                categoryNames.append(categoryCode + " ,");
            }
            else
            {
                categoryNames.append(getCategoryRepresentation(category) + " ,");
            }
        }
        return categoryNames.toString().substring(0, categoryNames.length() - 2);
    }

    /**
     * retrieves a Category based on the given {@code categoryCode}. This method uses the
     * {@link CategoryService#getCategoryForCode(String)} method.
     *
     * @param categoryCode
     *            the category's code
     * @return the category or null if none (or multiple) are found.
     */
    protected CategoryModel getCategory(final String categoryCode)
    {
        if (categoryCode != null)
        {
            try
            {
                return getCategoryService().getCategoryForCode(categoryCode);
            }
            catch (UnknownIdentifierException | AmbiguousIdentifierException e)
            {
                LOG.error("cannot resolve category code: " + categoryCode + " to a category.", e);
                return null;
            }
        }
        else
        {
            return null;
        }
    }

    /**
     * returns the {@link CategoryModel#getName()} for the given {@code category}.
     *
     * @param category
     *            the category
     * @return the name of the category
     */
    protected String getCategoryRepresentation(final CategoryModel category)
    {
        return category.getName();
    }

    protected CategoryService getCategoryService()
    {
        return categoryService;
    }

    @Required
    public void setCategoryService(CategoryService categoryService)
    {
        this.categoryService = categoryService;
    }


}
