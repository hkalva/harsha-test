/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.services;

import java.util.Map;

import de.hybris.platform.commons.model.renderer.RendererTemplateModel;

public abstract class AbstractExportConfig
{
    

    /**
     * Custom or default query. true for custom query.
     */
    private Boolean isQueryEnabled;
    /**
     * Custom query to be used if isQueryEnabled is true.
     */
    private String query;
    /**
     * Custom query parameters to be used for query.
     */
    private Map<String, String> queryParams;
    
    /**
     * RendererTemplate when using velocity based export.
     */
    private RendererTemplateModel rendererTemplate;
    /**
     * File export path for orders.
     */
    private String fileExportPath;
    /**
     * File name extension for orders.
     */
    private String fileNameExt;
    /**
     * Character encoding for orders.
     */
    private String fileEncoding;
    
    /**
     * @return the isQueryEnabled
     */
    public Boolean getIsQueryEnabled()
    {
        return isQueryEnabled;
    }

    /**
     * @param isQueryEnabled
     *           the isQueryEnabled to set
     */
    public void setIsQueryEnabled(final Boolean isQueryEnabled)
    {
        this.isQueryEnabled = isQueryEnabled;
    }

    /**
     * @return the query
     */
    public String getQuery()
    {
        return query;
    }

    /**
     * @param query
     *           the query to set
     */
    public void setQuery(final String query)
    {
        this.query = query;
    }

    /**
     * @return the queryParams
     */
    public Map<String, String> getQueryParams()
    {
        return queryParams;
    }

    /**
     * @param queryParams
     *           the queryParams to set
     */
    public void setQueryParams(final Map<String, String> queryParams)
    {
        this.queryParams = queryParams;
    }
    
    /**
     * @return the rendererTemplate
     */
    public RendererTemplateModel getRendererTemplate()
    {
        return rendererTemplate;
    }

    /**
     * @param rendererTemplate
     *           the rendererTemplate to set
     */
    public void setRendererTemplate(final RendererTemplateModel rendererTemplate)
    {
        this.rendererTemplate = rendererTemplate;
    }

    /**
     * @return the fileExportPath
     */
    public String getFileExportPath()
    {
        return fileExportPath;
    }

    /**
     * @param fileExportPath
     *           the fileExportPath to set
     */
    public void setFileExportPath(final String fileExportPath)
    {
        this.fileExportPath = fileExportPath;
    }

    /**
     * @return the fileNameExt
     */
    public String getFileNameExt()
    {
        return fileNameExt;
    }

    /**
     * @param fileNameExt
     *           the fileNameExt to set
     */
    public void setFileNameExt(final String fileNameExt)
    {
        this.fileNameExt = fileNameExt;
    }

    /**
     * @return the fileEncoding
     */
    public String getFileEncoding()
    {
        return fileEncoding;
    }

    /**
     * @param fileEncoding
     *           the fileEncoding to set
     */
    public void setFileEncoding(final String fileEncoding)
    {
        this.fileEncoding = fileEncoding;
    }




}
