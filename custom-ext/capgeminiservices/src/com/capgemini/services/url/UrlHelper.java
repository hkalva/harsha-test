/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.services.url;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * Implementation of URL helper class. It is parsing down the string into valid and well formated URL.
 *
 * @author lyonscg.
 */
public class UrlHelper
{
	private static final Logger LOG = LoggerFactory.getLogger(UrlHelper.class);
	
	private UrlHelper(){
		// Utility classes should not have a public or default constructor.
		// Define a private class to avoid checkstyle issue
	}

	/**
	 * Encode string with UTF8 encoding and then convert a string into a URL safe version of the string. Only upper and
	 * lower case letters and numbers are retained. '+', single and double quotes are removed all together. All other
	 * characters are replaced by a hyphen (-).
	 * 
	 * @param text
	 *           the text to sanitize
	 * @return the safe version of the text
	 */
	public static String urlSafe(final String text)
	{
		LOG.debug("Input text: {}", text);
		
		if (text == null || text.isEmpty())
		{
			LOG.debug("Input text was blank returning empty string");
			return StringUtils.EMPTY;
		}

        String safeString = text.replaceAll("[\"\\+'&]+", StringUtils.EMPTY);
        // Cleanup the text
        LOG.debug("safeString before replaceAll: {}", safeString);
        safeString = safeString.replaceAll("[^A-Za-z0-9\\/\\-]+", "-");

        // since we previously replaced some special characters with the empty string it is possible to 
        // have a series of dashes.  Now we want to get down to having only 1 between words
        safeString = safeString.replaceAll("[-]{2,}", "-");
        LOG.debug("Returning text: {}", safeString);
        String encodedText;
        
        try
        {
            encodedText = URLEncoder.encode(safeString, "utf-8");
        }
        catch (final UnsupportedEncodingException encodingException)
        {
            encodedText = safeString;
            LOG.warn("Using unencoded text.  ({}) failed encoding with: {}", safeString, encodingException.getMessage());
            LOG.error("Error in urlSafe", encodingException);
        }
        
        // Replace [%2F%2F...]+ to /
        //'/' is replaced by %2F during encoding. So changing it back to '/'.
        return encodedText.replaceAll("(%2F)+", "/");
	}

    /**
     * Replaces "/" with "-" .
     * 
     * @param name
     *            - String to escape.
     * @return - String with all "/" replaced by "-" .
     */
    public static String replaceSlash(String name)
    {
        if (name == null)
        {
            return null;
        }
        
        return name.replaceAll("/", "-");
    }

    /**
     * Replaces "//*" with "/" .
     * 
     * @param url
     *            - url to replace.
     * @return - String with all "/{2,}" replaced by "/" .
     */
    public static String removeDoubleSlashes(String url)
    {
        if (StringUtils.isNotBlank(url) && url.contains("//"))
        {
            return url.replaceAll("/{2,}", "/");
        }
        
        return url;
    }
}
