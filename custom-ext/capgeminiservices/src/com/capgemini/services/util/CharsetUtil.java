/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.services.util;

import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author lyonscg.
 *
 */
public class CharsetUtil
{
    /**
     * the logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(CharsetUtil.class);
    
    /**
     * constant for default character set.
     */
    private static final String UTF_8 = "UTF-8";

    /**
     * constant for configuration service bean.
     */
    private static final String CONFIGURATION_SERVICE = "configurationService";
    
    /**
     * constant for encoding charset property.
     */
    private static final String ENCODING_CHARSET = "encoding.charset";
    
    /**
     * default private constructor.
     */
    private CharsetUtil()
    {
        // Util class, no instantiation.
    }

    /**
     * Get the charset to be used for encoding from local.properties file. If the specified charset is illegal or not
     * supported, use "UTF-8" for encoding. If "UTF-8" is not supported, return system default charset
     *
     * @return Charset
     */
    public static Charset getCharset()
    {
        return getCharset(UTF_8);
    }

    /**
     * Return charset for name specified in local.properties file. If the configuration cannot be found or is illegal or
     * not supported, try to return charset for given String defaultCharsetName. If given parameter is illegal or not
     * supported, system default charset will be returned
     *
     * @param defaultCharsetName
     *           Default charset to use if the charset specified in local.properties is illegal or not supported. If the
     *           given defaultCharsetName is illegal or not supported, return system default charset
     *
     * @return Charset
     */
    public static Charset getCharset(final String defaultCharsetName)
    {
        final ConfigurationService configurationService = (ConfigurationService) Registry.getApplicationContext().getBean(CONFIGURATION_SERVICE);
        String charsetName;
        
        if (configurationService == null || configurationService.getConfiguration() == null)
        {
            LOG.warn("Cannot find bean with name {} or Configuration is null, use {} for encoding.", ConfigurationService.class.getName(), defaultCharsetName);
            charsetName = defaultCharsetName;
        }
        else
        {
            charsetName = configurationService.getConfiguration().getString(ENCODING_CHARSET, defaultCharsetName);
        }

        return getDefaultCharset(charsetName);
    }
    
    /**
     *
     * Return the default charset to use, it will return the charset for given name if possible, otherwise, it will
     * return the system default charset
     *
     * @param defaultCharsetName
     *           Default charset name to use before using system default charset. Return Charset for given name, if the
     *           given charset name is illegal or not supported, use system default charset
     * @return Charset
     */
    protected static Charset getDefaultCharset(final String defaultCharsetName)
    {
        Charset charset;
        
        try
        {
            if (Charset.isSupported(defaultCharsetName))
            {
                charset = Charset.forName(defaultCharsetName);
            }
            else
            {
                charset = Charset.defaultCharset();
                LOG.warn("Specified default charsetname {} is not supported, use system default charset {} instead", defaultCharsetName, charset.displayName());
            }
        }
        catch (final IllegalArgumentException e)
        {
            charset = Charset.defaultCharset();
            LOG.warn("Illegal default charsetname {}, use system default charset {} instead", defaultCharsetName, charset.displayName());
            LOG.error("Error in getDefaultCharset", e);
        }
        
        return charset;
    }

}
