/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.services.rulesengine.definitions.conditions;

import de.hybris.platform.ruleengineservices.compiler.RuleCompilerContext;
import de.hybris.platform.ruleengineservices.compiler.RuleConditionTranslator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerException;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeOperator;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionDefinitionData;

/**
 * This class prevents products that are part of bundles to be excluded from applying the promotion.
 */
public class LyonscgBundlePromoConditionTranslator implements RuleConditionTranslator
{
    public static final String IS_PART_OF_BUNDLE = "isPartOfBundle";

    /**
     * This method prevents products that are part of bundles to be excluded from applying the promotion.
     * @param ruleCompilerContext
     * @param ruleConditionData
     * @param ruleConditionDefinitionData
     * @return irAttributeCondition
     * @throws RuleCompilerException
     */
    @Override
    public RuleIrCondition translate(RuleCompilerContext ruleCompilerContext, RuleConditionData ruleConditionData, RuleConditionDefinitionData ruleConditionDefinitionData) throws RuleCompilerException {
        String leaOrderEntryRaoVariable = ruleCompilerContext.generateVariable(OrderEntryRAO.class);
        RuleIrAttributeCondition irAttributeCondition = new RuleIrAttributeCondition();
        irAttributeCondition.setVariable(leaOrderEntryRaoVariable);
        irAttributeCondition.setAttribute(IS_PART_OF_BUNDLE);
        irAttributeCondition.setOperator(RuleIrAttributeOperator.EQUAL);
        irAttributeCondition.setValue(Boolean.FALSE);
        return irAttributeCondition;
    }
}
