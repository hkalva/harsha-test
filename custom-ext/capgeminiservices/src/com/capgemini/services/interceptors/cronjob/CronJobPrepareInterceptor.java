/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.services.interceptors.cronjob;

import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;


/**
 * Prepare Interceptor for CronJobs to set default values.
 */
public class CronJobPrepareInterceptor extends AbstractCronJobInterceptor implements PrepareInterceptor<CronJobModel>
{

    /**
     * On Prepare method for CronJob. This method populates NodeID, SessionCurrency, DefaultLanguage if it is not set.
     *
     * @param cronJob
     *            - The CronJob that is prepared.
     * @param interceptorContext
     *            - Interceptor's context.
     * @throws InterceptorException
     */
    @Override
    public void onPrepare(CronJobModel cronJob, InterceptorContext interceptorContext) throws InterceptorException
    {
        setValues(cronJob);
    }

}
