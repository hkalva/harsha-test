/*
* Copyright (c) 2019 Capgemini. All rights reserved.
* 
* This software is the confidential and proprietary information of Capgemini
* ("Confidential Information"). You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the
* license agreement you entered into with Capgemini.
*/

package com.capgemini.services.interceptors.cronjob;

import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;


/**
 * Load the common values of Interceptor to the corresponding CronJobs to set default values.
 */

public abstract class AbstractCronJobInterceptor
{

    /**
     * the default currency iso.
     */
    private String defaultCurrencyIso;

    /**
     * the default language iso.
     */
    private String defaultLanguageIso;

    /**
     * the default node id.
     */
    private Integer defaultNodeId;

    /**
     * the common i18n service.
     */
    private CommonI18NService commonI18NService;

    /**
     * the default currency.
     */
    private CurrencyModel defaultCurrency;

    /**
     * the default language.
     */
    private LanguageModel defaultLanguage;

    protected Integer getDefaultNodeId()
    {
        return defaultNodeId;
    }

    @Required
    public void setDefaultNodeId(Integer defaultNodeId)
    {
        this.defaultNodeId = defaultNodeId;
    }

    protected String getDefaultCurrencyIso()
    {
        return defaultCurrencyIso;
    }

    @Required
    public void setDefaultCurrencyIso(String defaultCurrencyIso)
    {
        this.defaultCurrencyIso = defaultCurrencyIso;
    }

    protected String getDefaultLanguageIso()
    {
        return defaultLanguageIso;
    }

    @Required
    public void setDefaultLanguageIso(String defaultLanguageIso)
    {
        this.defaultLanguageIso = defaultLanguageIso;
    }

    protected CommonI18NService getCommonI18NService()
    {
        return commonI18NService;
    }

    @Required
    public void setCommonI18NService(CommonI18NService commonI18NService)
    {
        this.commonI18NService = commonI18NService;
    }

    protected CurrencyModel getDefaultCurrency()
    {
        return defaultCurrency;
    }

    public void setDefaultCurrency(CurrencyModel defaultCurrency)
    {
        this.defaultCurrency = defaultCurrency;
    }

    protected LanguageModel getDefaultLanguage()
    {
        return defaultLanguage;
    }

    public void setDefaultLanguage(LanguageModel defaultLanguage)
    {
        this.defaultLanguage = defaultLanguage;
    }

    public void setValues(CronJobModel cronJob)
    {
        if (null == cronJob.getNodeID())
        {
            cronJob.setNodeID(getDefaultNodeId());
        }

        if (null == cronJob.getSessionCurrency())
        {
            if (null == getDefaultCurrency())
            {
                setDefaultCurrency(getCommonI18NService().getCurrency(getDefaultCurrencyIso()));
            }

            cronJob.setSessionCurrency(getDefaultCurrency());
        }

        if (null == cronJob.getSessionLanguage())
        {
            if (null == getDefaultLanguage())
            {
                setDefaultLanguage(getCommonI18NService().getLanguage(getDefaultLanguageIso()));
            }

            cronJob.setSessionLanguage(getDefaultLanguage());
        }
    }

}
