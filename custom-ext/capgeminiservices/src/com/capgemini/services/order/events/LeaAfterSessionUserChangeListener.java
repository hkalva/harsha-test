/**
 *
 */
package com.capgemini.services.order.events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.event.events.AfterSessionUserChangeEvent;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.user.UserService;


/**
 * Overriding the AfterSessionUserChangeListener, to invoke the b2bCommerce's AfterSessionUserChangeListener to set the
 * session.branch attribute.
 */
public class LeaAfterSessionUserChangeListener extends AbstractEventListener<AfterSessionUserChangeEvent>
{
    /**
     * the logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(LeaAfterSessionUserChangeListener.class);

    /*
     * (non-Javadoc)
     *
     * @see
     * de.hybris.platform.servicelayer.event.impl.AbstractEventListener#onEvent(de.hybris.platform.servicelayer.event
     * .events.AbstractEvent)
     */
    @Override
    protected void onEvent(final AfterSessionUserChangeEvent event)
    {
        getB2bUnituserChangeListener().onApplicationEvent(event);
        if (LOG.isDebugEnabled())
        {
            LOG.debug("LeaAfterSessionUserChangeListener received.");
        }
        final UserModel user = getUserService().getCurrentUser();
        getCartService().changeCurrentCartUser(user);

    }

    protected CartService getCartService()
    {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getCartService().");
    }

    protected UserService getUserService()
    {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getUserService().");
    }

    /**
     * Lookup method for B2bUnituserChangeListener.
     *
     * @return - B2bUnituserChangeListener.
     */
    protected AbstractEventListener<AfterSessionUserChangeEvent> getB2bUnituserChangeListener()
    {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getB2bUnituserChangeListener().");
    }

}
