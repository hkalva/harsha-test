/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.services.order.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartRestorationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;


/**
 * This class is to prevent the out of the box implementation of setting the cart currency to session.
 */
public class LeaCommerceCartRestorationStrategy extends DefaultCommerceCartRestorationStrategy
{
    private static final Logger LOG = LoggerFactory.getLogger(LeaCommerceCartRestorationStrategy.class);

    /**
     * This method prevents the out of the box implementation of setting the cart currency to session.
     *
     * @param-CommerceCartParameter to restore cart.
     *
     * @return-Returns CommerceCartRestoration data.
     */
    @Override
    public CommerceCartRestoration restoreCart(CommerceCartParameter parameter) throws CommerceCartRestorationException
    {
        CartModel cartModel = parameter.getCart();
        final CommerceCartRestoration restoration = new CommerceCartRestoration();
        List<CommerceCartModification> modifications = new ArrayList<>();
        if (cartModel != null)
        {
            if (getBaseSiteService().getCurrentBaseSite().equals(cartModel.getSite()))
            {
                LOG.debug("Restoring from cart {}.", cartModel.getCode());
                
                if (isCartInValidityPeriod(cartModel))
                {
                    cartModel.setCalculated(Boolean.FALSE);
                    
                    cartModel= verifyAndClearPaymentTransactionsOnCart(cartModel);

                    cartModel=recalculateCartModel(parameter,cartModel);
                    
                    LOG.debug("Cart {} was found to be valid and was restored to the session.", cartModel.getCode());
                }
                else
                {
                    modifications= addAllCommerceCart(parameter,modifications);
                }
                //The only difference from DefaultCommerceCartRestorationStrategy
                //is the removal of the call to commerceCommonI18NService's 
                //setCurrentCurrency method
                getCommerceCartCalculationStrategy().calculateCart(parameter);
            }
            else
            {
                LOG.warn(String.format("Current Site %s does not equal to cart %s Site %s",
                        getBaseSiteService().getCurrentBaseSite(), cartModel, cartModel.getSite()));
            }
        }
        restoration.setModifications(modifications);
        return restoration;
    }
    /**
     * 
     * @param parameter
     * @param modifications 
     * @return 
     * @throws CommerceCartRestorationException 
     */
   private List<CommerceCartModification> addAllCommerceCart(CommerceCartParameter parameter, List<CommerceCartModification> modifications) throws CommerceCartRestorationException
    {
       try
       {
           modifications.addAll(rebuildSessionCart(parameter));
       }
       catch (final CommerceCartModificationException e)
       {
           throw new CommerceCartRestorationException(e.getMessage(), e);
       }
    return modifications;
        
    }
/**
    *  
    * @param parameter
    * @param cartModel
    * @return
    */
    private CartModel recalculateCartModel(CommerceCartParameter parameter, CartModel cartModel)
    {
        try
        {
            getCommerceCartCalculationStrategy().recalculateCart(parameter);
        }
        catch (final IllegalStateException ex)
        {
            LOG.error("Failed to recalculate order [" + cartModel.getCode() + "]", ex);
        }
        getCartService().setSessionCart(cartModel);
        return cartModel;
    }
/**
 * 
 * @param cartModel
 * @return
 */
    private CartModel verifyAndClearPaymentTransactionsOnCart(CartModel cartModel)
    {
        if (!cartModel.getPaymentTransactions().isEmpty())
        {
            // clear payment transactions
            clearPaymentTransactionsOnCart(cartModel);
            // reset guid since its used as a merchantId for payment subscriptions and is a base id for generating PaymentTransaction.code
            // see de.hybris.platform.payment.impl.DefaultPaymentServiceImpl.authorize(DefaultPaymentServiceImpl.java:177)
            cartModel.setGuid(getGuidKeyGenerator().generate().toString());
        }
        getModelService().save(cartModel);
        return cartModel;
    }
}
