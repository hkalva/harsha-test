/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.services.category;


import java.util.Collection;
import java.util.List;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;


/**
 * Extending OOTB Interface to add new custom method to implement
 *
 * Provides methods for working with {@link CategoryModel}.
 *
 * Please be aware that since CategoryModel is catalog version aware, all methods of this service will work according to
 * the active session catalog versions (see {@link CatalogVersionService#setSessionCatalogVersions(Collection)} for more
 * details). So in consequence, the methods may return different results for different session users.
 *
 * If required, the following example shows how to temporarily switch these active catalog versions without changing the
 * enclosing callers session context:
 *
 * <pre>
 * SessionService sessionService = ...;
 * CatalogVersionService catalogVersionService = ...;
 * CatalogVersionModel myCatalogVersion = ...;
 * 
 * Collection&lt;CategoryModel&gt; rootCategories = (Collection&lt;CategoryModel&gt;) sessionService
 * 		.executeInLocalView(new SessionExecutionBody()
 * 		{
 * 			public Object execute()
 * 			{
 * 				catalogVersionService.setSessionCatalogVersions(Collections.singleton(myCatalogVersion));
 * 
 * 				return categoryService.getRootCategoriesForCatalogVersion(myCatalogVersion);
 * 			}
 * 		});
 * </pre>
 *
 * @spring.bean categoryService
 *
 * @see CatalogVersionService
 */
public interface LeaCategoryService extends CategoryService
{
    /**
     * Filters the input list of categories to the list of categories that has at least one approved product considering
     * the sub categories as well
     *
     * @param categories
     *            - List of categories retrieved from the DB
     * @return List of categories where each category has at least one approved product
     */
    List<CategoryModel> findAllCategoriesWithApprovedProducts(final List<CategoryModel> categories);
}
