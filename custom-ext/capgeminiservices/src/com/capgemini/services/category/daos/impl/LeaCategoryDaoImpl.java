/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.capgemini.services.category.daos.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.capgemini.services.category.daos.LeaCategoryDao;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.category.daos.impl.DefaultCategoryDao;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.link.Link;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;


/**
 * Extending Default CategoryDao to find the count of all Approved products under a category and all its sub categories
 */
public class LeaCategoryDaoImpl extends DefaultCategoryDao implements LeaCategoryDao
{
    /*
     * (non-Javadoc)
     * 
     * @see com.lyonscg.services.category.daos.LeaCategoryDao#findIfCategoryHasApprovedProducts(de.hybris.platform
     * .category.model.CategoryModel)
     */
    public Boolean findIfCategoryHasApprovedProducts(final CategoryModel category)
    {
        final StringBuilder query = new StringBuilder();
        query.append("SELECT COUNT( {p:" + CategoryModel.PK + "} ) ");
        query.append("FROM {" + ProductModel._TYPECODE + " AS p ");
        query.append("JOIN " + ProductModel._CATEGORYPRODUCTRELATION + "* AS c2pRel ");
        query.append("ON {c2pRel." + Link.TARGET + "}={p:" + CategoryModel.PK + "} } ");
        query.append("WHERE {p." + ProductModel.APPROVALSTATUS + "}= ?" + ProductModel.APPROVALSTATUS);
        query.append(" AND {c2pRel." + Link.SOURCE + "} ");
        final Collection<CategoryModel> allSubCategories = category.getAllSubcategories();
        final List<Class> resultClasses = new ArrayList<Class>();
        resultClasses.add(Integer.class);
        if (allSubCategories.isEmpty())
        {
            query.append(" = ?").append(CategoryModel._TYPECODE);
            final Map<String, Object> params = new HashMap<String, Object>();
            params.put(ProductModel.APPROVALSTATUS, ArticleApprovalStatus.APPROVED);
            params.put(CategoryModel._TYPECODE, category);
            final Integer count = (Integer) doSearch(query.toString(), params, resultClasses).iterator().next();
            if (count > 0)
            {
                return true;
            }
        }
        else
        {
            //see PLA-5243
            final String categoriesParam = "categories";
            query.append(" IN ( ?" + categoriesParam + " )");
            int count = 0;
            final List<CategoryModel> categoryList = new ArrayList<CategoryModel>(allSubCategories);
            categoryList.add(category);
            int pageSize = Registry.getCurrentTenantNoFallback().getDataSource().getMaxPreparedParameterCount();
            if (pageSize == -1)
            {
                pageSize = categoryList.size();
            }
            int offset = 0;
            while (offset < categoryList.size())
            {
                final int currentPageEnd = Math.min(categoryList.size(), offset + pageSize);
                final Map<String, Object> params = new HashMap<String, Object>();
                params.put(ProductModel.APPROVALSTATUS, ArticleApprovalStatus.APPROVED);
                params.put(categoriesParam, categoryList.subList(offset, currentPageEnd));
                final Integer subTotalCount = (Integer) doSearch(query.toString(), params, resultClasses).iterator().next();
                count += subTotalCount.intValue();

                if (count > 0)
                {
                    return true;
                }

                // jump to next page for next turn
                offset += pageSize;
            }
        }

        return false;
    }

    private <T> List<T> doSearch(final String query, final Map<String, Object> params, final List<Class> resultClasses)
    {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query);
        if (params != null)
        {
            fQuery.addQueryParameters(params);
        }
        if (resultClasses != null)
        {
            fQuery.setResultClassList(resultClasses);
        }
        final SearchResult<T> result = getFlexibleSearchService().search(fQuery);
        final List<T> elements = result.getResult();
        return elements;
    }

}
