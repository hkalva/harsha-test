/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.capgemini.services.category.daos;

import de.hybris.platform.category.daos.CategoryDao;
import de.hybris.platform.category.model.CategoryModel;


/**
 * Extending the interface to include an additional custom method. The {@link CategoryModel} DAO.
 *
 * @spring.bean leaCategoryDao
 */
public interface LeaCategoryDao extends CategoryDao
{
    /**
     * Method to find whether the category has at least one approved product
     *
     * @param category
     * @return Boolean true if it has and false if not
     */
    public Boolean findIfCategoryHasApprovedProducts(final CategoryModel category);

}
