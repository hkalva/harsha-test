/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.capgemini.services.category.impl;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.impl.DefaultCategoryService;
import de.hybris.platform.category.model.CategoryModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.services.category.LeaCategoryService;
import com.capgemini.services.category.daos.LeaCategoryDao;


/**
 * Default implementation for {@link CategoryService}.
 */
public class LeaCategoryServiceImpl extends DefaultCategoryService implements LeaCategoryService
{

    private static final Logger LOG = LoggerFactory.getLogger(LeaCategoryServiceImpl.class);

    private LeaCategoryDao leaCategoryDao;

    /**
     * @param leaCategoryDao
     */
    public void setLeaCategoryDao(final LeaCategoryDao leaCategoryDao)
    {
        this.leaCategoryDao = leaCategoryDao;
    }

    /**
     * Iterate through the category list and remove the ones that do not have any approved product within it and its sub
     * categories hierarchy
     *
     * @param categories
     * @return list of categories with at least one approved products
     */
    public List<CategoryModel> findAllCategoriesWithApprovedProducts(final List<CategoryModel> categories)
    {
        if (CollectionUtils.isNotEmpty(categories))
        {
            LOG.debug("findAllCategoriesWithApprovedProducts: Entered: Categories list count:" + categories.size());
            final List<CategoryModel> filteredCategoryLst = new ArrayList<CategoryModel>(categories);
            for (final CategoryModel category : categories)
            {
                if (!leaCategoryDao.findIfCategoryHasApprovedProducts(category))
                {
                    LOG.debug("Category does not have any approved products :" + category.getCode());
                    filteredCategoryLst.remove(category);
                }
            }
            LOG.debug("findAllCategoriesWithApprovedProducts: Exit: Categories list count: " + filteredCategoryLst.size());
            return filteredCategoryLst;
        }
        return Collections.emptyList();
    }
}
