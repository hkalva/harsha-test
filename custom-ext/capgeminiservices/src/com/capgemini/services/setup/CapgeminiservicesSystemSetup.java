package com.capgemini.services.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.services.constants.CapgeminiservicesConstants;


/**
 * This class is to setup data during init/update.
 */
@SystemSetup(extension = CapgeminiservicesConstants.EXTENSIONNAME)
public class CapgeminiservicesSystemSetup extends AbstractSystemSetup
{
    /**
     * constant for catalog sync.
     */
    private static final String CATALOG_SYNC = "syncProducts&ContentCatalogs";

    /**
     * constant for rlp.
     */
    public static final String RLP = "rlp";

    /**
     * Create Essential Data method.
     */
    @SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
    public void createEssentialData()
    {
        //do nothing.
    }

    /**
     * This method will be called during the system initialization and project data.
     *
     * @param context
     *            the context provides the selected parameters and values
     */
    @SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.ALL)
    public void createProjectData(final SystemSetupContext context)
    {
        if (getBooleanSystemSetupParameter(context, CATALOG_SYNC))
        {
            synchronizeCatalog(context, String.format("%sProductCatalog", RLP));
            synchronizeCatalog(context, String.format("%sContentCatalog", RLP));
        }
    }

    private boolean synchronizeCatalog(final SystemSetupContext context, final String catalogName)
    {
        logInfo(context, String.format("Begin synchronizing Catalog [%s]", catalogName));
        final PerformResult syncCronJobResult = getSetupSyncJobService().executeCatalogSyncJob(catalogName);
        
        if (isSyncRerunNeeded(syncCronJobResult))
        {
            logError(context, String.format("Content Catalog [%s] sync has issues.", catalogName), null);
            return false;
        }
        
        return true;
    }

    /**
     * To display Initialization Options during init/update.
     *
     * @return - List of system setup parameters.
     */
    @SystemSetupParameterMethod
    @Override
    public List<SystemSetupParameter> getInitializationOptions()
    {
        final List<SystemSetupParameter> params = new ArrayList<>(1);
        params.add(createBooleanSystemSetupParameter(CATALOG_SYNC, "Sync catalogs", false));
        return params;
    }
    
}
