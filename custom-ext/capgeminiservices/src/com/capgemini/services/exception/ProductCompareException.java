/*
* Copyright (c) 2019 Capgemini. All rights reserved.
*
* This software is the confidential and proprietary information of Capgemini
* ("Confidential Information"). You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the
* license agreement you entered into with Capgemini.
*/
package com.capgemini.services.exception;

/**
 * 
 * @author lyonscg
 *
 */
public class ProductCompareException extends RuntimeException
{

    /**
     * default serial version uid.
     */
    private static final long serialVersionUID = 1L;

    /**
     * constructor with message parameter.
     * 
     * @param message
     *            - the message.
     */
    public ProductCompareException(String message)
    {
        super(message);
    }

    /**
     * constructor with throwable parameter.
     * 
     * @param throwable
     *            - the throwable.
     */
    public ProductCompareException(Throwable throwable)
    {
        super(throwable);
    }

    /**
     * constructor with message and throwable parameter.
     * 
     * @param message
     *            - the message.
     * @param throwable
     *            - the throwable.
     */

    public ProductCompareException(String message, Throwable throwable)
    {
        super(message, throwable);
    }

}
