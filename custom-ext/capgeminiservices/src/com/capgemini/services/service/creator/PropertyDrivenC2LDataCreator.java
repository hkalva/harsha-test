/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.services.service.creator;

import de.hybris.platform.core.systemsetup.datacreator.impl.C2LDataCreator;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.c2l.C2LManager;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Custom class to override the default Currency with a value from the property file.
 *
 * @author lyonscg
 *
 */
public class PropertyDrivenC2LDataCreator extends C2LDataCreator
{
    private static final Logger LOG = LoggerFactory.getLogger(PropertyDrivenC2LDataCreator.class);
    @Resource
    private ConfigurationService configurationService;

    @Override
    public void populateDatabase()
    {
        if (this.cleanUpPropertyDrivenArtificialCurrency() || this.getC2LManagerInstance().getAllCurrencies().isEmpty())
        {
            this.createPropertyDefaultCurrency();
        }
    }

    private void createPropertyDefaultCurrency()
    {
        String currencyCode = configurationService.getConfiguration().getString("rlp.c2l.default.currency", "USD");
        String currencySymbol = configurationService.getConfiguration().getString("rlp.c2l.default.currency.symbol", "$");
        final StringBuilder msg = new StringBuilder();
        msg.append("Creating default currency: ").append(currencyCode).append(" and setting it to current session context");
        this.logDebugStatement(msg.toString());
        final JaloSession currentSession = this.getJaloCurrentSession();
        final Currency currency = this.createOrGetCurrency(currencyCode, currencySymbol);
        currency.setBase();
        currentSession.getSessionContext().setCurrency(currency);
    }

    private JaloSession getJaloCurrentSession()
    {
        return JaloSession.getCurrentSession();
    }

    private boolean cleanUpPropertyDrivenArtificialCurrency()
    {
        try
        {
            this.getC2LManagerInstance().getCurrencyByIsoCode("---").remove();
            this.logDebugStatement("Clearing up artificial currency \'---\'");
            return true;
        }
        catch (final Exception e)
        {
            this.logDebugStatement("No need to clear up artificial currency \'---\' - not existent");
            LOG.error("No need to clear up artificial currency \\'---\\' - not existent", e);
            return false;
        }
    }

    private void logDebugStatement(final String msg)
    {
        if (LOG.isDebugEnabled())
        {
            LOG.debug(msg);
        }

    }

    private C2LManager getC2LManagerInstance()
    {
        return C2LManager.getInstance();
    }
}
