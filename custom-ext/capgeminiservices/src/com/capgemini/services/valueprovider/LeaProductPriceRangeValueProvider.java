/*
•Copyright (c) 2019 Capgemini. All rights reserved.
*
•This software is the confidential and proprietary information of Capgemini
•("Confidential Information"). You shall not disclose such Confidential
•Information and shall use it only in accordance with the terms of the
•license agreement you entered into with Capgemini.
*/
package com.capgemini.services.valueprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.cms2.model.contents.ContentCatalogModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.entity.SolrPriceRange;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.ProductPriceRangeValueProvider;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.variants.model.VariantProductModel;


/** @author lyonscg.
 * To provide price range for product variants.
 */
public class LeaProductPriceRangeValueProvider extends ProductPriceRangeValueProvider
{
    private FieldNameProvider fieldNameProvider;
    @Resource
    private PriceService priceService;
    @Resource
    private UserService userService;
    @Resource
    private SessionService sessionService;
    @Resource
    private CommonI18NService commonI18NService;
    @Resource
    private CatalogVersionService catalogVersionService;

    /**
     * Get field value for product model
     * 
     * @param {@link ProductModel} base product.
     * @return {@link Object} Return field value, in this case, it's the priceRange.
     */
    @Override
    public Object getFieldValue(final ProductModel product)
    {
        String priceRange = null;
        // make sure you have the baseProduct because variantProducts won't have other variants
        final ProductModel baseProduct = getBaseProduct(product);

        final Collection<VariantProductModel> variants = baseProduct.getVariants();
        if (CollectionUtils.isNotEmpty(variants))
        {
            final List<PriceInformation> allPricesInfos = new ArrayList<PriceInformation>();

            // collect all price infos
            for (final VariantProductModel variant : variants)
            {
                allPricesInfos.addAll(priceService.getPriceInformationsForProduct((ProductModel) variant));
            }

            if (!allPricesInfos.isEmpty())
            {
                priceRange = getPriceRange(allPricesInfos);
            }
        }
        return priceRange;
    }

    /**
     * Gets the price range.
     *
     * @param allPricesInfos
     *            {@link List<{@link PriceInformation}>} list of price information for all variant product.
     * @return {@link String} the price range for all variant product.
     */
    protected String getPriceRange(final List<PriceInformation> allPricesInfos)
    {

        if (CollectionUtils.isEmpty(allPricesInfos))
        {
            return "";
        }
        MinMaxPriceInformation minMax = getLowestHighestPriceInformation(allPricesInfos);

        final PriceInformation lowest = minMax.getMinValue();
        final PriceInformation highest = minMax.getMaxValue();
        return SolrPriceRange.buildSolrPropertyFromPriceRows(lowest.getPriceValue().getValue(), lowest.getPriceValue()
                .getCurrencyIso(), highest.getPriceValue().getValue(), highest.getPriceValue().getCurrencyIso());
    }

    /**
     * Gets the lowest and highest price information.
     *
     * @param allPricesInfos
     *            {@link List<{@link PriceInformation}>} list of price information for all variant product.
     * @return {@link Array<{@link PriceInformation}>} the lowest and highest price information with lowest at index 0
     *         and highest at index 1.
     */

    private MinMaxPriceInformation getLowestHighestPriceInformation(List<PriceInformation> allPricesInfos)
    {
        PriceInformation min = allPricesInfos.get(0);
        PriceInformation max = allPricesInfos.get(0);
        for (int i = 1; i < allPricesInfos.size(); i++)
        {
            if (allPricesInfos.get(i).getPriceValue().getValue() < min.getPriceValue().getValue())
            {
                min = allPricesInfos.get(i);
            }
            if (allPricesInfos.get(i).getPriceValue().getValue() > max.getPriceValue().getValue())
            {
                max = allPricesInfos.get(i);
            }
        }

        return new MinMaxPriceInformation(min, max);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * de.hybris.platform.commerceservices.search.solrfacetsearch.provider.AbstractMultidimensionalProductFieldValueProvider
     * #getFieldValues(de.hybris.platform.solrfacetsearch.config.IndexConfig,
     * de.hybris.platform.solrfacetsearch.config.IndexedProperty, java.lang.Object)
     */
    @Override
    public Collection<FieldValue> getFieldValues(IndexConfig indexConfig, IndexedProperty indexedProperty, Object model)
            throws FieldValueProviderException
    {
        checkModel(model);
        final Collection<FieldValue> fieldValues = new ArrayList<FieldValue>();

        final Collection<CatalogVersionModel> filteredCatalogVersions = filterCatalogVersions(catalogVersionService
                .getSessionCatalogVersions());

        if (model instanceof ProductModel)
        {
            for (final CurrencyModel currency : indexConfig.getCurrencies())
            {

                sessionService.executeInLocalView(new SessionExecutionBody() {
                    @Override
                    public void executeWithoutResult()
                    {
                        catalogVersionService.setSessionCatalogVersions(filteredCatalogVersions);
                        commonI18NService.setCurrentCurrency(currency);
                        final Object field = getFieldValue((ProductModel) model);
                        final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty,
                                currency.getIsocode());
                        for (String fieldName : fieldNames)
                        {
                            fieldValues.add(new FieldValue(fieldName, field));
                        }
                    }
                }, userService.getAnonymousUser());
            }

        }
        else
        {
            throw new FieldValueProviderException("Cannot get field for non-product item");
        }

        return fieldValues;
    }

    /**
     * Check if the given model is an instance of ProductModel.
     *
     * @param {@link Object} passed in model
     * 
     * @throws FieldValueProviderException
     *             if the given model is not an instance of {@link ProductModel}
     */
    protected void checkModel(final Object model) throws FieldValueProviderException
    {
        if (!(model instanceof ProductModel))
        {
            throw new FieldValueProviderException("Cannot evaluate price of non-product item");
        }
    }

    /**
     * Filter out catalogVersion which is an instance of {@link ClassificationSystemVersionModel} or the catalog of
     * which is an instance of {@link ContentCatalogModel}.
     *
     * @param sessionCatalogVersions
     *            the session catalog versions
     * @return the collection of {@link CatalogVersionModel}
     */
    protected Collection<CatalogVersionModel> filterCatalogVersions(final Collection<CatalogVersionModel> sessionCatalogVersions)
    {
        final List<CatalogVersionModel> result = new ArrayList<CatalogVersionModel>(sessionCatalogVersions.size());

        for (final CatalogVersionModel catalogVersion : sessionCatalogVersions)
        {
            if (!(catalogVersion instanceof ClassificationSystemVersionModel)
                    && !(catalogVersion.getCatalog() instanceof ContentCatalogModel))
            {
                result.add(catalogVersion);
            }
        }

        return result;
    }

    /**
     * Customized data structure to only contain min and max value for {@link PriceInformation}
     * 
     * @author lyonscg
     *
     */
    private static class MinMaxPriceInformation
    {
        private PriceInformation minValue, maxValue;

        public MinMaxPriceInformation(PriceInformation min, PriceInformation max)
        {
            this.setMinValue(min);
            this.setMaxValue(max);
        }

        public PriceInformation getMinValue()
        {
            return minValue;
        }

        public void setMinValue(PriceInformation min)
        {
            this.minValue = min;
        }

        public PriceInformation getMaxValue()
        {
            return maxValue;
        }

        public void setMaxValue(PriceInformation max)
        {
            this.maxValue = max;
        }
    }

    protected FieldNameProvider getFieldNameProvider()
    {
        return fieldNameProvider;
    }

    @Required
    public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
    {
        this.fieldNameProvider = fieldNameProvider;
    }

}
