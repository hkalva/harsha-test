package com.capgemini.services.util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;

/**
 *
 * @author lyonscg
 *
 */
@UnitTest
public class CharsetUtilTest
{
    
    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
    }
    
    /**
     * test null character set.
     */
    @Test
    public void testNullCharset()
    {
        Charset charset = CharsetUtil.getDefaultCharset(null);
        Assert.assertTrue(Charset.defaultCharset().equals(charset));
    }
    
    /**
     * test invalid character set.
     */
    @Test
    public void testInvalidCharset()
    {
        Charset charset = CharsetUtil.getDefaultCharset("HYB-16");
        Assert.assertTrue(Charset.defaultCharset().equals(charset));
    }
    
    /**
     * test valid character set.
     */
    @Test
    public void testValidCharset()
    {
        Charset charset = CharsetUtil.getDefaultCharset("UTF-16");
        Assert.assertTrue(StandardCharsets.UTF_16.equals(charset));
    }
    
    
}
