package com.capgemini.services.rulesengine.definitions.conditions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerContext;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerException;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeOperator;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionDefinitionData;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.capgemini.services.rulesengine.definitions.conditions.LyonscgBundlePromoConditionTranslator;

/**
 * This class tests the variables that are passed to the LyonscgBundlePromoConditionTranslator class.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class LyonscgBundlePromoConditionTranslatorTest
{
    private static final String ORDER_ENTRY_RAO_VAR = "orderEntryRaoVariable";

    @InjectMocks
    LyonscgBundlePromoConditionTranslator lyonscgBundlePromoConditionTranslator;

    @Mock
    private RuleCompilerContext context;

    @Mock
    private RuleConditionData condition;

    @Mock
    private RuleConditionDefinitionData conditionDefinition;


    /**
     * Setup mock data before test execution.
     */
    @Before
    public void setUp()
    {
        Mockito.when(this.context.generateVariable(OrderEntryRAO.class)).thenReturn(ORDER_ENTRY_RAO_VAR);
    }

    public LyonscgBundlePromoConditionTranslatorTest(){

    }

    /**
     * This method tests if all the variables are properly set in the translate method.
     * @throws RuleCompilerException
     */
    @Test
    public void testIfOrderEntryIsPartOfBundle() throws RuleCompilerException
    {
        try
        {
            RuleIrCondition ruleIrCondition = this.lyonscgBundlePromoConditionTranslator.translate(context, condition, conditionDefinition);
            Assert.assertThat(ruleIrCondition, CoreMatchers.instanceOf(RuleIrAttributeCondition.class));
            Assert.assertEquals(RuleIrAttributeOperator.EQUAL, ((RuleIrAttributeCondition) ruleIrCondition).getOperator());

        }
        catch (RuleCompilerException e)
        {
            e.printStackTrace();
        }
    }

}
