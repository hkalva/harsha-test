package com.capgemini.services.order.impl;

import static org.mockito.BDDMockito.given;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.services.order.impl.LeaCommerceCartRestorationStrategy;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartFactory;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.site.BaseSiteService;


/**
 * Test class for {@link LeaCommerceCartRestorationStrategy}.
 */
@UnitTest
public class LeaCommerceCartRestorationStrategyTest
{
    @InjectMocks
    private LeaCommerceCartRestorationStrategy testClass;
    private CommerceCartParameter parameter;
    @Mock
    private CartModel cartModel;
    @Mock
    private BaseSiteService baseSiteService;
    @Mock
    private BaseSiteModel baseSiteModel;
    @Mock
    private TimeService timeService;
    @Mock
    private ModelService modelService;
    @Mock
    private CommerceCartCalculationStrategy commerceCartCalculationStrategy;
    @Mock
    private CartService cartService;
    @Mock
    private CartFactory cartFactory;
    @Mock
    private CommerceCommonI18NService commerceCommonI18NService;
    private int cartValidityPeriod = 12960000;
    private Date currentTime;

    /**
     * Setup method called before every test method.
     */
    @Before
    public void setUp() throws Exception
    {
        testClass = new LeaCommerceCartRestorationStrategy();
        MockitoAnnotations.initMocks(this);
        testClass.setCartValidityPeriod(cartValidityPeriod);
        parameter = new CommerceCartParameter();
        parameter.setCart(cartModel);
        currentTime = new Date();

        given(cartModel.getSite()).willReturn(baseSiteModel);
        given(baseSiteService.getCurrentBaseSite()).willReturn(baseSiteModel);
        given(timeService.getCurrentTime()).willReturn(currentTime);
        given(cartModel.getModifiedtime()).willReturn(currentTime);
    }

    /**
     * Test method for
     * {@link com.lyonscg.services.order.impl.LeaCommerceCartRestorationStrategy.restoreCart(CommerceCartParameter)}.
     *
     * @throws CommerceCartRestorationException
     */
    @Test
    public void restoreCartTest() throws CommerceCartRestorationException
    {
        testClass.restoreCart(parameter);
        Mockito.verify(commerceCommonI18NService, Mockito.never()).setCurrentCurrency(Mockito.any());
    }
}
