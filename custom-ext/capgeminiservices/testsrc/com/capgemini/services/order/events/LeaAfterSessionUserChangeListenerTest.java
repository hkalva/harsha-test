package com.capgemini.services.order.events;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.europe1.enums.UserPriceGroup;
import de.hybris.platform.europe1.jalo.Europe1PriceFactory;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;


/**
 * Integration test for {@link LeaAfterSessionUserChangeListener}.
 */
@IntegrationTest
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(
        { LogRunListener.class })
public class LeaAfterSessionUserChangeListenerTest extends ServicelayerTest
{
    private Logger LOG = LoggerFactory.getLogger(LeaAfterSessionUserChangeListenerTest.class);

    @Resource
    private UserService userService;

    @Resource
    private CartService cartService;

    @Resource
    private ModelService modelService;

    @Resource
    private SessionService sessionService;

    B2BCustomerModel b2bUser;
    String userId;

    /**
     * Test the onEvent method by setting initial Admin user, changing it and validating the scenario. Followed by, set
     * the user to be a B2BCustomerModel and verifiy that the session's branch property is set.
     *
     * @throws Exception
     */
    @Test
    public void testOnEvent() throws Exception
    {
        // get admin user
        UserModel user = userService.getAdminUser();

        // create session cart, anonymous user is set on cart
        cartService.getSessionCart();

        // set admin user as current
        userService.setCurrentUser(user);

        // AfterSessionUserChangeEvent processed in background
        // ....

        // get current cart
        final CartModel cart = cartService.getSessionCart();

        // refresh cart to ensure that cart user is persisted
        modelService.refresh(cart);

        assertNotNull("Cart is null.", cart);
        assertNotNull("Cart user is null.", cart.getUser());
        assertEquals("Cart user differs.", user, cart.getUser());
        Assert.assertNull("Session Branch is not null.", sessionService.getCurrentSession().getAttribute("session.branch"));

        //Create B2BUser & B2BUnit, & PriceGroup
        userId = "test@test.com";
        final B2BCustomerModel customer = modelService.create(B2BCustomerModel.class);
        customer.setUid(userId);
        customer.setName(userId);
        customer.setEmail(userId);

        final B2BUnitModel unit = modelService.create(B2BUnitModel.class);
        final String uid = "aUnit";
        unit.setUid(uid);
        unit.setLocName("aUnit");

        Europe1PriceFactory.getInstance();
        final EnumerationValueModel userPriceGroup = (EnumerationValueModel) this.modelService.create("UserPriceGroup");
        userPriceGroup.setCode("testPriceGroup1");
        userPriceGroup.setName("testPriceGroup1");
        modelService.save(userPriceGroup);

        unit.setUserPriceGroup(UserPriceGroup.valueOf("testPriceGroup1"));

        final Set<PrincipalGroupModel> groups = new HashSet<>(customer.getGroups());
        groups.add(unit);
        customer.setGroups(groups);
        customer.setDefaultB2BUnit(unit);

        modelService.save(unit);
        modelService.save(customer);

        b2bUser = userService.getUserForUID(userId, B2BCustomerModel.class);
        Assert.assertNotNull(userId + " user is null", b2bUser);

        //Test with a B2BCustomerModel User
        user = userService.getUserForUID(userId);

        // set B2BCustomerModel user as current
        userService.setCurrentUser(user);

        // AfterSessionUserChangeEvent processed in background
        // ....

        // get current cart
        final CartModel cart1 = cartService.getSessionCart();

        // refresh cart to ensure that cart user is persisted
        modelService.refresh(cart1);

        assertNotNull("Cart is null.", cart1);
        assertNotNull("Cart user is null.", cart1.getUser());
        assertEquals("Cart user differs.", user, cart1.getUser());
        LOG.info("sessionService.getCurrentSession().getAttribute(branch) :"
                + sessionService.getCurrentSession().getAttribute("branch"));
        Assert.assertNotNull("Session Branch is null.", sessionService.getCurrentSession().getAttribute("branch"));

    }

}
