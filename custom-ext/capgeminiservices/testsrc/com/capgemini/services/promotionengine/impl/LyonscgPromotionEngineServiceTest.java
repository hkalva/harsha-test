/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.capgemini.services.promotionengine.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.customersupport.CommerceCustomerSupportService;
import de.hybris.platform.promotionengineservices.dao.PromotionDao;
import de.hybris.platform.promotionengineservices.dao.PromotionSourceRuleDao;
import de.hybris.platform.promotionengineservices.promotionengine.impl.DefaultPromotionEngineService;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerContextFactory;
import de.hybris.platform.ruleengineservices.compiler.RuleIrVariablesGeneratorFactory;
import de.hybris.platform.ruleengineservices.compiler.impl.DefaultRuleCompilerContext;
import de.hybris.platform.ruleengineservices.rule.dao.RuleDao;
import de.hybris.platform.ruleengineservices.rule.services.RuleConditionsService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.configuration.Configuration;
import org.slf4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.LoggerFactory;

import com.capgemini.services.promotionengine.impl.LyonscgPromotionEngineService;


/**
 * LyonscgPromotionEngineServiceTest test class to test the getPromotionsForProduct() method.
 */
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(LogRunListener.class)
@UnitTest
public class LyonscgPromotionEngineServiceTest extends ServicelayerTransactionalTest
{
	/**
	 * Logger for the Class.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(LyonscgPromotionEngineServiceTest.class);
	private static final String PRODUCT_CODE_1 = "1595";
	private static final String CATEGORY_CODE_1 = "3881018";
	@InjectMocks
	private LyonscgPromotionEngineService lyonscgPromotionEngineService;
	@Resource
	private PromotionSourceRuleDao promotionSourceRuleDao;
	@Resource
	private PromotionDao promotionDao;
	@Resource
	private RuleDao ruleDao;
	@Resource
	private RuleCompilerContextFactory<DefaultRuleCompilerContext> ruleCompilerContextFactory;
	@Resource
	private RuleConditionsService ruleConditionsService;
	@Resource
	private RuleIrVariablesGeneratorFactory ruleIrVariablesGeneratorFactory;
	//@Resource
	//private CatalogVersionService catalogVersionService;
	@Mock
	private CategoryService categoryService;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private Configuration configuration;
	@Mock
	private CommerceCustomerSupportService commerceCustomerSupportService;
	@Mock
	private DefaultPromotionEngineService defaultPromotionEngineService;
	@Mock
	private ProductModel product1;
	@Mock
	private CategoryModel category1;
	private HashSet superCategories;

	/**
	 * Setup method called before every test method.
	 */
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		lyonscgPromotionEngineService.setCategoryService(categoryService);
		lyonscgPromotionEngineService.setPromotionSourceRuleDao(promotionSourceRuleDao);
		lyonscgPromotionEngineService.setRuleDao(ruleDao);
		lyonscgPromotionEngineService.setRuleCompilerContextFactory(ruleCompilerContextFactory);
		lyonscgPromotionEngineService.setRuleConditionsService(ruleConditionsService);
		lyonscgPromotionEngineService.setRuleIrVariablesGeneratorFactory(ruleIrVariablesGeneratorFactory);

		importCsv("/capgeminiservices/test/asmPromotionData.impex", "utf-8");

		given(configurationService.getConfiguration()).willReturn(configuration);
		given(new Boolean(configuration.getBoolean("promotionengineservices.getpromotionsforproduct.disable"))).willReturn(
				Boolean.FALSE);
		given(new Boolean(commerceCustomerSupportService.isCustomerSupportAgentActive())).willReturn(Boolean.FALSE);

		given(category1.getCode()).willReturn(PRODUCT_CODE_1);
		given(category1.getSupercategories()).willReturn(new ArrayList());

		superCategories = new HashSet();
		superCategories.add(category1);

		given(product1.getCode()).willReturn(CATEGORY_CODE_1);
		given(product1.getSupercategories()).willReturn(superCategories);
		given(categoryService.getAllSupercategoriesForCategory(category1)).willReturn(new ArrayList());

	}

	/**
	 * Test method for
	 * {@link com.capgemini.services.promotionengine.impl.LyonscgPromotionEngineService#getPromotionsForProduct(java.util.Collection, de.hybris.platform.core.model.product.ProductModel)}
	 * .
	 */
	@Test
	public final void testGetPromotionsForProductCollectionOfPromotionGroupModelProductModel()
	{
		LOG.info("testGetPromotionsForProductCollectionOfPromotionGroupModelProductModel entered");
		final PromotionGroupModel group = this.promotionDao.findPromotionGroupByCode("website1");
		final HashSet promotionGroups = new HashSet();
		promotionGroups.add(group);
		//Test when the CustomerSupportAgent Session is Inactive
		List promotions = lyonscgPromotionEngineService.getPromotionsForProduct(promotionGroups, product1);
		Assert.assertEquals("Promotion List size should be 0", 0, promotions.size());

		//Test when the Customer Support Agent Session is active
		given(new Boolean(commerceCustomerSupportService.isCustomerSupportAgentActive())).willReturn(Boolean.TRUE);

		promotions = lyonscgPromotionEngineService.getPromotionsForProduct(promotionGroups, product1);
		Assert.assertEquals("Promotion List size should be 1", 1, promotions.size());

		//property value to disable the display Promotions on Product details page is TRUE - promotion list should be empty
		given(new Boolean(configuration.getBoolean("promotionengineservices.getpromotionsforproduct.disable"))).willReturn(
				Boolean.TRUE);
		promotions = lyonscgPromotionEngineService.getPromotionsForProduct(promotionGroups, product1);
		Assert.assertEquals("Promotion List size should be 0", 0, promotions.size());
	}

}
