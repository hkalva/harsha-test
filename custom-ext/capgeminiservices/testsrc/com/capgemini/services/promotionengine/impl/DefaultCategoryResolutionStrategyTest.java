package com.capgemini.services.promotionengine.impl;

import java.util.Arrays;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.ruleengineservices.rule.data.RuleParameterData;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

/**
 * 
 * @author lyonscg
 *
 */
@UnitTest
public class DefaultCategoryResolutionStrategyTest
{
    /**
     * constant for category code.
     */
    private static final String CATEGORY_CODE = "code";
    
    /**
     * constant for category name.
     */
    private static final String CATEGORY_NAME = "name";

    /**
     * constant for invalid category code.
     */
    private static final String INVALID_CATEGORY_CODE = "invalid";
    
    /**
     * the strategy to test.
     */
    private DefaultCategoryResolutionStrategy strategy;
    
    /**
     * the category service.
     */
    @Mock
    private CategoryService categoryService;
    
    /**
     * the category.
     */
    @Mock
    private CategoryModel category;
    
    /**
     * the rule parameter.
     */
    @Mock
    private RuleParameterData data;
    
    /**
     * the promotion result.
     */
    @Mock
    private PromotionResultModel promotionResult;
    
    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        strategy = new DefaultCategoryResolutionStrategy();
        strategy.setCategoryService(categoryService);
        Mockito.when(category.getCode()).thenReturn(CATEGORY_CODE);
        Mockito.when(category.getName()).thenReturn(CATEGORY_NAME);
        Mockito.when(categoryService.getCategoryForCode(CATEGORY_CODE)).thenReturn(category);
        Mockito.when(categoryService.getCategoryForCode(INVALID_CATEGORY_CODE)).thenThrow(UnknownIdentifierException.class);
        Mockito.when(data.getValue()).thenReturn(Arrays.asList(CATEGORY_CODE, INVALID_CATEGORY_CODE));
    }
    
    /**
     * test getCategoryRepresentation.
     */
    @Test
    public void testGetCategoryRepresentation()
    {
        Assert.assertTrue(CATEGORY_NAME.equals(strategy.getCategoryRepresentation(category)));
    }
    
    /**
     * test getCategory.
     */
    @Test
    public void testGetCategory()
    {
        Assert.assertTrue(strategy.getCategory(null) == null);
        Assert.assertTrue(strategy.getCategory(INVALID_CATEGORY_CODE) == null);
        Assert.assertTrue(category.equals(strategy.getCategory(CATEGORY_CODE)));
    }
    
    /**
     * test getValue.
     */
    @Test
    public void testGetValue()
    {
        String result = strategy.getValue(data, promotionResult, Locale.US);
        Assert.assertTrue((CATEGORY_NAME + " ," + INVALID_CATEGORY_CODE).equals(result));
    }
}
