package com.capgemini.services.url;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;


/**
 * Test class for {@link UrlHelper}
 */
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(LogRunListener.class)
@UnitTest
public class UrlHelperTest
{
	/**
	 * Logger for the Class.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UrlHelperTest.class);

	/**
	 * Setup Method - @ Before to be executed before every test method.
	 */
	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
	}


	/**
	 * Test the urlSafe method by passing the Product Name.
	 */
	@Test
	public void testUrlSafeForProductNames()
	{
		String url1 = "angle grinder rt-ag 115";
		String expectedSafeUrl = "angle-grinder-rt-ag-115";
		LOG.info("Input Url 1: {}", url1);

		String actualSafeUrl = UrlHelper.urlSafe(url1);
		LOG.info("Safe URL 1: {}", actualSafeUrl);
		assertEquals(expectedSafeUrl, actualSafeUrl);

		url1 = "14.4v cordless drill + 2 batteries";
		expectedSafeUrl = "14-4v-cordless-drill-2-batteries";
		LOG.info("Input Url 2: {}", url1);

		actualSafeUrl = UrlHelper.urlSafe(url1);
		LOG.info("Safe URL 2: {}", actualSafeUrl);
		assertEquals(expectedSafeUrl, actualSafeUrl);

		url1 = "";
		expectedSafeUrl = StringUtils.EMPTY;
		LOG.info("Input Url3 Empty String: {}", url1);

		actualSafeUrl = UrlHelper.urlSafe(url1);
		LOG.info("Safe URL3 Empty String: {}", actualSafeUrl);
		assertNotNull(actualSafeUrl);
		assertEquals(expectedSafeUrl, actualSafeUrl);


		url1 = null;
		expectedSafeUrl = StringUtils.EMPTY;
		LOG.info("Input Url4: {}", url1);

		actualSafeUrl = UrlHelper.urlSafe(url1);
		LOG.info("Safe URL4: {}", actualSafeUrl);
		assertNotNull(actualSafeUrl);
		assertEquals(expectedSafeUrl, actualSafeUrl);
	}

	/**
	 * Test urlSafe method by passing the Category Paths of various combinations.
	 */
	@Test
	public void testUrlSafeForCategoryPaths()
	{
		String url = "angle grinders";
		String expectedSafeUrl = "angle-grinders";
		LOG.info("Input Url 1: {}", url);

		String actualSafeUrl = UrlHelper.urlSafe(url);
		LOG.info("Safe URL 1: {}", actualSafeUrl);
		assertEquals(expectedSafeUrl, actualSafeUrl);

		url = "measuring-&-layout-tools";
		expectedSafeUrl = "measuring-layout-tools";
		LOG.info("Input Url 2: {}", url);

		actualSafeUrl = UrlHelper.urlSafe(url);
		LOG.info("Safe URL 2: {}", actualSafeUrl);
		assertEquals(expectedSafeUrl, actualSafeUrl);

		url = "safety/men's";
		expectedSafeUrl = "safety/mens";
		LOG.info("Input Url 3: {}", url);

		actualSafeUrl = UrlHelper.urlSafe(url);
		LOG.info("Safe URL 3: {}", actualSafeUrl);
		assertEquals(expectedSafeUrl, actualSafeUrl);
	}

    /**
     * Test replaceSlash method by passing the Category name with /.
     */
    @Test
    public void testReplaceSlash()
    {
        assertEquals("musical-play", UrlHelper.replaceSlash("musical/play"));
        assertNull(UrlHelper.replaceSlash(null));
    }

    /**
     * Test removeDoubleSlashes method by passing the Category url with //+.
     */
    @Test
    public void testRemoveDoubleSlashes()
    {
        assertNull(UrlHelper.removeDoubleSlashes(null));
        assertEquals("movie/action", UrlHelper.removeDoubleSlashes("movie/action"));
        assertEquals("theater/musical/play", UrlHelper.removeDoubleSlashes("theater////musical//play"));
    }
}
