package com.capgemini.services.valueprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.PropertyFieldValueProviderTestBase;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.variants.model.VariantProductModel;
import groovy.transform.IndexedProperty;


@UnitTest
public class LeaProductPriceRangeValueProviderTest extends PropertyFieldValueProviderTestBase
{
    
    
    private static final String SOLR_PROPERTY = "priceRange";
    private static final String FIELD_NAME_INDEXING = SOLR_PROPERTY + "_string";
    private static final String FIELD_NAME_SORTING = SOLR_PROPERTY + "_sortable_string";
    
    @Mock
    private ProductService productService;
    
    @Mock
    private ProductModel product;
    
    @Mock 
    private VariantProductModel varaintProduct;

    @Mock
    private IndexedProperty indexedProperty;

    private LeaProductPriceRangeValueProvider valueProvider;
    
    @Mock
    private PriceService priceService;
    
    @Mock
    VariantProductModel variant;

    @Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
        
        valueProvider = new LeaProductPriceRangeValueProvider();
        
        Mockito.when(productService.getProductForCode(Mockito.anyString())).thenAnswer(new Answer<ProductModel>() {

            @Override
            public ProductModel answer(final InvocationOnMock invocation) throws Throwable
            {
                Mockito.when(product.getCode()).thenReturn(invocation.getArgumentAt(0, String.class));
                return product;
            }

        });
        configure();
    }

    @Override
    protected String getPropertyName()
    {
        return SOLR_PROPERTY;
    }

    @Override
    protected void configure()
    {
        setPropertyFieldValueProvider(valueProvider);
        configureBase();

        ((LeaProductPriceRangeValueProvider) getPropertyFieldValueProvider()).setFieldNameProvider(fieldNameProvider);

        Assert.assertTrue(getPropertyFieldValueProvider() instanceof FieldValueProvider);
    }
    
    /**
     * 
     */
    @Test
    public void getFileValue()
    {
        
       ProductModel baseProduct = getBaseProduct(product);
       
       
       Collection<VariantProductModel> variants = baseProduct.getVariants();
       
       Assert.assertNotNull(variants);
       
      
       
       
       List<PriceInformation> priceInfoLst = new ArrayList<PriceInformation>();
       
         PriceValue priceVal1 =getPriceValue("USD",10.20d,false);
         
         PriceValue priceVal2 =getPriceValue("USD",8.00d,false);
         PriceValue priceVal3 =getPriceValue("USD",5.0d,false);
         PriceValue priceVal4 =getPriceValue("USD",7.20d,false);
         PriceValue priceVal5 =getPriceValue("USD",3.20d,false);
         PriceValue priceVal6 =getPriceValue("USD",55.20d,false);
         
        
          
         PriceInformation priceInfo1 = new PriceInformation(priceVal1);
         
         PriceInformation priceInfo2 = new PriceInformation(priceVal2);
         PriceInformation priceInfo3 = new PriceInformation(priceVal3);
         PriceInformation priceInfo4 = new PriceInformation(priceVal4);
         PriceInformation priceInfo5 = new PriceInformation(priceVal5);
         PriceInformation priceInfo6 = new PriceInformation(priceVal6);
         
         priceInfoLst.add(priceInfo1);
         priceInfoLst.add(priceInfo2);
         priceInfoLst.add(priceInfo3);
         priceInfoLst.add(priceInfo4);
         priceInfoLst.add(priceInfo5);
         priceInfoLst.add(priceInfo6);
         
       
       
        Mockito.when(priceService.getPriceInformationsForProduct((ProductModel) variant)).thenReturn(priceInfoLst);
        
        String priceRange = valueProvider.getPriceRange(priceInfoLst);
        
        Assert.assertNotNull(priceRange);
       
       
    }
    
    /**
     * This method return the PriceValue
     * @param currencyIso
     * @param price
     * @param neetto
     * @return
     */
    private PriceValue getPriceValue(String currencyIso,double price , boolean neetto)
    {
        
       return  new PriceValue(currencyIso,price,neetto);   
    }
    /**
     * For a given product, retrieve its base product.
     * 
     * @param model
     *           The product.
     * @return The base product, if it is a variant product, or the model, if it is already the base product or is not a
     *         variant product.
     */
    public ProductModel getBaseProduct(final ProductModel model)
    {
        final ProductModel baseProduct;
        if (model instanceof VariantProductModel)
        {
            final VariantProductModel variant = (VariantProductModel) model;
            baseProduct = variant.getBaseProduct();
        }
        else
        {
            baseProduct = model;
        }
        return baseProduct;
    }

}
