package com.capgemini.services.interceptors.cronjob;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;


/**
 * Test class for {@link com.capgemini.services.interceptors.cronjob.CronJobPrepareInterceptor}.
 */
@UnitTest
public class CronJobPrepareInterceptorTest
{
    /**
     * the default currency iso.
     */
    private static final String DEFAULT_CURRENCY_ISO = "USD";

    /**
     * the default language iso.
     */
    private static final String DEFAULT_LANGUAGE_ISO = "en";
    
    /**
     * the default node id.
     */
    private static final Integer DEFAULT_NODE_ID = Integer.valueOf(0);
    
    /**
     * the interceptor to test.
     */
    private CronJobPrepareInterceptor interceptor;
    
    /**
     * the default currency.
     */
    @Mock
    private CurrencyModel defaultCurrency;
    
    /**
     * the default language.
     */
    @Mock
    private LanguageModel defaultLanguage;
    
    /**
     * the common i18n service.
     */
    @Mock
    private CommonI18NService commonI18NService;

    /**
     * the cron job.
     */
    @Mock
    private CronJobModel cronJob;
    
    /**
     * the interceptor context.
     */
    @Mock
    private InterceptorContext interceptorContext;
    
    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        interceptor = new CronJobPrepareInterceptor();
        interceptor.setDefaultLanguageIso(DEFAULT_LANGUAGE_ISO);
        interceptor.setDefaultCurrencyIso(DEFAULT_CURRENCY_ISO);
        interceptor.setDefaultNodeId(DEFAULT_NODE_ID);
        interceptor.setCommonI18NService(commonI18NService);
        Mockito.when(commonI18NService.getCurrency(DEFAULT_CURRENCY_ISO)).thenReturn(defaultCurrency);
        Mockito.when(commonI18NService.getLanguage(DEFAULT_LANGUAGE_ISO)).thenReturn(defaultLanguage);
        Mockito.doNothing().when(cronJob).setNodeID(Mockito.any(Integer.class));
        Mockito.doNothing().when(cronJob).setSessionCurrency(Mockito.any(CurrencyModel.class));
        Mockito.doNothing().when(cronJob).setSessionLanguage(Mockito.any(LanguageModel.class));
    }
    
    /**
     * test with cronjob values.
     */
    @Test
    public void testCronjobWithValues() throws InterceptorException
    {
        Mockito.when(cronJob.getNodeID()).thenReturn(DEFAULT_NODE_ID);
        Mockito.when(cronJob.getSessionCurrency()).thenReturn(defaultCurrency);
        Mockito.when(cronJob.getSessionLanguage()).thenReturn(defaultLanguage);
        interceptor.onPrepare(cronJob, interceptorContext);
        Mockito.verify(cronJob, Mockito.never()).setNodeID(Mockito.any(Integer.class));
        Mockito.verify(cronJob, Mockito.never()).setSessionCurrency(Mockito.any(CurrencyModel.class));
        Mockito.verify(cronJob, Mockito.never()).setSessionLanguage(Mockito.any(LanguageModel.class));
    }
    
    /**
     * test without cronjob values.
     */
    @Test
    public void testCronjobWithoutValues() throws InterceptorException
    {
        Mockito.when(cronJob.getNodeID()).thenReturn(null);
        Mockito.when(cronJob.getSessionCurrency()).thenReturn(null);
        Mockito.when(cronJob.getSessionLanguage()).thenReturn(null);
        interceptor.onPrepare(cronJob, interceptorContext);
        Mockito.verify(cronJob, Mockito.times(1)).setNodeID(Mockito.any(Integer.class));
        Mockito.verify(cronJob, Mockito.times(1)).setSessionCurrency(Mockito.any(CurrencyModel.class));
        Mockito.verify(cronJob, Mockito.times(1)).setSessionLanguage(Mockito.any(LanguageModel.class));
        Mockito.verify(commonI18NService, Mockito.times(1)).getCurrency(Mockito.anyString());
        Mockito.verify(commonI18NService, Mockito.times(1)).getLanguage(Mockito.anyString());
        
        //call again
        Mockito.when(cronJob.getNodeID()).thenReturn(null);
        Mockito.when(cronJob.getSessionCurrency()).thenReturn(null);
        Mockito.when(cronJob.getSessionLanguage()).thenReturn(null);
        interceptor.onPrepare(cronJob, interceptorContext);
        Mockito.verify(cronJob, Mockito.times(2)).setNodeID(Mockito.any(Integer.class));
        Mockito.verify(cronJob, Mockito.times(2)).setSessionCurrency(Mockito.any(CurrencyModel.class));
        Mockito.verify(cronJob, Mockito.times(2)).setSessionLanguage(Mockito.any(LanguageModel.class));
        Mockito.verify(commonI18NService, Mockito.times(1)).getCurrency(Mockito.anyString());
        Mockito.verify(commonI18NService, Mockito.times(1)).getLanguage(Mockito.anyString());
    }
    
}
