/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.capgemini.services.category.impl;

import static junit.framework.Assert.assertEquals;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.services.category.LeaCategoryService;
import com.capgemini.services.category.daos.LeaCategoryDao;
import com.capgemini.services.category.daos.impl.LeaCategoryDaoImpl;
import com.capgemini.services.category.impl.LeaCategoryServiceImpl;


/**
 * Mock tests for the {@link LeaCategoryService}.
 */
@UnitTest
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(
        { LogRunListener.class })
public class LeaCategoryServiceImplTest
{
    @InjectMocks
    private final LeaCategoryService categoryService = new LeaCategoryServiceImpl();
    @Mock
    private final LeaCategoryDao leaCategoryDao = new LeaCategoryDaoImpl();
    @Mock
    private CategoryModel categoryMock1a;
    @Mock
    private CategoryModel categoryMock2a;
    @Mock
    private CategoryModel categoryMock1b;
    @Mock
    private CategoryModel categoryMock2b;

    /**
     * Method invoked before every test method execution, initMocks() will inject all the mocks into the
     * LeaCategoryService
     */
    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * To test FindAllCategoriesWithApprovedProducts() method with a empty list passed to it
     */
    @Test
    public void testFindAllCategoriesWithApprovedProductsWithEmptyCategoryList()
    {
        final List<CategoryModel> nullCatList = null;
        final List<CategoryModel> resultCatList = categoryService.findAllCategoriesWithApprovedProducts(nullCatList);

        assertThat(resultCatList).isNotNull();
        assertThat(resultCatList).isEqualTo(Collections.emptyList());
    }

    /**
     * To test FindAllCategoriesWithApprovedProducts() method with a null list passed to it
     */
    @Test
    public void testFindAllCategoriesWithApprovedProductsWithNullCategoryList()
    {
        final List<CategoryModel> emptyCatList = Collections.emptyList();
        final List<CategoryModel> resultCatList = categoryService.findAllCategoriesWithApprovedProducts(emptyCatList);

        assertThat(resultCatList).isNotNull();
        assertThat(resultCatList).isEqualTo(Collections.emptyList());

    }

    /**
     * To test FindAllCategoriesWithApprovedProducts() method with list of categories that do not have any approved
     * products passed to it
     */
    @Test
    public void testFindAllCategoriesWithApprovedProductsWithCatListHavingCatsWithNoApprovedProds()
    {
        // given
        given(leaCategoryDao.findIfCategoryHasApprovedProducts(categoryMock1a)).willReturn(Boolean.FALSE);
        given(leaCategoryDao.findIfCategoryHasApprovedProducts(categoryMock2a)).willReturn(Boolean.FALSE);
        given(leaCategoryDao.findIfCategoryHasApprovedProducts(categoryMock1b)).willReturn(Boolean.FALSE);
        given(leaCategoryDao.findIfCategoryHasApprovedProducts(categoryMock2b)).willReturn(Boolean.FALSE);

        final List<CategoryModel> categoryLst = new ArrayList<CategoryModel>();
        categoryLst.add(categoryMock1a);
        categoryLst.add(categoryMock2a);
        categoryLst.add(categoryMock1b);
        categoryLst.add(categoryMock2b);

        //when
        final List<CategoryModel> resultCatList = categoryService.findAllCategoriesWithApprovedProducts(categoryLst);

        //Then
        assertEquals("Category List is empty", 0, resultCatList.size());
        assertThat(resultCatList).hasSize(0);
        assertThat(resultCatList).isEqualTo(Collections.emptyList());

    }

    /**
     * To test FindAllCategoriesWithApprovedProducts() method with list of categories that all have approved products
     * passed to it
     */
    @Test
    public void testFindAllCategoriesWithApprovedProductsWithCatListHavingAllCatsWithApprovedProds()
    {
        // given
        given(leaCategoryDao.findIfCategoryHasApprovedProducts(categoryMock1a)).willReturn(Boolean.TRUE);
        given(leaCategoryDao.findIfCategoryHasApprovedProducts(categoryMock2a)).willReturn(Boolean.TRUE);
        given(leaCategoryDao.findIfCategoryHasApprovedProducts(categoryMock1b)).willReturn(Boolean.TRUE);
        given(leaCategoryDao.findIfCategoryHasApprovedProducts(categoryMock2b)).willReturn(Boolean.TRUE);

        final List<CategoryModel> categoryLst = new ArrayList<CategoryModel>();
        categoryLst.add(categoryMock1a);
        categoryLst.add(categoryMock2a);
        categoryLst.add(categoryMock1b);
        categoryLst.add(categoryMock2b);

        //when
        final List<CategoryModel> resultCatList = categoryService.findAllCategoriesWithApprovedProducts(categoryLst);

        //Then
        assertEquals("Category List has all the 4 categories", 4, resultCatList.size());
        assertThat(resultCatList).hasSize(4);

    }

    /**
     * To test FindAllCategoriesWithApprovedProducts() method with list of categories where some have approved products
     * passed to it
     */
    @Test
    public void testFindAllCategoriesWithApprovedProductsWithCatListHavingSomeCatsWithApprovedProds()
    {
        // given
        given(leaCategoryDao.findIfCategoryHasApprovedProducts(categoryMock1a)).willReturn(Boolean.TRUE);
        given(leaCategoryDao.findIfCategoryHasApprovedProducts(categoryMock2a)).willReturn(Boolean.FALSE);
        given(leaCategoryDao.findIfCategoryHasApprovedProducts(categoryMock1b)).willReturn(Boolean.TRUE);
        given(leaCategoryDao.findIfCategoryHasApprovedProducts(categoryMock2b)).willReturn(Boolean.FALSE);

        final List<CategoryModel> categoryLst = new ArrayList<CategoryModel>();
        categoryLst.add(categoryMock1a);
        categoryLst.add(categoryMock2a);
        categoryLst.add(categoryMock1b);
        categoryLst.add(categoryMock2b);

        //when
        final List<CategoryModel> resultCatList = categoryService.findAllCategoriesWithApprovedProducts(categoryLst);

        //Then
        assertEquals("Category List has 2 categories after filtering", 2, resultCatList.size());
        assertThat(resultCatList).hasSize(2);

    }

}
