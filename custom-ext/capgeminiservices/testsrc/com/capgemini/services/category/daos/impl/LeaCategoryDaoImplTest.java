/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.capgemini.services.category.daos.impl;

import static junit.framework.Assert.assertEquals;
import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.user.UserManager;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.time.TimeService;

import java.util.Collections;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.LoggerFactory;

import com.capgemini.services.category.LeaCategoryService;
import com.capgemini.services.category.daos.LeaCategoryDao;


/**
 * This test class is to verify the methods in LeaCategoryDao Class
 */
@IntegrationTest
public class LeaCategoryDaoImplTest extends ServicelayerTransactionalTest
{
    private static final Logger LOG = LoggerFactory.getLogger(LeaCategoryDaoImplTest.class);

    @Resource
    private LeaCategoryDao categoryDao;
    @Resource
    private LeaCategoryService categoryService;
    @Resource
    private ModelService modelService;
    @Resource
    private TimeService timeService;
    @Resource
    protected CatalogVersionService catalogVersionService;

    /**
     * Before method called before every test method is invoked and it creates the necessary data to perform testing
     *
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception
    {
        createCoreData();
        createLeaTestCatalog();
    }

    /**
     * @After method executed after the execution of every test method
     */
    @After
    public void tearDown()
    {
        timeService.resetTimeOffset();
    }

    /**
     * To create a new testLeaCatalog and import it from an Impex csv file for the category testing purposes
     *
     * @throws Exception
     */
    public static void createLeaTestCatalog() throws Exception
    {
        LOG.info("Creating test LeaCatalog ..");
        JaloSession.getCurrentSession().setUser(UserManager.getInstance().getAdminEmployee());
        final long startTime = System.currentTimeMillis();

        final FlexibleSearchService flexibleSearchService = (FlexibleSearchService) Registry.getApplicationContext().getBean(
                "flexibleSearchService");
        junit.framework.Assert.assertNotNull(flexibleSearchService);
        final ModelService modelService = (ModelService) Registry.getApplicationContext().getBean("modelService");
        junit.framework.Assert.assertNotNull(modelService);

        importCsv("/test/testLeaCatalog.csv", "windows-1252");

        final CatalogModel catalog = (CatalogModel) flexibleSearchService
                .search("SELECT {PK} FROM {Catalog} WHERE {id}='testLeaCatalog'").getResult().get(0);
        junit.framework.Assert.assertNotNull(catalog);
        final CatalogVersionModel version = (CatalogVersionModel) flexibleSearchService
                .search("SELECT {PK} FROM {CatalogVersion} WHERE {version}='Online' AND {catalog}=?catalog",
                        Collections.singletonMap("catalog", catalog)).getResult().get(0);
        junit.framework.Assert.assertNotNull(version);

        JaloSession.getCurrentSession().getSessionContext()
        .setAttribute("catalogversions", modelService.toPersistenceLayer(Collections.singletonList(version)));

        final CategoryModel category = (CategoryModel) flexibleSearchService
                .search("SELECT {PK} FROM {Category} WHERE {code}='testCategory2'").getResult().get(0);
        junit.framework.Assert.assertNotNull(category);

        final ProductModel product = (ProductModel) flexibleSearchService
                .search("SELECT {PK} FROM {Product} WHERE {code}='testProduct0'").getResult().get(0);
        junit.framework.Assert.assertNotNull(product);

        junit.framework.Assert.assertTrue(product.getSupercategories().contains(category));

        LOG.info("Finished creating test catalog in " + (System.currentTimeMillis() - startTime) + "ms");
    }

    /**
     * To test the method findIfCategoryHasApprovedProducts() with a empty category passed to it
     */
    @Test
    public void testfindIfCategoryHasApprovedProductsByEmptyCategory()
    {
        final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion("testLeaCatalog", "Online");
        final CategoryModel empty = modelService.create(CategoryModel.class);
        empty.setCode("empty");
        empty.setCatalogVersion(catalogVersion);
        modelService.save(empty);

        final Boolean hasApprovedProducts = categoryDao.findIfCategoryHasApprovedProducts(empty);
        assertEquals("Category does not have any approved products", Boolean.FALSE, hasApprovedProducts);
    }

    /**
     * Category with no sub categories and no products
     */
    @Test
    public void testfindIfCategoryHasApprovedProductsByCategoryWithNoSubCatAndNoProducts()
    {
        final String code = "testCategory5";
        final CategoryModel category = categoryService.getCategoryForCode(code);
        assertThat(category).isNotNull();
        final Boolean hasApprovedProducts = categoryDao.findIfCategoryHasApprovedProducts(category);
        assertEquals("Category has NO approved products", Boolean.FALSE, hasApprovedProducts);
    }

    /**
     * //Category with no sub categories and has products but not approved
     */
    @Test
    public void testfindIfCategoryHasApprovedProductsByCategoryWithNoSubCatAndNoApprovedProducts()
    {
        final String code = "testCategory6";
        final CategoryModel category = categoryService.getCategoryForCode(code);
        assertThat(category).isNotNull();
        final Boolean hasApprovedProducts = categoryDao.findIfCategoryHasApprovedProducts(category);
        assertEquals("Category has NO approved products", Boolean.FALSE, hasApprovedProducts);
    }

    /**
     * Category with no Sub Categories and all Approved Products
     */
    @Test
    public void testfindIfCategoryHasApprovedProductsByCategoryWithNoSubCatAndHasApprovedProducts()
    {
        final String code = "testCategory7";
        final CategoryModel category = categoryService.getCategoryForCode(code);
        assertThat(category).isNotNull();
        final Boolean hasApprovedProducts = categoryDao.findIfCategoryHasApprovedProducts(category);
        assertEquals("Category has approved products", Boolean.TRUE, hasApprovedProducts);
    }

    /**
     * Category with Sub Categories but has NO approved products in the sub tree
     */
    @Test
    public void testfindIfCategoryHasApprovedProductsByCategoryWithSubCatAndNoApprovedProducts()
    {
        final String code = "testCategory8";
        final CategoryModel category = categoryService.getCategoryForCode(code);
        assertThat(category).isNotNull();
        final Boolean hasApprovedProducts = categoryDao.findIfCategoryHasApprovedProducts(category);
        assertEquals("Category has NO approved products", Boolean.FALSE, hasApprovedProducts);
    }

    /**
     * Category with Sub Categories but has NO products in the sub tree
     */
    @Test
    public void testfindIfCategoryHasApprovedProductsByCategoryWithSubCatAndNoProducts()
    {
        final String code = "testCategory1";
        final CategoryModel category = categoryService.getCategoryForCode(code);
        assertThat(category).isNotNull();
        final Boolean hasApprovedProducts = categoryDao.findIfCategoryHasApprovedProducts(category);
        assertEquals("Category has NO approved products", Boolean.FALSE, hasApprovedProducts);
    }

    /**
     * Category with Sub Categories ( 3 levels ) and has approved products in the sub tree
     */
    @Test
    public void testfindIfCategoryHasApprovedProductsByCategoryWithSubCatAndHasApprovedProducts()
    {
        final String code = "testCategory0";
        final CategoryModel category = categoryService.getCategoryForCode(code);
        assertThat(category).isNotNull();
        final Boolean hasApprovedProducts = categoryDao.findIfCategoryHasApprovedProducts(category);
        assertEquals("Category has approved products", Boolean.TRUE, hasApprovedProducts);
    }
}
