/*
* Copyright (c) 2019 Capgemini. All rights reserved.
*
* This software is the confidential and proprietary information of Capgemini
* ("Confidential Information"). You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the
* license agreement you entered into with Capgemini.
*/

package com.capgemini.dataxferservices.impex.translator.keyword;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.dataxferservices.dataimport.batch.keyword.KeywordImportAdapter;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.impex.jalo.header.AbstractDescriptor.DescriptorParams;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.jalo.Item;

/**
 * 
 * @author lyonscg
 *
 */
@UnitTest
public class KeywordTranslatorTest
{
    /**
     * constant for language key.
     */
    private static final String LANG_KEY = "lang";
    
    /**
     * constant for mode key.
     */
    private static final String MODE_KEY = "mode";
    
    /**
     * constant for replace mode.
     */
    private static final String REPLACE_MODE = "replace";
    
    /**
     * constant for append mode.
     */
    private static final String APPEND_MODE = "append";
    
    /**
     * constant for ignore.
     */
    private static final String IGNORE_TAG = "<ignore>";
    
    /**
     * the translator to test.
     */
    @Mock
    private KeywordTranslator translator;
    
    /**
     * the keyword import adapter.
     */
    @Mock
    private KeywordImportAdapter keywordImportAdapter;
    
    /**
     * the column descriptor.
     */
    @Mock
    private SpecialColumnDescriptor descriptor;
    
    /**
     * the descriptor params.
     */
    @Mock
    private DescriptorParams params;
    
    /**
     * the item.
     */
    @Mock
    private Item item;
    
    /**
     * set up data.
     */
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        Mockito.when(translator.getKeywordImportAdapterFromRegistry()).thenReturn(keywordImportAdapter);
        Mockito.doCallRealMethod().when(translator).setKeywordImportAdapter(Mockito.any(KeywordImportAdapter.class));
        Mockito.doNothing().when(keywordImportAdapter).performImport(Mockito.anyString(), Mockito.any(Item.class), Mockito.anyString(), Mockito.anyString());
        Mockito.when(descriptor.getDescriptorData()).thenReturn(params);
        Mockito.doCallRealMethod().when(translator).init(Mockito.any(SpecialColumnDescriptor.class));
        Mockito.doCallRealMethod().when(translator).getLanguage();
        Mockito.doCallRealMethod().when(translator).getMode();
        Mockito.doCallRealMethod().when(translator).getKeywordImportAdapter();
        Mockito.doCallRealMethod().when(translator).performImport(Mockito.anyString(), Mockito.any(Item.class));
    }
    
    /**
     * test init replace.
     */
    @Test
    public void testInitReplace()
    {
        Mockito.when(params.getModifier(LANG_KEY)).thenReturn("en");
        Mockito.when(params.getModifier(MODE_KEY)).thenReturn("Replace");
        translator.init(descriptor);
        Mockito.verify(translator, Mockito.times(1)).getKeywordImportAdapterFromRegistry();
        Assert.assertTrue(REPLACE_MODE.equals(translator.getMode()));
    }
    
    /**
     * test init append.
     */
    @Test
    public void testInitAppend()
    {
        Mockito.when(params.getModifier(LANG_KEY)).thenReturn("es");
        Mockito.when(params.getModifier(MODE_KEY)).thenReturn("aPPend");
        translator.init(descriptor);
        Mockito.verify(translator, Mockito.times(1)).getKeywordImportAdapterFromRegistry();
        Assert.assertTrue(APPEND_MODE.equals(translator.getMode()));
    }

    /**
     * test init any.
     */
    @Test
    public void testInitAny()
    {
        Mockito.when(params.getModifier(LANG_KEY)).thenReturn("fr");
        Mockito.when(params.getModifier(MODE_KEY)).thenReturn("blah");
        translator.init(descriptor);
        Mockito.verify(translator, Mockito.times(1)).getKeywordImportAdapterFromRegistry();
        Assert.assertTrue(REPLACE_MODE.equals(translator.getMode()));
    }
    
    /**
     * test performImport empty value.
     */
    @Test
    public void performImportEmptyValue()
    {
        translator.performImport(null, item);
        Mockito.verify(keywordImportAdapter, Mockito.never()).performImport(Mockito.anyString(), Mockito.any(Item.class), Mockito.anyString(), Mockito.anyString());
    }
    
    /**
     * test performImport ignore.
     */
    @Test
    public void performImportIgnore()
    {
        translator.performImport(IGNORE_TAG, item);
        Mockito.verify(keywordImportAdapter, Mockito.never()).performImport(Mockito.anyString(), Mockito.any(Item.class), Mockito.anyString(), Mockito.anyString());
    }

    /**
     * test performImport.
     */
    @Test
    public void performImport()
    {
        Mockito.when(params.getModifier(LANG_KEY)).thenReturn("en");
        Mockito.when(params.getModifier(MODE_KEY)).thenReturn("replace");
        translator.init(descriptor);
        translator.performImport("blah", item);
        Mockito.verify(keywordImportAdapter, Mockito.times(1)).performImport(Mockito.anyString(), Mockito.any(Item.class), Mockito.anyString(), Mockito.anyString());
    }
    
}
