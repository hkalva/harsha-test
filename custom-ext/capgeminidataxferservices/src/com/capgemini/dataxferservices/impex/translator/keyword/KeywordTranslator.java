/*
* Copyright (c) 2019 Capgemini. All rights reserved.
* 
* This software is the confidential and proprietary information of Capgemini
* ("Confidential Information"). You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the
* license agreement you entered into with Capgemini.
*/

package com.capgemini.dataxferservices.impex.translator.keyword;

import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;

import org.apache.commons.lang.StringUtils;

import com.capgemini.dataxferservices.dataimport.batch.keyword.KeywordImportAdapter;


/**
 * Keyword Translator for product and category.
 *
 * @author lyonscg
 *
 */
public class KeywordTranslator extends AbstractSpecialValueTranslator
{
    /**
     * constant for ignore.
     */
    private static final String IGNORE_TAG = "<ignore>";
    
    /**
     * constant for keyword import adapter bean.
     */
    private static final String KEYWORD_IMPORT_ADAPTER_BEAN = "keywordImportAdapter";
    
    /**
     * constant for default language iso.
     */
    private static final String DEFAULT_LANGUAGE_ISO = "en";
    
    /**
     * constant for language key.
     */
    private static final String LANG_KEY = "lang";
    
    /**
     * constant for mode key.
     */
    private static final String MODE_KEY = "mode";
    
    /**
     * constant for replace mode.
     */
    private static final String REPLACE_MODE = "replace";
    
    /**
     * constant for append mode.
     */
    private static final String APPEND_MODE = "append";
    
    /**
     * the keyword import adapter.
     */
    private KeywordImportAdapter keywordImportAdapter;
    
    /**
     * the language.
     */
    private String language;
    
    /**
     * the mode.
     */
    private String mode;

    /**
     * Initialize and read header.
     * 
     * @param columnDescriptor
     *            - the column descriptor.
     */
    @Override
    public void init(final SpecialColumnDescriptor columnDescriptor)
    {
        setKeywordImportAdapter(getKeywordImportAdapterFromRegistry());
        language = columnDescriptor.getDescriptorData().getModifier(LANG_KEY);
        mode = StringUtils.trim(columnDescriptor.getDescriptorData().getModifier(MODE_KEY));
        
        if (REPLACE_MODE.equalsIgnoreCase(mode))
        {
            mode = REPLACE_MODE;
        }
        else if (APPEND_MODE.equalsIgnoreCase(mode))
        {
            mode = APPEND_MODE;
        }
        else
        {
            mode = REPLACE_MODE;
        }
    }

    /**
     * Call KeywordImportAdapter to create keywords.
     * 
     * @param cellValue
     *            - the cell value.
     * @param processedItem
     *            - the item.
     */
    @Override
    public void performImport(final String cellValue, final Item processedItem)
    {
        if (StringUtils.isBlank(cellValue) || IGNORE_TAG.equals(cellValue))
        {
            return;
        }
        
        getKeywordImportAdapter().performImport(cellValue, processedItem,
                StringUtils.isBlank(getLanguage()) ? DEFAULT_LANGUAGE_ISO : getLanguage().trim(), getMode());
    }

    /**
     * 
     * @return KeywordImportAdapter
     */
    protected KeywordImportAdapter getKeywordImportAdapter()
    {
        return keywordImportAdapter;
    }

    /**
     * 
     * @param keywordImportAdapter
     *            - the keyword import adapter.
     */
    public void setKeywordImportAdapter(final KeywordImportAdapter keywordImportAdapter)
    {
        this.keywordImportAdapter = keywordImportAdapter;
    }

    /**
     * added for testability.
     * 
     * @return KeywordImportAdapter
     */
    protected KeywordImportAdapter getKeywordImportAdapterFromRegistry()
    {
        return (KeywordImportAdapter) Registry.getApplicationContext().getBean(KEYWORD_IMPORT_ADAPTER_BEAN);
    }
    
    /**
     * added for testability.
     * 
     * @return String
     */
    protected String getMode()
    {
        return mode;
    }
    
    /**
     * added for testability.
     * 
     * @return String
     */
    protected String getLanguage()
    {
        return language;
    }
}
