/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.capgemini.dataxferservices.dataimport.batch.task;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.acceleratorservices.dataimport.batch.task.AbstractImpexRunnerTask;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.user.UserService;

import java.io.File;
import java.io.FileNotFoundException;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;


/**
 * Task that imports an impex file by executing impex. Extends the AbstractImpexRunnerTask and overrides the method to
 * set the local Session user to be admin for the purpose of loading B2BUnit parent
 */
public abstract class AbstractLcgImpexRunnerTask extends AbstractImpexRunnerTask
{
    private static final Logger LOG = LoggerFactory.getLogger(AbstractLcgImpexRunnerTask.class);

    private UserService userService;

    /**
     * @return userService
     */
    public UserService getUserService()
    {
        return userService;
    }

    /**
     * @param userService
     */
    public void setUserService(final UserService userService)
    {
        this.userService = userService;
    }

    /*
     * (non-Javadoc)
     *
     * @see de.hybris.platform.acceleratorservices.dataimport.batch.task.AbstractLcgImpexRunnerTask#execute(de.hybris.
     * platform .acceleratorservices.dataimport.batch.BatchHeader) Overriding the OOTB method to set the user as Admin
     * to load the top level B2BUnit's which can be created only by users with admingroup
     */
    @Override
    public BatchHeader execute(final BatchHeader header) throws FileNotFoundException
    {
        Assert.notNull(header);
        Assert.notNull(header.getEncoding());

        if (CollectionUtils.isNotEmpty(header.getTransformedFiles()))
        {
            LOG.debug("##################### AbstractLcgImpexRunnerTask : execute : Entered -#################");

            getSessionService().executeInLocalView(new SessionExecutionBody() {
                
                @Override
                public void executeWithoutResult()
                {
                    getSessionService().setAttribute(SessionContext.USER, userService.getAdminUser());
                    LOG.debug(
                            "##################### AbstractLcgImpexRunnerTask : execute : Local Session with Admin User -#################");
                    try
                    {
                        for (final File file : header.getTransformedFiles())
                        {
                            processFile(file, header.getEncoding());
                        }
                    }
                    catch (final FileNotFoundException fnfe)
                    {
                        LOG.error("AbstractLcgImpexRunnerTask : FileNotFoundException exception" + fnfe.getMessage(), fnfe);
                    }
                }

            });
        }

        return header;
    }
}
