/*
* Copyright (c) 2019 Capgemini. All rights reserved.
* 
* This software is the confidential and proprietary information of Capgemini
* ("Confidential Information"). You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the
* license agreement you entered into with Capgemini.
*/

package com.capgemini.dataxferservices.dataimport.batch.keyword.impl;

import de.hybris.platform.catalog.daos.KeywordDao;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.KeywordModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.capgemini.dataxferservices.dataimport.batch.keyword.KeywordImportAdapter;


/**
 * Default implementation of keyword impex adapter for product and category.
 *
 * @author lyonscg
 */
public class KeywordImportAdapterImpl implements KeywordImportAdapter
{
	private static final String COLLECTION_SEPARATOR = "|";
	private static final String IGNORE_TAG = "<ignore>";
	private static final String APPEND_MODE = "append";
	private ModelService modelService;
	private KeywordDao keywordDao;
	private CommonI18NService commonI18NService;

	/**
	 * Adds "|" separated keywords to product/category.
	 */
	@Override
	public void performImport(final String cellValue, final Item productOrCategory, final String languageIso, final String mode)
	{
		Assert.hasText(cellValue);
		Assert.hasText(languageIso);
		Assert.notNull(productOrCategory);

		final ItemModel itemModel = getModelService().get(productOrCategory);
		if ((itemModel instanceof ProductModel || itemModel instanceof CategoryModel) && (!IGNORE_TAG.equals(cellValue)))
		{
			final List<KeywordModel> newKeywords = findOrCreateKeywords(cellValue, itemModel, languageIso);
			if (APPEND_MODE.equalsIgnoreCase(mode))
			{
				final List<KeywordModel> oldKeywords = getModelService().getAttributeValue(itemModel, "keywords");
				if (CollectionUtils.isNotEmpty(oldKeywords))
				{
					newKeywords.addAll(oldKeywords);
				}
			}
			getModelService().setAttributeValue(itemModel, "keywords", newKeywords.stream().distinct().collect(Collectors.toList()));
			getModelService().save(itemModel);
		}
	}

	/**
	 * Finds existing or creates keywords models matching keyword, catalog version and language.
	 */
	protected List<KeywordModel> findOrCreateKeywords(String cellValue, final ItemModel itemModel, final String languageIso)
	{
		cellValue = cellValue.trim();
		final String[] keywordsArray = cellValue.split("\\" + COLLECTION_SEPARATOR);
		final CatalogVersionModel catalogVersion = getModelService().getAttributeValue(itemModel, "catalogVersion");
		final List<KeywordModel> newKeywords = new ArrayList<>(keywordsArray.length);
		final LanguageModel language = getCommonI18NService().getLanguage(languageIso);
		for (String keyword : keywordsArray)
		{
			if (StringUtils.isBlank(keyword))
			{
				continue;
			}
			keyword = keyword.trim();
			final List<KeywordModel> keywords = getKeywordDao().getKeywords(catalogVersion, keyword);
			KeywordModel keywordModel = null;
			if (CollectionUtils.isEmpty(keywords))
			{
				keywordModel = createKeyword(catalogVersion, language, keyword);
			}
			else
			{
				final List<KeywordModel> englishKeywords = keywords.stream().filter(k -> language.equals(k.getLanguage()))
						.collect(Collectors.toList());
				if (CollectionUtils.isEmpty(englishKeywords))
				{
					keywordModel = createKeyword(catalogVersion, language, keyword);
				}
				else
				{
					keywordModel = englishKeywords.get(0);
				}

			}
			newKeywords.add(keywordModel);
		}
		return newKeywords;
	}

	/**
	 * Creates keyword model with provided keyword, catalog version and language.
	 */
	protected KeywordModel createKeyword(final CatalogVersionModel catalogVersion, final LanguageModel language,
			final String keyword)
	{
		final KeywordModel keywordModel = getModelService().create(KeywordModel.class);
		keywordModel.setKeyword(keyword);
		keywordModel.setCatalogVersion(catalogVersion);
		keywordModel.setLanguage(language);
		getModelService().save(keywordModel);
		return keywordModel;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected KeywordDao getKeywordDao()
	{
		return keywordDao;
	}

	@Required
	public void setKeywordDao(final KeywordDao keywordDao)
	{
		this.keywordDao = keywordDao;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

}
