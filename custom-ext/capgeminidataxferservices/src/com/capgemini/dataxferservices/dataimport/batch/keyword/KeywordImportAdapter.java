/*
* Copyright (c) 2019 Capgemini. All rights reserved.
* 
* This software is the confidential and proprietary information of Capgemini
* ("Confidential Information"). You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the
* license agreement you entered into with Capgemini.
*/

package com.capgemini.dataxferservices.dataimport.batch.keyword;

import de.hybris.platform.jalo.Item;


/**
 * Keyword impex adapter for product and category.
 *
 * @author lyonscg
 */
public interface KeywordImportAdapter
{
	/**
	 * Add Keywords to Category using service layer.
	 * 
	 * @param cellValue
	 * @param category
	 * @param mode
	 * @throws IllegalArgumentException
	 *            if the cellValue is empty, null or invalid or the product is null
	 */
	void performImport(String cellValue, Item category, String languageIso, String mode);
}
