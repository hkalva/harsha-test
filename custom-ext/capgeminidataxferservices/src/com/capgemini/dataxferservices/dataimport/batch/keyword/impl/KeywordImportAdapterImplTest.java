package com.capgemini.dataxferservices.dataimport.batch.keyword.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.daos.KeywordDao;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.KeywordModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Unit test for {@link KeywordImportAdapterImpl}.
 */
@UnitTest
public class KeywordImportAdapterImplTest
{
	private final String COLLECTION_SEPARATOR = "|";
	@InjectMocks
	private KeywordImportAdapterImpl testClass;
	@Mock
	private ModelService modelService;
	@Mock
	private KeywordDao keywordDao;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private Item productOrCategory;
	@Mock
	private CategoryModel category;
	@Mock
	private CatalogVersionModel catalogVersion;
	@Mock
	private LanguageModel language;
	@Mock
	private KeywordModel oneEnglish, twoNonEnglish, twoNewEnglish, threeNewEnglish;
	private String cellValue;
	private final String languageIso = "en";
	private String mode;
	private static final String APPEND_MODE = "append";

	/**
	 * Setup initial data.
	 */
	@Before
	public void setUp()
	{
		testClass = new KeywordImportAdapterImpl();
		MockitoAnnotations.initMocks(this);
		mode = "replace";
		given(modelService.get(productOrCategory)).willReturn(category);
		given(modelService.getAttributeValue(category, "catalogVersion")).willReturn(catalogVersion);

		given(commonI18NService.getLanguage(languageIso)).willReturn(language);
		given(twoNonEnglish.getLanguage()).willReturn(new LanguageModel());
		given(oneEnglish.getLanguage()).willReturn(language);

		given(modelService.create(KeywordModel.class)).willReturn(new KeywordModel());

	}

	/**
	 * Positive test case for
	 * {@link KeywordImportAdapterImpl#performImport(String, de.hybris.platform.jalo.Item, String)}.
	 */
	@Test
	public void testPerformImport()
	{
		final String one = "one";
		final String two = "two";
		cellValue = one + COLLECTION_SEPARATOR + two + COLLECTION_SEPARATOR;
		given(keywordDao.getKeywords(catalogVersion, one)).willReturn(Collections.singletonList(oneEnglish));
		given(keywordDao.getKeywords(catalogVersion, two)).willReturn(Collections.singletonList(twoNonEnglish));

		final ArgumentCaptor<CategoryModel> categoryCaptor = ArgumentCaptor.forClass(CategoryModel.class);
		final ArgumentCaptor<String> attributeCaptor = ArgumentCaptor.forClass(String.class);
		final ArgumentCaptor<List> listCaptor = ArgumentCaptor.forClass(List.class);

		testClass.performImport(cellValue, productOrCategory, languageIso, mode);
		Mockito.verify(modelService, Mockito.times(1)).setAttributeValue(categoryCaptor.capture(), attributeCaptor.capture(),
				listCaptor.capture());
		Mockito.verify(modelService, Mockito.times(1)).save(category);
		Mockito.verify(modelService, Mockito.never()).getAttributeValue(category, "keywords");

		final List keyWordModels = listCaptor.getAllValues().get(0);

		Assert.assertTrue(keyWordModels.size() == 2);
		Assert.assertTrue(keyWordModels.contains(oneEnglish));
		keyWordModels.remove(oneEnglish);
		final KeywordModel newKeyword = (KeywordModel) keyWordModels.get(0);
		Assert.assertEquals(two, newKeyword.getKeyword());
		Assert.assertEquals(catalogVersion, newKeyword.getCatalogVersion());
		Assert.assertEquals(language, newKeyword.getLanguage());
	}

	/**
	 * Positive test case for replace mode
	 * {@link KeywordImportAdapterImpl#performImport(String, de.hybris.platform.jalo.Item, String)}.
	 */
	@Test
	public void testPerformImportReplace()
	{
		final String one = "one";
		final String two = "two";
		cellValue = one + COLLECTION_SEPARATOR + two + COLLECTION_SEPARATOR;
		given(keywordDao.getKeywords(catalogVersion, one)).willReturn(Collections.singletonList(oneEnglish));
		given(keywordDao.getKeywords(catalogVersion, two)).willReturn(Collections.singletonList(twoNonEnglish));

		final ArgumentCaptor<CategoryModel> categoryCaptor = ArgumentCaptor.forClass(CategoryModel.class);
		final ArgumentCaptor<String> attributeCaptor = ArgumentCaptor.forClass(String.class);
		final ArgumentCaptor<List> listCaptor = ArgumentCaptor.forClass(List.class);

		testClass.performImport(cellValue, productOrCategory, languageIso, APPEND_MODE);
		Mockito.verify(modelService, Mockito.times(1)).setAttributeValue(categoryCaptor.capture(), attributeCaptor.capture(),
				listCaptor.capture());
		Mockito.verify(modelService, Mockito.times(1)).save(category);
		Mockito.verify(modelService, Mockito.times(1)).getAttributeValue(category, "keywords");
		final List keyWordModels = listCaptor.getAllValues().get(0);

		Assert.assertTrue(keyWordModels.size() == 2);
		Assert.assertTrue(keyWordModels.contains(oneEnglish));
		keyWordModels.remove(oneEnglish);
		final KeywordModel newKeyword = (KeywordModel) keyWordModels.get(0);
		Assert.assertEquals(two, newKeyword.getKeyword());
		Assert.assertEquals(catalogVersion, newKeyword.getCatalogVersion());
		Assert.assertEquals(language, newKeyword.getLanguage());
	}

	/**
	 * Negative test case for
	 * {@link KeywordImportAdapterImpl#performImport(String, de.hybris.platform.jalo.Item, String)}.
	 */
	@Test
	public void testPerformImportForNonProdCat()
	{
		final String one = "one";
		final String two = "two";
		cellValue = one + COLLECTION_SEPARATOR + two + COLLECTION_SEPARATOR;

		given(modelService.get(productOrCategory)).willReturn(new UserModel());
		testClass.performImport(cellValue, productOrCategory, languageIso, mode);

		Mockito.verify(modelService, Mockito.never()).save(category);
		Mockito.verify(modelService, Mockito.never()).save(Mockito.any(KeywordModel.class));
	}

	/**
	 * <ignore> test case for
	 * {@link KeywordImportAdapterImpl#performImport(String, de.hybris.platform.jalo.Item, String)}.
	 */
	@Test
	public void testPerformImportForIgnore()
	{
		final String one = "one";
		final String two = "two";
		cellValue = one + COLLECTION_SEPARATOR + two + COLLECTION_SEPARATOR;

		testClass.performImport("<ignore>", productOrCategory, languageIso, mode);

		Mockito.verify(modelService, Mockito.never()).save(category);
		Mockito.verify(modelService, Mockito.never()).save(Mockito.any(KeywordModel.class));
	}
}
