
<div class="videowrapper">
	<div id="player"></div>
</div>

<script>
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    
    var player;

    function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
            videoId: '${videoId}',
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });
    }

    function onPlayerReady(event) {
       var autoPlay = Boolean(${autoPlay});
       if (autoPlay){
           event.target.playVideo();
       }
    }
    
    var done = false;
    
    function onPlayerStateChange(event) {
        var timeoutSeconds = parseInt(${timeoutSeconds});
        if (event.data == YT.PlayerState.PLAYING && !done && !isNaN(timeoutSeconds)) {
            setTimeout(stopVideo, (timeoutSeconds * 1000));
            done = true;
        }
    }
    
    function stopVideo() {
        player.stopVideo();
    }
</script>