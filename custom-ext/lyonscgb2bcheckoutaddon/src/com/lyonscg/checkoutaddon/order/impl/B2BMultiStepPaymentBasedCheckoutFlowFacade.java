package com.lyonscg.checkoutaddon.order.impl;

import static de.hybris.platform.util.localization.Localization.getLocalizedString;

import java.util.List;
import java.util.Locale;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.paypal.hybris.facade.PayPalCheckoutFacade;
import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.worldpay.facades.payment.WorldpayAdditionalInfoFacade;
import com.worldpay.order.data.WorldpayAdditionalInfoData;
import com.worldpay.strategy.WorldpayAuthenticatedShopperIdStrategy;
import com.worldpay.util.WorldpayUtil;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.b2bacceleratorfacades.checkout.data.PlaceOrderData;
import de.hybris.platform.b2bacceleratorfacades.exception.EntityValidationException;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BCommentData;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BReplenishmentRecurrenceEnum;
import de.hybris.platform.b2bacceleratorfacades.order.data.TriggerData;
import de.hybris.platform.b2bacceleratorfacades.order.impl.B2BMultiStepCheckoutFlowFacade;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.L10NService;


/**
 * This class is to override the out of the box logic of determining the CheckoutFlowGroup from basestore. It determines
 * the CheckoutFlowGroup based on the payment type in the cart.
 */
public class B2BMultiStepPaymentBasedCheckoutFlowFacade extends B2BMultiStepCheckoutFlowFacade
{
    private static final String CHECKOUT_FLOW_GROUP = "CheckoutFlowGroup";
    private static final String DOT = ".";
    private static final String CART_CHECKOUT_TRANSACTION_NOT_AUTHORIZED = "cart.transation.notAuthorized";
    private static final String CART_CHECKOUT_TERM_UNCHECKED = "cart.term.unchecked";
    private static final String CART_CHECKOUT_NO_QUOTE_DESCRIPTION = "cart.no.quote.description";
    private static final String CART_CHECKOUT_REPLENISHMENT_NO_STARTDATE = "cart.replenishment.no.startdate";
    private static final String CART_CHECKOUT_REPLENISHMENT_NO_FREQUENCY = "cart.replenishment.no.frequency";
    private static final String CART_CHECKOUT_QUOTE_REQUIREMENTS_NOT_SATISFIED = "cart.quote.requirements.not.satisfied";
    private static final String CART_CHECKOUT_DELIVERYADDRESS_INVALID = "cart.deliveryAddress.invalid";
    private static final String CART_CHECKOUT_DELIVERYMODE_INVALID = "cart.deliveryMode.invalid";
    private static final String CART_CHECKOUT_PAYMENTINFO_EMPTY = "cart.paymentInfo.empty";
    private static final String CART_CHECKOUT_NOT_CALCULATED = "cart.not.calculated";

    private WorldpayAuthenticatedShopperIdStrategy worldpayAuthenticatedShopperIdStrategy;
    private WorldpayAdditionalInfoFacade worldpayAdditionalInfoFacade;
    private String worldpayPaymentProvider;
    private SiteConfigService siteConfigService;
    private PayPalCheckoutFacade payPalCheckoutFacade;
    private Converter<PaypalPaymentInfoModel, CCPaymentInfoData> defaultPaypalPaymentInfoConverter;
    private L10NService l10nService;

    /**
     * Resolves the checkout flow group based configuration property with lowercase key as
     * paymenType.paymentProvider.CheckoutFlowGroup.
     * 
     * @return - The checkout flow group based on payment methods.
     */
    @Override
    public String getCheckoutFlowGroupForCheckout()
    {
        final CheckoutPaymentType paymentType = getCart().getPaymentType();
        String checkoutFlowGroup = null;
        final String paymentProvider = getBaseStoreService().getCurrentBaseStore().getPaymentProvider();
        if (null != paymentType)
        {
            String keyString = paymentType.getCode()
                    + (StringUtils.isBlank(paymentProvider) ? StringUtils.EMPTY : DOT + paymentProvider) + DOT
                    + CHECKOUT_FLOW_GROUP;
            checkoutFlowGroup = getSiteConfigService().getProperty(keyString.toLowerCase(Locale.US));
        }
        if (StringUtils.isBlank(checkoutFlowGroup))
        {
            return getAcceleratorCheckoutFacade().getCheckoutFlowGroupForCheckout();
        }
        return checkoutFlowGroup;
    }

    /**
     * Authorize payment based on security code.
     * 
     * @param - securityCode to authorize payment.
     * @return - true if authorized successfully.
     */
    @Override
    public boolean authorizePayment(String securityCode)
    {
        CartModel cart = getCart();
        String paymentType = cart.getPaymentType().getCode();
        PaymentInfoModel paymentInfo = cart.getPaymentInfo();
        if (CheckoutPaymentType.CARD.getCode().equals(paymentType) && (paymentInfo instanceof CreditCardPaymentInfoModel))
        {
            return super.authorizePayment(serializeCvv(securityCode));
        }
        else if (isPaypalPayment(paymentType, paymentInfo))
        {
            return getPayPalCheckoutFacade().authorizePayment(securityCode);
        }
        return super.authorizePayment(securityCode);
    }

    /**
     * Overrides OOTB Place order logic only for quote orders by skipping the AUTHORIZATION ACCEPTED check.
     * 
     * @param- {@link PlaceOrderData} contains the order submit information.
     * @return- OrderData for the submitted order.
     */
    @Override
    public <T extends AbstractOrderData> T placeOrder(final PlaceOrderData placeOrderData) throws InvalidCartException
    {
        // term must be checked
        if (!placeOrderData.getTermsCheck().equals(Boolean.TRUE))
        {
            throw new EntityValidationException(getL10nService().getLocalizedString(CART_CHECKOUT_TERM_UNCHECKED));
        }

        // for CARD type, transaction must be authorized before placing order but transaction need not be authorized before placing quote.
        final boolean isCardtPaymentType = CheckoutPaymentType.CARD.getCode().equals(getCart().getPaymentType().getCode());
        if (isCardtPaymentType && !placeOrderData.getNegotiateQuote().booleanValue())
        {
            final List<PaymentTransactionModel> transactions = getCart().getPaymentTransactions();
            boolean authorized = false;
            for (final PaymentTransactionModel transaction : transactions)
            {
                for (final PaymentTransactionEntryModel entry : transaction.getEntries())
                {
                    if (entry.getType().equals(PaymentTransactionType.AUTHORIZATION)
                            && TransactionStatus.ACCEPTED.name().equals(entry.getTransactionStatus()))
                    {
                        authorized = true;
                        break;
                    }
                }
            }
            if (!authorized)
            {
                // FIXME - change error message
                throw new EntityValidationException(getL10nService().getLocalizedString(CART_CHECKOUT_TRANSACTION_NOT_AUTHORIZED));
            }
        }

        if (isValidCheckoutCart(placeOrderData))
        {
            // validate quote negotiation
            if (placeOrderData.getNegotiateQuote() != null && placeOrderData.getNegotiateQuote().equals(Boolean.TRUE))
            {
                if (StringUtils.isBlank(placeOrderData.getQuoteRequestDescription()))
                {
                    throw new EntityValidationException(getL10nService().getLocalizedString(CART_CHECKOUT_NO_QUOTE_DESCRIPTION));
                }
                else
                {
                    final B2BCommentData b2BComment = new B2BCommentData();
                    b2BComment.setComment(placeOrderData.getQuoteRequestDescription());

                    final CartData cartData = new CartData();
                    cartData.setB2BComment(b2BComment);

                    updateCheckoutCart(cartData);
                }
            }

            // validate replenishment
            if (placeOrderData.getReplenishmentOrder() != null && placeOrderData.getReplenishmentOrder().equals(Boolean.TRUE))
            {
                if (placeOrderData.getReplenishmentStartDate() == null)
                {
                    throw new EntityValidationException(getL10nService().getLocalizedString(
                            CART_CHECKOUT_REPLENISHMENT_NO_STARTDATE));
                }

                if (placeOrderData.getReplenishmentRecurrence().equals(B2BReplenishmentRecurrenceEnum.WEEKLY)
                        && CollectionUtils.isEmpty(placeOrderData.getNDaysOfWeek()))
                {
                    throw new EntityValidationException(getL10nService().getLocalizedString(
                            CART_CHECKOUT_REPLENISHMENT_NO_FREQUENCY));
                }

                final TriggerData triggerData = new TriggerData();
                populateTriggerDataFromPlaceOrderData(placeOrderData, triggerData);

                return (T) scheduleOrder(triggerData);
            }

            return (T) super.placeOrder();
        }

        return null;
    }

    /**
     * Creates and populates {@link CCPaymentInfoData}.
     */
    @Override
    protected CCPaymentInfoData getPaymentDetails()
    {
        final CartModel cart = getCart();
        if (cart != null)
        {
            final PaymentInfoModel paymentInfo = cart.getPaymentInfo();
            if (paymentInfo instanceof CreditCardPaymentInfoModel)
            {
                return getCreditCardPaymentInfoConverter().convert((CreditCardPaymentInfoModel) paymentInfo);
            }
            else if (paymentInfo instanceof PaypalPaymentInfoModel)
            {
                return getDefaultPaypalPaymentInfoConverter().convert((PaypalPaymentInfoModel) paymentInfo);
            }
        }

        return null;
    }

    /**
     * For Card payment if worldpay is the payment provider and if the securityCode is not empty deserializes the
     * securityCode and adds additional information to it and serializes it back.
     * 
     * @param - securityCode to authorize order.
     * @return security code.
     */
    protected String serializeCvv(final String securityCode)
    {
        if (StringUtils.isNotBlank(securityCode)
                && CheckoutPaymentType.CARD.getCode().equals(getCart().getPaymentType().getCode())
                && getWorldpayPaymentProvider().equals(getBaseStoreService().getCurrentBaseStore().getPaymentProvider()))
        {
            WorldpayAdditionalInfoData worldpayAdditionalInfoData = WorldpayUtil.deserializeWorldpayAdditionalInfo(securityCode);
            worldpayAdditionalInfoData.setAuthenticatedShopperId(getWorldpayAuthenticatedShopperIdStrategy()
                    .getAuthenticatedShopperId(getUserService().getCurrentUser()));
            return WorldpayUtil.serializeWorldpayAdditionalInfo(worldpayAdditionalInfoData);
        }
        return securityCode;
    }

    /**
     * Validates if the cart is ready for order submission.
     * 
     * @param placeOrderData
     *            - Submitted place order form.
     * @return - return true if validated successfully.
     */
    protected boolean isValidCheckoutCart(final PlaceOrderData placeOrderData)
    {
        //boolean valid = super.isValidCheckoutCart(placeOrderData);

        final CartData cartData = getCheckoutCart();
        final boolean valid = true;

        if (!cartData.isCalculated())
        {
            throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_NOT_CALCULATED));
        }

        if (cartData.getDeliveryAddress() == null && !getAcceleratorCheckoutFacade().hasPickUpItems())
        {
            throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_DELIVERYADDRESS_INVALID));
        }

        if (cartData.getDeliveryMode() == null)
        {
            throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_DELIVERYMODE_INVALID));
        }

        final boolean accountPaymentType = CheckoutPaymentType.ACCOUNT.getCode().equals(cartData.getPaymentType().getCode());
        if (!accountPaymentType && cartData.getPaymentInfo() == null)
        {
            throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_PAYMENTINFO_EMPTY));
        }

        if (Boolean.TRUE.equals(placeOrderData.getNegotiateQuote()) && !cartData.getQuoteAllowed())
        {
            throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_QUOTE_REQUIREMENTS_NOT_SATISFIED));
        }

        CartModel cart = getCart();
        if (Boolean.TRUE.equals(placeOrderData.getNegotiateQuote())
                && isPaypalPayment(cart.getPaymentType().getCode(), cart.getPaymentInfo()))
        {
            throw new EntityValidationException(getL10nService().getLocalizedString(
                    CART_CHECKOUT_QUOTE_REQUIREMENTS_NOT_SATISFIED));
        }
        return valid;
    }

    /**
     * Check if Paypal is the current payment method in cart.
     * 
     * @param paymentType
     *            - Checkout payment type.
     * @param paymentInfo
     *            - {@link PaymentInfoModel} to check for.
     * @return - true for Paypal payment.
     */
    private boolean isPaypalPayment(String paymentType, PaymentInfoModel paymentInfo)
    {
        return (CheckoutPaymentType.CARD.getCode().equals(paymentType)) && (paymentInfo instanceof PaypalPaymentInfoModel);
    }

    protected WorldpayAuthenticatedShopperIdStrategy getWorldpayAuthenticatedShopperIdStrategy()
    {
        return worldpayAuthenticatedShopperIdStrategy;
    }

    @Required
    public void setWorldpayAuthenticatedShopperIdStrategy(
            WorldpayAuthenticatedShopperIdStrategy worldpayAuthenticatedShopperIdStrategy)
    {
        this.worldpayAuthenticatedShopperIdStrategy = worldpayAuthenticatedShopperIdStrategy;
    }

    protected WorldpayAdditionalInfoFacade getWorldpayAdditionalInfoFacade()
    {
        return worldpayAdditionalInfoFacade;
    }

    @Required
    public void setWorldpayAdditionalInfoFacade(WorldpayAdditionalInfoFacade worldpayAdditionalInfoFacade)
    {
        this.worldpayAdditionalInfoFacade = worldpayAdditionalInfoFacade;
    }

    protected String getWorldpayPaymentProvider()
    {
        return worldpayPaymentProvider;
    }

    @Required
    public void setWorldpayPaymentProvider(String worldpayPaymentProvider)
    {
        this.worldpayPaymentProvider = worldpayPaymentProvider;
    }

    protected SiteConfigService getSiteConfigService()
    {
        return siteConfigService;
    }

    @Required
    public void setSiteConfigService(SiteConfigService siteConfigService)
    {
        this.siteConfigService = siteConfigService;
    }

    protected PayPalCheckoutFacade getPayPalCheckoutFacade()
    {
        return payPalCheckoutFacade;
    }

    @Required
    public void setPayPalCheckoutFacade(PayPalCheckoutFacade payPalCheckoutFacade)
    {
        this.payPalCheckoutFacade = payPalCheckoutFacade;
    }

    public Converter<PaypalPaymentInfoModel, CCPaymentInfoData> getDefaultPaypalPaymentInfoConverter()
    {
        return defaultPaypalPaymentInfoConverter;
    }

    public void setDefaultPaypalPaymentInfoConverter(
            Converter<PaypalPaymentInfoModel, CCPaymentInfoData> defaultPaypalPaymentInfoConverter)
    {
        this.defaultPaypalPaymentInfoConverter = defaultPaypalPaymentInfoConverter;
    }

    protected L10NService getL10nService()
    {
        return l10nService;
    }

    @Required
    public void setL10nService(L10NService l10nService)
    {
        this.l10nService = l10nService;
    }
}
