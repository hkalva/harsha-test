/**
 *
 */
package com.lyonscg.checkoutaddon.order.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.worldpay.facades.payment.WorldpayAdditionalInfoFacade;
import com.worldpay.order.data.WorldpayAdditionalInfoData;
import com.worldpay.strategy.WorldpayAuthenticatedShopperIdStrategy;
import com.worldpay.util.WorldpayUtil;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.b2b.model.B2BCommentModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.services.B2BCommentService;
import de.hybris.platform.b2bacceleratorfacades.checkout.data.PlaceOrderData;
import de.hybris.platform.b2bacceleratorfacades.exception.EntityValidationException;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.L10NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;


/**
 * Test class for B2BMultiStepPaymentBasedCheckoutFlowFacade.
 */
@UnitTest
public class B2BMultiStepPaymentBasedCheckoutFlowFacadeTest
{
    private static final String DOT = ".";
    private static final String WORLDPAY_PAYMENT_PROVIDER = "Worldpay";
    private static final String MOCKUP_PAYMENT_PROVIDER = "Mockup";
    private static final String WORLDPAY_B2B_CHECKOUT_GROUP = "worldpayB2BCheckoutGroup";
    private static final String B2B_CHECKOUT_GROUP = "b2bCheckoutGroup";
    private static final String ACC_B2B_CHECKOUT_GROUP = "accb2bCheckoutGroup";
    private static final String CHECKOUT_FLOW_GROUP = "CheckoutFlowGroup";
    private static final String PLAIN_SECURITY_CODE = "PLAIN_SECURITY_CODE";
    private static final String CART_CHECKOUT_TERM_UNCHECKED = "cart.term.unchecked";

    @InjectMocks
    private B2BMultiStepPaymentBasedCheckoutFlowFacade b2bMultiStepPaymentBasedCheckoutFlowFacade;
    @Mock
    private CartModel cart;
    @Mock
    private CartFacade cartFacade;
    @Mock
    private CartService cartService;
    @Mock
    private BaseStoreService baseStoreService;
    @Mock
    private BaseStoreModel baseStoreModel;
    @Mock
    private AcceleratorCheckoutFacade acceleratorCheckoutFacade;
    @Mock
    private SiteConfigService siteConfigService;
    @Mock
    private WorldpayAuthenticatedShopperIdStrategy worldpayAuthenticatedShopperIdStrategy;
    @Mock
    private WorldpayAdditionalInfoFacade worldpayAdditionalInfoFacade;
    @Mock
    private UserService userService;
    @Mock
    private B2BCustomerModel b2bCustomer;
    @Mock
    private DeliveryService deliveryService;
    @Mock
    private CartData cartData;
    @Mock
    private ModelService modelService;
    @Mock
    private B2BCommentModel b2bComment;
    @Mock
    private B2BCommentService<AbstractOrderModel> b2bCommentService;
    @Mock
    private CheckoutCustomerStrategy checkoutCustomerStrategy;
    @Mock
    private CommerceCheckoutService commerceCheckoutService;
    @Mock
    private CommerceOrderResult commerceOrderResult;
    @Mock
    private OrderModel order;
    @Mock
    private Converter<OrderModel, OrderData> orderConverter;
    @Mock
    private PaypalPaymentInfoModel paypalPaymentInfoModel;
    @Mock
    private L10NService l10nService;
    @Mock
    private CreditCardPaymentInfoModel cardPaymentInfoModel;
    @Mock
    private InvoicePaymentInfoModel invoicePaymentInfoModel;
    @Mock
    private Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> creditCardPaymentInfoConverter;
    @Mock
    private Converter<PaypalPaymentInfoModel, CCPaymentInfoData> defaultPaypalPaymentInfoConverter;
    private PlaceOrderData placeOrderData = new PlaceOrderData();
    private AddressData deliveryAddressData = new AddressData();
    private DeliveryModeData deliveryModeData = new DeliveryModeData();
    private CCPaymentInfoData ccPaymentInfoData = new CCPaymentInfoData();
    private B2BPaymentTypeData b2bPaymentTypeData = new B2BPaymentTypeData();

    /**
     * Setup test data.
     */
    @Before
    public void setup() throws InvalidCartException
    {
        b2bMultiStepPaymentBasedCheckoutFlowFacade = new B2BMultiStepPaymentBasedCheckoutFlowFacade();
        MockitoAnnotations.initMocks(this);
        b2bMultiStepPaymentBasedCheckoutFlowFacade.setWorldpayPaymentProvider(WORLDPAY_PAYMENT_PROVIDER);
        b2bMultiStepPaymentBasedCheckoutFlowFacade.setOrderConverter(orderConverter);

        Mockito.doReturn(cartData).when(cartFacade).getSessionCart();
        Mockito.doReturn(null).when(cart).getDeliveryAddress();
        Mockito.doReturn(null).when(cart).getDeliveryMode();
        Mockito.doReturn(null).when(cart).getPaymentInfo();
        Mockito.doReturn(b2bCustomer).when(cart).getUser();
        Mockito.doReturn(CheckoutPaymentType.CARD).when(cart).getPaymentType();

        Mockito.doReturn(true).when(cartData).isCalculated();
        Mockito.doReturn(null).when(cartData).getB2BComment();
        Mockito.doReturn(deliveryAddressData).when(cartData).getDeliveryAddress();
        Mockito.doReturn(deliveryModeData).when(cartData).getDeliveryMode();
        Mockito.doReturn(b2bPaymentTypeData).when(cartData).getPaymentType();
        Mockito.doReturn(Boolean.TRUE).when(cartData).getQuoteAllowed();
        Mockito.doReturn(b2bComment).when(modelService).create(B2BCommentModel.class);
        Mockito.doReturn(b2bCustomer).when(checkoutCustomerStrategy).getCurrentUserForCheckout();
        Mockito.doReturn(commerceOrderResult).when(commerceCheckoutService)
                .placeOrder(Mockito.any(CommerceCheckoutParameter.class));
        Mockito.doReturn(order).when(commerceOrderResult).getOrder();

        Mockito.doReturn(ccPaymentInfoData).when(cartData).getPaymentInfo();
        b2bPaymentTypeData.setCode(CheckoutPaymentType.CARD.getCode());
        placeOrderData.setNegotiateQuote(Boolean.TRUE);
        placeOrderData.setQuoteRequestDescription("Need discount");

        Mockito.doReturn(Boolean.TRUE).when(cartFacade).hasSessionCart();
        Mockito.doReturn(cart).when(cartService).getSessionCart();
        Mockito.doReturn(baseStoreModel).when(baseStoreService).getCurrentBaseStore();
        Mockito.doReturn(ACC_B2B_CHECKOUT_GROUP).when(acceleratorCheckoutFacade).getCheckoutFlowGroupForCheckout();
        placeOrderData.setTermsCheck(Boolean.TRUE);
    }

    /**
     * Unit test for {@link B2BMultiStepPaymentBasedCheckoutFlowFacade#getCheckoutFlowGroupForCheckout()} with ACCOUNT
     * and mock payment provider.
     */
    @Test
    public void getCheckoutFlowGroupForCheckoutAccMockTest()
    {
        Mockito.doReturn(CheckoutPaymentType.ACCOUNT).when(cart).getPaymentType();
        Mockito.doReturn(MOCKUP_PAYMENT_PROVIDER).when(baseStoreModel).getPaymentProvider();
        Mockito.doReturn(B2B_CHECKOUT_GROUP)
                .when(siteConfigService)
                .getProperty(
                        (CheckoutPaymentType.ACCOUNT.getCode() + DOT + MOCKUP_PAYMENT_PROVIDER + DOT + CHECKOUT_FLOW_GROUP)
                                .toLowerCase());
        Assert.assertEquals(B2B_CHECKOUT_GROUP, b2bMultiStepPaymentBasedCheckoutFlowFacade.getCheckoutFlowGroupForCheckout());
    }

    /**
     * Unit test for {@link B2BMultiStepPaymentBasedCheckoutFlowFacade#getCheckoutFlowGroupForCheckout()} with CARD and
     * mock payment provider.
     */
    @Test
    public void getCheckoutFlowGroupForCheckoutCardMockTest()
    {
        Mockito.doReturn(CheckoutPaymentType.CARD).when(cart).getPaymentType();
        Mockito.doReturn(MOCKUP_PAYMENT_PROVIDER).when(baseStoreModel).getPaymentProvider();
        Mockito.doReturn(B2B_CHECKOUT_GROUP)
                .when(siteConfigService)
                .getProperty(
                        (CheckoutPaymentType.CARD.getCode() + DOT + MOCKUP_PAYMENT_PROVIDER + DOT + CHECKOUT_FLOW_GROUP)
                                .toLowerCase());
        Assert.assertEquals(B2B_CHECKOUT_GROUP, b2bMultiStepPaymentBasedCheckoutFlowFacade.getCheckoutFlowGroupForCheckout());
    }

    /**
     * Unit test for {@link B2BMultiStepPaymentBasedCheckoutFlowFacade#getCheckoutFlowGroupForCheckout()} with ACCOUNT
     * and Worldpay payment provider.
     */
    @Test
    public void getCheckoutFlowGroupForCheckoutAccWpTest()
    {
        Mockito.doReturn(CheckoutPaymentType.ACCOUNT).when(cart).getPaymentType();
        Mockito.doReturn(WORLDPAY_PAYMENT_PROVIDER).when(baseStoreModel).getPaymentProvider();
        Mockito.doReturn(B2B_CHECKOUT_GROUP)
                .when(siteConfigService)
                .getProperty(
                        (CheckoutPaymentType.ACCOUNT.getCode() + DOT + WORLDPAY_PAYMENT_PROVIDER + DOT + CHECKOUT_FLOW_GROUP)
                                .toLowerCase());
        Assert.assertEquals(B2B_CHECKOUT_GROUP, b2bMultiStepPaymentBasedCheckoutFlowFacade.getCheckoutFlowGroupForCheckout());
    }

    /**
     * Unit test for {@link B2BMultiStepPaymentBasedCheckoutFlowFacade#getCheckoutFlowGroupForCheckout()} with CARD and
     * Worldpay payment provider.
     */
    @Test
    public void getCheckoutFlowGroupForCheckoutCardWpTest()
    {
        Mockito.doReturn(CheckoutPaymentType.CARD).when(cart).getPaymentType();
        Mockito.doReturn(WORLDPAY_PAYMENT_PROVIDER).when(baseStoreModel).getPaymentProvider();
        Mockito.doReturn(WORLDPAY_B2B_CHECKOUT_GROUP)
                .when(siteConfigService)
                .getProperty(
                        (CheckoutPaymentType.CARD.getCode() + DOT + WORLDPAY_PAYMENT_PROVIDER + DOT + CHECKOUT_FLOW_GROUP)
                                .toLowerCase());
        Assert.assertEquals(WORLDPAY_B2B_CHECKOUT_GROUP,
                b2bMultiStepPaymentBasedCheckoutFlowFacade.getCheckoutFlowGroupForCheckout());
    }

    /**
     * Unit test for {@link B2BMultiStepPaymentBasedCheckoutFlowFacade#getCheckoutFlowGroupForCheckout()} with CARD and
     * unknown payment provider.
     */
    @Test
    public void getCheckoutFlowGroupForCheckoutNoProviderTest()
    {
        Mockito.doReturn(CheckoutPaymentType.CARD).when(cart).getPaymentType();
        Mockito.doReturn("UNKNOWN").when(baseStoreModel).getPaymentProvider();
        Mockito.doReturn(null).when(siteConfigService)
                .getProperty((CheckoutPaymentType.CARD.getCode() + DOT + "UNKNOWN" + DOT + CHECKOUT_FLOW_GROUP).toLowerCase());
        Assert.assertEquals(acceleratorCheckoutFacade.getCheckoutFlowGroupForCheckout(),
                b2bMultiStepPaymentBasedCheckoutFlowFacade.getCheckoutFlowGroupForCheckout());
    }

    /**
     * Unit test for {@link B2BMultiStepPaymentBasedCheckoutFlowFacade#getCheckoutFlowGroupForCheckout()} with CARD and
     * blank payment provider.
     */
    @Test
    public void getCheckoutFlowGroupForCheckoutBlankProviderTest()
    {
        Mockito.doReturn(CheckoutPaymentType.CARD).when(cart).getPaymentType();
        Mockito.doReturn("").when(baseStoreModel).getPaymentProvider();
        Mockito.doReturn(B2B_CHECKOUT_GROUP).when(siteConfigService)
                .getProperty((CheckoutPaymentType.CARD.getCode() + DOT + CHECKOUT_FLOW_GROUP).toLowerCase());
        Assert.assertEquals(B2B_CHECKOUT_GROUP, b2bMultiStepPaymentBasedCheckoutFlowFacade.getCheckoutFlowGroupForCheckout());
    }

    /**
     * Unit test for {@link B2BMultiStepPaymentBasedCheckoutFlowFacade#getCheckoutFlowGroupForCheckout()} with CARD and
     * null payment provider.
     */
    @Test
    public void getCheckoutFlowGroupForCheckoutNullProviderTest()
    {
        Mockito.doReturn(CheckoutPaymentType.CARD).when(cart).getPaymentType();
        Mockito.doReturn(null).when(baseStoreModel).getPaymentProvider();
        Mockito.doReturn(B2B_CHECKOUT_GROUP).when(siteConfigService)
                .getProperty((CheckoutPaymentType.CARD.getCode() + DOT + CHECKOUT_FLOW_GROUP).toLowerCase());
        Assert.assertEquals(B2B_CHECKOUT_GROUP, b2bMultiStepPaymentBasedCheckoutFlowFacade.getCheckoutFlowGroupForCheckout());
    }

    /**
     * Unit test for {@link B2BMultiStepPaymentBasedCheckoutFlowFacade#serializeCvv(String)} with CARD and Worldpay
     * payment provider.
     */
    @Test
    public void serializeCvvForWorldpayCARDTest()
    {
        Mockito.doReturn(b2bCustomer).when(userService).getCurrentUser();
        Mockito.doReturn("userId").when(b2bCustomer).getUid();
        Mockito.doReturn(b2bCustomer.getUid()).when(worldpayAuthenticatedShopperIdStrategy)
                .getAuthenticatedShopperId(b2bCustomer);
        Mockito.doReturn(CheckoutPaymentType.CARD).when(cart).getPaymentType();
        Mockito.doReturn(WORLDPAY_PAYMENT_PROVIDER).when(baseStoreModel).getPaymentProvider();
        WorldpayAdditionalInfoData additionalInfoData = new WorldpayAdditionalInfoData();
        additionalInfoData.setSecurityCode(null);
        String serializedSecurityCode = WorldpayUtil.serializeWorldpayAdditionalInfo(additionalInfoData);
        String processedSecurityCode = b2bMultiStepPaymentBasedCheckoutFlowFacade.serializeCvv(serializedSecurityCode);
        WorldpayAdditionalInfoData actualAddData = WorldpayUtil.deserializeWorldpayAdditionalInfo(processedSecurityCode);
        Assert.assertEquals(b2bCustomer.getUid(), actualAddData.getAuthenticatedShopperId());

    }

    /**
     * Unit test for {@link B2BMultiStepPaymentBasedCheckoutFlowFacade#serializeCvv(String)} with ACCOUNT and Worldpay
     * payment provider.
     */
    @Test
    public void serializeCvvForWorldpayACCOUNTTest()
    {
        Mockito.doReturn(CheckoutPaymentType.ACCOUNT).when(cart).getPaymentType();
        Mockito.doReturn(WORLDPAY_PAYMENT_PROVIDER).when(baseStoreModel).getPaymentProvider();
        Assert.assertEquals(PLAIN_SECURITY_CODE, b2bMultiStepPaymentBasedCheckoutFlowFacade.serializeCvv(PLAIN_SECURITY_CODE));

    }

    /**
     * Unit test for {@link B2BMultiStepPaymentBasedCheckoutFlowFacade#serializeCvv(String)} with CARD and Mock payment
     * provider.
     */
    @Test
    public void serializeCvvForMockCARDTest()
    {
        Mockito.doReturn(CheckoutPaymentType.CARD).when(cart).getPaymentType();
        Mockito.doReturn(MOCKUP_PAYMENT_PROVIDER).when(baseStoreModel).getPaymentProvider();
        Assert.assertEquals(PLAIN_SECURITY_CODE, b2bMultiStepPaymentBasedCheckoutFlowFacade.serializeCvv(PLAIN_SECURITY_CODE));

    }

    /**
     * Unit test for {@link B2BMultiStepPaymentBasedCheckoutFlowFacade#serializeCvv(String)} with ACCOUNT and Mock
     * payment provider.
     */
    @Test
    public void serializeCvvForMockACCOUNTTest()
    {
        Mockito.doReturn(CheckoutPaymentType.ACCOUNT).when(cart).getPaymentType();
        Mockito.doReturn(MOCKUP_PAYMENT_PROVIDER).when(baseStoreModel).getPaymentProvider();
        Assert.assertEquals(PLAIN_SECURITY_CODE, b2bMultiStepPaymentBasedCheckoutFlowFacade.serializeCvv(PLAIN_SECURITY_CODE));

    }

    /**
     * Unit test for {@link B2BMultiStepPaymentBasedCheckoutFlowFacade#placeOrder()} to make sure
     * EntityValidationException is not thrown.
     */
    @Test
    public void placeOrderCARDQuoteTest() throws InvalidCartException
    {
        b2bMultiStepPaymentBasedCheckoutFlowFacade.placeOrder(placeOrderData);
        //Make sure if EntityValidationException is not thrown for Quote orders with no payment info.
    }

    /**
     * Failure unit test for {@link B2BMultiStepPaymentBasedCheckoutFlowFacade#isValidCheckoutCart(PlaceOrderData)} for
     * paypal quote.
     * 
     */
    @Test(expected = EntityValidationException.class)
    public void isValidCheckoutCartNoQuoteForPaypalTest()
    {
        placeOrderData.setNegotiateQuote(Boolean.TRUE);
        Mockito.doReturn(CheckoutPaymentType.CARD).when(cart).getPaymentType();
        Mockito.doReturn(paypalPaymentInfoModel).when(cart).getPaymentInfo();
        Mockito.doReturn(CART_CHECKOUT_TERM_UNCHECKED).when(l10nService).getLocalizedString(CART_CHECKOUT_TERM_UNCHECKED);
        b2bMultiStepPaymentBasedCheckoutFlowFacade.isValidCheckoutCart(placeOrderData);
    }

    /**
     * Unit test for {@link B2BMultiStepPaymentBasedCheckoutFlowFacade#isValidCheckoutCart(PlaceOrderData)} for
     * replenishment order with paypal payment.
     * 
     */
    public void isValidCheckoutCartReplenishmentForPaypalTest()
    {
        placeOrderData.setReplenishmentOrder(Boolean.TRUE);
        Mockito.doReturn(CheckoutPaymentType.CARD).when(cart).getPaymentType();
        Mockito.doReturn(paypalPaymentInfoModel).when(cart).getPaymentInfo();
        Mockito.doReturn(CART_CHECKOUT_TERM_UNCHECKED).when(l10nService).getLocalizedString(CART_CHECKOUT_TERM_UNCHECKED);
        Assert.assertTrue(b2bMultiStepPaymentBasedCheckoutFlowFacade.isValidCheckoutCart(placeOrderData));
    }

    /**
     * Unit test for {@link B2BMultiStepPaymentBasedCheckoutFlowFacade#isValidCheckoutCart(PlaceOrderData)} for normal
     * order.
     * 
     */
    @Test
    public void isValidCheckoutCartPaypalTest()
    {
        placeOrderData.setReplenishmentOrder(Boolean.FALSE);
        placeOrderData.setNegotiateQuote(Boolean.FALSE);
        Mockito.doReturn(CheckoutPaymentType.CARD).when(cart).getPaymentType();
        Mockito.doReturn(paypalPaymentInfoModel).when(cart).getPaymentInfo();
        Assert.assertTrue(b2bMultiStepPaymentBasedCheckoutFlowFacade.isValidCheckoutCart(placeOrderData));
    }

    /**
     * ReplenishmentOrder Unit test for
     * {@link B2BMultiStepPaymentBasedCheckoutFlowFacade#isValidCheckoutCart(PlaceOrderData)}.
     */
    @Test
    public void isValidCheckoutReplenishmentTest()
    {
        placeOrderData.setReplenishmentOrder(Boolean.TRUE);
        assertValidCheckoutCart();
    }

    /**
     * Quote Unit test for {@link B2BMultiStepPaymentBasedCheckoutFlowFacade#isValidCheckoutCart(PlaceOrderData)}.
     */
    @Test
    public void isValidCheckoutQuoteTest()
    {
        placeOrderData.setNegotiateQuote(Boolean.TRUE);
        assertValidCheckoutCart();
    }

    private void assertValidCheckoutCart()
    {
        Mockito.doReturn(CheckoutPaymentType.CARD).when(cart).getPaymentType();
        Mockito.doReturn(cardPaymentInfoModel).when(cart).getPaymentInfo();
        Assert.assertTrue(b2bMultiStepPaymentBasedCheckoutFlowFacade.isValidCheckoutCart(placeOrderData));

        Mockito.doReturn(CheckoutPaymentType.CARD).when(cart).getPaymentType();
        Mockito.doReturn(null).when(cart).getPaymentInfo();
        Assert.assertTrue(b2bMultiStepPaymentBasedCheckoutFlowFacade.isValidCheckoutCart(placeOrderData));

        Mockito.doReturn(CheckoutPaymentType.CARD).when(cart).getPaymentType();
        Mockito.doReturn(invoicePaymentInfoModel).when(cart).getPaymentInfo();
        Assert.assertTrue(b2bMultiStepPaymentBasedCheckoutFlowFacade.isValidCheckoutCart(placeOrderData));

        Mockito.doReturn(CheckoutPaymentType.ACCOUNT).when(cart).getPaymentType();
        Mockito.doReturn(cardPaymentInfoModel).when(cart).getPaymentInfo();
        Assert.assertTrue(b2bMultiStepPaymentBasedCheckoutFlowFacade.isValidCheckoutCart(placeOrderData));

        Mockito.doReturn(CheckoutPaymentType.ACCOUNT).when(cart).getPaymentType();
        Mockito.doReturn(null).when(cart).getPaymentInfo();
        Assert.assertTrue(b2bMultiStepPaymentBasedCheckoutFlowFacade.isValidCheckoutCart(placeOrderData));

        Mockito.doReturn(CheckoutPaymentType.ACCOUNT).when(cart).getPaymentType();
        Mockito.doReturn(invoicePaymentInfoModel).when(cart).getPaymentInfo();
        Assert.assertTrue(b2bMultiStepPaymentBasedCheckoutFlowFacade.isValidCheckoutCart(placeOrderData));

        Mockito.doReturn(CheckoutPaymentType.ACCOUNT).when(cart).getPaymentType();
        Mockito.doReturn(paypalPaymentInfoModel).when(cart).getPaymentInfo();
        Assert.assertTrue(b2bMultiStepPaymentBasedCheckoutFlowFacade.isValidCheckoutCart(placeOrderData));
    }
}
