/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.lyonscg.assistedserviceaddon.solrfacetsearch.processors;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SolrQueryPostProcessor;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;


public class AsmQueryPostProcessor implements SolrQueryPostProcessor
{
	private static final Logger LOG = Logger.getLogger(AsmQueryPostProcessor.class);
	private AssistedServiceFacade assistedServiceFacade;
	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Override
	public SolrQuery process(final SolrQuery solrQuery, final SearchQuery searchQuery)
	{

		// do not filter out ASM category products if AS agent is logged in
		if (getAssistedServiceFacade().isAssistedServiceAgentLoggedIn())
		{
			return solrQuery;
		}

		LOG.debug("Filter Query:"
				+ configurationService.getConfiguration().getString("lyonscgb2bassistedserviceaddon.solr.filterquery"));
		//solrQuery.addFilterQuery("-allCategories_string_mv:1500");
		solrQuery.addFilterQuery(configurationService.getConfiguration().getString(
				"lyonscgb2bassistedserviceaddon.solr.filterquery"));

		return solrQuery;
	}

	protected AssistedServiceFacade getAssistedServiceFacade()
	{
		return assistedServiceFacade;
	}

	public void setAssistedServiceFacade(final AssistedServiceFacade assistedServiceFacade)
	{
		this.assistedServiceFacade = assistedServiceFacade;
	}

}
