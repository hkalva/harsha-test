package com.capgemini.seoaddon.storefront.util;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.product.ProductModel;


/**
 * Class that extends OOTB PageTitleResolver to override the Category Page Title and Product Page Title Title is
 * restricted to the just the Category / Product name along with its Site Name In case of Product, an additional Brand
 * name is included in the Title.
 */
public class SeoPageTitleResolver extends PageTitleResolver
{
    private final static Logger LOG = LogManager.getLogger(SeoPageTitleResolver.class.getName());
    protected static final String BRAND_STRING = "brand";

    /*
     * Changed the Category page title to list only the Category name and the Site name (non-Javadoc)
     *
     * @see
     * de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver#resolveCategoryPageTitle(de.hybris.platform
     * .category.model.CategoryModel)
     */
    @Override
    public String resolveCategoryPageTitle(final CategoryModel category)
    {
        final StringBuilder stringBuilder = new StringBuilder();
        //stringBuilder.append(category.getName());
        
        final List<CategoryModel> categories = this.getCategoryPath(category);
		for (final CategoryModel c : categories)
		{
			stringBuilder.append(c.getName()).append(TITLE_WORD_SEPARATOR);
		}

        final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();
        if (currentSite != null)
        {
            stringBuilder.append(currentSite.getName());
        }

        return StringEscapeUtils.escapeHtml(stringBuilder.toString());
    }

    /*
     * Changed the Product Page title to list only the Product name, Brand name if it exists and the Site name
     * (non-Javadoc)
     *
     * @see
     * de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver#resolveProductPageTitle(de.hybris.platform
     * .core.model.product.ProductModel)
     */
    @Override
    public String resolveProductPageTitle(final ProductModel product)
    {
        // Lookup categories
        final List<CategoryModel> path = getSuperCategoriesForProduct(product);

        // Lookup site (or store)
        final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();

        // Construct page title
        final String identifier = product.getName();
        final String articleNumber = product.getCode();
        final String productName = StringUtils.isEmpty(identifier) ? articleNumber : identifier;
        final StringBuilder builder = new StringBuilder(productName);

        for (final CategoryModel pathElement : path)
        {
            LOG.info(pathElement.getName() + " : " + pathElement.getCode());
            if (pathElement.getCode() != null && pathElement.getCode().contains(BRAND_STRING))
            {
                builder.append(TITLE_WORD_SEPARATOR).append(pathElement.getName());
                break;
            }
        }

        if (currentSite != null)
        {
            builder.append(TITLE_WORD_SEPARATOR).append(currentSite.getName());
        }

        return StringEscapeUtils.escapeHtml(builder.toString());
    }

    /**
     * Retrieve the list of super categories including Brand and excluding the Classification Category
     *
     * @param product
     * @return
     */
    protected List<CategoryModel> getSuperCategoriesForProduct(final ProductModel product)
    {
        final List<CategoryModel> superCategories = new LinkedList<CategoryModel>();
        // Get the super-category from the product that isn't a classification category
        if (CollectionUtils.isNotEmpty(product.getSupercategories()))
        {
            for (final CategoryModel category : product.getSupercategories())
            {
                if (!(category instanceof ClassificationClassModel))
                {
                    superCategories.add(category);
                }
            }
        }
        return superCategories;
    }
    
	protected List<CategoryModel> getCategoryPath(final CategoryModel category)
	{
		final Collection<List<CategoryModel>> paths = getCommerceCategoryService().getPathsForCategory(category);
		// Return first - there will always be at least 1
		final List<CategoryModel> cat2ret = paths.iterator().next();
		for (Iterator<CategoryModel> it = cat2ret.iterator(); it.hasNext();) {
			   if (!it.next().getUseInCategoryPathUrl()){
			     it.remove();
			   }
			}
		Collections.reverse(cat2ret);
		return cat2ret;
	}
}
