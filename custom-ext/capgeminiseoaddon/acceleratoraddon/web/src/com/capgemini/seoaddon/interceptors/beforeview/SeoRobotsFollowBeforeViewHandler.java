/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.capgemini.seoaddon.interceptors.beforeview;

import de.hybris.platform.acceleratorservices.storefront.data.MetaElementData;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


/**
 * Custom SeoRobotsFollowBeforeViewHandler is a copy of the OOTB class
 * /<storefrontextension>/web/src/com/lyonscg/storefront/interceptors/beforeview/SeoRobotsFollowBeforeViewHandler.java
 * with changes to identify Secure pages based on a list from local.properties, since all hybris V6 pages are secure
 * HTTPS pages
 *
 * SeoRobotsFollowBeforeViewHandler has to be removed from the defaultBeforeViewHandlersList list in
 * /<storefrontextension>/web/webroot/WEB-INF/config/spring-mvc-config.xml
 *
 * Logic in this BeforeViewHandler is to not set the meta tag, if it is already set. So, if we retain the OOTB
 * beforeViewHandler, then the custom lea handler will not set any value since they were already set in storefront
 * extension's handler
 */
public class SeoRobotsFollowBeforeViewHandler implements BeforeViewHandler
{
    /**
     * constant for seoRobotsSecurePages property.
     */
    private static final String SEO_ROBOTS_SECUREPAGES = "seoRobotsSecurePages";
    
    /**
     * constant for metatags.
     */
    private static final String META_TAGS = "metatags";
    
    /**
     * constant for robots.
     */
    private static final String ROBOTS = "robots";
    
    /**
     * the configuration service.
     */
    @Resource(name = "configurationService")
    private ConfigurationService configurationService;
    
    /**
     * the robot index JSON map.
     */
	private Map<String, String> robotIndexForJSONMapping;
	
	/**
	 * the list of secure pages.
	 */
	private List<String> securePagesLst;
	
	/**
	 * Read the set of Secure pages from local.properties & create the list of Secure pages
	 */
	public void init()
	{
		if (configurationService != null)
		{
			final String securePages = configurationService.getConfiguration().getString(SEO_ROBOTS_SECUREPAGES);
			securePagesLst = StringUtils.isNotEmpty(securePages) ? Arrays.asList(securePages.split(",")) : Collections.EMPTY_LIST;
		}
	}

	@Override
	public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView)
	{
		// Check to see if the controller has specified a Index/Follow directive for robots
		if (modelAndView != null && !modelAndView.getModel().containsKey(ThirdPartyConstants.SeoRobots.META_ROBOTS))
		{
			// Build a default directive
			String robotsValue = ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW;

			if (RequestMethod.GET.name().equalsIgnoreCase(request.getMethod()))
			{
				/* HLP2-32 - All pages are Secure pages in Lea 6.0 - So, removing the condition request.isSecure() */
				//Check the custom SecurepagesLst, if its not empty Call the custom method to check if the page is secure
				if (!CollectionUtils.isEmpty(securePagesLst) && isSecurePage(request.getServletPath()))
				{
					robotsValue = ThirdPartyConstants.SeoRobots.NOINDEX_FOLLOW;
				}
				//Since no model attribute metaRobots can be set for JSON response, then configure that servlet path in the xml.
				//If its a regular response and this setting has to be overriden then set model attribute metaRobots
				else if (CollectionUtils.contains(getRobotIndexForJSONMapping().keySet().iterator(), request.getServletPath()))
				{
					robotsValue = getRobotIndexForJSONMapping().get(request.getServletPath());
				}
				else
				{
					robotsValue = ThirdPartyConstants.SeoRobots.INDEX_FOLLOW;
				}
			}
			else if (RequestMethod.POST.name().equalsIgnoreCase(request.getMethod()))
			{
				robotsValue = ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW;
			}

			modelAndView.addObject(ThirdPartyConstants.SeoRobots.META_ROBOTS, robotsValue);
		}

		if (modelAndView != null && modelAndView.getModel().containsKey(META_TAGS))
		{
		    final List<MetaElementData> metatags = ((List<MetaElementData>) modelAndView.getModel().get(META_TAGS)).stream().filter(e -> !StringUtils.equals(e.getName(), ROBOTS)).collect(Collectors.toList());
			final MetaElementData metaElement = new MetaElementData();
			metaElement.setName(ROBOTS);
			metaElement.setContent((String) modelAndView.getModel().get(ThirdPartyConstants.SeoRobots.META_ROBOTS));
			metatags.add(metaElement);
			modelAndView.getModel().put(META_TAGS, metatags);
		}
	}

	/**
	 * HLP2-32 - Checks if the requestServletPath is contained within securePagesLst If yes, it is considered as a Secure
	 * page and returns true If not, it is not a secure page and returns false
	 *
	 * @param requestServletPath
	 * @return boolean isSecurePage or Not
	 */
	protected boolean isSecurePage(final String requestServletPath)
	{
		final Optional<String> found = securePagesLst.stream().filter(e -> requestServletPath.contains(e)).findFirst();
		return found.isPresent();
	}

	protected Map<String, String> getRobotIndexForJSONMapping()
	{
		return robotIndexForJSONMapping;
	}

	public void setRobotIndexForJSONMapping(final Map<String, String> robotIndexForJSONMapping)
	{
		this.robotIndexForJSONMapping = robotIndexForJSONMapping;
	}
}
