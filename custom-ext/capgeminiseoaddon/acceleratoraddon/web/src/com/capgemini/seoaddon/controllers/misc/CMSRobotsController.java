/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.capgemini.seoaddon.controllers.misc;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;


/**
 * Controller for web robots instructions
 */
@RequestMapping(value = "/robots.txt")
public class CMSRobotsController extends AbstractPageController
{
    // Number of seconds in one day
    private static final String ONE_DAY = String.valueOf(60 * 60 * 24);
    private static final String OVERRIDE_MISC_ROBOTS_PAGE = "addon:/capgeminiseoaddon/pages/misc/miscRobotsPage";
    private static final Logger LOG = LogManager.getLogger(CMSRobotsController.class);
    private String robotsPageIdConfig;

    @RequestMapping(method = RequestMethod.GET)
    public String getRobots(final HttpServletResponse response, final Model model) throws CMSItemNotFoundException
    {
        // Add cache control header to cache response for a day
        response.setHeader("Cache-Control", "public, max-age=" + ONE_DAY);
        ContentPageModel roboPage = getContentPageForLabelOrId(getSiteConfigService().getProperty(getRobotsPageIdConfig()));
        storeCmsPageInModel(model, roboPage);
        LOG.info("Rendering robots page from cms page " + roboPage);
        return getViewForPage(roboPage);
    }

    @Override
    protected String getViewForPage(final AbstractPageModel page)
    {
        if (page != null)
        {
            final PageTemplateModel masterTemplate = page.getMasterTemplate();
            if (masterTemplate != null)
            {
                final String targetPage = getCmsPageService().getFrontendTemplateName(masterTemplate);
                if (StringUtils.isNotBlank(targetPage))
                {
                    return targetPage;
                }
            }
        }
        return OVERRIDE_MISC_ROBOTS_PAGE;
    }

    protected String getRobotsPageIdConfig()
    {
        return robotsPageIdConfig;
    }

    @Required
    public void setRobotsPageIdConfig(String robotsPageIdConfig)
    {
        this.robotsPageIdConfig = robotsPageIdConfig;
    }

}
