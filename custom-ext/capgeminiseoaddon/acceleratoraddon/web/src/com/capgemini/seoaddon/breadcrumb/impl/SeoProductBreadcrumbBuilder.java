package com.capgemini.seoaddon.breadcrumb.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.capgemini.seoservices.dao.NavigationDaoImpl;
import com.capgemini.seoservices.url.impl.SeoProductModelUrlResolver;

import de.hybris.platform.acceleratorcms.model.components.NavigationBarComponentModel;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;


/**@author lyonscg.
 *ProductBreadcrumbBuilder to include specific categories in the URL.
 */
public class SeoProductBreadcrumbBuilder extends ProductBreadcrumbBuilder
{
	private static final Logger LOG = LogManager.getLogger(SeoProductBreadcrumbBuilder.class);
	private static final String LAST_LINK_CLASS = "active";
	private final String USE_NAVIGATION_NODE_PATHING = "use.navigation.node.based.path";

	@Resource(name = "siteConfigService")
	private SiteConfigService siteConfigService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	/**
	 * @param commonI18NService
	 */
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	@Resource(name = "navigationDao")
	private NavigationDaoImpl navigationDao;

	/**
	 * @param navigationDao
	 */
	public void setNavigationDao(final NavigationDaoImpl navigationDao)
	{
		this.navigationDao = navigationDao;
	}

	@Resource(name = "productModelUrlResolver")
	private SeoProductModelUrlResolver seoProductModelUrlResolver;

	/**
	 * @param seoProductModelUrlResolver
	 */
	public void setSeoProductModelUrlResolver(final SeoProductModelUrlResolver seoProductModelUrlResolver)
	{
		this.seoProductModelUrlResolver = seoProductModelUrlResolver;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder#getBreadcrumbs(java.lang
	 * .String)
	 */
	@Override
	public List<Breadcrumb> getBreadcrumbs(final String productCode)
	{
		final ProductModel productModel = getProductService().getProductForCode(productCode);
		List<Breadcrumb> breadcrumbs = new ArrayList<>();

		if (productModel != null)
		{
			breadcrumbs = getBreadcrumbs(productModel);
		}

		return breadcrumbs;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder#getBreadcrumbs(de.hybris
	 * .platform.core.model.product.ProductModel)
	 */
	
	public List<Breadcrumb> getBreadcrumbs(final ProductModel productModel) throws IllegalArgumentException
	{
		final List<Breadcrumb> breadcrumbs = new ArrayList<>();

		final Breadcrumb last;
		final ProductModel baseProductModel = getBaseProduct(productModel);
		last = getProductBreadcrumb(baseProductModel);
		last.setLinkClass(LAST_LINK_CLASS);
		breadcrumbs.add(last);

		final boolean useNavNodePath = BooleanUtils.toBoolean(siteConfigService.getProperty(USE_NAVIGATION_NODE_PATHING));
		LOG.info("useNavNodePath : " + useNavNodePath);
		final Locale locale = commonI18NService.getLocaleForLanguage(commonI18NService.getCurrentLanguage());

		if (useNavNodePath)
		{
			buildBreadcrumbsByNavigationPath(breadcrumbs, baseProductModel, locale);
		}
		else
		{
			buildBreadcrumbsByCategoryPath(breadcrumbs, baseProductModel);
		}

		Collections.reverse(breadcrumbs);
		return breadcrumbs;
	}

	/**
	 * Generate bread crumbs based on the category path of the product's super categories
	 *
	 * @param breadcrumbs
	 *           list of breadCrumbs to which the new ones to be added
	 * @param baseProductModel
	 *           product for which the breadCrumb is to be built
	 * @param locale
	 *           locale in which the breadCrumbs has to be rendered
	 */
	protected void buildBreadcrumbsByCategoryPath(final List<Breadcrumb> breadcrumbs, final ProductModel baseProductModel)
	{
		final Collection<CategoryModel> categoryModels = new ArrayList<>();
		categoryModels.addAll(baseProductModel.getSupercategories());
		while (!categoryModels.isEmpty())
		{
			CategoryModel toDisplay = null;
			for (final CategoryModel categoryModel : categoryModels)
			{
				if (!(categoryModel instanceof ClassificationClassModel))
				{
					if (toDisplay == null)
					{
						toDisplay = categoryModel;
					}
					if (getBrowseHistory().findEntryMatchUrlEndsWith(categoryModel.getCode()) != null)
					{
						break;
					}
				}
			}
			categoryModels.clear();
			if (toDisplay != null)
			{
				if (BooleanUtils.isTrue(toDisplay.getUseInCategoryBreadcrumb()))
				{
					breadcrumbs.add(getCategoryBreadcrumb(toDisplay));
					categoryModels.addAll(toDisplay.getSupercategories());
				}
			}
		}
	}

	/**
	 * Returns the bread crumbs list from the navigation path to the primary category of the selected product
	 *
	 * @param breadcrumbs
	 *           list of breadCrumbs to which the new ones to be added
	 * @param baseProductModel
	 *           product for which the breadCrumb is to be built
	 * @param locale
	 *           locale in which the breadCrumbs has to be rendered
	 */
	protected void buildBreadcrumbsByNavigationPath(final List<Breadcrumb> breadcrumbs, final ProductModel baseProductModel,
			final Locale locale)
	{
		final CategoryModel category = seoProductModelUrlResolver.getPrimaryCategoryForProduct(baseProductModel);

		buildCategoryBreadcrumbsByNavPath(breadcrumbs, category, locale, null);
	}

	/**
	 * build bread crumbs by using the navigation path method and with category model as the input
	 *
	 * @param breadcrumbs
	 *           list of breadCrumbs to which the new ones to be added
	 * @param category
	 *           Category model object for which breadCrumbs are to be built
	 * @param locale
	 *           locale in which the breadCrumbs has to be rendered
	 * @param linkClass
	 *           link class active or null for the style
	 */
	protected void buildCategoryBreadcrumbsByNavPath(final List<Breadcrumb> breadcrumbs, final CategoryModel category,
			final Locale locale, final String linkClass)
	{
		// find any CMS Link Components where the category code is the code for this category
		final List<CMSLinkComponentModel> cmsLinks = category.getLinkComponents();

		final List<NavigationBarComponentModel> navBarLinks = navigationDao.findAllNavigationBarLinksForGivingCategory(category);

		LOG.debug("Here are the following links for category: " + category.getCode());

		if (CollectionUtils.isNotEmpty(navBarLinks))
		{
			buildBreadcrumbForNavBarLinks(breadcrumbs, navBarLinks, locale, linkClass);
		}
		else if (CollectionUtils.isNotEmpty(cmsLinks))
		{
			buildBreadcrumbForCategoryCompLinks(breadcrumbs, cmsLinks, locale, linkClass);
		}
	}

	/**
	 * Build bread crumbs using the Category's CMSComponentLink Model. Iterate through the navigation nodes available for
	 * this link and determine the navigation path for breabCrumb
	 *
	 * @param breadcrumbs
	 *           list of breadCrumbs to which the new ones to be added
	 * @param cmsLinks
	 *           CMSLinkComponents of a Category
	 * @param locale
	 *           locale in which the breadCrumbs has to be rendered
	 * @param linkClass
	 *           link class active or null for the style
	 */
	private void buildBreadcrumbForCategoryCompLinks(final List<Breadcrumb> breadcrumbs,
			final List<CMSLinkComponentModel> cmsLinks, final Locale locale, final String linkClass)
	{
		if (CollectionUtils.isNotEmpty(cmsLinks))
		{
			for (final CMSLinkComponentModel link : cmsLinks)
			{
				final List<CMSNavigationNodeModel> navigationNodes = link.getNavigationNodes();
				final List<CMSNavigationNodeModel> navigationList = new ArrayList<CMSNavigationNodeModel>();

				addBreadcrumbWithCmsLinkComponent(breadcrumbs, null, link, locale, linkClass);

				if (!CollectionUtils.isEmpty(navigationNodes))
				{
					//Retrieve all the parent navigation nodes
					for (final CMSNavigationNodeModel nav : navigationNodes)
					{
						CMSNavigationNodeModel parent = nav.getParent();
						navigationList.add(nav);
						while (parent != null)
						{
							navigationList.add(parent);
							parent = parent.getParent();
						}
					}
					Collections.reverse(navigationList);

					for (final CMSNavigationNodeModel item : navigationList)
					{
						final String title = item.getTitle(locale);
						LOG.debug("Navigation Node: " + item.getUid() + " has title of: " + title
								+ " and getUseInBreadcrumbUrl value of: " + BooleanUtils.toBoolean(item.getUseInBreadcrumbUrl()));

						if (StringUtils.isNotEmpty(title) && BooleanUtils.toBoolean(item.getUseInBreadcrumbUrl()))
						{

							// Add bread crumbs URL for the category here or retrieve the category code
							final CMSLinkComponentModel cmsLinkForNavNode = navigationDao.findCMSLinkComponentForNavigationNode(item);
							addBreadcrumbWithCmsLinkComponent(breadcrumbs, title, cmsLinkForNavNode, locale, null);
						}
					}
				}
			}
		}
	}

	/**
	 * Check if Navigation Bar Component has a link, If yes, utilize it for building the bread crumb
	 *
	 * @param breadcrumbs
	 *           list of breadCrumbs to which the new ones to be added
	 * @param navBarComponentLst
	 *           NavigationBarComponentModels retrieved for the given category
	 * @param locale
	 *           locale in which the breadCrumbs has to be rendered
	 * @param linkClass
	 *           link class active or null for the style
	 */
	private void buildBreadcrumbForNavBarLinks(final List<Breadcrumb> breadcrumbs,
			final List<NavigationBarComponentModel> navBarComponentLst, final Locale locale, final String linkClass)
	{
		String breadCrumbName;
		// we are just going to take the last one because, with an assumption that there is only one bar component for each category.
		// if we find more than one here, that means that there are several navigation bar components that point to the same category
		NavigationBarComponentModel navBarCompModel = null;
		for (final NavigationBarComponentModel model : navBarComponentLst)
		{
			navBarCompModel = model;
		}

		if (navBarCompModel != null && navBarCompModel.getLink() != null)
		{
			breadCrumbName = navBarCompModel.getNavigationNode().getTitle(locale);
			LOG.debug("breadCrumbName :" + breadCrumbName);

			final CMSLinkComponentModel navLinkCompModel = navBarCompModel.getLink();

			addBreadcrumbWithCmsLinkComponent(breadcrumbs, breadCrumbName, navLinkCompModel, locale, linkClass);
		}
	}

	/**
	 * Create bread crumbs with CMSLinkComponent. If cmsLinkComponent had category object, build a bread crumb with the
	 * category object and add it to bread crumb If not, check whether the cmsLinkComponent has URL defined, if yes build
	 * it with the provided URL and add it to the bread crumb
	 *
	 * @param breadcrumbs
	 *           list of breadCrumbs to which the new ones to be added
	 * @param breadcrumbName
	 *           BreadCrumb name to be used in creating it
	 * @param cmsLinkComponent
	 *           CMSLinkComponent which has Category object, URL attributes that could be used to build breadCrumb
	 * @param locale
	 *           locale in which the breadCrumbs has to be rendered
	 * @param linkClass
	 *           link class active or null for the style
	 */
	private void addBreadcrumbWithCmsLinkComponent(final List<Breadcrumb> breadcrumbs, final String breadcrumbName,
			final CMSLinkComponentModel cmsLinkComponent, final Locale locale, final String linkClass)
	{
		if (cmsLinkComponent != null)
		{
			LOG.debug("cmsLinkComponent :getCategoryCode :  " + cmsLinkComponent.getCategoryCode());
			LOG.debug("cmsLinkComponent :getCategoryCode :  " + cmsLinkComponent.getCategory());
			LOG.debug("cmsLinkComponent :getLinkName(locale) : " + cmsLinkComponent.getLinkName(locale));
			LOG.debug("cmsLinkComponent :getUrl: " + cmsLinkComponent.getUrl());

			if (cmsLinkComponent.getCategory() != null)
			{
				breadcrumbs.add(getCategoryBreadcrumb(cmsLinkComponent.getCategory(), linkClass));
			}
			else if (StringUtils.isNotEmpty(cmsLinkComponent.getUrl()))
			{
				if (breadcrumbName != null)
				{
					breadcrumbs.add(new Breadcrumb(cmsLinkComponent.getUrl(), breadcrumbName, null));
				}
				else
				{
					breadcrumbs.add(new Breadcrumb(cmsLinkComponent.getUrl(), cmsLinkComponent.getLinkName(locale), null));
				}
			}
		}
	}

	/**
	 * To generate a breabCrumb based on the Link class passed into it. This was specifically implemented, as the Search
	 * Bread crumb builder needs to set it appropriately
	 *
	 * @param category
	 *           Category for which breadCrumb to be built
	 * @param linkClass
	 *           linkClass that is to be used for the breadCrumb
	 * @return bread crumb object with all the parameters set
	 */
	protected Breadcrumb getCategoryBreadcrumb(final CategoryModel category, final String linkClass)
	{
		final String categoryUrl = getCategoryModelUrlResolver().resolve(category);
		return new Breadcrumb(categoryUrl, category.getName(), linkClass, category.getCode());
	}
}
