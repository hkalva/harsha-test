package com.capgemini.seoaddon.breadcrumb.impl;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.SearchBreadcrumbBuilder;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;

import org.apache.commons.lang.BooleanUtils;


/**@author lyonscg.
 *SearchBreadcrumbBuilder to include specific category breadcrumbs.
 */
public class SeoSearchBreadcrumbBuilder extends SearchBreadcrumbBuilder
{
	protected static final String LAST_LINK_CLASS = "active";
	private static final String USE_NAVIGATION_NODE_PATHING = "use.navigation.node.based.path";

	@Resource(name = "productBreadcrumbBuilder")
	private SeoProductBreadcrumbBuilder productBreadcrumbBuilder;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "siteConfigService")
	private SiteConfigService siteConfigService;

	/**
	 * @param commonI18NService
	 */
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	/**
	 * @param productBreadcrumbBuilder
	 */
	public void setProductBreadcrumbBuilder(final SeoProductBreadcrumbBuilder productBreadcrumbBuilder)
	{
		this.productBreadcrumbBuilder = productBreadcrumbBuilder;
	}

	@Override
	public List<Breadcrumb> getBreadcrumbs(final String categoryCode, final String searchText, final boolean emptyBreadcrumbs)
			throws IllegalArgumentException
	{
		final List<Breadcrumb> breadcrumbs = new ArrayList<>();

		if (categoryCode == null)
		{
			final Breadcrumb breadcrumb = new Breadcrumb("/search?text=" + getEncodedUrl(searchText), searchText,
					(emptyBreadcrumbs ? LAST_LINK_CLASS : ""));
			breadcrumbs.add(breadcrumb);
		}
		else
		{
			// Create category hierarchy path for bread crumb
			final List<Breadcrumb> categoryBreadcrumbs = new ArrayList<>();

			final CategoryModel lastCategoryModel = getCommerceCategoryService().getCategoryForCode(categoryCode);

			final boolean useNavNodePath = BooleanUtils.toBoolean(siteConfigService.getProperty(USE_NAVIGATION_NODE_PATHING));
			if (useNavNodePath)
			{
				//call product bread crumb builder's build category buildCategoryBreadcrumbsByNavPath method
				final Locale locale = commonI18NService.getLocaleForLanguage(commonI18NService.getCurrentLanguage());
				productBreadcrumbBuilder.buildCategoryBreadcrumbsByNavPath(categoryBreadcrumbs, lastCategoryModel, locale,
						(!emptyBreadcrumbs ? LAST_LINK_CLASS : null));
			}
			else
			{
				if (BooleanUtils.isTrue(lastCategoryModel.getUseInCategoryBreadcrumb()))
				{
					categoryBreadcrumbs.add(getCategoryBreadcrumb(lastCategoryModel, (!emptyBreadcrumbs ? LAST_LINK_CLASS : "")));
				}

				final Collection<CategoryModel> categoryModels = new ArrayList<>();
				categoryModels.addAll(lastCategoryModel.getSupercategories());
				while (!categoryModels.isEmpty())
				{
					final CategoryModel categoryModel = categoryModels.iterator().next();

					if (!(categoryModel instanceof ClassificationClassModel))
					{
						if (categoryModel != null)
						{
							if (BooleanUtils.isTrue(categoryModel.getUseInCategoryBreadcrumb()))
							{
								categoryBreadcrumbs.add(getCategoryBreadcrumb(categoryModel));
							}
							categoryModels.clear();
							categoryModels.addAll(categoryModel.getSupercategories());
						}
					}
					else
					{
						categoryModels.remove(categoryModel);
					}
				}
			}
			Collections.reverse(categoryBreadcrumbs);
			breadcrumbs.addAll(categoryBreadcrumbs);
		}
		return breadcrumbs;
	}
}
