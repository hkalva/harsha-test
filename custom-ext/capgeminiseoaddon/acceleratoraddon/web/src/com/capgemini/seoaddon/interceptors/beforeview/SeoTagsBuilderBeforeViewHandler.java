/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Capgemini
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Capgemini.
 */
package com.capgemini.seoaddon.interceptors.beforeview;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;

import com.capgemini.seoaddon.beans.HrefLangBean;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.pages.ProductPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.store.services.BaseStoreService;


/**
 * Before View Handler to set the SEO Canonical, and Alternate URL parameter and set it in the modelAndViewObject.
 */
public class SeoTagsBuilderBeforeViewHandler implements BeforeViewHandler
{
    /**
     * the logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(SeoTagsBuilderBeforeViewHandler.class);

    /**
     * the common i18n service.
     */
    @Resource(name = "commonI18NService")
    private CommonI18NService commonI18NService;

    /**
     * the base store service.
     */
    @Resource(name = "baseStoreService")
    private BaseStoreService baseStoreService;

    /**
     * the cms site service.
     */
    @Resource(name = "cmsSiteService")
    private CMSSiteService cmsSiteService;

    /*
     * (non-Javadoc)
     *
     * @see
     * de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler#beforeView(javax.servlet.http.
     * HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.web.servlet.ModelAndView)
     */
    @Override
    public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView)
            throws Exception
    {
        final AbstractPageModel requestedPage = (AbstractPageModel) modelAndView.getModel()
                .get(AbstractPageController.CMS_PAGE_MODEL);

        //Set Canonical URL
        buildSEOCanonicalAndHrefLangURL(requestedPage, request, modelAndView);
    }

    /**
     * To derive the Canonical and Alternate URL for Search engines. Always set the locale to be en for Canonical URL,
     * this is to avoid duplicate URLs for the same pages
     *
     * @param cmsPage
     *            AbstractPageModel for every page being rendered
     * @param request
     *            HttpServletRequest object
     * @param modelAndView
     *            Canonical and alternate URLs are set within this object
     */
    protected void buildSEOCanonicalAndHrefLangURL(final AbstractPageModel cmsPage, final HttpServletRequest request,
            final ModelAndView modelAndView)
    {
        if (cmsPage == null || request == null || modelAndView == null)
        {
            return;
        }

        if (cmsPage instanceof ContentPageModel || cmsPage instanceof CategoryPageModel || cmsPage instanceof ProductPageModel)
        {
            final List<HrefLangBean> hrefLangEntries = new ArrayList<>();
            modelAndView.addObject("hreflangEntries", hrefLangEntries);

            LOG.debug("getRequestURL: {}", request.getRequestURL());

            final Locale currentLocale = commonI18NService.getLocaleForLanguage(commonI18NService.getCurrentLanguage());

            LOG.debug("currentLocale: {}, toString: {}, toLanguageTag: {}", currentLocale, currentLocale.toString(), currentLocale.toLanguageTag());

            //SEO : If current locale is not English, replace it to be English in the Canonical Link URL
            final String canonicalURL = StringUtils.defaultIfBlank((String) modelAndView.getModelMap().get("canonicalURL"),
                    request.getRequestURL().toString());
            final String enLocale = new StringBuilder("/").append(Locale.ENGLISH.toString()).append("/").toString();

            LOG.debug("Canonical URL: {}", canonicalURL);

            modelAndView.addObject("canonicalURL_en", canonicalURL);
            final HrefLangBean hrefLang = new HrefLangBean();
            hrefLang.setHreflang("x-default");
            hrefLang.setHref(canonicalURL);
            hrefLangEntries.add(hrefLang);
            populateOtherLocales(canonicalURL, enLocale, hrefLangEntries);
        }
    }

    /**
     * Alternate URL generation for all available languages. Iterate through the list of languages associated to the
     * base store and generate the locale specific canonical URLs.
     *
     * @param canonicalURL
     *            - the canonical url.
     * @param enLocale
     *            - the english locale.
     * @param hrefLangEntries
     *            - the href entries.
     */
    protected void populateOtherLocales(final String canonicalURL, final String enLocale, final List<HrefLangBean> hrefLangEntries)
    {
        final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
        final Set<LanguageModel> languages = baseStoreService.getCurrentBaseStore().getLanguages();

        for (final LanguageModel lang : languages)
        {
            final Locale loc = commonI18NService.getLocaleForLanguage(lang);
            final String locale = loc.toString();
            final String locale_key = currentSite.getLocale(loc).replace('_', '-');
            final String toBeLocale = new StringBuilder("/").append(locale).append("/").toString();
            final String canonicalURL_locale = canonicalURL.replace(enLocale, toBeLocale);
            final String canonicalURLParam = new StringBuilder("canonicalURL_").append(locale).toString();

            LOG.debug("Locale Key: {}, canonicalURLParam: {}, canonicalURL_locale: {}", locale_key, canonicalURLParam, canonicalURL_locale);

            final HrefLangBean hrefLang = new HrefLangBean();
            hrefLang.setHreflang(locale_key);
            hrefLang.setHref(canonicalURL_locale);
            hrefLangEntries.add(hrefLang);
        }
    }
}
