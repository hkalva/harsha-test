<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ page contentType="text/plain" language="java" trimDirectiveWhitespaces="true" %>
<cms:pageSlot position="main" var="feature">
    ${feature.content}
</cms:pageSlot>