<%--Canonical and HrefLang tag implementation, Values are populated from the SeoTagsBuilderBeforeViewHandler --%>
<%--seoTags.tag file has to be added into the master.tag file so, that every page on the site has these link tags rendered--%>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${not empty canonicalURL_en}">
	<link rel="canonical" href="${canonicalURL_en}" />
</c:if>

<c:if test="${not empty hreflangEntries}">
	<c:forEach items="${hreflangEntries}" var="hreflangEntry">
		<link rel="alternate" hreflang="${hreflangEntry.hreflang}" href="${hreflangEntry.href}" />
	</c:forEach>
</c:if>
