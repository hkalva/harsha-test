<%--Rich Snippet Script to add in the Store details page. --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="application/ld+json">
{
	"@context": "http://schema.org",
	"@type": "Store",
	
	<c:if test="${not empty store.openingHours}">
	"openingHours": [
		<c:forEach items="${store.openingHours.weekDayOpeningList}" var="weekDay" varStatus="stat">
			<c:if test="${not weekDay.closed}">
				<c:set var="days" value="${weekDay.weekDay}" />
				<c:set var="hours" value=" ${weekDay.openingTime.formattedHour} - ${weekDay.closingTime.formattedHour}" />
				 "${days} ${hours}"${!stat.last ? ',' : ''}
			</c:if>
		</c:forEach>
		],
	</c:if>
	<c:if test="${not empty store.address.line1 || not empty store.address.line2 ||
				  not empty store.address.town || not empty store.address.country.name ||
				  not empty store.address.postalCode}">

	"address": {
		"@type": "PostalAddress",
		"streetAddress": "${store.address.line1} ${store.address.line2}",
		"addressLocality": "${store.address.town}",
		"postalCode": "${store.address.postalCode}",
		"addressCountry": "${store.address.country.name}"
	},
	"telephone": "${store.address.phone}",

	</c:if>

	<c:if test="${not empty store.address.email}">
	"email:" "mailto:store.address.email",
	</c:if>

	"name": "${store.displayName}",
	
	<c:set value="${ycommerce:storeImage(store, 'cartIcon')}" var="storeImage"/>
	<c:if test="${not empty storeImage}" >
		<c:set var="req" value="${pageContext.request}" />
		<c:set var="scheme" value="${req.scheme}" />
		<c:set var="serverName" value="${req.serverName}" />
		<c:set var="serverPort" value="${req.serverPort}" />
		<spring:eval expression="T(de.hybris.platform.util.Config).getParameter('DEFAULT_PORTS_1')" var="DEFAULT_PORTS_1" scope="page" />
		<spring:eval expression="T(de.hybris.platform.util.Config).getParameter('DEFAULT_PORTS_2')" var="DEFAULT_PORTS_2" scope="page" />
		<c:choose>
			<c:when test="${serverPort eq DEFAULT_PORTS_1 || serverPort eq DEFAULT_PORTS_2 }">
				<c:set value="${scheme}://${serverName}" var="siteUrl" />
			</c:when>
			<c:otherwise>
				<c:set value="${scheme}://${serverName}:${serverPort}" var="siteUrl" />
			</c:otherwise>
		</c:choose>

		"image": "${siteUrl}${storeImage.url}"
	</c:if>
}
</script>
