<%--This tag file is used to generate the Rich Snippet for the Product Details page --%>
<%--It has to be added within the <div class="product-details"> section of productDetailsPanel.tag file in the storefront extension--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:set var="req" value="${pageContext.request}" />
<c:set var="scheme" value="${req.scheme}" />
<c:set var="serverName" value="${req.serverName}" />
<c:set var="serverPort" value="${req.serverPort}" />
<spring:eval expression="T(de.hybris.platform.util.Config).getParameter('DEFAULT_PORTS_1')" var="DEFAULT_PORTS_1" scope="page" />
<spring:eval expression="T(de.hybris.platform.util.Config).getParameter('DEFAULT_PORTS_2')" var="DEFAULT_PORTS_2" scope="page" />
<c:choose>
	<c:when test="${serverPort eq DEFAULT_PORTS_1 || serverPort eq DEFAULT_PORTS_2 }">
		<c:set value="${scheme}://${serverName}" var="siteUrl" />
	</c:when>
	<c:otherwise>
		<c:set value="${scheme}://${serverName}:${serverPort}" var="siteUrl" />
	</c:otherwise>
</c:choose>

<c:set value="${ycommerce:productImage(product, 'zoom')}" var="primaryImage" />

<script type="application/ld+json">
{
	"@context": "http://schema.org/",
	"@type": "Product",
	"description": "<spring:escapeBody javaScriptEscape="true">${product.summary}</spring:escapeBody>",

	<c:if test="${not empty product.averageRating}">

	"aggregateRating": {
	"@type": "AggregateRating",
	"ratingValue": "${product.averageRating}",
	"reviewCount": "${fn:length(product.reviews)}"
	},
	</c:if>

	<c:choose>
		<c:when test="${empty product.volumePrices}">
			<c:choose>
				<c:when test="${(not empty product.priceRange) and (product.priceRange.minPrice.value ne product.priceRange.maxPrice.value) and ((empty product.baseProduct) or (not empty isOrderForm and isOrderForm))}">

					"offers": {
						"@type": "AggregateOffer",
						"priceCurrency": "${product.price.currencyIso}",
						"lowPrice": "${product.priceRange.minPrice}",
						"highPrice": "${product.priceRange.maxPrice}"
					},
				</c:when>
				<c:otherwise>
					"offers": {
						"@type": "Offer",
						"priceCurrency": "${product.price.currencyIso}",
						"price": "${product.price.value}"
					},
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:otherwise>

			"offers": [

			<c:forEach var="volPrice" items="${product.volumePrices}" varStatus="stat">
				{
				"eligibleQuantity": {
					<c:if test="${not empty volPrice.maxQuantity}">
					"maxValue": "${volPrice.maxQuantity}",
					</c:if>
					"minValue": "${volPrice.minQuantity}"
				},

				"price": "${volPrice.value}",
				"priceCurrency": "${product.price.currencyIso}",
				"@type": "Offer"
				}${!stat.last ? ',' : ''}

			</c:forEach>
			],

		</c:otherwise>
	</c:choose>

	"name": "<spring:escapeBody javaScriptEscape="true">${product.name}</spring:escapeBody>",
	"image": "${siteUrl}${primaryImage.url}"

}
</script>
