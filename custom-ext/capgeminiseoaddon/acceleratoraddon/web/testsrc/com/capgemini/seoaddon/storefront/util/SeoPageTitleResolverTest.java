/**
 *
 */
package com.capgemini.seoaddon.storefront.util;

import static org.junit.Assert.assertNotSame;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;

import java.util.Arrays;
import java.util.Collections;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * To test the public methods of SeoPageTitleREsolver.
 */
@UnitTest
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(
{ LogRunListener.class })
public class SeoPageTitleResolverTest
{

	/**
	 * Logger for SeoPageTitleResolverTest class.
	 */
	private static final Logger LOG = Logger.getLogger(SeoPageTitleResolverTest.class);

	/**
	 * SeoPageTitleResolver object to invoke the class being tested.
	 */
	@InjectMocks
	private final SeoPageTitleResolver seoPageTitleResolver = new SeoPageTitleResolver();

	/** Mocked Test category models. */
	@Mock
	private CategoryModel leafCategory, brandCategory1;
	@Mock
	private ProductModel productModel1, productModel2, productModel3;
	@Mock
	private CMSSiteModel currentCmsSite;
	@Mock
	private CMSSiteService cmsSiteService;

	private static final String LEAF_CAT_NAME = "leafcategoryName";
	private static final String BRAND_1_CAT_NAME = "brand1CategoryName";

	private static final String LEAF_CAT_CODE = "leafCategoryCode";
	private static final String BRAND_1_CAT_CODE = "brand1CategoryCode";

	private static final String BRAND_NAME_1 = "brandName1";

	private static final String PRODUCT_NAME_1 = "productName1";
	private static final String PRODUCT_NAME_2 = "productName2";
	private static final String PRODUCT_NAME_3 = "productName3";

	private static final String PRODUCT_CODE_1 = "productCode1";
	private static final String PRODUCT_CODE_2 = "productCode2";
	private static final String PRODUCT_CODE_3 = "productCode3";


	private static final String SITE_NAME = "siteName";
	private static final String TITLE_WORD_SEPARATOR_PIPE = " | ";

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

		LOG.info("SeoPageTitleResolverTest : setup");

		given(leafCategory.getName()).willReturn(LEAF_CAT_NAME);
		given(leafCategory.getCode()).willReturn(LEAF_CAT_CODE);

		given(brandCategory1.getName()).willReturn(BRAND_1_CAT_NAME);
		given(brandCategory1.getCode()).willReturn(BRAND_1_CAT_CODE);

		given(productModel1.getName()).willReturn(PRODUCT_NAME_1);
		given(productModel1.getCode()).willReturn(PRODUCT_CODE_1);
		given(productModel1.getSupercategories()).willReturn(Arrays.asList(leafCategory, brandCategory1));

		given(productModel2.getName()).willReturn(PRODUCT_NAME_2);
		given(productModel2.getCode()).willReturn(PRODUCT_CODE_2);
		given(productModel2.getSupercategories()).willReturn(Collections.singletonList(leafCategory));

		given(productModel3.getName()).willReturn(PRODUCT_NAME_3);
		given(productModel3.getCode()).willReturn(PRODUCT_CODE_3);

		given(cmsSiteService.getCurrentSite()).willReturn(currentCmsSite);

		given(currentCmsSite.getName()).willReturn(SITE_NAME);
	}

	/**
	 * To test if the CategoryPageTitle method for the mock category returns the expected value
	 */
	@Test
	public void testResolveCategoryPageTitle()
	{
		final String expectedCatPageTitle = new StringBuilder(leafCategory.getName()).append(TITLE_WORD_SEPARATOR_PIPE)
				.append(SITE_NAME).toString();
		final String assertMessage = new StringBuilder("Expected Category Page Title is :").append(expectedCatPageTitle).toString();

		final String actualCatPageTitle = seoPageTitleResolver.resolveCategoryPageTitle(leafCategory);

		assertNotSame(assertMessage, expectedCatPageTitle, actualCatPageTitle);
	}

	/**
	 * To test the Product page title - Positive case
	 */
	@Test
	public void testResolveProductPageTitleWithBrand()
	{
		final String expectedProductPageTitle1 = new StringBuilder(productModel1.getName()).append(TITLE_WORD_SEPARATOR_PIPE)
				.append(BRAND_NAME_1).append(TITLE_WORD_SEPARATOR_PIPE).append(SITE_NAME).toString();
		final String assertMessage = new StringBuilder("Expected Product Page Title is :").append(expectedProductPageTitle1)
				.toString();

		final String actualProductPageTitle = seoPageTitleResolver.resolveProductPageTitle(productModel1);
		assertNotSame(assertMessage, expectedProductPageTitle1, actualProductPageTitle);
	}

	/**
	 * To test the Product page title when the product doesn't have any Brand specific super category
	 */
	@Test
	public void testResolveProductPageTitleWithOutBrand()
	{
		final String expectedProductPageTitle1 = new StringBuilder(productModel2.getName()).append(TITLE_WORD_SEPARATOR_PIPE)
				.append(SITE_NAME).toString();
		final String assertMessage = new StringBuilder("Expected Product Page Title is :").append(expectedProductPageTitle1)
				.toString();

		final String actualProductPageTitle = seoPageTitleResolver.resolveProductPageTitle(productModel2);
		assertNotSame(assertMessage, expectedProductPageTitle1, actualProductPageTitle);
	}

	/**
	 * To test the Product page title when the product doesn't have any super category
	 */
	@Test
	public void testResolveProductPageTitleWithNoSuperCategories()
	{
		final String expectedProductPageTitle1 = new StringBuilder(productModel3.getName()).toString();
		final String assertMessage = new StringBuilder("Expected Product Page Title is :").append(expectedProductPageTitle1)
				.toString();

		final String actualProductPageTitle = seoPageTitleResolver.resolveProductPageTitle(productModel3);
		assertNotSame(assertMessage, expectedProductPageTitle1, actualProductPageTitle);
	}


}
