package com.capgemini.seoaddon.breadcrumb.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.seoservices.dao.NavigationDaoImpl;
import com.capgemini.seoservices.url.impl.SeoProductModelUrlResolver;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorcms.model.components.NavigationBarComponentModel;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.history.BrowseHistory;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;



/**@author lyonscg.
 *Unit test for SeoProductBreadcrumbBuilder.
 */
@UnitTest
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(
{ LogRunListener.class })
public class SeoProductBreadcrumbBuilderTest
{
	private static final Logger LOG = Logger.getLogger(SeoProductBreadcrumbBuilderTest.class);

	@InjectMocks
	private final SeoProductBreadcrumbBuilder productBreadcrumbBuilder = new SeoProductBreadcrumbBuilder();
	private static final String LEAF_CAT_NAME = "leafcategoryName";
	private static final String MID_CAT_NAME = "midCategoryName";
	private static final String ROOT_CAT_NAME = "rootCategoryName";

	private static final String LEAF_CAT_CODE = "leafCategoryCode";
	private static final String MID_CAT_CODE = "midCategoryCode";
	private static final String ROOT_CAT_CODE = "rootCategoryCode";

	private static final String PRODUCT_NAME = "productName";
	private static final String PRODUCT_CODE = "productCode";

	private static final String PRODUCT_NAME_LVL3 = "productNameLvl3";
	private static final String PRODUCT_CODE_LVL3 = "productCodeLvl3";

	private static final String LAST_LINK_CLASS = "active";

	private static final String LVL1_CAT_NAME = "categoryNameLvl1";
	private static final String LVL2_CAT_NAME = "categoryNameLvl2";
	private static final String LVL3_CAT_NAME = "categoryNameLvl3";

	private static final String LVL1_CAT_CODE = "categoryCodeLvl1";
	private static final String LVL2_CAT_CODE = "categoryCodeLvl2";
	private static final String LVL3_CAT_CODE = "categoryCodeLvl3";

	/** Mocked Test category models */
	@Mock
	private CategoryModel middleCategory, rootCategory, leafCategory, categoryLvl1, categoryLvl2, categoryLvl3;
	/** Mocked productModelUrlResolver */
	@Mock
	private SeoProductModelUrlResolver productModelUrlResolver;
	/** Mocked categoryModelUrlResolver */
	@Mock
	private UrlResolver<CategoryModel> categoryModelUrlResolver;
	/** Mocked productModel */
	@Mock
	private ProductModel productModel, productModelLvl3;
	/** Mocked browseHistory */
	@Mock
	private BrowseHistory browseHistory;

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private SiteConfigService siteConfigService;
	@Mock
	private NavigationDaoImpl navigationDao;
	@Mock
	NavigationBarComponentModel navBarCompModel1;
	@Mock
	NavigationBarComponentModel navBarCompModel2;
	@Mock
	CMSLinkComponentModel navLinkCompModel1, navLinkCompModel2;
	@Mock
	CMSLinkComponentModel navLinkCompModelLvl1, navLinkCompModelLvl2, navLinkCompModelLvl3;
	@Mock
	CMSNavigationNodeModel navNodeModel1;
	@Mock
	CMSNavigationNodeModel navNodeModel2;
	@Mock
	CMSNavigationNodeModel navNodeModelLvl3;
	@Mock
	CMSNavigationNodeModel navNodeModelLvl2;
	@Mock
	CMSNavigationNodeModel navNodeModelLvl1;
	@Mock
	CMSLinkComponentModel categoryLink;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		given(rootCategory.getName()).willReturn(ROOT_CAT_NAME);
		given(rootCategory.getCode()).willReturn(ROOT_CAT_CODE);
		given(rootCategory.getUseInCategoryBreadcrumb()).willReturn(Boolean.FALSE);
		given(rootCategory.getSupercategories()).willReturn(Collections.EMPTY_LIST);

		given(middleCategory.getName()).willReturn(MID_CAT_NAME);
		given(middleCategory.getCode()).willReturn(MID_CAT_CODE);
		given(middleCategory.getUseInCategoryBreadcrumb()).willReturn(Boolean.FALSE);
		given(middleCategory.getSupercategories()).willReturn(Collections.singletonList(rootCategory));

		given(leafCategory.getName()).willReturn(LEAF_CAT_NAME);
		given(leafCategory.getCode()).willReturn(LEAF_CAT_CODE);
		given(leafCategory.getUseInCategoryBreadcrumb()).willReturn(Boolean.TRUE);
		given(leafCategory.getSupercategories()).willReturn(Collections.singletonList(middleCategory));

		given(productModel.getName()).willReturn(PRODUCT_NAME);
		given(productModel.getCode()).willReturn(PRODUCT_CODE);

		given(categoryModelUrlResolver.resolve(rootCategory)).willReturn("/c/" + ROOT_CAT_CODE);
		given(categoryModelUrlResolver.resolve(middleCategory)).willReturn("/c/" + MID_CAT_CODE);
		given(categoryModelUrlResolver.resolve(leafCategory)).willReturn("/" + LEAF_CAT_NAME + "/c/" + LEAF_CAT_CODE);

		given(browseHistory.findEntryMatchUrlEndsWith(Mockito.anyString())).willReturn(null);
		productBreadcrumbBuilder.setBrowseHistory(browseHistory);

		given(categoryModelUrlResolver.resolve(categoryLvl1)).willReturn("/" + LVL1_CAT_NAME + "/c/" + LVL1_CAT_CODE);
		given(categoryModelUrlResolver.resolve(categoryLvl2)).willReturn("/" + LVL2_CAT_NAME + "/c/" + LVL2_CAT_CODE);
		given(categoryModelUrlResolver.resolve(categoryLvl3)).willReturn("/" + LVL3_CAT_NAME + "/c/" + LVL3_CAT_CODE);

		given(productModelLvl3.getName()).willReturn(PRODUCT_NAME_LVL3);
		given(productModelLvl3.getCode()).willReturn(PRODUCT_CODE_LVL3);

		given(productModelUrlResolver.resolve(productModelLvl3)).willReturn(
				"/" + LVL3_CAT_NAME + "/" + PRODUCT_NAME_LVL3 + "/p/" + PRODUCT_CODE_LVL3);

		given(categoryLvl1.getName()).willReturn(LVL1_CAT_NAME);
		given(categoryLvl1.getCode()).willReturn(LVL1_CAT_CODE);
		given(categoryLvl1.getUseInCategoryBreadcrumb()).willReturn(Boolean.TRUE);
		given(categoryLvl1.getSupercategories()).willReturn(Collections.EMPTY_LIST);

		given(categoryLvl2.getName()).willReturn(LVL2_CAT_NAME);
		given(categoryLvl2.getCode()).willReturn(LVL2_CAT_CODE);
		given(categoryLvl2.getUseInCategoryBreadcrumb()).willReturn(Boolean.TRUE);
		given(categoryLvl2.getSupercategories()).willReturn(Collections.singletonList(categoryLvl1));

		given(categoryLvl3.getName()).willReturn(LVL3_CAT_NAME);
		given(categoryLvl3.getCode()).willReturn(LVL3_CAT_CODE);
		given(categoryLvl3.getUseInCategoryBreadcrumb()).willReturn(Boolean.TRUE);
		given(categoryLvl3.getSupercategories()).willReturn(Collections.singletonList(categoryLvl2));

		given(navNodeModelLvl1.getTitle(Locale.ENGLISH)).willReturn("");
		given(navNodeModelLvl2.getTitle(Locale.ENGLISH)).willReturn("NAV_NODE_LVL2_NAME");
		given(navNodeModelLvl3.getTitle(Locale.ENGLISH)).willReturn("NAV_NODE_LVL3_NAME");
		given(navNodeModelLvl1.getUseInBreadcrumbUrl()).willReturn(Boolean.FALSE);
		given(navNodeModelLvl2.getUseInBreadcrumbUrl()).willReturn(Boolean.TRUE);
		given(navNodeModelLvl3.getUseInBreadcrumbUrl()).willReturn(Boolean.FALSE);

		final LanguageModel languageModel = mock(LanguageModel.class);
		languageModel.setIsocode("EN");
		given(commonI18NService.getCurrentLanguage()).willReturn(languageModel);
		given(commonI18NService.getLocaleForLanguage(commonI18NService.getCurrentLanguage())).willReturn(Locale.ENGLISH);

	}

	@Test
	public void testGetBreadcrumbsLeafCategoryByCategoryPath()
	{
		Mockito.when(siteConfigService.getProperty("use.navigation.node.based.path")).thenReturn("false");

		final LanguageModel languageModel = mock(LanguageModel.class);
		languageModel.setIsocode("EN");
		given(commonI18NService.getCurrentLanguage()).willReturn(languageModel);
		given(commonI18NService.getLocaleForLanguage(commonI18NService.getCurrentLanguage())).willReturn(Locale.ENGLISH);
		given(productModel.getSupercategories()).willReturn(Collections.singletonList(leafCategory));
		given(productModelUrlResolver.resolve(productModel)).willReturn(
				"/" + LEAF_CAT_NAME + "/" + PRODUCT_NAME + "/p/" + PRODUCT_CODE);

		final List<Breadcrumb> breadcrumbs = productBreadcrumbBuilder.getBreadcrumbs(productModel);
		LOG.info("breadcrumbs" + breadcrumbs.size());
		Assert.assertTrue(breadcrumbs.size() == 2);

		Breadcrumb actualBreadcrumb = breadcrumbs.get(0);
		Assert.assertTrue(actualBreadcrumb.getUrl().equals("/" + LEAF_CAT_NAME + "/c/" + LEAF_CAT_CODE));
		Assert.assertTrue(actualBreadcrumb.getName().equals(LEAF_CAT_NAME));
		Assert.assertNull(actualBreadcrumb.getLinkClass());

		actualBreadcrumb = breadcrumbs.get(1);
		Assert.assertTrue(actualBreadcrumb.getUrl().equals("/" + LEAF_CAT_NAME + "/" + PRODUCT_NAME + "/p/" + PRODUCT_CODE));
		Assert.assertTrue(actualBreadcrumb.getName().equals(PRODUCT_NAME));
		Assert.assertTrue(actualBreadcrumb.getLinkClass().equals(LAST_LINK_CLASS));

	}

	@Test
	public void testGetBreadcrumbsMiddleCategoryByCategoryPath()
	{
		Mockito.when(siteConfigService.getProperty("use.navigation.node.based.path")).thenReturn("false");

		final LanguageModel languageModel = mock(LanguageModel.class);
		languageModel.setIsocode("EN");
		given(commonI18NService.getCurrentLanguage()).willReturn(languageModel);
		given(commonI18NService.getLocaleForLanguage(commonI18NService.getCurrentLanguage())).willReturn(Locale.ENGLISH);
		given(productModel.getSupercategories()).willReturn(Collections.singletonList(middleCategory));
		given(productModelUrlResolver.resolve(productModel)).willReturn(
				"/" + MID_CAT_NAME + "/" + PRODUCT_NAME + "/p/" + PRODUCT_CODE);

		final List<Breadcrumb> breadcrumbs = productBreadcrumbBuilder.getBreadcrumbs(productModel);
		Assert.assertTrue(breadcrumbs.size() == 1);

		final Breadcrumb actualBreadcrumb = breadcrumbs.get(0);
		Assert.assertTrue(actualBreadcrumb.getUrl().equals("/" + MID_CAT_NAME + "/" + PRODUCT_NAME + "/p/" + PRODUCT_CODE));
		Assert.assertTrue(actualBreadcrumb.getName().equals(PRODUCT_NAME));
		Assert.assertTrue(actualBreadcrumb.getLinkClass().equals(LAST_LINK_CLASS));
	}

	/**
	 * Test Breadcrumbs by navigation path. Where the primary category of the product has a navigation bar component
	 * associated with it. And this NavigationBarComponent has a CMSLinkComponent, which has a category object in it.
	 * This category object within the CMSLinkComponent will be used to create the breadcrumb This is like the Top
	 * category Test
	 */
	@Test
	public void testGetBreadcrumbsByNavPathWithNavBarLinks()
	{
		given(siteConfigService.getProperty("use.navigation.node.based.path")).willReturn("true");

		given(productModelUrlResolver.getPrimaryCategoryForProduct(productModel)).willReturn(middleCategory);

		given(navLinkCompModel1.getCategory()).willReturn(middleCategory);

		given(navNodeModel1.getTitle(Locale.ENGLISH)).willReturn("NAV_NODE_1_NAME");

		given(navBarCompModel1.getNavigationNode()).willReturn(navNodeModel1);
		given(navBarCompModel1.getNavigationNode().getTitle(Locale.ENGLISH)).willReturn("NAV_NODE_1_NAME");
		given(navBarCompModel1.getLink()).willReturn(navLinkCompModel1);

		given(categoryModelUrlResolver.resolve(middleCategory)).willReturn("/" + MID_CAT_NAME + "/c/" + MID_CAT_CODE);
		given(productModelUrlResolver.resolve(productModel)).willReturn(
				"/" + MID_CAT_NAME + "/" + PRODUCT_NAME + "/p/" + PRODUCT_CODE);

		final List<NavigationBarComponentModel> navBarLinksRoot = new ArrayList<NavigationBarComponentModel>();
		navBarLinksRoot.add(navBarCompModel1);

		given(navigationDao.findAllNavigationBarLinksForGivingCategory(middleCategory)).willReturn(navBarLinksRoot);

		//Setup Ends here

		final List<Breadcrumb> breadcrumbs = productBreadcrumbBuilder.getBreadcrumbs(productModel);
		Assert.assertTrue(breadcrumbs.size() == 2);
		//Irrespective of the category hierarchy, if the category returns a navigation bar component, that is what will be used for the Bread crumb
		final Breadcrumb catBreadcrumb = breadcrumbs.get(0);
		Assert.assertNotNull(catBreadcrumb);
		Assert.assertNotNull(catBreadcrumb.getUrl());
		LOG.info("catBreadcrumb.getUrl() :" + catBreadcrumb.getUrl());
		Assert.assertTrue(catBreadcrumb.getUrl().equals("/" + MID_CAT_NAME + "/c/" + MID_CAT_CODE));
		Assert.assertTrue(catBreadcrumb.getName().equals(MID_CAT_NAME));
		Assert.assertTrue(StringUtils.isEmpty(catBreadcrumb.getLinkClass()));

		final Breadcrumb lastBreadcrumb = breadcrumbs.get(1);
		LOG.info("lastBreadcrumb.getUrl() :" + lastBreadcrumb.getUrl());
		Assert.assertTrue(lastBreadcrumb.getUrl().equals("/" + MID_CAT_NAME + "/" + PRODUCT_NAME + "/p/" + PRODUCT_CODE));
		Assert.assertTrue(lastBreadcrumb.getName().equals(PRODUCT_NAME));
		Assert.assertTrue(lastBreadcrumb.getLinkClass().equals(LAST_LINK_CLASS));

	}

	/**
	 * This test case is for data structure with Navigation Bar which has Sub Links, and this is bread crumb generation
	 * for a product assigned to these sub links This is middle or leaf level test
	 */
	@Test
	public void testGetBreadcrumbsByNavPathWithCMSLinkComponents()
	{
		given(siteConfigService.getProperty("use.navigation.node.based.path")).willReturn("true");

		given(productModelUrlResolver.getPrimaryCategoryForProduct(productModelLvl3)).willReturn(categoryLvl3);

		given(navigationDao.findAllNavigationBarLinksForGivingCategory(categoryLvl3)).willReturn(Collections.EMPTY_LIST);

		given(navigationDao.findCMSLinkComponentForNavigationNode(navNodeModelLvl2)).willReturn(navLinkCompModelLvl2);
		given(navLinkCompModelLvl2.getCategory()).willReturn(categoryLvl2);

		given(navNodeModelLvl2.getParent()).willReturn(navNodeModelLvl1);
		given(navNodeModelLvl3.getParent()).willReturn(navNodeModelLvl2);

		given(navBarCompModel2.getNavigationNode()).willReturn(navNodeModelLvl2);
		given(navBarCompModel2.getLink()).willReturn(navLinkCompModelLvl2);

		final List<CMSNavigationNodeModel> linkNavigationNodes = new ArrayList<CMSNavigationNodeModel>();
		linkNavigationNodes.add(navNodeModelLvl3);

		given(navLinkCompModelLvl3.getNavigationNodes()).willReturn(linkNavigationNodes);
		given(navLinkCompModelLvl3.getCategory()).willReturn(categoryLvl3);

		//Link Components associated with a category
		final List<CMSLinkComponentModel> categoryCmsLinks = new ArrayList<CMSLinkComponentModel>();
		categoryCmsLinks.add(navLinkCompModelLvl3);

		given(categoryLvl3.getLinkComponents()).willReturn(categoryCmsLinks);

		LOG.info("Setup Complete");
		//Setup ends here

		final List<Breadcrumb> breadcrumbs = productBreadcrumbBuilder.getBreadcrumbs(productModelLvl3);
		Assert.assertTrue(breadcrumbs.size() == 3);
		//Irrespective of the category hierarchy, if the category returns a navigation bar component, that is what will be used for the Bread crumb
		final Breadcrumb catBreadcrumb1 = breadcrumbs.get(0);
		Assert.assertNotNull(catBreadcrumb1);
		Assert.assertNotNull(catBreadcrumb1.getUrl());
		LOG.info("catBreadcrumb.getUrl() :" + catBreadcrumb1.getUrl());
		Assert.assertTrue(catBreadcrumb1.getUrl().equals("/" + LVL2_CAT_NAME + "/c/" + LVL2_CAT_CODE));
		Assert.assertTrue(catBreadcrumb1.getName().equals(LVL2_CAT_NAME));
		Assert.assertTrue(StringUtils.isEmpty(catBreadcrumb1.getLinkClass()));

		final Breadcrumb catBreadcrumb2 = breadcrumbs.get(1);
		Assert.assertNotNull(catBreadcrumb2);
		Assert.assertNotNull(catBreadcrumb2.getUrl());
		LOG.info("catBreadcrumb2.getUrl() :" + catBreadcrumb2.getUrl());
		Assert.assertTrue(catBreadcrumb2.getUrl().equals("/" + LVL3_CAT_NAME + "/c/" + LVL3_CAT_CODE));
		Assert.assertTrue(catBreadcrumb2.getName().equals(LVL3_CAT_NAME));
		Assert.assertTrue(StringUtils.isEmpty(catBreadcrumb2.getLinkClass()));

		final Breadcrumb lastBreadcrumb = breadcrumbs.get(2);
		LOG.info("lastBreadcrumb.getUrl() :" + lastBreadcrumb.getUrl());
		Assert.assertTrue(lastBreadcrumb.getUrl().equals("/" + LVL3_CAT_NAME + "/" + PRODUCT_NAME_LVL3 + "/p/" + PRODUCT_CODE_LVL3));
		Assert.assertTrue(lastBreadcrumb.getName().equals(PRODUCT_NAME_LVL3));
		Assert.assertTrue(lastBreadcrumb.getLinkClass().equals(LAST_LINK_CLASS));

	}
}
