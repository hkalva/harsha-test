package com.capgemini.seoaddon.breadcrumb.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.testframework.HybrisJUnit4ClassRunner;
import de.hybris.platform.testframework.RunListeners;
import de.hybris.platform.testframework.runlistener.LogRunListener;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**@author lyonscg.
 *Unit test for SeoSearchBreadcrumbBuilder.
 */
@UnitTest
@RunWith(HybrisJUnit4ClassRunner.class)
@RunListeners(
{ LogRunListener.class })
public class SeoSearchBreadcrumbBuilderTest
{
	@InjectMocks
	private final SeoSearchBreadcrumbBuilder searchBreadcrumbBuilder = new SeoSearchBreadcrumbBuilder();
	private static final String LEAF_CAT_NAME = "leafcategoryName";
	private static final String MID_CAT_NAME = "midCategoryName";
	private static final String ROOT_CAT_NAME = "rootCategoryName";

	private static final String LEAF_CAT_CODE = "leafCategoryCode";
	private static final String MID_CAT_CODE = "midCategoryCode";
	private static final String ROOT_CAT_CODE = "rootCategoryCode";

	private static final String PRODUCT_NAME = "productName";
	private static final String PRODUCT_CODE = "productCode";

	/** Mocked Test category models */
	@Mock
	private CategoryModel middleCategory, rootCategory, leafCategory;
	/** Mocked categoryModelUrlResolver */
	@Mock
	private UrlResolver<CategoryModel> categoryModelUrlResolver;
	/** Mocked commerceCategoryService */
	@Mock
	private CommerceCategoryService commerceCategoryService;
	/** Mocked productModel */
	@Mock
	private ProductModel productModel;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private SiteConfigService siteConfigService;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		given(rootCategory.getName()).willReturn(ROOT_CAT_NAME);
		given(rootCategory.getCode()).willReturn(ROOT_CAT_CODE);
		given(rootCategory.getUseInCategoryBreadcrumb()).willReturn(Boolean.FALSE);
		given(rootCategory.getSupercategories()).willReturn(Collections.EMPTY_LIST);

		given(middleCategory.getName()).willReturn(MID_CAT_NAME);
		given(middleCategory.getCode()).willReturn(MID_CAT_CODE);
		given(middleCategory.getUseInCategoryBreadcrumb()).willReturn(Boolean.FALSE);
		given(middleCategory.getSupercategories()).willReturn(Collections.singletonList(rootCategory));

		given(leafCategory.getName()).willReturn(LEAF_CAT_NAME);
		given(leafCategory.getCode()).willReturn(LEAF_CAT_CODE);
		given(leafCategory.getUseInCategoryBreadcrumb()).willReturn(Boolean.TRUE);
		given(leafCategory.getSupercategories()).willReturn(Collections.singletonList(middleCategory));

		given(productModel.getName()).willReturn(PRODUCT_NAME);
		given(productModel.getCode()).willReturn(PRODUCT_CODE);
		given(productModel.getSupercategories()).willReturn(Collections.singletonList(leafCategory));

		given(categoryModelUrlResolver.resolve(rootCategory)).willReturn("/c/" + ROOT_CAT_CODE);
		given(categoryModelUrlResolver.resolve(middleCategory)).willReturn("/c/" + MID_CAT_CODE);
		given(categoryModelUrlResolver.resolve(leafCategory)).willReturn("/" + LEAF_CAT_NAME + "/c/" + LEAF_CAT_CODE);
		searchBreadcrumbBuilder.setCategoryModelUrlResolver(categoryModelUrlResolver);

		given(commerceCategoryService.getCategoryForCode(ROOT_CAT_CODE)).willReturn(rootCategory);
		given(commerceCategoryService.getCategoryForCode(MID_CAT_CODE)).willReturn(middleCategory);
		given(commerceCategoryService.getCategoryForCode(LEAF_CAT_CODE)).willReturn(leafCategory);
		searchBreadcrumbBuilder.setCommerceCategoryService(commerceCategoryService);

		final LanguageModel languageModel = mock(LanguageModel.class);
		languageModel.setIsocode("EN");
		given(commonI18NService.getCurrentLanguage()).willReturn(languageModel);
		given(commonI18NService.getLocaleForLanguage(commonI18NService.getCurrentLanguage())).willReturn(Locale.ENGLISH);

	}

	@Test
	public void testGetBreadcrumbsLeafCategory()
	{
		Mockito.when(siteConfigService.getProperty("use.navigation.node.based.path")).thenReturn("false");

		List<Breadcrumb> breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(LEAF_CAT_CODE, PRODUCT_NAME, true);
		Assert.assertTrue(breadcrumbs.size() == 1);
		Breadcrumb actualBreadcrumb = breadcrumbs.get(0);
		Assert.assertTrue(actualBreadcrumb.getUrl().equals("/" + LEAF_CAT_NAME + "/c/" + LEAF_CAT_CODE));
		Assert.assertTrue(actualBreadcrumb.getName().equals(LEAF_CAT_NAME));
		Assert.assertTrue(actualBreadcrumb.getLinkClass().equals(""));

		breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(LEAF_CAT_CODE, PRODUCT_NAME, false);
		Assert.assertTrue(breadcrumbs.size() == 1);
		actualBreadcrumb = breadcrumbs.get(0);
		Assert.assertTrue(actualBreadcrumb.getUrl().equals("/" + LEAF_CAT_NAME + "/c/" + LEAF_CAT_CODE));
		Assert.assertTrue(actualBreadcrumb.getName().equals(LEAF_CAT_NAME));
		Assert.assertTrue(actualBreadcrumb.getLinkClass().equals(SeoSearchBreadcrumbBuilder.LAST_LINK_CLASS));

	}

	@Test
	public void testGetBreadcrumbsNonLeafCategory()
	{
		Mockito.when(siteConfigService.getProperty("use.navigation.node.based.path")).thenReturn("false");

		List<Breadcrumb> breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(MID_CAT_CODE, PRODUCT_NAME, false);
		Assert.assertTrue(breadcrumbs.size() == 0);

		breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(ROOT_CAT_CODE, PRODUCT_NAME, false);
		Assert.assertTrue(breadcrumbs.size() == 0);

		breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(MID_CAT_CODE, PRODUCT_NAME, true);
		Assert.assertTrue(breadcrumbs.size() == 0);

		breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(ROOT_CAT_CODE, PRODUCT_NAME, true);
		Assert.assertTrue(breadcrumbs.size() == 0);
	}

	@Test
	public void testGetBreadcrumbsNoCategory()
	{
		List<Breadcrumb> breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(null, PRODUCT_NAME, false);
		Assert.assertTrue(breadcrumbs.size() == 1);
		Breadcrumb actualBreadcrumb = breadcrumbs.get(0);
		Assert.assertTrue(actualBreadcrumb.getUrl().equals("/search?text=" + PRODUCT_NAME));
		Assert.assertTrue(actualBreadcrumb.getName().equals(PRODUCT_NAME));
		Assert.assertTrue(actualBreadcrumb.getLinkClass().equals(""));

		breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(null, PRODUCT_NAME, false);
		Assert.assertTrue(breadcrumbs.size() == 1);
		actualBreadcrumb = breadcrumbs.get(0);
		Assert.assertTrue(actualBreadcrumb.getUrl().equals("/search?text=" + PRODUCT_NAME));
		Assert.assertTrue(actualBreadcrumb.getName().equals(PRODUCT_NAME));
		Assert.assertTrue(actualBreadcrumb.getLinkClass().equals(""));
	}

}
