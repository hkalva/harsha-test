package com.capgemini.seoaddon.constants;

/**
 *
 * @author lyonscg
 *
 */
public class CapgeminiseoaddonConstants extends GeneratedCapgeminiseoaddonConstants
{
    /**
     * constant for extension name.
     */
	public static final String EXTENSIONNAME = "capgeminiseoaddon";

	/**
	 * the default private constructor.
	 */
	private CapgeminiseoaddonConstants()
	{
		//empty
	}
	
	
}
