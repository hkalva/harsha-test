package com.capgemini.seoaddon.jalo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.capgemini.seoaddon.constants.LyonscgseoaddonConstants;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;

@SuppressWarnings("PMD")
public class LyonscgseoaddonManager extends GeneratedLyonscgseoaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = LogManager.getLogger( LyonscgseoaddonManager.class.getName() );
	
	public static final LyonscgseoaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (LyonscgseoaddonManager) em.getExtension(LyonscgseoaddonConstants.EXTENSIONNAME);
	}
	
}
