/**
 * Copyright (c) 2019 Capgemini. All rights reserved.
 *
 * This software is the confidential and proprietary information of Capgemini
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Capgemini.
 */
package com.capgemini.report.cronjob;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.type.TypeService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.capgemini.report.model.DataReportCronJobModel;


/**
 * Cronjob to create data reports based on queries.
 *
 * @author lyonscg
 */
public class DataReportJobPerformable extends AbstractJobPerformable<DataReportCronJobModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(DataReportJobPerformable.class);
	private TypeService typeService;
	private final Map<String, String> catalogVersionMap = new HashMap<>();

	/**
	 * Iterates through the report queries in {@link DataReportCronJobModel} and logs the results.
	 */
	@Override
	public PerformResult perform(final DataReportCronJobModel dataReportCronJob)
	{
		final Map<String, String> queryMap = dataReportCronJob.getReportQueries();
		boolean error = false;
		if (org.apache.commons.collections4.MapUtils.isNotEmpty(queryMap))
		{
			final Set<Entry<String, String>> entries = queryMap.entrySet();
			final List<String> reportHeaders = new ArrayList<>();
			final List<List<String>> fullErrorList = new ArrayList<>();
			for (final Entry<String, String> entry : entries)
			{
				try
				{
					final SearchResult<ItemModel> results = flexibleSearchService.search(entry.getValue());
					createReport(reportHeaders, fullErrorList, entry.getKey(), results.getResult());
				}
				catch (final Exception e)
				{
					error = true;
					LOG.error("Error executing report '" + entry.getKey() + "' skipping and proceeding to next report", e);
				}
			}
			logErrorReport(reportHeaders, fullErrorList);
		}
		return new PerformResult(error ? CronJobResult.ERROR : CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	/**
	 * Creates a report based on the items in the list.
	 *
	 * @param reportHeaders
	 *           - String list that will be used to added the header of each report.
	 * @param fullErrorList
	 *           - String list that will have the complete details of the report.
	 * @param name
	 *           - Name of the report.
	 * @param items
	 *           - The items in the report.
	 */
	protected void createReport(final List<String> reportHeaders, final List<List<String>> fullErrorList, final String name,
			final List<ItemModel> items)
	{
		if (CollectionUtils.isNotEmpty(items))
		{
			final List<String> reportLines = new ArrayList<>();
			final String itemType = items.get(0).getItemtype();
			reportLines
					.add("HEADER :" + items.size() + " Items in " + name + " report[" + itemType + "]" + SystemUtils.LINE_SEPARATOR);
			reportHeaders.add(items.size() + " Items in " + name + " report[" + itemType + "]" + SystemUtils.LINE_SEPARATOR);
			final Set<String> uniqueAttributes = getTypeService().getUniqueAttributes(itemType);
			for (final ItemModel item : items)
			{
				if (CollectionUtils.isNotEmpty(uniqueAttributes))
				{
					reportLines.add(getUniqueAttributesMap(uniqueAttributes, item).toString() + SystemUtils.LINE_SEPARATOR);
				}
			}
			if (!reportLines.isEmpty())
			{
				fullErrorList.add(reportLines);
			}
		}
	}

	private void logErrorReport(final List<String> reportHeaders, final List<List<String>> fullErrorList)
	{
		if (CollectionUtils.isNotEmpty(reportHeaders))
		{
		    String error = SystemUtils.LINE_SEPARATOR + String.join(StringUtils.EMPTY, reportHeaders);
			LOG.error("{}", error);
			
			for (final List<String> list : fullErrorList)
			{
			    error = SystemUtils.LINE_SEPARATOR + String.join(StringUtils.EMPTY, list);
				LOG.error("{}", error);
			}
		}
	}

	private Map<String, String> getUniqueAttributesMap(final Set<String> uniqueAttributes, final ItemModel item)
	{
		final Map<String, String> uniqueAttributeMap = new HashMap<>(uniqueAttributes.size());
		for (final String uniqueAttribute : uniqueAttributes)
		{
			final String attributeValue = modelService.getAttributeValue(item, uniqueAttribute).toString();
			if ("catalogVersion".equalsIgnoreCase(uniqueAttribute))
			{
				if (!getCatalogVersionMap().containsKey(attributeValue))
				{
					final CatalogVersionModel catalogVersion = modelService.getAttributeValue(item, uniqueAttribute);
					getCatalogVersionMap().put(attributeValue,
							catalogVersion.getCatalog().getId() + ":" + catalogVersion.getCatalog().getVersion());
				}
				uniqueAttributeMap.put(uniqueAttribute, getCatalogVersionMap().get(attributeValue));
			}
			else
			{
				uniqueAttributeMap.put(uniqueAttribute, attributeValue);
			}
		}
		return uniqueAttributeMap;
	}

	protected TypeService getTypeService()
	{
		return typeService;
	}

	protected Map<String, String> getCatalogVersionMap()
	{
		return catalogVersionMap;
	}

	@Required
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

}
