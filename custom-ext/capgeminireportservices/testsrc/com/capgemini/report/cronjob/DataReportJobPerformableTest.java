package com.capgemini.report.cronjob;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.type.TypeService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.SystemUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.report.model.DataReportCronJobModel;


/**
 * Unit test for {@link DataReportJobPerformable}.
 */
@UnitTest
public class DataReportJobPerformableTest
{
	private static final String CATALOG_VERSION = "Online";
	private static final String CATALOG_ID = "ticketnetworkProductCatalog";
	private static final String CATEGORY_CODE = "789456123";
	private static final String CATALOG_VERSION_ATTR = "catalogVersion";
	private static final String CODE_ATTR = "code";
	private static final String CATEGORY_TYPE = "Category";
	private static final String REPORT_NAME = "No category name";
	private static final String QUERY = "select {pk} from {category} where {name} is null";
	@InjectMocks
	private DataReportJobPerformable testClass;
	@Mock
	private FlexibleSearchService flexibleSearchService;
	@Mock
	private TypeService typeService;
	@Mock
	private ModelService modelService;
	@Mock
	private DataReportCronJobModel dataReportCronJobModel;
	@Mock
	private SearchResult<Object> results;
	@Mock
	private CategoryModel categoryModel;
	@Mock
	private CatalogVersionModel catalogVersion;
	@Mock
	private CatalogModel catalogModel;
	private List<Object> categoryModels;
	private Set<String> uniqueAttributes;

	private Map<String, String> queryMap;

	/**
	 * Setup test data.
	 */
	@Before
	public void setUp()
	{
		testClass = new DataReportJobPerformable();
		MockitoAnnotations.initMocks(this);

		queryMap = new HashMap<>(1);
		categoryModels = Collections.singletonList(categoryModel);
		queryMap.put(REPORT_NAME, QUERY);

		uniqueAttributes = new HashSet<>(2);
		uniqueAttributes.add(CODE_ATTR);
		uniqueAttributes.add(CATALOG_VERSION_ATTR);

		given(dataReportCronJobModel.getReportQueries()).willReturn(queryMap);
		given(flexibleSearchService.search(QUERY)).willReturn(results);
		given(results.getResult()).willReturn(categoryModels);

		given(categoryModel.getItemtype()).willReturn(CATEGORY_TYPE);
		given(typeService.getUniqueAttributes(CATEGORY_TYPE)).willReturn(uniqueAttributes);

		given(modelService.getAttributeValue(categoryModel, CODE_ATTR)).willReturn(CATEGORY_CODE);
		given(modelService.getAttributeValue(categoryModel, CATALOG_VERSION_ATTR)).willReturn(catalogVersion);

		given(catalogVersion.getCatalog()).willReturn(catalogModel);
		given(catalogModel.getId()).willReturn(CATALOG_ID);
		given(catalogModel.getVersion()).willReturn(CATALOG_VERSION);
	}

	/**
	 * Positive test case for {@link DataReportJobPerformable#createReport(List, List, String, List)}.
	 */
	@Test
	public void createReportTest()
	{
		final List<String> reportHeaders = new ArrayList<>();
		final List<List<String>> fullErrorList = new ArrayList<>();
		testClass.createReport(reportHeaders, fullErrorList, REPORT_NAME, Collections.singletonList(categoryModel));

		final String topReportHeaderLine1 = categoryModels.size() + " Items in " + REPORT_NAME + " report[" + CATEGORY_TYPE + "]"
				+ SystemUtils.LINE_SEPARATOR;
		final String detailedReportHeaderLine1 = "HEADER :" + topReportHeaderLine1;
		final String detailedReportHeaderLine2 = "{" + CATALOG_VERSION_ATTR + "=" + CATALOG_ID + ":" + CATALOG_VERSION + ", "
				+ CODE_ATTR + "=" + CATEGORY_CODE + "}" + SystemUtils.LINE_SEPARATOR;

		Assert.assertTrue(reportHeaders.size() == 1);
		Assert.assertTrue(fullErrorList.size() == 1);
		final List<String> reportDetails = fullErrorList.get(0);
		Assert.assertTrue(reportDetails.size() == 2);

		Assert.assertEquals(topReportHeaderLine1, reportHeaders.get(0));
		Assert.assertEquals(detailedReportHeaderLine1, reportDetails.get(0));
		Assert.assertEquals(detailedReportHeaderLine2, reportDetails.get(1));
	}

	/**
	 * No data test case for {@link DataReportJobPerformable#createReport(List, List, String, List)}.
	 */
	@Test
	public void createReportNoDataTest()
	{
		final List<String> reportHeaders = new ArrayList<>();
		final List<List<String>> fullErrorList = new ArrayList<>();
		testClass.createReport(reportHeaders, fullErrorList, REPORT_NAME, Collections.emptyList());
		Assert.assertTrue(reportHeaders.isEmpty());
		Assert.assertTrue(fullErrorList.isEmpty());
	}

	/**
	 * Positive test case for {@link DataReportJobPerformable#perform(DataReportCronJobModel)}.
	 */
	@Test
	public void performTest()
	{
		final PerformResult performResult = testClass.perform(dataReportCronJobModel);
		Assert.assertEquals(CronJobResult.SUCCESS, performResult.getResult());
		Assert.assertEquals(CronJobStatus.FINISHED, performResult.getStatus());
	}

	/**
	 * Positive test case with no results for {@link DataReportJobPerformable#perform(DataReportCronJobModel)}.
	 */
	@Test
	public void performNoResultsTest()
	{
		given(results.getResult()).willReturn(Collections.emptyList());
		final PerformResult performResult = testClass.perform(dataReportCronJobModel);
		Assert.assertEquals(CronJobResult.SUCCESS, performResult.getResult());
		Assert.assertEquals(CronJobStatus.FINISHED, performResult.getStatus());
	}

	/**
	 * Exception test case with no results for {@link DataReportJobPerformable#perform(DataReportCronJobModel)}.
	 */
	@Test
	public void performExceptionTest()
	{
		given(flexibleSearchService.search(QUERY)).willThrow(new IllegalStateException());
		final PerformResult performResult = testClass.perform(dataReportCronJobModel);
		Assert.assertEquals(CronJobResult.ERROR, performResult.getResult());
		Assert.assertEquals(CronJobStatus.FINISHED, performResult.getStatus());
	}

}
