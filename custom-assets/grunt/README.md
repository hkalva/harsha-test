# FED BUILD SYSTEM

## INSTALLATION

### NODE.JS AND NPM

```
sudo apt update
sudo apt install nodejs 
sudo apt install npm
```

To verify the install:

```
$ nodejs --version
v10.15.3
```

```
$ npm --version
6.9.0
```

### GRUNT, ESLINT AND BABEL

```
npm install --save-dev eslint grunt grunt-cli grunt-eslint grunt-contrib-clean grunt-contrib-concat grunt-babel @babel/core @babel/preset-env 
```

## CONFIGURATION

### ESLINT

TBD

### BABEL

#### Setting javascript target files

By default, babel will only run against files with names that match _acc*.js_ in the directory you set in _custom-assets/grunt/Gruntfile.js_:

```
const JSDIR = ROOTDIR
			+ 'rlpstorefront/web/webroot/_ui/responsive/common/js/';
. . .
		concat : {
			options : {
				sourceMap : true
			},
			js : {
				src : [ JSDIR + 'acc*.js' ],
				dest : 'build/concat.js'
			}
		},
```

 Include and exclude lists can also be added to _.babelrc_.

```
{
  "ignore": [
    "foo.js",
    "bar/**/*.js"
  ]
}
```

[.babelrc Usage](https://babeljs.io/docs/en/6.26.3/babelrc)

#### Setting supported browsers

The default configuration in _custom-assets/grunt/.browserslistrc_ is:

```
last 1 version
> 1%
not dead
```

You can verify which browsers that defines by doing this:

```
lyonscg@xubuntu:/opt/lyonscg/sapcc-rlp/custom-assets/grunt$ npx browserslist
and_chr 74
and_ff 66
and_qq 1.2
and_uc 11.8
android 67
baidu 7.12
chrome 74
chrome 73
chrome 72
edge 18
edge 17
firefox 66
ie 11
ie_mob 11
ios_saf 12.2
ios_saf 12.0-12.1
kaios 2.5
op_mini all
op_mob 46
opera 58
safari 12.1
safari 12
samsung 9.2
samsung 8.2
```
[https://babeljs.io/docs/en/config-files](https://babeljs.io/docs/en/config-files)
[https://www.npmjs.com/package/browserslist](https://www.npmjs.com/package/browserslist)
