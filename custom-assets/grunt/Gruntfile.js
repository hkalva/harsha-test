"use strict";

module.exports = function(grunt) {

	const ROOTDIR = __dirname.replace('custom-assets/grunt', 'custom-ext/');
	const JSDIR = ROOTDIR
			+ 'rlpstorefront/web/webroot/_ui/responsive/common/js/';

	// Project configuration.
	grunt.initConfig({
		pkg : grunt.file.readJSON('package.json'),

		eslint : {
			options : {
				configFile : '.eslintrc.json'
			},
			target : [ ROOTDIR ]
		},
		
		clean : {
			tests : [ 'build' ]
		},
		
		concat : {
			options : {
				sourceMap : true
			},
			js : {
				src : [ JSDIR + 'acc*.js' ],
				dest : 'build/concat.js'
			}
		},
		
		babel : {
			options : {
				presets : [ '@babel/preset-env' ],
				sourceMap : true
			},
			dist : {
				options : {
					sourceMap : true,
					inputSourceMap : grunt.file.readJSON('build/concat.js.map')
				},
				src : [ 'build/concat.js', ],
				dest : JSDIR + 'build/addons.js'
			}
		}
	});

	// Load the plugin
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-eslint');
	grunt.loadNpmTasks('grunt-babel');


	// Default task(s).
	grunt.registerTask('default', [ 'clean' , 'concat', 'babel' ]);
	grunt.registerTask('rlp-all', [ 'eslint', 'clean', 'concat', 'babel' ]);
	grunt.registerTask('rlp-eslint', [ 'eslint' ]);
	grunt.registerTask('rlp-babel', [ 'clean', 'concat', 'babel' ]);

};