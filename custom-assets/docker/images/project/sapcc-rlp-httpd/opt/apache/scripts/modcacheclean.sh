#!/bin/bash

###############################################
##        mod_cache cleaning script                         
##
##  This script replaces the Apache htcacheclean script
##  as it's simply too inefficient to process a 
##  cache with hundreds of thousands of records without
##  spending hours spooling the results.
##
##          @Author Ryan Heusinkveld  
##############################################

LOG_FILE=/opt/apache/logs/cacheclean.log
CACHE_PATH=/opt/apache/cache/
MOD_TIME_MINUTES=75
INODE_THRESHOLD=80

TIMESTAMP=`date +"%Y/%m/%d-%H:%M:%S"`
INODE_COUNT=`df -i ${CACHE_PATH} | tail -n1 | awk '{print $(NF-2)}'`


function doverify {
	ACTION="verify"

	INODE_PERCENT=`df -i ${CACHE_PATH} | tail -n1 | awk '{print $(NF-1)}' | sed 's/%//'`
	if [ ${INODE_PERCENT} -gt ${INODE_THRESHOLD} ]; then
		# Attempt to compute an approximate number of directories to 
		# traverse and clean to bring below the threshold by 10%
		DIR_COUNT=`find ${CACHE_PATH} -maxdepth 1 | wc -l`
		NUM_DIR_CLEAN=$(( (DIR_COUNT * (INODE_PERCENT - INODE_THRESHOLD + 10))/100))
		runclear ${NUM_DIR_CLEAN}
	fi
}

function doclean {
	ACTION="clean"
	runclear 100000
}

function runclear {

	COUNT=0
	for SUB_PATH in ${CACHE_PATH}*
	do
		if [[ ${COUNT} -gt $1 ]]; then
			return
		fi
		COUNT=$((COUNT + 1))
		#echo "Cleaning ${SUB_PATH}"
		find ${SUB_PATH} -type f -mmin +${MOD_TIME_MINUTES} -delete
	done
	find ${SUB_PATH} -type d -empty -delete
}

case $1 in
	verify)
		doverify
		;;
	clean)
		doclean
		;;
	*)
		doverify
		;;
esac

INODE_COUNT_AFTER=`df -i ${CACHE_PATH} | tail -n1 | awk '{print $(NF-2)}'`
echo "[${TIMESTAMP}] $ACTION  inode Before=${INODE_COUNT} After=${INODE_COUNT_AFTER}" >> ${LOG_FILE}

