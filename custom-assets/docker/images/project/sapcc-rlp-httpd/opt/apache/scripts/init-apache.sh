#!/bin/bash

# Holds any commands required/desired to run at startup for Apache.
# Must be explicitly called (i.e. is not automatically triggered by Apache startup)

REWRITE_MAP_DIR=${HTTP_SERVER_INSTALL_BASE}/dynamicmaps

echo "Initializing Apache"

echo "  Converting rewrite maps to DBM"
for F_PATH in ${REWRITE_MAP_DIR}/*.txt
do
	if [ -f F_PATH ]; then
		echo "    Converting ${F_PATH}"
		F_NAME="${F_PATH%.*}"

	# Remove previously compiled dynamicmaps
		DIR_FILE=${F_NAME}.dir
		PAG_FILE=${F_NAME}.pag
		[ -f $DIR_FILE ] && echo "Removing ${DIR_FILE}" && rm -rf $DIR_FILE
		[ -f $PAG_FILE ] && echo "Removing ${PAG_FILE}" && rm -rf $PAG_FILE

		httxt2dbm -i ${F_PATH} -o ${F_NAME}
	fi
done
