#!/bin/bash
# Author      : erich.weiss
# Date        : 30 October 2016
# Description : Docker wrapper script to run commands after the docker container has started

if [ ${HTTP_CACHE_ENABLED} != true ] ; then
    chmod 111 /opt/apache/html
    chmod 111 /opt/apache/cache
fi

# Make sure we're not confused by old, incompletely-shutdown httpd
# context after restarting the container.  httpd won't start correctly
# if it thinks it is already running.
rm -rf /run/httpd/* /tmp/httpd*

# Deleting any old binary dynamic maps before converting the new ones.
# Looks like if we remove an entry from a txt file that's already in the binary file
# the entry does not get removed from the binary file. So we're cleaning any
# old binary file.
rm -f /opt/apache/dynamicmaps/*.dir
rm -f /opt/apache/dynamicmaps/*.pag

/opt/apache/scripts/init-apache.sh

# Startup commands
exec httpd -DFOREGROUND
