# Docker Build: Solr

To build a Solr image for the current Hybris version, we get the appropriate Solr server directory fromt the pom.xml, via project environment variables which are set via the custom-assets/scripts/.bash_aliases when we enter the project directory in bash. This is done via ondir. Refer to the [Docker development documentation](https://lyonscg.atlassian.net/wiki/spaces/HRP3/pages/736233459/Installation+and+Development+with+Docker) for more info.

Build Solr image:
```docker build -t lyons-solr:7.5-hybris-1811.4 -f Dockerfile $DIR_HYBRIS_SOLR_CONTEXT```

Here we see that the image is tagged for the Solr version and Hybris version. The Dockerfile is in this directory, however the Solr contrib and server directories are needed from the Solr server in the solrserver extension. For the version example above, this context directory is:

```hybris-suite/hybris/bin/ext-commerce/solrserver/resources/solr/7.5/server```
