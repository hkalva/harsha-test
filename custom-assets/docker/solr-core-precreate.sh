##!/bin/bash
# Startup script for Solr container

# Notes:
# 	Docker Solr includes a mechanism for running commands prior to Solr startup, by adding scripts to /docker-entrypoint-initdb.d
# 	As such, commands in this file will be run when the script is bind mounted during docker-compose from host to the Solr vm under:
#	 /docker-entrypoint-initdb.d
#
# Example: a docker-compose.yaml:

# RLP Docker Solr Compose Example
#version: '3.7'
#services:
#
# Solr 7.2
#  solr:
#    image: lyons-solr:7.5-hybris-1811.4
#    user: solr
#    ports:
#      - "8983:8983"
#    volumes:
#      - type: volume
#        source: solrdata2
#        target: /opt/solr/server/solr/mycores
#      - type: bind
#        source: ../../custom-assets/docker/solr-startup.sh
#        target: /docker-entrypoint-initdb.d/solr-startup.sh
#        consistency: cached

set -e

# NOTES: The following precreate-core commands are disabled, as there were issues with schema not being created property.
# Eg. code_string(plongs) instead of code_string(string), when using precreate-core vs. allowing Hybris to just create 
# the core anew with first index.

# core for rlp product catalog
precreate-core master_rlp_Product_default /opt/solr/server/solr/configsets/default

# core for backoffice search
precreate-core master_backoffice_backoffice_product_default /opt/solr/server/solr/configsets/default
