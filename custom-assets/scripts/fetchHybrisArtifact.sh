#!/bin/bash
# @TODO: Setup an array of hybris versions, and resource paths, so that this script can be used dynamically to pass in the necessary hybris version and have it installed into the m2 repository. Consider using pom.xml for hybris version matrix, or are we going to submodule the docker-related stuff, in each project?
SFTP_HOST=lcg-prd-sftp-02.lcgosc.com
SFTP_PATH=/sftproot/hybris/extensions/hybris-zips/18XX/1811_4
HYBRIS_PLATFORM_ZIP_SFTP=CXCOMM181100P_4-70004085.ZIP
HYBRIS_PLATFORM_ZIP_LOCAL=CXCOMM181100P_4-70004085.zip

read -p 'Username: ' INPUT_USERNAME
#read -sp 'Password: ' INPUT_PASSWORD

printf "Downloading ${HYBRIS_PLATFORM_ZIP_SFTP} to /tmp/${HYBRIS_PLATFORM_ZIP_LOCAL}\n"
sftp ${INPUT_USERNAME}@${SFTP_HOST}:${SFTP_PATH}/${HYBRIS_PLATFORM_ZIP_SFTP} /tmp/${HYBRIS_PLATFORM_ZIP_LOCAL}

printf "Installing artifact:\n"
mvn install:install-file \
-Dfile=/tmp/${HYBRIS_PLATFORM_ZIP_LOCAL} \
-DgroupId=com.lyonscg \
-DartifactId=hybris-commerce-suite \
-Dversion=${PROJECT_HYBRIS_VERSION} \
-Dpackaging=zip

echo "Removing tmp file /tmp/${HYBRIS_PLATFORM_ZIP_LOCAL}"
rm /tmp/${HYBRIS_PLATFORM_ZIP_LOCAL}
