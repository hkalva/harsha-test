#!/bin/bash
# @TODO: Turn this into a maven plugin, as part of the dockerization prep. Fetch necessary dependencies from SFTP using plugin, and remove this reference cruft
DIR_HYBRIS_SUITE=${DIR_HYBRIS_PROJECT}/hybris-suite
DIR_HYBRIS_CMR=${DIR_HYBRIS_SUITE}/hybris-dependencies
DIR_HYBRIS_ZIP=/Users/zselby/Development/hybris/sap-downloads/hybris-platform-zips
HYBRIS_VERSION=6.7.0.9
HYBRIS_CMR_ZIP=HYBRISCMR6700P_0-80003497.zip
HYBRIS_COMM_ZIP=HYBRISCOMM6700P_9-80003492.zip

if [ ! -f ${DIR_HYBRIS_ZIP}/${HYBRIS_VERSION}/${HYBRIS_COMM_ZIP} ]; then
    echo "Please download ${HYBRIS_COMM_ZIP} from lyonscg server to ${DIR_HYBRIS_ZIP}"
    exit 1
fi

if [ ! -f ${DIR_HYBRIS_ZIP}/${HYBRIS_VERSION}/${HYBRIS_CMR_ZIP} ]; then
    echo "Please download ${HYBRIS_CMR_ZIP} from lyonscg server to ${DIR_HYBRIS_ZIP}"
    exit 1
fi

rm -r ${DIR_HYBRIS_CMR}
cd ${DIR_HYBRIS_ZIP}/${HYBRIS_VERSION}
unzip ${HYBRIS_CMR_ZIP}
mv hybris-dependencies ${DIR_HYBRIS_SUITE}

#rm -f ~/.m2/repository/com/lyonscg/hybris-commerce-suite/6.7.0.0//hybris-commerce-suite-6.7.0.0.zip
cd ${DIR_HYBRIS_ZIP}/${HYBRIS_VERSION}
mvn install:install-file \
-Dfile=${HYBRIS_COMM_ZIP} \
-DgroupId=com.lyonscg \
-DartifactId=hybris-commerce-suite \
-Dversion=${HYBRIS_VERSION} \
-Dpackaging=zip

rm -r -f ${DIR_HYBRIS_SUITE}
cd ${DIR_HYBRIS_PROJECT}
mvn -P hybris-init validate

cd ${DIR_HYBRIS_SUITE}/hybris/bin
rm -r ${DIR_HYBRIS_SUITE}/hybris/bin/custom
ln -s ${DIR_HYBRIS_PROJECT}/custom-ext custom

cd ${DIR_HYBRIS_PROJECT}
mvn compile

cd ${DIR_HYBRIS_SUITE}/hybris/bin/platform
ant addonuninstall -Daddonnames="promotionenginesamplesaddon,customerticketingaddon,ordermanagementaddon,orderselfserviceaddon,consignmenttrackingaddon,commerceorgsamplesaddon,adaptivesearchsamplesaddon,pcmbackofficesamplesaddon,assistedservicestorefront,smarteditaddon,sapsubscriptionaddon" -DaddonStorefront.yacceleratorstorefront="meijerstorefront"

ant addoninstall -Daddonnames="promotionenginesamplesaddon,customerticketingaddon,ordermanagementaddon,orderselfserviceaddon,consignmenttrackingaddon,commerceorgsamplesaddon,adaptivesearchsamplesaddon,pcmbackofficesamplesaddon,assistedservicestorefront,smarteditaddon,sapsubscriptionaddon" -DaddonStorefront.yacceleratorstorefront="meijerstorefront"

ant addonuninstall -Daddonnames="acceleratorwebservicesaddon" -DaddonStorefront.ycommercewebservices="meijerwebservices"
ant addoninstall -Daddonnames="acceleratorwebservicesaddon" -DaddonStorefront.ycommercewebservices="meijerwebservices"

ant addonuninstall -Daddonnames="lyonscgstaticversioning,lyonscgseoaddon" -DaddonStorefront.yacceleratorstorefront="meijerstorefront"
ant addoninstall -Daddonnames="lyonscgstaticversioning,lyonscgseoaddon" -DaddonStorefront.yacceleratorstorefront="meijerstorefront"

ant npminstall

cd ${DIR_HYBRIS_PROJECT}
mvn clean validate compile

#cp /opt/meijer/hybris-commerce/custom-assets/license/installedSaplicenses.properties /opt/meijer/hybris-commerce/hybris-suite/hybris/config/licence

echo "Hybris ${HYBRIS_VERSION} upgrade complete"
