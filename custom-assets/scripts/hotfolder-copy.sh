#!/bin/sh

# Default path for RLP VM
DIR_VM_HYBRIS_PROJECT=/opt/lyonscg/sapcc-rlp

# Setup working directory for hybris:
#  If environment var DIR_HYBRIS_PROJECT is set, it's a docker-based native environament
#  Else, default to the path for vm-based environemnt
DIR_HYBRIS_PROJECT_HYBRIS="${DIR_HYBRIS_PROJECT:-${DIR_VM_HYBRIS_PROJECT}}/hybris-suite/hybris"

hfdrop=${DIR_HYBRIS_PROJECT_HYBRIS}/data/acceleratorservices/import/master/rlp
hfhome=${DIR_HYBRIS_PROJECT_HYBRIS}/bin/custom/capgeminidataxferservices/resources/dataxfer/integration/hot-folder-samples/rlp
hfimport=`dirname ${hfhome}`
tdate=`date +"%m%d%y%H%M%S"`

hfcustomer() {
  cp ${hfhome}/userGroup-001.csv ${hfdrop}/userGroup-${tdate}.csv  
  cp ${hfhome}/b2bCreditLimit-001.csv ${hfdrop}/b2bCreditLimit-${tdate}.csv
  cp ${hfhome}/b2bQuoteLimit-001.csv ${hfdrop}/b2bQuoteLimit-${tdate}.csv
  cp ${hfhome}/employee-001.csv ${hfdrop}/employee-${tdate}.csv
  cp ${hfhome}/baseB2bUnit-001.csv ${hfdrop}/baseB2bUnit-${tdate}.csv
  cp ${hfhome}/b2bUnitLang-en-001.csv ${hfdrop}/b2bUnitLang-en-${tdate}.csv
  cp ${hfhome}/b2bUnitLang-de-001.csv ${hfdrop}/b2bUnitLang-de-${tdate}.csv
  cp ${hfhome}/b2bUnitLang-ja-001.csv ${hfdrop}/b2bUnitLang-ja-${tdate}.csv
  cp ${hfhome}/b2bUnitLang-zh-001.csv ${hfdrop}/b2bUnitLang-zh-${tdate}.csv
  cp ${hfhome}/salesUnits-001.csv ${hfdrop}/salesUnits-${tdate}.csv
  cp ${hfhome}/b2bOrderThresholdPermission-001.csv ${hfdrop}/b2bOrderThresholdPermission-${tdate}.csv
  cp ${hfhome}/b2bOrderThresholdTimespanPermission-001.csv ${hfdrop}/b2bOrderThresholdTimespanPermission-${tdate}.csv
  cp ${hfhome}/b2bBudgetExceededPermission-001.csv ${hfdrop}/b2bBudgetExceededPermission-${tdate}.csv
  cp ${hfhome}/b2bUserGroup-001.csv ${hfdrop}/b2bUserGroup-${tdate}.csv
  cp ${hfhome}/b2bCustomer-001.csv ${hfdrop}/b2bCustomer-${tdate}.csv
  cp ${hfhome}/b2bCustPermissions-001.csv ${hfdrop}/b2bCustPermissions-${tdate}.csv
  cp ${hfhome}/b2bUnit2Approvers-001.csv ${hfdrop}/b2bUnit2Approvers-${tdate}.csv
  cp ${hfhome}/baseB2bBudget-001.csv ${hfdrop}/baseB2bBudget-${tdate}.csv
  cp ${hfhome}/baseB2bCostCenter-001.csv ${hfdrop}/baseB2bCostCenter-${tdate}.csv
  cp ${hfhome}/address-001.csv ${hfdrop}/address-${tdate}.csv
  cp ${hfhome}/b2bBudgetLang-en-001.csv ${hfdrop}/b2bBudgetLang-en-${tdate}.csv
  cp ${hfhome}/b2bBudgetLang-de-001.csv ${hfdrop}/b2bBudgetLang-de-${tdate}.csv
  cp ${hfhome}/b2bBudgetLang-ja-001.csv ${hfdrop}/b2bBudgetLang-ja-${tdate}.csv
  cp ${hfhome}/b2bBudgetLang-zh-001.csv ${hfdrop}/b2bBudgetLang-zh-${tdate}.csv
  cp ${hfhome}/b2bCostCenterLang-en-001.csv ${hfdrop}/b2bCostCenterLang-en-${tdate}.csv
  cp ${hfhome}/b2bCostCenterLang-de-001.csv ${hfdrop}/b2bCostCenterLang-de-${tdate}.csv
  cp ${hfhome}/b2bCostCenterLang-ja-001.csv ${hfdrop}/b2bCostCenterLang-ja-${tdate}.csv
  cp ${hfhome}/b2bCostCenterLang-zh-001.csv ${hfdrop}/b2bCostCenterLang-zh-${tdate}.csv
  cp ${hfhome}/salesUnitToOrgUnits-001.csv ${hfdrop}/salesUnitsToOrgUnits-${tdate}.csv
  cp ${hfhome}/emplToOrgUnits-001.csv ${hfdrop}/emplToOrgUnits-${tdate}.csv
  cp ${hfhome}/b2bUnitsToSalesUnits-001.csv ${hfdrop}/b2bUnitsToSalesUnits-${tdate}.csv
  cp ${hfhome}/b2bDocuments-001.csv ${hfdrop}/b2bDocuments-${tdate}.csv
}

hfproduct() {
  yes | cp -rf ${hfimport}/images ${hfdrop}
  cp ${hfhome}/classifyAttributeUnit-001.csv ${hfdrop}/classifyAttributeUnit-${tdate}.csv
  cp ${hfhome}/classificationClass-001.csv ${hfdrop}/classificationClass-${tdate}.csv
  cp ${hfhome}/classificationAttribute-001.csv ${hfdrop}/classificationAttribute-${tdate}.csv
  cp ${hfhome}/classAttributeAssignment-001.csv ${hfdrop}/classAttributeAssignment-${tdate}.csv
  cp ${hfhome}/baseCategory-001.csv ${hfdrop}/baseCategory-${tdate}.csv
  cp ${hfhome}/insertVendor-001.csv ${hfdrop}/insertVendor-${tdate}.csv
  cp ${hfhome}/variantCategory-001.csv ${hfdrop}/variantCategory-${tdate}.csv
  cp ${hfhome}/valueVariantCategory-001.csv ${hfdrop}/valueVariantCategory-${tdate}.csv
  cp ${hfhome}/refinedStock-001.csv ${hfdrop}/refinedStock-${tdate}.csv
  cp ${hfhome}/baseProduct-001.csv ${hfdrop}/baseProduct-${tdate}.csv
  cp ${hfhome}/genericVariant-001.csv ${hfdrop}/genericVariant-${tdate}.csv
  cp ${hfhome}/insertFolderMedia-001.csv ${hfdrop}/insertFolderMedia-${tdate}.csv
  cp ${hfhome}/refinedPrice-001.csv ${hfdrop}/refinedPrice-${tdate}.csv
  cp ${hfhome}/insertAnyMedia-001.csv ${hfdrop}/insertAnyMedia-${tdate}.csv
  cp ${hfhome}/insertMediaContainer-001.csv ${hfdrop}/insertMediaContainer-${tdate}.csv
  cp ${hfhome}/productLang-en-001.csv ${hfdrop}/productLang-en-${tdate}.csv
  cp ${hfhome}/productLang-de-001.csv ${hfdrop}/productLang-de-${tdate}.csv
  cp ${hfhome}/productLang-ja-001.csv ${hfdrop}/productLang-ja-${tdate}.csv
  cp ${hfhome}/productLang-zh-001.csv ${hfdrop}/productLang-zh-${tdate}.csv
  cp ${hfhome}/categoryLang-en-001.csv ${hfdrop}/categoryLang-en-${tdate}.csv
  cp ${hfhome}/categoryLang-de-001.csv ${hfdrop}/categoryLang-de-${tdate}.csv
  cp ${hfhome}/categoryLang-ja-001.csv ${hfdrop}/categoryLang-ja-${tdate}.csv
  cp ${hfhome}/categoryLang-zh-001.csv ${hfdrop}/categoryLang-zh-${tdate}.csv
  cp ${hfhome}/langClassAttri-en-001.csv ${hfdrop}/langClassAttri-en-${tdate}.csv
  cp ${hfhome}/langClassAttri-de-001.csv ${hfdrop}/langClassAttri-de-${tdate}.csv
  cp ${hfhome}/langClassAttri-ja-001.csv ${hfdrop}/langClassAttri-ja-${tdate}.csv
  cp ${hfhome}/langClassAttri-zh-001.csv ${hfdrop}/langClassAttri-zh-${tdate}.csv
  cp ${hfhome}/updateVendorLang-en-001.csv ${hfdrop}/updateVendorLang-en-${tdate}.csv
  cp ${hfhome}/merchandise-001.csv ${hfdrop}/merchandise-${tdate}.csv
  cp ${hfhome}/updateProductMedia-001.csv ${hfdrop}/updateProductMedia-${tdate}.csv
  cp ${hfhome}/classifyProduct-en-001.csv ${hfdrop}/classifyProduct-en-${tdate}.csv
  cp ${hfhome}/classifyProduct-de-001.csv ${hfdrop}/classifyProduct-de-${tdate}.csv
  cp ${hfhome}/classifyProduct-ja-001.csv ${hfdrop}/classifyProduct-ja-${tdate}.csv
  cp ${hfhome}/classifyProduct-zh-001.csv ${hfdrop}/classifyProduct-zh-${tdate}.csv
  cp ${hfhome}/langClassifyAttributeUnit-en-001.csv ${hfdrop}/langClassifyAttributeUnit-en-${tdate}.csv
  cp ${hfhome}/langClassifyAttributeUnit-de-001.csv ${hfdrop}/langClassifyAttributeUnit-de-${tdate}.csv
  cp ${hfhome}/langClassifyAttributeUnit-ja-001.csv ${hfdrop}/langClassifyAttributeUnit-ja-${tdate}.csv
  cp ${hfhome}/langClassifyAttributeUnit-zh-001.csv ${hfdrop}/langClassifyAttributeUnit-zh-${tdate}.csv
  cp ${hfhome}/updateCategoryMedia-001.csv ${hfdrop}/updateCategoryMedia-${tdate}.csv
  cp ${hfhome}/external_tax-001.csv ${hfdrop}/external_tax-${tdate}.csv
  cp ${hfhome}/productFutureStock-001.csv ${hfdrop}/productFutureStock-${tdate}.csv
}

hfstore() {
  # warehouse codes are loaded as part of Essential Data, as it is referenced in the Product Catalog
  cp ${hfhome}/baseWareHouse-001.csv ${hfdrop}/baseWareHouse-${tdate}.csv
  cp ${hfhome}/wareHouseLang-en-001.csv ${hfdrop}/wareHouseLang-en-${tdate}.csv
  cp ${hfhome}/openingSchedule-001.csv ${hfdrop}/openingSchedule-${tdate}.csv
  cp ${hfhome}/weekdayOpeningDay-001.csv ${hfdrop}/weekdayOpeningDay-${tdate}.csv
  cp ${hfhome}/baseSpecialOpeningDay-001.csv ${hfdrop}/baseSpecialOpeningDay-${tdate}.csv
  cp ${hfhome}/specialOpeningDayLang-en-001.csv ${hfdrop}/specialOpeningDayLang-en-${tdate}.csv
  cp ${hfhome}/specialOpeningDayLang-de-001.csv ${hfdrop}/specialOpeningDayLang-de-${tdate}.csv
  cp ${hfhome}/specialOpeningDayLang-ja-001.csv ${hfdrop}/specialOpeningDayLang-ja-${tdate}.csv
  cp ${hfhome}/specialOpeningDayLang-zh-001.csv ${hfdrop}/specialOpeningDayLang-zh-${tdate}.csv
  cp ${hfhome}/pos-warehouse-001.csv ${hfdrop}/pos-warehouse-${tdate}.csv
  cp ${hfhome}/pos-store-001.csv ${hfdrop}/pos-store-${tdate}.csv
  cp ${hfhome}/pointOfServiceLang-en-001.csv ${hfdrop}/pointOfServiceLang-en-${tdate}.csv
  # Address should not be loaded twice, hence setting its suffix as 001.If duplicates, it will cause PoS To Address relationship load to fail 
  cp ${hfhome}/pointOfServiceAddress-001.csv ${hfdrop}/pointOfServiceAddress-001.csv
  cp ${hfhome}/baseStore-001.csv ${hfdrop}/baseStore-${tdate}.csv
  cp ${hfhome}/baseStore2WarehouseRel-001.csv ${hfdrop}/baseStore2WarehouseRel-${tdate}.csv
  cp ${hfhome}/warehouse2DeliveryModeRelation-001.csv ${hfdrop}/warehouse2DeliveryModeRelation-${tdate}.csv
  # insertAnyMedia, insertMediaContainer contains PoS media entries too. Since, they are already loaded as part of product, need not reload 
  #cp ${hfhome}/insertAnyMedia-001.csv ${hfdrop}/insertAnyMedia-${tdate}.csv
  #cp ${hfhome}/insertMediaContainer-001.csv ${hfdrop}/insertMediaContainer-${tdate}.csv
}

hfbundle() {
  # warehouse codes are loaded as part of Essential Data, as it is referenced in the Product Catalog
  cp ${hfhome}/bundleStatus-001.csv ${hfdrop}/bundleStatus-${tdate}.csv
  cp ${hfhome}/pickExactlyNBundle-001.csv ${hfdrop}/pickExactlyNBundle-${tdate}.csv
  cp ${hfhome}/pickNToMBundle-001.csv ${hfdrop}/pickNToMBundle-${tdate}.csv
  cp ${hfhome}/baseBundleTemplate-001.csv ${hfdrop}/baseBundleTemplate-${tdate}.csv
  cp ${hfhome}/priceBundleRule-001.csv ${hfdrop}/priceBundleRule-${tdate}.csv
  cp ${hfhome}/langBundle-en-001.csv ${hfdrop}/langBundle-en-${tdate}.csv
  cp ${hfhome}/langBundle-de-001.csv ${hfdrop}/langBundle-de-${tdate}.csv
  cp ${hfhome}/langBundle-ja-001.csv ${hfdrop}/langBundle-ja-${tdate}.csv
  cp ${hfhome}/langBundle-zh-001.csv ${hfdrop}/langBundle-zh-${tdate}.csv
}

hfcustomer;
hfproduct;
hfstore;
hfbundle;
mkdir -p ${DIR_HYBRIS_PROJECT_HYBRIS}/data/acceleratorservices/export/products;
