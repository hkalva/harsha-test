#!/bin/bash
###############################################
# file: gitUnchanged.sh
#
# This script will add all files listed in /opt/lyonscg/hybris-lea-6.0/custom-hybris-patch/hybris/bin/custom
# to git update-index --assume-unchanged. Since this script does not have a significant role in build process
# +e option is added to ignore errors caused by this script in the mvn build process.
#
###############################################
set +e
cd /opt/lyonscg/sapcc-rlp/custom-hybris-patch/hybris/bin/custom
fileList=`find . -name "*.*"`
cd /opt/lyonscg/sapcc-rlp
for file in ${fileList}; do
	if [ ${file} != "." ]
	then
		if [ -f custom-ext${file:1} ]
		then
			echo git update-index --assume-unchanged custom-ext${file:1}
			git update-index --assume-unchanged custom-ext${file:1}
		fi
	fi
done