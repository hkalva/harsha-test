module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    eslint: {
        options: {
            configFile: '.eslintrc.json'
        },
        target: ['../../custom-ext/']
    }
    
});

  // Plugins
  grunt.loadNpmTasks('grunt-eslint');
  grunt.registerTask('default', ['eslint']);
  grunt.registerTask('rlp-all', ['eslint']);

};