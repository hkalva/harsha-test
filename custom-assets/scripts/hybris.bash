#Modify and add to your .bash_profile
#created based on 5.7.0.0 commerce suite, you may need to update versions in the commands below
# @TODO: Determine whether this is still valid, or needed, for SAPCC RLP based projects

export yPATH=<path/to/working/dir>
export lcgCLIENT=<clientname>

mkproject() {
    cd ${yPATH}
    rm -rf ${lcgCLIENT}/
    mvn -B archetype:generate -DarchetypeGroupId=com.lyonscg -DarchetypeArtifactId=lyonscg-hybris-archetype -DarchetypeVersion=1.0 -DgroupId=com.${lcgCLIENT} -DartifactId=${lcgCLIENT} -Dversion=1.0-SNAPSHOT
}

setupdatahub() {
    cd ${yPATH}/${lcgCLIENT}
    #Install webapp
    mvn install:install-file -Dfile=hybris-suite/hybris/bin/ext-integration/datahub/web-app/datahub-webapp-5.7.0.0-RC5.war \
    -DgroupId=com.hybris.datahub -DartifactId=datahub-webapp -Dversion=5.7.0.0-RC5 -Dpackaging=war

    #Install SDK
    mvn install:install-file -Dfile=hybris-suite/hybris/bin/ext-integration/datahub/sdk/datahub-extension-sdk-5.7.0.0-RC5.jar \
    -DgroupId=com.hybris.datahub -DartifactId=datahub-extension -Dversion=5.7.0.0-RC5 -Dpackaging=jar

    #Install Archetype
    mvn install:install-file -DgroupId=com.hybris.datahub -DartifactId=datahub-extension-archetype -Dversion=5.7.0.0-RC4 \
    -Dpackaging=jar -Dfile=custom-assets/datahub/datahub-extension-archetype-5.7.0.0-RC5.jar -DgeneratePom=true

}

mkdatahub() {
    cd ${yPATH}/${lcgCLIENT}/hybris-datahub
    mvn -B archetype:generate -DarchetypeGroupId=com.hybris.datahub -DarchetypeArtifactId=datahub-extension-archetype -DarchetypeVersion=5.6.0.0-RC4 -DgroupId=com.${lcgCLIENT} -DartifactId=datahub-extension -Dversion=1.0-SNAPSHOT -DsdkVersion=5.6.0.0-RC4
}

mkcis() {
    cd ${yPATH}/${lcgCLIENT}/hybris-cis
    mvn archetype:generate -DarchetypeGroupId=com.hybris.cis.ext -DarchetypeArtifactId=hybris-cis-core-tax-ext-archetype -DarchetypeVersion=5.7.0.0 -DgroupId=com.${lcgCLIENT} -DartifactId=cisTaxService -Dversion=1.0-SNAPSHOT
    mvn archetype:generate -DarchetypeGroupId=com.hybris.cis.ext -DarchetypeArtifactId=hybris-cis-core-payment-ext-archetype -DarchetypeVersion=5.7.0.0 -DgroupId=com.${lcgCLIENT} -DartifactId=cisPaymentService -Dversion=1.0-SNAPSHOT
}

mkoms() {
    cd ${yPATH}/${lcgCLIENT}/hybris-oms
    mvn -B archetype:generate -DarchetypeGroupId=com.hybris.oms.ext -DarchetypeArtifactId=oms-ext-archetype -DarchetypeVersion=5.6.0.10 -DgroupId=com.${lcgCLIENT} -DartifactId=oms-ext -Dversion=1.0-SNAPSHOT
}
