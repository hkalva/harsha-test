#Ask for Hybris Dir
hybris_dir=`dialog --stdout --inputbox 'Please enter Hybris base directory: (ex: ~/dev/rf/src/hybris)'  10 80`
if [ $? -ne 0 ]
then
    clear
    echo "Program exited."
    exit 1
fi
clear

hybris_dir=`echo $hybris_dir | sed "s:~:${HOME}:"`

#verify that hybris dir exists and is valid
if [ ! -d $hybris_dir ]
then
    echo "Directory doesn't exist. exiting...";
    exit 1
fi

if [ ! -d $hybris_dir/bin/ext-integration/datahub/extensions/sap/ ]
then
    echo "$hybris_dir/bin/ext-integration/datahub/extensions/sap/ doesn't appear to exist.  Hybris dir doesn't appear to be valid. exiting..."
    exit 1
fi


if [ ! -d $hybris_dir/bin/ext-integration/datahub/web-app/ ]; then
    echo "$hybris_dir/bin/ext-integration/datahub/web-app/ doesn't appear to exist.  Hybris dir doesn't appear to be valid. exiting...";
    exit 1
fi

if [ ! -d $hybris_dir/bin/ext-integration/datahub/sdk/ ]; then
    echo "$hybris_dir/bin/ext-integration/datahub/sdk/ doesn't appear to exist.  Hybris dir doesn't appear to be valid. exiting...";
    exit 1
fi
