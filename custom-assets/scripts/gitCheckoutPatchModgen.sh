#!/bin/bash
###############################################
# file: gitCheckoutPatchModgen.sh
#
# This script will do git checkout all files corresponding to /opt/lyonscg/hybris-lea-6.0/custom-hybris-patch/hybris/bin/custom
# in custom-ext.
#
###############################################
cd /opt/lyonscg/sapcc-rlp/custom-hybris-patch/hybris/bin/custom
fileList=`find . -name "*.*"`
cd /opt/lyonscg/sapcc-rlp
for file in ${fileList}; do
	if [ ${file} != "." ]
	then
		if [ -f custom-ext${file:1} ]
		then
			echo executing git checkout custom-ext${file:1}
			git checkout custom-ext${file:1}
		fi
	fi
done