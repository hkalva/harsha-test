# Source: .bash_alises from meijer project, with some changes to be more dynamic
# Prerequisite: ondir, which watches project folders, and applies the project-specific custom-assets/scripts/.bash_aliases (this file)
# @TODO: Document the prerequisite application, ondir, across various projects and on MacOS / Windows (CygWin) (https://github.com/alecthomas/ondir). This utility will set project-specific aliases and env variables

function getXPath()
{
    # This takes too long to parse pom.xml on each invocation:
    #echo $(mvn help:evaluate -Dexpression=${1} -q -DforceStdout)

    # This is crummy
    #sed -n -e '/<${1}>/,/<\/${1}>/p' pom.xml | tr -d '\n\t '

    # @TODO: document this in the installation prerequisites
    # xml2 is ideal, but requires installation with brew: $ brew install -y xml2
    propertyValue=$(xml2 < pom.xml  | grep ${1}= | sed 's/.*=//')
    echo ${propertyValue/\$\{*\}/}
}

function setupGitHooks()
{
    hooksPath=${DIR_HYBRIS_PROJECT}/custom-assets/git/hooks/
    ln -sf ${hooksPath}/pre-commit .git/hooks/pre-commit
    ln -sf ${hooksPath}/pre-rebase .git/hooks/pre-rebase
}

#DIR_HYBRIS_PROJECT=/Users/zselby/Development/projects/meijer
baseHybrisDir=$(getXPath properties/baseHybrisDir)
export DIR_HYBRIS_PROJECT=${PWD}
export PROJECT_NAME=$(getXPath project/artifactId)
export PROJECT_HYBRIS_VERSION=$(getXPath properties/hybrisVersion)
export PROJECT_HYBRIS_RECIPE=$(getXPath properties/hybrisRecipe)
export DIR_HYBRIS_CONFIG=$(getXPath properties/baseHybrisDir)$(getXPath properties/hybrisConfigDir)
export DIR_HYBRIS_PLATFORM=${DIR_HYBRIS_PROJECT}/$(getXPath properties/baseHybrisDir)$(getXPath properties/hybrisPlatformDir)
export DIR_HYBRIS_SOLR_CONTEXT=${DIR_HYBRIS_PROJECT}/$baseHybrisDir/$(getXPath properties/solrDockerContext)
export DIR_CUSTOM_PATCH=$(getXPath properties/customPatch)
export DIR_CUSTOM_SCRIPT=$(getXPath properties/customScriptDir)
export DIR_CI_EXT=$(getXPath properties/extensionsDir)
alias hydev="cd ${DIR_HYBRIS_PROJECT}"
alias hyplt="cd ${DIR_HYBRIS_PLATFORM}"
alias hyrun="cd ${DIR_HYBRIS_PLATFORM}; ./hybrisserver.sh debug"
alias hytail="docker-compose logs -f ${DIR_HYBRIS_PROJECT}/custom-assets/docker/docker-compose.yml"
alias hys="cd ${DIR_HYBRIS_PROJECT}"
alias hyp="cd ${DIR_HYBRIS_PLATFORM}"
alias hyc="cd ${DIR_HYBRIS_CONFIG}"
alias setant="hyp;. ./setantenv.sh"
alias hyb="setant;ant all"
alias hycb="setant;ant clean all"
alias mcc="hys;mvn clean compile"
alias hyg="hys;git status"
alias findbugs="hys;mvn findbugs:gui"
alias checkstyle="hys;mvn checkstyle:checkstyle"
alias grupdate="hys;git remote update"
alias grebase="grupdate;git rebase -i origin/develop-r1"
alias gdevelop="hys;git checkout develop-r1"
alias udevb="hyg;git checkout develop-r1;git pull origin develop-r1"
alias updatetime="sudo ntpdate time.nist.gov"
alias syncexts="~/Documents/syncExts.sh"

# Docker stuff
export TZ=${TZ:=America/Chicago}
export COMPOSE_PROJECT_NAME=${PROJECT_NAME}
export COMPOSE_FILE=custom-assets/docker/docker-compose.yaml

setupGitHooks

echo "Welcome to the ${PROJECT_NAME} project!"
